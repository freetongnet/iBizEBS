package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_automation;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_automationService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_automationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"自动动作" })
@RestController("Core-base_automation")
@RequestMapping("")
public class Base_automationResource {

    @Autowired
    public IBase_automationService base_automationService;

    @Autowired
    @Lazy
    public Base_automationMapping base_automationMapping;

    @PreAuthorize("hasPermission(this.base_automationMapping.toDomain(#base_automationdto),'iBizBusinessCentral-Base_automation-Create')")
    @ApiOperation(value = "新建自动动作", tags = {"自动动作" },  notes = "新建自动动作")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automations")
    public ResponseEntity<Base_automationDTO> create(@Validated @RequestBody Base_automationDTO base_automationdto) {
        Base_automation domain = base_automationMapping.toDomain(base_automationdto);
		base_automationService.create(domain);
        Base_automationDTO dto = base_automationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_automationMapping.toDomain(#base_automationdtos),'iBizBusinessCentral-Base_automation-Create')")
    @ApiOperation(value = "批量新建自动动作", tags = {"自动动作" },  notes = "批量新建自动动作")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_automationDTO> base_automationdtos) {
        base_automationService.createBatch(base_automationMapping.toDomain(base_automationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_automation" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_automationService.get(#base_automation_id),'iBizBusinessCentral-Base_automation-Update')")
    @ApiOperation(value = "更新自动动作", tags = {"自动动作" },  notes = "更新自动动作")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_automations/{base_automation_id}")
    public ResponseEntity<Base_automationDTO> update(@PathVariable("base_automation_id") Long base_automation_id, @RequestBody Base_automationDTO base_automationdto) {
		Base_automation domain  = base_automationMapping.toDomain(base_automationdto);
        domain .setId(base_automation_id);
		base_automationService.update(domain );
		Base_automationDTO dto = base_automationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_automationService.getBaseAutomationByEntities(this.base_automationMapping.toDomain(#base_automationdtos)),'iBizBusinessCentral-Base_automation-Update')")
    @ApiOperation(value = "批量更新自动动作", tags = {"自动动作" },  notes = "批量更新自动动作")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_automations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_automationDTO> base_automationdtos) {
        base_automationService.updateBatch(base_automationMapping.toDomain(base_automationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_automationService.get(#base_automation_id),'iBizBusinessCentral-Base_automation-Remove')")
    @ApiOperation(value = "删除自动动作", tags = {"自动动作" },  notes = "删除自动动作")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_automations/{base_automation_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_automation_id") Long base_automation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_automationService.remove(base_automation_id));
    }

    @PreAuthorize("hasPermission(this.base_automationService.getBaseAutomationByIds(#ids),'iBizBusinessCentral-Base_automation-Remove')")
    @ApiOperation(value = "批量删除自动动作", tags = {"自动动作" },  notes = "批量删除自动动作")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_automations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_automationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_automationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_automation-Get')")
    @ApiOperation(value = "获取自动动作", tags = {"自动动作" },  notes = "获取自动动作")
	@RequestMapping(method = RequestMethod.GET, value = "/base_automations/{base_automation_id}")
    public ResponseEntity<Base_automationDTO> get(@PathVariable("base_automation_id") Long base_automation_id) {
        Base_automation domain = base_automationService.get(base_automation_id);
        Base_automationDTO dto = base_automationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取自动动作草稿", tags = {"自动动作" },  notes = "获取自动动作草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_automations/getdraft")
    public ResponseEntity<Base_automationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_automationMapping.toDto(base_automationService.getDraft(new Base_automation())));
    }

    @ApiOperation(value = "检查自动动作", tags = {"自动动作" },  notes = "检查自动动作")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_automationDTO base_automationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_automationService.checkKey(base_automationMapping.toDomain(base_automationdto)));
    }

    @PreAuthorize("hasPermission(this.base_automationMapping.toDomain(#base_automationdto),'iBizBusinessCentral-Base_automation-Save')")
    @ApiOperation(value = "保存自动动作", tags = {"自动动作" },  notes = "保存自动动作")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automations/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_automationDTO base_automationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_automationService.save(base_automationMapping.toDomain(base_automationdto)));
    }

    @PreAuthorize("hasPermission(this.base_automationMapping.toDomain(#base_automationdtos),'iBizBusinessCentral-Base_automation-Save')")
    @ApiOperation(value = "批量保存自动动作", tags = {"自动动作" },  notes = "批量保存自动动作")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_automationDTO> base_automationdtos) {
        base_automationService.saveBatch(base_automationMapping.toDomain(base_automationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_automation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_automation-Get')")
	@ApiOperation(value = "获取数据集", tags = {"自动动作" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_automations/fetchdefault")
	public ResponseEntity<List<Base_automationDTO>> fetchDefault(Base_automationSearchContext context) {
        Page<Base_automation> domains = base_automationService.searchDefault(context) ;
        List<Base_automationDTO> list = base_automationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_automation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_automation-Get')")
	@ApiOperation(value = "查询数据集", tags = {"自动动作" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_automations/searchdefault")
	public ResponseEntity<Page<Base_automationDTO>> searchDefault(@RequestBody Base_automationSearchContext context) {
        Page<Base_automation> domains = base_automationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_automationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


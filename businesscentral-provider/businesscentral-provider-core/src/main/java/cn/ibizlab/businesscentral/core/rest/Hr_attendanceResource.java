package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_attendance;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_attendanceService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_attendanceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"出勤" })
@RestController("Core-hr_attendance")
@RequestMapping("")
public class Hr_attendanceResource {

    @Autowired
    public IHr_attendanceService hr_attendanceService;

    @Autowired
    @Lazy
    public Hr_attendanceMapping hr_attendanceMapping;

    @PreAuthorize("hasPermission(this.hr_attendanceMapping.toDomain(#hr_attendancedto),'iBizBusinessCentral-Hr_attendance-Create')")
    @ApiOperation(value = "新建出勤", tags = {"出勤" },  notes = "新建出勤")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_attendances")
    public ResponseEntity<Hr_attendanceDTO> create(@Validated @RequestBody Hr_attendanceDTO hr_attendancedto) {
        Hr_attendance domain = hr_attendanceMapping.toDomain(hr_attendancedto);
		hr_attendanceService.create(domain);
        Hr_attendanceDTO dto = hr_attendanceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_attendanceMapping.toDomain(#hr_attendancedtos),'iBizBusinessCentral-Hr_attendance-Create')")
    @ApiOperation(value = "批量新建出勤", tags = {"出勤" },  notes = "批量新建出勤")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_attendances/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_attendanceDTO> hr_attendancedtos) {
        hr_attendanceService.createBatch(hr_attendanceMapping.toDomain(hr_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_attendance" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_attendanceService.get(#hr_attendance_id),'iBizBusinessCentral-Hr_attendance-Update')")
    @ApiOperation(value = "更新出勤", tags = {"出勤" },  notes = "更新出勤")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_attendances/{hr_attendance_id}")
    public ResponseEntity<Hr_attendanceDTO> update(@PathVariable("hr_attendance_id") Long hr_attendance_id, @RequestBody Hr_attendanceDTO hr_attendancedto) {
		Hr_attendance domain  = hr_attendanceMapping.toDomain(hr_attendancedto);
        domain .setId(hr_attendance_id);
		hr_attendanceService.update(domain );
		Hr_attendanceDTO dto = hr_attendanceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_attendanceService.getHrAttendanceByEntities(this.hr_attendanceMapping.toDomain(#hr_attendancedtos)),'iBizBusinessCentral-Hr_attendance-Update')")
    @ApiOperation(value = "批量更新出勤", tags = {"出勤" },  notes = "批量更新出勤")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_attendances/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_attendanceDTO> hr_attendancedtos) {
        hr_attendanceService.updateBatch(hr_attendanceMapping.toDomain(hr_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_attendanceService.get(#hr_attendance_id),'iBizBusinessCentral-Hr_attendance-Remove')")
    @ApiOperation(value = "删除出勤", tags = {"出勤" },  notes = "删除出勤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_attendances/{hr_attendance_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_attendance_id") Long hr_attendance_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_attendanceService.remove(hr_attendance_id));
    }

    @PreAuthorize("hasPermission(this.hr_attendanceService.getHrAttendanceByIds(#ids),'iBizBusinessCentral-Hr_attendance-Remove')")
    @ApiOperation(value = "批量删除出勤", tags = {"出勤" },  notes = "批量删除出勤")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_attendances/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_attendanceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_attendanceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_attendance-Get')")
    @ApiOperation(value = "获取出勤", tags = {"出勤" },  notes = "获取出勤")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_attendances/{hr_attendance_id}")
    public ResponseEntity<Hr_attendanceDTO> get(@PathVariable("hr_attendance_id") Long hr_attendance_id) {
        Hr_attendance domain = hr_attendanceService.get(hr_attendance_id);
        Hr_attendanceDTO dto = hr_attendanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取出勤草稿", tags = {"出勤" },  notes = "获取出勤草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_attendances/getdraft")
    public ResponseEntity<Hr_attendanceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_attendanceMapping.toDto(hr_attendanceService.getDraft(new Hr_attendance())));
    }

    @ApiOperation(value = "检查出勤", tags = {"出勤" },  notes = "检查出勤")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_attendances/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_attendanceDTO hr_attendancedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_attendanceService.checkKey(hr_attendanceMapping.toDomain(hr_attendancedto)));
    }

    @PreAuthorize("hasPermission(this.hr_attendanceMapping.toDomain(#hr_attendancedto),'iBizBusinessCentral-Hr_attendance-Save')")
    @ApiOperation(value = "保存出勤", tags = {"出勤" },  notes = "保存出勤")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_attendances/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_attendanceDTO hr_attendancedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_attendanceService.save(hr_attendanceMapping.toDomain(hr_attendancedto)));
    }

    @PreAuthorize("hasPermission(this.hr_attendanceMapping.toDomain(#hr_attendancedtos),'iBizBusinessCentral-Hr_attendance-Save')")
    @ApiOperation(value = "批量保存出勤", tags = {"出勤" },  notes = "批量保存出勤")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_attendances/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_attendanceDTO> hr_attendancedtos) {
        hr_attendanceService.saveBatch(hr_attendanceMapping.toDomain(hr_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_attendance-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_attendance-Get')")
	@ApiOperation(value = "获取数据集", tags = {"出勤" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_attendances/fetchdefault")
	public ResponseEntity<List<Hr_attendanceDTO>> fetchDefault(Hr_attendanceSearchContext context) {
        Page<Hr_attendance> domains = hr_attendanceService.searchDefault(context) ;
        List<Hr_attendanceDTO> list = hr_attendanceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_attendance-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_attendance-Get')")
	@ApiOperation(value = "查询数据集", tags = {"出勤" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_attendances/searchdefault")
	public ResponseEntity<Page<Hr_attendanceDTO>> searchDefault(@RequestBody Hr_attendanceSearchContext context) {
        Page<Hr_attendance> domains = hr_attendanceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_attendanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


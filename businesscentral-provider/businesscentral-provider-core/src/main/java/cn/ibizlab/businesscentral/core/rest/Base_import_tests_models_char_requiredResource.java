package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char_required;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_char_requiredService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_char_requiredSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型，字符必选" })
@RestController("Core-base_import_tests_models_char_required")
@RequestMapping("")
public class Base_import_tests_models_char_requiredResource {

    @Autowired
    public IBase_import_tests_models_char_requiredService base_import_tests_models_char_requiredService;

    @Autowired
    @Lazy
    public Base_import_tests_models_char_requiredMapping base_import_tests_models_char_requiredMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_requiredMapping.toDomain(#base_import_tests_models_char_requireddto),'iBizBusinessCentral-Base_import_tests_models_char_required-Create')")
    @ApiOperation(value = "新建测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "新建测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds")
    public ResponseEntity<Base_import_tests_models_char_requiredDTO> create(@Validated @RequestBody Base_import_tests_models_char_requiredDTO base_import_tests_models_char_requireddto) {
        Base_import_tests_models_char_required domain = base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddto);
		base_import_tests_models_char_requiredService.create(domain);
        Base_import_tests_models_char_requiredDTO dto = base_import_tests_models_char_requiredMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_requiredMapping.toDomain(#base_import_tests_models_char_requireddtos),'iBizBusinessCentral-Base_import_tests_models_char_required-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "批量新建测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_char_requiredDTO> base_import_tests_models_char_requireddtos) {
        base_import_tests_models_char_requiredService.createBatch(base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_char_required" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_char_requiredService.get(#base_import_tests_models_char_required_id),'iBizBusinessCentral-Base_import_tests_models_char_required-Update')")
    @ApiOperation(value = "更新测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "更新测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_requireds/{base_import_tests_models_char_required_id}")
    public ResponseEntity<Base_import_tests_models_char_requiredDTO> update(@PathVariable("base_import_tests_models_char_required_id") Long base_import_tests_models_char_required_id, @RequestBody Base_import_tests_models_char_requiredDTO base_import_tests_models_char_requireddto) {
		Base_import_tests_models_char_required domain  = base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddto);
        domain .setId(base_import_tests_models_char_required_id);
		base_import_tests_models_char_requiredService.update(domain );
		Base_import_tests_models_char_requiredDTO dto = base_import_tests_models_char_requiredMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_requiredService.getBaseImportTestsModelsCharRequiredByEntities(this.base_import_tests_models_char_requiredMapping.toDomain(#base_import_tests_models_char_requireddtos)),'iBizBusinessCentral-Base_import_tests_models_char_required-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "批量更新测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_requireds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_char_requiredDTO> base_import_tests_models_char_requireddtos) {
        base_import_tests_models_char_requiredService.updateBatch(base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_requiredService.get(#base_import_tests_models_char_required_id),'iBizBusinessCentral-Base_import_tests_models_char_required-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "删除测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_requireds/{base_import_tests_models_char_required_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_required_id") Long base_import_tests_models_char_required_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_requiredService.remove(base_import_tests_models_char_required_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_requiredService.getBaseImportTestsModelsCharRequiredByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_char_required-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "批量删除测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_requireds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_char_requiredService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_char_requiredMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_char_required-Get')")
    @ApiOperation(value = "获取测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "获取测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_requireds/{base_import_tests_models_char_required_id}")
    public ResponseEntity<Base_import_tests_models_char_requiredDTO> get(@PathVariable("base_import_tests_models_char_required_id") Long base_import_tests_models_char_required_id) {
        Base_import_tests_models_char_required domain = base_import_tests_models_char_requiredService.get(base_import_tests_models_char_required_id);
        Base_import_tests_models_char_requiredDTO dto = base_import_tests_models_char_requiredMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型，字符必选草稿", tags = {"测试:基本导入模型，字符必选" },  notes = "获取测试:基本导入模型，字符必选草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_requireds/getdraft")
    public ResponseEntity<Base_import_tests_models_char_requiredDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_requiredMapping.toDto(base_import_tests_models_char_requiredService.getDraft(new Base_import_tests_models_char_required())));
    }

    @ApiOperation(value = "检查测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "检查测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_char_requiredDTO base_import_tests_models_char_requireddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_requiredService.checkKey(base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_requiredMapping.toDomain(#base_import_tests_models_char_requireddto),'iBizBusinessCentral-Base_import_tests_models_char_required-Save')")
    @ApiOperation(value = "保存测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "保存测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_char_requiredDTO base_import_tests_models_char_requireddto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_requiredService.save(base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_requiredMapping.toDomain(#base_import_tests_models_char_requireddtos),'iBizBusinessCentral-Base_import_tests_models_char_required-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型，字符必选", tags = {"测试:基本导入模型，字符必选" },  notes = "批量保存测试:基本导入模型，字符必选")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_requireds/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_char_requiredDTO> base_import_tests_models_char_requireddtos) {
        base_import_tests_models_char_requiredService.saveBatch(base_import_tests_models_char_requiredMapping.toDomain(base_import_tests_models_char_requireddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_char_required-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_char_required-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型，字符必选" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_char_requireds/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_char_requiredDTO>> fetchDefault(Base_import_tests_models_char_requiredSearchContext context) {
        Page<Base_import_tests_models_char_required> domains = base_import_tests_models_char_requiredService.searchDefault(context) ;
        List<Base_import_tests_models_char_requiredDTO> list = base_import_tests_models_char_requiredMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_char_required-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_char_required-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型，字符必选" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_char_requireds/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_char_requiredDTO>> searchDefault(@RequestBody Base_import_tests_models_char_requiredSearchContext context) {
        Page<Base_import_tests_models_char_required> domains = base_import_tests_models_char_requiredService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_char_requiredMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_unbuildService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"拆解数量短缺警告" })
@RestController("Core-stock_warn_insufficient_qty_unbuild")
@RequestMapping("")
public class Stock_warn_insufficient_qty_unbuildResource {

    @Autowired
    public IStock_warn_insufficient_qty_unbuildService stock_warn_insufficient_qty_unbuildService;

    @Autowired
    @Lazy
    public Stock_warn_insufficient_qty_unbuildMapping stock_warn_insufficient_qty_unbuildMapping;

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_unbuildMapping.toDomain(#stock_warn_insufficient_qty_unbuilddto),'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Create')")
    @ApiOperation(value = "新建拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "新建拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds")
    public ResponseEntity<Stock_warn_insufficient_qty_unbuildDTO> create(@Validated @RequestBody Stock_warn_insufficient_qty_unbuildDTO stock_warn_insufficient_qty_unbuilddto) {
        Stock_warn_insufficient_qty_unbuild domain = stock_warn_insufficient_qty_unbuildMapping.toDomain(stock_warn_insufficient_qty_unbuilddto);
		stock_warn_insufficient_qty_unbuildService.create(domain);
        Stock_warn_insufficient_qty_unbuildDTO dto = stock_warn_insufficient_qty_unbuildMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_unbuildMapping.toDomain(#stock_warn_insufficient_qty_unbuilddtos),'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Create')")
    @ApiOperation(value = "批量新建拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "批量新建拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_warn_insufficient_qty_unbuildDTO> stock_warn_insufficient_qty_unbuilddtos) {
        stock_warn_insufficient_qty_unbuildService.createBatch(stock_warn_insufficient_qty_unbuildMapping.toDomain(stock_warn_insufficient_qty_unbuilddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_warn_insufficient_qty_unbuild" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_unbuildService.get(#stock_warn_insufficient_qty_unbuild_id),'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Update')")
    @ApiOperation(value = "更新拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "更新拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_unbuilds/{stock_warn_insufficient_qty_unbuild_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_unbuildDTO> update(@PathVariable("stock_warn_insufficient_qty_unbuild_id") Long stock_warn_insufficient_qty_unbuild_id, @RequestBody Stock_warn_insufficient_qty_unbuildDTO stock_warn_insufficient_qty_unbuilddto) {
		Stock_warn_insufficient_qty_unbuild domain  = stock_warn_insufficient_qty_unbuildMapping.toDomain(stock_warn_insufficient_qty_unbuilddto);
        domain .setId(stock_warn_insufficient_qty_unbuild_id);
		stock_warn_insufficient_qty_unbuildService.update(domain );
		Stock_warn_insufficient_qty_unbuildDTO dto = stock_warn_insufficient_qty_unbuildMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_unbuildService.getStockWarnInsufficientQtyUnbuildByEntities(this.stock_warn_insufficient_qty_unbuildMapping.toDomain(#stock_warn_insufficient_qty_unbuilddtos)),'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Update')")
    @ApiOperation(value = "批量更新拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "批量更新拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_unbuilds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warn_insufficient_qty_unbuildDTO> stock_warn_insufficient_qty_unbuilddtos) {
        stock_warn_insufficient_qty_unbuildService.updateBatch(stock_warn_insufficient_qty_unbuildMapping.toDomain(stock_warn_insufficient_qty_unbuilddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_unbuildService.get(#stock_warn_insufficient_qty_unbuild_id),'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Remove')")
    @ApiOperation(value = "删除拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "删除拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_unbuilds/{stock_warn_insufficient_qty_unbuild_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_warn_insufficient_qty_unbuild_id") Long stock_warn_insufficient_qty_unbuild_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_unbuildService.remove(stock_warn_insufficient_qty_unbuild_id));
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_unbuildService.getStockWarnInsufficientQtyUnbuildByIds(#ids),'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Remove')")
    @ApiOperation(value = "批量删除拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "批量删除拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_unbuilds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_warn_insufficient_qty_unbuildService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_warn_insufficient_qty_unbuildMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Get')")
    @ApiOperation(value = "获取拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "获取拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_unbuilds/{stock_warn_insufficient_qty_unbuild_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_unbuildDTO> get(@PathVariable("stock_warn_insufficient_qty_unbuild_id") Long stock_warn_insufficient_qty_unbuild_id) {
        Stock_warn_insufficient_qty_unbuild domain = stock_warn_insufficient_qty_unbuildService.get(stock_warn_insufficient_qty_unbuild_id);
        Stock_warn_insufficient_qty_unbuildDTO dto = stock_warn_insufficient_qty_unbuildMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取拆解数量短缺警告草稿", tags = {"拆解数量短缺警告" },  notes = "获取拆解数量短缺警告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_unbuilds/getdraft")
    public ResponseEntity<Stock_warn_insufficient_qty_unbuildDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_unbuildMapping.toDto(stock_warn_insufficient_qty_unbuildService.getDraft(new Stock_warn_insufficient_qty_unbuild())));
    }

    @ApiOperation(value = "检查拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "检查拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_warn_insufficient_qty_unbuildDTO stock_warn_insufficient_qty_unbuilddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_unbuildService.checkKey(stock_warn_insufficient_qty_unbuildMapping.toDomain(stock_warn_insufficient_qty_unbuilddto)));
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_unbuildMapping.toDomain(#stock_warn_insufficient_qty_unbuilddto),'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Save')")
    @ApiOperation(value = "保存拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "保存拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_warn_insufficient_qty_unbuildDTO stock_warn_insufficient_qty_unbuilddto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_unbuildService.save(stock_warn_insufficient_qty_unbuildMapping.toDomain(stock_warn_insufficient_qty_unbuilddto)));
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_unbuildMapping.toDomain(#stock_warn_insufficient_qty_unbuilddtos),'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Save')")
    @ApiOperation(value = "批量保存拆解数量短缺警告", tags = {"拆解数量短缺警告" },  notes = "批量保存拆解数量短缺警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_unbuilds/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_warn_insufficient_qty_unbuildDTO> stock_warn_insufficient_qty_unbuilddtos) {
        stock_warn_insufficient_qty_unbuildService.saveBatch(stock_warn_insufficient_qty_unbuildMapping.toDomain(stock_warn_insufficient_qty_unbuilddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Get')")
	@ApiOperation(value = "获取数据集", tags = {"拆解数量短缺警告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warn_insufficient_qty_unbuilds/fetchdefault")
	public ResponseEntity<List<Stock_warn_insufficient_qty_unbuildDTO>> fetchDefault(Stock_warn_insufficient_qty_unbuildSearchContext context) {
        Page<Stock_warn_insufficient_qty_unbuild> domains = stock_warn_insufficient_qty_unbuildService.searchDefault(context) ;
        List<Stock_warn_insufficient_qty_unbuildDTO> list = stock_warn_insufficient_qty_unbuildMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warn_insufficient_qty_unbuild-Get')")
	@ApiOperation(value = "查询数据集", tags = {"拆解数量短缺警告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_warn_insufficient_qty_unbuilds/searchdefault")
	public ResponseEntity<Page<Stock_warn_insufficient_qty_unbuildDTO>> searchDefault(@RequestBody Stock_warn_insufficient_qty_unbuildSearchContext context) {
        Page<Stock_warn_insufficient_qty_unbuild> domains = stock_warn_insufficient_qty_unbuildService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_warn_insufficient_qty_unbuildMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


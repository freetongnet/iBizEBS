package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_channel;
import cn.ibizlab.businesscentral.core.dto.Im_livechat_report_channelDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreIm_livechat_report_channelMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Im_livechat_report_channelMapping extends MappingBase<Im_livechat_report_channelDTO, Im_livechat_report_channel> {


}


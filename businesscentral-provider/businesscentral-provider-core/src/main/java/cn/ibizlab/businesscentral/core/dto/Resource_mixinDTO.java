package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Resource_mixinDTO]
 */
@Data
public class Resource_mixinDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [TZ]
     *
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String tz;

    /**
     * 属性 [RESOURCE_ID_TEXT]
     *
     */
    @JSONField(name = "resource_id_text")
    @JsonProperty("resource_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resourceIdText;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String resourceCalendarIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long resourceCalendarId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [RESOURCE_ID]
     *
     */
    @JSONField(name = "resource_id")
    @JsonProperty("resource_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[资源]不允许为空!")
    private Long resourceId;


    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    public void setResourceCalendarId(Long  resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [RESOURCE_ID]
     */
    public void setResourceId(Long  resourceId){
        this.resourceId = resourceId ;
        this.modify("resource_id",resourceId);
    }


}



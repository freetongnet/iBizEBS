package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leaveService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leaveSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"休假" })
@RestController("Core-hr_leave")
@RequestMapping("")
public class Hr_leaveResource {

    @Autowired
    public IHr_leaveService hr_leaveService;

    @Autowired
    @Lazy
    public Hr_leaveMapping hr_leaveMapping;

    @PreAuthorize("hasPermission(this.hr_leaveMapping.toDomain(#hr_leavedto),'iBizBusinessCentral-Hr_leave-Create')")
    @ApiOperation(value = "新建休假", tags = {"休假" },  notes = "新建休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leaves")
    public ResponseEntity<Hr_leaveDTO> create(@Validated @RequestBody Hr_leaveDTO hr_leavedto) {
        Hr_leave domain = hr_leaveMapping.toDomain(hr_leavedto);
		hr_leaveService.create(domain);
        Hr_leaveDTO dto = hr_leaveMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_leaveMapping.toDomain(#hr_leavedtos),'iBizBusinessCentral-Hr_leave-Create')")
    @ApiOperation(value = "批量新建休假", tags = {"休假" },  notes = "批量新建休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        hr_leaveService.createBatch(hr_leaveMapping.toDomain(hr_leavedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_leave" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_leaveService.get(#hr_leave_id),'iBizBusinessCentral-Hr_leave-Update')")
    @ApiOperation(value = "更新休假", tags = {"休假" },  notes = "更新休假")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leaves/{hr_leave_id}")
    public ResponseEntity<Hr_leaveDTO> update(@PathVariable("hr_leave_id") Long hr_leave_id, @RequestBody Hr_leaveDTO hr_leavedto) {
		Hr_leave domain  = hr_leaveMapping.toDomain(hr_leavedto);
        domain .setId(hr_leave_id);
		hr_leaveService.update(domain );
		Hr_leaveDTO dto = hr_leaveMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_leaveService.getHrLeaveByEntities(this.hr_leaveMapping.toDomain(#hr_leavedtos)),'iBizBusinessCentral-Hr_leave-Update')")
    @ApiOperation(value = "批量更新休假", tags = {"休假" },  notes = "批量更新休假")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leaves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        hr_leaveService.updateBatch(hr_leaveMapping.toDomain(hr_leavedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_leaveService.get(#hr_leave_id),'iBizBusinessCentral-Hr_leave-Remove')")
    @ApiOperation(value = "删除休假", tags = {"休假" },  notes = "删除休假")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leaves/{hr_leave_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_id") Long hr_leave_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_leaveService.remove(hr_leave_id));
    }

    @PreAuthorize("hasPermission(this.hr_leaveService.getHrLeaveByIds(#ids),'iBizBusinessCentral-Hr_leave-Remove')")
    @ApiOperation(value = "批量删除休假", tags = {"休假" },  notes = "批量删除休假")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leaves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_leaveService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_leaveMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_leave-Get')")
    @ApiOperation(value = "获取休假", tags = {"休假" },  notes = "获取休假")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leaves/{hr_leave_id}")
    public ResponseEntity<Hr_leaveDTO> get(@PathVariable("hr_leave_id") Long hr_leave_id) {
        Hr_leave domain = hr_leaveService.get(hr_leave_id);
        Hr_leaveDTO dto = hr_leaveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取休假草稿", tags = {"休假" },  notes = "获取休假草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leaves/getdraft")
    public ResponseEntity<Hr_leaveDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_leaveMapping.toDto(hr_leaveService.getDraft(new Hr_leave())));
    }

    @ApiOperation(value = "检查休假", tags = {"休假" },  notes = "检查休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_leaveDTO hr_leavedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_leaveService.checkKey(hr_leaveMapping.toDomain(hr_leavedto)));
    }

    @PreAuthorize("hasPermission(this.hr_leaveMapping.toDomain(#hr_leavedto),'iBizBusinessCentral-Hr_leave-Save')")
    @ApiOperation(value = "保存休假", tags = {"休假" },  notes = "保存休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_leaveDTO hr_leavedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_leaveService.save(hr_leaveMapping.toDomain(hr_leavedto)));
    }

    @PreAuthorize("hasPermission(this.hr_leaveMapping.toDomain(#hr_leavedtos),'iBizBusinessCentral-Hr_leave-Save')")
    @ApiOperation(value = "批量保存休假", tags = {"休假" },  notes = "批量保存休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        hr_leaveService.saveBatch(hr_leaveMapping.toDomain(hr_leavedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_leave-Get')")
	@ApiOperation(value = "获取数据集", tags = {"休假" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leaves/fetchdefault")
	public ResponseEntity<List<Hr_leaveDTO>> fetchDefault(Hr_leaveSearchContext context) {
        Page<Hr_leave> domains = hr_leaveService.searchDefault(context) ;
        List<Hr_leaveDTO> list = hr_leaveMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_leave-Get')")
	@ApiOperation(value = "查询数据集", tags = {"休假" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_leaves/searchdefault")
	public ResponseEntity<Page<Hr_leaveDTO>> searchDefault(@RequestBody Hr_leaveSearchContext context) {
        Page<Hr_leave> domains = hr_leaveService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_leaveMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.hr_leaveMapping.toDomain(#hr_leavedto),'iBizBusinessCentral-Hr_leave-Create')")
    @ApiOperation(value = "根据员工建立休假", tags = {"休假" },  notes = "根据员工建立休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_leaves")
    public ResponseEntity<Hr_leaveDTO> createByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_leaveDTO hr_leavedto) {
        Hr_leave domain = hr_leaveMapping.toDomain(hr_leavedto);
        domain.setEmployeeId(hr_employee_id);
		hr_leaveService.create(domain);
        Hr_leaveDTO dto = hr_leaveMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_leaveMapping.toDomain(#hr_leavedtos),'iBizBusinessCentral-Hr_leave-Create')")
    @ApiOperation(value = "根据员工批量建立休假", tags = {"休假" },  notes = "根据员工批量建立休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_leaves/batch")
    public ResponseEntity<Boolean> createBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        List<Hr_leave> domainlist=hr_leaveMapping.toDomain(hr_leavedtos);
        for(Hr_leave domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        hr_leaveService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_leave" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_leaveService.get(#hr_leave_id),'iBizBusinessCentral-Hr_leave-Update')")
    @ApiOperation(value = "根据员工更新休假", tags = {"休假" },  notes = "根据员工更新休假")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/hr_leaves/{hr_leave_id}")
    public ResponseEntity<Hr_leaveDTO> updateByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_leave_id") Long hr_leave_id, @RequestBody Hr_leaveDTO hr_leavedto) {
        Hr_leave domain = hr_leaveMapping.toDomain(hr_leavedto);
        domain.setEmployeeId(hr_employee_id);
        domain.setId(hr_leave_id);
		hr_leaveService.update(domain);
        Hr_leaveDTO dto = hr_leaveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_leaveService.getHrLeaveByEntities(this.hr_leaveMapping.toDomain(#hr_leavedtos)),'iBizBusinessCentral-Hr_leave-Update')")
    @ApiOperation(value = "根据员工批量更新休假", tags = {"休假" },  notes = "根据员工批量更新休假")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/hr_leaves/batch")
    public ResponseEntity<Boolean> updateBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        List<Hr_leave> domainlist=hr_leaveMapping.toDomain(hr_leavedtos);
        for(Hr_leave domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        hr_leaveService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_leaveService.get(#hr_leave_id),'iBizBusinessCentral-Hr_leave-Remove')")
    @ApiOperation(value = "根据员工删除休假", tags = {"休假" },  notes = "根据员工删除休假")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/hr_leaves/{hr_leave_id}")
    public ResponseEntity<Boolean> removeByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_leave_id") Long hr_leave_id) {
		return ResponseEntity.status(HttpStatus.OK).body(hr_leaveService.remove(hr_leave_id));
    }

    @PreAuthorize("hasPermission(this.hr_leaveService.getHrLeaveByIds(#ids),'iBizBusinessCentral-Hr_leave-Remove')")
    @ApiOperation(value = "根据员工批量删除休假", tags = {"休假" },  notes = "根据员工批量删除休假")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/hr_leaves/batch")
    public ResponseEntity<Boolean> removeBatchByHr_employee(@RequestBody List<Long> ids) {
        hr_leaveService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_leaveMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_leave-Get')")
    @ApiOperation(value = "根据员工获取休假", tags = {"休假" },  notes = "根据员工获取休假")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/hr_leaves/{hr_leave_id}")
    public ResponseEntity<Hr_leaveDTO> getByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("hr_leave_id") Long hr_leave_id) {
        Hr_leave domain = hr_leaveService.get(hr_leave_id);
        Hr_leaveDTO dto = hr_leaveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据员工获取休假草稿", tags = {"休假" },  notes = "根据员工获取休假草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/hr_leaves/getdraft")
    public ResponseEntity<Hr_leaveDTO> getDraftByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id) {
        Hr_leave domain = new Hr_leave();
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_leaveMapping.toDto(hr_leaveService.getDraft(domain)));
    }

    @ApiOperation(value = "根据员工检查休假", tags = {"休假" },  notes = "根据员工检查休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_leaves/checkkey")
    public ResponseEntity<Boolean> checkKeyByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_leaveDTO hr_leavedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_leaveService.checkKey(hr_leaveMapping.toDomain(hr_leavedto)));
    }

    @PreAuthorize("hasPermission(this.hr_leaveMapping.toDomain(#hr_leavedto),'iBizBusinessCentral-Hr_leave-Save')")
    @ApiOperation(value = "根据员工保存休假", tags = {"休假" },  notes = "根据员工保存休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_leaves/save")
    public ResponseEntity<Boolean> saveByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_leaveDTO hr_leavedto) {
        Hr_leave domain = hr_leaveMapping.toDomain(hr_leavedto);
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_leaveService.save(domain));
    }

    @PreAuthorize("hasPermission(this.hr_leaveMapping.toDomain(#hr_leavedtos),'iBizBusinessCentral-Hr_leave-Save')")
    @ApiOperation(value = "根据员工批量保存休假", tags = {"休假" },  notes = "根据员工批量保存休假")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/hr_leaves/savebatch")
    public ResponseEntity<Boolean> saveBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        List<Hr_leave> domainlist=hr_leaveMapping.toDomain(hr_leavedtos);
        for(Hr_leave domain:domainlist){
             domain.setEmployeeId(hr_employee_id);
        }
        hr_leaveService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_leave-Get')")
	@ApiOperation(value = "根据员工获取数据集", tags = {"休假" } ,notes = "根据员工获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employees/{hr_employee_id}/hr_leaves/fetchdefault")
	public ResponseEntity<List<Hr_leaveDTO>> fetchHr_leaveDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id,Hr_leaveSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Hr_leave> domains = hr_leaveService.searchDefault(context) ;
        List<Hr_leaveDTO> list = hr_leaveMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_leave-Get')")
	@ApiOperation(value = "根据员工查询数据集", tags = {"休假" } ,notes = "根据员工查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_employees/{hr_employee_id}/hr_leaves/searchdefault")
	public ResponseEntity<Page<Hr_leaveDTO>> searchHr_leaveDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Hr_leaveSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Hr_leave> domains = hr_leaveService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_leaveMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


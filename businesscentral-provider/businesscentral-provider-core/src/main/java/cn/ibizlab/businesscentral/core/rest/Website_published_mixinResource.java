package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_published_mixin;
import cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_published_mixinService;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_published_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"网站发布Mixin" })
@RestController("Core-website_published_mixin")
@RequestMapping("")
public class Website_published_mixinResource {

    @Autowired
    public IWebsite_published_mixinService website_published_mixinService;

    @Autowired
    @Lazy
    public Website_published_mixinMapping website_published_mixinMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-Create-all')")
    @ApiOperation(value = "新建网站发布Mixin", tags = {"网站发布Mixin" },  notes = "新建网站发布Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_published_mixins")
    public ResponseEntity<Website_published_mixinDTO> create(@Validated @RequestBody Website_published_mixinDTO website_published_mixindto) {
        Website_published_mixin domain = website_published_mixinMapping.toDomain(website_published_mixindto);
		website_published_mixinService.create(domain);
        Website_published_mixinDTO dto = website_published_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-Create-all')")
    @ApiOperation(value = "批量新建网站发布Mixin", tags = {"网站发布Mixin" },  notes = "批量新建网站发布Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_published_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_published_mixinDTO> website_published_mixindtos) {
        website_published_mixinService.createBatch(website_published_mixinMapping.toDomain(website_published_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-Update-all')")
    @ApiOperation(value = "更新网站发布Mixin", tags = {"网站发布Mixin" },  notes = "更新网站发布Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_published_mixins/{website_published_mixin_id}")
    public ResponseEntity<Website_published_mixinDTO> update(@PathVariable("website_published_mixin_id") Long website_published_mixin_id, @RequestBody Website_published_mixinDTO website_published_mixindto) {
		Website_published_mixin domain  = website_published_mixinMapping.toDomain(website_published_mixindto);
        domain .setId(website_published_mixin_id);
		website_published_mixinService.update(domain );
		Website_published_mixinDTO dto = website_published_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-Update-all')")
    @ApiOperation(value = "批量更新网站发布Mixin", tags = {"网站发布Mixin" },  notes = "批量更新网站发布Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_published_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_published_mixinDTO> website_published_mixindtos) {
        website_published_mixinService.updateBatch(website_published_mixinMapping.toDomain(website_published_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-Remove-all')")
    @ApiOperation(value = "删除网站发布Mixin", tags = {"网站发布Mixin" },  notes = "删除网站发布Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_published_mixins/{website_published_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("website_published_mixin_id") Long website_published_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_published_mixinService.remove(website_published_mixin_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-Remove-all')")
    @ApiOperation(value = "批量删除网站发布Mixin", tags = {"网站发布Mixin" },  notes = "批量删除网站发布Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_published_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        website_published_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-Get-all')")
    @ApiOperation(value = "获取网站发布Mixin", tags = {"网站发布Mixin" },  notes = "获取网站发布Mixin")
	@RequestMapping(method = RequestMethod.GET, value = "/website_published_mixins/{website_published_mixin_id}")
    public ResponseEntity<Website_published_mixinDTO> get(@PathVariable("website_published_mixin_id") Long website_published_mixin_id) {
        Website_published_mixin domain = website_published_mixinService.get(website_published_mixin_id);
        Website_published_mixinDTO dto = website_published_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取网站发布Mixin草稿", tags = {"网站发布Mixin" },  notes = "获取网站发布Mixin草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/website_published_mixins/getdraft")
    public ResponseEntity<Website_published_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(website_published_mixinMapping.toDto(website_published_mixinService.getDraft(new Website_published_mixin())));
    }

    @ApiOperation(value = "检查网站发布Mixin", tags = {"网站发布Mixin" },  notes = "检查网站发布Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_published_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Website_published_mixinDTO website_published_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(website_published_mixinService.checkKey(website_published_mixinMapping.toDomain(website_published_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-Save-all')")
    @ApiOperation(value = "保存网站发布Mixin", tags = {"网站发布Mixin" },  notes = "保存网站发布Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_published_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Website_published_mixinDTO website_published_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(website_published_mixinService.save(website_published_mixinMapping.toDomain(website_published_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-Save-all')")
    @ApiOperation(value = "批量保存网站发布Mixin", tags = {"网站发布Mixin" },  notes = "批量保存网站发布Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/website_published_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Website_published_mixinDTO> website_published_mixindtos) {
        website_published_mixinService.saveBatch(website_published_mixinMapping.toDomain(website_published_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"网站发布Mixin" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/website_published_mixins/fetchdefault")
	public ResponseEntity<List<Website_published_mixinDTO>> fetchDefault(Website_published_mixinSearchContext context) {
        Page<Website_published_mixin> domains = website_published_mixinService.searchDefault(context) ;
        List<Website_published_mixinDTO> list = website_published_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_published_mixin-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"网站发布Mixin" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/website_published_mixins/searchdefault")
	public ResponseEntity<Page<Website_published_mixinDTO>> searchDefault(@RequestBody Website_published_mixinSearchContext context) {
        Page<Website_published_mixin> domains = website_published_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_published_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


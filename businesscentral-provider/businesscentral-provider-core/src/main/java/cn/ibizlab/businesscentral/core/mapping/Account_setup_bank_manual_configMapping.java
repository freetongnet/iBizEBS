package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.businesscentral.core.dto.Account_setup_bank_manual_configDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_setup_bank_manual_configMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_setup_bank_manual_configMapping extends MappingBase<Account_setup_bank_manual_configDTO, Account_setup_bank_manual_config> {


}


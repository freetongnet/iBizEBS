package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipmentService;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"保养设备" })
@RestController("Core-maintenance_equipment")
@RequestMapping("")
public class Maintenance_equipmentResource {

    @Autowired
    public IMaintenance_equipmentService maintenance_equipmentService;

    @Autowired
    @Lazy
    public Maintenance_equipmentMapping maintenance_equipmentMapping;

    @PreAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdto),'iBizBusinessCentral-Maintenance_equipment-Create')")
    @ApiOperation(value = "新建保养设备", tags = {"保养设备" },  notes = "新建保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments")
    public ResponseEntity<Maintenance_equipmentDTO> create(@Validated @RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
        Maintenance_equipment domain = maintenance_equipmentMapping.toDomain(maintenance_equipmentdto);
		maintenance_equipmentService.create(domain);
        Maintenance_equipmentDTO dto = maintenance_equipmentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdtos),'iBizBusinessCentral-Maintenance_equipment-Create')")
    @ApiOperation(value = "批量新建保养设备", tags = {"保养设备" },  notes = "批量新建保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        maintenance_equipmentService.createBatch(maintenance_equipmentMapping.toDomain(maintenance_equipmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "maintenance_equipment" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.maintenance_equipmentService.get(#maintenance_equipment_id),'iBizBusinessCentral-Maintenance_equipment-Update')")
    @ApiOperation(value = "更新保养设备", tags = {"保养设备" },  notes = "更新保养设备")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipments/{maintenance_equipment_id}")
    public ResponseEntity<Maintenance_equipmentDTO> update(@PathVariable("maintenance_equipment_id") Long maintenance_equipment_id, @RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
		Maintenance_equipment domain  = maintenance_equipmentMapping.toDomain(maintenance_equipmentdto);
        domain .setId(maintenance_equipment_id);
		maintenance_equipmentService.update(domain );
		Maintenance_equipmentDTO dto = maintenance_equipmentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentService.getMaintenanceEquipmentByEntities(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdtos)),'iBizBusinessCentral-Maintenance_equipment-Update')")
    @ApiOperation(value = "批量更新保养设备", tags = {"保养设备" },  notes = "批量更新保养设备")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        maintenance_equipmentService.updateBatch(maintenance_equipmentMapping.toDomain(maintenance_equipmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentService.get(#maintenance_equipment_id),'iBizBusinessCentral-Maintenance_equipment-Remove')")
    @ApiOperation(value = "删除保养设备", tags = {"保养设备" },  notes = "删除保养设备")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipments/{maintenance_equipment_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_equipment_id") Long maintenance_equipment_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipmentService.remove(maintenance_equipment_id));
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentService.getMaintenanceEquipmentByIds(#ids),'iBizBusinessCentral-Maintenance_equipment-Remove')")
    @ApiOperation(value = "批量删除保养设备", tags = {"保养设备" },  notes = "批量删除保养设备")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        maintenance_equipmentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(returnObject.body),'iBizBusinessCentral-Maintenance_equipment-Get')")
    @ApiOperation(value = "获取保养设备", tags = {"保养设备" },  notes = "获取保养设备")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipments/{maintenance_equipment_id}")
    public ResponseEntity<Maintenance_equipmentDTO> get(@PathVariable("maintenance_equipment_id") Long maintenance_equipment_id) {
        Maintenance_equipment domain = maintenance_equipmentService.get(maintenance_equipment_id);
        Maintenance_equipmentDTO dto = maintenance_equipmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取保养设备草稿", tags = {"保养设备" },  notes = "获取保养设备草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipments/getdraft")
    public ResponseEntity<Maintenance_equipmentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipmentMapping.toDto(maintenance_equipmentService.getDraft(new Maintenance_equipment())));
    }

    @ApiOperation(value = "检查保养设备", tags = {"保养设备" },  notes = "检查保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(maintenance_equipmentService.checkKey(maintenance_equipmentMapping.toDomain(maintenance_equipmentdto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdto),'iBizBusinessCentral-Maintenance_equipment-Save')")
    @ApiOperation(value = "保存保养设备", tags = {"保养设备" },  notes = "保存保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/save")
    public ResponseEntity<Boolean> save(@RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipmentService.save(maintenance_equipmentMapping.toDomain(maintenance_equipmentdto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdtos),'iBizBusinessCentral-Maintenance_equipment-Save')")
    @ApiOperation(value = "批量保存保养设备", tags = {"保养设备" },  notes = "批量保存保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        maintenance_equipmentService.saveBatch(maintenance_equipmentMapping.toDomain(maintenance_equipmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_equipment-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_equipment-Get')")
	@ApiOperation(value = "获取数据集", tags = {"保养设备" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_equipments/fetchdefault")
	public ResponseEntity<List<Maintenance_equipmentDTO>> fetchDefault(Maintenance_equipmentSearchContext context) {
        Page<Maintenance_equipment> domains = maintenance_equipmentService.searchDefault(context) ;
        List<Maintenance_equipmentDTO> list = maintenance_equipmentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_equipment-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_equipment-Get')")
	@ApiOperation(value = "查询数据集", tags = {"保养设备" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/maintenance_equipments/searchdefault")
	public ResponseEntity<Page<Maintenance_equipmentDTO>> searchDefault(@RequestBody Maintenance_equipmentSearchContext context) {
        Page<Maintenance_equipment> domains = maintenance_equipmentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_equipmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdto),'iBizBusinessCentral-Maintenance_equipment-Create')")
    @ApiOperation(value = "根据员工建立保养设备", tags = {"保养设备" },  notes = "根据员工建立保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/maintenance_equipments")
    public ResponseEntity<Maintenance_equipmentDTO> createByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
        Maintenance_equipment domain = maintenance_equipmentMapping.toDomain(maintenance_equipmentdto);
        domain.setEmployeeId(hr_employee_id);
		maintenance_equipmentService.create(domain);
        Maintenance_equipmentDTO dto = maintenance_equipmentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdtos),'iBizBusinessCentral-Maintenance_equipment-Create')")
    @ApiOperation(value = "根据员工批量建立保养设备", tags = {"保养设备" },  notes = "根据员工批量建立保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/batch")
    public ResponseEntity<Boolean> createBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        List<Maintenance_equipment> domainlist=maintenance_equipmentMapping.toDomain(maintenance_equipmentdtos);
        for(Maintenance_equipment domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        maintenance_equipmentService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "maintenance_equipment" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.maintenance_equipmentService.get(#maintenance_equipment_id),'iBizBusinessCentral-Maintenance_equipment-Update')")
    @ApiOperation(value = "根据员工更新保养设备", tags = {"保养设备" },  notes = "根据员工更新保养设备")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/{maintenance_equipment_id}")
    public ResponseEntity<Maintenance_equipmentDTO> updateByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("maintenance_equipment_id") Long maintenance_equipment_id, @RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
        Maintenance_equipment domain = maintenance_equipmentMapping.toDomain(maintenance_equipmentdto);
        domain.setEmployeeId(hr_employee_id);
        domain.setId(maintenance_equipment_id);
		maintenance_equipmentService.update(domain);
        Maintenance_equipmentDTO dto = maintenance_equipmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentService.getMaintenanceEquipmentByEntities(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdtos)),'iBizBusinessCentral-Maintenance_equipment-Update')")
    @ApiOperation(value = "根据员工批量更新保养设备", tags = {"保养设备" },  notes = "根据员工批量更新保养设备")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/batch")
    public ResponseEntity<Boolean> updateBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        List<Maintenance_equipment> domainlist=maintenance_equipmentMapping.toDomain(maintenance_equipmentdtos);
        for(Maintenance_equipment domain:domainlist){
            domain.setEmployeeId(hr_employee_id);
        }
        maintenance_equipmentService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentService.get(#maintenance_equipment_id),'iBizBusinessCentral-Maintenance_equipment-Remove')")
    @ApiOperation(value = "根据员工删除保养设备", tags = {"保养设备" },  notes = "根据员工删除保养设备")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/{maintenance_equipment_id}")
    public ResponseEntity<Boolean> removeByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("maintenance_equipment_id") Long maintenance_equipment_id) {
		return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipmentService.remove(maintenance_equipment_id));
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentService.getMaintenanceEquipmentByIds(#ids),'iBizBusinessCentral-Maintenance_equipment-Remove')")
    @ApiOperation(value = "根据员工批量删除保养设备", tags = {"保养设备" },  notes = "根据员工批量删除保养设备")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/batch")
    public ResponseEntity<Boolean> removeBatchByHr_employee(@RequestBody List<Long> ids) {
        maintenance_equipmentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(returnObject.body),'iBizBusinessCentral-Maintenance_equipment-Get')")
    @ApiOperation(value = "根据员工获取保养设备", tags = {"保养设备" },  notes = "根据员工获取保养设备")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/{maintenance_equipment_id}")
    public ResponseEntity<Maintenance_equipmentDTO> getByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @PathVariable("maintenance_equipment_id") Long maintenance_equipment_id) {
        Maintenance_equipment domain = maintenance_equipmentService.get(maintenance_equipment_id);
        Maintenance_equipmentDTO dto = maintenance_equipmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据员工获取保养设备草稿", tags = {"保养设备" },  notes = "根据员工获取保养设备草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/getdraft")
    public ResponseEntity<Maintenance_equipmentDTO> getDraftByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id) {
        Maintenance_equipment domain = new Maintenance_equipment();
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipmentMapping.toDto(maintenance_equipmentService.getDraft(domain)));
    }

    @ApiOperation(value = "根据员工检查保养设备", tags = {"保养设备" },  notes = "根据员工检查保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/checkkey")
    public ResponseEntity<Boolean> checkKeyByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(maintenance_equipmentService.checkKey(maintenance_equipmentMapping.toDomain(maintenance_equipmentdto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdto),'iBizBusinessCentral-Maintenance_equipment-Save')")
    @ApiOperation(value = "根据员工保存保养设备", tags = {"保养设备" },  notes = "根据员工保存保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/save")
    public ResponseEntity<Boolean> saveByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
        Maintenance_equipment domain = maintenance_equipmentMapping.toDomain(maintenance_equipmentdto);
        domain.setEmployeeId(hr_employee_id);
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipmentService.save(domain));
    }

    @PreAuthorize("hasPermission(this.maintenance_equipmentMapping.toDomain(#maintenance_equipmentdtos),'iBizBusinessCentral-Maintenance_equipment-Save')")
    @ApiOperation(value = "根据员工批量保存保养设备", tags = {"保养设备" },  notes = "根据员工批量保存保养设备")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/{hr_employee_id}/maintenance_equipments/savebatch")
    public ResponseEntity<Boolean> saveBatchByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        List<Maintenance_equipment> domainlist=maintenance_equipmentMapping.toDomain(maintenance_equipmentdtos);
        for(Maintenance_equipment domain:domainlist){
             domain.setEmployeeId(hr_employee_id);
        }
        maintenance_equipmentService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_equipment-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_equipment-Get')")
	@ApiOperation(value = "根据员工获取数据集", tags = {"保养设备" } ,notes = "根据员工获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employees/{hr_employee_id}/maintenance_equipments/fetchdefault")
	public ResponseEntity<List<Maintenance_equipmentDTO>> fetchMaintenance_equipmentDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id,Maintenance_equipmentSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Maintenance_equipment> domains = maintenance_equipmentService.searchDefault(context) ;
        List<Maintenance_equipmentDTO> list = maintenance_equipmentMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_equipment-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_equipment-Get')")
	@ApiOperation(value = "根据员工查询数据集", tags = {"保养设备" } ,notes = "根据员工查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_employees/{hr_employee_id}/maintenance_equipments/searchdefault")
	public ResponseEntity<Page<Maintenance_equipmentDTO>> searchMaintenance_equipmentDefaultByHr_employee(@PathVariable("hr_employee_id") Long hr_employee_id, @RequestBody Maintenance_equipmentSearchContext context) {
        context.setN_employee_id_eq(hr_employee_id);
        Page<Maintenance_equipment> domains = maintenance_equipmentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_equipmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


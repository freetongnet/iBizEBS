package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.businesscentral.core.dto.Crm_merge_opportunityDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreCrm_merge_opportunityMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_merge_opportunityMapping extends MappingBase<Crm_merge_opportunityDTO, Crm_merge_opportunity> {


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_installer;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_config_installerService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_config_installerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"配置安装器" })
@RestController("Core-res_config_installer")
@RequestMapping("")
public class Res_config_installerResource {

    @Autowired
    public IRes_config_installerService res_config_installerService;

    @Autowired
    @Lazy
    public Res_config_installerMapping res_config_installerMapping;

    @PreAuthorize("hasPermission(this.res_config_installerMapping.toDomain(#res_config_installerdto),'iBizBusinessCentral-Res_config_installer-Create')")
    @ApiOperation(value = "新建配置安装器", tags = {"配置安装器" },  notes = "新建配置安装器")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_installers")
    public ResponseEntity<Res_config_installerDTO> create(@Validated @RequestBody Res_config_installerDTO res_config_installerdto) {
        Res_config_installer domain = res_config_installerMapping.toDomain(res_config_installerdto);
		res_config_installerService.create(domain);
        Res_config_installerDTO dto = res_config_installerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_config_installerMapping.toDomain(#res_config_installerdtos),'iBizBusinessCentral-Res_config_installer-Create')")
    @ApiOperation(value = "批量新建配置安装器", tags = {"配置安装器" },  notes = "批量新建配置安装器")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_installers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_config_installerDTO> res_config_installerdtos) {
        res_config_installerService.createBatch(res_config_installerMapping.toDomain(res_config_installerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_config_installer" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_config_installerService.get(#res_config_installer_id),'iBizBusinessCentral-Res_config_installer-Update')")
    @ApiOperation(value = "更新配置安装器", tags = {"配置安装器" },  notes = "更新配置安装器")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_config_installers/{res_config_installer_id}")
    public ResponseEntity<Res_config_installerDTO> update(@PathVariable("res_config_installer_id") Long res_config_installer_id, @RequestBody Res_config_installerDTO res_config_installerdto) {
		Res_config_installer domain  = res_config_installerMapping.toDomain(res_config_installerdto);
        domain .setId(res_config_installer_id);
		res_config_installerService.update(domain );
		Res_config_installerDTO dto = res_config_installerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_config_installerService.getResConfigInstallerByEntities(this.res_config_installerMapping.toDomain(#res_config_installerdtos)),'iBizBusinessCentral-Res_config_installer-Update')")
    @ApiOperation(value = "批量更新配置安装器", tags = {"配置安装器" },  notes = "批量更新配置安装器")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_config_installers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_config_installerDTO> res_config_installerdtos) {
        res_config_installerService.updateBatch(res_config_installerMapping.toDomain(res_config_installerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_config_installerService.get(#res_config_installer_id),'iBizBusinessCentral-Res_config_installer-Remove')")
    @ApiOperation(value = "删除配置安装器", tags = {"配置安装器" },  notes = "删除配置安装器")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_config_installers/{res_config_installer_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_config_installer_id") Long res_config_installer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_config_installerService.remove(res_config_installer_id));
    }

    @PreAuthorize("hasPermission(this.res_config_installerService.getResConfigInstallerByIds(#ids),'iBizBusinessCentral-Res_config_installer-Remove')")
    @ApiOperation(value = "批量删除配置安装器", tags = {"配置安装器" },  notes = "批量删除配置安装器")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_config_installers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_config_installerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_config_installerMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_config_installer-Get')")
    @ApiOperation(value = "获取配置安装器", tags = {"配置安装器" },  notes = "获取配置安装器")
	@RequestMapping(method = RequestMethod.GET, value = "/res_config_installers/{res_config_installer_id}")
    public ResponseEntity<Res_config_installerDTO> get(@PathVariable("res_config_installer_id") Long res_config_installer_id) {
        Res_config_installer domain = res_config_installerService.get(res_config_installer_id);
        Res_config_installerDTO dto = res_config_installerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取配置安装器草稿", tags = {"配置安装器" },  notes = "获取配置安装器草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_config_installers/getdraft")
    public ResponseEntity<Res_config_installerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_config_installerMapping.toDto(res_config_installerService.getDraft(new Res_config_installer())));
    }

    @ApiOperation(value = "检查配置安装器", tags = {"配置安装器" },  notes = "检查配置安装器")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_installers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_config_installerDTO res_config_installerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_config_installerService.checkKey(res_config_installerMapping.toDomain(res_config_installerdto)));
    }

    @PreAuthorize("hasPermission(this.res_config_installerMapping.toDomain(#res_config_installerdto),'iBizBusinessCentral-Res_config_installer-Save')")
    @ApiOperation(value = "保存配置安装器", tags = {"配置安装器" },  notes = "保存配置安装器")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_installers/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_config_installerDTO res_config_installerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_config_installerService.save(res_config_installerMapping.toDomain(res_config_installerdto)));
    }

    @PreAuthorize("hasPermission(this.res_config_installerMapping.toDomain(#res_config_installerdtos),'iBizBusinessCentral-Res_config_installer-Save')")
    @ApiOperation(value = "批量保存配置安装器", tags = {"配置安装器" },  notes = "批量保存配置安装器")
	@RequestMapping(method = RequestMethod.POST, value = "/res_config_installers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_config_installerDTO> res_config_installerdtos) {
        res_config_installerService.saveBatch(res_config_installerMapping.toDomain(res_config_installerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_config_installer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_config_installer-Get')")
	@ApiOperation(value = "获取数据集", tags = {"配置安装器" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_config_installers/fetchdefault")
	public ResponseEntity<List<Res_config_installerDTO>> fetchDefault(Res_config_installerSearchContext context) {
        Page<Res_config_installer> domains = res_config_installerService.searchDefault(context) ;
        List<Res_config_installerDTO> list = res_config_installerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_config_installer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_config_installer-Get')")
	@ApiOperation(value = "查询数据集", tags = {"配置安装器" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_config_installers/searchdefault")
	public ResponseEntity<Page<Res_config_installerDTO>> searchDefault(@RequestBody Res_config_installerSearchContext context) {
        Page<Res_config_installer> domains = res_config_installerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_config_installerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


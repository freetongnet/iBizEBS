package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.businesscentral.core.dto.Crm_lead_lostDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreCrm_lead_lostMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_lead_lostMapping extends MappingBase<Crm_lead_lostDTO, Crm_lead_lost> {


}


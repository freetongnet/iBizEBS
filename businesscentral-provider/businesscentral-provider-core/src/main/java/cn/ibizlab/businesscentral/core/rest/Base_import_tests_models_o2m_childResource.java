package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_o2m_child;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_o2m_childService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_o2m_childSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型，一对多" })
@RestController("Core-base_import_tests_models_o2m_child")
@RequestMapping("")
public class Base_import_tests_models_o2m_childResource {

    @Autowired
    public IBase_import_tests_models_o2m_childService base_import_tests_models_o2m_childService;

    @Autowired
    @Lazy
    public Base_import_tests_models_o2m_childMapping base_import_tests_models_o2m_childMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2m_childMapping.toDomain(#base_import_tests_models_o2m_childdto),'iBizBusinessCentral-Base_import_tests_models_o2m_child-Create')")
    @ApiOperation(value = "新建测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "新建测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children")
    public ResponseEntity<Base_import_tests_models_o2m_childDTO> create(@Validated @RequestBody Base_import_tests_models_o2m_childDTO base_import_tests_models_o2m_childdto) {
        Base_import_tests_models_o2m_child domain = base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdto);
		base_import_tests_models_o2m_childService.create(domain);
        Base_import_tests_models_o2m_childDTO dto = base_import_tests_models_o2m_childMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2m_childMapping.toDomain(#base_import_tests_models_o2m_childdtos),'iBizBusinessCentral-Base_import_tests_models_o2m_child-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "批量新建测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_o2m_childDTO> base_import_tests_models_o2m_childdtos) {
        base_import_tests_models_o2m_childService.createBatch(base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_o2m_child" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_o2m_childService.get(#base_import_tests_models_o2m_child_id),'iBizBusinessCentral-Base_import_tests_models_o2m_child-Update')")
    @ApiOperation(value = "更新测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "更新测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2m_children/{base_import_tests_models_o2m_child_id}")
    public ResponseEntity<Base_import_tests_models_o2m_childDTO> update(@PathVariable("base_import_tests_models_o2m_child_id") Long base_import_tests_models_o2m_child_id, @RequestBody Base_import_tests_models_o2m_childDTO base_import_tests_models_o2m_childdto) {
		Base_import_tests_models_o2m_child domain  = base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdto);
        domain .setId(base_import_tests_models_o2m_child_id);
		base_import_tests_models_o2m_childService.update(domain );
		Base_import_tests_models_o2m_childDTO dto = base_import_tests_models_o2m_childMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2m_childService.getBaseImportTestsModelsO2mChildByEntities(this.base_import_tests_models_o2m_childMapping.toDomain(#base_import_tests_models_o2m_childdtos)),'iBizBusinessCentral-Base_import_tests_models_o2m_child-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "批量更新测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2m_children/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_o2m_childDTO> base_import_tests_models_o2m_childdtos) {
        base_import_tests_models_o2m_childService.updateBatch(base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2m_childService.get(#base_import_tests_models_o2m_child_id),'iBizBusinessCentral-Base_import_tests_models_o2m_child-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "删除测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2m_children/{base_import_tests_models_o2m_child_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_o2m_child_id") Long base_import_tests_models_o2m_child_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2m_childService.remove(base_import_tests_models_o2m_child_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2m_childService.getBaseImportTestsModelsO2mChildByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_o2m_child-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "批量删除测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2m_children/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_o2m_childService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_o2m_childMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_o2m_child-Get')")
    @ApiOperation(value = "获取测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "获取测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2m_children/{base_import_tests_models_o2m_child_id}")
    public ResponseEntity<Base_import_tests_models_o2m_childDTO> get(@PathVariable("base_import_tests_models_o2m_child_id") Long base_import_tests_models_o2m_child_id) {
        Base_import_tests_models_o2m_child domain = base_import_tests_models_o2m_childService.get(base_import_tests_models_o2m_child_id);
        Base_import_tests_models_o2m_childDTO dto = base_import_tests_models_o2m_childMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型，一对多草稿", tags = {"测试:基本导入模型，一对多" },  notes = "获取测试:基本导入模型，一对多草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2m_children/getdraft")
    public ResponseEntity<Base_import_tests_models_o2m_childDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2m_childMapping.toDto(base_import_tests_models_o2m_childService.getDraft(new Base_import_tests_models_o2m_child())));
    }

    @ApiOperation(value = "检查测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "检查测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_o2m_childDTO base_import_tests_models_o2m_childdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2m_childService.checkKey(base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2m_childMapping.toDomain(#base_import_tests_models_o2m_childdto),'iBizBusinessCentral-Base_import_tests_models_o2m_child-Save')")
    @ApiOperation(value = "保存测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "保存测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_o2m_childDTO base_import_tests_models_o2m_childdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2m_childService.save(base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_o2m_childMapping.toDomain(#base_import_tests_models_o2m_childdtos),'iBizBusinessCentral-Base_import_tests_models_o2m_child-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型，一对多", tags = {"测试:基本导入模型，一对多" },  notes = "批量保存测试:基本导入模型，一对多")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_o2m_childDTO> base_import_tests_models_o2m_childdtos) {
        base_import_tests_models_o2m_childService.saveBatch(base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_o2m_child-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_o2m_child-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型，一对多" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_o2m_children/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_o2m_childDTO>> fetchDefault(Base_import_tests_models_o2m_childSearchContext context) {
        Page<Base_import_tests_models_o2m_child> domains = base_import_tests_models_o2m_childService.searchDefault(context) ;
        List<Base_import_tests_models_o2m_childDTO> list = base_import_tests_models_o2m_childMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_o2m_child-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_o2m_child-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型，一对多" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_o2m_children/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_o2m_childDTO>> searchDefault(@RequestBody Base_import_tests_models_o2m_childSearchContext context) {
        Page<Base_import_tests_models_o2m_child> domains = base_import_tests_models_o2m_childService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_o2m_childMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


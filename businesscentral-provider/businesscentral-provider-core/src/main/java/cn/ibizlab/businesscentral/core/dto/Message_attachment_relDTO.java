package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Message_attachment_relDTO]
 */
@Data
public class Message_attachment_relDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long messageId;

    /**
     * 属性 [ATTACHMENT_ID]
     *
     */
    @JSONField(name = "attachment_id")
    @JsonProperty("attachment_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long attachmentId;


    /**
     * 设置 [MESSAGE_ID]
     */
    public void setMessageId(Long  messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }

    /**
     * 设置 [ATTACHMENT_ID]
     */
    public void setAttachmentId(Long  attachmentId){
        this.attachmentId = attachmentId ;
        this.modify("attachment_id",attachmentId);
    }


}



package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_project;
import cn.ibizlab.businesscentral.core.odoo_project.service.IProject_projectService;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_projectSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"项目" })
@RestController("Core-project_project")
@RequestMapping("")
public class Project_projectResource {

    @Autowired
    public IProject_projectService project_projectService;

    @Autowired
    @Lazy
    public Project_projectMapping project_projectMapping;

    @PreAuthorize("hasPermission(this.project_projectMapping.toDomain(#project_projectdto),'iBizBusinessCentral-Project_project-Create')")
    @ApiOperation(value = "新建项目", tags = {"项目" },  notes = "新建项目")
	@RequestMapping(method = RequestMethod.POST, value = "/project_projects")
    public ResponseEntity<Project_projectDTO> create(@Validated @RequestBody Project_projectDTO project_projectdto) {
        Project_project domain = project_projectMapping.toDomain(project_projectdto);
		project_projectService.create(domain);
        Project_projectDTO dto = project_projectMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.project_projectMapping.toDomain(#project_projectdtos),'iBizBusinessCentral-Project_project-Create')")
    @ApiOperation(value = "批量新建项目", tags = {"项目" },  notes = "批量新建项目")
	@RequestMapping(method = RequestMethod.POST, value = "/project_projects/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Project_projectDTO> project_projectdtos) {
        project_projectService.createBatch(project_projectMapping.toDomain(project_projectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "project_project" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.project_projectService.get(#project_project_id),'iBizBusinessCentral-Project_project-Update')")
    @ApiOperation(value = "更新项目", tags = {"项目" },  notes = "更新项目")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_projects/{project_project_id}")
    public ResponseEntity<Project_projectDTO> update(@PathVariable("project_project_id") Long project_project_id, @RequestBody Project_projectDTO project_projectdto) {
		Project_project domain  = project_projectMapping.toDomain(project_projectdto);
        domain .setId(project_project_id);
		project_projectService.update(domain );
		Project_projectDTO dto = project_projectMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.project_projectService.getProjectProjectByEntities(this.project_projectMapping.toDomain(#project_projectdtos)),'iBizBusinessCentral-Project_project-Update')")
    @ApiOperation(value = "批量更新项目", tags = {"项目" },  notes = "批量更新项目")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_projects/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_projectDTO> project_projectdtos) {
        project_projectService.updateBatch(project_projectMapping.toDomain(project_projectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.project_projectService.get(#project_project_id),'iBizBusinessCentral-Project_project-Remove')")
    @ApiOperation(value = "删除项目", tags = {"项目" },  notes = "删除项目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_projects/{project_project_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("project_project_id") Long project_project_id) {
         return ResponseEntity.status(HttpStatus.OK).body(project_projectService.remove(project_project_id));
    }

    @PreAuthorize("hasPermission(this.project_projectService.getProjectProjectByIds(#ids),'iBizBusinessCentral-Project_project-Remove')")
    @ApiOperation(value = "批量删除项目", tags = {"项目" },  notes = "批量删除项目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_projects/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        project_projectService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.project_projectMapping.toDomain(returnObject.body),'iBizBusinessCentral-Project_project-Get')")
    @ApiOperation(value = "获取项目", tags = {"项目" },  notes = "获取项目")
	@RequestMapping(method = RequestMethod.GET, value = "/project_projects/{project_project_id}")
    public ResponseEntity<Project_projectDTO> get(@PathVariable("project_project_id") Long project_project_id) {
        Project_project domain = project_projectService.get(project_project_id);
        Project_projectDTO dto = project_projectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取项目草稿", tags = {"项目" },  notes = "获取项目草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/project_projects/getdraft")
    public ResponseEntity<Project_projectDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(project_projectMapping.toDto(project_projectService.getDraft(new Project_project())));
    }

    @ApiOperation(value = "检查项目", tags = {"项目" },  notes = "检查项目")
	@RequestMapping(method = RequestMethod.POST, value = "/project_projects/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Project_projectDTO project_projectdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(project_projectService.checkKey(project_projectMapping.toDomain(project_projectdto)));
    }

    @PreAuthorize("hasPermission(this.project_projectMapping.toDomain(#project_projectdto),'iBizBusinessCentral-Project_project-Save')")
    @ApiOperation(value = "保存项目", tags = {"项目" },  notes = "保存项目")
	@RequestMapping(method = RequestMethod.POST, value = "/project_projects/save")
    public ResponseEntity<Boolean> save(@RequestBody Project_projectDTO project_projectdto) {
        return ResponseEntity.status(HttpStatus.OK).body(project_projectService.save(project_projectMapping.toDomain(project_projectdto)));
    }

    @PreAuthorize("hasPermission(this.project_projectMapping.toDomain(#project_projectdtos),'iBizBusinessCentral-Project_project-Save')")
    @ApiOperation(value = "批量保存项目", tags = {"项目" },  notes = "批量保存项目")
	@RequestMapping(method = RequestMethod.POST, value = "/project_projects/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Project_projectDTO> project_projectdtos) {
        project_projectService.saveBatch(project_projectMapping.toDomain(project_projectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Project_project-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Project_project-Get')")
	@ApiOperation(value = "获取数据集", tags = {"项目" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/project_projects/fetchdefault")
	public ResponseEntity<List<Project_projectDTO>> fetchDefault(Project_projectSearchContext context) {
        Page<Project_project> domains = project_projectService.searchDefault(context) ;
        List<Project_projectDTO> list = project_projectMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Project_project-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Project_project-Get')")
	@ApiOperation(value = "查询数据集", tags = {"项目" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/project_projects/searchdefault")
	public ResponseEntity<Page<Project_projectDTO>> searchDefault(@RequestBody Project_projectSearchContext context) {
        Page<Project_project> domains = project_projectService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(project_projectMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


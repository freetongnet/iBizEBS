package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_image;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_imageService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_imageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品图片" })
@RestController("Core-product_image")
@RequestMapping("")
public class Product_imageResource {

    @Autowired
    public IProduct_imageService product_imageService;

    @Autowired
    @Lazy
    public Product_imageMapping product_imageMapping;

    @PreAuthorize("hasPermission(this.product_imageMapping.toDomain(#product_imagedto),'iBizBusinessCentral-Product_image-Create')")
    @ApiOperation(value = "新建产品图片", tags = {"产品图片" },  notes = "新建产品图片")
	@RequestMapping(method = RequestMethod.POST, value = "/product_images")
    public ResponseEntity<Product_imageDTO> create(@Validated @RequestBody Product_imageDTO product_imagedto) {
        Product_image domain = product_imageMapping.toDomain(product_imagedto);
		product_imageService.create(domain);
        Product_imageDTO dto = product_imageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_imageMapping.toDomain(#product_imagedtos),'iBizBusinessCentral-Product_image-Create')")
    @ApiOperation(value = "批量新建产品图片", tags = {"产品图片" },  notes = "批量新建产品图片")
	@RequestMapping(method = RequestMethod.POST, value = "/product_images/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_imageDTO> product_imagedtos) {
        product_imageService.createBatch(product_imageMapping.toDomain(product_imagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_image" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_imageService.get(#product_image_id),'iBizBusinessCentral-Product_image-Update')")
    @ApiOperation(value = "更新产品图片", tags = {"产品图片" },  notes = "更新产品图片")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_images/{product_image_id}")
    public ResponseEntity<Product_imageDTO> update(@PathVariable("product_image_id") Long product_image_id, @RequestBody Product_imageDTO product_imagedto) {
		Product_image domain  = product_imageMapping.toDomain(product_imagedto);
        domain .setId(product_image_id);
		product_imageService.update(domain );
		Product_imageDTO dto = product_imageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_imageService.getProductImageByEntities(this.product_imageMapping.toDomain(#product_imagedtos)),'iBizBusinessCentral-Product_image-Update')")
    @ApiOperation(value = "批量更新产品图片", tags = {"产品图片" },  notes = "批量更新产品图片")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_images/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_imageDTO> product_imagedtos) {
        product_imageService.updateBatch(product_imageMapping.toDomain(product_imagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_imageService.get(#product_image_id),'iBizBusinessCentral-Product_image-Remove')")
    @ApiOperation(value = "删除产品图片", tags = {"产品图片" },  notes = "删除产品图片")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_images/{product_image_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_image_id") Long product_image_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_imageService.remove(product_image_id));
    }

    @PreAuthorize("hasPermission(this.product_imageService.getProductImageByIds(#ids),'iBizBusinessCentral-Product_image-Remove')")
    @ApiOperation(value = "批量删除产品图片", tags = {"产品图片" },  notes = "批量删除产品图片")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_images/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_imageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_imageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_image-Get')")
    @ApiOperation(value = "获取产品图片", tags = {"产品图片" },  notes = "获取产品图片")
	@RequestMapping(method = RequestMethod.GET, value = "/product_images/{product_image_id}")
    public ResponseEntity<Product_imageDTO> get(@PathVariable("product_image_id") Long product_image_id) {
        Product_image domain = product_imageService.get(product_image_id);
        Product_imageDTO dto = product_imageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品图片草稿", tags = {"产品图片" },  notes = "获取产品图片草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_images/getdraft")
    public ResponseEntity<Product_imageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_imageMapping.toDto(product_imageService.getDraft(new Product_image())));
    }

    @ApiOperation(value = "检查产品图片", tags = {"产品图片" },  notes = "检查产品图片")
	@RequestMapping(method = RequestMethod.POST, value = "/product_images/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_imageDTO product_imagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_imageService.checkKey(product_imageMapping.toDomain(product_imagedto)));
    }

    @PreAuthorize("hasPermission(this.product_imageMapping.toDomain(#product_imagedto),'iBizBusinessCentral-Product_image-Save')")
    @ApiOperation(value = "保存产品图片", tags = {"产品图片" },  notes = "保存产品图片")
	@RequestMapping(method = RequestMethod.POST, value = "/product_images/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_imageDTO product_imagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_imageService.save(product_imageMapping.toDomain(product_imagedto)));
    }

    @PreAuthorize("hasPermission(this.product_imageMapping.toDomain(#product_imagedtos),'iBizBusinessCentral-Product_image-Save')")
    @ApiOperation(value = "批量保存产品图片", tags = {"产品图片" },  notes = "批量保存产品图片")
	@RequestMapping(method = RequestMethod.POST, value = "/product_images/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_imageDTO> product_imagedtos) {
        product_imageService.saveBatch(product_imageMapping.toDomain(product_imagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_image-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_image-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品图片" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_images/fetchdefault")
	public ResponseEntity<List<Product_imageDTO>> fetchDefault(Product_imageSearchContext context) {
        Page<Product_image> domains = product_imageService.searchDefault(context) ;
        List<Product_imageDTO> list = product_imageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_image-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_image-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品图片" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_images/searchdefault")
	public ResponseEntity<Page<Product_imageDTO>> searchDefault(@RequestBody Product_imageSearchContext context) {
        Page<Product_image> domains = product_imageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_imageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment_category;
import cn.ibizlab.businesscentral.core.dto.Maintenance_equipment_categoryDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMaintenance_equipment_categoryMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Maintenance_equipment_categoryMapping extends MappingBase<Maintenance_equipment_categoryDTO, Maintenance_equipment_category> {


}


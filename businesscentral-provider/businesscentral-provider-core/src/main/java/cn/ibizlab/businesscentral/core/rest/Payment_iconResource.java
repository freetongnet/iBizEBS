package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_iconService;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_iconSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"支付图标" })
@RestController("Core-payment_icon")
@RequestMapping("")
public class Payment_iconResource {

    @Autowired
    public IPayment_iconService payment_iconService;

    @Autowired
    @Lazy
    public Payment_iconMapping payment_iconMapping;

    @PreAuthorize("hasPermission(this.payment_iconMapping.toDomain(#payment_icondto),'iBizBusinessCentral-Payment_icon-Create')")
    @ApiOperation(value = "新建支付图标", tags = {"支付图标" },  notes = "新建支付图标")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_icons")
    public ResponseEntity<Payment_iconDTO> create(@Validated @RequestBody Payment_iconDTO payment_icondto) {
        Payment_icon domain = payment_iconMapping.toDomain(payment_icondto);
		payment_iconService.create(domain);
        Payment_iconDTO dto = payment_iconMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_iconMapping.toDomain(#payment_icondtos),'iBizBusinessCentral-Payment_icon-Create')")
    @ApiOperation(value = "批量新建支付图标", tags = {"支付图标" },  notes = "批量新建支付图标")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_icons/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_iconDTO> payment_icondtos) {
        payment_iconService.createBatch(payment_iconMapping.toDomain(payment_icondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "payment_icon" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.payment_iconService.get(#payment_icon_id),'iBizBusinessCentral-Payment_icon-Update')")
    @ApiOperation(value = "更新支付图标", tags = {"支付图标" },  notes = "更新支付图标")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_icons/{payment_icon_id}")
    public ResponseEntity<Payment_iconDTO> update(@PathVariable("payment_icon_id") Long payment_icon_id, @RequestBody Payment_iconDTO payment_icondto) {
		Payment_icon domain  = payment_iconMapping.toDomain(payment_icondto);
        domain .setId(payment_icon_id);
		payment_iconService.update(domain );
		Payment_iconDTO dto = payment_iconMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_iconService.getPaymentIconByEntities(this.payment_iconMapping.toDomain(#payment_icondtos)),'iBizBusinessCentral-Payment_icon-Update')")
    @ApiOperation(value = "批量更新支付图标", tags = {"支付图标" },  notes = "批量更新支付图标")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_icons/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_iconDTO> payment_icondtos) {
        payment_iconService.updateBatch(payment_iconMapping.toDomain(payment_icondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.payment_iconService.get(#payment_icon_id),'iBizBusinessCentral-Payment_icon-Remove')")
    @ApiOperation(value = "删除支付图标", tags = {"支付图标" },  notes = "删除支付图标")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_icons/{payment_icon_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("payment_icon_id") Long payment_icon_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_iconService.remove(payment_icon_id));
    }

    @PreAuthorize("hasPermission(this.payment_iconService.getPaymentIconByIds(#ids),'iBizBusinessCentral-Payment_icon-Remove')")
    @ApiOperation(value = "批量删除支付图标", tags = {"支付图标" },  notes = "批量删除支付图标")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_icons/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        payment_iconService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.payment_iconMapping.toDomain(returnObject.body),'iBizBusinessCentral-Payment_icon-Get')")
    @ApiOperation(value = "获取支付图标", tags = {"支付图标" },  notes = "获取支付图标")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_icons/{payment_icon_id}")
    public ResponseEntity<Payment_iconDTO> get(@PathVariable("payment_icon_id") Long payment_icon_id) {
        Payment_icon domain = payment_iconService.get(payment_icon_id);
        Payment_iconDTO dto = payment_iconMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取支付图标草稿", tags = {"支付图标" },  notes = "获取支付图标草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_icons/getdraft")
    public ResponseEntity<Payment_iconDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(payment_iconMapping.toDto(payment_iconService.getDraft(new Payment_icon())));
    }

    @ApiOperation(value = "检查支付图标", tags = {"支付图标" },  notes = "检查支付图标")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_icons/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Payment_iconDTO payment_icondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(payment_iconService.checkKey(payment_iconMapping.toDomain(payment_icondto)));
    }

    @PreAuthorize("hasPermission(this.payment_iconMapping.toDomain(#payment_icondto),'iBizBusinessCentral-Payment_icon-Save')")
    @ApiOperation(value = "保存支付图标", tags = {"支付图标" },  notes = "保存支付图标")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_icons/save")
    public ResponseEntity<Boolean> save(@RequestBody Payment_iconDTO payment_icondto) {
        return ResponseEntity.status(HttpStatus.OK).body(payment_iconService.save(payment_iconMapping.toDomain(payment_icondto)));
    }

    @PreAuthorize("hasPermission(this.payment_iconMapping.toDomain(#payment_icondtos),'iBizBusinessCentral-Payment_icon-Save')")
    @ApiOperation(value = "批量保存支付图标", tags = {"支付图标" },  notes = "批量保存支付图标")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_icons/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Payment_iconDTO> payment_icondtos) {
        payment_iconService.saveBatch(payment_iconMapping.toDomain(payment_icondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_icon-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_icon-Get')")
	@ApiOperation(value = "获取数据集", tags = {"支付图标" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/payment_icons/fetchdefault")
	public ResponseEntity<List<Payment_iconDTO>> fetchDefault(Payment_iconSearchContext context) {
        Page<Payment_icon> domains = payment_iconService.searchDefault(context) ;
        List<Payment_iconDTO> list = payment_iconMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_icon-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_icon-Get')")
	@ApiOperation(value = "查询数据集", tags = {"支付图标" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/payment_icons/searchdefault")
	public ResponseEntity<Page<Payment_iconDTO>> searchDefault(@RequestBody Payment_iconSearchContext context) {
        Page<Payment_icon> domains = payment_iconService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_iconMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_import;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_importService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_importSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"基础导入" })
@RestController("Core-base_import_import")
@RequestMapping("")
public class Base_import_importResource {

    @Autowired
    public IBase_import_importService base_import_importService;

    @Autowired
    @Lazy
    public Base_import_importMapping base_import_importMapping;

    @PreAuthorize("hasPermission(this.base_import_importMapping.toDomain(#base_import_importdto),'iBizBusinessCentral-Base_import_import-Create')")
    @ApiOperation(value = "新建基础导入", tags = {"基础导入" },  notes = "新建基础导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_imports")
    public ResponseEntity<Base_import_importDTO> create(@Validated @RequestBody Base_import_importDTO base_import_importdto) {
        Base_import_import domain = base_import_importMapping.toDomain(base_import_importdto);
		base_import_importService.create(domain);
        Base_import_importDTO dto = base_import_importMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_importMapping.toDomain(#base_import_importdtos),'iBizBusinessCentral-Base_import_import-Create')")
    @ApiOperation(value = "批量新建基础导入", tags = {"基础导入" },  notes = "批量新建基础导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_importDTO> base_import_importdtos) {
        base_import_importService.createBatch(base_import_importMapping.toDomain(base_import_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_import" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_importService.get(#base_import_import_id),'iBizBusinessCentral-Base_import_import-Update')")
    @ApiOperation(value = "更新基础导入", tags = {"基础导入" },  notes = "更新基础导入")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_imports/{base_import_import_id}")
    public ResponseEntity<Base_import_importDTO> update(@PathVariable("base_import_import_id") Long base_import_import_id, @RequestBody Base_import_importDTO base_import_importdto) {
		Base_import_import domain  = base_import_importMapping.toDomain(base_import_importdto);
        domain .setId(base_import_import_id);
		base_import_importService.update(domain );
		Base_import_importDTO dto = base_import_importMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_importService.getBaseImportImportByEntities(this.base_import_importMapping.toDomain(#base_import_importdtos)),'iBizBusinessCentral-Base_import_import-Update')")
    @ApiOperation(value = "批量更新基础导入", tags = {"基础导入" },  notes = "批量更新基础导入")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_imports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_importDTO> base_import_importdtos) {
        base_import_importService.updateBatch(base_import_importMapping.toDomain(base_import_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_importService.get(#base_import_import_id),'iBizBusinessCentral-Base_import_import-Remove')")
    @ApiOperation(value = "删除基础导入", tags = {"基础导入" },  notes = "删除基础导入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_imports/{base_import_import_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_import_id") Long base_import_import_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_importService.remove(base_import_import_id));
    }

    @PreAuthorize("hasPermission(this.base_import_importService.getBaseImportImportByIds(#ids),'iBizBusinessCentral-Base_import_import-Remove')")
    @ApiOperation(value = "批量删除基础导入", tags = {"基础导入" },  notes = "批量删除基础导入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_imports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_importService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_importMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_import-Get')")
    @ApiOperation(value = "获取基础导入", tags = {"基础导入" },  notes = "获取基础导入")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_imports/{base_import_import_id}")
    public ResponseEntity<Base_import_importDTO> get(@PathVariable("base_import_import_id") Long base_import_import_id) {
        Base_import_import domain = base_import_importService.get(base_import_import_id);
        Base_import_importDTO dto = base_import_importMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取基础导入草稿", tags = {"基础导入" },  notes = "获取基础导入草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_imports/getdraft")
    public ResponseEntity<Base_import_importDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_importMapping.toDto(base_import_importService.getDraft(new Base_import_import())));
    }

    @ApiOperation(value = "检查基础导入", tags = {"基础导入" },  notes = "检查基础导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_importDTO base_import_importdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_importService.checkKey(base_import_importMapping.toDomain(base_import_importdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_importMapping.toDomain(#base_import_importdto),'iBizBusinessCentral-Base_import_import-Save')")
    @ApiOperation(value = "保存基础导入", tags = {"基础导入" },  notes = "保存基础导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_importDTO base_import_importdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_importService.save(base_import_importMapping.toDomain(base_import_importdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_importMapping.toDomain(#base_import_importdtos),'iBizBusinessCentral-Base_import_import-Save')")
    @ApiOperation(value = "批量保存基础导入", tags = {"基础导入" },  notes = "批量保存基础导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_importDTO> base_import_importdtos) {
        base_import_importService.saveBatch(base_import_importMapping.toDomain(base_import_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_import-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_import-Get')")
	@ApiOperation(value = "获取数据集", tags = {"基础导入" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_imports/fetchdefault")
	public ResponseEntity<List<Base_import_importDTO>> fetchDefault(Base_import_importSearchContext context) {
        Page<Base_import_import> domains = base_import_importService.searchDefault(context) ;
        List<Base_import_importDTO> list = base_import_importMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_import-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_import-Get')")
	@ApiOperation(value = "查询数据集", tags = {"基础导入" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_imports/searchdefault")
	public ResponseEntity<Page<Base_import_importDTO>> searchDefault(@RequestBody Base_import_importSearchContext context) {
        Page<Base_import_import> domains = base_import_importService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_importMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


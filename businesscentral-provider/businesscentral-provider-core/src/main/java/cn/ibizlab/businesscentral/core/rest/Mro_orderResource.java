package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_order;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_orderService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_orderSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Maintenance Order" })
@RestController("Core-mro_order")
@RequestMapping("")
public class Mro_orderResource {

    @Autowired
    public IMro_orderService mro_orderService;

    @Autowired
    @Lazy
    public Mro_orderMapping mro_orderMapping;

    @PreAuthorize("hasPermission(this.mro_orderMapping.toDomain(#mro_orderdto),'iBizBusinessCentral-Mro_order-Create')")
    @ApiOperation(value = "新建Maintenance Order", tags = {"Maintenance Order" },  notes = "新建Maintenance Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_orders")
    public ResponseEntity<Mro_orderDTO> create(@Validated @RequestBody Mro_orderDTO mro_orderdto) {
        Mro_order domain = mro_orderMapping.toDomain(mro_orderdto);
		mro_orderService.create(domain);
        Mro_orderDTO dto = mro_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_orderMapping.toDomain(#mro_orderdtos),'iBizBusinessCentral-Mro_order-Create')")
    @ApiOperation(value = "批量新建Maintenance Order", tags = {"Maintenance Order" },  notes = "批量新建Maintenance Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_orderDTO> mro_orderdtos) {
        mro_orderService.createBatch(mro_orderMapping.toDomain(mro_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_order" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_orderService.get(#mro_order_id),'iBizBusinessCentral-Mro_order-Update')")
    @ApiOperation(value = "更新Maintenance Order", tags = {"Maintenance Order" },  notes = "更新Maintenance Order")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_orders/{mro_order_id}")
    public ResponseEntity<Mro_orderDTO> update(@PathVariable("mro_order_id") Long mro_order_id, @RequestBody Mro_orderDTO mro_orderdto) {
		Mro_order domain  = mro_orderMapping.toDomain(mro_orderdto);
        domain .setId(mro_order_id);
		mro_orderService.update(domain );
		Mro_orderDTO dto = mro_orderMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_orderService.getMroOrderByEntities(this.mro_orderMapping.toDomain(#mro_orderdtos)),'iBizBusinessCentral-Mro_order-Update')")
    @ApiOperation(value = "批量更新Maintenance Order", tags = {"Maintenance Order" },  notes = "批量更新Maintenance Order")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_orderDTO> mro_orderdtos) {
        mro_orderService.updateBatch(mro_orderMapping.toDomain(mro_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_orderService.get(#mro_order_id),'iBizBusinessCentral-Mro_order-Remove')")
    @ApiOperation(value = "删除Maintenance Order", tags = {"Maintenance Order" },  notes = "删除Maintenance Order")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_orders/{mro_order_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_order_id") Long mro_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_orderService.remove(mro_order_id));
    }

    @PreAuthorize("hasPermission(this.mro_orderService.getMroOrderByIds(#ids),'iBizBusinessCentral-Mro_order-Remove')")
    @ApiOperation(value = "批量删除Maintenance Order", tags = {"Maintenance Order" },  notes = "批量删除Maintenance Order")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_orderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_order-Get')")
    @ApiOperation(value = "获取Maintenance Order", tags = {"Maintenance Order" },  notes = "获取Maintenance Order")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_orders/{mro_order_id}")
    public ResponseEntity<Mro_orderDTO> get(@PathVariable("mro_order_id") Long mro_order_id) {
        Mro_order domain = mro_orderService.get(mro_order_id);
        Mro_orderDTO dto = mro_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Maintenance Order草稿", tags = {"Maintenance Order" },  notes = "获取Maintenance Order草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_orders/getdraft")
    public ResponseEntity<Mro_orderDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_orderMapping.toDto(mro_orderService.getDraft(new Mro_order())));
    }

    @ApiOperation(value = "检查Maintenance Order", tags = {"Maintenance Order" },  notes = "检查Maintenance Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_orders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_orderDTO mro_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_orderService.checkKey(mro_orderMapping.toDomain(mro_orderdto)));
    }

    @PreAuthorize("hasPermission(this.mro_orderMapping.toDomain(#mro_orderdto),'iBizBusinessCentral-Mro_order-Save')")
    @ApiOperation(value = "保存Maintenance Order", tags = {"Maintenance Order" },  notes = "保存Maintenance Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_orders/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_orderDTO mro_orderdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_orderService.save(mro_orderMapping.toDomain(mro_orderdto)));
    }

    @PreAuthorize("hasPermission(this.mro_orderMapping.toDomain(#mro_orderdtos),'iBizBusinessCentral-Mro_order-Save')")
    @ApiOperation(value = "批量保存Maintenance Order", tags = {"Maintenance Order" },  notes = "批量保存Maintenance Order")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_orders/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_orderDTO> mro_orderdtos) {
        mro_orderService.saveBatch(mro_orderMapping.toDomain(mro_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_order-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Maintenance Order" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_orders/fetchdefault")
	public ResponseEntity<List<Mro_orderDTO>> fetchDefault(Mro_orderSearchContext context) {
        Page<Mro_order> domains = mro_orderService.searchDefault(context) ;
        List<Mro_orderDTO> list = mro_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_order-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Maintenance Order" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_orders/searchdefault")
	public ResponseEntity<Page<Mro_orderDTO>> searchDefault(@RequestBody Mro_orderSearchContext context) {
        Page<Mro_order> domains = mro_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


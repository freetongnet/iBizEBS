package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char_required;
import cn.ibizlab.businesscentral.core.dto.Base_import_tests_models_char_requiredDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreBase_import_tests_models_char_requiredMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_import_tests_models_char_requiredMapping extends MappingBase<Base_import_tests_models_char_requiredDTO, Base_import_tests_models_char_required> {


}


package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Purchase_requisitionDTO]
 */
@Data
public class Purchase_requisitionDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[申请编号]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String origin;

    /**
     * 属性 [DATE_END]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_end")
    private Timestamp dateEnd;

    /**
     * 属性 [SCHEDULE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedule_date" , format="yyyy-MM-dd")
    @JsonProperty("schedule_date")
    private Timestamp scheduleDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [ORDERING_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "ordering_date" , format="yyyy-MM-dd")
    @JsonProperty("ordering_date")
    private Timestamp orderingDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String state;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ORDER_COUNT]
     *
     */
    @JSONField(name = "order_count")
    @JsonProperty("order_count")
    private Integer orderCount;

    /**
     * 属性 [CURRENCY_NAME]
     *
     */
    @JSONField(name = "currency_name")
    @JsonProperty("currency_name")
    @Size(min = 0, max = 3, message = "内容长度必须小于等于[3]")
    private String currencyName;

    /**
     * 属性 [CREATE_UNAME]
     *
     */
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String createUname;

    /**
     * 属性 [QUANTITY_COPY]
     *
     */
    @JSONField(name = "quantity_copy")
    @JsonProperty("quantity_copy")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    private String quantityCopy;

    /**
     * 属性 [COMPANY_NAME]
     *
     */
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String companyName;

    /**
     * 属性 [USER_NAME]
     *
     */
    @JSONField(name = "user_name")
    @JsonProperty("user_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String userName;

    /**
     * 属性 [TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "type_id_text")
    @JsonProperty("type_id_text")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String typeIdText;

    /**
     * 属性 [VENDOR_ID_TEXT]
     *
     */
    @JSONField(name = "vendor_id_text")
    @JsonProperty("vendor_id_text")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String vendorIdText;

    /**
     * 属性 [WRITE_UNAME]
     *
     */
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String writeUname;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pickingTypeId;

    /**
     * 属性 [VENDOR_ID]
     *
     */
    @JSONField(name = "vendor_id")
    @JsonProperty("vendor_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long vendorId;

    /**
     * 属性 [TYPE_ID]
     *
     */
    @JSONField(name = "type_id")
    @JsonProperty("type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long typeId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long warehouseId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [DATE_END]
     */
    public void setDateEnd(Timestamp  dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }

    /**
     * 设置 [SCHEDULE_DATE]
     */
    public void setScheduleDate(Timestamp  scheduleDate){
        this.scheduleDate = scheduleDate ;
        this.modify("schedule_date",scheduleDate);
    }

    /**
     * 设置 [CREATE_DATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 设置 [ORDERING_DATE]
     */
    public void setOrderingDate(Timestamp  orderingDate){
        this.orderingDate = orderingDate ;
        this.modify("ordering_date",orderingDate);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [WRITE_DATE]
     */
    public void setWriteDate(Timestamp  writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    public void setPickingTypeId(Long  pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }

    /**
     * 设置 [VENDOR_ID]
     */
    public void setVendorId(Long  vendorId){
        this.vendorId = vendorId ;
        this.modify("vendor_id",vendorId);
    }

    /**
     * 设置 [TYPE_ID]
     */
    public void setTypeId(Long  typeId){
        this.typeId = typeId ;
        this.modify("type_id",typeId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [CREATE_UID]
     */
    public void setCreateUid(Long  createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }

    /**
     * 设置 [WRITE_UID]
     */
    public void setWriteUid(Long  writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    public void setWarehouseId(Long  warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }


}



package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Fleet_vehicleDTO]
 */
@Data
public class Fleet_vehicleDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [FUEL_TYPE]
     *
     */
    @JSONField(name = "fuel_type")
    @JsonProperty("fuel_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String fuelType;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String tagIds;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [POWER]
     *
     */
    @JSONField(name = "power")
    @JsonProperty("power")
    private Integer power;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [RESIDUAL_VALUE]
     *
     */
    @JSONField(name = "residual_value")
    @JsonProperty("residual_value")
    private Double residualValue;

    /**
     * 属性 [ODOMETER]
     *
     */
    @JSONField(name = "odometer")
    @JsonProperty("odometer")
    private Double odometer;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [FUEL_LOGS_COUNT]
     *
     */
    @JSONField(name = "fuel_logs_count")
    @JsonProperty("fuel_logs_count")
    private Integer fuelLogsCount;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [MODEL_YEAR]
     *
     */
    @JSONField(name = "model_year")
    @JsonProperty("model_year")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String modelYear;

    /**
     * 属性 [CAR_VALUE]
     *
     */
    @JSONField(name = "car_value")
    @JsonProperty("car_value")
    private Double carValue;

    /**
     * 属性 [LICENSE_PLATE]
     *
     */
    @JSONField(name = "license_plate")
    @JsonProperty("license_plate")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String licensePlate;

    /**
     * 属性 [CONTRACT_RENEWAL_OVERDUE]
     *
     */
    @JSONField(name = "contract_renewal_overdue")
    @JsonProperty("contract_renewal_overdue")
    private Boolean contractRenewalOverdue;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [ACQUISITION_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "acquisition_date" , format="yyyy-MM-dd")
    @JsonProperty("acquisition_date")
    private Timestamp acquisitionDate;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [HORSEPOWER_TAX]
     *
     */
    @JSONField(name = "horsepower_tax")
    @JsonProperty("horsepower_tax")
    private Double horsepowerTax;

    /**
     * 属性 [LOCATION]
     *
     */
    @JSONField(name = "location")
    @JsonProperty("location")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String location;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [HORSEPOWER]
     *
     */
    @JSONField(name = "horsepower")
    @JsonProperty("horsepower")
    private Integer horsepower;

    /**
     * 属性 [VIN_SN]
     *
     */
    @JSONField(name = "vin_sn")
    @JsonProperty("vin_sn")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String vinSn;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [ODOMETER_COUNT]
     *
     */
    @JSONField(name = "odometer_count")
    @JsonProperty("odometer_count")
    private Integer odometerCount;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [CONTRACT_RENEWAL_NAME]
     *
     */
    @JSONField(name = "contract_renewal_name")
    @JsonProperty("contract_renewal_name")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String contractRenewalName;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [SEATS]
     *
     */
    @JSONField(name = "seats")
    @JsonProperty("seats")
    private Integer seats;

    /**
     * 属性 [LOG_FUEL]
     *
     */
    @JSONField(name = "log_fuel")
    @JsonProperty("log_fuel")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String logFuel;

    /**
     * 属性 [COST_COUNT]
     *
     */
    @JSONField(name = "cost_count")
    @JsonProperty("cost_count")
    private Integer costCount;

    /**
     * 属性 [LOG_DRIVERS]
     *
     */
    @JSONField(name = "log_drivers")
    @JsonProperty("log_drivers")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String logDrivers;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CONTRACT_RENEWAL_DUE_SOON]
     *
     */
    @JSONField(name = "contract_renewal_due_soon")
    @JsonProperty("contract_renewal_due_soon")
    private Boolean contractRenewalDueSoon;

    /**
     * 属性 [ODOMETER_UNIT]
     *
     */
    @JSONField(name = "odometer_unit")
    @JsonProperty("odometer_unit")
    @NotBlank(message = "[里程表单位]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String odometerUnit;

    /**
     * 属性 [CO2]
     *
     */
    @JSONField(name = "co2")
    @JsonProperty("co2")
    private Double co2;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [LOG_CONTRACTS]
     *
     */
    @JSONField(name = "log_contracts")
    @JsonProperty("log_contracts")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String logContracts;

    /**
     * 属性 [FIRST_CONTRACT_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "first_contract_date" , format="yyyy-MM-dd")
    @JsonProperty("first_contract_date")
    private Timestamp firstContractDate;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String color;

    /**
     * 属性 [DOORS]
     *
     */
    @JSONField(name = "doors")
    @JsonProperty("doors")
    private Integer doors;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [LOG_SERVICES]
     *
     */
    @JSONField(name = "log_services")
    @JsonProperty("log_services")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String logServices;

    /**
     * 属性 [CONTRACT_COUNT]
     *
     */
    @JSONField(name = "contract_count")
    @JsonProperty("contract_count")
    private Integer contractCount;

    /**
     * 属性 [TRANSMISSION]
     *
     */
    @JSONField(name = "transmission")
    @JsonProperty("transmission")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String transmission;

    /**
     * 属性 [SERVICE_COUNT]
     *
     */
    @JSONField(name = "service_count")
    @JsonProperty("service_count")
    private Integer serviceCount;

    /**
     * 属性 [CONTRACT_RENEWAL_TOTAL]
     *
     */
    @JSONField(name = "contract_renewal_total")
    @JsonProperty("contract_renewal_total")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String contractRenewalTotal;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [DRIVER_ID_TEXT]
     *
     */
    @JSONField(name = "driver_id_text")
    @JsonProperty("driver_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String driverIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [MODEL_ID_TEXT]
     *
     */
    @JSONField(name = "model_id_text")
    @JsonProperty("model_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String modelIdText;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [BRAND_ID_TEXT]
     *
     */
    @JSONField(name = "brand_id_text")
    @JsonProperty("brand_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String brandIdText;

    /**
     * 属性 [STATE_ID_TEXT]
     *
     */
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stateIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [MODEL_ID]
     *
     */
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[型号]不允许为空!")
    private Long modelId;

    /**
     * 属性 [BRAND_ID]
     *
     */
    @JSONField(name = "brand_id")
    @JsonProperty("brand_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long brandId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [STATE_ID]
     *
     */
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stateId;

    /**
     * 属性 [DRIVER_ID]
     *
     */
    @JSONField(name = "driver_id")
    @JsonProperty("driver_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long driverId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [FUEL_TYPE]
     */
    public void setFuelType(String  fuelType){
        this.fuelType = fuelType ;
        this.modify("fuel_type",fuelType);
    }

    /**
     * 设置 [POWER]
     */
    public void setPower(Integer  power){
        this.power = power ;
        this.modify("power",power);
    }

    /**
     * 设置 [RESIDUAL_VALUE]
     */
    public void setResidualValue(Double  residualValue){
        this.residualValue = residualValue ;
        this.modify("residual_value",residualValue);
    }

    /**
     * 设置 [MODEL_YEAR]
     */
    public void setModelYear(String  modelYear){
        this.modelYear = modelYear ;
        this.modify("model_year",modelYear);
    }

    /**
     * 设置 [CAR_VALUE]
     */
    public void setCarValue(Double  carValue){
        this.carValue = carValue ;
        this.modify("car_value",carValue);
    }

    /**
     * 设置 [LICENSE_PLATE]
     */
    public void setLicensePlate(String  licensePlate){
        this.licensePlate = licensePlate ;
        this.modify("license_plate",licensePlate);
    }

    /**
     * 设置 [ACQUISITION_DATE]
     */
    public void setAcquisitionDate(Timestamp  acquisitionDate){
        this.acquisitionDate = acquisitionDate ;
        this.modify("acquisition_date",acquisitionDate);
    }

    /**
     * 设置 [HORSEPOWER_TAX]
     */
    public void setHorsepowerTax(Double  horsepowerTax){
        this.horsepowerTax = horsepowerTax ;
        this.modify("horsepower_tax",horsepowerTax);
    }

    /**
     * 设置 [LOCATION]
     */
    public void setLocation(String  location){
        this.location = location ;
        this.modify("location",location);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [HORSEPOWER]
     */
    public void setHorsepower(Integer  horsepower){
        this.horsepower = horsepower ;
        this.modify("horsepower",horsepower);
    }

    /**
     * 设置 [VIN_SN]
     */
    public void setVinSn(String  vinSn){
        this.vinSn = vinSn ;
        this.modify("vin_sn",vinSn);
    }

    /**
     * 设置 [SEATS]
     */
    public void setSeats(Integer  seats){
        this.seats = seats ;
        this.modify("seats",seats);
    }

    /**
     * 设置 [ODOMETER_UNIT]
     */
    public void setOdometerUnit(String  odometerUnit){
        this.odometerUnit = odometerUnit ;
        this.modify("odometer_unit",odometerUnit);
    }

    /**
     * 设置 [CO2]
     */
    public void setCo2(Double  co2){
        this.co2 = co2 ;
        this.modify("co2",co2);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [FIRST_CONTRACT_DATE]
     */
    public void setFirstContractDate(Timestamp  firstContractDate){
        this.firstContractDate = firstContractDate ;
        this.modify("first_contract_date",firstContractDate);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(String  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [DOORS]
     */
    public void setDoors(Integer  doors){
        this.doors = doors ;
        this.modify("doors",doors);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [TRANSMISSION]
     */
    public void setTransmission(String  transmission){
        this.transmission = transmission ;
        this.modify("transmission",transmission);
    }

    /**
     * 设置 [MODEL_ID]
     */
    public void setModelId(Long  modelId){
        this.modelId = modelId ;
        this.modify("model_id",modelId);
    }

    /**
     * 设置 [BRAND_ID]
     */
    public void setBrandId(Long  brandId){
        this.brandId = brandId ;
        this.modify("brand_id",brandId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [STATE_ID]
     */
    public void setStateId(Long  stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }

    /**
     * 设置 [DRIVER_ID]
     */
    public void setDriverId(Long  driverId){
        this.driverId = driverId ;
        this.modify("driver_id",driverId);
    }


}



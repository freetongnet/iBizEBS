package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_departmentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"HR 部门" })
@RestController("Core-hr_department")
@RequestMapping("")
public class Hr_departmentResource {

    @Autowired
    public IHr_departmentService hr_departmentService;

    @Autowired
    @Lazy
    public Hr_departmentMapping hr_departmentMapping;

    @PreAuthorize("hasPermission(this.hr_departmentMapping.toDomain(#hr_departmentdto),'iBizBusinessCentral-Hr_department-Create')")
    @ApiOperation(value = "新建HR 部门", tags = {"HR 部门" },  notes = "新建HR 部门")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_departments")
    public ResponseEntity<Hr_departmentDTO> create(@Validated @RequestBody Hr_departmentDTO hr_departmentdto) {
        Hr_department domain = hr_departmentMapping.toDomain(hr_departmentdto);
		hr_departmentService.create(domain);
        Hr_departmentDTO dto = hr_departmentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_departmentMapping.toDomain(#hr_departmentdtos),'iBizBusinessCentral-Hr_department-Create')")
    @ApiOperation(value = "批量新建HR 部门", tags = {"HR 部门" },  notes = "批量新建HR 部门")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_departments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_departmentDTO> hr_departmentdtos) {
        hr_departmentService.createBatch(hr_departmentMapping.toDomain(hr_departmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_department" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_departmentService.get(#hr_department_id),'iBizBusinessCentral-Hr_department-Update')")
    @ApiOperation(value = "更新HR 部门", tags = {"HR 部门" },  notes = "更新HR 部门")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_departments/{hr_department_id}")
    public ResponseEntity<Hr_departmentDTO> update(@PathVariable("hr_department_id") Long hr_department_id, @RequestBody Hr_departmentDTO hr_departmentdto) {
		Hr_department domain  = hr_departmentMapping.toDomain(hr_departmentdto);
        domain .setId(hr_department_id);
		hr_departmentService.update(domain );
		Hr_departmentDTO dto = hr_departmentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_departmentService.getHrDepartmentByEntities(this.hr_departmentMapping.toDomain(#hr_departmentdtos)),'iBizBusinessCentral-Hr_department-Update')")
    @ApiOperation(value = "批量更新HR 部门", tags = {"HR 部门" },  notes = "批量更新HR 部门")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_departments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_departmentDTO> hr_departmentdtos) {
        hr_departmentService.updateBatch(hr_departmentMapping.toDomain(hr_departmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_departmentService.get(#hr_department_id),'iBizBusinessCentral-Hr_department-Remove')")
    @ApiOperation(value = "删除HR 部门", tags = {"HR 部门" },  notes = "删除HR 部门")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_departments/{hr_department_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_department_id") Long hr_department_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_departmentService.remove(hr_department_id));
    }

    @PreAuthorize("hasPermission(this.hr_departmentService.getHrDepartmentByIds(#ids),'iBizBusinessCentral-Hr_department-Remove')")
    @ApiOperation(value = "批量删除HR 部门", tags = {"HR 部门" },  notes = "批量删除HR 部门")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_departments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_departmentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_departmentMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_department-Get')")
    @ApiOperation(value = "获取HR 部门", tags = {"HR 部门" },  notes = "获取HR 部门")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_departments/{hr_department_id}")
    public ResponseEntity<Hr_departmentDTO> get(@PathVariable("hr_department_id") Long hr_department_id) {
        Hr_department domain = hr_departmentService.get(hr_department_id);
        Hr_departmentDTO dto = hr_departmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取HR 部门草稿", tags = {"HR 部门" },  notes = "获取HR 部门草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_departments/getdraft")
    public ResponseEntity<Hr_departmentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_departmentMapping.toDto(hr_departmentService.getDraft(new Hr_department())));
    }

    @ApiOperation(value = "检查HR 部门", tags = {"HR 部门" },  notes = "检查HR 部门")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_departments/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_departmentDTO hr_departmentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_departmentService.checkKey(hr_departmentMapping.toDomain(hr_departmentdto)));
    }

    @PreAuthorize("hasPermission(this.hr_departmentMapping.toDomain(#hr_departmentdto),'iBizBusinessCentral-Hr_department-Save')")
    @ApiOperation(value = "保存HR 部门", tags = {"HR 部门" },  notes = "保存HR 部门")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_departments/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_departmentDTO hr_departmentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_departmentService.save(hr_departmentMapping.toDomain(hr_departmentdto)));
    }

    @PreAuthorize("hasPermission(this.hr_departmentMapping.toDomain(#hr_departmentdtos),'iBizBusinessCentral-Hr_department-Save')")
    @ApiOperation(value = "批量保存HR 部门", tags = {"HR 部门" },  notes = "批量保存HR 部门")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_departments/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_departmentDTO> hr_departmentdtos) {
        hr_departmentService.saveBatch(hr_departmentMapping.toDomain(hr_departmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_department-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_department-Get')")
	@ApiOperation(value = "获取数据集", tags = {"HR 部门" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_departments/fetchdefault")
	public ResponseEntity<List<Hr_departmentDTO>> fetchDefault(Hr_departmentSearchContext context) {
        Page<Hr_department> domains = hr_departmentService.searchDefault(context) ;
        List<Hr_departmentDTO> list = hr_departmentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_department-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_department-Get')")
	@ApiOperation(value = "查询数据集", tags = {"HR 部门" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_departments/searchdefault")
	public ResponseEntity<Page<Hr_departmentDTO>> searchDefault(@RequestBody Hr_departmentSearchContext context) {
        Page<Hr_department> domains = hr_departmentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_departmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_department-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Hr_department-Get')")
	@ApiOperation(value = "获取首选表格", tags = {"HR 部门" } ,notes = "获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/hr_departments/fetchmaster")
	public ResponseEntity<List<Hr_departmentDTO>> fetchMaster(Hr_departmentSearchContext context) {
        Page<Hr_department> domains = hr_departmentService.searchMaster(context) ;
        List<Hr_departmentDTO> list = hr_departmentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_department-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Hr_department-Get')")
	@ApiOperation(value = "查询首选表格", tags = {"HR 部门" } ,notes = "查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/hr_departments/searchmaster")
	public ResponseEntity<Page<Hr_departmentDTO>> searchMaster(@RequestBody Hr_departmentSearchContext context) {
        Page<Hr_department> domains = hr_departmentService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_departmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_department-searchROOT-all') and hasPermission(#context,'iBizBusinessCentral-Hr_department-Get')")
	@ApiOperation(value = "获取根节点部门查询", tags = {"HR 部门" } ,notes = "获取根节点部门查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_departments/fetchroot")
	public ResponseEntity<List<Hr_departmentDTO>> fetchROOT(Hr_departmentSearchContext context) {
        Page<Hr_department> domains = hr_departmentService.searchROOT(context) ;
        List<Hr_departmentDTO> list = hr_departmentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_department-searchROOT-all') and hasPermission(#context,'iBizBusinessCentral-Hr_department-Get')")
	@ApiOperation(value = "查询根节点部门查询", tags = {"HR 部门" } ,notes = "查询根节点部门查询")
    @RequestMapping(method= RequestMethod.POST , value="/hr_departments/searchroot")
	public ResponseEntity<Page<Hr_departmentDTO>> searchROOT(@RequestBody Hr_departmentSearchContext context) {
        Page<Hr_department> domains = hr_departmentService.searchROOT(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_departmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


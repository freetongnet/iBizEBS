package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order;
import cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_orderSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"维修单" })
@RestController("Core-repair_order")
@RequestMapping("")
public class Repair_orderResource {

    @Autowired
    public IRepair_orderService repair_orderService;

    @Autowired
    @Lazy
    public Repair_orderMapping repair_orderMapping;

    @PreAuthorize("hasPermission(this.repair_orderMapping.toDomain(#repair_orderdto),'iBizBusinessCentral-Repair_order-Create')")
    @ApiOperation(value = "新建维修单", tags = {"维修单" },  notes = "新建维修单")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_orders")
    public ResponseEntity<Repair_orderDTO> create(@Validated @RequestBody Repair_orderDTO repair_orderdto) {
        Repair_order domain = repair_orderMapping.toDomain(repair_orderdto);
		repair_orderService.create(domain);
        Repair_orderDTO dto = repair_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_orderMapping.toDomain(#repair_orderdtos),'iBizBusinessCentral-Repair_order-Create')")
    @ApiOperation(value = "批量新建维修单", tags = {"维修单" },  notes = "批量新建维修单")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_orderDTO> repair_orderdtos) {
        repair_orderService.createBatch(repair_orderMapping.toDomain(repair_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "repair_order" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.repair_orderService.get(#repair_order_id),'iBizBusinessCentral-Repair_order-Update')")
    @ApiOperation(value = "更新维修单", tags = {"维修单" },  notes = "更新维修单")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_orders/{repair_order_id}")
    public ResponseEntity<Repair_orderDTO> update(@PathVariable("repair_order_id") Long repair_order_id, @RequestBody Repair_orderDTO repair_orderdto) {
		Repair_order domain  = repair_orderMapping.toDomain(repair_orderdto);
        domain .setId(repair_order_id);
		repair_orderService.update(domain );
		Repair_orderDTO dto = repair_orderMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_orderService.getRepairOrderByEntities(this.repair_orderMapping.toDomain(#repair_orderdtos)),'iBizBusinessCentral-Repair_order-Update')")
    @ApiOperation(value = "批量更新维修单", tags = {"维修单" },  notes = "批量更新维修单")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_orderDTO> repair_orderdtos) {
        repair_orderService.updateBatch(repair_orderMapping.toDomain(repair_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.repair_orderService.get(#repair_order_id),'iBizBusinessCentral-Repair_order-Remove')")
    @ApiOperation(value = "删除维修单", tags = {"维修单" },  notes = "删除维修单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_orders/{repair_order_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("repair_order_id") Long repair_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_orderService.remove(repair_order_id));
    }

    @PreAuthorize("hasPermission(this.repair_orderService.getRepairOrderByIds(#ids),'iBizBusinessCentral-Repair_order-Remove')")
    @ApiOperation(value = "批量删除维修单", tags = {"维修单" },  notes = "批量删除维修单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        repair_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.repair_orderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Repair_order-Get')")
    @ApiOperation(value = "获取维修单", tags = {"维修单" },  notes = "获取维修单")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_orders/{repair_order_id}")
    public ResponseEntity<Repair_orderDTO> get(@PathVariable("repair_order_id") Long repair_order_id) {
        Repair_order domain = repair_orderService.get(repair_order_id);
        Repair_orderDTO dto = repair_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取维修单草稿", tags = {"维修单" },  notes = "获取维修单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_orders/getdraft")
    public ResponseEntity<Repair_orderDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(repair_orderMapping.toDto(repair_orderService.getDraft(new Repair_order())));
    }

    @ApiOperation(value = "检查维修单", tags = {"维修单" },  notes = "检查维修单")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_orders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Repair_orderDTO repair_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(repair_orderService.checkKey(repair_orderMapping.toDomain(repair_orderdto)));
    }

    @PreAuthorize("hasPermission(this.repair_orderMapping.toDomain(#repair_orderdto),'iBizBusinessCentral-Repair_order-Save')")
    @ApiOperation(value = "保存维修单", tags = {"维修单" },  notes = "保存维修单")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_orders/save")
    public ResponseEntity<Boolean> save(@RequestBody Repair_orderDTO repair_orderdto) {
        return ResponseEntity.status(HttpStatus.OK).body(repair_orderService.save(repair_orderMapping.toDomain(repair_orderdto)));
    }

    @PreAuthorize("hasPermission(this.repair_orderMapping.toDomain(#repair_orderdtos),'iBizBusinessCentral-Repair_order-Save')")
    @ApiOperation(value = "批量保存维修单", tags = {"维修单" },  notes = "批量保存维修单")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_orders/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Repair_orderDTO> repair_orderdtos) {
        repair_orderService.saveBatch(repair_orderMapping.toDomain(repair_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_order-Get')")
	@ApiOperation(value = "获取数据集", tags = {"维修单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/repair_orders/fetchdefault")
	public ResponseEntity<List<Repair_orderDTO>> fetchDefault(Repair_orderSearchContext context) {
        Page<Repair_order> domains = repair_orderService.searchDefault(context) ;
        List<Repair_orderDTO> list = repair_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_order-Get')")
	@ApiOperation(value = "查询数据集", tags = {"维修单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/repair_orders/searchdefault")
	public ResponseEntity<Page<Repair_orderDTO>> searchDefault(@RequestBody Repair_orderSearchContext context) {
        Page<Repair_order> domains = repair_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


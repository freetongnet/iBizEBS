package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_customer;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_customerService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_customerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"客户" })
@RestController("Core-res_customer")
@RequestMapping("")
public class Res_customerResource {

    @Autowired
    public IRes_customerService res_customerService;

    @Autowired
    @Lazy
    public Res_customerMapping res_customerMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-Create-all')")
    @ApiOperation(value = "新建客户", tags = {"客户" },  notes = "新建客户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_customers")
    public ResponseEntity<Res_customerDTO> create(@Validated @RequestBody Res_customerDTO res_customerdto) {
        Res_customer domain = res_customerMapping.toDomain(res_customerdto);
		res_customerService.create(domain);
        Res_customerDTO dto = res_customerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-Create-all')")
    @ApiOperation(value = "批量新建客户", tags = {"客户" },  notes = "批量新建客户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_customers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_customerDTO> res_customerdtos) {
        res_customerService.createBatch(res_customerMapping.toDomain(res_customerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-Update-all')")
    @ApiOperation(value = "更新客户", tags = {"客户" },  notes = "更新客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_customers/{res_customer_id}")
    public ResponseEntity<Res_customerDTO> update(@PathVariable("res_customer_id") Long res_customer_id, @RequestBody Res_customerDTO res_customerdto) {
		Res_customer domain  = res_customerMapping.toDomain(res_customerdto);
        domain .setId(res_customer_id);
		res_customerService.update(domain );
		Res_customerDTO dto = res_customerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-Update-all')")
    @ApiOperation(value = "批量更新客户", tags = {"客户" },  notes = "批量更新客户")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_customers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_customerDTO> res_customerdtos) {
        res_customerService.updateBatch(res_customerMapping.toDomain(res_customerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-Remove-all')")
    @ApiOperation(value = "删除客户", tags = {"客户" },  notes = "删除客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_customers/{res_customer_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_customer_id") Long res_customer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_customerService.remove(res_customer_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-Remove-all')")
    @ApiOperation(value = "批量删除客户", tags = {"客户" },  notes = "批量删除客户")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_customers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_customerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-Get-all')")
    @ApiOperation(value = "获取客户", tags = {"客户" },  notes = "获取客户")
	@RequestMapping(method = RequestMethod.GET, value = "/res_customers/{res_customer_id}")
    public ResponseEntity<Res_customerDTO> get(@PathVariable("res_customer_id") Long res_customer_id) {
        Res_customer domain = res_customerService.get(res_customer_id);
        Res_customerDTO dto = res_customerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取客户草稿", tags = {"客户" },  notes = "获取客户草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_customers/getdraft")
    public ResponseEntity<Res_customerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_customerMapping.toDto(res_customerService.getDraft(new Res_customer())));
    }

    @ApiOperation(value = "检查客户", tags = {"客户" },  notes = "检查客户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_customers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_customerDTO res_customerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_customerService.checkKey(res_customerMapping.toDomain(res_customerdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-Save-all')")
    @ApiOperation(value = "保存客户", tags = {"客户" },  notes = "保存客户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_customers/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_customerDTO res_customerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_customerService.save(res_customerMapping.toDomain(res_customerdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-Save-all')")
    @ApiOperation(value = "批量保存客户", tags = {"客户" },  notes = "批量保存客户")
	@RequestMapping(method = RequestMethod.POST, value = "/res_customers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_customerDTO> res_customerdtos) {
        res_customerService.saveBatch(res_customerMapping.toDomain(res_customerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"客户" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_customers/fetchdefault")
	public ResponseEntity<List<Res_customerDTO>> fetchDefault(Res_customerSearchContext context) {
        Page<Res_customer> domains = res_customerService.searchDefault(context) ;
        List<Res_customerDTO> list = res_customerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_customer-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"客户" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_customers/searchdefault")
	public ResponseEntity<Page<Res_customerDTO>> searchDefault(@RequestBody Res_customerSearchContext context) {
        Page<Res_customer> domains = res_customerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_customerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


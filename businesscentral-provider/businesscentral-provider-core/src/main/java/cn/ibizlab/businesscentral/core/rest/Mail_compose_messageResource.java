package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_compose_message;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_compose_messageService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_compose_messageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"邮件撰写向导" })
@RestController("Core-mail_compose_message")
@RequestMapping("")
public class Mail_compose_messageResource {

    @Autowired
    public IMail_compose_messageService mail_compose_messageService;

    @Autowired
    @Lazy
    public Mail_compose_messageMapping mail_compose_messageMapping;

    @PreAuthorize("hasPermission(this.mail_compose_messageMapping.toDomain(#mail_compose_messagedto),'iBizBusinessCentral-Mail_compose_message-Create')")
    @ApiOperation(value = "新建邮件撰写向导", tags = {"邮件撰写向导" },  notes = "新建邮件撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages")
    public ResponseEntity<Mail_compose_messageDTO> create(@Validated @RequestBody Mail_compose_messageDTO mail_compose_messagedto) {
        Mail_compose_message domain = mail_compose_messageMapping.toDomain(mail_compose_messagedto);
		mail_compose_messageService.create(domain);
        Mail_compose_messageDTO dto = mail_compose_messageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_compose_messageMapping.toDomain(#mail_compose_messagedtos),'iBizBusinessCentral-Mail_compose_message-Create')")
    @ApiOperation(value = "批量新建邮件撰写向导", tags = {"邮件撰写向导" },  notes = "批量新建邮件撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_compose_messageDTO> mail_compose_messagedtos) {
        mail_compose_messageService.createBatch(mail_compose_messageMapping.toDomain(mail_compose_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_compose_message" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_compose_messageService.get(#mail_compose_message_id),'iBizBusinessCentral-Mail_compose_message-Update')")
    @ApiOperation(value = "更新邮件撰写向导", tags = {"邮件撰写向导" },  notes = "更新邮件撰写向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_compose_messages/{mail_compose_message_id}")
    public ResponseEntity<Mail_compose_messageDTO> update(@PathVariable("mail_compose_message_id") Long mail_compose_message_id, @RequestBody Mail_compose_messageDTO mail_compose_messagedto) {
		Mail_compose_message domain  = mail_compose_messageMapping.toDomain(mail_compose_messagedto);
        domain .setId(mail_compose_message_id);
		mail_compose_messageService.update(domain );
		Mail_compose_messageDTO dto = mail_compose_messageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_compose_messageService.getMailComposeMessageByEntities(this.mail_compose_messageMapping.toDomain(#mail_compose_messagedtos)),'iBizBusinessCentral-Mail_compose_message-Update')")
    @ApiOperation(value = "批量更新邮件撰写向导", tags = {"邮件撰写向导" },  notes = "批量更新邮件撰写向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_compose_messages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_compose_messageDTO> mail_compose_messagedtos) {
        mail_compose_messageService.updateBatch(mail_compose_messageMapping.toDomain(mail_compose_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_compose_messageService.get(#mail_compose_message_id),'iBizBusinessCentral-Mail_compose_message-Remove')")
    @ApiOperation(value = "删除邮件撰写向导", tags = {"邮件撰写向导" },  notes = "删除邮件撰写向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_compose_messages/{mail_compose_message_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_compose_message_id") Long mail_compose_message_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_compose_messageService.remove(mail_compose_message_id));
    }

    @PreAuthorize("hasPermission(this.mail_compose_messageService.getMailComposeMessageByIds(#ids),'iBizBusinessCentral-Mail_compose_message-Remove')")
    @ApiOperation(value = "批量删除邮件撰写向导", tags = {"邮件撰写向导" },  notes = "批量删除邮件撰写向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_compose_messages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_compose_messageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_compose_messageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_compose_message-Get')")
    @ApiOperation(value = "获取邮件撰写向导", tags = {"邮件撰写向导" },  notes = "获取邮件撰写向导")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_compose_messages/{mail_compose_message_id}")
    public ResponseEntity<Mail_compose_messageDTO> get(@PathVariable("mail_compose_message_id") Long mail_compose_message_id) {
        Mail_compose_message domain = mail_compose_messageService.get(mail_compose_message_id);
        Mail_compose_messageDTO dto = mail_compose_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取邮件撰写向导草稿", tags = {"邮件撰写向导" },  notes = "获取邮件撰写向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_compose_messages/getdraft")
    public ResponseEntity<Mail_compose_messageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_compose_messageMapping.toDto(mail_compose_messageService.getDraft(new Mail_compose_message())));
    }

    @ApiOperation(value = "检查邮件撰写向导", tags = {"邮件撰写向导" },  notes = "检查邮件撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_compose_messageDTO mail_compose_messagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_compose_messageService.checkKey(mail_compose_messageMapping.toDomain(mail_compose_messagedto)));
    }

    @PreAuthorize("hasPermission(this.mail_compose_messageMapping.toDomain(#mail_compose_messagedto),'iBizBusinessCentral-Mail_compose_message-Save')")
    @ApiOperation(value = "保存邮件撰写向导", tags = {"邮件撰写向导" },  notes = "保存邮件撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_compose_messageDTO mail_compose_messagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_compose_messageService.save(mail_compose_messageMapping.toDomain(mail_compose_messagedto)));
    }

    @PreAuthorize("hasPermission(this.mail_compose_messageMapping.toDomain(#mail_compose_messagedtos),'iBizBusinessCentral-Mail_compose_message-Save')")
    @ApiOperation(value = "批量保存邮件撰写向导", tags = {"邮件撰写向导" },  notes = "批量保存邮件撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_compose_messages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_compose_messageDTO> mail_compose_messagedtos) {
        mail_compose_messageService.saveBatch(mail_compose_messageMapping.toDomain(mail_compose_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_compose_message-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_compose_message-Get')")
	@ApiOperation(value = "获取数据集", tags = {"邮件撰写向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_compose_messages/fetchdefault")
	public ResponseEntity<List<Mail_compose_messageDTO>> fetchDefault(Mail_compose_messageSearchContext context) {
        Page<Mail_compose_message> domains = mail_compose_messageService.searchDefault(context) ;
        List<Mail_compose_messageDTO> list = mail_compose_messageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_compose_message-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_compose_message-Get')")
	@ApiOperation(value = "查询数据集", tags = {"邮件撰写向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_compose_messages/searchdefault")
	public ResponseEntity<Page<Mail_compose_messageDTO>> searchDefault(@RequestBody Mail_compose_messageSearchContext context) {
        Page<Mail_compose_message> domains = mail_compose_messageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_compose_messageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_companySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"公司" })
@RestController("Core-res_company")
@RequestMapping("")
public class Res_companyResource {

    @Autowired
    public IRes_companyService res_companyService;

    @Autowired
    @Lazy
    public Res_companyMapping res_companyMapping;

    @PreAuthorize("hasPermission(this.res_companyMapping.toDomain(#res_companydto),'iBizBusinessCentral-Res_company-Create')")
    @ApiOperation(value = "新建公司", tags = {"公司" },  notes = "新建公司")
	@RequestMapping(method = RequestMethod.POST, value = "/res_companies")
    public ResponseEntity<Res_companyDTO> create(@Validated @RequestBody Res_companyDTO res_companydto) {
        Res_company domain = res_companyMapping.toDomain(res_companydto);
		res_companyService.create(domain);
        Res_companyDTO dto = res_companyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_companyMapping.toDomain(#res_companydtos),'iBizBusinessCentral-Res_company-Create')")
    @ApiOperation(value = "批量新建公司", tags = {"公司" },  notes = "批量新建公司")
	@RequestMapping(method = RequestMethod.POST, value = "/res_companies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_companyDTO> res_companydtos) {
        res_companyService.createBatch(res_companyMapping.toDomain(res_companydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_company" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_companyService.get(#res_company_id),'iBizBusinessCentral-Res_company-Update')")
    @ApiOperation(value = "更新公司", tags = {"公司" },  notes = "更新公司")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_companies/{res_company_id}")
    public ResponseEntity<Res_companyDTO> update(@PathVariable("res_company_id") Long res_company_id, @RequestBody Res_companyDTO res_companydto) {
		Res_company domain  = res_companyMapping.toDomain(res_companydto);
        domain .setId(res_company_id);
		res_companyService.update(domain );
		Res_companyDTO dto = res_companyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_companyService.getResCompanyByEntities(this.res_companyMapping.toDomain(#res_companydtos)),'iBizBusinessCentral-Res_company-Update')")
    @ApiOperation(value = "批量更新公司", tags = {"公司" },  notes = "批量更新公司")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_companies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_companyDTO> res_companydtos) {
        res_companyService.updateBatch(res_companyMapping.toDomain(res_companydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_companyService.get(#res_company_id),'iBizBusinessCentral-Res_company-Remove')")
    @ApiOperation(value = "删除公司", tags = {"公司" },  notes = "删除公司")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_companies/{res_company_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_company_id") Long res_company_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_companyService.remove(res_company_id));
    }

    @PreAuthorize("hasPermission(this.res_companyService.getResCompanyByIds(#ids),'iBizBusinessCentral-Res_company-Remove')")
    @ApiOperation(value = "批量删除公司", tags = {"公司" },  notes = "批量删除公司")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_companies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_companyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_companyMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_company-Get')")
    @ApiOperation(value = "获取公司", tags = {"公司" },  notes = "获取公司")
	@RequestMapping(method = RequestMethod.GET, value = "/res_companies/{res_company_id}")
    public ResponseEntity<Res_companyDTO> get(@PathVariable("res_company_id") Long res_company_id) {
        Res_company domain = res_companyService.get(res_company_id);
        Res_companyDTO dto = res_companyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取公司草稿", tags = {"公司" },  notes = "获取公司草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_companies/getdraft")
    public ResponseEntity<Res_companyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_companyMapping.toDto(res_companyService.getDraft(new Res_company())));
    }

    @ApiOperation(value = "检查公司", tags = {"公司" },  notes = "检查公司")
	@RequestMapping(method = RequestMethod.POST, value = "/res_companies/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_companyDTO res_companydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_companyService.checkKey(res_companyMapping.toDomain(res_companydto)));
    }

    @PreAuthorize("hasPermission(this.res_companyMapping.toDomain(#res_companydto),'iBizBusinessCentral-Res_company-Save')")
    @ApiOperation(value = "保存公司", tags = {"公司" },  notes = "保存公司")
	@RequestMapping(method = RequestMethod.POST, value = "/res_companies/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_companyDTO res_companydto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_companyService.save(res_companyMapping.toDomain(res_companydto)));
    }

    @PreAuthorize("hasPermission(this.res_companyMapping.toDomain(#res_companydtos),'iBizBusinessCentral-Res_company-Save')")
    @ApiOperation(value = "批量保存公司", tags = {"公司" },  notes = "批量保存公司")
	@RequestMapping(method = RequestMethod.POST, value = "/res_companies/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_companyDTO> res_companydtos) {
        res_companyService.saveBatch(res_companyMapping.toDomain(res_companydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_company-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_company-Get')")
	@ApiOperation(value = "获取数据集", tags = {"公司" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_companies/fetchdefault")
	public ResponseEntity<List<Res_companyDTO>> fetchDefault(Res_companySearchContext context) {
        Page<Res_company> domains = res_companyService.searchDefault(context) ;
        List<Res_companyDTO> list = res_companyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_company-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_company-Get')")
	@ApiOperation(value = "查询数据集", tags = {"公司" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_companies/searchdefault")
	public ResponseEntity<Page<Res_companyDTO>> searchDefault(@RequestBody Res_companySearchContext context) {
        Page<Res_company> domains = res_companyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_companyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_company-searchROOT-all') and hasPermission(#context,'iBizBusinessCentral-Res_company-Get')")
	@ApiOperation(value = "获取根节点查询", tags = {"公司" } ,notes = "获取根节点查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_companies/fetchroot")
	public ResponseEntity<List<Res_companyDTO>> fetchROOT(Res_companySearchContext context) {
        Page<Res_company> domains = res_companyService.searchROOT(context) ;
        List<Res_companyDTO> list = res_companyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_company-searchROOT-all') and hasPermission(#context,'iBizBusinessCentral-Res_company-Get')")
	@ApiOperation(value = "查询根节点查询", tags = {"公司" } ,notes = "查询根节点查询")
    @RequestMapping(method= RequestMethod.POST , value="/res_companies/searchroot")
	public ResponseEntity<Page<Res_companyDTO>> searchROOT(@RequestBody Res_companySearchContext context) {
        Page<Res_company> domains = res_companyService.searchROOT(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_companyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


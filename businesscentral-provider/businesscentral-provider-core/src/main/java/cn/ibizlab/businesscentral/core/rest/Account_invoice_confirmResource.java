package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_confirm;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_confirmService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_confirmSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"确认选定的发票" })
@RestController("Core-account_invoice_confirm")
@RequestMapping("")
public class Account_invoice_confirmResource {

    @Autowired
    public IAccount_invoice_confirmService account_invoice_confirmService;

    @Autowired
    @Lazy
    public Account_invoice_confirmMapping account_invoice_confirmMapping;

    @PreAuthorize("hasPermission(this.account_invoice_confirmMapping.toDomain(#account_invoice_confirmdto),'iBizBusinessCentral-Account_invoice_confirm-Create')")
    @ApiOperation(value = "新建确认选定的发票", tags = {"确认选定的发票" },  notes = "新建确认选定的发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms")
    public ResponseEntity<Account_invoice_confirmDTO> create(@Validated @RequestBody Account_invoice_confirmDTO account_invoice_confirmdto) {
        Account_invoice_confirm domain = account_invoice_confirmMapping.toDomain(account_invoice_confirmdto);
		account_invoice_confirmService.create(domain);
        Account_invoice_confirmDTO dto = account_invoice_confirmMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_confirmMapping.toDomain(#account_invoice_confirmdtos),'iBizBusinessCentral-Account_invoice_confirm-Create')")
    @ApiOperation(value = "批量新建确认选定的发票", tags = {"确认选定的发票" },  notes = "批量新建确认选定的发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_confirmDTO> account_invoice_confirmdtos) {
        account_invoice_confirmService.createBatch(account_invoice_confirmMapping.toDomain(account_invoice_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_invoice_confirm" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_invoice_confirmService.get(#account_invoice_confirm_id),'iBizBusinessCentral-Account_invoice_confirm-Update')")
    @ApiOperation(value = "更新确认选定的发票", tags = {"确认选定的发票" },  notes = "更新确认选定的发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_confirms/{account_invoice_confirm_id}")
    public ResponseEntity<Account_invoice_confirmDTO> update(@PathVariable("account_invoice_confirm_id") Long account_invoice_confirm_id, @RequestBody Account_invoice_confirmDTO account_invoice_confirmdto) {
		Account_invoice_confirm domain  = account_invoice_confirmMapping.toDomain(account_invoice_confirmdto);
        domain .setId(account_invoice_confirm_id);
		account_invoice_confirmService.update(domain );
		Account_invoice_confirmDTO dto = account_invoice_confirmMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_confirmService.getAccountInvoiceConfirmByEntities(this.account_invoice_confirmMapping.toDomain(#account_invoice_confirmdtos)),'iBizBusinessCentral-Account_invoice_confirm-Update')")
    @ApiOperation(value = "批量更新确认选定的发票", tags = {"确认选定的发票" },  notes = "批量更新确认选定的发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_confirms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_confirmDTO> account_invoice_confirmdtos) {
        account_invoice_confirmService.updateBatch(account_invoice_confirmMapping.toDomain(account_invoice_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_invoice_confirmService.get(#account_invoice_confirm_id),'iBizBusinessCentral-Account_invoice_confirm-Remove')")
    @ApiOperation(value = "删除确认选定的发票", tags = {"确认选定的发票" },  notes = "删除确认选定的发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_confirms/{account_invoice_confirm_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_confirm_id") Long account_invoice_confirm_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_confirmService.remove(account_invoice_confirm_id));
    }

    @PreAuthorize("hasPermission(this.account_invoice_confirmService.getAccountInvoiceConfirmByIds(#ids),'iBizBusinessCentral-Account_invoice_confirm-Remove')")
    @ApiOperation(value = "批量删除确认选定的发票", tags = {"确认选定的发票" },  notes = "批量删除确认选定的发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_confirms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_invoice_confirmService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_invoice_confirmMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_invoice_confirm-Get')")
    @ApiOperation(value = "获取确认选定的发票", tags = {"确认选定的发票" },  notes = "获取确认选定的发票")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_confirms/{account_invoice_confirm_id}")
    public ResponseEntity<Account_invoice_confirmDTO> get(@PathVariable("account_invoice_confirm_id") Long account_invoice_confirm_id) {
        Account_invoice_confirm domain = account_invoice_confirmService.get(account_invoice_confirm_id);
        Account_invoice_confirmDTO dto = account_invoice_confirmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取确认选定的发票草稿", tags = {"确认选定的发票" },  notes = "获取确认选定的发票草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_confirms/getdraft")
    public ResponseEntity<Account_invoice_confirmDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_confirmMapping.toDto(account_invoice_confirmService.getDraft(new Account_invoice_confirm())));
    }

    @ApiOperation(value = "检查确认选定的发票", tags = {"确认选定的发票" },  notes = "检查确认选定的发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_confirmDTO account_invoice_confirmdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoice_confirmService.checkKey(account_invoice_confirmMapping.toDomain(account_invoice_confirmdto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_confirmMapping.toDomain(#account_invoice_confirmdto),'iBizBusinessCentral-Account_invoice_confirm-Save')")
    @ApiOperation(value = "保存确认选定的发票", tags = {"确认选定的发票" },  notes = "保存确认选定的发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoice_confirmDTO account_invoice_confirmdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_confirmService.save(account_invoice_confirmMapping.toDomain(account_invoice_confirmdto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_confirmMapping.toDomain(#account_invoice_confirmdtos),'iBizBusinessCentral-Account_invoice_confirm-Save')")
    @ApiOperation(value = "批量保存确认选定的发票", tags = {"确认选定的发票" },  notes = "批量保存确认选定的发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoice_confirmDTO> account_invoice_confirmdtos) {
        account_invoice_confirmService.saveBatch(account_invoice_confirmMapping.toDomain(account_invoice_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_confirm-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_confirm-Get')")
	@ApiOperation(value = "获取数据集", tags = {"确认选定的发票" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_confirms/fetchdefault")
	public ResponseEntity<List<Account_invoice_confirmDTO>> fetchDefault(Account_invoice_confirmSearchContext context) {
        Page<Account_invoice_confirm> domains = account_invoice_confirmService.searchDefault(context) ;
        List<Account_invoice_confirmDTO> list = account_invoice_confirmMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_confirm-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_confirm-Get')")
	@ApiOperation(value = "查询数据集", tags = {"确认选定的发票" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoice_confirms/searchdefault")
	public ResponseEntity<Page<Account_invoice_confirmDTO>> searchDefault(@RequestBody Account_invoice_confirmSearchContext context) {
        Page<Account_invoice_confirm> domains = account_invoice_confirmService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_confirmMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


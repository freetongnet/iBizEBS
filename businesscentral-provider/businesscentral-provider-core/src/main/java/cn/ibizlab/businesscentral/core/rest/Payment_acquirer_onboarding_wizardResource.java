package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_acquirer_onboarding_wizardService;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"付款获得onboarding向导" })
@RestController("Core-payment_acquirer_onboarding_wizard")
@RequestMapping("")
public class Payment_acquirer_onboarding_wizardResource {

    @Autowired
    public IPayment_acquirer_onboarding_wizardService payment_acquirer_onboarding_wizardService;

    @Autowired
    @Lazy
    public Payment_acquirer_onboarding_wizardMapping payment_acquirer_onboarding_wizardMapping;

    @PreAuthorize("hasPermission(this.payment_acquirer_onboarding_wizardMapping.toDomain(#payment_acquirer_onboarding_wizarddto),'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Create')")
    @ApiOperation(value = "新建付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "新建付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards")
    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> create(@Validated @RequestBody Payment_acquirer_onboarding_wizardDTO payment_acquirer_onboarding_wizarddto) {
        Payment_acquirer_onboarding_wizard domain = payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddto);
		payment_acquirer_onboarding_wizardService.create(domain);
        Payment_acquirer_onboarding_wizardDTO dto = payment_acquirer_onboarding_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_acquirer_onboarding_wizardMapping.toDomain(#payment_acquirer_onboarding_wizarddtos),'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Create')")
    @ApiOperation(value = "批量新建付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "批量新建付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_acquirer_onboarding_wizardDTO> payment_acquirer_onboarding_wizarddtos) {
        payment_acquirer_onboarding_wizardService.createBatch(payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "payment_acquirer_onboarding_wizard" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.payment_acquirer_onboarding_wizardService.get(#payment_acquirer_onboarding_wizard_id),'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Update')")
    @ApiOperation(value = "更新付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "更新付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirer_onboarding_wizards/{payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> update(@PathVariable("payment_acquirer_onboarding_wizard_id") Long payment_acquirer_onboarding_wizard_id, @RequestBody Payment_acquirer_onboarding_wizardDTO payment_acquirer_onboarding_wizarddto) {
		Payment_acquirer_onboarding_wizard domain  = payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddto);
        domain .setId(payment_acquirer_onboarding_wizard_id);
		payment_acquirer_onboarding_wizardService.update(domain );
		Payment_acquirer_onboarding_wizardDTO dto = payment_acquirer_onboarding_wizardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_acquirer_onboarding_wizardService.getPaymentAcquirerOnboardingWizardByEntities(this.payment_acquirer_onboarding_wizardMapping.toDomain(#payment_acquirer_onboarding_wizarddtos)),'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Update')")
    @ApiOperation(value = "批量更新付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "批量更新付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_acquirer_onboarding_wizardDTO> payment_acquirer_onboarding_wizarddtos) {
        payment_acquirer_onboarding_wizardService.updateBatch(payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.payment_acquirer_onboarding_wizardService.get(#payment_acquirer_onboarding_wizard_id),'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Remove')")
    @ApiOperation(value = "删除付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "删除付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirer_onboarding_wizards/{payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("payment_acquirer_onboarding_wizard_id") Long payment_acquirer_onboarding_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_acquirer_onboarding_wizardService.remove(payment_acquirer_onboarding_wizard_id));
    }

    @PreAuthorize("hasPermission(this.payment_acquirer_onboarding_wizardService.getPaymentAcquirerOnboardingWizardByIds(#ids),'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Remove')")
    @ApiOperation(value = "批量删除付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "批量删除付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        payment_acquirer_onboarding_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.payment_acquirer_onboarding_wizardMapping.toDomain(returnObject.body),'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Get')")
    @ApiOperation(value = "获取付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "获取付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_acquirer_onboarding_wizards/{payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> get(@PathVariable("payment_acquirer_onboarding_wizard_id") Long payment_acquirer_onboarding_wizard_id) {
        Payment_acquirer_onboarding_wizard domain = payment_acquirer_onboarding_wizardService.get(payment_acquirer_onboarding_wizard_id);
        Payment_acquirer_onboarding_wizardDTO dto = payment_acquirer_onboarding_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取付款获得onboarding向导草稿", tags = {"付款获得onboarding向导" },  notes = "获取付款获得onboarding向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_acquirer_onboarding_wizards/getdraft")
    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(payment_acquirer_onboarding_wizardMapping.toDto(payment_acquirer_onboarding_wizardService.getDraft(new Payment_acquirer_onboarding_wizard())));
    }

    @ApiOperation(value = "检查付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "检查付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Payment_acquirer_onboarding_wizardDTO payment_acquirer_onboarding_wizarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(payment_acquirer_onboarding_wizardService.checkKey(payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.payment_acquirer_onboarding_wizardMapping.toDomain(#payment_acquirer_onboarding_wizarddto),'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Save')")
    @ApiOperation(value = "保存付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "保存付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards/save")
    public ResponseEntity<Boolean> save(@RequestBody Payment_acquirer_onboarding_wizardDTO payment_acquirer_onboarding_wizarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(payment_acquirer_onboarding_wizardService.save(payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.payment_acquirer_onboarding_wizardMapping.toDomain(#payment_acquirer_onboarding_wizarddtos),'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Save')")
    @ApiOperation(value = "批量保存付款获得onboarding向导", tags = {"付款获得onboarding向导" },  notes = "批量保存付款获得onboarding向导")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Payment_acquirer_onboarding_wizardDTO> payment_acquirer_onboarding_wizarddtos) {
        payment_acquirer_onboarding_wizardService.saveBatch(payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_acquirer_onboarding_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Get')")
	@ApiOperation(value = "获取数据集", tags = {"付款获得onboarding向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/payment_acquirer_onboarding_wizards/fetchdefault")
	public ResponseEntity<List<Payment_acquirer_onboarding_wizardDTO>> fetchDefault(Payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Payment_acquirer_onboarding_wizard> domains = payment_acquirer_onboarding_wizardService.searchDefault(context) ;
        List<Payment_acquirer_onboarding_wizardDTO> list = payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_acquirer_onboarding_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_acquirer_onboarding_wizard-Get')")
	@ApiOperation(value = "查询数据集", tags = {"付款获得onboarding向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/payment_acquirer_onboarding_wizards/searchdefault")
	public ResponseEntity<Page<Payment_acquirer_onboarding_wizardDTO>> searchDefault(@RequestBody Payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Payment_acquirer_onboarding_wizard> domains = payment_acquirer_onboarding_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


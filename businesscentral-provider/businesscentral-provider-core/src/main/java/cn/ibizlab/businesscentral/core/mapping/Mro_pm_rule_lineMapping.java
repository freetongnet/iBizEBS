package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_rule_line;
import cn.ibizlab.businesscentral.core.dto.Mro_pm_rule_lineDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMro_pm_rule_lineMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mro_pm_rule_lineMapping extends MappingBase<Mro_pm_rule_lineDTO, Mro_pm_rule_line> {


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_allocationService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leave_allocationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"休假分配" })
@RestController("Core-hr_leave_allocation")
@RequestMapping("")
public class Hr_leave_allocationResource {

    @Autowired
    public IHr_leave_allocationService hr_leave_allocationService;

    @Autowired
    @Lazy
    public Hr_leave_allocationMapping hr_leave_allocationMapping;

    @PreAuthorize("hasPermission(this.hr_leave_allocationMapping.toDomain(#hr_leave_allocationdto),'iBizBusinessCentral-Hr_leave_allocation-Create')")
    @ApiOperation(value = "新建休假分配", tags = {"休假分配" },  notes = "新建休假分配")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations")
    public ResponseEntity<Hr_leave_allocationDTO> create(@Validated @RequestBody Hr_leave_allocationDTO hr_leave_allocationdto) {
        Hr_leave_allocation domain = hr_leave_allocationMapping.toDomain(hr_leave_allocationdto);
		hr_leave_allocationService.create(domain);
        Hr_leave_allocationDTO dto = hr_leave_allocationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_leave_allocationMapping.toDomain(#hr_leave_allocationdtos),'iBizBusinessCentral-Hr_leave_allocation-Create')")
    @ApiOperation(value = "批量新建休假分配", tags = {"休假分配" },  notes = "批量新建休假分配")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_leave_allocationDTO> hr_leave_allocationdtos) {
        hr_leave_allocationService.createBatch(hr_leave_allocationMapping.toDomain(hr_leave_allocationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_leave_allocation" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_leave_allocationService.get(#hr_leave_allocation_id),'iBizBusinessCentral-Hr_leave_allocation-Update')")
    @ApiOperation(value = "更新休假分配", tags = {"休假分配" },  notes = "更新休假分配")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_allocations/{hr_leave_allocation_id}")
    public ResponseEntity<Hr_leave_allocationDTO> update(@PathVariable("hr_leave_allocation_id") Long hr_leave_allocation_id, @RequestBody Hr_leave_allocationDTO hr_leave_allocationdto) {
		Hr_leave_allocation domain  = hr_leave_allocationMapping.toDomain(hr_leave_allocationdto);
        domain .setId(hr_leave_allocation_id);
		hr_leave_allocationService.update(domain );
		Hr_leave_allocationDTO dto = hr_leave_allocationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_leave_allocationService.getHrLeaveAllocationByEntities(this.hr_leave_allocationMapping.toDomain(#hr_leave_allocationdtos)),'iBizBusinessCentral-Hr_leave_allocation-Update')")
    @ApiOperation(value = "批量更新休假分配", tags = {"休假分配" },  notes = "批量更新休假分配")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_allocations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leave_allocationDTO> hr_leave_allocationdtos) {
        hr_leave_allocationService.updateBatch(hr_leave_allocationMapping.toDomain(hr_leave_allocationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_leave_allocationService.get(#hr_leave_allocation_id),'iBizBusinessCentral-Hr_leave_allocation-Remove')")
    @ApiOperation(value = "删除休假分配", tags = {"休假分配" },  notes = "删除休假分配")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_allocations/{hr_leave_allocation_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_allocation_id") Long hr_leave_allocation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_leave_allocationService.remove(hr_leave_allocation_id));
    }

    @PreAuthorize("hasPermission(this.hr_leave_allocationService.getHrLeaveAllocationByIds(#ids),'iBizBusinessCentral-Hr_leave_allocation-Remove')")
    @ApiOperation(value = "批量删除休假分配", tags = {"休假分配" },  notes = "批量删除休假分配")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_allocations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_leave_allocationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_leave_allocationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_leave_allocation-Get')")
    @ApiOperation(value = "获取休假分配", tags = {"休假分配" },  notes = "获取休假分配")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leave_allocations/{hr_leave_allocation_id}")
    public ResponseEntity<Hr_leave_allocationDTO> get(@PathVariable("hr_leave_allocation_id") Long hr_leave_allocation_id) {
        Hr_leave_allocation domain = hr_leave_allocationService.get(hr_leave_allocation_id);
        Hr_leave_allocationDTO dto = hr_leave_allocationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取休假分配草稿", tags = {"休假分配" },  notes = "获取休假分配草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leave_allocations/getdraft")
    public ResponseEntity<Hr_leave_allocationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_leave_allocationMapping.toDto(hr_leave_allocationService.getDraft(new Hr_leave_allocation())));
    }

    @ApiOperation(value = "检查休假分配", tags = {"休假分配" },  notes = "检查休假分配")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_leave_allocationDTO hr_leave_allocationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_leave_allocationService.checkKey(hr_leave_allocationMapping.toDomain(hr_leave_allocationdto)));
    }

    @PreAuthorize("hasPermission(this.hr_leave_allocationMapping.toDomain(#hr_leave_allocationdto),'iBizBusinessCentral-Hr_leave_allocation-Save')")
    @ApiOperation(value = "保存休假分配", tags = {"休假分配" },  notes = "保存休假分配")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_leave_allocationDTO hr_leave_allocationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_leave_allocationService.save(hr_leave_allocationMapping.toDomain(hr_leave_allocationdto)));
    }

    @PreAuthorize("hasPermission(this.hr_leave_allocationMapping.toDomain(#hr_leave_allocationdtos),'iBizBusinessCentral-Hr_leave_allocation-Save')")
    @ApiOperation(value = "批量保存休假分配", tags = {"休假分配" },  notes = "批量保存休假分配")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_allocations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_leave_allocationDTO> hr_leave_allocationdtos) {
        hr_leave_allocationService.saveBatch(hr_leave_allocationMapping.toDomain(hr_leave_allocationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_allocation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_leave_allocation-Get')")
	@ApiOperation(value = "获取数据集", tags = {"休假分配" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leave_allocations/fetchdefault")
	public ResponseEntity<List<Hr_leave_allocationDTO>> fetchDefault(Hr_leave_allocationSearchContext context) {
        Page<Hr_leave_allocation> domains = hr_leave_allocationService.searchDefault(context) ;
        List<Hr_leave_allocationDTO> list = hr_leave_allocationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_allocation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_leave_allocation-Get')")
	@ApiOperation(value = "查询数据集", tags = {"休假分配" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_leave_allocations/searchdefault")
	public ResponseEntity<Page<Hr_leave_allocationDTO>> searchDefault(@RequestBody Hr_leave_allocationSearchContext context) {
        Page<Hr_leave_allocation> domains = hr_leave_allocationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_leave_allocationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


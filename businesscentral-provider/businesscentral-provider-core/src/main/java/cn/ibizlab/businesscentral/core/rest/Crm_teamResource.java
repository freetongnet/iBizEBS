package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_teamSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"销售渠道" })
@RestController("Core-crm_team")
@RequestMapping("")
public class Crm_teamResource {

    @Autowired
    public ICrm_teamService crm_teamService;

    @Autowired
    @Lazy
    public Crm_teamMapping crm_teamMapping;

    @PreAuthorize("hasPermission(this.crm_teamMapping.toDomain(#crm_teamdto),'iBizBusinessCentral-Crm_team-Create')")
    @ApiOperation(value = "新建销售渠道", tags = {"销售渠道" },  notes = "新建销售渠道")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams")
    public ResponseEntity<Crm_teamDTO> create(@Validated @RequestBody Crm_teamDTO crm_teamdto) {
        Crm_team domain = crm_teamMapping.toDomain(crm_teamdto);
		crm_teamService.create(domain);
        Crm_teamDTO dto = crm_teamMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_teamMapping.toDomain(#crm_teamdtos),'iBizBusinessCentral-Crm_team-Create')")
    @ApiOperation(value = "批量新建销售渠道", tags = {"销售渠道" },  notes = "批量新建销售渠道")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_teamDTO> crm_teamdtos) {
        crm_teamService.createBatch(crm_teamMapping.toDomain(crm_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "crm_team" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.crm_teamService.get(#crm_team_id),'iBizBusinessCentral-Crm_team-Update')")
    @ApiOperation(value = "更新销售渠道", tags = {"销售渠道" },  notes = "更新销售渠道")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/{crm_team_id}")
    public ResponseEntity<Crm_teamDTO> update(@PathVariable("crm_team_id") Long crm_team_id, @RequestBody Crm_teamDTO crm_teamdto) {
		Crm_team domain  = crm_teamMapping.toDomain(crm_teamdto);
        domain .setId(crm_team_id);
		crm_teamService.update(domain );
		Crm_teamDTO dto = crm_teamMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_teamService.getCrmTeamByEntities(this.crm_teamMapping.toDomain(#crm_teamdtos)),'iBizBusinessCentral-Crm_team-Update')")
    @ApiOperation(value = "批量更新销售渠道", tags = {"销售渠道" },  notes = "批量更新销售渠道")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_teams/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_teamDTO> crm_teamdtos) {
        crm_teamService.updateBatch(crm_teamMapping.toDomain(crm_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.crm_teamService.get(#crm_team_id),'iBizBusinessCentral-Crm_team-Remove')")
    @ApiOperation(value = "删除销售渠道", tags = {"销售渠道" },  notes = "删除销售渠道")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/{crm_team_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_team_id") Long crm_team_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_teamService.remove(crm_team_id));
    }

    @PreAuthorize("hasPermission(this.crm_teamService.getCrmTeamByIds(#ids),'iBizBusinessCentral-Crm_team-Remove')")
    @ApiOperation(value = "批量删除销售渠道", tags = {"销售渠道" },  notes = "批量删除销售渠道")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_teams/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        crm_teamService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.crm_teamMapping.toDomain(returnObject.body),'iBizBusinessCentral-Crm_team-Get')")
    @ApiOperation(value = "获取销售渠道", tags = {"销售渠道" },  notes = "获取销售渠道")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_teams/{crm_team_id}")
    public ResponseEntity<Crm_teamDTO> get(@PathVariable("crm_team_id") Long crm_team_id) {
        Crm_team domain = crm_teamService.get(crm_team_id);
        Crm_teamDTO dto = crm_teamMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取销售渠道草稿", tags = {"销售渠道" },  notes = "获取销售渠道草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_teams/getdraft")
    public ResponseEntity<Crm_teamDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_teamMapping.toDto(crm_teamService.getDraft(new Crm_team())));
    }

    @ApiOperation(value = "检查销售渠道", tags = {"销售渠道" },  notes = "检查销售渠道")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_teamDTO crm_teamdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_teamService.checkKey(crm_teamMapping.toDomain(crm_teamdto)));
    }

    @PreAuthorize("hasPermission(this.crm_teamMapping.toDomain(#crm_teamdto),'iBizBusinessCentral-Crm_team-Save')")
    @ApiOperation(value = "保存销售渠道", tags = {"销售渠道" },  notes = "保存销售渠道")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_teamDTO crm_teamdto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_teamService.save(crm_teamMapping.toDomain(crm_teamdto)));
    }

    @PreAuthorize("hasPermission(this.crm_teamMapping.toDomain(#crm_teamdtos),'iBizBusinessCentral-Crm_team-Save')")
    @ApiOperation(value = "批量保存销售渠道", tags = {"销售渠道" },  notes = "批量保存销售渠道")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_teams/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_teamDTO> crm_teamdtos) {
        crm_teamService.saveBatch(crm_teamMapping.toDomain(crm_teamdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_team-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_team-Get')")
	@ApiOperation(value = "获取数据集", tags = {"销售渠道" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/crm_teams/fetchdefault")
	public ResponseEntity<List<Crm_teamDTO>> fetchDefault(Crm_teamSearchContext context) {
        Page<Crm_team> domains = crm_teamService.searchDefault(context) ;
        List<Crm_teamDTO> list = crm_teamMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_team-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_team-Get')")
	@ApiOperation(value = "查询数据集", tags = {"销售渠道" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/crm_teams/searchdefault")
	public ResponseEntity<Page<Crm_teamDTO>> searchDefault(@RequestBody Crm_teamSearchContext context) {
        Page<Crm_team> domains = crm_teamService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_teamMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


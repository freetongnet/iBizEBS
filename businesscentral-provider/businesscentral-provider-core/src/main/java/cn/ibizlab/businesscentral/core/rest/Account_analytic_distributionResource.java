package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_distribution;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_distributionService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_distributionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"分析账户分配" })
@RestController("Core-account_analytic_distribution")
@RequestMapping("")
public class Account_analytic_distributionResource {

    @Autowired
    public IAccount_analytic_distributionService account_analytic_distributionService;

    @Autowired
    @Lazy
    public Account_analytic_distributionMapping account_analytic_distributionMapping;

    @PreAuthorize("hasPermission(this.account_analytic_distributionMapping.toDomain(#account_analytic_distributiondto),'iBizBusinessCentral-Account_analytic_distribution-Create')")
    @ApiOperation(value = "新建分析账户分配", tags = {"分析账户分配" },  notes = "新建分析账户分配")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions")
    public ResponseEntity<Account_analytic_distributionDTO> create(@Validated @RequestBody Account_analytic_distributionDTO account_analytic_distributiondto) {
        Account_analytic_distribution domain = account_analytic_distributionMapping.toDomain(account_analytic_distributiondto);
		account_analytic_distributionService.create(domain);
        Account_analytic_distributionDTO dto = account_analytic_distributionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_distributionMapping.toDomain(#account_analytic_distributiondtos),'iBizBusinessCentral-Account_analytic_distribution-Create')")
    @ApiOperation(value = "批量新建分析账户分配", tags = {"分析账户分配" },  notes = "批量新建分析账户分配")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_analytic_distributionDTO> account_analytic_distributiondtos) {
        account_analytic_distributionService.createBatch(account_analytic_distributionMapping.toDomain(account_analytic_distributiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_analytic_distribution" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_analytic_distributionService.get(#account_analytic_distribution_id),'iBizBusinessCentral-Account_analytic_distribution-Update')")
    @ApiOperation(value = "更新分析账户分配", tags = {"分析账户分配" },  notes = "更新分析账户分配")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_distributions/{account_analytic_distribution_id}")
    public ResponseEntity<Account_analytic_distributionDTO> update(@PathVariable("account_analytic_distribution_id") Long account_analytic_distribution_id, @RequestBody Account_analytic_distributionDTO account_analytic_distributiondto) {
		Account_analytic_distribution domain  = account_analytic_distributionMapping.toDomain(account_analytic_distributiondto);
        domain .setId(account_analytic_distribution_id);
		account_analytic_distributionService.update(domain );
		Account_analytic_distributionDTO dto = account_analytic_distributionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_distributionService.getAccountAnalyticDistributionByEntities(this.account_analytic_distributionMapping.toDomain(#account_analytic_distributiondtos)),'iBizBusinessCentral-Account_analytic_distribution-Update')")
    @ApiOperation(value = "批量更新分析账户分配", tags = {"分析账户分配" },  notes = "批量更新分析账户分配")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_distributions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_distributionDTO> account_analytic_distributiondtos) {
        account_analytic_distributionService.updateBatch(account_analytic_distributionMapping.toDomain(account_analytic_distributiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_analytic_distributionService.get(#account_analytic_distribution_id),'iBizBusinessCentral-Account_analytic_distribution-Remove')")
    @ApiOperation(value = "删除分析账户分配", tags = {"分析账户分配" },  notes = "删除分析账户分配")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_distributions/{account_analytic_distribution_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_distribution_id") Long account_analytic_distribution_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_analytic_distributionService.remove(account_analytic_distribution_id));
    }

    @PreAuthorize("hasPermission(this.account_analytic_distributionService.getAccountAnalyticDistributionByIds(#ids),'iBizBusinessCentral-Account_analytic_distribution-Remove')")
    @ApiOperation(value = "批量删除分析账户分配", tags = {"分析账户分配" },  notes = "批量删除分析账户分配")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_distributions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_analytic_distributionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_analytic_distributionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_analytic_distribution-Get')")
    @ApiOperation(value = "获取分析账户分配", tags = {"分析账户分配" },  notes = "获取分析账户分配")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_distributions/{account_analytic_distribution_id}")
    public ResponseEntity<Account_analytic_distributionDTO> get(@PathVariable("account_analytic_distribution_id") Long account_analytic_distribution_id) {
        Account_analytic_distribution domain = account_analytic_distributionService.get(account_analytic_distribution_id);
        Account_analytic_distributionDTO dto = account_analytic_distributionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取分析账户分配草稿", tags = {"分析账户分配" },  notes = "获取分析账户分配草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_distributions/getdraft")
    public ResponseEntity<Account_analytic_distributionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_distributionMapping.toDto(account_analytic_distributionService.getDraft(new Account_analytic_distribution())));
    }

    @ApiOperation(value = "检查分析账户分配", tags = {"分析账户分配" },  notes = "检查分析账户分配")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_analytic_distributionDTO account_analytic_distributiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_analytic_distributionService.checkKey(account_analytic_distributionMapping.toDomain(account_analytic_distributiondto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_distributionMapping.toDomain(#account_analytic_distributiondto),'iBizBusinessCentral-Account_analytic_distribution-Save')")
    @ApiOperation(value = "保存分析账户分配", tags = {"分析账户分配" },  notes = "保存分析账户分配")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_analytic_distributionDTO account_analytic_distributiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_distributionService.save(account_analytic_distributionMapping.toDomain(account_analytic_distributiondto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_distributionMapping.toDomain(#account_analytic_distributiondtos),'iBizBusinessCentral-Account_analytic_distribution-Save')")
    @ApiOperation(value = "批量保存分析账户分配", tags = {"分析账户分配" },  notes = "批量保存分析账户分配")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_analytic_distributionDTO> account_analytic_distributiondtos) {
        account_analytic_distributionService.saveBatch(account_analytic_distributionMapping.toDomain(account_analytic_distributiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_distribution-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_distribution-Get')")
	@ApiOperation(value = "获取数据集", tags = {"分析账户分配" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_distributions/fetchdefault")
	public ResponseEntity<List<Account_analytic_distributionDTO>> fetchDefault(Account_analytic_distributionSearchContext context) {
        Page<Account_analytic_distribution> domains = account_analytic_distributionService.searchDefault(context) ;
        List<Account_analytic_distributionDTO> list = account_analytic_distributionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_distribution-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_distribution-Get')")
	@ApiOperation(value = "查询数据集", tags = {"分析账户分配" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_analytic_distributions/searchdefault")
	public ResponseEntity<Page<Account_analytic_distributionDTO>> searchDefault(@RequestBody Account_analytic_distributionSearchContext context) {
        Page<Account_analytic_distribution> domains = account_analytic_distributionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_analytic_distributionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


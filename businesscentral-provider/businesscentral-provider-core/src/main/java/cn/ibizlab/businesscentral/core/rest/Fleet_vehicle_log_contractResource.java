package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_contractService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆合同信息" })
@RestController("Core-fleet_vehicle_log_contract")
@RequestMapping("")
public class Fleet_vehicle_log_contractResource {

    @Autowired
    public IFleet_vehicle_log_contractService fleet_vehicle_log_contractService;

    @Autowired
    @Lazy
    public Fleet_vehicle_log_contractMapping fleet_vehicle_log_contractMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_contractMapping.toDomain(#fleet_vehicle_log_contractdto),'iBizBusinessCentral-Fleet_vehicle_log_contract-Create')")
    @ApiOperation(value = "新建车辆合同信息", tags = {"车辆合同信息" },  notes = "新建车辆合同信息")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts")
    public ResponseEntity<Fleet_vehicle_log_contractDTO> create(@Validated @RequestBody Fleet_vehicle_log_contractDTO fleet_vehicle_log_contractdto) {
        Fleet_vehicle_log_contract domain = fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdto);
		fleet_vehicle_log_contractService.create(domain);
        Fleet_vehicle_log_contractDTO dto = fleet_vehicle_log_contractMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_contractMapping.toDomain(#fleet_vehicle_log_contractdtos),'iBizBusinessCentral-Fleet_vehicle_log_contract-Create')")
    @ApiOperation(value = "批量新建车辆合同信息", tags = {"车辆合同信息" },  notes = "批量新建车辆合同信息")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_log_contractDTO> fleet_vehicle_log_contractdtos) {
        fleet_vehicle_log_contractService.createBatch(fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_log_contract" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_log_contractService.get(#fleet_vehicle_log_contract_id),'iBizBusinessCentral-Fleet_vehicle_log_contract-Update')")
    @ApiOperation(value = "更新车辆合同信息", tags = {"车辆合同信息" },  notes = "更新车辆合同信息")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_contracts/{fleet_vehicle_log_contract_id}")
    public ResponseEntity<Fleet_vehicle_log_contractDTO> update(@PathVariable("fleet_vehicle_log_contract_id") Long fleet_vehicle_log_contract_id, @RequestBody Fleet_vehicle_log_contractDTO fleet_vehicle_log_contractdto) {
		Fleet_vehicle_log_contract domain  = fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdto);
        domain .setId(fleet_vehicle_log_contract_id);
		fleet_vehicle_log_contractService.update(domain );
		Fleet_vehicle_log_contractDTO dto = fleet_vehicle_log_contractMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_contractService.getFleetVehicleLogContractByEntities(this.fleet_vehicle_log_contractMapping.toDomain(#fleet_vehicle_log_contractdtos)),'iBizBusinessCentral-Fleet_vehicle_log_contract-Update')")
    @ApiOperation(value = "批量更新车辆合同信息", tags = {"车辆合同信息" },  notes = "批量更新车辆合同信息")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_contracts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_log_contractDTO> fleet_vehicle_log_contractdtos) {
        fleet_vehicle_log_contractService.updateBatch(fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_contractService.get(#fleet_vehicle_log_contract_id),'iBizBusinessCentral-Fleet_vehicle_log_contract-Remove')")
    @ApiOperation(value = "删除车辆合同信息", tags = {"车辆合同信息" },  notes = "删除车辆合同信息")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_contracts/{fleet_vehicle_log_contract_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_log_contract_id") Long fleet_vehicle_log_contract_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_contractService.remove(fleet_vehicle_log_contract_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_contractService.getFleetVehicleLogContractByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_log_contract-Remove')")
    @ApiOperation(value = "批量删除车辆合同信息", tags = {"车辆合同信息" },  notes = "批量删除车辆合同信息")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_contracts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_log_contractService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_log_contractMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_log_contract-Get')")
    @ApiOperation(value = "获取车辆合同信息", tags = {"车辆合同信息" },  notes = "获取车辆合同信息")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_contracts/{fleet_vehicle_log_contract_id}")
    public ResponseEntity<Fleet_vehicle_log_contractDTO> get(@PathVariable("fleet_vehicle_log_contract_id") Long fleet_vehicle_log_contract_id) {
        Fleet_vehicle_log_contract domain = fleet_vehicle_log_contractService.get(fleet_vehicle_log_contract_id);
        Fleet_vehicle_log_contractDTO dto = fleet_vehicle_log_contractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆合同信息草稿", tags = {"车辆合同信息" },  notes = "获取车辆合同信息草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_contracts/getdraft")
    public ResponseEntity<Fleet_vehicle_log_contractDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_contractMapping.toDto(fleet_vehicle_log_contractService.getDraft(new Fleet_vehicle_log_contract())));
    }

    @ApiOperation(value = "检查车辆合同信息", tags = {"车辆合同信息" },  notes = "检查车辆合同信息")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_log_contractDTO fleet_vehicle_log_contractdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_contractService.checkKey(fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_contractMapping.toDomain(#fleet_vehicle_log_contractdto),'iBizBusinessCentral-Fleet_vehicle_log_contract-Save')")
    @ApiOperation(value = "保存车辆合同信息", tags = {"车辆合同信息" },  notes = "保存车辆合同信息")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_log_contractDTO fleet_vehicle_log_contractdto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_contractService.save(fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_log_contractMapping.toDomain(#fleet_vehicle_log_contractdtos),'iBizBusinessCentral-Fleet_vehicle_log_contract-Save')")
    @ApiOperation(value = "批量保存车辆合同信息", tags = {"车辆合同信息" },  notes = "批量保存车辆合同信息")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_log_contractDTO> fleet_vehicle_log_contractdtos) {
        fleet_vehicle_log_contractService.saveBatch(fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_log_contract-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_log_contract-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆合同信息" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_log_contracts/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_log_contractDTO>> fetchDefault(Fleet_vehicle_log_contractSearchContext context) {
        Page<Fleet_vehicle_log_contract> domains = fleet_vehicle_log_contractService.searchDefault(context) ;
        List<Fleet_vehicle_log_contractDTO> list = fleet_vehicle_log_contractMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_log_contract-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_log_contract-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆合同信息" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_log_contracts/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_log_contractDTO>> searchDefault(@RequestBody Fleet_vehicle_log_contractSearchContext context) {
        Page<Fleet_vehicle_log_contract> domains = fleet_vehicle_log_contractService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_log_contractMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


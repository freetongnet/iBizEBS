package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badge_user_wizardService;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"游戏化用户徽章向导" })
@RestController("Core-gamification_badge_user_wizard")
@RequestMapping("")
public class Gamification_badge_user_wizardResource {

    @Autowired
    public IGamification_badge_user_wizardService gamification_badge_user_wizardService;

    @Autowired
    @Lazy
    public Gamification_badge_user_wizardMapping gamification_badge_user_wizardMapping;

    @PreAuthorize("hasPermission(this.gamification_badge_user_wizardMapping.toDomain(#gamification_badge_user_wizarddto),'iBizBusinessCentral-Gamification_badge_user_wizard-Create')")
    @ApiOperation(value = "新建游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "新建游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards")
    public ResponseEntity<Gamification_badge_user_wizardDTO> create(@Validated @RequestBody Gamification_badge_user_wizardDTO gamification_badge_user_wizarddto) {
        Gamification_badge_user_wizard domain = gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddto);
		gamification_badge_user_wizardService.create(domain);
        Gamification_badge_user_wizardDTO dto = gamification_badge_user_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_badge_user_wizardMapping.toDomain(#gamification_badge_user_wizarddtos),'iBizBusinessCentral-Gamification_badge_user_wizard-Create')")
    @ApiOperation(value = "批量新建游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "批量新建游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_badge_user_wizardDTO> gamification_badge_user_wizarddtos) {
        gamification_badge_user_wizardService.createBatch(gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "gamification_badge_user_wizard" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.gamification_badge_user_wizardService.get(#gamification_badge_user_wizard_id),'iBizBusinessCentral-Gamification_badge_user_wizard-Update')")
    @ApiOperation(value = "更新游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "更新游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_user_wizards/{gamification_badge_user_wizard_id}")
    public ResponseEntity<Gamification_badge_user_wizardDTO> update(@PathVariable("gamification_badge_user_wizard_id") Long gamification_badge_user_wizard_id, @RequestBody Gamification_badge_user_wizardDTO gamification_badge_user_wizarddto) {
		Gamification_badge_user_wizard domain  = gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddto);
        domain .setId(gamification_badge_user_wizard_id);
		gamification_badge_user_wizardService.update(domain );
		Gamification_badge_user_wizardDTO dto = gamification_badge_user_wizardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_badge_user_wizardService.getGamificationBadgeUserWizardByEntities(this.gamification_badge_user_wizardMapping.toDomain(#gamification_badge_user_wizarddtos)),'iBizBusinessCentral-Gamification_badge_user_wizard-Update')")
    @ApiOperation(value = "批量更新游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "批量更新游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_user_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_badge_user_wizardDTO> gamification_badge_user_wizarddtos) {
        gamification_badge_user_wizardService.updateBatch(gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.gamification_badge_user_wizardService.get(#gamification_badge_user_wizard_id),'iBizBusinessCentral-Gamification_badge_user_wizard-Remove')")
    @ApiOperation(value = "删除游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "删除游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_user_wizards/{gamification_badge_user_wizard_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("gamification_badge_user_wizard_id") Long gamification_badge_user_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_badge_user_wizardService.remove(gamification_badge_user_wizard_id));
    }

    @PreAuthorize("hasPermission(this.gamification_badge_user_wizardService.getGamificationBadgeUserWizardByIds(#ids),'iBizBusinessCentral-Gamification_badge_user_wizard-Remove')")
    @ApiOperation(value = "批量删除游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "批量删除游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_user_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        gamification_badge_user_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.gamification_badge_user_wizardMapping.toDomain(returnObject.body),'iBizBusinessCentral-Gamification_badge_user_wizard-Get')")
    @ApiOperation(value = "获取游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "获取游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_user_wizards/{gamification_badge_user_wizard_id}")
    public ResponseEntity<Gamification_badge_user_wizardDTO> get(@PathVariable("gamification_badge_user_wizard_id") Long gamification_badge_user_wizard_id) {
        Gamification_badge_user_wizard domain = gamification_badge_user_wizardService.get(gamification_badge_user_wizard_id);
        Gamification_badge_user_wizardDTO dto = gamification_badge_user_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取游戏化用户徽章向导草稿", tags = {"游戏化用户徽章向导" },  notes = "获取游戏化用户徽章向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_user_wizards/getdraft")
    public ResponseEntity<Gamification_badge_user_wizardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_badge_user_wizardMapping.toDto(gamification_badge_user_wizardService.getDraft(new Gamification_badge_user_wizard())));
    }

    @ApiOperation(value = "检查游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "检查游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Gamification_badge_user_wizardDTO gamification_badge_user_wizarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(gamification_badge_user_wizardService.checkKey(gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.gamification_badge_user_wizardMapping.toDomain(#gamification_badge_user_wizarddto),'iBizBusinessCentral-Gamification_badge_user_wizard-Save')")
    @ApiOperation(value = "保存游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "保存游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/save")
    public ResponseEntity<Boolean> save(@RequestBody Gamification_badge_user_wizardDTO gamification_badge_user_wizarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_badge_user_wizardService.save(gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.gamification_badge_user_wizardMapping.toDomain(#gamification_badge_user_wizarddtos),'iBizBusinessCentral-Gamification_badge_user_wizard-Save')")
    @ApiOperation(value = "批量保存游戏化用户徽章向导", tags = {"游戏化用户徽章向导" },  notes = "批量保存游戏化用户徽章向导")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_user_wizards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Gamification_badge_user_wizardDTO> gamification_badge_user_wizarddtos) {
        gamification_badge_user_wizardService.saveBatch(gamification_badge_user_wizardMapping.toDomain(gamification_badge_user_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_badge_user_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_badge_user_wizard-Get')")
	@ApiOperation(value = "获取数据集", tags = {"游戏化用户徽章向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_badge_user_wizards/fetchdefault")
	public ResponseEntity<List<Gamification_badge_user_wizardDTO>> fetchDefault(Gamification_badge_user_wizardSearchContext context) {
        Page<Gamification_badge_user_wizard> domains = gamification_badge_user_wizardService.searchDefault(context) ;
        List<Gamification_badge_user_wizardDTO> list = gamification_badge_user_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_badge_user_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_badge_user_wizard-Get')")
	@ApiOperation(value = "查询数据集", tags = {"游戏化用户徽章向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/gamification_badge_user_wizards/searchdefault")
	public ResponseEntity<Page<Gamification_badge_user_wizardDTO>> searchDefault(@RequestBody Gamification_badge_user_wizardSearchContext context) {
        Page<Gamification_badge_user_wizard> domains = gamification_badge_user_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_badge_user_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_order_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"采购订单行" })
@RestController("Core-purchase_order_line")
@RequestMapping("")
public class Purchase_order_lineResource {

    @Autowired
    public IPurchase_order_lineService purchase_order_lineService;

    @Autowired
    @Lazy
    public Purchase_order_lineMapping purchase_order_lineMapping;

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "新建采购订单行", tags = {"采购订单行" },  notes = "新建采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines")
    public ResponseEntity<Purchase_order_lineDTO> create(@Validated @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
		purchase_order_lineService.create(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "批量新建采购订单行", tags = {"采购订单行" },  notes = "批量新建采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        purchase_order_lineService.createBatch(purchase_order_lineMapping.toDomain(purchase_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "更新采购订单行", tags = {"采购订单行" },  notes = "更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> update(@PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
		Purchase_order_line domain  = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain .setId(purchase_order_line_id);
		purchase_order_lineService.update(domain );
		Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByEntities(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos)),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "批量更新采购订单行", tags = {"采购订单行" },  notes = "批量更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_order_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        purchase_order_lineService.updateBatch(purchase_order_lineMapping.toDomain(purchase_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "删除采购订单行", tags = {"采购订单行" },  notes = "删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.remove(purchase_order_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByIds(#ids),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "批量删除采购订单行", tags = {"采购订单行" },  notes = "批量删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_order_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        purchase_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order_line-Get')")
    @ApiOperation(value = "获取采购订单行", tags = {"采购订单行" },  notes = "获取采购订单行")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> get(@PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
        Purchase_order_line domain = purchase_order_lineService.get(purchase_order_line_id);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取采购订单行草稿", tags = {"采购订单行" },  notes = "获取采购订单行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_order_lines/getdraft")
    public ResponseEntity<Purchase_order_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineMapping.toDto(purchase_order_lineService.getDraft(new Purchase_order_line())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_amount-all')")
    @ApiOperation(value = "计算订单金额", tags = {"采购订单行" },  notes = "计算订单金额")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/{purchase_order_line_id}/calc_amount")
    public ResponseEntity<Purchase_order_lineDTO> calc_amount(@PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setId(purchase_order_line_id);
        domain = purchase_order_lineService.calc_amount(domain);
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_price-all')")
    @ApiOperation(value = "计算金额", tags = {"采购订单行" },  notes = "计算金额")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/{purchase_order_line_id}/calc_price")
    public ResponseEntity<Purchase_order_lineDTO> calc_price(@PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setId(purchase_order_line_id);
        domain = purchase_order_lineService.calc_price(domain);
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @ApiOperation(value = "检查采购订单行", tags = {"采购订单行" },  notes = "检查采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.checkKey(purchase_order_lineMapping.toDomain(purchase_order_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Product_change-all')")
    @ApiOperation(value = "选择产品", tags = {"采购订单行" },  notes = "选择产品")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/{purchase_order_line_id}/product_change")
    public ResponseEntity<Purchase_order_lineDTO> product_change(@PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setId(purchase_order_line_id);
        domain = purchase_order_lineService.product_change(domain);
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "保存采购订单行", tags = {"采购订单行" },  notes = "保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.save(purchase_order_lineMapping.toDomain(purchase_order_linedto)));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "批量保存采购订单行", tags = {"采购订单行" },  notes = "批量保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_order_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        purchase_order_lineService.saveBatch(purchase_order_lineMapping.toDomain(purchase_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取calc_order_amount", tags = {"采购订单行" } ,notes = "获取calc_order_amount")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_order_lines/fetchcalc_order_amount")
	public ResponseEntity<List<HashMap>> fetchCalc_order_amount(Purchase_order_lineSearchContext context) {
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询calc_order_amount", tags = {"采购订单行" } ,notes = "查询calc_order_amount")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_order_lines/searchcalc_order_amount")
	public ResponseEntity<Page<HashMap>> searchCalc_order_amount(@RequestBody Purchase_order_lineSearchContext context) {
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"采购订单行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_order_lines/fetchdefault")
	public ResponseEntity<List<Purchase_order_lineDTO>> fetchDefault(Purchase_order_lineSearchContext context) {
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
        List<Purchase_order_lineDTO> list = purchase_order_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"采购订单行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_order_lines/searchdefault")
	public ResponseEntity<Page<Purchase_order_lineDTO>> searchDefault(@RequestBody Purchase_order_lineSearchContext context) {
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据产品建立采购订单行", tags = {"采购订单行" },  notes = "根据产品建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_order_lines")
    public ResponseEntity<Purchase_order_lineDTO> createByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
		purchase_order_lineService.create(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据产品批量建立采购订单行", tags = {"采购订单行" },  notes = "根据产品批量建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> createBatchByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setProductId(product_product_id);
        }
        purchase_order_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据产品更新采购订单行", tags = {"采购订单行" },  notes = "根据产品更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> updateByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        domain.setId(purchase_order_line_id);
		purchase_order_lineService.update(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByEntities(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos)),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据产品批量更新采购订单行", tags = {"采购订单行" },  notes = "根据产品批量更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> updateBatchByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setProductId(product_product_id);
        }
        purchase_order_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据产品删除采购订单行", tags = {"采购订单行" },  notes = "根据产品删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Boolean> removeByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.remove(purchase_order_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByIds(#ids),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据产品批量删除采购订单行", tags = {"采购订单行" },  notes = "根据产品批量删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> removeBatchByProduct_product(@RequestBody List<Long> ids) {
        purchase_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order_line-Get')")
    @ApiOperation(value = "根据产品获取采购订单行", tags = {"采购订单行" },  notes = "根据产品获取采购订单行")
	@RequestMapping(method = RequestMethod.GET, value = "/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> getByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
        Purchase_order_line domain = purchase_order_lineService.get(purchase_order_line_id);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品获取采购订单行草稿", tags = {"采购订单行" },  notes = "根据产品获取采购订单行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/product_products/{product_product_id}/purchase_order_lines/getdraft")
    public ResponseEntity<Purchase_order_lineDTO> getDraftByProduct_product(@PathVariable("product_product_id") Long product_product_id) {
        Purchase_order_line domain = new Purchase_order_line();
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineMapping.toDto(purchase_order_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_amount-all')")
    @ApiOperation(value = "根据产品采购订单行", tags = {"采购订单行" },  notes = "根据产品采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}/calc_amount")
    public ResponseEntity<Purchase_order_lineDTO> calc_amountByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_order_lineService.calc_amount(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_price-all')")
    @ApiOperation(value = "根据产品采购订单行", tags = {"采购订单行" },  notes = "根据产品采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}/calc_price")
    public ResponseEntity<Purchase_order_lineDTO> calc_priceByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_order_lineService.calc_price(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @ApiOperation(value = "根据产品检查采购订单行", tags = {"采购订单行" },  notes = "根据产品检查采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.checkKey(purchase_order_lineMapping.toDomain(purchase_order_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Product_change-all')")
    @ApiOperation(value = "根据产品采购订单行", tags = {"采购订单行" },  notes = "根据产品采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}/product_change")
    public ResponseEntity<Purchase_order_lineDTO> product_changeByProduct_product(@PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_order_lineService.product_change(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据产品保存采购订单行", tags = {"采购订单行" },  notes = "根据产品保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_order_lines/save")
    public ResponseEntity<Boolean> saveByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据产品批量保存采购订单行", tags = {"采购订单行" },  notes = "根据产品批量保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/{product_product_id}/purchase_order_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
             domain.setProductId(product_product_id);
        }
        purchase_order_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "根据产品获取calc_order_amount", tags = {"采购订单行" } ,notes = "根据产品获取calc_order_amount")
    @RequestMapping(method= RequestMethod.GET , value="/product_products/{product_product_id}/purchase_order_lines/fetchcalc_order_amount")
	public ResponseEntity<List<HashMap>> fetchPurchase_order_lineCalc_order_amountByProduct_product(@PathVariable("product_product_id") Long product_product_id,Purchase_order_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据产品查询calc_order_amount", tags = {"采购订单行" } ,notes = "根据产品查询calc_order_amount")
    @RequestMapping(method= RequestMethod.POST , value="/product_products/{product_product_id}/purchase_order_lines/searchcalc_order_amount")
	public ResponseEntity<Page<HashMap>> searchPurchase_order_lineCalc_order_amountByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据产品获取数据集", tags = {"采购订单行" } ,notes = "根据产品获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_products/{product_product_id}/purchase_order_lines/fetchdefault")
	public ResponseEntity<List<Purchase_order_lineDTO>> fetchPurchase_order_lineDefaultByProduct_product(@PathVariable("product_product_id") Long product_product_id,Purchase_order_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
        List<Purchase_order_lineDTO> list = purchase_order_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据产品查询数据集", tags = {"采购订单行" } ,notes = "根据产品查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_products/{product_product_id}/purchase_order_lines/searchdefault")
	public ResponseEntity<Page<Purchase_order_lineDTO>> searchPurchase_order_lineDefaultByProduct_product(@PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据采购订单建立采购订单行", tags = {"采购订单行" },  notes = "根据采购订单建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines")
    public ResponseEntity<Purchase_order_lineDTO> createByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
		purchase_order_lineService.create(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据采购订单批量建立采购订单行", tags = {"采购订单行" },  notes = "根据采购订单批量建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> createBatchByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据采购订单更新采购订单行", tags = {"采购订单行" },  notes = "根据采购订单更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> updateByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain.setId(purchase_order_line_id);
		purchase_order_lineService.update(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByEntities(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos)),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据采购订单批量更新采购订单行", tags = {"采购订单行" },  notes = "根据采购订单批量更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> updateBatchByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据采购订单删除采购订单行", tags = {"采购订单行" },  notes = "根据采购订单删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Boolean> removeByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.remove(purchase_order_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByIds(#ids),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据采购订单批量删除采购订单行", tags = {"采购订单行" },  notes = "根据采购订单批量删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> removeBatchByPurchase_order(@RequestBody List<Long> ids) {
        purchase_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order_line-Get')")
    @ApiOperation(value = "根据采购订单获取采购订单行", tags = {"采购订单行" },  notes = "根据采购订单获取采购订单行")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> getByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
        Purchase_order_line domain = purchase_order_lineService.get(purchase_order_line_id);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据采购订单获取采购订单行草稿", tags = {"采购订单行" },  notes = "根据采购订单获取采购订单行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/getdraft")
    public ResponseEntity<Purchase_order_lineDTO> getDraftByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id) {
        Purchase_order_line domain = new Purchase_order_line();
        domain.setOrderId(purchase_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineMapping.toDto(purchase_order_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_amount-all')")
    @ApiOperation(value = "根据采购订单采购订单行", tags = {"采购订单行" },  notes = "根据采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/calc_amount")
    public ResponseEntity<Purchase_order_lineDTO> calc_amountByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.calc_amount(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_price-all')")
    @ApiOperation(value = "根据采购订单采购订单行", tags = {"采购订单行" },  notes = "根据采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/calc_price")
    public ResponseEntity<Purchase_order_lineDTO> calc_priceByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.calc_price(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @ApiOperation(value = "根据采购订单检查采购订单行", tags = {"采购订单行" },  notes = "根据采购订单检查采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.checkKey(purchase_order_lineMapping.toDomain(purchase_order_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Product_change-all')")
    @ApiOperation(value = "根据采购订单采购订单行", tags = {"采购订单行" },  notes = "根据采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/product_change")
    public ResponseEntity<Purchase_order_lineDTO> product_changeByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.product_change(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据采购订单保存采购订单行", tags = {"采购订单行" },  notes = "根据采购订单保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/save")
    public ResponseEntity<Boolean> saveByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据采购订单批量保存采购订单行", tags = {"采购订单行" },  notes = "根据采购订单批量保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{purchase_order_id}/purchase_order_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
             domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "根据采购订单获取calc_order_amount", tags = {"采购订单行" } ,notes = "根据采购订单获取calc_order_amount")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_orders/{purchase_order_id}/purchase_order_lines/fetchcalc_order_amount")
	public ResponseEntity<List<HashMap>> fetchPurchase_order_lineCalc_order_amountByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id,Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据采购订单查询calc_order_amount", tags = {"采购订单行" } ,notes = "根据采购订单查询calc_order_amount")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_orders/{purchase_order_id}/purchase_order_lines/searchcalc_order_amount")
	public ResponseEntity<Page<HashMap>> searchPurchase_order_lineCalc_order_amountByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据采购订单获取数据集", tags = {"采购订单行" } ,notes = "根据采购订单获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_orders/{purchase_order_id}/purchase_order_lines/fetchdefault")
	public ResponseEntity<List<Purchase_order_lineDTO>> fetchPurchase_order_lineDefaultByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id,Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
        List<Purchase_order_lineDTO> list = purchase_order_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据采购订单查询数据集", tags = {"采购订单行" } ,notes = "根据采购订单查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_orders/{purchase_order_id}/purchase_order_lines/searchdefault")
	public ResponseEntity<Page<Purchase_order_lineDTO>> searchPurchase_order_lineDefaultByPurchase_order(@PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据产品模板产品建立采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines")
    public ResponseEntity<Purchase_order_lineDTO> createByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
		purchase_order_lineService.create(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据产品模板产品批量建立采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品批量建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> createBatchByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setProductId(product_product_id);
        }
        purchase_order_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据产品模板产品更新采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> updateByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        domain.setId(purchase_order_line_id);
		purchase_order_lineService.update(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByEntities(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos)),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据产品模板产品批量更新采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品批量更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> updateBatchByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setProductId(product_product_id);
        }
        purchase_order_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据产品模板产品删除采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Boolean> removeByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.remove(purchase_order_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByIds(#ids),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据产品模板产品批量删除采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品批量删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> removeBatchByProduct_templateProduct_product(@RequestBody List<Long> ids) {
        purchase_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order_line-Get')")
    @ApiOperation(value = "根据产品模板产品获取采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品获取采购订单行")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> getByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
        Purchase_order_line domain = purchase_order_lineService.get(purchase_order_line_id);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品模板产品获取采购订单行草稿", tags = {"采购订单行" },  notes = "根据产品模板产品获取采购订单行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/getdraft")
    public ResponseEntity<Purchase_order_lineDTO> getDraftByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id) {
        Purchase_order_line domain = new Purchase_order_line();
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineMapping.toDto(purchase_order_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_amount-all')")
    @ApiOperation(value = "根据产品模板产品采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}/calc_amount")
    public ResponseEntity<Purchase_order_lineDTO> calc_amountByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_order_lineService.calc_amount(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_price-all')")
    @ApiOperation(value = "根据产品模板产品采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}/calc_price")
    public ResponseEntity<Purchase_order_lineDTO> calc_priceByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_order_lineService.calc_price(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @ApiOperation(value = "根据产品模板产品检查采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品检查采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.checkKey(purchase_order_lineMapping.toDomain(purchase_order_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Product_change-all')")
    @ApiOperation(value = "根据产品模板产品采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/{purchase_order_line_id}/product_change")
    public ResponseEntity<Purchase_order_lineDTO> product_changeByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        domain = purchase_order_lineService.product_change(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据产品模板产品保存采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/save")
    public ResponseEntity<Boolean> saveByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setProductId(product_product_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据产品模板产品批量保存采购订单行", tags = {"采购订单行" },  notes = "根据产品模板产品批量保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
             domain.setProductId(product_product_id);
        }
        purchase_order_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "根据产品模板产品获取calc_order_amount", tags = {"采购订单行" } ,notes = "根据产品模板产品获取calc_order_amount")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/fetchcalc_order_amount")
	public ResponseEntity<List<HashMap>> fetchPurchase_order_lineCalc_order_amountByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id,Purchase_order_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据产品模板产品查询calc_order_amount", tags = {"采购订单行" } ,notes = "根据产品模板产品查询calc_order_amount")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/searchcalc_order_amount")
	public ResponseEntity<Page<HashMap>> searchPurchase_order_lineCalc_order_amountByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据产品模板产品获取数据集", tags = {"采购订单行" } ,notes = "根据产品模板产品获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/fetchdefault")
	public ResponseEntity<List<Purchase_order_lineDTO>> fetchPurchase_order_lineDefaultByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id,Purchase_order_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
        List<Purchase_order_lineDTO> list = purchase_order_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据产品模板产品查询数据集", tags = {"采购订单行" } ,notes = "根据产品模板产品查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/{product_template_id}/product_products/{product_product_id}/purchase_order_lines/searchdefault")
	public ResponseEntity<Page<Purchase_order_lineDTO>> searchPurchase_order_lineDefaultByProduct_templateProduct_product(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_product_id_eq(product_product_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据采购申请采购订单建立采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines")
    public ResponseEntity<Purchase_order_lineDTO> createByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
		purchase_order_lineService.create(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据采购申请采购订单批量建立采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单批量建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> createBatchByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据采购申请采购订单更新采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> updateByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain.setId(purchase_order_line_id);
		purchase_order_lineService.update(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByEntities(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos)),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据采购申请采购订单批量更新采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单批量更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> updateBatchByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据采购申请采购订单删除采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Boolean> removeByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.remove(purchase_order_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByIds(#ids),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据采购申请采购订单批量删除采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单批量删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> removeBatchByPurchase_requisitionPurchase_order(@RequestBody List<Long> ids) {
        purchase_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order_line-Get')")
    @ApiOperation(value = "根据采购申请采购订单获取采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单获取采购订单行")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> getByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
        Purchase_order_line domain = purchase_order_lineService.get(purchase_order_line_id);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据采购申请采购订单获取采购订单行草稿", tags = {"采购订单行" },  notes = "根据采购申请采购订单获取采购订单行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/getdraft")
    public ResponseEntity<Purchase_order_lineDTO> getDraftByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id) {
        Purchase_order_line domain = new Purchase_order_line();
        domain.setOrderId(purchase_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineMapping.toDto(purchase_order_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_amount-all')")
    @ApiOperation(value = "根据采购申请采购订单采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/calc_amount")
    public ResponseEntity<Purchase_order_lineDTO> calc_amountByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.calc_amount(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_price-all')")
    @ApiOperation(value = "根据采购申请采购订单采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/calc_price")
    public ResponseEntity<Purchase_order_lineDTO> calc_priceByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.calc_price(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @ApiOperation(value = "根据采购申请采购订单检查采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单检查采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.checkKey(purchase_order_lineMapping.toDomain(purchase_order_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Product_change-all')")
    @ApiOperation(value = "根据采购申请采购订单采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/product_change")
    public ResponseEntity<Purchase_order_lineDTO> product_changeByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.product_change(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据采购申请采购订单保存采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/save")
    public ResponseEntity<Boolean> saveByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据采购申请采购订单批量保存采购订单行", tags = {"采购订单行" },  notes = "根据采购申请采购订单批量保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
             domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "根据采购申请采购订单获取calc_order_amount", tags = {"采购订单行" } ,notes = "根据采购申请采购订单获取calc_order_amount")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/fetchcalc_order_amount")
	public ResponseEntity<List<HashMap>> fetchPurchase_order_lineCalc_order_amountByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id,Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据采购申请采购订单查询calc_order_amount", tags = {"采购订单行" } ,notes = "根据采购申请采购订单查询calc_order_amount")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/searchcalc_order_amount")
	public ResponseEntity<Page<HashMap>> searchPurchase_order_lineCalc_order_amountByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据采购申请采购订单获取数据集", tags = {"采购订单行" } ,notes = "根据采购申请采购订单获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/fetchdefault")
	public ResponseEntity<List<Purchase_order_lineDTO>> fetchPurchase_order_lineDefaultByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id,Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
        List<Purchase_order_lineDTO> list = purchase_order_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据采购申请采购订单查询数据集", tags = {"采购订单行" } ,notes = "根据采购申请采购订单查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/searchdefault")
	public ResponseEntity<Page<Purchase_order_lineDTO>> searchPurchase_order_lineDefaultByPurchase_requisitionPurchase_order(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据供应商采购订单建立采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines")
    public ResponseEntity<Purchase_order_lineDTO> createByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
		purchase_order_lineService.create(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据供应商采购订单批量建立采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单批量建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> createBatchByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据供应商采购订单更新采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> updateByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain.setId(purchase_order_line_id);
		purchase_order_lineService.update(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByEntities(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos)),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据供应商采购订单批量更新采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单批量更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> updateBatchByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据供应商采购订单删除采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Boolean> removeByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.remove(purchase_order_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByIds(#ids),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据供应商采购订单批量删除采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单批量删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> removeBatchByRes_supplierPurchase_order(@RequestBody List<Long> ids) {
        purchase_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order_line-Get')")
    @ApiOperation(value = "根据供应商采购订单获取采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单获取采购订单行")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> getByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
        Purchase_order_line domain = purchase_order_lineService.get(purchase_order_line_id);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据供应商采购订单获取采购订单行草稿", tags = {"采购订单行" },  notes = "根据供应商采购订单获取采购订单行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/getdraft")
    public ResponseEntity<Purchase_order_lineDTO> getDraftByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id) {
        Purchase_order_line domain = new Purchase_order_line();
        domain.setOrderId(purchase_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineMapping.toDto(purchase_order_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_amount-all')")
    @ApiOperation(value = "根据供应商采购订单采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/calc_amount")
    public ResponseEntity<Purchase_order_lineDTO> calc_amountByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.calc_amount(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_price-all')")
    @ApiOperation(value = "根据供应商采购订单采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/calc_price")
    public ResponseEntity<Purchase_order_lineDTO> calc_priceByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.calc_price(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @ApiOperation(value = "根据供应商采购订单检查采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单检查采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.checkKey(purchase_order_lineMapping.toDomain(purchase_order_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Product_change-all')")
    @ApiOperation(value = "根据供应商采购订单采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/product_change")
    public ResponseEntity<Purchase_order_lineDTO> product_changeByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.product_change(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据供应商采购订单保存采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/save")
    public ResponseEntity<Boolean> saveByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据供应商采购订单批量保存采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购订单批量保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
             domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "根据供应商采购订单获取calc_order_amount", tags = {"采购订单行" } ,notes = "根据供应商采购订单获取calc_order_amount")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/fetchcalc_order_amount")
	public ResponseEntity<List<HashMap>> fetchPurchase_order_lineCalc_order_amountByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id,Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据供应商采购订单查询calc_order_amount", tags = {"采购订单行" } ,notes = "根据供应商采购订单查询calc_order_amount")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/searchcalc_order_amount")
	public ResponseEntity<Page<HashMap>> searchPurchase_order_lineCalc_order_amountByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据供应商采购订单获取数据集", tags = {"采购订单行" } ,notes = "根据供应商采购订单获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/fetchdefault")
	public ResponseEntity<List<Purchase_order_lineDTO>> fetchPurchase_order_lineDefaultByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id,Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
        List<Purchase_order_lineDTO> list = purchase_order_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据供应商采购订单查询数据集", tags = {"采购订单行" } ,notes = "根据供应商采购订单查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/searchdefault")
	public ResponseEntity<Page<Purchase_order_lineDTO>> searchPurchase_order_lineDefaultByRes_supplierPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据供应商采购申请采购订单建立采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines")
    public ResponseEntity<Purchase_order_lineDTO> createByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
		purchase_order_lineService.create(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Create')")
    @ApiOperation(value = "根据供应商采购申请采购订单批量建立采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单批量建立采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> createBatchByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "purchase_order_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据供应商采购申请采购订单更新采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> updateByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain.setId(purchase_order_line_id);
		purchase_order_lineService.update(domain);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByEntities(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos)),'iBizBusinessCentral-Purchase_order_line-Update')")
    @ApiOperation(value = "根据供应商采购申请采购订单批量更新采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单批量更新采购订单行")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> updateBatchByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
            domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.get(#purchase_order_line_id),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据供应商采购申请采购订单删除采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Boolean> removeByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.remove(purchase_order_line_id));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineService.getPurchaseOrderLineByIds(#ids),'iBizBusinessCentral-Purchase_order_line-Remove')")
    @ApiOperation(value = "根据供应商采购申请采购订单批量删除采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单批量删除采购订单行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/batch")
    public ResponseEntity<Boolean> removeBatchByRes_supplierPurchase_requisitionPurchase_order(@RequestBody List<Long> ids) {
        purchase_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_order_line-Get')")
    @ApiOperation(value = "根据供应商采购申请采购订单获取采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单获取采购订单行")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}")
    public ResponseEntity<Purchase_order_lineDTO> getByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id) {
        Purchase_order_line domain = purchase_order_lineService.get(purchase_order_line_id);
        Purchase_order_lineDTO dto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据供应商采购申请采购订单获取采购订单行草稿", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单获取采购订单行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/getdraft")
    public ResponseEntity<Purchase_order_lineDTO> getDraftByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id) {
        Purchase_order_line domain = new Purchase_order_line();
        domain.setOrderId(purchase_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineMapping.toDto(purchase_order_lineService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_amount-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/calc_amount")
    public ResponseEntity<Purchase_order_lineDTO> calc_amountByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.calc_amount(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Calc_price-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/calc_price")
    public ResponseEntity<Purchase_order_lineDTO> calc_priceByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.calc_price(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @ApiOperation(value = "根据供应商采购申请采购订单检查采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单检查采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.checkKey(purchase_order_lineMapping.toDomain(purchase_order_linedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-Product_change-all')")
    @ApiOperation(value = "根据供应商采购申请采购订单采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/{purchase_order_line_id}/product_change")
    public ResponseEntity<Purchase_order_lineDTO> product_changeByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @PathVariable("purchase_order_line_id") Long purchase_order_line_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        domain = purchase_order_lineService.product_change(domain) ;
        purchase_order_linedto = purchase_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_linedto);
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedto),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据供应商采购申请采购订单保存采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/save")
    public ResponseEntity<Boolean> saveByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineDTO purchase_order_linedto) {
        Purchase_order_line domain = purchase_order_lineMapping.toDomain(purchase_order_linedto);
        domain.setOrderId(purchase_order_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_order_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_order_lineMapping.toDomain(#purchase_order_linedtos),'iBizBusinessCentral-Purchase_order_line-Save')")
    @ApiOperation(value = "根据供应商采购申请采购订单批量保存采购订单行", tags = {"采购订单行" },  notes = "根据供应商采购申请采购订单批量保存采购订单行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody List<Purchase_order_lineDTO> purchase_order_linedtos) {
        List<Purchase_order_line> domainlist=purchase_order_lineMapping.toDomain(purchase_order_linedtos);
        for(Purchase_order_line domain:domainlist){
             domain.setOrderId(purchase_order_id);
        }
        purchase_order_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "根据供应商采购申请采购订单获取calc_order_amount", tags = {"采购订单行" } ,notes = "根据供应商采购申请采购订单获取calc_order_amount")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/fetchcalc_order_amount")
	public ResponseEntity<List<HashMap>> fetchPurchase_order_lineCalc_order_amountByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id,Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据供应商采购申请采购订单查询calc_order_amount", tags = {"采购订单行" } ,notes = "根据供应商采购申请采购订单查询calc_order_amount")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/searchcalc_order_amount")
	public ResponseEntity<Page<HashMap>> searchPurchase_order_lineCalc_order_amountByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<HashMap> domains = purchase_order_lineService.searchCalc_order_amount(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据供应商采购申请采购订单获取数据集", tags = {"采购订单行" } ,notes = "根据供应商采购申请采购订单获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/fetchdefault")
	public ResponseEntity<List<Purchase_order_lineDTO>> fetchPurchase_order_lineDefaultByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id,Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
        List<Purchase_order_lineDTO> list = purchase_order_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_order_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_order_line-Get')")
	@ApiOperation(value = "根据供应商采购申请采购订单查询数据集", tags = {"采购订单行" } ,notes = "根据供应商采购申请采购订单查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/purchase_orders/{purchase_order_id}/purchase_order_lines/searchdefault")
	public ResponseEntity<Page<Purchase_order_lineDTO>> searchPurchase_order_lineDefaultByRes_supplierPurchase_requisitionPurchase_order(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @PathVariable("purchase_order_id") Long purchase_order_id, @RequestBody Purchase_order_lineSearchContext context) {
        context.setN_order_id_eq(purchase_order_id);
        Page<Purchase_order_line> domains = purchase_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Crm_activity_reportDTO]
 */
@Data
public class Crm_activity_reportDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [PROBABILITY]
     *
     */
    @JSONField(name = "probability")
    @JsonProperty("probability")
    private Double probability;

    /**
     * 属性 [LEAD_TYPE]
     *
     */
    @JSONField(name = "lead_type")
    @JsonProperty("lead_type")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String leadType;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String subject;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamIdText;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "mail_activity_type_id_text")
    @JsonProperty("mail_activity_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mailActivityTypeIdText;

    /**
     * 属性 [LEAD_ID_TEXT]
     *
     */
    @JSONField(name = "lead_id_text")
    @JsonProperty("lead_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String leadIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [SUBTYPE_ID_TEXT]
     *
     */
    @JSONField(name = "subtype_id_text")
    @JsonProperty("subtype_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String subtypeIdText;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String countryIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stageIdText;

    /**
     * 属性 [AUTHOR_ID_TEXT]
     *
     */
    @JSONField(name = "author_id_text")
    @JsonProperty("author_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String authorIdText;

    /**
     * 属性 [AUTHOR_ID]
     *
     */
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long authorId;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countryId;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stageId;

    /**
     * 属性 [SUBTYPE_ID]
     *
     */
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long subtypeId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [LEAD_ID]
     *
     */
    @JSONField(name = "lead_id")
    @JsonProperty("lead_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long leadId;

    /**
     * 属性 [MAIL_ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long mailActivityTypeId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teamId;


    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [PROBABILITY]
     */
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.modify("probability",probability);
    }

    /**
     * 设置 [LEAD_TYPE]
     */
    public void setLeadType(String  leadType){
        this.leadType = leadType ;
        this.modify("lead_type",leadType);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [SUBJECT]
     */
    public void setSubject(String  subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [AUTHOR_ID]
     */
    public void setAuthorId(Long  authorId){
        this.authorId = authorId ;
        this.modify("author_id",authorId);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Long  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [STAGE_ID]
     */
    public void setStageId(Long  stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }

    /**
     * 设置 [SUBTYPE_ID]
     */
    public void setSubtypeId(Long  subtypeId){
        this.subtypeId = subtypeId ;
        this.modify("subtype_id",subtypeId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [LEAD_ID]
     */
    public void setLeadId(Long  leadId){
        this.leadId = leadId ;
        this.modify("lead_id",leadId);
    }

    /**
     * 设置 [MAIL_ACTIVITY_TYPE_ID]
     */
    public void setMailActivityTypeId(Long  mailActivityTypeId){
        this.mailActivityTypeId = mailActivityTypeId ;
        this.modify("mail_activity_type_id",mailActivityTypeId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [TEAM_ID]
     */
    public void setTeamId(Long  teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }


}



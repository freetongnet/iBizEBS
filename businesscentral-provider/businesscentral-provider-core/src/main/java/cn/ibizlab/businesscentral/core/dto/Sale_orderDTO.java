package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Sale_orderDTO]
 */
@Data
public class Sale_orderDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DATE_ORDER]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_order" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_order")
    @NotNull(message = "[单据日期]不允许为空!")
    private Timestamp dateOrder;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceIds;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [AMOUNT_UNTAXED]
     *
     */
    @JSONField(name = "amount_untaxed")
    @JsonProperty("amount_untaxed")
    private BigDecimal amountUntaxed;

    /**
     * 属性 [VALIDITY_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validity_date" , format="yyyy-MM-dd")
    @JsonProperty("validity_date")
    private Timestamp validityDate;

    /**
     * 属性 [AMOUNT_UNDISCOUNTED]
     *
     */
    @JSONField(name = "amount_undiscounted")
    @JsonProperty("amount_undiscounted")
    private Double amountUndiscounted;

    /**
     * 属性 [INVOICE_COUNT]
     *
     */
    @JSONField(name = "invoice_count")
    @JsonProperty("invoice_count")
    private Integer invoiceCount;

    /**
     * 属性 [ACCESS_WARNING]
     *
     */
    @JSONField(name = "access_warning")
    @JsonProperty("access_warning")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String accessWarning;

    /**
     * 属性 [WARNING_STOCK]
     *
     */
    @JSONField(name = "warning_stock")
    @JsonProperty("warning_stock")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String warningStock;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [EFFECTIVE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effective_date" , format="yyyy-MM-dd")
    @JsonProperty("effective_date")
    private Timestamp effectiveDate;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [REQUIRE_SIGNATURE]
     *
     */
    @JSONField(name = "require_signature")
    @JsonProperty("require_signature")
    private Boolean requireSignature;

    /**
     * 属性 [CURRENCY_RATE]
     *
     */
    @JSONField(name = "currency_rate")
    @JsonProperty("currency_rate")
    private Double currencyRate;

    /**
     * 属性 [PICKING_POLICY]
     *
     */
    @JSONField(name = "picking_policy")
    @JsonProperty("picking_policy")
    @NotBlank(message = "[送货策略]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pickingPolicy;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [SIGNATURE]
     *
     */
    @JSONField(name = "signature")
    @JsonProperty("signature")
    private byte[] signature;

    /**
     * 属性 [PROCUREMENT_GROUP_ID]
     *
     */
    @JSONField(name = "procurement_group_id")
    @JsonProperty("procurement_group_id")
    private Integer procurementGroupId;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [CART_QUANTITY]
     *
     */
    @JSONField(name = "cart_quantity")
    @JsonProperty("cart_quantity")
    private Integer cartQuantity;

    /**
     * 属性 [TYPE_NAME]
     *
     */
    @JSONField(name = "type_name")
    @JsonProperty("type_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String typeName;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [DELIVERY_COUNT]
     *
     */
    @JSONField(name = "delivery_count")
    @JsonProperty("delivery_count")
    private Integer deliveryCount;

    /**
     * 属性 [SIGNED_BY]
     *
     */
    @JSONField(name = "signed_by")
    @JsonProperty("signed_by")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String signedBy;

    /**
     * 属性 [ORDER_LINE]
     *
     */
    @JSONField(name = "order_line")
    @JsonProperty("order_line")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String orderLine;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [REQUIRE_PAYMENT]
     *
     */
    @JSONField(name = "require_payment")
    @JsonProperty("require_payment")
    private Boolean requirePayment;

    /**
     * 属性 [ONLY_SERVICES]
     *
     */
    @JSONField(name = "only_services")
    @JsonProperty("only_services")
    private Boolean onlyServices;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [WEBSITE_ORDER_LINE]
     *
     */
    @JSONField(name = "website_order_line")
    @JsonProperty("website_order_line")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteOrderLine;

    /**
     * 属性 [REFERENCE]
     *
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String reference;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [INVOICE_STATUS]
     *
     */
    @JSONField(name = "invoice_status")
    @JsonProperty("invoice_status")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String invoiceStatus;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String tagIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [TRANSACTION_IDS]
     *
     */
    @JSONField(name = "transaction_ids")
    @JsonProperty("transaction_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String transactionIds;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [IS_ABANDONED_CART]
     *
     */
    @JSONField(name = "is_abandoned_cart")
    @JsonProperty("is_abandoned_cart")
    private Boolean isAbandonedCart;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [EXPENSE_COUNT]
     *
     */
    @JSONField(name = "expense_count")
    @JsonProperty("expense_count")
    private Integer expenseCount;

    /**
     * 属性 [AUTHORIZED_TRANSACTION_IDS]
     *
     */
    @JSONField(name = "authorized_transaction_ids")
    @JsonProperty("authorized_transaction_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String authorizedTransactionIds;

    /**
     * 属性 [COMMITMENT_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "commitment_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("commitment_date")
    private Timestamp commitmentDate;

    /**
     * 属性 [IS_EXPIRED]
     *
     */
    @JSONField(name = "is_expired")
    @JsonProperty("is_expired")
    private Boolean isExpired;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [REMAINING_VALIDITY_DAYS]
     *
     */
    @JSONField(name = "remaining_validity_days")
    @JsonProperty("remaining_validity_days")
    private Integer remainingValidityDays;

    /**
     * 属性 [EXPECTED_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expected_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expected_date")
    private Timestamp expectedDate;

    /**
     * 属性 [PURCHASE_ORDER_COUNT]
     *
     */
    @JSONField(name = "purchase_order_count")
    @JsonProperty("purchase_order_count")
    private Integer purchaseOrderCount;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [ACCESS_URL]
     *
     */
    @JSONField(name = "access_url")
    @JsonProperty("access_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String accessUrl;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[订单关联]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [SALE_ORDER_OPTION_IDS]
     *
     */
    @JSONField(name = "sale_order_option_ids")
    @JsonProperty("sale_order_option_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String saleOrderOptionIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String origin;

    /**
     * 属性 [CONFIRMATION_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "confirmation_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("confirmation_date")
    private Timestamp confirmationDate;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String note;

    /**
     * 属性 [AMOUNT_BY_GROUP]
     *
     */
    @JSONField(name = "amount_by_group")
    @JsonProperty("amount_by_group")
    private byte[] amountByGroup;

    /**
     * 属性 [PICKING_IDS]
     *
     */
    @JSONField(name = "picking_ids")
    @JsonProperty("picking_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String pickingIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String accessToken;

    /**
     * 属性 [AMOUNT_TOTAL]
     *
     */
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private BigDecimal amountTotal;

    /**
     * 属性 [CLIENT_ORDER_REF]
     *
     */
    @JSONField(name = "client_order_ref")
    @JsonProperty("client_order_ref")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String clientOrderRef;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [CART_RECOVERY_EMAIL_SENT]
     *
     */
    @JSONField(name = "cart_recovery_email_sent")
    @JsonProperty("cart_recovery_email_sent")
    private Boolean cartRecoveryEmailSent;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [AMOUNT_TAX]
     *
     */
    @JSONField(name = "amount_tax")
    @JsonProperty("amount_tax")
    private BigDecimal amountTax;

    /**
     * 属性 [EXPENSE_IDS]
     *
     */
    @JSONField(name = "expense_ids")
    @JsonProperty("expense_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String expenseIds;

    /**
     * 属性 [PARTNER_INVOICE_ID_TEXT]
     *
     */
    @JSONField(name = "partner_invoice_id_text")
    @JsonProperty("partner_invoice_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerInvoiceIdText;

    /**
     * 属性 [INCOTERM_TEXT]
     *
     */
    @JSONField(name = "incoterm_text")
    @JsonProperty("incoterm_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String incotermText;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamIdText;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String analyticAccountIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String campaignIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [PARTNER_SHIPPING_ID_TEXT]
     *
     */
    @JSONField(name = "partner_shipping_id_text")
    @JsonProperty("partner_shipping_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerShippingIdText;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String warehouseIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [OPPORTUNITY_ID_TEXT]
     *
     */
    @JSONField(name = "opportunity_id_text")
    @JsonProperty("opportunity_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String opportunityIdText;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sourceIdText;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mediumIdText;

    /**
     * 属性 [FISCAL_POSITION_ID_TEXT]
     *
     */
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String fiscalPositionIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [PRICELIST_ID_TEXT]
     *
     */
    @JSONField(name = "pricelist_id_text")
    @JsonProperty("pricelist_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pricelistIdText;

    /**
     * 属性 [PAYMENT_TERM_ID_TEXT]
     *
     */
    @JSONField(name = "payment_term_id_text")
    @JsonProperty("payment_term_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String paymentTermIdText;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "sale_order_template_id_text")
    @JsonProperty("sale_order_template_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String saleOrderTemplateIdText;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[币种]不允许为空!")
    private Long currencyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teamId;

    /**
     * 属性 [PARTNER_INVOICE_ID]
     *
     */
    @JSONField(name = "partner_invoice_id")
    @JsonProperty("partner_invoice_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[发票地址]不允许为空!")
    private Long partnerInvoiceId;

    /**
     * 属性 [FISCAL_POSITION_ID]
     *
     */
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long fiscalPositionId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[客户]不允许为空!")
    private Long partnerId;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long campaignId;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sourceId;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_ID]
     *
     */
    @JSONField(name = "sale_order_template_id")
    @JsonProperty("sale_order_template_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long saleOrderTemplateId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long mediumId;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID]
     *
     */
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long analyticAccountId;

    /**
     * 属性 [PAYMENT_TERM_ID]
     *
     */
    @JSONField(name = "payment_term_id")
    @JsonProperty("payment_term_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long paymentTermId;

    /**
     * 属性 [PARTNER_SHIPPING_ID]
     *
     */
    @JSONField(name = "partner_shipping_id")
    @JsonProperty("partner_shipping_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[送货地址]不允许为空!")
    private Long partnerShippingId;

    /**
     * 属性 [OPPORTUNITY_ID]
     *
     */
    @JSONField(name = "opportunity_id")
    @JsonProperty("opportunity_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long opportunityId;

    /**
     * 属性 [INCOTERM]
     *
     */
    @JSONField(name = "incoterm")
    @JsonProperty("incoterm")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long incoterm;

    /**
     * 属性 [PRICELIST_ID]
     *
     */
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[价格表]不允许为空!")
    private Long pricelistId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[仓库]不允许为空!")
    private Long warehouseId;


    /**
     * 设置 [DATE_ORDER]
     */
    public void setDateOrder(Timestamp  dateOrder){
        this.dateOrder = dateOrder ;
        this.modify("date_order",dateOrder);
    }

    /**
     * 设置 [AMOUNT_UNTAXED]
     */
    public void setAmountUntaxed(BigDecimal  amountUntaxed){
        this.amountUntaxed = amountUntaxed ;
        this.modify("amount_untaxed",amountUntaxed);
    }

    /**
     * 设置 [VALIDITY_DATE]
     */
    public void setValidityDate(Timestamp  validityDate){
        this.validityDate = validityDate ;
        this.modify("validity_date",validityDate);
    }

    /**
     * 设置 [WARNING_STOCK]
     */
    public void setWarningStock(String  warningStock){
        this.warningStock = warningStock ;
        this.modify("warning_stock",warningStock);
    }

    /**
     * 设置 [EFFECTIVE_DATE]
     */
    public void setEffectiveDate(Timestamp  effectiveDate){
        this.effectiveDate = effectiveDate ;
        this.modify("effective_date",effectiveDate);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [REQUIRE_SIGNATURE]
     */
    public void setRequireSignature(Boolean  requireSignature){
        this.requireSignature = requireSignature ;
        this.modify("require_signature",requireSignature);
    }

    /**
     * 设置 [CURRENCY_RATE]
     */
    public void setCurrencyRate(Double  currencyRate){
        this.currencyRate = currencyRate ;
        this.modify("currency_rate",currencyRate);
    }

    /**
     * 设置 [PICKING_POLICY]
     */
    public void setPickingPolicy(String  pickingPolicy){
        this.pickingPolicy = pickingPolicy ;
        this.modify("picking_policy",pickingPolicy);
    }

    /**
     * 设置 [PROCUREMENT_GROUP_ID]
     */
    public void setProcurementGroupId(Integer  procurementGroupId){
        this.procurementGroupId = procurementGroupId ;
        this.modify("procurement_group_id",procurementGroupId);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [SIGNED_BY]
     */
    public void setSignedBy(String  signedBy){
        this.signedBy = signedBy ;
        this.modify("signed_by",signedBy);
    }

    /**
     * 设置 [REQUIRE_PAYMENT]
     */
    public void setRequirePayment(Boolean  requirePayment){
        this.requirePayment = requirePayment ;
        this.modify("require_payment",requirePayment);
    }

    /**
     * 设置 [REFERENCE]
     */
    public void setReference(String  reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [INVOICE_STATUS]
     */
    public void setInvoiceStatus(String  invoiceStatus){
        this.invoiceStatus = invoiceStatus ;
        this.modify("invoice_status",invoiceStatus);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [COMMITMENT_DATE]
     */
    public void setCommitmentDate(Timestamp  commitmentDate){
        this.commitmentDate = commitmentDate ;
        this.modify("commitment_date",commitmentDate);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [CONFIRMATION_DATE]
     */
    public void setConfirmationDate(Timestamp  confirmationDate){
        this.confirmationDate = confirmationDate ;
        this.modify("confirmation_date",confirmationDate);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    public void setAccessToken(String  accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [AMOUNT_TOTAL]
     */
    public void setAmountTotal(BigDecimal  amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }

    /**
     * 设置 [CLIENT_ORDER_REF]
     */
    public void setClientOrderRef(String  clientOrderRef){
        this.clientOrderRef = clientOrderRef ;
        this.modify("client_order_ref",clientOrderRef);
    }

    /**
     * 设置 [CART_RECOVERY_EMAIL_SENT]
     */
    public void setCartRecoveryEmailSent(Boolean  cartRecoveryEmailSent){
        this.cartRecoveryEmailSent = cartRecoveryEmailSent ;
        this.modify("cart_recovery_email_sent",cartRecoveryEmailSent);
    }

    /**
     * 设置 [AMOUNT_TAX]
     */
    public void setAmountTax(BigDecimal  amountTax){
        this.amountTax = amountTax ;
        this.modify("amount_tax",amountTax);
    }

    /**
     * 设置 [TEAM_ID]
     */
    public void setTeamId(Long  teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [PARTNER_INVOICE_ID]
     */
    public void setPartnerInvoiceId(Long  partnerInvoiceId){
        this.partnerInvoiceId = partnerInvoiceId ;
        this.modify("partner_invoice_id",partnerInvoiceId);
    }

    /**
     * 设置 [FISCAL_POSITION_ID]
     */
    public void setFiscalPositionId(Long  fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    public void setCampaignId(Long  campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [SOURCE_ID]
     */
    public void setSourceId(Long  sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [SALE_ORDER_TEMPLATE_ID]
     */
    public void setSaleOrderTemplateId(Long  saleOrderTemplateId){
        this.saleOrderTemplateId = saleOrderTemplateId ;
        this.modify("sale_order_template_id",saleOrderTemplateId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    public void setMediumId(Long  mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID]
     */
    public void setAnalyticAccountId(Long  analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }

    /**
     * 设置 [PAYMENT_TERM_ID]
     */
    public void setPaymentTermId(Long  paymentTermId){
        this.paymentTermId = paymentTermId ;
        this.modify("payment_term_id",paymentTermId);
    }

    /**
     * 设置 [PARTNER_SHIPPING_ID]
     */
    public void setPartnerShippingId(Long  partnerShippingId){
        this.partnerShippingId = partnerShippingId ;
        this.modify("partner_shipping_id",partnerShippingId);
    }

    /**
     * 设置 [OPPORTUNITY_ID]
     */
    public void setOpportunityId(Long  opportunityId){
        this.opportunityId = opportunityId ;
        this.modify("opportunity_id",opportunityId);
    }

    /**
     * 设置 [INCOTERM]
     */
    public void setIncoterm(Long  incoterm){
        this.incoterm = incoterm ;
        this.modify("incoterm",incoterm);
    }

    /**
     * 设置 [PRICELIST_ID]
     */
    public void setPricelistId(Long  pricelistId){
        this.pricelistId = pricelistId ;
        this.modify("pricelist_id",pricelistId);
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    public void setWarehouseId(Long  warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }


}



package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_scheduler_compute;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scheduler_computeService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_scheduler_computeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"手动运行调度器" })
@RestController("Core-stock_scheduler_compute")
@RequestMapping("")
public class Stock_scheduler_computeResource {

    @Autowired
    public IStock_scheduler_computeService stock_scheduler_computeService;

    @Autowired
    @Lazy
    public Stock_scheduler_computeMapping stock_scheduler_computeMapping;

    @PreAuthorize("hasPermission(this.stock_scheduler_computeMapping.toDomain(#stock_scheduler_computedto),'iBizBusinessCentral-Stock_scheduler_compute-Create')")
    @ApiOperation(value = "新建手动运行调度器", tags = {"手动运行调度器" },  notes = "新建手动运行调度器")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes")
    public ResponseEntity<Stock_scheduler_computeDTO> create(@Validated @RequestBody Stock_scheduler_computeDTO stock_scheduler_computedto) {
        Stock_scheduler_compute domain = stock_scheduler_computeMapping.toDomain(stock_scheduler_computedto);
		stock_scheduler_computeService.create(domain);
        Stock_scheduler_computeDTO dto = stock_scheduler_computeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_scheduler_computeMapping.toDomain(#stock_scheduler_computedtos),'iBizBusinessCentral-Stock_scheduler_compute-Create')")
    @ApiOperation(value = "批量新建手动运行调度器", tags = {"手动运行调度器" },  notes = "批量新建手动运行调度器")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_scheduler_computeDTO> stock_scheduler_computedtos) {
        stock_scheduler_computeService.createBatch(stock_scheduler_computeMapping.toDomain(stock_scheduler_computedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_scheduler_compute" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_scheduler_computeService.get(#stock_scheduler_compute_id),'iBizBusinessCentral-Stock_scheduler_compute-Update')")
    @ApiOperation(value = "更新手动运行调度器", tags = {"手动运行调度器" },  notes = "更新手动运行调度器")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_scheduler_computes/{stock_scheduler_compute_id}")
    public ResponseEntity<Stock_scheduler_computeDTO> update(@PathVariable("stock_scheduler_compute_id") Long stock_scheduler_compute_id, @RequestBody Stock_scheduler_computeDTO stock_scheduler_computedto) {
		Stock_scheduler_compute domain  = stock_scheduler_computeMapping.toDomain(stock_scheduler_computedto);
        domain .setId(stock_scheduler_compute_id);
		stock_scheduler_computeService.update(domain );
		Stock_scheduler_computeDTO dto = stock_scheduler_computeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_scheduler_computeService.getStockSchedulerComputeByEntities(this.stock_scheduler_computeMapping.toDomain(#stock_scheduler_computedtos)),'iBizBusinessCentral-Stock_scheduler_compute-Update')")
    @ApiOperation(value = "批量更新手动运行调度器", tags = {"手动运行调度器" },  notes = "批量更新手动运行调度器")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_scheduler_computes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_scheduler_computeDTO> stock_scheduler_computedtos) {
        stock_scheduler_computeService.updateBatch(stock_scheduler_computeMapping.toDomain(stock_scheduler_computedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_scheduler_computeService.get(#stock_scheduler_compute_id),'iBizBusinessCentral-Stock_scheduler_compute-Remove')")
    @ApiOperation(value = "删除手动运行调度器", tags = {"手动运行调度器" },  notes = "删除手动运行调度器")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_scheduler_computes/{stock_scheduler_compute_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_scheduler_compute_id") Long stock_scheduler_compute_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_scheduler_computeService.remove(stock_scheduler_compute_id));
    }

    @PreAuthorize("hasPermission(this.stock_scheduler_computeService.getStockSchedulerComputeByIds(#ids),'iBizBusinessCentral-Stock_scheduler_compute-Remove')")
    @ApiOperation(value = "批量删除手动运行调度器", tags = {"手动运行调度器" },  notes = "批量删除手动运行调度器")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_scheduler_computes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_scheduler_computeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_scheduler_computeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_scheduler_compute-Get')")
    @ApiOperation(value = "获取手动运行调度器", tags = {"手动运行调度器" },  notes = "获取手动运行调度器")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_scheduler_computes/{stock_scheduler_compute_id}")
    public ResponseEntity<Stock_scheduler_computeDTO> get(@PathVariable("stock_scheduler_compute_id") Long stock_scheduler_compute_id) {
        Stock_scheduler_compute domain = stock_scheduler_computeService.get(stock_scheduler_compute_id);
        Stock_scheduler_computeDTO dto = stock_scheduler_computeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取手动运行调度器草稿", tags = {"手动运行调度器" },  notes = "获取手动运行调度器草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_scheduler_computes/getdraft")
    public ResponseEntity<Stock_scheduler_computeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_scheduler_computeMapping.toDto(stock_scheduler_computeService.getDraft(new Stock_scheduler_compute())));
    }

    @ApiOperation(value = "检查手动运行调度器", tags = {"手动运行调度器" },  notes = "检查手动运行调度器")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_scheduler_computeDTO stock_scheduler_computedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_scheduler_computeService.checkKey(stock_scheduler_computeMapping.toDomain(stock_scheduler_computedto)));
    }

    @PreAuthorize("hasPermission(this.stock_scheduler_computeMapping.toDomain(#stock_scheduler_computedto),'iBizBusinessCentral-Stock_scheduler_compute-Save')")
    @ApiOperation(value = "保存手动运行调度器", tags = {"手动运行调度器" },  notes = "保存手动运行调度器")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_scheduler_computeDTO stock_scheduler_computedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_scheduler_computeService.save(stock_scheduler_computeMapping.toDomain(stock_scheduler_computedto)));
    }

    @PreAuthorize("hasPermission(this.stock_scheduler_computeMapping.toDomain(#stock_scheduler_computedtos),'iBizBusinessCentral-Stock_scheduler_compute-Save')")
    @ApiOperation(value = "批量保存手动运行调度器", tags = {"手动运行调度器" },  notes = "批量保存手动运行调度器")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_scheduler_computeDTO> stock_scheduler_computedtos) {
        stock_scheduler_computeService.saveBatch(stock_scheduler_computeMapping.toDomain(stock_scheduler_computedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_scheduler_compute-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_scheduler_compute-Get')")
	@ApiOperation(value = "获取数据集", tags = {"手动运行调度器" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_scheduler_computes/fetchdefault")
	public ResponseEntity<List<Stock_scheduler_computeDTO>> fetchDefault(Stock_scheduler_computeSearchContext context) {
        Page<Stock_scheduler_compute> domains = stock_scheduler_computeService.searchDefault(context) ;
        List<Stock_scheduler_computeDTO> list = stock_scheduler_computeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_scheduler_compute-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_scheduler_compute-Get')")
	@ApiOperation(value = "查询数据集", tags = {"手动运行调度器" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_scheduler_computes/searchdefault")
	public ResponseEntity<Page<Stock_scheduler_computeDTO>> searchDefault(@RequestBody Stock_scheduler_computeSearchContext context) {
        Page<Stock_scheduler_compute> domains = stock_scheduler_computeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_scheduler_computeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


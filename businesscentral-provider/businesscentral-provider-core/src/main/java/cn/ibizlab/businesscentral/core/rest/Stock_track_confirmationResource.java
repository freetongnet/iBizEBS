package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_track_confirmation;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_track_confirmationService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_track_confirmationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存追溯确认" })
@RestController("Core-stock_track_confirmation")
@RequestMapping("")
public class Stock_track_confirmationResource {

    @Autowired
    public IStock_track_confirmationService stock_track_confirmationService;

    @Autowired
    @Lazy
    public Stock_track_confirmationMapping stock_track_confirmationMapping;

    @PreAuthorize("hasPermission(this.stock_track_confirmationMapping.toDomain(#stock_track_confirmationdto),'iBizBusinessCentral-Stock_track_confirmation-Create')")
    @ApiOperation(value = "新建库存追溯确认", tags = {"库存追溯确认" },  notes = "新建库存追溯确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations")
    public ResponseEntity<Stock_track_confirmationDTO> create(@Validated @RequestBody Stock_track_confirmationDTO stock_track_confirmationdto) {
        Stock_track_confirmation domain = stock_track_confirmationMapping.toDomain(stock_track_confirmationdto);
		stock_track_confirmationService.create(domain);
        Stock_track_confirmationDTO dto = stock_track_confirmationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_track_confirmationMapping.toDomain(#stock_track_confirmationdtos),'iBizBusinessCentral-Stock_track_confirmation-Create')")
    @ApiOperation(value = "批量新建库存追溯确认", tags = {"库存追溯确认" },  notes = "批量新建库存追溯确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_track_confirmationDTO> stock_track_confirmationdtos) {
        stock_track_confirmationService.createBatch(stock_track_confirmationMapping.toDomain(stock_track_confirmationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_track_confirmation" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_track_confirmationService.get(#stock_track_confirmation_id),'iBizBusinessCentral-Stock_track_confirmation-Update')")
    @ApiOperation(value = "更新库存追溯确认", tags = {"库存追溯确认" },  notes = "更新库存追溯确认")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_track_confirmations/{stock_track_confirmation_id}")
    public ResponseEntity<Stock_track_confirmationDTO> update(@PathVariable("stock_track_confirmation_id") Long stock_track_confirmation_id, @RequestBody Stock_track_confirmationDTO stock_track_confirmationdto) {
		Stock_track_confirmation domain  = stock_track_confirmationMapping.toDomain(stock_track_confirmationdto);
        domain .setId(stock_track_confirmation_id);
		stock_track_confirmationService.update(domain );
		Stock_track_confirmationDTO dto = stock_track_confirmationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_track_confirmationService.getStockTrackConfirmationByEntities(this.stock_track_confirmationMapping.toDomain(#stock_track_confirmationdtos)),'iBizBusinessCentral-Stock_track_confirmation-Update')")
    @ApiOperation(value = "批量更新库存追溯确认", tags = {"库存追溯确认" },  notes = "批量更新库存追溯确认")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_track_confirmations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_track_confirmationDTO> stock_track_confirmationdtos) {
        stock_track_confirmationService.updateBatch(stock_track_confirmationMapping.toDomain(stock_track_confirmationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_track_confirmationService.get(#stock_track_confirmation_id),'iBizBusinessCentral-Stock_track_confirmation-Remove')")
    @ApiOperation(value = "删除库存追溯确认", tags = {"库存追溯确认" },  notes = "删除库存追溯确认")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_confirmations/{stock_track_confirmation_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_track_confirmation_id") Long stock_track_confirmation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_track_confirmationService.remove(stock_track_confirmation_id));
    }

    @PreAuthorize("hasPermission(this.stock_track_confirmationService.getStockTrackConfirmationByIds(#ids),'iBizBusinessCentral-Stock_track_confirmation-Remove')")
    @ApiOperation(value = "批量删除库存追溯确认", tags = {"库存追溯确认" },  notes = "批量删除库存追溯确认")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_track_confirmations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_track_confirmationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_track_confirmationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_track_confirmation-Get')")
    @ApiOperation(value = "获取库存追溯确认", tags = {"库存追溯确认" },  notes = "获取库存追溯确认")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_track_confirmations/{stock_track_confirmation_id}")
    public ResponseEntity<Stock_track_confirmationDTO> get(@PathVariable("stock_track_confirmation_id") Long stock_track_confirmation_id) {
        Stock_track_confirmation domain = stock_track_confirmationService.get(stock_track_confirmation_id);
        Stock_track_confirmationDTO dto = stock_track_confirmationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存追溯确认草稿", tags = {"库存追溯确认" },  notes = "获取库存追溯确认草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_track_confirmations/getdraft")
    public ResponseEntity<Stock_track_confirmationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_track_confirmationMapping.toDto(stock_track_confirmationService.getDraft(new Stock_track_confirmation())));
    }

    @ApiOperation(value = "检查库存追溯确认", tags = {"库存追溯确认" },  notes = "检查库存追溯确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_track_confirmationDTO stock_track_confirmationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_track_confirmationService.checkKey(stock_track_confirmationMapping.toDomain(stock_track_confirmationdto)));
    }

    @PreAuthorize("hasPermission(this.stock_track_confirmationMapping.toDomain(#stock_track_confirmationdto),'iBizBusinessCentral-Stock_track_confirmation-Save')")
    @ApiOperation(value = "保存库存追溯确认", tags = {"库存追溯确认" },  notes = "保存库存追溯确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_track_confirmationDTO stock_track_confirmationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_track_confirmationService.save(stock_track_confirmationMapping.toDomain(stock_track_confirmationdto)));
    }

    @PreAuthorize("hasPermission(this.stock_track_confirmationMapping.toDomain(#stock_track_confirmationdtos),'iBizBusinessCentral-Stock_track_confirmation-Save')")
    @ApiOperation(value = "批量保存库存追溯确认", tags = {"库存追溯确认" },  notes = "批量保存库存追溯确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_track_confirmations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_track_confirmationDTO> stock_track_confirmationdtos) {
        stock_track_confirmationService.saveBatch(stock_track_confirmationMapping.toDomain(stock_track_confirmationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_track_confirmation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_track_confirmation-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存追溯确认" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_track_confirmations/fetchdefault")
	public ResponseEntity<List<Stock_track_confirmationDTO>> fetchDefault(Stock_track_confirmationSearchContext context) {
        Page<Stock_track_confirmation> domains = stock_track_confirmationService.searchDefault(context) ;
        List<Stock_track_confirmationDTO> list = stock_track_confirmationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_track_confirmation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_track_confirmation-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存追溯确认" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_track_confirmations/searchdefault")
	public ResponseEntity<Page<Stock_track_confirmationDTO>> searchDefault(@RequestBody Stock_track_confirmationSearchContext context) {
        Page<Stock_track_confirmation> domains = stock_track_confirmationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_track_confirmationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_closebalanceService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"银行对账单期末余额" })
@RestController("Core-account_bank_statement_closebalance")
@RequestMapping("")
public class Account_bank_statement_closebalanceResource {

    @Autowired
    public IAccount_bank_statement_closebalanceService account_bank_statement_closebalanceService;

    @Autowired
    @Lazy
    public Account_bank_statement_closebalanceMapping account_bank_statement_closebalanceMapping;

    @PreAuthorize("hasPermission(this.account_bank_statement_closebalanceMapping.toDomain(#account_bank_statement_closebalancedto),'iBizBusinessCentral-Account_bank_statement_closebalance-Create')")
    @ApiOperation(value = "新建银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "新建银行对账单期末余额")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances")
    public ResponseEntity<Account_bank_statement_closebalanceDTO> create(@Validated @RequestBody Account_bank_statement_closebalanceDTO account_bank_statement_closebalancedto) {
        Account_bank_statement_closebalance domain = account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedto);
		account_bank_statement_closebalanceService.create(domain);
        Account_bank_statement_closebalanceDTO dto = account_bank_statement_closebalanceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_closebalanceMapping.toDomain(#account_bank_statement_closebalancedtos),'iBizBusinessCentral-Account_bank_statement_closebalance-Create')")
    @ApiOperation(value = "批量新建银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "批量新建银行对账单期末余额")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statement_closebalanceDTO> account_bank_statement_closebalancedtos) {
        account_bank_statement_closebalanceService.createBatch(account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_bank_statement_closebalance" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_bank_statement_closebalanceService.get(#account_bank_statement_closebalance_id),'iBizBusinessCentral-Account_bank_statement_closebalance-Update')")
    @ApiOperation(value = "更新银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "更新银行对账单期末余额")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_closebalances/{account_bank_statement_closebalance_id}")
    public ResponseEntity<Account_bank_statement_closebalanceDTO> update(@PathVariable("account_bank_statement_closebalance_id") Long account_bank_statement_closebalance_id, @RequestBody Account_bank_statement_closebalanceDTO account_bank_statement_closebalancedto) {
		Account_bank_statement_closebalance domain  = account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedto);
        domain .setId(account_bank_statement_closebalance_id);
		account_bank_statement_closebalanceService.update(domain );
		Account_bank_statement_closebalanceDTO dto = account_bank_statement_closebalanceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_closebalanceService.getAccountBankStatementClosebalanceByEntities(this.account_bank_statement_closebalanceMapping.toDomain(#account_bank_statement_closebalancedtos)),'iBizBusinessCentral-Account_bank_statement_closebalance-Update')")
    @ApiOperation(value = "批量更新银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "批量更新银行对账单期末余额")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_closebalances/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_closebalanceDTO> account_bank_statement_closebalancedtos) {
        account_bank_statement_closebalanceService.updateBatch(account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_closebalanceService.get(#account_bank_statement_closebalance_id),'iBizBusinessCentral-Account_bank_statement_closebalance-Remove')")
    @ApiOperation(value = "删除银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "删除银行对账单期末余额")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_closebalances/{account_bank_statement_closebalance_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_closebalance_id") Long account_bank_statement_closebalance_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_closebalanceService.remove(account_bank_statement_closebalance_id));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_closebalanceService.getAccountBankStatementClosebalanceByIds(#ids),'iBizBusinessCentral-Account_bank_statement_closebalance-Remove')")
    @ApiOperation(value = "批量删除银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "批量删除银行对账单期末余额")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_closebalances/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_bank_statement_closebalanceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_bank_statement_closebalanceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_bank_statement_closebalance-Get')")
    @ApiOperation(value = "获取银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "获取银行对账单期末余额")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_closebalances/{account_bank_statement_closebalance_id}")
    public ResponseEntity<Account_bank_statement_closebalanceDTO> get(@PathVariable("account_bank_statement_closebalance_id") Long account_bank_statement_closebalance_id) {
        Account_bank_statement_closebalance domain = account_bank_statement_closebalanceService.get(account_bank_statement_closebalance_id);
        Account_bank_statement_closebalanceDTO dto = account_bank_statement_closebalanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取银行对账单期末余额草稿", tags = {"银行对账单期末余额" },  notes = "获取银行对账单期末余额草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_closebalances/getdraft")
    public ResponseEntity<Account_bank_statement_closebalanceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_closebalanceMapping.toDto(account_bank_statement_closebalanceService.getDraft(new Account_bank_statement_closebalance())));
    }

    @ApiOperation(value = "检查银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "检查银行对账单期末余额")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_bank_statement_closebalanceDTO account_bank_statement_closebalancedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_closebalanceService.checkKey(account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_closebalanceMapping.toDomain(#account_bank_statement_closebalancedto),'iBizBusinessCentral-Account_bank_statement_closebalance-Save')")
    @ApiOperation(value = "保存银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "保存银行对账单期末余额")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_bank_statement_closebalanceDTO account_bank_statement_closebalancedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_closebalanceService.save(account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_closebalanceMapping.toDomain(#account_bank_statement_closebalancedtos),'iBizBusinessCentral-Account_bank_statement_closebalance-Save')")
    @ApiOperation(value = "批量保存银行对账单期末余额", tags = {"银行对账单期末余额" },  notes = "批量保存银行对账单期末余额")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_bank_statement_closebalanceDTO> account_bank_statement_closebalancedtos) {
        account_bank_statement_closebalanceService.saveBatch(account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_closebalance-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_closebalance-Get')")
	@ApiOperation(value = "获取数据集", tags = {"银行对账单期末余额" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_closebalances/fetchdefault")
	public ResponseEntity<List<Account_bank_statement_closebalanceDTO>> fetchDefault(Account_bank_statement_closebalanceSearchContext context) {
        Page<Account_bank_statement_closebalance> domains = account_bank_statement_closebalanceService.searchDefault(context) ;
        List<Account_bank_statement_closebalanceDTO> list = account_bank_statement_closebalanceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_closebalance-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_closebalance-Get')")
	@ApiOperation(value = "查询数据集", tags = {"银行对账单期末余额" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_bank_statement_closebalances/searchdefault")
	public ResponseEntity<Page<Account_bank_statement_closebalanceDTO>> searchDefault(@RequestBody Account_bank_statement_closebalanceSearchContext context) {
        Page<Account_bank_statement_closebalance> domains = account_bank_statement_closebalanceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statement_closebalanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


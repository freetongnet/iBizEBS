package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.businesscentral.core.dto.Lunch_order_line_luckyDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreLunch_order_line_luckyMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Lunch_order_line_luckyMapping extends MappingBase<Lunch_order_line_luckyDTO, Lunch_order_line_lucky> {


}


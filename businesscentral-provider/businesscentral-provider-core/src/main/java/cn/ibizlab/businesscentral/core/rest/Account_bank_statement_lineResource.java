package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_line;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_lineService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"银行对账单明细" })
@RestController("Core-account_bank_statement_line")
@RequestMapping("")
public class Account_bank_statement_lineResource {

    @Autowired
    public IAccount_bank_statement_lineService account_bank_statement_lineService;

    @Autowired
    @Lazy
    public Account_bank_statement_lineMapping account_bank_statement_lineMapping;

    @PreAuthorize("hasPermission(this.account_bank_statement_lineMapping.toDomain(#account_bank_statement_linedto),'iBizBusinessCentral-Account_bank_statement_line-Create')")
    @ApiOperation(value = "新建银行对账单明细", tags = {"银行对账单明细" },  notes = "新建银行对账单明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines")
    public ResponseEntity<Account_bank_statement_lineDTO> create(@Validated @RequestBody Account_bank_statement_lineDTO account_bank_statement_linedto) {
        Account_bank_statement_line domain = account_bank_statement_lineMapping.toDomain(account_bank_statement_linedto);
		account_bank_statement_lineService.create(domain);
        Account_bank_statement_lineDTO dto = account_bank_statement_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_lineMapping.toDomain(#account_bank_statement_linedtos),'iBizBusinessCentral-Account_bank_statement_line-Create')")
    @ApiOperation(value = "批量新建银行对账单明细", tags = {"银行对账单明细" },  notes = "批量新建银行对账单明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statement_lineDTO> account_bank_statement_linedtos) {
        account_bank_statement_lineService.createBatch(account_bank_statement_lineMapping.toDomain(account_bank_statement_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_bank_statement_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_bank_statement_lineService.get(#account_bank_statement_line_id),'iBizBusinessCentral-Account_bank_statement_line-Update')")
    @ApiOperation(value = "更新银行对账单明细", tags = {"银行对账单明细" },  notes = "更新银行对账单明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_lines/{account_bank_statement_line_id}")
    public ResponseEntity<Account_bank_statement_lineDTO> update(@PathVariable("account_bank_statement_line_id") Long account_bank_statement_line_id, @RequestBody Account_bank_statement_lineDTO account_bank_statement_linedto) {
		Account_bank_statement_line domain  = account_bank_statement_lineMapping.toDomain(account_bank_statement_linedto);
        domain .setId(account_bank_statement_line_id);
		account_bank_statement_lineService.update(domain );
		Account_bank_statement_lineDTO dto = account_bank_statement_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_lineService.getAccountBankStatementLineByEntities(this.account_bank_statement_lineMapping.toDomain(#account_bank_statement_linedtos)),'iBizBusinessCentral-Account_bank_statement_line-Update')")
    @ApiOperation(value = "批量更新银行对账单明细", tags = {"银行对账单明细" },  notes = "批量更新银行对账单明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_lineDTO> account_bank_statement_linedtos) {
        account_bank_statement_lineService.updateBatch(account_bank_statement_lineMapping.toDomain(account_bank_statement_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_lineService.get(#account_bank_statement_line_id),'iBizBusinessCentral-Account_bank_statement_line-Remove')")
    @ApiOperation(value = "删除银行对账单明细", tags = {"银行对账单明细" },  notes = "删除银行对账单明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_lines/{account_bank_statement_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_line_id") Long account_bank_statement_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_lineService.remove(account_bank_statement_line_id));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_lineService.getAccountBankStatementLineByIds(#ids),'iBizBusinessCentral-Account_bank_statement_line-Remove')")
    @ApiOperation(value = "批量删除银行对账单明细", tags = {"银行对账单明细" },  notes = "批量删除银行对账单明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_bank_statement_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_bank_statement_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_bank_statement_line-Get')")
    @ApiOperation(value = "获取银行对账单明细", tags = {"银行对账单明细" },  notes = "获取银行对账单明细")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_lines/{account_bank_statement_line_id}")
    public ResponseEntity<Account_bank_statement_lineDTO> get(@PathVariable("account_bank_statement_line_id") Long account_bank_statement_line_id) {
        Account_bank_statement_line domain = account_bank_statement_lineService.get(account_bank_statement_line_id);
        Account_bank_statement_lineDTO dto = account_bank_statement_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取银行对账单明细草稿", tags = {"银行对账单明细" },  notes = "获取银行对账单明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_lines/getdraft")
    public ResponseEntity<Account_bank_statement_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_lineMapping.toDto(account_bank_statement_lineService.getDraft(new Account_bank_statement_line())));
    }

    @ApiOperation(value = "检查银行对账单明细", tags = {"银行对账单明细" },  notes = "检查银行对账单明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_bank_statement_lineDTO account_bank_statement_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_lineService.checkKey(account_bank_statement_lineMapping.toDomain(account_bank_statement_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_lineMapping.toDomain(#account_bank_statement_linedto),'iBizBusinessCentral-Account_bank_statement_line-Save')")
    @ApiOperation(value = "保存银行对账单明细", tags = {"银行对账单明细" },  notes = "保存银行对账单明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_bank_statement_lineDTO account_bank_statement_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_lineService.save(account_bank_statement_lineMapping.toDomain(account_bank_statement_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statement_lineMapping.toDomain(#account_bank_statement_linedtos),'iBizBusinessCentral-Account_bank_statement_line-Save')")
    @ApiOperation(value = "批量保存银行对账单明细", tags = {"银行对账单明细" },  notes = "批量保存银行对账单明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_bank_statement_lineDTO> account_bank_statement_linedtos) {
        account_bank_statement_lineService.saveBatch(account_bank_statement_lineMapping.toDomain(account_bank_statement_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"银行对账单明细" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_lines/fetchdefault")
	public ResponseEntity<List<Account_bank_statement_lineDTO>> fetchDefault(Account_bank_statement_lineSearchContext context) {
        Page<Account_bank_statement_line> domains = account_bank_statement_lineService.searchDefault(context) ;
        List<Account_bank_statement_lineDTO> list = account_bank_statement_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"银行对账单明细" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_bank_statement_lines/searchdefault")
	public ResponseEntity<Page<Account_bank_statement_lineDTO>> searchDefault(@RequestBody Account_bank_statement_lineSearchContext context) {
        Page<Account_bank_statement_line> domains = account_bank_statement_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statement_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


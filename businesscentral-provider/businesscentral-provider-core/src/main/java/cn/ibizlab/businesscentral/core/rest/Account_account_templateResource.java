package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_templateService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_account_templateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"科目模板" })
@RestController("Core-account_account_template")
@RequestMapping("")
public class Account_account_templateResource {

    @Autowired
    public IAccount_account_templateService account_account_templateService;

    @Autowired
    @Lazy
    public Account_account_templateMapping account_account_templateMapping;

    @PreAuthorize("hasPermission(this.account_account_templateMapping.toDomain(#account_account_templatedto),'iBizBusinessCentral-Account_account_template-Create')")
    @ApiOperation(value = "新建科目模板", tags = {"科目模板" },  notes = "新建科目模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_templates")
    public ResponseEntity<Account_account_templateDTO> create(@Validated @RequestBody Account_account_templateDTO account_account_templatedto) {
        Account_account_template domain = account_account_templateMapping.toDomain(account_account_templatedto);
		account_account_templateService.create(domain);
        Account_account_templateDTO dto = account_account_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_account_templateMapping.toDomain(#account_account_templatedtos),'iBizBusinessCentral-Account_account_template-Create')")
    @ApiOperation(value = "批量新建科目模板", tags = {"科目模板" },  notes = "批量新建科目模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_account_templateDTO> account_account_templatedtos) {
        account_account_templateService.createBatch(account_account_templateMapping.toDomain(account_account_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_account_template" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_account_templateService.get(#account_account_template_id),'iBizBusinessCentral-Account_account_template-Update')")
    @ApiOperation(value = "更新科目模板", tags = {"科目模板" },  notes = "更新科目模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_account_templates/{account_account_template_id}")
    public ResponseEntity<Account_account_templateDTO> update(@PathVariable("account_account_template_id") Long account_account_template_id, @RequestBody Account_account_templateDTO account_account_templatedto) {
		Account_account_template domain  = account_account_templateMapping.toDomain(account_account_templatedto);
        domain .setId(account_account_template_id);
		account_account_templateService.update(domain );
		Account_account_templateDTO dto = account_account_templateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_account_templateService.getAccountAccountTemplateByEntities(this.account_account_templateMapping.toDomain(#account_account_templatedtos)),'iBizBusinessCentral-Account_account_template-Update')")
    @ApiOperation(value = "批量更新科目模板", tags = {"科目模板" },  notes = "批量更新科目模板")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_account_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_account_templateDTO> account_account_templatedtos) {
        account_account_templateService.updateBatch(account_account_templateMapping.toDomain(account_account_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_account_templateService.get(#account_account_template_id),'iBizBusinessCentral-Account_account_template-Remove')")
    @ApiOperation(value = "删除科目模板", tags = {"科目模板" },  notes = "删除科目模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_account_templates/{account_account_template_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_account_template_id") Long account_account_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_account_templateService.remove(account_account_template_id));
    }

    @PreAuthorize("hasPermission(this.account_account_templateService.getAccountAccountTemplateByIds(#ids),'iBizBusinessCentral-Account_account_template-Remove')")
    @ApiOperation(value = "批量删除科目模板", tags = {"科目模板" },  notes = "批量删除科目模板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_account_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_account_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_account_templateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_account_template-Get')")
    @ApiOperation(value = "获取科目模板", tags = {"科目模板" },  notes = "获取科目模板")
	@RequestMapping(method = RequestMethod.GET, value = "/account_account_templates/{account_account_template_id}")
    public ResponseEntity<Account_account_templateDTO> get(@PathVariable("account_account_template_id") Long account_account_template_id) {
        Account_account_template domain = account_account_templateService.get(account_account_template_id);
        Account_account_templateDTO dto = account_account_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取科目模板草稿", tags = {"科目模板" },  notes = "获取科目模板草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_account_templates/getdraft")
    public ResponseEntity<Account_account_templateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_account_templateMapping.toDto(account_account_templateService.getDraft(new Account_account_template())));
    }

    @ApiOperation(value = "检查科目模板", tags = {"科目模板" },  notes = "检查科目模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_templates/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_account_templateDTO account_account_templatedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_account_templateService.checkKey(account_account_templateMapping.toDomain(account_account_templatedto)));
    }

    @PreAuthorize("hasPermission(this.account_account_templateMapping.toDomain(#account_account_templatedto),'iBizBusinessCentral-Account_account_template-Save')")
    @ApiOperation(value = "保存科目模板", tags = {"科目模板" },  notes = "保存科目模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_templates/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_account_templateDTO account_account_templatedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_account_templateService.save(account_account_templateMapping.toDomain(account_account_templatedto)));
    }

    @PreAuthorize("hasPermission(this.account_account_templateMapping.toDomain(#account_account_templatedtos),'iBizBusinessCentral-Account_account_template-Save')")
    @ApiOperation(value = "批量保存科目模板", tags = {"科目模板" },  notes = "批量保存科目模板")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_templates/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_account_templateDTO> account_account_templatedtos) {
        account_account_templateService.saveBatch(account_account_templateMapping.toDomain(account_account_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_account_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_account_template-Get')")
	@ApiOperation(value = "获取数据集", tags = {"科目模板" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_account_templates/fetchdefault")
	public ResponseEntity<List<Account_account_templateDTO>> fetchDefault(Account_account_templateSearchContext context) {
        Page<Account_account_template> domains = account_account_templateService.searchDefault(context) ;
        List<Account_account_templateDTO> list = account_account_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_account_template-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_account_template-Get')")
	@ApiOperation(value = "查询数据集", tags = {"科目模板" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_account_templates/searchdefault")
	public ResponseEntity<Page<Account_account_templateDTO>> searchDefault(@RequestBody Account_account_templateSearchContext context) {
        Page<Account_account_template> domains = account_account_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_account_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


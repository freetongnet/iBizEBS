package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.businesscentral.core.odoo_asset.service.IAsset_categoryService;
import cn.ibizlab.businesscentral.core.odoo_asset.filter.Asset_categorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Asset Tags" })
@RestController("Core-asset_category")
@RequestMapping("")
public class Asset_categoryResource {

    @Autowired
    public IAsset_categoryService asset_categoryService;

    @Autowired
    @Lazy
    public Asset_categoryMapping asset_categoryMapping;

    @PreAuthorize("hasPermission(this.asset_categoryMapping.toDomain(#asset_categorydto),'iBizBusinessCentral-Asset_category-Create')")
    @ApiOperation(value = "新建Asset Tags", tags = {"Asset Tags" },  notes = "新建Asset Tags")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_categories")
    public ResponseEntity<Asset_categoryDTO> create(@Validated @RequestBody Asset_categoryDTO asset_categorydto) {
        Asset_category domain = asset_categoryMapping.toDomain(asset_categorydto);
		asset_categoryService.create(domain);
        Asset_categoryDTO dto = asset_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.asset_categoryMapping.toDomain(#asset_categorydtos),'iBizBusinessCentral-Asset_category-Create')")
    @ApiOperation(value = "批量新建Asset Tags", tags = {"Asset Tags" },  notes = "批量新建Asset Tags")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Asset_categoryDTO> asset_categorydtos) {
        asset_categoryService.createBatch(asset_categoryMapping.toDomain(asset_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "asset_category" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.asset_categoryService.get(#asset_category_id),'iBizBusinessCentral-Asset_category-Update')")
    @ApiOperation(value = "更新Asset Tags", tags = {"Asset Tags" },  notes = "更新Asset Tags")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_categories/{asset_category_id}")
    public ResponseEntity<Asset_categoryDTO> update(@PathVariable("asset_category_id") Long asset_category_id, @RequestBody Asset_categoryDTO asset_categorydto) {
		Asset_category domain  = asset_categoryMapping.toDomain(asset_categorydto);
        domain .setId(asset_category_id);
		asset_categoryService.update(domain );
		Asset_categoryDTO dto = asset_categoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.asset_categoryService.getAssetCategoryByEntities(this.asset_categoryMapping.toDomain(#asset_categorydtos)),'iBizBusinessCentral-Asset_category-Update')")
    @ApiOperation(value = "批量更新Asset Tags", tags = {"Asset Tags" },  notes = "批量更新Asset Tags")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Asset_categoryDTO> asset_categorydtos) {
        asset_categoryService.updateBatch(asset_categoryMapping.toDomain(asset_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.asset_categoryService.get(#asset_category_id),'iBizBusinessCentral-Asset_category-Remove')")
    @ApiOperation(value = "删除Asset Tags", tags = {"Asset Tags" },  notes = "删除Asset Tags")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_categories/{asset_category_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("asset_category_id") Long asset_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(asset_categoryService.remove(asset_category_id));
    }

    @PreAuthorize("hasPermission(this.asset_categoryService.getAssetCategoryByIds(#ids),'iBizBusinessCentral-Asset_category-Remove')")
    @ApiOperation(value = "批量删除Asset Tags", tags = {"Asset Tags" },  notes = "批量删除Asset Tags")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        asset_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.asset_categoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Asset_category-Get')")
    @ApiOperation(value = "获取Asset Tags", tags = {"Asset Tags" },  notes = "获取Asset Tags")
	@RequestMapping(method = RequestMethod.GET, value = "/asset_categories/{asset_category_id}")
    public ResponseEntity<Asset_categoryDTO> get(@PathVariable("asset_category_id") Long asset_category_id) {
        Asset_category domain = asset_categoryService.get(asset_category_id);
        Asset_categoryDTO dto = asset_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Asset Tags草稿", tags = {"Asset Tags" },  notes = "获取Asset Tags草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/asset_categories/getdraft")
    public ResponseEntity<Asset_categoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(asset_categoryMapping.toDto(asset_categoryService.getDraft(new Asset_category())));
    }

    @ApiOperation(value = "检查Asset Tags", tags = {"Asset Tags" },  notes = "检查Asset Tags")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Asset_categoryDTO asset_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(asset_categoryService.checkKey(asset_categoryMapping.toDomain(asset_categorydto)));
    }

    @PreAuthorize("hasPermission(this.asset_categoryMapping.toDomain(#asset_categorydto),'iBizBusinessCentral-Asset_category-Save')")
    @ApiOperation(value = "保存Asset Tags", tags = {"Asset Tags" },  notes = "保存Asset Tags")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_categories/save")
    public ResponseEntity<Boolean> save(@RequestBody Asset_categoryDTO asset_categorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(asset_categoryService.save(asset_categoryMapping.toDomain(asset_categorydto)));
    }

    @PreAuthorize("hasPermission(this.asset_categoryMapping.toDomain(#asset_categorydtos),'iBizBusinessCentral-Asset_category-Save')")
    @ApiOperation(value = "批量保存Asset Tags", tags = {"Asset Tags" },  notes = "批量保存Asset Tags")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_categories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Asset_categoryDTO> asset_categorydtos) {
        asset_categoryService.saveBatch(asset_categoryMapping.toDomain(asset_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Asset_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Asset_category-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Asset Tags" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/asset_categories/fetchdefault")
	public ResponseEntity<List<Asset_categoryDTO>> fetchDefault(Asset_categorySearchContext context) {
        Page<Asset_category> domains = asset_categoryService.searchDefault(context) ;
        List<Asset_categoryDTO> list = asset_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Asset_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Asset_category-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Asset Tags" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/asset_categories/searchdefault")
	public ResponseEntity<Page<Asset_categoryDTO>> searchDefault(@RequestBody Asset_categorySearchContext context) {
        Page<Asset_category> domains = asset_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(asset_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


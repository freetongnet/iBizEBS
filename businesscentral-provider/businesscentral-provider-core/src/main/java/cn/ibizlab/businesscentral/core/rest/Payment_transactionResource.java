package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_transactionService;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_transactionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"付款交易" })
@RestController("Core-payment_transaction")
@RequestMapping("")
public class Payment_transactionResource {

    @Autowired
    public IPayment_transactionService payment_transactionService;

    @Autowired
    @Lazy
    public Payment_transactionMapping payment_transactionMapping;

    @PreAuthorize("hasPermission(this.payment_transactionMapping.toDomain(#payment_transactiondto),'iBizBusinessCentral-Payment_transaction-Create')")
    @ApiOperation(value = "新建付款交易", tags = {"付款交易" },  notes = "新建付款交易")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_transactions")
    public ResponseEntity<Payment_transactionDTO> create(@Validated @RequestBody Payment_transactionDTO payment_transactiondto) {
        Payment_transaction domain = payment_transactionMapping.toDomain(payment_transactiondto);
		payment_transactionService.create(domain);
        Payment_transactionDTO dto = payment_transactionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_transactionMapping.toDomain(#payment_transactiondtos),'iBizBusinessCentral-Payment_transaction-Create')")
    @ApiOperation(value = "批量新建付款交易", tags = {"付款交易" },  notes = "批量新建付款交易")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_transactions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_transactionDTO> payment_transactiondtos) {
        payment_transactionService.createBatch(payment_transactionMapping.toDomain(payment_transactiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "payment_transaction" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.payment_transactionService.get(#payment_transaction_id),'iBizBusinessCentral-Payment_transaction-Update')")
    @ApiOperation(value = "更新付款交易", tags = {"付款交易" },  notes = "更新付款交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_transactions/{payment_transaction_id}")
    public ResponseEntity<Payment_transactionDTO> update(@PathVariable("payment_transaction_id") Long payment_transaction_id, @RequestBody Payment_transactionDTO payment_transactiondto) {
		Payment_transaction domain  = payment_transactionMapping.toDomain(payment_transactiondto);
        domain .setId(payment_transaction_id);
		payment_transactionService.update(domain );
		Payment_transactionDTO dto = payment_transactionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_transactionService.getPaymentTransactionByEntities(this.payment_transactionMapping.toDomain(#payment_transactiondtos)),'iBizBusinessCentral-Payment_transaction-Update')")
    @ApiOperation(value = "批量更新付款交易", tags = {"付款交易" },  notes = "批量更新付款交易")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_transactions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_transactionDTO> payment_transactiondtos) {
        payment_transactionService.updateBatch(payment_transactionMapping.toDomain(payment_transactiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.payment_transactionService.get(#payment_transaction_id),'iBizBusinessCentral-Payment_transaction-Remove')")
    @ApiOperation(value = "删除付款交易", tags = {"付款交易" },  notes = "删除付款交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_transactions/{payment_transaction_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("payment_transaction_id") Long payment_transaction_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_transactionService.remove(payment_transaction_id));
    }

    @PreAuthorize("hasPermission(this.payment_transactionService.getPaymentTransactionByIds(#ids),'iBizBusinessCentral-Payment_transaction-Remove')")
    @ApiOperation(value = "批量删除付款交易", tags = {"付款交易" },  notes = "批量删除付款交易")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_transactions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        payment_transactionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.payment_transactionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Payment_transaction-Get')")
    @ApiOperation(value = "获取付款交易", tags = {"付款交易" },  notes = "获取付款交易")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_transactions/{payment_transaction_id}")
    public ResponseEntity<Payment_transactionDTO> get(@PathVariable("payment_transaction_id") Long payment_transaction_id) {
        Payment_transaction domain = payment_transactionService.get(payment_transaction_id);
        Payment_transactionDTO dto = payment_transactionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取付款交易草稿", tags = {"付款交易" },  notes = "获取付款交易草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_transactions/getdraft")
    public ResponseEntity<Payment_transactionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(payment_transactionMapping.toDto(payment_transactionService.getDraft(new Payment_transaction())));
    }

    @ApiOperation(value = "检查付款交易", tags = {"付款交易" },  notes = "检查付款交易")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_transactions/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Payment_transactionDTO payment_transactiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(payment_transactionService.checkKey(payment_transactionMapping.toDomain(payment_transactiondto)));
    }

    @PreAuthorize("hasPermission(this.payment_transactionMapping.toDomain(#payment_transactiondto),'iBizBusinessCentral-Payment_transaction-Save')")
    @ApiOperation(value = "保存付款交易", tags = {"付款交易" },  notes = "保存付款交易")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_transactions/save")
    public ResponseEntity<Boolean> save(@RequestBody Payment_transactionDTO payment_transactiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(payment_transactionService.save(payment_transactionMapping.toDomain(payment_transactiondto)));
    }

    @PreAuthorize("hasPermission(this.payment_transactionMapping.toDomain(#payment_transactiondtos),'iBizBusinessCentral-Payment_transaction-Save')")
    @ApiOperation(value = "批量保存付款交易", tags = {"付款交易" },  notes = "批量保存付款交易")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_transactions/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Payment_transactionDTO> payment_transactiondtos) {
        payment_transactionService.saveBatch(payment_transactionMapping.toDomain(payment_transactiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_transaction-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_transaction-Get')")
	@ApiOperation(value = "获取数据集", tags = {"付款交易" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/payment_transactions/fetchdefault")
	public ResponseEntity<List<Payment_transactionDTO>> fetchDefault(Payment_transactionSearchContext context) {
        Page<Payment_transaction> domains = payment_transactionService.searchDefault(context) ;
        List<Payment_transactionDTO> list = payment_transactionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_transaction-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_transaction-Get')")
	@ApiOperation(value = "查询数据集", tags = {"付款交易" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/payment_transactions/searchdefault")
	public ResponseEntity<Page<Payment_transactionDTO>> searchDefault(@RequestBody Payment_transactionSearchContext context) {
        Page<Payment_transaction> domains = payment_transactionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_transactionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


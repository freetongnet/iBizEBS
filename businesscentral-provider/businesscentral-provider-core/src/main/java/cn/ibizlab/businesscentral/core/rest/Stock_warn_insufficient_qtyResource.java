package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qtyService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qtySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存不足时发出警告" })
@RestController("Core-stock_warn_insufficient_qty")
@RequestMapping("")
public class Stock_warn_insufficient_qtyResource {

    @Autowired
    public IStock_warn_insufficient_qtyService stock_warn_insufficient_qtyService;

    @Autowired
    @Lazy
    public Stock_warn_insufficient_qtyMapping stock_warn_insufficient_qtyMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-Create-all')")
    @ApiOperation(value = "新建库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "新建库存不足时发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qties")
    public ResponseEntity<Stock_warn_insufficient_qtyDTO> create(@Validated @RequestBody Stock_warn_insufficient_qtyDTO stock_warn_insufficient_qtydto) {
        Stock_warn_insufficient_qty domain = stock_warn_insufficient_qtyMapping.toDomain(stock_warn_insufficient_qtydto);
		stock_warn_insufficient_qtyService.create(domain);
        Stock_warn_insufficient_qtyDTO dto = stock_warn_insufficient_qtyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-Create-all')")
    @ApiOperation(value = "批量新建库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "批量新建库存不足时发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qties/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_warn_insufficient_qtyDTO> stock_warn_insufficient_qtydtos) {
        stock_warn_insufficient_qtyService.createBatch(stock_warn_insufficient_qtyMapping.toDomain(stock_warn_insufficient_qtydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-Update-all')")
    @ApiOperation(value = "更新库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "更新库存不足时发出警告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qties/{stock_warn_insufficient_qty_id}")
    public ResponseEntity<Stock_warn_insufficient_qtyDTO> update(@PathVariable("stock_warn_insufficient_qty_id") Long stock_warn_insufficient_qty_id, @RequestBody Stock_warn_insufficient_qtyDTO stock_warn_insufficient_qtydto) {
		Stock_warn_insufficient_qty domain  = stock_warn_insufficient_qtyMapping.toDomain(stock_warn_insufficient_qtydto);
        domain .setId(stock_warn_insufficient_qty_id);
		stock_warn_insufficient_qtyService.update(domain );
		Stock_warn_insufficient_qtyDTO dto = stock_warn_insufficient_qtyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-Update-all')")
    @ApiOperation(value = "批量更新库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "批量更新库存不足时发出警告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qties/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warn_insufficient_qtyDTO> stock_warn_insufficient_qtydtos) {
        stock_warn_insufficient_qtyService.updateBatch(stock_warn_insufficient_qtyMapping.toDomain(stock_warn_insufficient_qtydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-Remove-all')")
    @ApiOperation(value = "删除库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "删除库存不足时发出警告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qties/{stock_warn_insufficient_qty_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_warn_insufficient_qty_id") Long stock_warn_insufficient_qty_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qtyService.remove(stock_warn_insufficient_qty_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-Remove-all')")
    @ApiOperation(value = "批量删除库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "批量删除库存不足时发出警告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qties/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_warn_insufficient_qtyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-Get-all')")
    @ApiOperation(value = "获取库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "获取库存不足时发出警告")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qties/{stock_warn_insufficient_qty_id}")
    public ResponseEntity<Stock_warn_insufficient_qtyDTO> get(@PathVariable("stock_warn_insufficient_qty_id") Long stock_warn_insufficient_qty_id) {
        Stock_warn_insufficient_qty domain = stock_warn_insufficient_qtyService.get(stock_warn_insufficient_qty_id);
        Stock_warn_insufficient_qtyDTO dto = stock_warn_insufficient_qtyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存不足时发出警告草稿", tags = {"库存不足时发出警告" },  notes = "获取库存不足时发出警告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qties/getdraft")
    public ResponseEntity<Stock_warn_insufficient_qtyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qtyMapping.toDto(stock_warn_insufficient_qtyService.getDraft(new Stock_warn_insufficient_qty())));
    }

    @ApiOperation(value = "检查库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "检查库存不足时发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qties/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_warn_insufficient_qtyDTO stock_warn_insufficient_qtydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qtyService.checkKey(stock_warn_insufficient_qtyMapping.toDomain(stock_warn_insufficient_qtydto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-Save-all')")
    @ApiOperation(value = "保存库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "保存库存不足时发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qties/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_warn_insufficient_qtyDTO stock_warn_insufficient_qtydto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qtyService.save(stock_warn_insufficient_qtyMapping.toDomain(stock_warn_insufficient_qtydto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-Save-all')")
    @ApiOperation(value = "批量保存库存不足时发出警告", tags = {"库存不足时发出警告" },  notes = "批量保存库存不足时发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qties/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_warn_insufficient_qtyDTO> stock_warn_insufficient_qtydtos) {
        stock_warn_insufficient_qtyService.saveBatch(stock_warn_insufficient_qtyMapping.toDomain(stock_warn_insufficient_qtydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"库存不足时发出警告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warn_insufficient_qties/fetchdefault")
	public ResponseEntity<List<Stock_warn_insufficient_qtyDTO>> fetchDefault(Stock_warn_insufficient_qtySearchContext context) {
        Page<Stock_warn_insufficient_qty> domains = stock_warn_insufficient_qtyService.searchDefault(context) ;
        List<Stock_warn_insufficient_qtyDTO> list = stock_warn_insufficient_qtyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"库存不足时发出警告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_warn_insufficient_qties/searchdefault")
	public ResponseEntity<Page<Stock_warn_insufficient_qtyDTO>> searchDefault(@RequestBody Stock_warn_insufficient_qtySearchContext context) {
        Page<Stock_warn_insufficient_qty> domains = stock_warn_insufficient_qtyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_warn_insufficient_qtyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_message_subtypeService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_message_subtypeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"消息子类型" })
@RestController("Core-mail_message_subtype")
@RequestMapping("")
public class Mail_message_subtypeResource {

    @Autowired
    public IMail_message_subtypeService mail_message_subtypeService;

    @Autowired
    @Lazy
    public Mail_message_subtypeMapping mail_message_subtypeMapping;

    @PreAuthorize("hasPermission(this.mail_message_subtypeMapping.toDomain(#mail_message_subtypedto),'iBizBusinessCentral-Mail_message_subtype-Create')")
    @ApiOperation(value = "新建消息子类型", tags = {"消息子类型" },  notes = "新建消息子类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes")
    public ResponseEntity<Mail_message_subtypeDTO> create(@Validated @RequestBody Mail_message_subtypeDTO mail_message_subtypedto) {
        Mail_message_subtype domain = mail_message_subtypeMapping.toDomain(mail_message_subtypedto);
		mail_message_subtypeService.create(domain);
        Mail_message_subtypeDTO dto = mail_message_subtypeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_message_subtypeMapping.toDomain(#mail_message_subtypedtos),'iBizBusinessCentral-Mail_message_subtype-Create')")
    @ApiOperation(value = "批量新建消息子类型", tags = {"消息子类型" },  notes = "批量新建消息子类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_message_subtypeDTO> mail_message_subtypedtos) {
        mail_message_subtypeService.createBatch(mail_message_subtypeMapping.toDomain(mail_message_subtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_message_subtype" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_message_subtypeService.get(#mail_message_subtype_id),'iBizBusinessCentral-Mail_message_subtype-Update')")
    @ApiOperation(value = "更新消息子类型", tags = {"消息子类型" },  notes = "更新消息子类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_message_subtypes/{mail_message_subtype_id}")
    public ResponseEntity<Mail_message_subtypeDTO> update(@PathVariable("mail_message_subtype_id") Long mail_message_subtype_id, @RequestBody Mail_message_subtypeDTO mail_message_subtypedto) {
		Mail_message_subtype domain  = mail_message_subtypeMapping.toDomain(mail_message_subtypedto);
        domain .setId(mail_message_subtype_id);
		mail_message_subtypeService.update(domain );
		Mail_message_subtypeDTO dto = mail_message_subtypeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_message_subtypeService.getMailMessageSubtypeByEntities(this.mail_message_subtypeMapping.toDomain(#mail_message_subtypedtos)),'iBizBusinessCentral-Mail_message_subtype-Update')")
    @ApiOperation(value = "批量更新消息子类型", tags = {"消息子类型" },  notes = "批量更新消息子类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_message_subtypes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_message_subtypeDTO> mail_message_subtypedtos) {
        mail_message_subtypeService.updateBatch(mail_message_subtypeMapping.toDomain(mail_message_subtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_message_subtypeService.get(#mail_message_subtype_id),'iBizBusinessCentral-Mail_message_subtype-Remove')")
    @ApiOperation(value = "删除消息子类型", tags = {"消息子类型" },  notes = "删除消息子类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_message_subtypes/{mail_message_subtype_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_message_subtype_id") Long mail_message_subtype_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_message_subtypeService.remove(mail_message_subtype_id));
    }

    @PreAuthorize("hasPermission(this.mail_message_subtypeService.getMailMessageSubtypeByIds(#ids),'iBizBusinessCentral-Mail_message_subtype-Remove')")
    @ApiOperation(value = "批量删除消息子类型", tags = {"消息子类型" },  notes = "批量删除消息子类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_message_subtypes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_message_subtypeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_message_subtypeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_message_subtype-Get')")
    @ApiOperation(value = "获取消息子类型", tags = {"消息子类型" },  notes = "获取消息子类型")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_message_subtypes/{mail_message_subtype_id}")
    public ResponseEntity<Mail_message_subtypeDTO> get(@PathVariable("mail_message_subtype_id") Long mail_message_subtype_id) {
        Mail_message_subtype domain = mail_message_subtypeService.get(mail_message_subtype_id);
        Mail_message_subtypeDTO dto = mail_message_subtypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取消息子类型草稿", tags = {"消息子类型" },  notes = "获取消息子类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_message_subtypes/getdraft")
    public ResponseEntity<Mail_message_subtypeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_message_subtypeMapping.toDto(mail_message_subtypeService.getDraft(new Mail_message_subtype())));
    }

    @ApiOperation(value = "检查消息子类型", tags = {"消息子类型" },  notes = "检查消息子类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_message_subtypeDTO mail_message_subtypedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_message_subtypeService.checkKey(mail_message_subtypeMapping.toDomain(mail_message_subtypedto)));
    }

    @PreAuthorize("hasPermission(this.mail_message_subtypeMapping.toDomain(#mail_message_subtypedto),'iBizBusinessCentral-Mail_message_subtype-Save')")
    @ApiOperation(value = "保存消息子类型", tags = {"消息子类型" },  notes = "保存消息子类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_message_subtypeDTO mail_message_subtypedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_message_subtypeService.save(mail_message_subtypeMapping.toDomain(mail_message_subtypedto)));
    }

    @PreAuthorize("hasPermission(this.mail_message_subtypeMapping.toDomain(#mail_message_subtypedtos),'iBizBusinessCentral-Mail_message_subtype-Save')")
    @ApiOperation(value = "批量保存消息子类型", tags = {"消息子类型" },  notes = "批量保存消息子类型")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_message_subtypes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_message_subtypeDTO> mail_message_subtypedtos) {
        mail_message_subtypeService.saveBatch(mail_message_subtypeMapping.toDomain(mail_message_subtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message_subtype-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_message_subtype-Get')")
	@ApiOperation(value = "获取数据集", tags = {"消息子类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_message_subtypes/fetchdefault")
	public ResponseEntity<List<Mail_message_subtypeDTO>> fetchDefault(Mail_message_subtypeSearchContext context) {
        Page<Mail_message_subtype> domains = mail_message_subtypeService.searchDefault(context) ;
        List<Mail_message_subtypeDTO> list = mail_message_subtypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message_subtype-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_message_subtype-Get')")
	@ApiOperation(value = "查询数据集", tags = {"消息子类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_message_subtypes/searchdefault")
	public ResponseEntity<Page<Mail_message_subtypeDTO>> searchDefault(@RequestBody Mail_message_subtypeSearchContext context) {
        Page<Mail_message_subtype> domains = mail_message_subtypeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_message_subtypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message_subtype-searchDefaultEx-all') and hasPermission(#context,'iBizBusinessCentral-Mail_message_subtype-Get')")
	@ApiOperation(value = "获取数据查询2", tags = {"消息子类型" } ,notes = "获取数据查询2")
    @RequestMapping(method= RequestMethod.GET , value="/mail_message_subtypes/fetchdefaultex")
	public ResponseEntity<List<Mail_message_subtypeDTO>> fetchDefaultEx(Mail_message_subtypeSearchContext context) {
        Page<Mail_message_subtype> domains = mail_message_subtypeService.searchDefaultEx(context) ;
        List<Mail_message_subtypeDTO> list = mail_message_subtypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message_subtype-searchDefaultEx-all') and hasPermission(#context,'iBizBusinessCentral-Mail_message_subtype-Get')")
	@ApiOperation(value = "查询数据查询2", tags = {"消息子类型" } ,notes = "查询数据查询2")
    @RequestMapping(method= RequestMethod.POST , value="/mail_message_subtypes/searchdefaultex")
	public ResponseEntity<Page<Mail_message_subtypeDTO>> searchDefaultEx(@RequestBody Mail_message_subtypeSearchContext context) {
        Page<Mail_message_subtype> domains = mail_message_subtypeService.searchDefaultEx(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_message_subtypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


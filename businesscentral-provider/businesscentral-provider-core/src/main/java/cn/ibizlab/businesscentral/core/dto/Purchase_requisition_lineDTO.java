package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Purchase_requisition_lineDTO]
 */
@Data
public class Purchase_requisition_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SCHEDULE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedule_date" , format="yyyy-MM-dd")
    @JsonProperty("schedule_date")
    private Timestamp scheduleDate;

    /**
     * 属性 [QTY_ORDERED]
     *
     */
    @JSONField(name = "qty_ordered")
    @JsonProperty("qty_ordered")
    private Integer qtyOrdered;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;

    /**
     * 属性 [COMPANY_NAME]
     *
     */
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String companyName;

    /**
     * 属性 [REQUISITION_NAME]
     *
     */
    @JSONField(name = "requisition_name")
    @JsonProperty("requisition_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String requisitionName;

    /**
     * 属性 [PRODUCT_UOM_NAME]
     *
     */
    @JSONField(name = "product_uom_name")
    @JsonProperty("product_uom_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String productUomName;

    /**
     * 属性 [MOVE_DEST_NAME]
     *
     */
    @JSONField(name = "move_dest_name")
    @JsonProperty("move_dest_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String moveDestName;

    /**
     * 属性 [ACCOUNT_ANALYTIC_NAME]
     *
     */
    @JSONField(name = "account_analytic_name")
    @JsonProperty("account_analytic_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String accountAnalyticName;

    /**
     * 属性 [WRITE_UNAME]
     *
     */
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String writeUname;

    /**
     * 属性 [PRODUCT_NAME]
     *
     */
    @JSONField(name = "product_name")
    @JsonProperty("product_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String productName;

    /**
     * 属性 [CREATE_UNAME]
     *
     */
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String createUname;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productUomId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [MOVE_DEST_ID]
     *
     */
    @JSONField(name = "move_dest_id")
    @JsonProperty("move_dest_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long moveDestId;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountAnalyticId;

    /**
     * 属性 [REQUISITION_ID]
     *
     */
    @JSONField(name = "requisition_id")
    @JsonProperty("requisition_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long requisitionId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;


    /**
     * 设置 [PRODUCT_QTY]
     */
    public void setProductQty(Double  productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [WRITE_DATE]
     */
    public void setWriteDate(Timestamp  writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 设置 [CREATE_DATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 设置 [SCHEDULE_DATE]
     */
    public void setScheduleDate(Timestamp  scheduleDate){
        this.scheduleDate = scheduleDate ;
        this.modify("schedule_date",scheduleDate);
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    public void setPriceUnit(Double  priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [WRITE_UID]
     */
    public void setWriteUid(Long  writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Long  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [CREATE_UID]
     */
    public void setCreateUid(Long  createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }

    /**
     * 设置 [MOVE_DEST_ID]
     */
    public void setMoveDestId(Long  moveDestId){
        this.moveDestId = moveDestId ;
        this.modify("move_dest_id",moveDestId);
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    public void setAccountAnalyticId(Long  accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }

    /**
     * 设置 [REQUISITION_ID]
     */
    public void setRequisitionId(Long  requisitionId){
        this.requisitionId = requisitionId ;
        this.modify("requisition_id",requisitionId);
    }


}



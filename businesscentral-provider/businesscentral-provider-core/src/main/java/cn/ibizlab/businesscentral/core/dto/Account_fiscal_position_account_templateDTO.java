package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_fiscal_position_account_templateDTO]
 */
@Data
public class Account_fiscal_position_account_templateDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [POSITION_ID_TEXT]
     *
     */
    @JSONField(name = "position_id_text")
    @JsonProperty("position_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String positionIdText;

    /**
     * 属性 [ACCOUNT_DEST_ID_TEXT]
     *
     */
    @JSONField(name = "account_dest_id_text")
    @JsonProperty("account_dest_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountDestIdText;

    /**
     * 属性 [ACCOUNT_SRC_ID_TEXT]
     *
     */
    @JSONField(name = "account_src_id_text")
    @JsonProperty("account_src_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountSrcIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [ACCOUNT_DEST_ID]
     *
     */
    @JSONField(name = "account_dest_id")
    @JsonProperty("account_dest_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[科目目标]不允许为空!")
    private Long accountDestId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [ACCOUNT_SRC_ID]
     *
     */
    @JSONField(name = "account_src_id")
    @JsonProperty("account_src_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[科目来源]不允许为空!")
    private Long accountSrcId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [POSITION_ID]
     *
     */
    @JSONField(name = "position_id")
    @JsonProperty("position_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[财务映射]不允许为空!")
    private Long positionId;


    /**
     * 设置 [ACCOUNT_DEST_ID]
     */
    public void setAccountDestId(Long  accountDestId){
        this.accountDestId = accountDestId ;
        this.modify("account_dest_id",accountDestId);
    }

    /**
     * 设置 [ACCOUNT_SRC_ID]
     */
    public void setAccountSrcId(Long  accountSrcId){
        this.accountSrcId = accountSrcId ;
        this.modify("account_src_id",accountSrcId);
    }

    /**
     * 设置 [POSITION_ID]
     */
    public void setPositionId(Long  positionId){
        this.positionId = positionId ;
        this.modify("position_id",positionId);
    }


}



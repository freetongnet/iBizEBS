package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventoryService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_inventorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存" })
@RestController("Core-stock_inventory")
@RequestMapping("")
public class Stock_inventoryResource {

    @Autowired
    public IStock_inventoryService stock_inventoryService;

    @Autowired
    @Lazy
    public Stock_inventoryMapping stock_inventoryMapping;

    @PreAuthorize("hasPermission(this.stock_inventoryMapping.toDomain(#stock_inventorydto),'iBizBusinessCentral-Stock_inventory-Create')")
    @ApiOperation(value = "新建库存", tags = {"库存" },  notes = "新建库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventories")
    public ResponseEntity<Stock_inventoryDTO> create(@Validated @RequestBody Stock_inventoryDTO stock_inventorydto) {
        Stock_inventory domain = stock_inventoryMapping.toDomain(stock_inventorydto);
		stock_inventoryService.create(domain);
        Stock_inventoryDTO dto = stock_inventoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_inventoryMapping.toDomain(#stock_inventorydtos),'iBizBusinessCentral-Stock_inventory-Create')")
    @ApiOperation(value = "批量新建库存", tags = {"库存" },  notes = "批量新建库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_inventoryDTO> stock_inventorydtos) {
        stock_inventoryService.createBatch(stock_inventoryMapping.toDomain(stock_inventorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_inventory" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_inventoryService.get(#stock_inventory_id),'iBizBusinessCentral-Stock_inventory-Update')")
    @ApiOperation(value = "更新库存", tags = {"库存" },  notes = "更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_inventories/{stock_inventory_id}")
    public ResponseEntity<Stock_inventoryDTO> update(@PathVariable("stock_inventory_id") Long stock_inventory_id, @RequestBody Stock_inventoryDTO stock_inventorydto) {
		Stock_inventory domain  = stock_inventoryMapping.toDomain(stock_inventorydto);
        domain .setId(stock_inventory_id);
		stock_inventoryService.update(domain );
		Stock_inventoryDTO dto = stock_inventoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_inventoryService.getStockInventoryByEntities(this.stock_inventoryMapping.toDomain(#stock_inventorydtos)),'iBizBusinessCentral-Stock_inventory-Update')")
    @ApiOperation(value = "批量更新库存", tags = {"库存" },  notes = "批量更新库存")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_inventories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_inventoryDTO> stock_inventorydtos) {
        stock_inventoryService.updateBatch(stock_inventoryMapping.toDomain(stock_inventorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_inventoryService.get(#stock_inventory_id),'iBizBusinessCentral-Stock_inventory-Remove')")
    @ApiOperation(value = "删除库存", tags = {"库存" },  notes = "删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventories/{stock_inventory_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_inventory_id") Long stock_inventory_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_inventoryService.remove(stock_inventory_id));
    }

    @PreAuthorize("hasPermission(this.stock_inventoryService.getStockInventoryByIds(#ids),'iBizBusinessCentral-Stock_inventory-Remove')")
    @ApiOperation(value = "批量删除库存", tags = {"库存" },  notes = "批量删除库存")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_inventoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_inventoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_inventory-Get')")
    @ApiOperation(value = "获取库存", tags = {"库存" },  notes = "获取库存")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_inventories/{stock_inventory_id}")
    public ResponseEntity<Stock_inventoryDTO> get(@PathVariable("stock_inventory_id") Long stock_inventory_id) {
        Stock_inventory domain = stock_inventoryService.get(stock_inventory_id);
        Stock_inventoryDTO dto = stock_inventoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存草稿", tags = {"库存" },  notes = "获取库存草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_inventories/getdraft")
    public ResponseEntity<Stock_inventoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_inventoryMapping.toDto(stock_inventoryService.getDraft(new Stock_inventory())));
    }

    @ApiOperation(value = "检查库存", tags = {"库存" },  notes = "检查库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_inventoryDTO stock_inventorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_inventoryService.checkKey(stock_inventoryMapping.toDomain(stock_inventorydto)));
    }

    @PreAuthorize("hasPermission(this.stock_inventoryMapping.toDomain(#stock_inventorydto),'iBizBusinessCentral-Stock_inventory-Save')")
    @ApiOperation(value = "保存库存", tags = {"库存" },  notes = "保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_inventoryDTO stock_inventorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_inventoryService.save(stock_inventoryMapping.toDomain(stock_inventorydto)));
    }

    @PreAuthorize("hasPermission(this.stock_inventoryMapping.toDomain(#stock_inventorydtos),'iBizBusinessCentral-Stock_inventory-Save')")
    @ApiOperation(value = "批量保存库存", tags = {"库存" },  notes = "批量保存库存")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_inventoryDTO> stock_inventorydtos) {
        stock_inventoryService.saveBatch(stock_inventoryMapping.toDomain(stock_inventorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_inventory-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_inventory-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_inventories/fetchdefault")
	public ResponseEntity<List<Stock_inventoryDTO>> fetchDefault(Stock_inventorySearchContext context) {
        Page<Stock_inventory> domains = stock_inventoryService.searchDefault(context) ;
        List<Stock_inventoryDTO> list = stock_inventoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_inventory-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_inventory-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_inventories/searchdefault")
	public ResponseEntity<Page<Stock_inventoryDTO>> searchDefault(@RequestBody Stock_inventorySearchContext context) {
        Page<Stock_inventory> domains = stock_inventoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_inventoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


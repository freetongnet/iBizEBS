package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_contactsService;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_contactsSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"日历联系人" })
@RestController("Core-calendar_contacts")
@RequestMapping("")
public class Calendar_contactsResource {

    @Autowired
    public ICalendar_contactsService calendar_contactsService;

    @Autowired
    @Lazy
    public Calendar_contactsMapping calendar_contactsMapping;

    @PreAuthorize("hasPermission(this.calendar_contactsMapping.toDomain(#calendar_contactsdto),'iBizBusinessCentral-Calendar_contacts-Create')")
    @ApiOperation(value = "新建日历联系人", tags = {"日历联系人" },  notes = "新建日历联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts")
    public ResponseEntity<Calendar_contactsDTO> create(@Validated @RequestBody Calendar_contactsDTO calendar_contactsdto) {
        Calendar_contacts domain = calendar_contactsMapping.toDomain(calendar_contactsdto);
		calendar_contactsService.create(domain);
        Calendar_contactsDTO dto = calendar_contactsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_contactsMapping.toDomain(#calendar_contactsdtos),'iBizBusinessCentral-Calendar_contacts-Create')")
    @ApiOperation(value = "批量新建日历联系人", tags = {"日历联系人" },  notes = "批量新建日历联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_contactsDTO> calendar_contactsdtos) {
        calendar_contactsService.createBatch(calendar_contactsMapping.toDomain(calendar_contactsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "calendar_contacts" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.calendar_contactsService.get(#calendar_contacts_id),'iBizBusinessCentral-Calendar_contacts-Update')")
    @ApiOperation(value = "更新日历联系人", tags = {"日历联系人" },  notes = "更新日历联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_contacts/{calendar_contacts_id}")
    public ResponseEntity<Calendar_contactsDTO> update(@PathVariable("calendar_contacts_id") Long calendar_contacts_id, @RequestBody Calendar_contactsDTO calendar_contactsdto) {
		Calendar_contacts domain  = calendar_contactsMapping.toDomain(calendar_contactsdto);
        domain .setId(calendar_contacts_id);
		calendar_contactsService.update(domain );
		Calendar_contactsDTO dto = calendar_contactsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_contactsService.getCalendarContactsByEntities(this.calendar_contactsMapping.toDomain(#calendar_contactsdtos)),'iBizBusinessCentral-Calendar_contacts-Update')")
    @ApiOperation(value = "批量更新日历联系人", tags = {"日历联系人" },  notes = "批量更新日历联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_contacts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_contactsDTO> calendar_contactsdtos) {
        calendar_contactsService.updateBatch(calendar_contactsMapping.toDomain(calendar_contactsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.calendar_contactsService.get(#calendar_contacts_id),'iBizBusinessCentral-Calendar_contacts-Remove')")
    @ApiOperation(value = "删除日历联系人", tags = {"日历联系人" },  notes = "删除日历联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_contacts/{calendar_contacts_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("calendar_contacts_id") Long calendar_contacts_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_contactsService.remove(calendar_contacts_id));
    }

    @PreAuthorize("hasPermission(this.calendar_contactsService.getCalendarContactsByIds(#ids),'iBizBusinessCentral-Calendar_contacts-Remove')")
    @ApiOperation(value = "批量删除日历联系人", tags = {"日历联系人" },  notes = "批量删除日历联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_contacts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        calendar_contactsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.calendar_contactsMapping.toDomain(returnObject.body),'iBizBusinessCentral-Calendar_contacts-Get')")
    @ApiOperation(value = "获取日历联系人", tags = {"日历联系人" },  notes = "获取日历联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_contacts/{calendar_contacts_id}")
    public ResponseEntity<Calendar_contactsDTO> get(@PathVariable("calendar_contacts_id") Long calendar_contacts_id) {
        Calendar_contacts domain = calendar_contactsService.get(calendar_contacts_id);
        Calendar_contactsDTO dto = calendar_contactsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取日历联系人草稿", tags = {"日历联系人" },  notes = "获取日历联系人草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_contacts/getdraft")
    public ResponseEntity<Calendar_contactsDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_contactsMapping.toDto(calendar_contactsService.getDraft(new Calendar_contacts())));
    }

    @ApiOperation(value = "检查日历联系人", tags = {"日历联系人" },  notes = "检查日历联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Calendar_contactsDTO calendar_contactsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(calendar_contactsService.checkKey(calendar_contactsMapping.toDomain(calendar_contactsdto)));
    }

    @PreAuthorize("hasPermission(this.calendar_contactsMapping.toDomain(#calendar_contactsdto),'iBizBusinessCentral-Calendar_contacts-Save')")
    @ApiOperation(value = "保存日历联系人", tags = {"日历联系人" },  notes = "保存日历联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts/save")
    public ResponseEntity<Boolean> save(@RequestBody Calendar_contactsDTO calendar_contactsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_contactsService.save(calendar_contactsMapping.toDomain(calendar_contactsdto)));
    }

    @PreAuthorize("hasPermission(this.calendar_contactsMapping.toDomain(#calendar_contactsdtos),'iBizBusinessCentral-Calendar_contacts-Save')")
    @ApiOperation(value = "批量保存日历联系人", tags = {"日历联系人" },  notes = "批量保存日历联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Calendar_contactsDTO> calendar_contactsdtos) {
        calendar_contactsService.saveBatch(calendar_contactsMapping.toDomain(calendar_contactsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_contacts-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_contacts-Get')")
	@ApiOperation(value = "获取数据集", tags = {"日历联系人" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_contacts/fetchdefault")
	public ResponseEntity<List<Calendar_contactsDTO>> fetchDefault(Calendar_contactsSearchContext context) {
        Page<Calendar_contacts> domains = calendar_contactsService.searchDefault(context) ;
        List<Calendar_contactsDTO> list = calendar_contactsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_contacts-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_contacts-Get')")
	@ApiOperation(value = "查询数据集", tags = {"日历联系人" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/calendar_contacts/searchdefault")
	public ResponseEntity<Page<Calendar_contactsDTO>> searchDefault(@RequestBody Calendar_contactsSearchContext context) {
        Page<Calendar_contacts> domains = calendar_contactsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_contactsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


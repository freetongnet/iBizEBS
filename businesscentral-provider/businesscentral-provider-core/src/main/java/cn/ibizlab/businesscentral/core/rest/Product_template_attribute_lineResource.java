package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_line;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_lineService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_template_attribute_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品模板属性明细行" })
@RestController("Core-product_template_attribute_line")
@RequestMapping("")
public class Product_template_attribute_lineResource {

    @Autowired
    public IProduct_template_attribute_lineService product_template_attribute_lineService;

    @Autowired
    @Lazy
    public Product_template_attribute_lineMapping product_template_attribute_lineMapping;

    @PreAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedto),'iBizBusinessCentral-Product_template_attribute_line-Create')")
    @ApiOperation(value = "新建产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "新建产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines")
    public ResponseEntity<Product_template_attribute_lineDTO> create(@Validated @RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
        Product_template_attribute_line domain = product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto);
		product_template_attribute_lineService.create(domain);
        Product_template_attribute_lineDTO dto = product_template_attribute_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedtos),'iBizBusinessCentral-Product_template_attribute_line-Create')")
    @ApiOperation(value = "批量新建产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "批量新建产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        product_template_attribute_lineService.createBatch(product_template_attribute_lineMapping.toDomain(product_template_attribute_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_template_attribute_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_template_attribute_lineService.get(#product_template_attribute_line_id),'iBizBusinessCentral-Product_template_attribute_line-Update')")
    @ApiOperation(value = "更新产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "更新产品模板属性明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_lines/{product_template_attribute_line_id}")
    public ResponseEntity<Product_template_attribute_lineDTO> update(@PathVariable("product_template_attribute_line_id") Long product_template_attribute_line_id, @RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
		Product_template_attribute_line domain  = product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto);
        domain .setId(product_template_attribute_line_id);
		product_template_attribute_lineService.update(domain );
		Product_template_attribute_lineDTO dto = product_template_attribute_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineService.getProductTemplateAttributeLineByEntities(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedtos)),'iBizBusinessCentral-Product_template_attribute_line-Update')")
    @ApiOperation(value = "批量更新产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "批量更新产品模板属性明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        product_template_attribute_lineService.updateBatch(product_template_attribute_lineMapping.toDomain(product_template_attribute_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineService.get(#product_template_attribute_line_id),'iBizBusinessCentral-Product_template_attribute_line-Remove')")
    @ApiOperation(value = "删除产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "删除产品模板属性明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_lines/{product_template_attribute_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_template_attribute_line_id") Long product_template_attribute_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_lineService.remove(product_template_attribute_line_id));
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineService.getProductTemplateAttributeLineByIds(#ids),'iBizBusinessCentral-Product_template_attribute_line-Remove')")
    @ApiOperation(value = "批量删除产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "批量删除产品模板属性明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_template_attribute_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_template_attribute_line-Get')")
    @ApiOperation(value = "获取产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "获取产品模板属性明细行")
	@RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_lines/{product_template_attribute_line_id}")
    public ResponseEntity<Product_template_attribute_lineDTO> get(@PathVariable("product_template_attribute_line_id") Long product_template_attribute_line_id) {
        Product_template_attribute_line domain = product_template_attribute_lineService.get(product_template_attribute_line_id);
        Product_template_attribute_lineDTO dto = product_template_attribute_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品模板属性明细行草稿", tags = {"产品模板属性明细行" },  notes = "获取产品模板属性明细行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_lines/getdraft")
    public ResponseEntity<Product_template_attribute_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_lineMapping.toDto(product_template_attribute_lineService.getDraft(new Product_template_attribute_line())));
    }

    @ApiOperation(value = "检查产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "检查产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_lineService.checkKey(product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto)));
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedto),'iBizBusinessCentral-Product_template_attribute_line-Save')")
    @ApiOperation(value = "保存产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "保存产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_lineService.save(product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto)));
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedtos),'iBizBusinessCentral-Product_template_attribute_line-Save')")
    @ApiOperation(value = "批量保存产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "批量保存产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        product_template_attribute_lineService.saveBatch(product_template_attribute_lineMapping.toDomain(product_template_attribute_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template_attribute_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_template_attribute_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品模板属性明细行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_template_attribute_lines/fetchdefault")
	public ResponseEntity<List<Product_template_attribute_lineDTO>> fetchDefault(Product_template_attribute_lineSearchContext context) {
        Page<Product_template_attribute_line> domains = product_template_attribute_lineService.searchDefault(context) ;
        List<Product_template_attribute_lineDTO> list = product_template_attribute_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template_attribute_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_template_attribute_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品模板属性明细行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_template_attribute_lines/searchdefault")
	public ResponseEntity<Page<Product_template_attribute_lineDTO>> searchDefault(@RequestBody Product_template_attribute_lineSearchContext context) {
        Page<Product_template_attribute_line> domains = product_template_attribute_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_template_attribute_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedto),'iBizBusinessCentral-Product_template_attribute_line-Create')")
    @ApiOperation(value = "根据产品模板建立产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板建立产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_template_attribute_lines")
    public ResponseEntity<Product_template_attribute_lineDTO> createByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
        Product_template_attribute_line domain = product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto);
        domain.setProductTmplId(product_template_id);
		product_template_attribute_lineService.create(domain);
        Product_template_attribute_lineDTO dto = product_template_attribute_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedtos),'iBizBusinessCentral-Product_template_attribute_line-Create')")
    @ApiOperation(value = "根据产品模板批量建立产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板批量建立产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_template_attribute_lines/batch")
    public ResponseEntity<Boolean> createBatchByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        List<Product_template_attribute_line> domainlist=product_template_attribute_lineMapping.toDomain(product_template_attribute_linedtos);
        for(Product_template_attribute_line domain:domainlist){
            domain.setProductTmplId(product_template_id);
        }
        product_template_attribute_lineService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_template_attribute_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_template_attribute_lineService.get(#product_template_attribute_line_id),'iBizBusinessCentral-Product_template_attribute_line-Update')")
    @ApiOperation(value = "根据产品模板更新产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板更新产品模板属性明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_template_attribute_lines/{product_template_attribute_line_id}")
    public ResponseEntity<Product_template_attribute_lineDTO> updateByProduct_template(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_template_attribute_line_id") Long product_template_attribute_line_id, @RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
        Product_template_attribute_line domain = product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto);
        domain.setProductTmplId(product_template_id);
        domain.setId(product_template_attribute_line_id);
		product_template_attribute_lineService.update(domain);
        Product_template_attribute_lineDTO dto = product_template_attribute_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineService.getProductTemplateAttributeLineByEntities(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedtos)),'iBizBusinessCentral-Product_template_attribute_line-Update')")
    @ApiOperation(value = "根据产品模板批量更新产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板批量更新产品模板属性明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_template_attribute_lines/batch")
    public ResponseEntity<Boolean> updateBatchByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        List<Product_template_attribute_line> domainlist=product_template_attribute_lineMapping.toDomain(product_template_attribute_linedtos);
        for(Product_template_attribute_line domain:domainlist){
            domain.setProductTmplId(product_template_id);
        }
        product_template_attribute_lineService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineService.get(#product_template_attribute_line_id),'iBizBusinessCentral-Product_template_attribute_line-Remove')")
    @ApiOperation(value = "根据产品模板删除产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板删除产品模板属性明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_template_attribute_lines/{product_template_attribute_line_id}")
    public ResponseEntity<Boolean> removeByProduct_template(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_template_attribute_line_id") Long product_template_attribute_line_id) {
		return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_lineService.remove(product_template_attribute_line_id));
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineService.getProductTemplateAttributeLineByIds(#ids),'iBizBusinessCentral-Product_template_attribute_line-Remove')")
    @ApiOperation(value = "根据产品模板批量删除产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板批量删除产品模板属性明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_template_attribute_lines/batch")
    public ResponseEntity<Boolean> removeBatchByProduct_template(@RequestBody List<Long> ids) {
        product_template_attribute_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_template_attribute_line-Get')")
    @ApiOperation(value = "根据产品模板获取产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板获取产品模板属性明细行")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_template_attribute_lines/{product_template_attribute_line_id}")
    public ResponseEntity<Product_template_attribute_lineDTO> getByProduct_template(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_template_attribute_line_id") Long product_template_attribute_line_id) {
        Product_template_attribute_line domain = product_template_attribute_lineService.get(product_template_attribute_line_id);
        Product_template_attribute_lineDTO dto = product_template_attribute_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品模板获取产品模板属性明细行草稿", tags = {"产品模板属性明细行" },  notes = "根据产品模板获取产品模板属性明细行草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_template_attribute_lines/getdraft")
    public ResponseEntity<Product_template_attribute_lineDTO> getDraftByProduct_template(@PathVariable("product_template_id") Long product_template_id) {
        Product_template_attribute_line domain = new Product_template_attribute_line();
        domain.setProductTmplId(product_template_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_lineMapping.toDto(product_template_attribute_lineService.getDraft(domain)));
    }

    @ApiOperation(value = "根据产品模板检查产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板检查产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_template_attribute_lines/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_lineService.checkKey(product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto)));
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedto),'iBizBusinessCentral-Product_template_attribute_line-Save')")
    @ApiOperation(value = "根据产品模板保存产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板保存产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_template_attribute_lines/save")
    public ResponseEntity<Boolean> saveByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
        Product_template_attribute_line domain = product_template_attribute_lineMapping.toDomain(product_template_attribute_linedto);
        domain.setProductTmplId(product_template_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_lineService.save(domain));
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_lineMapping.toDomain(#product_template_attribute_linedtos),'iBizBusinessCentral-Product_template_attribute_line-Save')")
    @ApiOperation(value = "根据产品模板批量保存产品模板属性明细行", tags = {"产品模板属性明细行" },  notes = "根据产品模板批量保存产品模板属性明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_template_attribute_lines/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        List<Product_template_attribute_line> domainlist=product_template_attribute_lineMapping.toDomain(product_template_attribute_linedtos);
        for(Product_template_attribute_line domain:domainlist){
             domain.setProductTmplId(product_template_id);
        }
        product_template_attribute_lineService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template_attribute_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_template_attribute_line-Get')")
	@ApiOperation(value = "根据产品模板获取数据集", tags = {"产品模板属性明细行" } ,notes = "根据产品模板获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/{product_template_id}/product_template_attribute_lines/fetchdefault")
	public ResponseEntity<List<Product_template_attribute_lineDTO>> fetchProduct_template_attribute_lineDefaultByProduct_template(@PathVariable("product_template_id") Long product_template_id,Product_template_attribute_lineSearchContext context) {
        context.setN_product_tmpl_id_eq(product_template_id);
        Page<Product_template_attribute_line> domains = product_template_attribute_lineService.searchDefault(context) ;
        List<Product_template_attribute_lineDTO> list = product_template_attribute_lineMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template_attribute_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_template_attribute_line-Get')")
	@ApiOperation(value = "根据产品模板查询数据集", tags = {"产品模板属性明细行" } ,notes = "根据产品模板查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/{product_template_id}/product_template_attribute_lines/searchdefault")
	public ResponseEntity<Page<Product_template_attribute_lineDTO>> searchProduct_template_attribute_lineDefaultByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_template_attribute_lineSearchContext context) {
        context.setN_product_tmpl_id_eq(product_template_id);
        Page<Product_template_attribute_line> domains = product_template_attribute_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_template_attribute_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Hr_applicantDTO]
 */
@Data
public class Hr_applicantDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String priority;

    /**
     * 属性 [SALARY_EXPECTED]
     *
     */
    @JSONField(name = "salary_expected")
    @JsonProperty("salary_expected")
    private Double salaryExpected;

    /**
     * 属性 [CATEG_IDS]
     *
     */
    @JSONField(name = "categ_ids")
    @JsonProperty("categ_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String categIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [SALARY_PROPOSED]
     *
     */
    @JSONField(name = "salary_proposed")
    @JsonProperty("salary_proposed")
    private Double salaryProposed;

    /**
     * 属性 [ATTACHMENT_NUMBER]
     *
     */
    @JSONField(name = "attachment_number")
    @JsonProperty("attachment_number")
    private Integer attachmentNumber;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String description;

    /**
     * 属性 [PARTNER_PHONE]
     *
     */
    @JSONField(name = "partner_phone")
    @JsonProperty("partner_phone")
    @Size(min = 0, max = 32, message = "内容长度必须小于等于[32]")
    private String partnerPhone;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [KANBAN_STATE]
     *
     */
    @JSONField(name = "kanban_state")
    @JsonProperty("kanban_state")
    @NotBlank(message = "[看板状态]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String kanbanState;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [DATE_OPEN]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_open" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_open")
    private Timestamp dateOpen;

    /**
     * 属性 [DAY_OPEN]
     *
     */
    @JSONField(name = "day_open")
    @JsonProperty("day_open")
    private Double dayOpen;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [AVAILABILITY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "availability" , format="yyyy-MM-dd")
    @JsonProperty("availability")
    private Timestamp availability;

    /**
     * 属性 [SALARY_EXPECTED_EXTRA]
     *
     */
    @JSONField(name = "salary_expected_extra")
    @JsonProperty("salary_expected_extra")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String salaryExpectedExtra;

    /**
     * 属性 [DATE_CLOSED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_closed" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_closed")
    private Timestamp dateClosed;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [PARTNER_MOBILE]
     *
     */
    @JSONField(name = "partner_mobile")
    @JsonProperty("partner_mobile")
    @Size(min = 0, max = 32, message = "内容长度必须小于等于[32]")
    private String partnerMobile;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [SALARY_PROPOSED_EXTRA]
     *
     */
    @JSONField(name = "salary_proposed_extra")
    @JsonProperty("salary_proposed_extra")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String salaryProposedExtra;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    @Size(min = 0, max = 128, message = "内容长度必须小于等于[128]")
    private String emailFrom;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[主题/应用 名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [DATE_LAST_STAGE_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_last_stage_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_last_stage_update")
    private Timestamp dateLastStageUpdate;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [REFERENCE]
     *
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String reference;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String attachmentIds;

    /**
     * 属性 [EMAIL_CC]
     *
     */
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    @Size(min = 0, max = 252, message = "内容长度必须小于等于[252]")
    private String emailCc;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [PARTNER_NAME]
     *
     */
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String partnerName;

    /**
     * 属性 [DAY_CLOSE]
     *
     */
    @JSONField(name = "day_close")
    @JsonProperty("day_close")
    private Double dayClose;

    /**
     * 属性 [PROBABILITY]
     *
     */
    @JSONField(name = "probability")
    @JsonProperty("probability")
    private Double probability;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [DELAY_CLOSE]
     *
     */
    @JSONField(name = "delay_close")
    @JsonProperty("delay_close")
    private Double delayClose;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sourceIdText;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String campaignIdText;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String departmentIdText;

    /**
     * 属性 [LAST_STAGE_ID_TEXT]
     *
     */
    @JSONField(name = "last_stage_id_text")
    @JsonProperty("last_stage_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lastStageIdText;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String stageIdText;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mediumIdText;

    /**
     * 属性 [LEGEND_NORMAL]
     *
     */
    @JSONField(name = "legend_normal")
    @JsonProperty("legend_normal")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String legendNormal;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "type_id_text")
    @JsonProperty("type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String typeIdText;

    /**
     * 属性 [JOB_ID_TEXT]
     *
     */
    @JSONField(name = "job_id_text")
    @JsonProperty("job_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String jobIdText;

    /**
     * 属性 [LEGEND_DONE]
     *
     */
    @JSONField(name = "legend_done")
    @JsonProperty("legend_done")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String legendDone;

    /**
     * 属性 [LEGEND_BLOCKED]
     *
     */
    @JSONField(name = "legend_blocked")
    @JsonProperty("legend_blocked")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String legendBlocked;

    /**
     * 属性 [EMPLOYEE_NAME]
     *
     */
    @JSONField(name = "employee_name")
    @JsonProperty("employee_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String employeeName;

    /**
     * 属性 [USER_EMAIL]
     *
     */
    @JSONField(name = "user_email")
    @JsonProperty("user_email")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String userEmail;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [JOB_ID]
     *
     */
    @JSONField(name = "job_id")
    @JsonProperty("job_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long jobId;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long campaignId;

    /**
     * 属性 [LAST_STAGE_ID]
     *
     */
    @JSONField(name = "last_stage_id")
    @JsonProperty("last_stage_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lastStageId;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long departmentId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long mediumId;

    /**
     * 属性 [EMP_ID]
     *
     */
    @JSONField(name = "emp_id")
    @JsonProperty("emp_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long empId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [TYPE_ID]
     *
     */
    @JSONField(name = "type_id")
    @JsonProperty("type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long typeId;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sourceId;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long stageId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;


    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [SALARY_EXPECTED]
     */
    public void setSalaryExpected(Double  salaryExpected){
        this.salaryExpected = salaryExpected ;
        this.modify("salary_expected",salaryExpected);
    }

    /**
     * 设置 [SALARY_PROPOSED]
     */
    public void setSalaryProposed(Double  salaryProposed){
        this.salaryProposed = salaryProposed ;
        this.modify("salary_proposed",salaryProposed);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [PARTNER_PHONE]
     */
    public void setPartnerPhone(String  partnerPhone){
        this.partnerPhone = partnerPhone ;
        this.modify("partner_phone",partnerPhone);
    }

    /**
     * 设置 [KANBAN_STATE]
     */
    public void setKanbanState(String  kanbanState){
        this.kanbanState = kanbanState ;
        this.modify("kanban_state",kanbanState);
    }

    /**
     * 设置 [DATE_OPEN]
     */
    public void setDateOpen(Timestamp  dateOpen){
        this.dateOpen = dateOpen ;
        this.modify("date_open",dateOpen);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [AVAILABILITY]
     */
    public void setAvailability(Timestamp  availability){
        this.availability = availability ;
        this.modify("availability",availability);
    }

    /**
     * 设置 [SALARY_EXPECTED_EXTRA]
     */
    public void setSalaryExpectedExtra(String  salaryExpectedExtra){
        this.salaryExpectedExtra = salaryExpectedExtra ;
        this.modify("salary_expected_extra",salaryExpectedExtra);
    }

    /**
     * 设置 [DATE_CLOSED]
     */
    public void setDateClosed(Timestamp  dateClosed){
        this.dateClosed = dateClosed ;
        this.modify("date_closed",dateClosed);
    }

    /**
     * 设置 [PARTNER_MOBILE]
     */
    public void setPartnerMobile(String  partnerMobile){
        this.partnerMobile = partnerMobile ;
        this.modify("partner_mobile",partnerMobile);
    }

    /**
     * 设置 [SALARY_PROPOSED_EXTRA]
     */
    public void setSalaryProposedExtra(String  salaryProposedExtra){
        this.salaryProposedExtra = salaryProposedExtra ;
        this.modify("salary_proposed_extra",salaryProposedExtra);
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    public void setEmailFrom(String  emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [DATE_LAST_STAGE_UPDATE]
     */
    public void setDateLastStageUpdate(Timestamp  dateLastStageUpdate){
        this.dateLastStageUpdate = dateLastStageUpdate ;
        this.modify("date_last_stage_update",dateLastStageUpdate);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [REFERENCE]
     */
    public void setReference(String  reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [EMAIL_CC]
     */
    public void setEmailCc(String  emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }

    /**
     * 设置 [PARTNER_NAME]
     */
    public void setPartnerName(String  partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }

    /**
     * 设置 [PROBABILITY]
     */
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.modify("probability",probability);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [DELAY_CLOSE]
     */
    public void setDelayClose(Double  delayClose){
        this.delayClose = delayClose ;
        this.modify("delay_close",delayClose);
    }

    /**
     * 设置 [JOB_ID]
     */
    public void setJobId(Long  jobId){
        this.jobId = jobId ;
        this.modify("job_id",jobId);
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    public void setCampaignId(Long  campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [LAST_STAGE_ID]
     */
    public void setLastStageId(Long  lastStageId){
        this.lastStageId = lastStageId ;
        this.modify("last_stage_id",lastStageId);
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    public void setDepartmentId(Long  departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    public void setMediumId(Long  mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [EMP_ID]
     */
    public void setEmpId(Long  empId){
        this.empId = empId ;
        this.modify("emp_id",empId);
    }

    /**
     * 设置 [TYPE_ID]
     */
    public void setTypeId(Long  typeId){
        this.typeId = typeId ;
        this.modify("type_id",typeId);
    }

    /**
     * 设置 [SOURCE_ID]
     */
    public void setSourceId(Long  sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [STAGE_ID]
     */
    public void setStageId(Long  stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }


}



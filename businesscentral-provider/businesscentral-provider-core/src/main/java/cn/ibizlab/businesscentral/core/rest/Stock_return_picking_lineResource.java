package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_return_picking_line;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_return_picking_lineService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_return_picking_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"退料明细行" })
@RestController("Core-stock_return_picking_line")
@RequestMapping("")
public class Stock_return_picking_lineResource {

    @Autowired
    public IStock_return_picking_lineService stock_return_picking_lineService;

    @Autowired
    @Lazy
    public Stock_return_picking_lineMapping stock_return_picking_lineMapping;

    @PreAuthorize("hasPermission(this.stock_return_picking_lineMapping.toDomain(#stock_return_picking_linedto),'iBizBusinessCentral-Stock_return_picking_line-Create')")
    @ApiOperation(value = "新建退料明细行", tags = {"退料明细行" },  notes = "新建退料明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines")
    public ResponseEntity<Stock_return_picking_lineDTO> create(@Validated @RequestBody Stock_return_picking_lineDTO stock_return_picking_linedto) {
        Stock_return_picking_line domain = stock_return_picking_lineMapping.toDomain(stock_return_picking_linedto);
		stock_return_picking_lineService.create(domain);
        Stock_return_picking_lineDTO dto = stock_return_picking_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_return_picking_lineMapping.toDomain(#stock_return_picking_linedtos),'iBizBusinessCentral-Stock_return_picking_line-Create')")
    @ApiOperation(value = "批量新建退料明细行", tags = {"退料明细行" },  notes = "批量新建退料明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_return_picking_lineDTO> stock_return_picking_linedtos) {
        stock_return_picking_lineService.createBatch(stock_return_picking_lineMapping.toDomain(stock_return_picking_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_return_picking_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_return_picking_lineService.get(#stock_return_picking_line_id),'iBizBusinessCentral-Stock_return_picking_line-Update')")
    @ApiOperation(value = "更新退料明细行", tags = {"退料明细行" },  notes = "更新退料明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_return_picking_lines/{stock_return_picking_line_id}")
    public ResponseEntity<Stock_return_picking_lineDTO> update(@PathVariable("stock_return_picking_line_id") Long stock_return_picking_line_id, @RequestBody Stock_return_picking_lineDTO stock_return_picking_linedto) {
		Stock_return_picking_line domain  = stock_return_picking_lineMapping.toDomain(stock_return_picking_linedto);
        domain .setId(stock_return_picking_line_id);
		stock_return_picking_lineService.update(domain );
		Stock_return_picking_lineDTO dto = stock_return_picking_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_return_picking_lineService.getStockReturnPickingLineByEntities(this.stock_return_picking_lineMapping.toDomain(#stock_return_picking_linedtos)),'iBizBusinessCentral-Stock_return_picking_line-Update')")
    @ApiOperation(value = "批量更新退料明细行", tags = {"退料明细行" },  notes = "批量更新退料明细行")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_return_picking_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_return_picking_lineDTO> stock_return_picking_linedtos) {
        stock_return_picking_lineService.updateBatch(stock_return_picking_lineMapping.toDomain(stock_return_picking_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_return_picking_lineService.get(#stock_return_picking_line_id),'iBizBusinessCentral-Stock_return_picking_line-Remove')")
    @ApiOperation(value = "删除退料明细行", tags = {"退料明细行" },  notes = "删除退料明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_picking_lines/{stock_return_picking_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_return_picking_line_id") Long stock_return_picking_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_return_picking_lineService.remove(stock_return_picking_line_id));
    }

    @PreAuthorize("hasPermission(this.stock_return_picking_lineService.getStockReturnPickingLineByIds(#ids),'iBizBusinessCentral-Stock_return_picking_line-Remove')")
    @ApiOperation(value = "批量删除退料明细行", tags = {"退料明细行" },  notes = "批量删除退料明细行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_picking_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_return_picking_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_return_picking_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_return_picking_line-Get')")
    @ApiOperation(value = "获取退料明细行", tags = {"退料明细行" },  notes = "获取退料明细行")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_return_picking_lines/{stock_return_picking_line_id}")
    public ResponseEntity<Stock_return_picking_lineDTO> get(@PathVariable("stock_return_picking_line_id") Long stock_return_picking_line_id) {
        Stock_return_picking_line domain = stock_return_picking_lineService.get(stock_return_picking_line_id);
        Stock_return_picking_lineDTO dto = stock_return_picking_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取退料明细行草稿", tags = {"退料明细行" },  notes = "获取退料明细行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_return_picking_lines/getdraft")
    public ResponseEntity<Stock_return_picking_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_return_picking_lineMapping.toDto(stock_return_picking_lineService.getDraft(new Stock_return_picking_line())));
    }

    @ApiOperation(value = "检查退料明细行", tags = {"退料明细行" },  notes = "检查退料明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_return_picking_lineDTO stock_return_picking_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_return_picking_lineService.checkKey(stock_return_picking_lineMapping.toDomain(stock_return_picking_linedto)));
    }

    @PreAuthorize("hasPermission(this.stock_return_picking_lineMapping.toDomain(#stock_return_picking_linedto),'iBizBusinessCentral-Stock_return_picking_line-Save')")
    @ApiOperation(value = "保存退料明细行", tags = {"退料明细行" },  notes = "保存退料明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_return_picking_lineDTO stock_return_picking_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_return_picking_lineService.save(stock_return_picking_lineMapping.toDomain(stock_return_picking_linedto)));
    }

    @PreAuthorize("hasPermission(this.stock_return_picking_lineMapping.toDomain(#stock_return_picking_linedtos),'iBizBusinessCentral-Stock_return_picking_line-Save')")
    @ApiOperation(value = "批量保存退料明细行", tags = {"退料明细行" },  notes = "批量保存退料明细行")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_return_picking_lineDTO> stock_return_picking_linedtos) {
        stock_return_picking_lineService.saveBatch(stock_return_picking_lineMapping.toDomain(stock_return_picking_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_return_picking_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_return_picking_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"退料明细行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_return_picking_lines/fetchdefault")
	public ResponseEntity<List<Stock_return_picking_lineDTO>> fetchDefault(Stock_return_picking_lineSearchContext context) {
        Page<Stock_return_picking_line> domains = stock_return_picking_lineService.searchDefault(context) ;
        List<Stock_return_picking_lineDTO> list = stock_return_picking_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_return_picking_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_return_picking_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"退料明细行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_return_picking_lines/searchdefault")
	public ResponseEntity<Page<Stock_return_picking_lineDTO>> searchDefault(@RequestBody Stock_return_picking_lineSearchContext context) {
        Page<Stock_return_picking_line> domains = stock_return_picking_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_return_picking_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


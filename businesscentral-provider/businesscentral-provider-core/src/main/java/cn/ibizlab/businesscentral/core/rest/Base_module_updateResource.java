package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_module_update;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_module_updateService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_module_updateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"更新模块" })
@RestController("Core-base_module_update")
@RequestMapping("")
public class Base_module_updateResource {

    @Autowired
    public IBase_module_updateService base_module_updateService;

    @Autowired
    @Lazy
    public Base_module_updateMapping base_module_updateMapping;

    @PreAuthorize("hasPermission(this.base_module_updateMapping.toDomain(#base_module_updatedto),'iBizBusinessCentral-Base_module_update-Create')")
    @ApiOperation(value = "新建更新模块", tags = {"更新模块" },  notes = "新建更新模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_updates")
    public ResponseEntity<Base_module_updateDTO> create(@Validated @RequestBody Base_module_updateDTO base_module_updatedto) {
        Base_module_update domain = base_module_updateMapping.toDomain(base_module_updatedto);
		base_module_updateService.create(domain);
        Base_module_updateDTO dto = base_module_updateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_module_updateMapping.toDomain(#base_module_updatedtos),'iBizBusinessCentral-Base_module_update-Create')")
    @ApiOperation(value = "批量新建更新模块", tags = {"更新模块" },  notes = "批量新建更新模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_updates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_module_updateDTO> base_module_updatedtos) {
        base_module_updateService.createBatch(base_module_updateMapping.toDomain(base_module_updatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_module_update" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_module_updateService.get(#base_module_update_id),'iBizBusinessCentral-Base_module_update-Update')")
    @ApiOperation(value = "更新更新模块", tags = {"更新模块" },  notes = "更新更新模块")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_updates/{base_module_update_id}")
    public ResponseEntity<Base_module_updateDTO> update(@PathVariable("base_module_update_id") Long base_module_update_id, @RequestBody Base_module_updateDTO base_module_updatedto) {
		Base_module_update domain  = base_module_updateMapping.toDomain(base_module_updatedto);
        domain .setId(base_module_update_id);
		base_module_updateService.update(domain );
		Base_module_updateDTO dto = base_module_updateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_module_updateService.getBaseModuleUpdateByEntities(this.base_module_updateMapping.toDomain(#base_module_updatedtos)),'iBizBusinessCentral-Base_module_update-Update')")
    @ApiOperation(value = "批量更新更新模块", tags = {"更新模块" },  notes = "批量更新更新模块")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_updates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_module_updateDTO> base_module_updatedtos) {
        base_module_updateService.updateBatch(base_module_updateMapping.toDomain(base_module_updatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_module_updateService.get(#base_module_update_id),'iBizBusinessCentral-Base_module_update-Remove')")
    @ApiOperation(value = "删除更新模块", tags = {"更新模块" },  notes = "删除更新模块")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_updates/{base_module_update_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_module_update_id") Long base_module_update_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_module_updateService.remove(base_module_update_id));
    }

    @PreAuthorize("hasPermission(this.base_module_updateService.getBaseModuleUpdateByIds(#ids),'iBizBusinessCentral-Base_module_update-Remove')")
    @ApiOperation(value = "批量删除更新模块", tags = {"更新模块" },  notes = "批量删除更新模块")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_updates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_module_updateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_module_updateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_module_update-Get')")
    @ApiOperation(value = "获取更新模块", tags = {"更新模块" },  notes = "获取更新模块")
	@RequestMapping(method = RequestMethod.GET, value = "/base_module_updates/{base_module_update_id}")
    public ResponseEntity<Base_module_updateDTO> get(@PathVariable("base_module_update_id") Long base_module_update_id) {
        Base_module_update domain = base_module_updateService.get(base_module_update_id);
        Base_module_updateDTO dto = base_module_updateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取更新模块草稿", tags = {"更新模块" },  notes = "获取更新模块草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_module_updates/getdraft")
    public ResponseEntity<Base_module_updateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_module_updateMapping.toDto(base_module_updateService.getDraft(new Base_module_update())));
    }

    @ApiOperation(value = "检查更新模块", tags = {"更新模块" },  notes = "检查更新模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_updates/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_module_updateDTO base_module_updatedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_module_updateService.checkKey(base_module_updateMapping.toDomain(base_module_updatedto)));
    }

    @PreAuthorize("hasPermission(this.base_module_updateMapping.toDomain(#base_module_updatedto),'iBizBusinessCentral-Base_module_update-Save')")
    @ApiOperation(value = "保存更新模块", tags = {"更新模块" },  notes = "保存更新模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_updates/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_module_updateDTO base_module_updatedto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_module_updateService.save(base_module_updateMapping.toDomain(base_module_updatedto)));
    }

    @PreAuthorize("hasPermission(this.base_module_updateMapping.toDomain(#base_module_updatedtos),'iBizBusinessCentral-Base_module_update-Save')")
    @ApiOperation(value = "批量保存更新模块", tags = {"更新模块" },  notes = "批量保存更新模块")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_updates/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_module_updateDTO> base_module_updatedtos) {
        base_module_updateService.saveBatch(base_module_updateMapping.toDomain(base_module_updatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_module_update-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_module_update-Get')")
	@ApiOperation(value = "获取数据集", tags = {"更新模块" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_module_updates/fetchdefault")
	public ResponseEntity<List<Base_module_updateDTO>> fetchDefault(Base_module_updateSearchContext context) {
        Page<Base_module_update> domains = base_module_updateService.searchDefault(context) ;
        List<Base_module_updateDTO> list = base_module_updateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_module_update-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_module_update-Get')")
	@ApiOperation(value = "查询数据集", tags = {"更新模块" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_module_updates/searchdefault")
	public ResponseEntity<Page<Base_module_updateDTO>> searchDefault(@RequestBody Base_module_updateSearchContext context) {
        Page<Base_module_update> domains = base_module_updateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_module_updateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


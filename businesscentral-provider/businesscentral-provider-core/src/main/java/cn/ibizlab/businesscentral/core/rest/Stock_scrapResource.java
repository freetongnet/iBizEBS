package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_scrapSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"报废" })
@RestController("Core-stock_scrap")
@RequestMapping("")
public class Stock_scrapResource {

    @Autowired
    public IStock_scrapService stock_scrapService;

    @Autowired
    @Lazy
    public Stock_scrapMapping stock_scrapMapping;

    @PreAuthorize("hasPermission(this.stock_scrapMapping.toDomain(#stock_scrapdto),'iBizBusinessCentral-Stock_scrap-Create')")
    @ApiOperation(value = "新建报废", tags = {"报废" },  notes = "新建报废")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scraps")
    public ResponseEntity<Stock_scrapDTO> create(@Validated @RequestBody Stock_scrapDTO stock_scrapdto) {
        Stock_scrap domain = stock_scrapMapping.toDomain(stock_scrapdto);
		stock_scrapService.create(domain);
        Stock_scrapDTO dto = stock_scrapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_scrapMapping.toDomain(#stock_scrapdtos),'iBizBusinessCentral-Stock_scrap-Create')")
    @ApiOperation(value = "批量新建报废", tags = {"报废" },  notes = "批量新建报废")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scraps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_scrapDTO> stock_scrapdtos) {
        stock_scrapService.createBatch(stock_scrapMapping.toDomain(stock_scrapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_scrap" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_scrapService.get(#stock_scrap_id),'iBizBusinessCentral-Stock_scrap-Update')")
    @ApiOperation(value = "更新报废", tags = {"报废" },  notes = "更新报废")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_scraps/{stock_scrap_id}")
    public ResponseEntity<Stock_scrapDTO> update(@PathVariable("stock_scrap_id") Long stock_scrap_id, @RequestBody Stock_scrapDTO stock_scrapdto) {
		Stock_scrap domain  = stock_scrapMapping.toDomain(stock_scrapdto);
        domain .setId(stock_scrap_id);
		stock_scrapService.update(domain );
		Stock_scrapDTO dto = stock_scrapMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_scrapService.getStockScrapByEntities(this.stock_scrapMapping.toDomain(#stock_scrapdtos)),'iBizBusinessCentral-Stock_scrap-Update')")
    @ApiOperation(value = "批量更新报废", tags = {"报废" },  notes = "批量更新报废")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_scraps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_scrapDTO> stock_scrapdtos) {
        stock_scrapService.updateBatch(stock_scrapMapping.toDomain(stock_scrapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_scrapService.get(#stock_scrap_id),'iBizBusinessCentral-Stock_scrap-Remove')")
    @ApiOperation(value = "删除报废", tags = {"报废" },  notes = "删除报废")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_scraps/{stock_scrap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_scrap_id") Long stock_scrap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_scrapService.remove(stock_scrap_id));
    }

    @PreAuthorize("hasPermission(this.stock_scrapService.getStockScrapByIds(#ids),'iBizBusinessCentral-Stock_scrap-Remove')")
    @ApiOperation(value = "批量删除报废", tags = {"报废" },  notes = "批量删除报废")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_scraps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_scrapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_scrapMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_scrap-Get')")
    @ApiOperation(value = "获取报废", tags = {"报废" },  notes = "获取报废")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_scraps/{stock_scrap_id}")
    public ResponseEntity<Stock_scrapDTO> get(@PathVariable("stock_scrap_id") Long stock_scrap_id) {
        Stock_scrap domain = stock_scrapService.get(stock_scrap_id);
        Stock_scrapDTO dto = stock_scrapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取报废草稿", tags = {"报废" },  notes = "获取报废草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_scraps/getdraft")
    public ResponseEntity<Stock_scrapDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_scrapMapping.toDto(stock_scrapService.getDraft(new Stock_scrap())));
    }

    @ApiOperation(value = "检查报废", tags = {"报废" },  notes = "检查报废")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scraps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_scrapDTO stock_scrapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_scrapService.checkKey(stock_scrapMapping.toDomain(stock_scrapdto)));
    }

    @PreAuthorize("hasPermission(this.stock_scrapMapping.toDomain(#stock_scrapdto),'iBizBusinessCentral-Stock_scrap-Save')")
    @ApiOperation(value = "保存报废", tags = {"报废" },  notes = "保存报废")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scraps/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_scrapDTO stock_scrapdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_scrapService.save(stock_scrapMapping.toDomain(stock_scrapdto)));
    }

    @PreAuthorize("hasPermission(this.stock_scrapMapping.toDomain(#stock_scrapdtos),'iBizBusinessCentral-Stock_scrap-Save')")
    @ApiOperation(value = "批量保存报废", tags = {"报废" },  notes = "批量保存报废")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scraps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_scrapDTO> stock_scrapdtos) {
        stock_scrapService.saveBatch(stock_scrapMapping.toDomain(stock_scrapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_scrap-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_scrap-Get')")
	@ApiOperation(value = "获取数据集", tags = {"报废" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_scraps/fetchdefault")
	public ResponseEntity<List<Stock_scrapDTO>> fetchDefault(Stock_scrapSearchContext context) {
        Page<Stock_scrap> domains = stock_scrapService.searchDefault(context) ;
        List<Stock_scrapDTO> list = stock_scrapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_scrap-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_scrap-Get')")
	@ApiOperation(value = "查询数据集", tags = {"报废" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_scraps/searchdefault")
	public ResponseEntity<Page<Stock_scrapDTO>> searchDefault(@RequestBody Stock_scrapSearchContext context) {
        Page<Stock_scrap> domains = stock_scrapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_scrapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


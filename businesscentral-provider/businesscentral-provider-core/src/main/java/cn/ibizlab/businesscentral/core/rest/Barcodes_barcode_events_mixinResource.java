package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.businesscentral.core.odoo_barcodes.service.IBarcodes_barcode_events_mixinService;
import cn.ibizlab.businesscentral.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"条形码事件混合" })
@RestController("Core-barcodes_barcode_events_mixin")
@RequestMapping("")
public class Barcodes_barcode_events_mixinResource {

    @Autowired
    public IBarcodes_barcode_events_mixinService barcodes_barcode_events_mixinService;

    @Autowired
    @Lazy
    public Barcodes_barcode_events_mixinMapping barcodes_barcode_events_mixinMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-Create-all')")
    @ApiOperation(value = "新建条形码事件混合", tags = {"条形码事件混合" },  notes = "新建条形码事件混合")
	@RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins")
    public ResponseEntity<Barcodes_barcode_events_mixinDTO> create(@Validated @RequestBody Barcodes_barcode_events_mixinDTO barcodes_barcode_events_mixindto) {
        Barcodes_barcode_events_mixin domain = barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindto);
		barcodes_barcode_events_mixinService.create(domain);
        Barcodes_barcode_events_mixinDTO dto = barcodes_barcode_events_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-Create-all')")
    @ApiOperation(value = "批量新建条形码事件混合", tags = {"条形码事件混合" },  notes = "批量新建条形码事件混合")
	@RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Barcodes_barcode_events_mixinDTO> barcodes_barcode_events_mixindtos) {
        barcodes_barcode_events_mixinService.createBatch(barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-Update-all')")
    @ApiOperation(value = "更新条形码事件混合", tags = {"条形码事件混合" },  notes = "更新条形码事件混合")
	@RequestMapping(method = RequestMethod.PUT, value = "/barcodes_barcode_events_mixins/{barcodes_barcode_events_mixin_id}")
    public ResponseEntity<Barcodes_barcode_events_mixinDTO> update(@PathVariable("barcodes_barcode_events_mixin_id") Long barcodes_barcode_events_mixin_id, @RequestBody Barcodes_barcode_events_mixinDTO barcodes_barcode_events_mixindto) {
		Barcodes_barcode_events_mixin domain  = barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindto);
        domain .setId(barcodes_barcode_events_mixin_id);
		barcodes_barcode_events_mixinService.update(domain );
		Barcodes_barcode_events_mixinDTO dto = barcodes_barcode_events_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-Update-all')")
    @ApiOperation(value = "批量更新条形码事件混合", tags = {"条形码事件混合" },  notes = "批量更新条形码事件混合")
	@RequestMapping(method = RequestMethod.PUT, value = "/barcodes_barcode_events_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Barcodes_barcode_events_mixinDTO> barcodes_barcode_events_mixindtos) {
        barcodes_barcode_events_mixinService.updateBatch(barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-Remove-all')")
    @ApiOperation(value = "删除条形码事件混合", tags = {"条形码事件混合" },  notes = "删除条形码事件混合")
	@RequestMapping(method = RequestMethod.DELETE, value = "/barcodes_barcode_events_mixins/{barcodes_barcode_events_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("barcodes_barcode_events_mixin_id") Long barcodes_barcode_events_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(barcodes_barcode_events_mixinService.remove(barcodes_barcode_events_mixin_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-Remove-all')")
    @ApiOperation(value = "批量删除条形码事件混合", tags = {"条形码事件混合" },  notes = "批量删除条形码事件混合")
	@RequestMapping(method = RequestMethod.DELETE, value = "/barcodes_barcode_events_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        barcodes_barcode_events_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-Get-all')")
    @ApiOperation(value = "获取条形码事件混合", tags = {"条形码事件混合" },  notes = "获取条形码事件混合")
	@RequestMapping(method = RequestMethod.GET, value = "/barcodes_barcode_events_mixins/{barcodes_barcode_events_mixin_id}")
    public ResponseEntity<Barcodes_barcode_events_mixinDTO> get(@PathVariable("barcodes_barcode_events_mixin_id") Long barcodes_barcode_events_mixin_id) {
        Barcodes_barcode_events_mixin domain = barcodes_barcode_events_mixinService.get(barcodes_barcode_events_mixin_id);
        Barcodes_barcode_events_mixinDTO dto = barcodes_barcode_events_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取条形码事件混合草稿", tags = {"条形码事件混合" },  notes = "获取条形码事件混合草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/barcodes_barcode_events_mixins/getdraft")
    public ResponseEntity<Barcodes_barcode_events_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(barcodes_barcode_events_mixinMapping.toDto(barcodes_barcode_events_mixinService.getDraft(new Barcodes_barcode_events_mixin())));
    }

    @ApiOperation(value = "检查条形码事件混合", tags = {"条形码事件混合" },  notes = "检查条形码事件混合")
	@RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Barcodes_barcode_events_mixinDTO barcodes_barcode_events_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(barcodes_barcode_events_mixinService.checkKey(barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-Save-all')")
    @ApiOperation(value = "保存条形码事件混合", tags = {"条形码事件混合" },  notes = "保存条形码事件混合")
	@RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Barcodes_barcode_events_mixinDTO barcodes_barcode_events_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(barcodes_barcode_events_mixinService.save(barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-Save-all')")
    @ApiOperation(value = "批量保存条形码事件混合", tags = {"条形码事件混合" },  notes = "批量保存条形码事件混合")
	@RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Barcodes_barcode_events_mixinDTO> barcodes_barcode_events_mixindtos) {
        barcodes_barcode_events_mixinService.saveBatch(barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"条形码事件混合" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/barcodes_barcode_events_mixins/fetchdefault")
	public ResponseEntity<List<Barcodes_barcode_events_mixinDTO>> fetchDefault(Barcodes_barcode_events_mixinSearchContext context) {
        Page<Barcodes_barcode_events_mixin> domains = barcodes_barcode_events_mixinService.searchDefault(context) ;
        List<Barcodes_barcode_events_mixinDTO> list = barcodes_barcode_events_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Barcodes_barcode_events_mixin-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"条形码事件混合" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/barcodes_barcode_events_mixins/searchdefault")
	public ResponseEntity<Page<Barcodes_barcode_events_mixinDTO>> searchDefault(@RequestBody Barcodes_barcode_events_mixinSearchContext context) {
        Page<Barcodes_barcode_events_mixin> domains = barcodes_barcode_events_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(barcodes_barcode_events_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


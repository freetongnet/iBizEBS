package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_resume_line_type;
import cn.ibizlab.businesscentral.core.dto.Hr_resume_line_typeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreHr_resume_line_typeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_resume_line_typeMapping extends MappingBase<Hr_resume_line_typeDTO, Hr_resume_line_type> {


}


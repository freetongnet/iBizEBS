package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_confirm;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_confirmService;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_confirmSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动确认" })
@RestController("Core-event_confirm")
@RequestMapping("")
public class Event_confirmResource {

    @Autowired
    public IEvent_confirmService event_confirmService;

    @Autowired
    @Lazy
    public Event_confirmMapping event_confirmMapping;

    @PreAuthorize("hasPermission(this.event_confirmMapping.toDomain(#event_confirmdto),'iBizBusinessCentral-Event_confirm-Create')")
    @ApiOperation(value = "新建活动确认", tags = {"活动确认" },  notes = "新建活动确认")
	@RequestMapping(method = RequestMethod.POST, value = "/event_confirms")
    public ResponseEntity<Event_confirmDTO> create(@Validated @RequestBody Event_confirmDTO event_confirmdto) {
        Event_confirm domain = event_confirmMapping.toDomain(event_confirmdto);
		event_confirmService.create(domain);
        Event_confirmDTO dto = event_confirmMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_confirmMapping.toDomain(#event_confirmdtos),'iBizBusinessCentral-Event_confirm-Create')")
    @ApiOperation(value = "批量新建活动确认", tags = {"活动确认" },  notes = "批量新建活动确认")
	@RequestMapping(method = RequestMethod.POST, value = "/event_confirms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_confirmDTO> event_confirmdtos) {
        event_confirmService.createBatch(event_confirmMapping.toDomain(event_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "event_confirm" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.event_confirmService.get(#event_confirm_id),'iBizBusinessCentral-Event_confirm-Update')")
    @ApiOperation(value = "更新活动确认", tags = {"活动确认" },  notes = "更新活动确认")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_confirms/{event_confirm_id}")
    public ResponseEntity<Event_confirmDTO> update(@PathVariable("event_confirm_id") Long event_confirm_id, @RequestBody Event_confirmDTO event_confirmdto) {
		Event_confirm domain  = event_confirmMapping.toDomain(event_confirmdto);
        domain .setId(event_confirm_id);
		event_confirmService.update(domain );
		Event_confirmDTO dto = event_confirmMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_confirmService.getEventConfirmByEntities(this.event_confirmMapping.toDomain(#event_confirmdtos)),'iBizBusinessCentral-Event_confirm-Update')")
    @ApiOperation(value = "批量更新活动确认", tags = {"活动确认" },  notes = "批量更新活动确认")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_confirms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_confirmDTO> event_confirmdtos) {
        event_confirmService.updateBatch(event_confirmMapping.toDomain(event_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.event_confirmService.get(#event_confirm_id),'iBizBusinessCentral-Event_confirm-Remove')")
    @ApiOperation(value = "删除活动确认", tags = {"活动确认" },  notes = "删除活动确认")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_confirms/{event_confirm_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("event_confirm_id") Long event_confirm_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_confirmService.remove(event_confirm_id));
    }

    @PreAuthorize("hasPermission(this.event_confirmService.getEventConfirmByIds(#ids),'iBizBusinessCentral-Event_confirm-Remove')")
    @ApiOperation(value = "批量删除活动确认", tags = {"活动确认" },  notes = "批量删除活动确认")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_confirms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        event_confirmService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.event_confirmMapping.toDomain(returnObject.body),'iBizBusinessCentral-Event_confirm-Get')")
    @ApiOperation(value = "获取活动确认", tags = {"活动确认" },  notes = "获取活动确认")
	@RequestMapping(method = RequestMethod.GET, value = "/event_confirms/{event_confirm_id}")
    public ResponseEntity<Event_confirmDTO> get(@PathVariable("event_confirm_id") Long event_confirm_id) {
        Event_confirm domain = event_confirmService.get(event_confirm_id);
        Event_confirmDTO dto = event_confirmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动确认草稿", tags = {"活动确认" },  notes = "获取活动确认草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/event_confirms/getdraft")
    public ResponseEntity<Event_confirmDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(event_confirmMapping.toDto(event_confirmService.getDraft(new Event_confirm())));
    }

    @ApiOperation(value = "检查活动确认", tags = {"活动确认" },  notes = "检查活动确认")
	@RequestMapping(method = RequestMethod.POST, value = "/event_confirms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Event_confirmDTO event_confirmdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(event_confirmService.checkKey(event_confirmMapping.toDomain(event_confirmdto)));
    }

    @PreAuthorize("hasPermission(this.event_confirmMapping.toDomain(#event_confirmdto),'iBizBusinessCentral-Event_confirm-Save')")
    @ApiOperation(value = "保存活动确认", tags = {"活动确认" },  notes = "保存活动确认")
	@RequestMapping(method = RequestMethod.POST, value = "/event_confirms/save")
    public ResponseEntity<Boolean> save(@RequestBody Event_confirmDTO event_confirmdto) {
        return ResponseEntity.status(HttpStatus.OK).body(event_confirmService.save(event_confirmMapping.toDomain(event_confirmdto)));
    }

    @PreAuthorize("hasPermission(this.event_confirmMapping.toDomain(#event_confirmdtos),'iBizBusinessCentral-Event_confirm-Save')")
    @ApiOperation(value = "批量保存活动确认", tags = {"活动确认" },  notes = "批量保存活动确认")
	@RequestMapping(method = RequestMethod.POST, value = "/event_confirms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Event_confirmDTO> event_confirmdtos) {
        event_confirmService.saveBatch(event_confirmMapping.toDomain(event_confirmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_confirm-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_confirm-Get')")
	@ApiOperation(value = "获取数据集", tags = {"活动确认" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/event_confirms/fetchdefault")
	public ResponseEntity<List<Event_confirmDTO>> fetchDefault(Event_confirmSearchContext context) {
        Page<Event_confirm> domains = event_confirmService.searchDefault(context) ;
        List<Event_confirmDTO> list = event_confirmMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_confirm-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_confirm-Get')")
	@ApiOperation(value = "查询数据集", tags = {"活动确认" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/event_confirms/searchdefault")
	public ResponseEntity<Page<Event_confirmDTO>> searchDefault(@RequestBody Event_confirmSearchContext context) {
        Page<Event_confirm> domains = event_confirmService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_confirmMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


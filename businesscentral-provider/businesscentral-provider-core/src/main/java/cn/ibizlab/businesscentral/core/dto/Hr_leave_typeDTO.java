package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Hr_leave_typeDTO]
 */
@Data
public class Hr_leave_typeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [VALID]
     *
     */
    @JSONField(name = "valid")
    @JsonProperty("valid")
    private Boolean valid;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [VIRTUAL_REMAINING_LEAVES]
     *
     */
    @JSONField(name = "virtual_remaining_leaves")
    @JsonProperty("virtual_remaining_leaves")
    private Double virtualRemainingLeaves;

    /**
     * 属性 [LEAVES_TAKEN]
     *
     */
    @JSONField(name = "leaves_taken")
    @JsonProperty("leaves_taken")
    private Double leavesTaken;

    /**
     * 属性 [GROUP_DAYS_ALLOCATION]
     *
     */
    @JSONField(name = "group_days_allocation")
    @JsonProperty("group_days_allocation")
    private Double groupDaysAllocation;

    /**
     * 属性 [DOUBLE_VALIDATION]
     *
     */
    @JSONField(name = "double_validation")
    @JsonProperty("double_validation")
    private Boolean doubleValidation;

    /**
     * 属性 [ALLOCATION_TYPE]
     *
     */
    @JSONField(name = "allocation_type")
    @JsonProperty("allocation_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String allocationType;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [UNPAID]
     *
     */
    @JSONField(name = "unpaid")
    @JsonProperty("unpaid")
    private Boolean unpaid;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [MAX_LEAVES]
     *
     */
    @JSONField(name = "max_leaves")
    @JsonProperty("max_leaves")
    private Double maxLeaves;

    /**
     * 属性 [VALIDITY_STOP]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validity_stop" , format="yyyy-MM-dd")
    @JsonProperty("validity_stop")
    private Timestamp validityStop;

    /**
     * 属性 [VALIDATION_TYPE]
     *
     */
    @JSONField(name = "validation_type")
    @JsonProperty("validation_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String validationType;

    /**
     * 属性 [TIME_TYPE]
     *
     */
    @JSONField(name = "time_type")
    @JsonProperty("time_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String timeType;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [REQUEST_UNIT]
     *
     */
    @JSONField(name = "request_unit")
    @JsonProperty("request_unit")
    @NotBlank(message = "[休假]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String requestUnit;

    /**
     * 属性 [GROUP_DAYS_LEAVE]
     *
     */
    @JSONField(name = "group_days_leave")
    @JsonProperty("group_days_leave")
    private Double groupDaysLeave;

    /**
     * 属性 [COLOR_NAME]
     *
     */
    @JSONField(name = "color_name")
    @JsonProperty("color_name")
    @NotBlank(message = "[报表中的颜色]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String colorName;

    /**
     * 属性 [REMAINING_LEAVES]
     *
     */
    @JSONField(name = "remaining_leaves")
    @JsonProperty("remaining_leaves")
    private Double remainingLeaves;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[休假类型]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [VALIDITY_START]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validity_start" , format="yyyy-MM-dd")
    @JsonProperty("validity_start")
    private Timestamp validityStart;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [CATEG_ID_TEXT]
     *
     */
    @JSONField(name = "categ_id_text")
    @JsonProperty("categ_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String categIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [CATEG_ID]
     *
     */
    @JSONField(name = "categ_id")
    @JsonProperty("categ_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [ALLOCATION_TYPE]
     */
    public void setAllocationType(String  allocationType){
        this.allocationType = allocationType ;
        this.modify("allocation_type",allocationType);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [UNPAID]
     */
    public void setUnpaid(Boolean  unpaid){
        this.unpaid = unpaid ;
        this.modify("unpaid",unpaid);
    }

    /**
     * 设置 [VALIDITY_STOP]
     */
    public void setValidityStop(Timestamp  validityStop){
        this.validityStop = validityStop ;
        this.modify("validity_stop",validityStop);
    }

    /**
     * 设置 [VALIDATION_TYPE]
     */
    public void setValidationType(String  validationType){
        this.validationType = validationType ;
        this.modify("validation_type",validationType);
    }

    /**
     * 设置 [TIME_TYPE]
     */
    public void setTimeType(String  timeType){
        this.timeType = timeType ;
        this.modify("time_type",timeType);
    }

    /**
     * 设置 [REQUEST_UNIT]
     */
    public void setRequestUnit(String  requestUnit){
        this.requestUnit = requestUnit ;
        this.modify("request_unit",requestUnit);
    }

    /**
     * 设置 [COLOR_NAME]
     */
    public void setColorName(String  colorName){
        this.colorName = colorName ;
        this.modify("color_name",colorName);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [VALIDITY_START]
     */
    public void setValidityStart(Timestamp  validityStart){
        this.validityStart = validityStart ;
        this.modify("validity_start",validityStart);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [CATEG_ID]
     */
    public void setCategId(Long  categId){
        this.categId = categId ;
        this.modify("categ_id",categId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }


}



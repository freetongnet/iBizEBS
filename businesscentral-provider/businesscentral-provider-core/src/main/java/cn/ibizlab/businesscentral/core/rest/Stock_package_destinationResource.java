package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_package_destination;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_destinationService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_package_destinationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"包裹目的地" })
@RestController("Core-stock_package_destination")
@RequestMapping("")
public class Stock_package_destinationResource {

    @Autowired
    public IStock_package_destinationService stock_package_destinationService;

    @Autowired
    @Lazy
    public Stock_package_destinationMapping stock_package_destinationMapping;

    @PreAuthorize("hasPermission(this.stock_package_destinationMapping.toDomain(#stock_package_destinationdto),'iBizBusinessCentral-Stock_package_destination-Create')")
    @ApiOperation(value = "新建包裹目的地", tags = {"包裹目的地" },  notes = "新建包裹目的地")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations")
    public ResponseEntity<Stock_package_destinationDTO> create(@Validated @RequestBody Stock_package_destinationDTO stock_package_destinationdto) {
        Stock_package_destination domain = stock_package_destinationMapping.toDomain(stock_package_destinationdto);
		stock_package_destinationService.create(domain);
        Stock_package_destinationDTO dto = stock_package_destinationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_package_destinationMapping.toDomain(#stock_package_destinationdtos),'iBizBusinessCentral-Stock_package_destination-Create')")
    @ApiOperation(value = "批量新建包裹目的地", tags = {"包裹目的地" },  notes = "批量新建包裹目的地")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_package_destinationDTO> stock_package_destinationdtos) {
        stock_package_destinationService.createBatch(stock_package_destinationMapping.toDomain(stock_package_destinationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_package_destination" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_package_destinationService.get(#stock_package_destination_id),'iBizBusinessCentral-Stock_package_destination-Update')")
    @ApiOperation(value = "更新包裹目的地", tags = {"包裹目的地" },  notes = "更新包裹目的地")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_package_destinations/{stock_package_destination_id}")
    public ResponseEntity<Stock_package_destinationDTO> update(@PathVariable("stock_package_destination_id") Long stock_package_destination_id, @RequestBody Stock_package_destinationDTO stock_package_destinationdto) {
		Stock_package_destination domain  = stock_package_destinationMapping.toDomain(stock_package_destinationdto);
        domain .setId(stock_package_destination_id);
		stock_package_destinationService.update(domain );
		Stock_package_destinationDTO dto = stock_package_destinationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_package_destinationService.getStockPackageDestinationByEntities(this.stock_package_destinationMapping.toDomain(#stock_package_destinationdtos)),'iBizBusinessCentral-Stock_package_destination-Update')")
    @ApiOperation(value = "批量更新包裹目的地", tags = {"包裹目的地" },  notes = "批量更新包裹目的地")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_package_destinations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_package_destinationDTO> stock_package_destinationdtos) {
        stock_package_destinationService.updateBatch(stock_package_destinationMapping.toDomain(stock_package_destinationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_package_destinationService.get(#stock_package_destination_id),'iBizBusinessCentral-Stock_package_destination-Remove')")
    @ApiOperation(value = "删除包裹目的地", tags = {"包裹目的地" },  notes = "删除包裹目的地")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_destinations/{stock_package_destination_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_package_destination_id") Long stock_package_destination_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_package_destinationService.remove(stock_package_destination_id));
    }

    @PreAuthorize("hasPermission(this.stock_package_destinationService.getStockPackageDestinationByIds(#ids),'iBizBusinessCentral-Stock_package_destination-Remove')")
    @ApiOperation(value = "批量删除包裹目的地", tags = {"包裹目的地" },  notes = "批量删除包裹目的地")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_destinations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_package_destinationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_package_destinationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_package_destination-Get')")
    @ApiOperation(value = "获取包裹目的地", tags = {"包裹目的地" },  notes = "获取包裹目的地")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_package_destinations/{stock_package_destination_id}")
    public ResponseEntity<Stock_package_destinationDTO> get(@PathVariable("stock_package_destination_id") Long stock_package_destination_id) {
        Stock_package_destination domain = stock_package_destinationService.get(stock_package_destination_id);
        Stock_package_destinationDTO dto = stock_package_destinationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取包裹目的地草稿", tags = {"包裹目的地" },  notes = "获取包裹目的地草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_package_destinations/getdraft")
    public ResponseEntity<Stock_package_destinationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_package_destinationMapping.toDto(stock_package_destinationService.getDraft(new Stock_package_destination())));
    }

    @ApiOperation(value = "检查包裹目的地", tags = {"包裹目的地" },  notes = "检查包裹目的地")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_package_destinationDTO stock_package_destinationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_package_destinationService.checkKey(stock_package_destinationMapping.toDomain(stock_package_destinationdto)));
    }

    @PreAuthorize("hasPermission(this.stock_package_destinationMapping.toDomain(#stock_package_destinationdto),'iBizBusinessCentral-Stock_package_destination-Save')")
    @ApiOperation(value = "保存包裹目的地", tags = {"包裹目的地" },  notes = "保存包裹目的地")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_package_destinationDTO stock_package_destinationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_package_destinationService.save(stock_package_destinationMapping.toDomain(stock_package_destinationdto)));
    }

    @PreAuthorize("hasPermission(this.stock_package_destinationMapping.toDomain(#stock_package_destinationdtos),'iBizBusinessCentral-Stock_package_destination-Save')")
    @ApiOperation(value = "批量保存包裹目的地", tags = {"包裹目的地" },  notes = "批量保存包裹目的地")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_destinations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_package_destinationDTO> stock_package_destinationdtos) {
        stock_package_destinationService.saveBatch(stock_package_destinationMapping.toDomain(stock_package_destinationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_package_destination-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_package_destination-Get')")
	@ApiOperation(value = "获取数据集", tags = {"包裹目的地" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_package_destinations/fetchdefault")
	public ResponseEntity<List<Stock_package_destinationDTO>> fetchDefault(Stock_package_destinationSearchContext context) {
        Page<Stock_package_destination> domains = stock_package_destinationService.searchDefault(context) ;
        List<Stock_package_destinationDTO> list = stock_package_destinationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_package_destination-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_package_destination-Get')")
	@ApiOperation(value = "查询数据集", tags = {"包裹目的地" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_package_destinations/searchdefault")
	public ResponseEntity<Page<Stock_package_destinationDTO>> searchDefault(@RequestBody Stock_package_destinationSearchContext context) {
        Page<Stock_package_destination> domains = stock_package_destinationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_package_destinationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


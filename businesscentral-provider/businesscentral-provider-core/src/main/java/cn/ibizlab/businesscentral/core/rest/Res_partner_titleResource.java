package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_title;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_titleService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_titleSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"业务伙伴称谓" })
@RestController("Core-res_partner_title")
@RequestMapping("")
public class Res_partner_titleResource {

    @Autowired
    public IRes_partner_titleService res_partner_titleService;

    @Autowired
    @Lazy
    public Res_partner_titleMapping res_partner_titleMapping;

    @PreAuthorize("hasPermission(this.res_partner_titleMapping.toDomain(#res_partner_titledto),'iBizBusinessCentral-Res_partner_title-Create')")
    @ApiOperation(value = "新建业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "新建业务伙伴称谓")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles")
    public ResponseEntity<Res_partner_titleDTO> create(@Validated @RequestBody Res_partner_titleDTO res_partner_titledto) {
        Res_partner_title domain = res_partner_titleMapping.toDomain(res_partner_titledto);
		res_partner_titleService.create(domain);
        Res_partner_titleDTO dto = res_partner_titleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_titleMapping.toDomain(#res_partner_titledtos),'iBizBusinessCentral-Res_partner_title-Create')")
    @ApiOperation(value = "批量新建业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "批量新建业务伙伴称谓")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_titleDTO> res_partner_titledtos) {
        res_partner_titleService.createBatch(res_partner_titleMapping.toDomain(res_partner_titledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_partner_title" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_partner_titleService.get(#res_partner_title_id),'iBizBusinessCentral-Res_partner_title-Update')")
    @ApiOperation(value = "更新业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "更新业务伙伴称谓")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_titles/{res_partner_title_id}")
    public ResponseEntity<Res_partner_titleDTO> update(@PathVariable("res_partner_title_id") Long res_partner_title_id, @RequestBody Res_partner_titleDTO res_partner_titledto) {
		Res_partner_title domain  = res_partner_titleMapping.toDomain(res_partner_titledto);
        domain .setId(res_partner_title_id);
		res_partner_titleService.update(domain );
		Res_partner_titleDTO dto = res_partner_titleMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_titleService.getResPartnerTitleByEntities(this.res_partner_titleMapping.toDomain(#res_partner_titledtos)),'iBizBusinessCentral-Res_partner_title-Update')")
    @ApiOperation(value = "批量更新业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "批量更新业务伙伴称谓")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_titles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_titleDTO> res_partner_titledtos) {
        res_partner_titleService.updateBatch(res_partner_titleMapping.toDomain(res_partner_titledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_partner_titleService.get(#res_partner_title_id),'iBizBusinessCentral-Res_partner_title-Remove')")
    @ApiOperation(value = "删除业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "删除业务伙伴称谓")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_titles/{res_partner_title_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_title_id") Long res_partner_title_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partner_titleService.remove(res_partner_title_id));
    }

    @PreAuthorize("hasPermission(this.res_partner_titleService.getResPartnerTitleByIds(#ids),'iBizBusinessCentral-Res_partner_title-Remove')")
    @ApiOperation(value = "批量删除业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "批量删除业务伙伴称谓")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_titles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_partner_titleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_partner_titleMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_partner_title-Get')")
    @ApiOperation(value = "获取业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "获取业务伙伴称谓")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_titles/{res_partner_title_id}")
    public ResponseEntity<Res_partner_titleDTO> get(@PathVariable("res_partner_title_id") Long res_partner_title_id) {
        Res_partner_title domain = res_partner_titleService.get(res_partner_title_id);
        Res_partner_titleDTO dto = res_partner_titleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取业务伙伴称谓草稿", tags = {"业务伙伴称谓" },  notes = "获取业务伙伴称谓草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_titles/getdraft")
    public ResponseEntity<Res_partner_titleDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_titleMapping.toDto(res_partner_titleService.getDraft(new Res_partner_title())));
    }

    @ApiOperation(value = "检查业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "检查业务伙伴称谓")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partner_titleDTO res_partner_titledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partner_titleService.checkKey(res_partner_titleMapping.toDomain(res_partner_titledto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_titleMapping.toDomain(#res_partner_titledto),'iBizBusinessCentral-Res_partner_title-Save')")
    @ApiOperation(value = "保存业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "保存业务伙伴称谓")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_partner_titleDTO res_partner_titledto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_titleService.save(res_partner_titleMapping.toDomain(res_partner_titledto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_titleMapping.toDomain(#res_partner_titledtos),'iBizBusinessCentral-Res_partner_title-Save')")
    @ApiOperation(value = "批量保存业务伙伴称谓", tags = {"业务伙伴称谓" },  notes = "批量保存业务伙伴称谓")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_partner_titleDTO> res_partner_titledtos) {
        res_partner_titleService.saveBatch(res_partner_titleMapping.toDomain(res_partner_titledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_title-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_title-Get')")
	@ApiOperation(value = "获取数据集", tags = {"业务伙伴称谓" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_titles/fetchdefault")
	public ResponseEntity<List<Res_partner_titleDTO>> fetchDefault(Res_partner_titleSearchContext context) {
        Page<Res_partner_title> domains = res_partner_titleService.searchDefault(context) ;
        List<Res_partner_titleDTO> list = res_partner_titleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_title-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_title-Get')")
	@ApiOperation(value = "查询数据集", tags = {"业务伙伴称谓" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_partner_titles/searchdefault")
	public ResponseEntity<Page<Res_partner_titleDTO>> searchDefault(@RequestBody Res_partner_titleSearchContext context) {
        Page<Res_partner_title> domains = res_partner_titleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_titleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


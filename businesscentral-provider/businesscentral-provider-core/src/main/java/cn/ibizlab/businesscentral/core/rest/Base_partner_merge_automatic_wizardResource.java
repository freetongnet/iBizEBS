package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_partner_merge_automatic_wizardService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"合并业务伙伴向导" })
@RestController("Core-base_partner_merge_automatic_wizard")
@RequestMapping("")
public class Base_partner_merge_automatic_wizardResource {

    @Autowired
    public IBase_partner_merge_automatic_wizardService base_partner_merge_automatic_wizardService;

    @Autowired
    @Lazy
    public Base_partner_merge_automatic_wizardMapping base_partner_merge_automatic_wizardMapping;

    @PreAuthorize("hasPermission(this.base_partner_merge_automatic_wizardMapping.toDomain(#base_partner_merge_automatic_wizarddto),'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Create')")
    @ApiOperation(value = "新建合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "新建合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards")
    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> create(@Validated @RequestBody Base_partner_merge_automatic_wizardDTO base_partner_merge_automatic_wizarddto) {
        Base_partner_merge_automatic_wizard domain = base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddto);
		base_partner_merge_automatic_wizardService.create(domain);
        Base_partner_merge_automatic_wizardDTO dto = base_partner_merge_automatic_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_automatic_wizardMapping.toDomain(#base_partner_merge_automatic_wizarddtos),'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Create')")
    @ApiOperation(value = "批量新建合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "批量新建合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_partner_merge_automatic_wizardDTO> base_partner_merge_automatic_wizarddtos) {
        base_partner_merge_automatic_wizardService.createBatch(base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_partner_merge_automatic_wizard" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_partner_merge_automatic_wizardService.get(#base_partner_merge_automatic_wizard_id),'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Update')")
    @ApiOperation(value = "更新合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "更新合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_automatic_wizards/{base_partner_merge_automatic_wizard_id}")
    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> update(@PathVariable("base_partner_merge_automatic_wizard_id") Long base_partner_merge_automatic_wizard_id, @RequestBody Base_partner_merge_automatic_wizardDTO base_partner_merge_automatic_wizarddto) {
		Base_partner_merge_automatic_wizard domain  = base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddto);
        domain .setId(base_partner_merge_automatic_wizard_id);
		base_partner_merge_automatic_wizardService.update(domain );
		Base_partner_merge_automatic_wizardDTO dto = base_partner_merge_automatic_wizardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_automatic_wizardService.getBasePartnerMergeAutomaticWizardByEntities(this.base_partner_merge_automatic_wizardMapping.toDomain(#base_partner_merge_automatic_wizarddtos)),'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Update')")
    @ApiOperation(value = "批量更新合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "批量更新合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_automatic_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_partner_merge_automatic_wizardDTO> base_partner_merge_automatic_wizarddtos) {
        base_partner_merge_automatic_wizardService.updateBatch(base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_automatic_wizardService.get(#base_partner_merge_automatic_wizard_id),'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Remove')")
    @ApiOperation(value = "删除合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "删除合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_automatic_wizards/{base_partner_merge_automatic_wizard_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_partner_merge_automatic_wizard_id") Long base_partner_merge_automatic_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_automatic_wizardService.remove(base_partner_merge_automatic_wizard_id));
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_automatic_wizardService.getBasePartnerMergeAutomaticWizardByIds(#ids),'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Remove')")
    @ApiOperation(value = "批量删除合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "批量删除合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_automatic_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_partner_merge_automatic_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_partner_merge_automatic_wizardMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Get')")
    @ApiOperation(value = "获取合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "获取合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_automatic_wizards/{base_partner_merge_automatic_wizard_id}")
    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> get(@PathVariable("base_partner_merge_automatic_wizard_id") Long base_partner_merge_automatic_wizard_id) {
        Base_partner_merge_automatic_wizard domain = base_partner_merge_automatic_wizardService.get(base_partner_merge_automatic_wizard_id);
        Base_partner_merge_automatic_wizardDTO dto = base_partner_merge_automatic_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取合并业务伙伴向导草稿", tags = {"合并业务伙伴向导" },  notes = "获取合并业务伙伴向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_automatic_wizards/getdraft")
    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_automatic_wizardMapping.toDto(base_partner_merge_automatic_wizardService.getDraft(new Base_partner_merge_automatic_wizard())));
    }

    @ApiOperation(value = "检查合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "检查合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_partner_merge_automatic_wizardDTO base_partner_merge_automatic_wizarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_automatic_wizardService.checkKey(base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_automatic_wizardMapping.toDomain(#base_partner_merge_automatic_wizarddto),'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Save')")
    @ApiOperation(value = "保存合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "保存合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_partner_merge_automatic_wizardDTO base_partner_merge_automatic_wizarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_automatic_wizardService.save(base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.base_partner_merge_automatic_wizardMapping.toDomain(#base_partner_merge_automatic_wizarddtos),'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Save')")
    @ApiOperation(value = "批量保存合并业务伙伴向导", tags = {"合并业务伙伴向导" },  notes = "批量保存合并业务伙伴向导")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_partner_merge_automatic_wizardDTO> base_partner_merge_automatic_wizarddtos) {
        base_partner_merge_automatic_wizardService.saveBatch(base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_partner_merge_automatic_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Get')")
	@ApiOperation(value = "获取数据集", tags = {"合并业务伙伴向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_partner_merge_automatic_wizards/fetchdefault")
	public ResponseEntity<List<Base_partner_merge_automatic_wizardDTO>> fetchDefault(Base_partner_merge_automatic_wizardSearchContext context) {
        Page<Base_partner_merge_automatic_wizard> domains = base_partner_merge_automatic_wizardService.searchDefault(context) ;
        List<Base_partner_merge_automatic_wizardDTO> list = base_partner_merge_automatic_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_partner_merge_automatic_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_partner_merge_automatic_wizard-Get')")
	@ApiOperation(value = "查询数据集", tags = {"合并业务伙伴向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_partner_merge_automatic_wizards/searchdefault")
	public ResponseEntity<Page<Base_partner_merge_automatic_wizardDTO>> searchDefault(@RequestBody Base_partner_merge_automatic_wizardSearchContext context) {
        Page<Base_partner_merge_automatic_wizard> domains = base_partner_merge_automatic_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_partner_merge_automatic_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


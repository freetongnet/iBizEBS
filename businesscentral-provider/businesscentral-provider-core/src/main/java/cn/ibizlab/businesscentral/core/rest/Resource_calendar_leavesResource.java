package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendar_leavesService;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"休假详情" })
@RestController("Core-resource_calendar_leaves")
@RequestMapping("")
public class Resource_calendar_leavesResource {

    @Autowired
    public IResource_calendar_leavesService resource_calendar_leavesService;

    @Autowired
    @Lazy
    public Resource_calendar_leavesMapping resource_calendar_leavesMapping;

    @PreAuthorize("hasPermission(this.resource_calendar_leavesMapping.toDomain(#resource_calendar_leavesdto),'iBizBusinessCentral-Resource_calendar_leaves-Create')")
    @ApiOperation(value = "新建休假详情", tags = {"休假详情" },  notes = "新建休假详情")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves")
    public ResponseEntity<Resource_calendar_leavesDTO> create(@Validated @RequestBody Resource_calendar_leavesDTO resource_calendar_leavesdto) {
        Resource_calendar_leaves domain = resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdto);
		resource_calendar_leavesService.create(domain);
        Resource_calendar_leavesDTO dto = resource_calendar_leavesMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_calendar_leavesMapping.toDomain(#resource_calendar_leavesdtos),'iBizBusinessCentral-Resource_calendar_leaves-Create')")
    @ApiOperation(value = "批量新建休假详情", tags = {"休假详情" },  notes = "批量新建休假详情")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_calendar_leavesDTO> resource_calendar_leavesdtos) {
        resource_calendar_leavesService.createBatch(resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "resource_calendar_leaves" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.resource_calendar_leavesService.get(#resource_calendar_leaves_id),'iBizBusinessCentral-Resource_calendar_leaves-Update')")
    @ApiOperation(value = "更新休假详情", tags = {"休假详情" },  notes = "更新休假详情")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_leaves/{resource_calendar_leaves_id}")
    public ResponseEntity<Resource_calendar_leavesDTO> update(@PathVariable("resource_calendar_leaves_id") Long resource_calendar_leaves_id, @RequestBody Resource_calendar_leavesDTO resource_calendar_leavesdto) {
		Resource_calendar_leaves domain  = resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdto);
        domain .setId(resource_calendar_leaves_id);
		resource_calendar_leavesService.update(domain );
		Resource_calendar_leavesDTO dto = resource_calendar_leavesMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_calendar_leavesService.getResourceCalendarLeavesByEntities(this.resource_calendar_leavesMapping.toDomain(#resource_calendar_leavesdtos)),'iBizBusinessCentral-Resource_calendar_leaves-Update')")
    @ApiOperation(value = "批量更新休假详情", tags = {"休假详情" },  notes = "批量更新休假详情")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_leaves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_calendar_leavesDTO> resource_calendar_leavesdtos) {
        resource_calendar_leavesService.updateBatch(resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.resource_calendar_leavesService.get(#resource_calendar_leaves_id),'iBizBusinessCentral-Resource_calendar_leaves-Remove')")
    @ApiOperation(value = "删除休假详情", tags = {"休假详情" },  notes = "删除休假详情")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_leaves/{resource_calendar_leaves_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("resource_calendar_leaves_id") Long resource_calendar_leaves_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_calendar_leavesService.remove(resource_calendar_leaves_id));
    }

    @PreAuthorize("hasPermission(this.resource_calendar_leavesService.getResourceCalendarLeavesByIds(#ids),'iBizBusinessCentral-Resource_calendar_leaves-Remove')")
    @ApiOperation(value = "批量删除休假详情", tags = {"休假详情" },  notes = "批量删除休假详情")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_leaves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        resource_calendar_leavesService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.resource_calendar_leavesMapping.toDomain(returnObject.body),'iBizBusinessCentral-Resource_calendar_leaves-Get')")
    @ApiOperation(value = "获取休假详情", tags = {"休假详情" },  notes = "获取休假详情")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_leaves/{resource_calendar_leaves_id}")
    public ResponseEntity<Resource_calendar_leavesDTO> get(@PathVariable("resource_calendar_leaves_id") Long resource_calendar_leaves_id) {
        Resource_calendar_leaves domain = resource_calendar_leavesService.get(resource_calendar_leaves_id);
        Resource_calendar_leavesDTO dto = resource_calendar_leavesMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取休假详情草稿", tags = {"休假详情" },  notes = "获取休假详情草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_leaves/getdraft")
    public ResponseEntity<Resource_calendar_leavesDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(resource_calendar_leavesMapping.toDto(resource_calendar_leavesService.getDraft(new Resource_calendar_leaves())));
    }

    @ApiOperation(value = "检查休假详情", tags = {"休假详情" },  notes = "检查休假详情")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Resource_calendar_leavesDTO resource_calendar_leavesdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(resource_calendar_leavesService.checkKey(resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdto)));
    }

    @PreAuthorize("hasPermission(this.resource_calendar_leavesMapping.toDomain(#resource_calendar_leavesdto),'iBizBusinessCentral-Resource_calendar_leaves-Save')")
    @ApiOperation(value = "保存休假详情", tags = {"休假详情" },  notes = "保存休假详情")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves/save")
    public ResponseEntity<Boolean> save(@RequestBody Resource_calendar_leavesDTO resource_calendar_leavesdto) {
        return ResponseEntity.status(HttpStatus.OK).body(resource_calendar_leavesService.save(resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdto)));
    }

    @PreAuthorize("hasPermission(this.resource_calendar_leavesMapping.toDomain(#resource_calendar_leavesdtos),'iBizBusinessCentral-Resource_calendar_leaves-Save')")
    @ApiOperation(value = "批量保存休假详情", tags = {"休假详情" },  notes = "批量保存休假详情")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Resource_calendar_leavesDTO> resource_calendar_leavesdtos) {
        resource_calendar_leavesService.saveBatch(resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_calendar_leaves-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_calendar_leaves-Get')")
	@ApiOperation(value = "获取数据集", tags = {"休假详情" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/resource_calendar_leaves/fetchdefault")
	public ResponseEntity<List<Resource_calendar_leavesDTO>> fetchDefault(Resource_calendar_leavesSearchContext context) {
        Page<Resource_calendar_leaves> domains = resource_calendar_leavesService.searchDefault(context) ;
        List<Resource_calendar_leavesDTO> list = resource_calendar_leavesMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_calendar_leaves-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_calendar_leaves-Get')")
	@ApiOperation(value = "查询数据集", tags = {"休假详情" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/resource_calendar_leaves/searchdefault")
	public ResponseEntity<Page<Resource_calendar_leavesDTO>> searchDefault(@RequestBody Resource_calendar_leavesSearchContext context) {
        Page<Resource_calendar_leaves> domains = resource_calendar_leavesService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_calendar_leavesMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Stock_ruleDTO]
 */
@Data
public class Stock_ruleDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [PROCURE_METHOD]
     *
     */
    @JSONField(name = "procure_method")
    @JsonProperty("procure_method")
    @NotBlank(message = "[移动供应方法]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String procureMethod;

    /**
     * 属性 [PROPAGATE]
     *
     */
    @JSONField(name = "propagate")
    @JsonProperty("propagate")
    private Boolean propagate;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    private Integer groupId;

    /**
     * 属性 [RULE_MESSAGE]
     *
     */
    @JSONField(name = "rule_message")
    @JsonProperty("rule_message")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String ruleMessage;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [AUTO]
     *
     */
    @JSONField(name = "auto")
    @JsonProperty("auto")
    @NotBlank(message = "[自动移动]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String auto;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DELAY]
     *
     */
    @JSONField(name = "delay")
    @JsonProperty("delay")
    private Integer delay;

    /**
     * 属性 [GROUP_PROPAGATION_OPTION]
     *
     */
    @JSONField(name = "group_propagation_option")
    @JsonProperty("group_propagation_option")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String groupPropagationOption;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ACTION]
     *
     */
    @JSONField(name = "action")
    @JsonProperty("action")
    @NotBlank(message = "[动作]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String action;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [PARTNER_ADDRESS_ID_TEXT]
     *
     */
    @JSONField(name = "partner_address_id_text")
    @JsonProperty("partner_address_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerAddressIdText;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String locationIdText;

    /**
     * 属性 [ROUTE_ID_TEXT]
     *
     */
    @JSONField(name = "route_id_text")
    @JsonProperty("route_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String routeIdText;

    /**
     * 属性 [ROUTE_SEQUENCE]
     *
     */
    @JSONField(name = "route_sequence")
    @JsonProperty("route_sequence")
    private Integer routeSequence;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pickingTypeIdText;

    /**
     * 属性 [PROPAGATE_WAREHOUSE_ID_TEXT]
     *
     */
    @JSONField(name = "propagate_warehouse_id_text")
    @JsonProperty("propagate_warehouse_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String propagateWarehouseIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [LOCATION_SRC_ID_TEXT]
     *
     */
    @JSONField(name = "location_src_id_text")
    @JsonProperty("location_src_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String locationSrcIdText;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String warehouseIdText;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[目的位置]不允许为空!")
    private Long locationId;

    /**
     * 属性 [PARTNER_ADDRESS_ID]
     *
     */
    @JSONField(name = "partner_address_id")
    @JsonProperty("partner_address_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerAddressId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long warehouseId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[作业类型]不允许为空!")
    private Long pickingTypeId;

    /**
     * 属性 [ROUTE_ID]
     *
     */
    @JSONField(name = "route_id")
    @JsonProperty("route_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[路线]不允许为空!")
    private Long routeId;

    /**
     * 属性 [LOCATION_SRC_ID]
     *
     */
    @JSONField(name = "location_src_id")
    @JsonProperty("location_src_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long locationSrcId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [PROPAGATE_WAREHOUSE_ID]
     *
     */
    @JSONField(name = "propagate_warehouse_id")
    @JsonProperty("propagate_warehouse_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long propagateWarehouseId;


    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [PROCURE_METHOD]
     */
    public void setProcureMethod(String  procureMethod){
        this.procureMethod = procureMethod ;
        this.modify("procure_method",procureMethod);
    }

    /**
     * 设置 [PROPAGATE]
     */
    public void setPropagate(Boolean  propagate){
        this.propagate = propagate ;
        this.modify("propagate",propagate);
    }

    /**
     * 设置 [GROUP_ID]
     */
    public void setGroupId(Integer  groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [AUTO]
     */
    public void setAuto(String  auto){
        this.auto = auto ;
        this.modify("auto",auto);
    }

    /**
     * 设置 [DELAY]
     */
    public void setDelay(Integer  delay){
        this.delay = delay ;
        this.modify("delay",delay);
    }

    /**
     * 设置 [GROUP_PROPAGATION_OPTION]
     */
    public void setGroupPropagationOption(String  groupPropagationOption){
        this.groupPropagationOption = groupPropagationOption ;
        this.modify("group_propagation_option",groupPropagationOption);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [ACTION]
     */
    public void setAction(String  action){
        this.action = action ;
        this.modify("action",action);
    }

    /**
     * 设置 [LOCATION_ID]
     */
    public void setLocationId(Long  locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [PARTNER_ADDRESS_ID]
     */
    public void setPartnerAddressId(Long  partnerAddressId){
        this.partnerAddressId = partnerAddressId ;
        this.modify("partner_address_id",partnerAddressId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    public void setWarehouseId(Long  warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    public void setPickingTypeId(Long  pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }

    /**
     * 设置 [ROUTE_ID]
     */
    public void setRouteId(Long  routeId){
        this.routeId = routeId ;
        this.modify("route_id",routeId);
    }

    /**
     * 设置 [LOCATION_SRC_ID]
     */
    public void setLocationSrcId(Long  locationSrcId){
        this.locationSrcId = locationSrcId ;
        this.modify("location_src_id",locationSrcId);
    }

    /**
     * 设置 [PROPAGATE_WAREHOUSE_ID]
     */
    public void setPropagateWarehouseId(Long  propagateWarehouseId){
        this.propagateWarehouseId = propagateWarehouseId ;
        this.modify("propagate_warehouse_id",propagateWarehouseId);
    }


}



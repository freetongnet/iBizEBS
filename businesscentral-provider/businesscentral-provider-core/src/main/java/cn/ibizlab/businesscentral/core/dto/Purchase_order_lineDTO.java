package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Purchase_order_lineDTO]
 */
@Data
public class Purchase_order_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    @NotNull(message = "[单价]不允许为空!")
    private Double priceUnit;

    /**
     * 属性 [QTY_RECEIVED]
     *
     */
    @JSONField(name = "qty_received")
    @JsonProperty("qty_received")
    private Double qtyReceived;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private BigDecimal priceTotal;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[说明]不允许为空!")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    private String name;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    @NotNull(message = "[数量]不允许为空!")
    private Double productQty;

    /**
     * 属性 [PRICE_TAX]
     *
     */
    @JSONField(name = "price_tax")
    @JsonProperty("price_tax")
    private Double priceTax;

    /**
     * 属性 [MOVE_IDS]
     *
     */
    @JSONField(name = "move_ids")
    @JsonProperty("move_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String moveIds;

    /**
     * 属性 [PRICE_SUBTOTAL]
     *
     */
    @JSONField(name = "price_subtotal")
    @JsonProperty("price_subtotal")
    private BigDecimal priceSubtotal;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;

    /**
     * 属性 [TAXES_ID]
     *
     */
    @JSONField(name = "taxes_id")
    @JsonProperty("taxes_id")
    private String taxesId;
    /**
     * 属性 [INVOICE_LINES]
     *
     */
    @JSONField(name = "invoice_lines")
    @JsonProperty("invoice_lines")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceLines;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DATE_PLANNED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned")
    @NotNull(message = "[计划日期]不允许为空!")
    private Timestamp datePlanned;

    /**
     * 属性 [QTY_INVOICED]
     *
     */
    @JSONField(name = "qty_invoiced")
    @JsonProperty("qty_invoiced")
    private Double qtyInvoiced;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String analyticTagIds;

    /**
     * 属性 [MOVE_DEST_IDS]
     *
     */
    @JSONField(name = "move_dest_ids")
    @JsonProperty("move_dest_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String moveDestIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [ORDERPOINT_ID_TEXT]
     *
     */
    @JSONField(name = "orderpoint_id_text")
    @JsonProperty("orderpoint_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String orderpointIdText;

    /**
     * 属性 [SALE_ORDER_ID_TEXT]
     *
     */
    @JSONField(name = "sale_order_id_text")
    @JsonProperty("sale_order_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String saleOrderIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [PRODUCT_TYPE]
     *
     */
    @JSONField(name = "product_type")
    @JsonProperty("product_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productType;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID_TEXT]
     *
     */
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountAnalyticIdText;

    /**
     * 属性 [PRODUCT_IMAGE]
     *
     */
    @JSONField(name = "product_image")
    @JsonProperty("product_image")
    private byte[] productImage;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productUomText;

    /**
     * 属性 [ORDER_ID_TEXT]
     *
     */
    @JSONField(name = "order_id_text")
    @JsonProperty("order_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String orderIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [SALE_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "sale_line_id_text")
    @JsonProperty("sale_line_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String saleLineIdText;

    /**
     * 属性 [PRODUCT_UOM_CATEGORY_ID]
     *
     */
    @JSONField(name = "product_uom_category_id")
    @JsonProperty("product_uom_category_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productUomCategoryId;

    /**
     * 属性 [DATE_ORDER]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_order" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_order")
    private Timestamp dateOrder;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountAnalyticId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [SALE_ORDER_ID]
     *
     */
    @JSONField(name = "sale_order_id")
    @JsonProperty("sale_order_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long saleOrderId;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[计量单位]不允许为空!")
    private Long productUom;

    /**
     * 属性 [SALE_LINE_ID]
     *
     */
    @JSONField(name = "sale_line_id")
    @JsonProperty("sale_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long saleLineId;

    /**
     * 属性 [ORDER_ID]
     *
     */
    @JSONField(name = "order_id")
    @JsonProperty("order_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[订单关联]不允许为空!")
    private Long orderId;

    /**
     * 属性 [ORDERPOINT_ID]
     *
     */
    @JSONField(name = "orderpoint_id")
    @JsonProperty("orderpoint_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long orderpointId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[产品]不允许为空!")
    private Long productId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;


    /**
     * 设置 [PRICE_UNIT]
     */
    public void setPriceUnit(Double  priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [QTY_RECEIVED]
     */
    public void setQtyReceived(Double  qtyReceived){
        this.qtyReceived = qtyReceived ;
        this.modify("qty_received",qtyReceived);
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    public void setPriceTotal(BigDecimal  priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    public void setProductQty(Double  productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [PRICE_TAX]
     */
    public void setPriceTax(Double  priceTax){
        this.priceTax = priceTax ;
        this.modify("price_tax",priceTax);
    }

    /**
     * 设置 [PRICE_SUBTOTAL]
     */
    public void setPriceSubtotal(BigDecimal  priceSubtotal){
        this.priceSubtotal = priceSubtotal ;
        this.modify("price_subtotal",priceSubtotal);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    public void setProductUomQty(Double  productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [TAXES_ID]
     */
    public void setTaxesId(String  taxesId){
        this.taxesId = taxesId ;
        this.modify("taxes_id",taxesId);
    }

    /**
     * 设置 [DATE_PLANNED]
     */
    public void setDatePlanned(Timestamp  datePlanned){
        this.datePlanned = datePlanned ;
        this.modify("date_planned",datePlanned);
    }

    /**
     * 设置 [QTY_INVOICED]
     */
    public void setQtyInvoiced(Double  qtyInvoiced){
        this.qtyInvoiced = qtyInvoiced ;
        this.modify("qty_invoiced",qtyInvoiced);
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    public void setAccountAnalyticId(Long  accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [SALE_ORDER_ID]
     */
    public void setSaleOrderId(Long  saleOrderId){
        this.saleOrderId = saleOrderId ;
        this.modify("sale_order_id",saleOrderId);
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    public void setProductUom(Long  productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [SALE_LINE_ID]
     */
    public void setSaleLineId(Long  saleLineId){
        this.saleLineId = saleLineId ;
        this.modify("sale_line_id",saleLineId);
    }

    /**
     * 设置 [ORDER_ID]
     */
    public void setOrderId(Long  orderId){
        this.orderId = orderId ;
        this.modify("order_id",orderId);
    }

    /**
     * 设置 [ORDERPOINT_ID]
     */
    public void setOrderpointId(Long  orderpointId){
        this.orderpointId = orderpointId ;
        this.modify("orderpoint_id",orderpointId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }


}



package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_removal;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_removalService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_removalSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"下架策略" })
@RestController("Core-product_removal")
@RequestMapping("")
public class Product_removalResource {

    @Autowired
    public IProduct_removalService product_removalService;

    @Autowired
    @Lazy
    public Product_removalMapping product_removalMapping;

    @PreAuthorize("hasPermission(this.product_removalMapping.toDomain(#product_removaldto),'iBizBusinessCentral-Product_removal-Create')")
    @ApiOperation(value = "新建下架策略", tags = {"下架策略" },  notes = "新建下架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_removals")
    public ResponseEntity<Product_removalDTO> create(@Validated @RequestBody Product_removalDTO product_removaldto) {
        Product_removal domain = product_removalMapping.toDomain(product_removaldto);
		product_removalService.create(domain);
        Product_removalDTO dto = product_removalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_removalMapping.toDomain(#product_removaldtos),'iBizBusinessCentral-Product_removal-Create')")
    @ApiOperation(value = "批量新建下架策略", tags = {"下架策略" },  notes = "批量新建下架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_removals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_removalDTO> product_removaldtos) {
        product_removalService.createBatch(product_removalMapping.toDomain(product_removaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_removal" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_removalService.get(#product_removal_id),'iBizBusinessCentral-Product_removal-Update')")
    @ApiOperation(value = "更新下架策略", tags = {"下架策略" },  notes = "更新下架策略")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_removals/{product_removal_id}")
    public ResponseEntity<Product_removalDTO> update(@PathVariable("product_removal_id") Long product_removal_id, @RequestBody Product_removalDTO product_removaldto) {
		Product_removal domain  = product_removalMapping.toDomain(product_removaldto);
        domain .setId(product_removal_id);
		product_removalService.update(domain );
		Product_removalDTO dto = product_removalMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_removalService.getProductRemovalByEntities(this.product_removalMapping.toDomain(#product_removaldtos)),'iBizBusinessCentral-Product_removal-Update')")
    @ApiOperation(value = "批量更新下架策略", tags = {"下架策略" },  notes = "批量更新下架策略")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_removals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_removalDTO> product_removaldtos) {
        product_removalService.updateBatch(product_removalMapping.toDomain(product_removaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_removalService.get(#product_removal_id),'iBizBusinessCentral-Product_removal-Remove')")
    @ApiOperation(value = "删除下架策略", tags = {"下架策略" },  notes = "删除下架策略")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_removals/{product_removal_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_removal_id") Long product_removal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_removalService.remove(product_removal_id));
    }

    @PreAuthorize("hasPermission(this.product_removalService.getProductRemovalByIds(#ids),'iBizBusinessCentral-Product_removal-Remove')")
    @ApiOperation(value = "批量删除下架策略", tags = {"下架策略" },  notes = "批量删除下架策略")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_removals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_removalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_removalMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_removal-Get')")
    @ApiOperation(value = "获取下架策略", tags = {"下架策略" },  notes = "获取下架策略")
	@RequestMapping(method = RequestMethod.GET, value = "/product_removals/{product_removal_id}")
    public ResponseEntity<Product_removalDTO> get(@PathVariable("product_removal_id") Long product_removal_id) {
        Product_removal domain = product_removalService.get(product_removal_id);
        Product_removalDTO dto = product_removalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取下架策略草稿", tags = {"下架策略" },  notes = "获取下架策略草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_removals/getdraft")
    public ResponseEntity<Product_removalDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_removalMapping.toDto(product_removalService.getDraft(new Product_removal())));
    }

    @ApiOperation(value = "检查下架策略", tags = {"下架策略" },  notes = "检查下架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_removals/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_removalDTO product_removaldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_removalService.checkKey(product_removalMapping.toDomain(product_removaldto)));
    }

    @PreAuthorize("hasPermission(this.product_removalMapping.toDomain(#product_removaldto),'iBizBusinessCentral-Product_removal-Save')")
    @ApiOperation(value = "保存下架策略", tags = {"下架策略" },  notes = "保存下架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_removals/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_removalDTO product_removaldto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_removalService.save(product_removalMapping.toDomain(product_removaldto)));
    }

    @PreAuthorize("hasPermission(this.product_removalMapping.toDomain(#product_removaldtos),'iBizBusinessCentral-Product_removal-Save')")
    @ApiOperation(value = "批量保存下架策略", tags = {"下架策略" },  notes = "批量保存下架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_removals/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_removalDTO> product_removaldtos) {
        product_removalService.saveBatch(product_removalMapping.toDomain(product_removaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_removal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_removal-Get')")
	@ApiOperation(value = "获取数据集", tags = {"下架策略" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_removals/fetchdefault")
	public ResponseEntity<List<Product_removalDTO>> fetchDefault(Product_removalSearchContext context) {
        Page<Product_removal> domains = product_removalService.searchDefault(context) ;
        List<Product_removalDTO> list = product_removalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_removal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_removal-Get')")
	@ApiOperation(value = "查询数据集", tags = {"下架策略" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_removals/searchdefault")
	public ResponseEntity<Page<Product_removalDTO>> searchDefault(@RequestBody Product_removalSearchContext context) {
        Page<Product_removal> domains = product_removalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_removalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Hr_employee_skillDTO]
 */
@Data
public class Hr_employee_skillDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [SKILL_TYPE_NAME]
     *
     */
    @JSONField(name = "skill_type_name")
    @JsonProperty("skill_type_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String skillTypeName;

    /**
     * 属性 [SKILL_LEVEL_NAME]
     *
     */
    @JSONField(name = "skill_level_name")
    @JsonProperty("skill_level_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String skillLevelName;

    /**
     * 属性 [SKILL_NAME]
     *
     */
    @JSONField(name = "skill_name")
    @JsonProperty("skill_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String skillName;

    /**
     * 属性 [EMPLOYEE_NAME]
     *
     */
    @JSONField(name = "employee_name")
    @JsonProperty("employee_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String employeeName;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long employeeId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [SKILL_ID]
     *
     */
    @JSONField(name = "skill_id")
    @JsonProperty("skill_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skillId;

    /**
     * 属性 [SKILL_TYPE_ID]
     *
     */
    @JSONField(name = "skill_type_id")
    @JsonProperty("skill_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skillTypeId;

    /**
     * 属性 [SKILL_LEVEL_ID]
     *
     */
    @JSONField(name = "skill_level_id")
    @JsonProperty("skill_level_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long skillLevelId;


    /**
     * 设置 [CREATE_DATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 设置 [WRITE_DATE]
     */
    public void setWriteDate(Timestamp  writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    public void setEmployeeId(Long  employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }

    /**
     * 设置 [CREATE_UID]
     */
    public void setCreateUid(Long  createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }

    /**
     * 设置 [WRITE_UID]
     */
    public void setWriteUid(Long  writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [SKILL_ID]
     */
    public void setSkillId(Long  skillId){
        this.skillId = skillId ;
        this.modify("skill_id",skillId);
    }

    /**
     * 设置 [SKILL_TYPE_ID]
     */
    public void setSkillTypeId(Long  skillTypeId){
        this.skillTypeId = skillTypeId ;
        this.modify("skill_type_id",skillTypeId);
    }

    /**
     * 设置 [SKILL_LEVEL_ID]
     */
    public void setSkillLevelId(Long  skillLevelId){
        this.skillLevelId = skillLevelId ;
        this.modify("skill_level_id",skillLevelId);
    }


}



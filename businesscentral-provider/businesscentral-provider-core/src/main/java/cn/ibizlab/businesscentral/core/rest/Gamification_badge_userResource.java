package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badge_userService;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badge_userSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"游戏化用户徽章" })
@RestController("Core-gamification_badge_user")
@RequestMapping("")
public class Gamification_badge_userResource {

    @Autowired
    public IGamification_badge_userService gamification_badge_userService;

    @Autowired
    @Lazy
    public Gamification_badge_userMapping gamification_badge_userMapping;

    @PreAuthorize("hasPermission(this.gamification_badge_userMapping.toDomain(#gamification_badge_userdto),'iBizBusinessCentral-Gamification_badge_user-Create')")
    @ApiOperation(value = "新建游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "新建游戏化用户徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users")
    public ResponseEntity<Gamification_badge_userDTO> create(@Validated @RequestBody Gamification_badge_userDTO gamification_badge_userdto) {
        Gamification_badge_user domain = gamification_badge_userMapping.toDomain(gamification_badge_userdto);
		gamification_badge_userService.create(domain);
        Gamification_badge_userDTO dto = gamification_badge_userMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_badge_userMapping.toDomain(#gamification_badge_userdtos),'iBizBusinessCentral-Gamification_badge_user-Create')")
    @ApiOperation(value = "批量新建游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "批量新建游戏化用户徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_badge_userDTO> gamification_badge_userdtos) {
        gamification_badge_userService.createBatch(gamification_badge_userMapping.toDomain(gamification_badge_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "gamification_badge_user" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.gamification_badge_userService.get(#gamification_badge_user_id),'iBizBusinessCentral-Gamification_badge_user-Update')")
    @ApiOperation(value = "更新游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "更新游戏化用户徽章")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_users/{gamification_badge_user_id}")
    public ResponseEntity<Gamification_badge_userDTO> update(@PathVariable("gamification_badge_user_id") Long gamification_badge_user_id, @RequestBody Gamification_badge_userDTO gamification_badge_userdto) {
		Gamification_badge_user domain  = gamification_badge_userMapping.toDomain(gamification_badge_userdto);
        domain .setId(gamification_badge_user_id);
		gamification_badge_userService.update(domain );
		Gamification_badge_userDTO dto = gamification_badge_userMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_badge_userService.getGamificationBadgeUserByEntities(this.gamification_badge_userMapping.toDomain(#gamification_badge_userdtos)),'iBizBusinessCentral-Gamification_badge_user-Update')")
    @ApiOperation(value = "批量更新游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "批量更新游戏化用户徽章")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_badge_users/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_badge_userDTO> gamification_badge_userdtos) {
        gamification_badge_userService.updateBatch(gamification_badge_userMapping.toDomain(gamification_badge_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.gamification_badge_userService.get(#gamification_badge_user_id),'iBizBusinessCentral-Gamification_badge_user-Remove')")
    @ApiOperation(value = "删除游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "删除游戏化用户徽章")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_users/{gamification_badge_user_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("gamification_badge_user_id") Long gamification_badge_user_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_badge_userService.remove(gamification_badge_user_id));
    }

    @PreAuthorize("hasPermission(this.gamification_badge_userService.getGamificationBadgeUserByIds(#ids),'iBizBusinessCentral-Gamification_badge_user-Remove')")
    @ApiOperation(value = "批量删除游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "批量删除游戏化用户徽章")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badge_users/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        gamification_badge_userService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.gamification_badge_userMapping.toDomain(returnObject.body),'iBizBusinessCentral-Gamification_badge_user-Get')")
    @ApiOperation(value = "获取游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "获取游戏化用户徽章")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_users/{gamification_badge_user_id}")
    public ResponseEntity<Gamification_badge_userDTO> get(@PathVariable("gamification_badge_user_id") Long gamification_badge_user_id) {
        Gamification_badge_user domain = gamification_badge_userService.get(gamification_badge_user_id);
        Gamification_badge_userDTO dto = gamification_badge_userMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取游戏化用户徽章草稿", tags = {"游戏化用户徽章" },  notes = "获取游戏化用户徽章草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_badge_users/getdraft")
    public ResponseEntity<Gamification_badge_userDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_badge_userMapping.toDto(gamification_badge_userService.getDraft(new Gamification_badge_user())));
    }

    @ApiOperation(value = "检查游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "检查游戏化用户徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Gamification_badge_userDTO gamification_badge_userdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(gamification_badge_userService.checkKey(gamification_badge_userMapping.toDomain(gamification_badge_userdto)));
    }

    @PreAuthorize("hasPermission(this.gamification_badge_userMapping.toDomain(#gamification_badge_userdto),'iBizBusinessCentral-Gamification_badge_user-Save')")
    @ApiOperation(value = "保存游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "保存游戏化用户徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/save")
    public ResponseEntity<Boolean> save(@RequestBody Gamification_badge_userDTO gamification_badge_userdto) {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_badge_userService.save(gamification_badge_userMapping.toDomain(gamification_badge_userdto)));
    }

    @PreAuthorize("hasPermission(this.gamification_badge_userMapping.toDomain(#gamification_badge_userdtos),'iBizBusinessCentral-Gamification_badge_user-Save')")
    @ApiOperation(value = "批量保存游戏化用户徽章", tags = {"游戏化用户徽章" },  notes = "批量保存游戏化用户徽章")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_badge_users/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Gamification_badge_userDTO> gamification_badge_userdtos) {
        gamification_badge_userService.saveBatch(gamification_badge_userMapping.toDomain(gamification_badge_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_badge_user-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_badge_user-Get')")
	@ApiOperation(value = "获取数据集", tags = {"游戏化用户徽章" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_badge_users/fetchdefault")
	public ResponseEntity<List<Gamification_badge_userDTO>> fetchDefault(Gamification_badge_userSearchContext context) {
        Page<Gamification_badge_user> domains = gamification_badge_userService.searchDefault(context) ;
        List<Gamification_badge_userDTO> list = gamification_badge_userMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_badge_user-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_badge_user-Get')")
	@ApiOperation(value = "查询数据集", tags = {"游戏化用户徽章" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/gamification_badge_users/searchdefault")
	public ResponseEntity<Page<Gamification_badge_userDTO>> searchDefault(@RequestBody Gamification_badge_userSearchContext context) {
        Page<Gamification_badge_user> domains = gamification_badge_userService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_badge_userMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.businesscentral.core.dto.Fleet_vehicle_log_contractDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreFleet_vehicle_log_contractMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Fleet_vehicle_log_contractMapping extends MappingBase<Fleet_vehicle_log_contractDTO, Fleet_vehicle_log_contract> {


}


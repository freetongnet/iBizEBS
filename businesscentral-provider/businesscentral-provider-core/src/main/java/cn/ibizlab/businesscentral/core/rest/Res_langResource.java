package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_lang;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_langService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_langSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"语言" })
@RestController("Core-res_lang")
@RequestMapping("")
public class Res_langResource {

    @Autowired
    public IRes_langService res_langService;

    @Autowired
    @Lazy
    public Res_langMapping res_langMapping;

    @PreAuthorize("hasPermission(this.res_langMapping.toDomain(#res_langdto),'iBizBusinessCentral-Res_lang-Create')")
    @ApiOperation(value = "新建语言", tags = {"语言" },  notes = "新建语言")
	@RequestMapping(method = RequestMethod.POST, value = "/res_langs")
    public ResponseEntity<Res_langDTO> create(@Validated @RequestBody Res_langDTO res_langdto) {
        Res_lang domain = res_langMapping.toDomain(res_langdto);
		res_langService.create(domain);
        Res_langDTO dto = res_langMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_langMapping.toDomain(#res_langdtos),'iBizBusinessCentral-Res_lang-Create')")
    @ApiOperation(value = "批量新建语言", tags = {"语言" },  notes = "批量新建语言")
	@RequestMapping(method = RequestMethod.POST, value = "/res_langs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_langDTO> res_langdtos) {
        res_langService.createBatch(res_langMapping.toDomain(res_langdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_lang" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_langService.get(#res_lang_id),'iBizBusinessCentral-Res_lang-Update')")
    @ApiOperation(value = "更新语言", tags = {"语言" },  notes = "更新语言")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_langs/{res_lang_id}")
    public ResponseEntity<Res_langDTO> update(@PathVariable("res_lang_id") Long res_lang_id, @RequestBody Res_langDTO res_langdto) {
		Res_lang domain  = res_langMapping.toDomain(res_langdto);
        domain .setId(res_lang_id);
		res_langService.update(domain );
		Res_langDTO dto = res_langMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_langService.getResLangByEntities(this.res_langMapping.toDomain(#res_langdtos)),'iBizBusinessCentral-Res_lang-Update')")
    @ApiOperation(value = "批量更新语言", tags = {"语言" },  notes = "批量更新语言")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_langs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_langDTO> res_langdtos) {
        res_langService.updateBatch(res_langMapping.toDomain(res_langdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_langService.get(#res_lang_id),'iBizBusinessCentral-Res_lang-Remove')")
    @ApiOperation(value = "删除语言", tags = {"语言" },  notes = "删除语言")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_langs/{res_lang_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_lang_id") Long res_lang_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_langService.remove(res_lang_id));
    }

    @PreAuthorize("hasPermission(this.res_langService.getResLangByIds(#ids),'iBizBusinessCentral-Res_lang-Remove')")
    @ApiOperation(value = "批量删除语言", tags = {"语言" },  notes = "批量删除语言")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_langs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_langService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_langMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_lang-Get')")
    @ApiOperation(value = "获取语言", tags = {"语言" },  notes = "获取语言")
	@RequestMapping(method = RequestMethod.GET, value = "/res_langs/{res_lang_id}")
    public ResponseEntity<Res_langDTO> get(@PathVariable("res_lang_id") Long res_lang_id) {
        Res_lang domain = res_langService.get(res_lang_id);
        Res_langDTO dto = res_langMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取语言草稿", tags = {"语言" },  notes = "获取语言草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_langs/getdraft")
    public ResponseEntity<Res_langDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_langMapping.toDto(res_langService.getDraft(new Res_lang())));
    }

    @ApiOperation(value = "检查语言", tags = {"语言" },  notes = "检查语言")
	@RequestMapping(method = RequestMethod.POST, value = "/res_langs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_langDTO res_langdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_langService.checkKey(res_langMapping.toDomain(res_langdto)));
    }

    @PreAuthorize("hasPermission(this.res_langMapping.toDomain(#res_langdto),'iBizBusinessCentral-Res_lang-Save')")
    @ApiOperation(value = "保存语言", tags = {"语言" },  notes = "保存语言")
	@RequestMapping(method = RequestMethod.POST, value = "/res_langs/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_langDTO res_langdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_langService.save(res_langMapping.toDomain(res_langdto)));
    }

    @PreAuthorize("hasPermission(this.res_langMapping.toDomain(#res_langdtos),'iBizBusinessCentral-Res_lang-Save')")
    @ApiOperation(value = "批量保存语言", tags = {"语言" },  notes = "批量保存语言")
	@RequestMapping(method = RequestMethod.POST, value = "/res_langs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_langDTO> res_langdtos) {
        res_langService.saveBatch(res_langMapping.toDomain(res_langdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_lang-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_lang-Get')")
	@ApiOperation(value = "获取数据集", tags = {"语言" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_langs/fetchdefault")
	public ResponseEntity<List<Res_langDTO>> fetchDefault(Res_langSearchContext context) {
        Page<Res_lang> domains = res_langService.searchDefault(context) ;
        List<Res_langDTO> list = res_langMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_lang-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_lang-Get')")
	@ApiOperation(value = "查询数据集", tags = {"语言" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_langs/searchdefault")
	public ResponseEntity<Page<Res_langDTO>> searchDefault(@RequestBody Res_langSearchContext context) {
        Page<Res_lang> domains = res_langService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_langMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_locationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存位置" })
@RestController("Core-stock_location")
@RequestMapping("")
public class Stock_locationResource {

    @Autowired
    public IStock_locationService stock_locationService;

    @Autowired
    @Lazy
    public Stock_locationMapping stock_locationMapping;

    @PreAuthorize("hasPermission(this.stock_locationMapping.toDomain(#stock_locationdto),'iBizBusinessCentral-Stock_location-Create')")
    @ApiOperation(value = "新建库存位置", tags = {"库存位置" },  notes = "新建库存位置")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_locations")
    public ResponseEntity<Stock_locationDTO> create(@Validated @RequestBody Stock_locationDTO stock_locationdto) {
        Stock_location domain = stock_locationMapping.toDomain(stock_locationdto);
		stock_locationService.create(domain);
        Stock_locationDTO dto = stock_locationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_locationMapping.toDomain(#stock_locationdtos),'iBizBusinessCentral-Stock_location-Create')")
    @ApiOperation(value = "批量新建库存位置", tags = {"库存位置" },  notes = "批量新建库存位置")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_locations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_locationDTO> stock_locationdtos) {
        stock_locationService.createBatch(stock_locationMapping.toDomain(stock_locationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_location" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_locationService.get(#stock_location_id),'iBizBusinessCentral-Stock_location-Update')")
    @ApiOperation(value = "更新库存位置", tags = {"库存位置" },  notes = "更新库存位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_locations/{stock_location_id}")
    public ResponseEntity<Stock_locationDTO> update(@PathVariable("stock_location_id") Long stock_location_id, @RequestBody Stock_locationDTO stock_locationdto) {
		Stock_location domain  = stock_locationMapping.toDomain(stock_locationdto);
        domain .setId(stock_location_id);
		stock_locationService.update(domain );
		Stock_locationDTO dto = stock_locationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_locationService.getStockLocationByEntities(this.stock_locationMapping.toDomain(#stock_locationdtos)),'iBizBusinessCentral-Stock_location-Update')")
    @ApiOperation(value = "批量更新库存位置", tags = {"库存位置" },  notes = "批量更新库存位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_locations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_locationDTO> stock_locationdtos) {
        stock_locationService.updateBatch(stock_locationMapping.toDomain(stock_locationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_locationService.get(#stock_location_id),'iBizBusinessCentral-Stock_location-Remove')")
    @ApiOperation(value = "删除库存位置", tags = {"库存位置" },  notes = "删除库存位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_locations/{stock_location_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_location_id") Long stock_location_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_locationService.remove(stock_location_id));
    }

    @PreAuthorize("hasPermission(this.stock_locationService.getStockLocationByIds(#ids),'iBizBusinessCentral-Stock_location-Remove')")
    @ApiOperation(value = "批量删除库存位置", tags = {"库存位置" },  notes = "批量删除库存位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_locations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_locationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_locationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_location-Get')")
    @ApiOperation(value = "获取库存位置", tags = {"库存位置" },  notes = "获取库存位置")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_locations/{stock_location_id}")
    public ResponseEntity<Stock_locationDTO> get(@PathVariable("stock_location_id") Long stock_location_id) {
        Stock_location domain = stock_locationService.get(stock_location_id);
        Stock_locationDTO dto = stock_locationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存位置草稿", tags = {"库存位置" },  notes = "获取库存位置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_locations/getdraft")
    public ResponseEntity<Stock_locationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_locationMapping.toDto(stock_locationService.getDraft(new Stock_location())));
    }

    @ApiOperation(value = "检查库存位置", tags = {"库存位置" },  notes = "检查库存位置")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_locations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_locationDTO stock_locationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_locationService.checkKey(stock_locationMapping.toDomain(stock_locationdto)));
    }

    @PreAuthorize("hasPermission(this.stock_locationMapping.toDomain(#stock_locationdto),'iBizBusinessCentral-Stock_location-Save')")
    @ApiOperation(value = "保存库存位置", tags = {"库存位置" },  notes = "保存库存位置")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_locations/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_locationDTO stock_locationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_locationService.save(stock_locationMapping.toDomain(stock_locationdto)));
    }

    @PreAuthorize("hasPermission(this.stock_locationMapping.toDomain(#stock_locationdtos),'iBizBusinessCentral-Stock_location-Save')")
    @ApiOperation(value = "批量保存库存位置", tags = {"库存位置" },  notes = "批量保存库存位置")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_locations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_locationDTO> stock_locationdtos) {
        stock_locationService.saveBatch(stock_locationMapping.toDomain(stock_locationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_location-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_location-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存位置" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_locations/fetchdefault")
	public ResponseEntity<List<Stock_locationDTO>> fetchDefault(Stock_locationSearchContext context) {
        Page<Stock_location> domains = stock_locationService.searchDefault(context) ;
        List<Stock_locationDTO> list = stock_locationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_location-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_location-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存位置" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_locations/searchdefault")
	public ResponseEntity<Page<Stock_locationDTO>> searchDefault(@RequestBody Stock_locationSearchContext context) {
        Page<Stock_location> domains = stock_locationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_locationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warehouseSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"仓库" })
@RestController("Core-stock_warehouse")
@RequestMapping("")
public class Stock_warehouseResource {

    @Autowired
    public IStock_warehouseService stock_warehouseService;

    @Autowired
    @Lazy
    public Stock_warehouseMapping stock_warehouseMapping;

    @PreAuthorize("hasPermission(this.stock_warehouseMapping.toDomain(#stock_warehousedto),'iBizBusinessCentral-Stock_warehouse-Create')")
    @ApiOperation(value = "新建仓库", tags = {"仓库" },  notes = "新建仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses")
    public ResponseEntity<Stock_warehouseDTO> create(@Validated @RequestBody Stock_warehouseDTO stock_warehousedto) {
        Stock_warehouse domain = stock_warehouseMapping.toDomain(stock_warehousedto);
		stock_warehouseService.create(domain);
        Stock_warehouseDTO dto = stock_warehouseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warehouseMapping.toDomain(#stock_warehousedtos),'iBizBusinessCentral-Stock_warehouse-Create')")
    @ApiOperation(value = "批量新建仓库", tags = {"仓库" },  notes = "批量新建仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_warehouseDTO> stock_warehousedtos) {
        stock_warehouseService.createBatch(stock_warehouseMapping.toDomain(stock_warehousedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_warehouse" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_warehouseService.get(#stock_warehouse_id),'iBizBusinessCentral-Stock_warehouse-Update')")
    @ApiOperation(value = "更新仓库", tags = {"仓库" },  notes = "更新仓库")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouses/{stock_warehouse_id}")
    public ResponseEntity<Stock_warehouseDTO> update(@PathVariable("stock_warehouse_id") Long stock_warehouse_id, @RequestBody Stock_warehouseDTO stock_warehousedto) {
		Stock_warehouse domain  = stock_warehouseMapping.toDomain(stock_warehousedto);
        domain .setId(stock_warehouse_id);
		stock_warehouseService.update(domain );
		Stock_warehouseDTO dto = stock_warehouseMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warehouseService.getStockWarehouseByEntities(this.stock_warehouseMapping.toDomain(#stock_warehousedtos)),'iBizBusinessCentral-Stock_warehouse-Update')")
    @ApiOperation(value = "批量更新仓库", tags = {"仓库" },  notes = "批量更新仓库")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warehouseDTO> stock_warehousedtos) {
        stock_warehouseService.updateBatch(stock_warehouseMapping.toDomain(stock_warehousedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_warehouseService.get(#stock_warehouse_id),'iBizBusinessCentral-Stock_warehouse-Remove')")
    @ApiOperation(value = "删除仓库", tags = {"仓库" },  notes = "删除仓库")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouses/{stock_warehouse_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_warehouse_id") Long stock_warehouse_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_warehouseService.remove(stock_warehouse_id));
    }

    @PreAuthorize("hasPermission(this.stock_warehouseService.getStockWarehouseByIds(#ids),'iBizBusinessCentral-Stock_warehouse-Remove')")
    @ApiOperation(value = "批量删除仓库", tags = {"仓库" },  notes = "批量删除仓库")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_warehouseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_warehouseMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_warehouse-Get')")
    @ApiOperation(value = "获取仓库", tags = {"仓库" },  notes = "获取仓库")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warehouses/{stock_warehouse_id}")
    public ResponseEntity<Stock_warehouseDTO> get(@PathVariable("stock_warehouse_id") Long stock_warehouse_id) {
        Stock_warehouse domain = stock_warehouseService.get(stock_warehouse_id);
        Stock_warehouseDTO dto = stock_warehouseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取仓库草稿", tags = {"仓库" },  notes = "获取仓库草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warehouses/getdraft")
    public ResponseEntity<Stock_warehouseDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warehouseMapping.toDto(stock_warehouseService.getDraft(new Stock_warehouse())));
    }

    @ApiOperation(value = "检查仓库", tags = {"仓库" },  notes = "检查仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_warehouseDTO stock_warehousedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_warehouseService.checkKey(stock_warehouseMapping.toDomain(stock_warehousedto)));
    }

    @PreAuthorize("hasPermission(this.stock_warehouseMapping.toDomain(#stock_warehousedto),'iBizBusinessCentral-Stock_warehouse-Save')")
    @ApiOperation(value = "保存仓库", tags = {"仓库" },  notes = "保存仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_warehouseDTO stock_warehousedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warehouseService.save(stock_warehouseMapping.toDomain(stock_warehousedto)));
    }

    @PreAuthorize("hasPermission(this.stock_warehouseMapping.toDomain(#stock_warehousedtos),'iBizBusinessCentral-Stock_warehouse-Save')")
    @ApiOperation(value = "批量保存仓库", tags = {"仓库" },  notes = "批量保存仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_warehouseDTO> stock_warehousedtos) {
        stock_warehouseService.saveBatch(stock_warehouseMapping.toDomain(stock_warehousedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warehouse-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warehouse-Get')")
	@ApiOperation(value = "获取数据集", tags = {"仓库" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warehouses/fetchdefault")
	public ResponseEntity<List<Stock_warehouseDTO>> fetchDefault(Stock_warehouseSearchContext context) {
        Page<Stock_warehouse> domains = stock_warehouseService.searchDefault(context) ;
        List<Stock_warehouseDTO> list = stock_warehouseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warehouse-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warehouse-Get')")
	@ApiOperation(value = "查询数据集", tags = {"仓库" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_warehouses/searchdefault")
	public ResponseEntity<Page<Stock_warehouseDTO>> searchDefault(@RequestBody Stock_warehouseSearchContext context) {
        Page<Stock_warehouse> domains = stock_warehouseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_warehouseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


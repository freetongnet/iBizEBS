package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partnerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"联系人" })
@RestController("Core-res_partner")
@RequestMapping("")
public class Res_partnerResource {

    @Autowired
    public IRes_partnerService res_partnerService;

    @Autowired
    @Lazy
    public Res_partnerMapping res_partnerMapping;

    @PreAuthorize("hasPermission(this.res_partnerMapping.toDomain(#res_partnerdto),'iBizBusinessCentral-Res_partner-Create')")
    @ApiOperation(value = "新建联系人", tags = {"联系人" },  notes = "新建联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners")
    public ResponseEntity<Res_partnerDTO> create(@Validated @RequestBody Res_partnerDTO res_partnerdto) {
        Res_partner domain = res_partnerMapping.toDomain(res_partnerdto);
		res_partnerService.create(domain);
        Res_partnerDTO dto = res_partnerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partnerMapping.toDomain(#res_partnerdtos),'iBizBusinessCentral-Res_partner-Create')")
    @ApiOperation(value = "批量新建联系人", tags = {"联系人" },  notes = "批量新建联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partnerDTO> res_partnerdtos) {
        res_partnerService.createBatch(res_partnerMapping.toDomain(res_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_partner" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_partnerService.get(#res_partner_id),'iBizBusinessCentral-Res_partner-Update')")
    @ApiOperation(value = "更新联系人", tags = {"联系人" },  notes = "更新联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/{res_partner_id}")
    public ResponseEntity<Res_partnerDTO> update(@PathVariable("res_partner_id") Long res_partner_id, @RequestBody Res_partnerDTO res_partnerdto) {
		Res_partner domain  = res_partnerMapping.toDomain(res_partnerdto);
        domain .setId(res_partner_id);
		res_partnerService.update(domain );
		Res_partnerDTO dto = res_partnerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partnerService.getResPartnerByEntities(this.res_partnerMapping.toDomain(#res_partnerdtos)),'iBizBusinessCentral-Res_partner-Update')")
    @ApiOperation(value = "批量更新联系人", tags = {"联系人" },  notes = "批量更新联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partners/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partnerDTO> res_partnerdtos) {
        res_partnerService.updateBatch(res_partnerMapping.toDomain(res_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_partnerService.get(#res_partner_id),'iBizBusinessCentral-Res_partner-Remove')")
    @ApiOperation(value = "删除联系人", tags = {"联系人" },  notes = "删除联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/{res_partner_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_id") Long res_partner_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.remove(res_partner_id));
    }

    @PreAuthorize("hasPermission(this.res_partnerService.getResPartnerByIds(#ids),'iBizBusinessCentral-Res_partner-Remove')")
    @ApiOperation(value = "批量删除联系人", tags = {"联系人" },  notes = "批量删除联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partners/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_partnerMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_partner-Get')")
    @ApiOperation(value = "获取联系人", tags = {"联系人" },  notes = "获取联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/{res_partner_id}")
    public ResponseEntity<Res_partnerDTO> get(@PathVariable("res_partner_id") Long res_partner_id) {
        Res_partner domain = res_partnerService.get(res_partner_id);
        Res_partnerDTO dto = res_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取联系人草稿", tags = {"联系人" },  notes = "获取联系人草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partners/getdraft")
    public ResponseEntity<Res_partnerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_partnerMapping.toDto(res_partnerService.getDraft(new Res_partner())));
    }

    @ApiOperation(value = "检查联系人", tags = {"联系人" },  notes = "检查联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partnerDTO res_partnerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partnerService.checkKey(res_partnerMapping.toDomain(res_partnerdto)));
    }

    @PreAuthorize("hasPermission(this.res_partnerMapping.toDomain(#res_partnerdto),'iBizBusinessCentral-Res_partner-Save')")
    @ApiOperation(value = "保存联系人", tags = {"联系人" },  notes = "保存联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_partnerDTO res_partnerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.save(res_partnerMapping.toDomain(res_partnerdto)));
    }

    @PreAuthorize("hasPermission(this.res_partnerMapping.toDomain(#res_partnerdtos),'iBizBusinessCentral-Res_partner-Save')")
    @ApiOperation(value = "批量保存联系人", tags = {"联系人" },  notes = "批量保存联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partners/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_partnerDTO> res_partnerdtos) {
        res_partnerService.saveBatch(res_partnerMapping.toDomain(res_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner-Get')")
	@ApiOperation(value = "获取数据集", tags = {"联系人" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_partners/fetchdefault")
	public ResponseEntity<List<Res_partnerDTO>> fetchDefault(Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
        List<Res_partnerDTO> list = res_partnerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner-Get')")
	@ApiOperation(value = "查询数据集", tags = {"联系人" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_partners/searchdefault")
	public ResponseEntity<Page<Res_partnerDTO>> searchDefault(@RequestBody Res_partnerSearchContext context) {
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.res_partnerMapping.toDomain(#res_partnerdto),'iBizBusinessCentral-Res_partner-Create')")
    @ApiOperation(value = "根据供应商建立联系人", tags = {"联系人" },  notes = "根据供应商建立联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partners")
    public ResponseEntity<Res_partnerDTO> createByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_partnerDTO res_partnerdto) {
        Res_partner domain = res_partnerMapping.toDomain(res_partnerdto);
        domain.setParentId(res_supplier_id);
		res_partnerService.create(domain);
        Res_partnerDTO dto = res_partnerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partnerMapping.toDomain(#res_partnerdtos),'iBizBusinessCentral-Res_partner-Create')")
    @ApiOperation(value = "根据供应商批量建立联系人", tags = {"联系人" },  notes = "根据供应商批量建立联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partners/batch")
    public ResponseEntity<Boolean> createBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Res_partnerDTO> res_partnerdtos) {
        List<Res_partner> domainlist=res_partnerMapping.toDomain(res_partnerdtos);
        for(Res_partner domain:domainlist){
            domain.setParentId(res_supplier_id);
        }
        res_partnerService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_partner" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_partnerService.get(#res_partner_id),'iBizBusinessCentral-Res_partner-Update')")
    @ApiOperation(value = "根据供应商更新联系人", tags = {"联系人" },  notes = "根据供应商更新联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/res_partners/{res_partner_id}")
    public ResponseEntity<Res_partnerDTO> updateByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("res_partner_id") Long res_partner_id, @RequestBody Res_partnerDTO res_partnerdto) {
        Res_partner domain = res_partnerMapping.toDomain(res_partnerdto);
        domain.setParentId(res_supplier_id);
        domain.setId(res_partner_id);
		res_partnerService.update(domain);
        Res_partnerDTO dto = res_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partnerService.getResPartnerByEntities(this.res_partnerMapping.toDomain(#res_partnerdtos)),'iBizBusinessCentral-Res_partner-Update')")
    @ApiOperation(value = "根据供应商批量更新联系人", tags = {"联系人" },  notes = "根据供应商批量更新联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/res_partners/batch")
    public ResponseEntity<Boolean> updateBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Res_partnerDTO> res_partnerdtos) {
        List<Res_partner> domainlist=res_partnerMapping.toDomain(res_partnerdtos);
        for(Res_partner domain:domainlist){
            domain.setParentId(res_supplier_id);
        }
        res_partnerService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_partnerService.get(#res_partner_id),'iBizBusinessCentral-Res_partner-Remove')")
    @ApiOperation(value = "根据供应商删除联系人", tags = {"联系人" },  notes = "根据供应商删除联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/res_partners/{res_partner_id}")
    public ResponseEntity<Boolean> removeByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("res_partner_id") Long res_partner_id) {
		return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.remove(res_partner_id));
    }

    @PreAuthorize("hasPermission(this.res_partnerService.getResPartnerByIds(#ids),'iBizBusinessCentral-Res_partner-Remove')")
    @ApiOperation(value = "根据供应商批量删除联系人", tags = {"联系人" },  notes = "根据供应商批量删除联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/res_partners/batch")
    public ResponseEntity<Boolean> removeBatchByRes_supplier(@RequestBody List<Long> ids) {
        res_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_partnerMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_partner-Get')")
    @ApiOperation(value = "根据供应商获取联系人", tags = {"联系人" },  notes = "根据供应商获取联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/res_partners/{res_partner_id}")
    public ResponseEntity<Res_partnerDTO> getByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("res_partner_id") Long res_partner_id) {
        Res_partner domain = res_partnerService.get(res_partner_id);
        Res_partnerDTO dto = res_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据供应商获取联系人草稿", tags = {"联系人" },  notes = "根据供应商获取联系人草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/res_partners/getdraft")
    public ResponseEntity<Res_partnerDTO> getDraftByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id) {
        Res_partner domain = new Res_partner();
        domain.setParentId(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_partnerMapping.toDto(res_partnerService.getDraft(domain)));
    }

    @ApiOperation(value = "根据供应商检查联系人", tags = {"联系人" },  notes = "根据供应商检查联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partners/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_partnerDTO res_partnerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partnerService.checkKey(res_partnerMapping.toDomain(res_partnerdto)));
    }

    @PreAuthorize("hasPermission(this.res_partnerMapping.toDomain(#res_partnerdto),'iBizBusinessCentral-Res_partner-Save')")
    @ApiOperation(value = "根据供应商保存联系人", tags = {"联系人" },  notes = "根据供应商保存联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partners/save")
    public ResponseEntity<Boolean> saveByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_partnerDTO res_partnerdto) {
        Res_partner domain = res_partnerMapping.toDomain(res_partnerdto);
        domain.setParentId(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(res_partnerService.save(domain));
    }

    @PreAuthorize("hasPermission(this.res_partnerMapping.toDomain(#res_partnerdtos),'iBizBusinessCentral-Res_partner-Save')")
    @ApiOperation(value = "根据供应商批量保存联系人", tags = {"联系人" },  notes = "根据供应商批量保存联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/res_partners/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Res_partnerDTO> res_partnerdtos) {
        List<Res_partner> domainlist=res_partnerMapping.toDomain(res_partnerdtos);
        for(Res_partner domain:domainlist){
             domain.setParentId(res_supplier_id);
        }
        res_partnerService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner-Get')")
	@ApiOperation(value = "根据供应商获取数据集", tags = {"联系人" } ,notes = "根据供应商获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/res_partners/fetchdefault")
	public ResponseEntity<List<Res_partnerDTO>> fetchRes_partnerDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id,Res_partnerSearchContext context) {
        context.setN_parent_id_eq(res_supplier_id);
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
        List<Res_partnerDTO> list = res_partnerMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner-Get')")
	@ApiOperation(value = "根据供应商查询数据集", tags = {"联系人" } ,notes = "根据供应商查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/res_partners/searchdefault")
	public ResponseEntity<Page<Res_partnerDTO>> searchRes_partnerDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Res_partnerSearchContext context) {
        context.setN_parent_id_eq(res_supplier_id);
        Page<Res_partner> domains = res_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


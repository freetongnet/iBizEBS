package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.businesscentral.core.dto.Survey_user_input_lineDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreSurvey_user_input_lineMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Survey_user_input_lineMapping extends MappingBase<Survey_user_input_lineDTO, Survey_user_input_line> {


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_pickingSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"调拨" })
@RestController("Core-stock_picking")
@RequestMapping("")
public class Stock_pickingResource {

    @Autowired
    public IStock_pickingService stock_pickingService;

    @Autowired
    @Lazy
    public Stock_pickingMapping stock_pickingMapping;

    @PreAuthorize("hasPermission(this.stock_pickingMapping.toDomain(#stock_pickingdto),'iBizBusinessCentral-Stock_picking-Create')")
    @ApiOperation(value = "新建调拨", tags = {"调拨" },  notes = "新建调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_pickings")
    public ResponseEntity<Stock_pickingDTO> create(@Validated @RequestBody Stock_pickingDTO stock_pickingdto) {
        Stock_picking domain = stock_pickingMapping.toDomain(stock_pickingdto);
		stock_pickingService.create(domain);
        Stock_pickingDTO dto = stock_pickingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_pickingMapping.toDomain(#stock_pickingdtos),'iBizBusinessCentral-Stock_picking-Create')")
    @ApiOperation(value = "批量新建调拨", tags = {"调拨" },  notes = "批量新建调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_pickings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_pickingDTO> stock_pickingdtos) {
        stock_pickingService.createBatch(stock_pickingMapping.toDomain(stock_pickingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_picking" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_pickingService.get(#stock_picking_id),'iBizBusinessCentral-Stock_picking-Update')")
    @ApiOperation(value = "更新调拨", tags = {"调拨" },  notes = "更新调拨")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_pickings/{stock_picking_id}")
    public ResponseEntity<Stock_pickingDTO> update(@PathVariable("stock_picking_id") Long stock_picking_id, @RequestBody Stock_pickingDTO stock_pickingdto) {
		Stock_picking domain  = stock_pickingMapping.toDomain(stock_pickingdto);
        domain .setId(stock_picking_id);
		stock_pickingService.update(domain );
		Stock_pickingDTO dto = stock_pickingMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_pickingService.getStockPickingByEntities(this.stock_pickingMapping.toDomain(#stock_pickingdtos)),'iBizBusinessCentral-Stock_picking-Update')")
    @ApiOperation(value = "批量更新调拨", tags = {"调拨" },  notes = "批量更新调拨")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_pickings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_pickingDTO> stock_pickingdtos) {
        stock_pickingService.updateBatch(stock_pickingMapping.toDomain(stock_pickingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_pickingService.get(#stock_picking_id),'iBizBusinessCentral-Stock_picking-Remove')")
    @ApiOperation(value = "删除调拨", tags = {"调拨" },  notes = "删除调拨")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_pickings/{stock_picking_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_picking_id") Long stock_picking_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_pickingService.remove(stock_picking_id));
    }

    @PreAuthorize("hasPermission(this.stock_pickingService.getStockPickingByIds(#ids),'iBizBusinessCentral-Stock_picking-Remove')")
    @ApiOperation(value = "批量删除调拨", tags = {"调拨" },  notes = "批量删除调拨")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_pickings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_pickingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_pickingMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_picking-Get')")
    @ApiOperation(value = "获取调拨", tags = {"调拨" },  notes = "获取调拨")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_pickings/{stock_picking_id}")
    public ResponseEntity<Stock_pickingDTO> get(@PathVariable("stock_picking_id") Long stock_picking_id) {
        Stock_picking domain = stock_pickingService.get(stock_picking_id);
        Stock_pickingDTO dto = stock_pickingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取调拨草稿", tags = {"调拨" },  notes = "获取调拨草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_pickings/getdraft")
    public ResponseEntity<Stock_pickingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_pickingMapping.toDto(stock_pickingService.getDraft(new Stock_picking())));
    }

    @ApiOperation(value = "检查调拨", tags = {"调拨" },  notes = "检查调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_pickings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_pickingDTO stock_pickingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_pickingService.checkKey(stock_pickingMapping.toDomain(stock_pickingdto)));
    }

    @PreAuthorize("hasPermission(this.stock_pickingMapping.toDomain(#stock_pickingdto),'iBizBusinessCentral-Stock_picking-Save')")
    @ApiOperation(value = "保存调拨", tags = {"调拨" },  notes = "保存调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_pickings/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_pickingDTO stock_pickingdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_pickingService.save(stock_pickingMapping.toDomain(stock_pickingdto)));
    }

    @PreAuthorize("hasPermission(this.stock_pickingMapping.toDomain(#stock_pickingdtos),'iBizBusinessCentral-Stock_picking-Save')")
    @ApiOperation(value = "批量保存调拨", tags = {"调拨" },  notes = "批量保存调拨")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_pickings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_pickingDTO> stock_pickingdtos) {
        stock_pickingService.saveBatch(stock_pickingMapping.toDomain(stock_pickingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_picking-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_picking-Get')")
	@ApiOperation(value = "获取数据集", tags = {"调拨" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_pickings/fetchdefault")
	public ResponseEntity<List<Stock_pickingDTO>> fetchDefault(Stock_pickingSearchContext context) {
        Page<Stock_picking> domains = stock_pickingService.searchDefault(context) ;
        List<Stock_pickingDTO> list = stock_pickingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_picking-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_picking-Get')")
	@ApiOperation(value = "查询数据集", tags = {"调拨" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_pickings/searchdefault")
	public ResponseEntity<Page<Stock_pickingDTO>> searchDefault(@RequestBody Stock_pickingSearchContext context) {
        Page<Stock_picking> domains = stock_pickingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_pickingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


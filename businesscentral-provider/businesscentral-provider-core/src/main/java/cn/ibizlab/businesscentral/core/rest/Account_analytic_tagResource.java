package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_tag;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_tagService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_tagSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"分析标签" })
@RestController("Core-account_analytic_tag")
@RequestMapping("")
public class Account_analytic_tagResource {

    @Autowired
    public IAccount_analytic_tagService account_analytic_tagService;

    @Autowired
    @Lazy
    public Account_analytic_tagMapping account_analytic_tagMapping;

    @PreAuthorize("hasPermission(this.account_analytic_tagMapping.toDomain(#account_analytic_tagdto),'iBizBusinessCentral-Account_analytic_tag-Create')")
    @ApiOperation(value = "新建分析标签", tags = {"分析标签" },  notes = "新建分析标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_tags")
    public ResponseEntity<Account_analytic_tagDTO> create(@Validated @RequestBody Account_analytic_tagDTO account_analytic_tagdto) {
        Account_analytic_tag domain = account_analytic_tagMapping.toDomain(account_analytic_tagdto);
		account_analytic_tagService.create(domain);
        Account_analytic_tagDTO dto = account_analytic_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_tagMapping.toDomain(#account_analytic_tagdtos),'iBizBusinessCentral-Account_analytic_tag-Create')")
    @ApiOperation(value = "批量新建分析标签", tags = {"分析标签" },  notes = "批量新建分析标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_analytic_tagDTO> account_analytic_tagdtos) {
        account_analytic_tagService.createBatch(account_analytic_tagMapping.toDomain(account_analytic_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_analytic_tag" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_analytic_tagService.get(#account_analytic_tag_id),'iBizBusinessCentral-Account_analytic_tag-Update')")
    @ApiOperation(value = "更新分析标签", tags = {"分析标签" },  notes = "更新分析标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_tags/{account_analytic_tag_id}")
    public ResponseEntity<Account_analytic_tagDTO> update(@PathVariable("account_analytic_tag_id") Long account_analytic_tag_id, @RequestBody Account_analytic_tagDTO account_analytic_tagdto) {
		Account_analytic_tag domain  = account_analytic_tagMapping.toDomain(account_analytic_tagdto);
        domain .setId(account_analytic_tag_id);
		account_analytic_tagService.update(domain );
		Account_analytic_tagDTO dto = account_analytic_tagMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_analytic_tagService.getAccountAnalyticTagByEntities(this.account_analytic_tagMapping.toDomain(#account_analytic_tagdtos)),'iBizBusinessCentral-Account_analytic_tag-Update')")
    @ApiOperation(value = "批量更新分析标签", tags = {"分析标签" },  notes = "批量更新分析标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_tagDTO> account_analytic_tagdtos) {
        account_analytic_tagService.updateBatch(account_analytic_tagMapping.toDomain(account_analytic_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_analytic_tagService.get(#account_analytic_tag_id),'iBizBusinessCentral-Account_analytic_tag-Remove')")
    @ApiOperation(value = "删除分析标签", tags = {"分析标签" },  notes = "删除分析标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_tags/{account_analytic_tag_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_tag_id") Long account_analytic_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_analytic_tagService.remove(account_analytic_tag_id));
    }

    @PreAuthorize("hasPermission(this.account_analytic_tagService.getAccountAnalyticTagByIds(#ids),'iBizBusinessCentral-Account_analytic_tag-Remove')")
    @ApiOperation(value = "批量删除分析标签", tags = {"分析标签" },  notes = "批量删除分析标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_analytic_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_analytic_tagMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_analytic_tag-Get')")
    @ApiOperation(value = "获取分析标签", tags = {"分析标签" },  notes = "获取分析标签")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_tags/{account_analytic_tag_id}")
    public ResponseEntity<Account_analytic_tagDTO> get(@PathVariable("account_analytic_tag_id") Long account_analytic_tag_id) {
        Account_analytic_tag domain = account_analytic_tagService.get(account_analytic_tag_id);
        Account_analytic_tagDTO dto = account_analytic_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取分析标签草稿", tags = {"分析标签" },  notes = "获取分析标签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_tags/getdraft")
    public ResponseEntity<Account_analytic_tagDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_tagMapping.toDto(account_analytic_tagService.getDraft(new Account_analytic_tag())));
    }

    @ApiOperation(value = "检查分析标签", tags = {"分析标签" },  notes = "检查分析标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_tags/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_analytic_tagDTO account_analytic_tagdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_analytic_tagService.checkKey(account_analytic_tagMapping.toDomain(account_analytic_tagdto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_tagMapping.toDomain(#account_analytic_tagdto),'iBizBusinessCentral-Account_analytic_tag-Save')")
    @ApiOperation(value = "保存分析标签", tags = {"分析标签" },  notes = "保存分析标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_tags/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_analytic_tagDTO account_analytic_tagdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_analytic_tagService.save(account_analytic_tagMapping.toDomain(account_analytic_tagdto)));
    }

    @PreAuthorize("hasPermission(this.account_analytic_tagMapping.toDomain(#account_analytic_tagdtos),'iBizBusinessCentral-Account_analytic_tag-Save')")
    @ApiOperation(value = "批量保存分析标签", tags = {"分析标签" },  notes = "批量保存分析标签")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_tags/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_analytic_tagDTO> account_analytic_tagdtos) {
        account_analytic_tagService.saveBatch(account_analytic_tagMapping.toDomain(account_analytic_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_tag-Get')")
	@ApiOperation(value = "获取数据集", tags = {"分析标签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_tags/fetchdefault")
	public ResponseEntity<List<Account_analytic_tagDTO>> fetchDefault(Account_analytic_tagSearchContext context) {
        Page<Account_analytic_tag> domains = account_analytic_tagService.searchDefault(context) ;
        List<Account_analytic_tagDTO> list = account_analytic_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_analytic_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_analytic_tag-Get')")
	@ApiOperation(value = "查询数据集", tags = {"分析标签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_analytic_tags/searchdefault")
	public ResponseEntity<Page<Account_analytic_tagDTO>> searchDefault(@RequestBody Account_analytic_tagSearchContext context) {
        Page<Account_analytic_tag> domains = account_analytic_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_analytic_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


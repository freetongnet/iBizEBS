package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mailService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mailSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"寄出邮件" })
@RestController("Core-mail_mail")
@RequestMapping("")
public class Mail_mailResource {

    @Autowired
    public IMail_mailService mail_mailService;

    @Autowired
    @Lazy
    public Mail_mailMapping mail_mailMapping;

    @PreAuthorize("hasPermission(this.mail_mailMapping.toDomain(#mail_maildto),'iBizBusinessCentral-Mail_mail-Create')")
    @ApiOperation(value = "新建寄出邮件", tags = {"寄出邮件" },  notes = "新建寄出邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mails")
    public ResponseEntity<Mail_mailDTO> create(@Validated @RequestBody Mail_mailDTO mail_maildto) {
        Mail_mail domain = mail_mailMapping.toDomain(mail_maildto);
		mail_mailService.create(domain);
        Mail_mailDTO dto = mail_mailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mailMapping.toDomain(#mail_maildtos),'iBizBusinessCentral-Mail_mail-Create')")
    @ApiOperation(value = "批量新建寄出邮件", tags = {"寄出邮件" },  notes = "批量新建寄出邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mailDTO> mail_maildtos) {
        mail_mailService.createBatch(mail_mailMapping.toDomain(mail_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_mail" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_mailService.get(#mail_mail_id),'iBizBusinessCentral-Mail_mail-Update')")
    @ApiOperation(value = "更新寄出邮件", tags = {"寄出邮件" },  notes = "更新寄出邮件")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mails/{mail_mail_id}")
    public ResponseEntity<Mail_mailDTO> update(@PathVariable("mail_mail_id") Long mail_mail_id, @RequestBody Mail_mailDTO mail_maildto) {
		Mail_mail domain  = mail_mailMapping.toDomain(mail_maildto);
        domain .setId(mail_mail_id);
		mail_mailService.update(domain );
		Mail_mailDTO dto = mail_mailMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mailService.getMailMailByEntities(this.mail_mailMapping.toDomain(#mail_maildtos)),'iBizBusinessCentral-Mail_mail-Update')")
    @ApiOperation(value = "批量更新寄出邮件", tags = {"寄出邮件" },  notes = "批量更新寄出邮件")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mailDTO> mail_maildtos) {
        mail_mailService.updateBatch(mail_mailMapping.toDomain(mail_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_mailService.get(#mail_mail_id),'iBizBusinessCentral-Mail_mail-Remove')")
    @ApiOperation(value = "删除寄出邮件", tags = {"寄出邮件" },  notes = "删除寄出邮件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mails/{mail_mail_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_mail_id") Long mail_mail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mailService.remove(mail_mail_id));
    }

    @PreAuthorize("hasPermission(this.mail_mailService.getMailMailByIds(#ids),'iBizBusinessCentral-Mail_mail-Remove')")
    @ApiOperation(value = "批量删除寄出邮件", tags = {"寄出邮件" },  notes = "批量删除寄出邮件")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_mailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_mailMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_mail-Get')")
    @ApiOperation(value = "获取寄出邮件", tags = {"寄出邮件" },  notes = "获取寄出邮件")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mails/{mail_mail_id}")
    public ResponseEntity<Mail_mailDTO> get(@PathVariable("mail_mail_id") Long mail_mail_id) {
        Mail_mail domain = mail_mailService.get(mail_mail_id);
        Mail_mailDTO dto = mail_mailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取寄出邮件草稿", tags = {"寄出邮件" },  notes = "获取寄出邮件草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mails/getdraft")
    public ResponseEntity<Mail_mailDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mailMapping.toDto(mail_mailService.getDraft(new Mail_mail())));
    }

    @ApiOperation(value = "检查寄出邮件", tags = {"寄出邮件" },  notes = "检查寄出邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mails/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_mailDTO mail_maildto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_mailService.checkKey(mail_mailMapping.toDomain(mail_maildto)));
    }

    @PreAuthorize("hasPermission(this.mail_mailMapping.toDomain(#mail_maildto),'iBizBusinessCentral-Mail_mail-Save')")
    @ApiOperation(value = "保存寄出邮件", tags = {"寄出邮件" },  notes = "保存寄出邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mails/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_mailDTO mail_maildto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mailService.save(mail_mailMapping.toDomain(mail_maildto)));
    }

    @PreAuthorize("hasPermission(this.mail_mailMapping.toDomain(#mail_maildtos),'iBizBusinessCentral-Mail_mail-Save')")
    @ApiOperation(value = "批量保存寄出邮件", tags = {"寄出邮件" },  notes = "批量保存寄出邮件")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mails/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_mailDTO> mail_maildtos) {
        mail_mailService.saveBatch(mail_mailMapping.toDomain(mail_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mail-Get')")
	@ApiOperation(value = "获取数据集", tags = {"寄出邮件" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mails/fetchdefault")
	public ResponseEntity<List<Mail_mailDTO>> fetchDefault(Mail_mailSearchContext context) {
        Page<Mail_mail> domains = mail_mailService.searchDefault(context) ;
        List<Mail_mailDTO> list = mail_mailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mail-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mail-Get')")
	@ApiOperation(value = "查询数据集", tags = {"寄出邮件" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_mails/searchdefault")
	public ResponseEntity<Page<Mail_mailDTO>> searchDefault(@RequestBody Mail_mailSearchContext context) {
        Page<Mail_mail> domains = mail_mailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


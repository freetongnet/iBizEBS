package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_import_wizard;
import cn.ibizlab.businesscentral.core.dto.Account_invoice_import_wizardDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_invoice_import_wizardMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_invoice_import_wizardMapping extends MappingBase<Account_invoice_import_wizardDTO, Account_invoice_import_wizard> {


}


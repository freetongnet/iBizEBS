package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goal_definitionService;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"游戏化目标定义" })
@RestController("Core-gamification_goal_definition")
@RequestMapping("")
public class Gamification_goal_definitionResource {

    @Autowired
    public IGamification_goal_definitionService gamification_goal_definitionService;

    @Autowired
    @Lazy
    public Gamification_goal_definitionMapping gamification_goal_definitionMapping;

    @PreAuthorize("hasPermission(this.gamification_goal_definitionMapping.toDomain(#gamification_goal_definitiondto),'iBizBusinessCentral-Gamification_goal_definition-Create')")
    @ApiOperation(value = "新建游戏化目标定义", tags = {"游戏化目标定义" },  notes = "新建游戏化目标定义")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions")
    public ResponseEntity<Gamification_goal_definitionDTO> create(@Validated @RequestBody Gamification_goal_definitionDTO gamification_goal_definitiondto) {
        Gamification_goal_definition domain = gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondto);
		gamification_goal_definitionService.create(domain);
        Gamification_goal_definitionDTO dto = gamification_goal_definitionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_goal_definitionMapping.toDomain(#gamification_goal_definitiondtos),'iBizBusinessCentral-Gamification_goal_definition-Create')")
    @ApiOperation(value = "批量新建游戏化目标定义", tags = {"游戏化目标定义" },  notes = "批量新建游戏化目标定义")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_goal_definitionDTO> gamification_goal_definitiondtos) {
        gamification_goal_definitionService.createBatch(gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "gamification_goal_definition" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.gamification_goal_definitionService.get(#gamification_goal_definition_id),'iBizBusinessCentral-Gamification_goal_definition-Update')")
    @ApiOperation(value = "更新游戏化目标定义", tags = {"游戏化目标定义" },  notes = "更新游戏化目标定义")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_definitions/{gamification_goal_definition_id}")
    public ResponseEntity<Gamification_goal_definitionDTO> update(@PathVariable("gamification_goal_definition_id") Long gamification_goal_definition_id, @RequestBody Gamification_goal_definitionDTO gamification_goal_definitiondto) {
		Gamification_goal_definition domain  = gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondto);
        domain .setId(gamification_goal_definition_id);
		gamification_goal_definitionService.update(domain );
		Gamification_goal_definitionDTO dto = gamification_goal_definitionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.gamification_goal_definitionService.getGamificationGoalDefinitionByEntities(this.gamification_goal_definitionMapping.toDomain(#gamification_goal_definitiondtos)),'iBizBusinessCentral-Gamification_goal_definition-Update')")
    @ApiOperation(value = "批量更新游戏化目标定义", tags = {"游戏化目标定义" },  notes = "批量更新游戏化目标定义")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_definitions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_goal_definitionDTO> gamification_goal_definitiondtos) {
        gamification_goal_definitionService.updateBatch(gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.gamification_goal_definitionService.get(#gamification_goal_definition_id),'iBizBusinessCentral-Gamification_goal_definition-Remove')")
    @ApiOperation(value = "删除游戏化目标定义", tags = {"游戏化目标定义" },  notes = "删除游戏化目标定义")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_definitions/{gamification_goal_definition_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("gamification_goal_definition_id") Long gamification_goal_definition_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_goal_definitionService.remove(gamification_goal_definition_id));
    }

    @PreAuthorize("hasPermission(this.gamification_goal_definitionService.getGamificationGoalDefinitionByIds(#ids),'iBizBusinessCentral-Gamification_goal_definition-Remove')")
    @ApiOperation(value = "批量删除游戏化目标定义", tags = {"游戏化目标定义" },  notes = "批量删除游戏化目标定义")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_definitions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        gamification_goal_definitionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.gamification_goal_definitionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Gamification_goal_definition-Get')")
    @ApiOperation(value = "获取游戏化目标定义", tags = {"游戏化目标定义" },  notes = "获取游戏化目标定义")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_definitions/{gamification_goal_definition_id}")
    public ResponseEntity<Gamification_goal_definitionDTO> get(@PathVariable("gamification_goal_definition_id") Long gamification_goal_definition_id) {
        Gamification_goal_definition domain = gamification_goal_definitionService.get(gamification_goal_definition_id);
        Gamification_goal_definitionDTO dto = gamification_goal_definitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取游戏化目标定义草稿", tags = {"游戏化目标定义" },  notes = "获取游戏化目标定义草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_definitions/getdraft")
    public ResponseEntity<Gamification_goal_definitionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_goal_definitionMapping.toDto(gamification_goal_definitionService.getDraft(new Gamification_goal_definition())));
    }

    @ApiOperation(value = "检查游戏化目标定义", tags = {"游戏化目标定义" },  notes = "检查游戏化目标定义")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Gamification_goal_definitionDTO gamification_goal_definitiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(gamification_goal_definitionService.checkKey(gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondto)));
    }

    @PreAuthorize("hasPermission(this.gamification_goal_definitionMapping.toDomain(#gamification_goal_definitiondto),'iBizBusinessCentral-Gamification_goal_definition-Save')")
    @ApiOperation(value = "保存游戏化目标定义", tags = {"游戏化目标定义" },  notes = "保存游戏化目标定义")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/save")
    public ResponseEntity<Boolean> save(@RequestBody Gamification_goal_definitionDTO gamification_goal_definitiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(gamification_goal_definitionService.save(gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondto)));
    }

    @PreAuthorize("hasPermission(this.gamification_goal_definitionMapping.toDomain(#gamification_goal_definitiondtos),'iBizBusinessCentral-Gamification_goal_definition-Save')")
    @ApiOperation(value = "批量保存游戏化目标定义", tags = {"游戏化目标定义" },  notes = "批量保存游戏化目标定义")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Gamification_goal_definitionDTO> gamification_goal_definitiondtos) {
        gamification_goal_definitionService.saveBatch(gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_goal_definition-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_goal_definition-Get')")
	@ApiOperation(value = "获取数据集", tags = {"游戏化目标定义" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_goal_definitions/fetchdefault")
	public ResponseEntity<List<Gamification_goal_definitionDTO>> fetchDefault(Gamification_goal_definitionSearchContext context) {
        Page<Gamification_goal_definition> domains = gamification_goal_definitionService.searchDefault(context) ;
        List<Gamification_goal_definitionDTO> list = gamification_goal_definitionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Gamification_goal_definition-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Gamification_goal_definition-Get')")
	@ApiOperation(value = "查询数据集", tags = {"游戏化目标定义" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/gamification_goal_definitions/searchdefault")
	public ResponseEntity<Page<Gamification_goal_definitionDTO>> searchDefault(@RequestBody Gamification_goal_definitionSearchContext context) {
        Page<Gamification_goal_definition> domains = gamification_goal_definitionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_goal_definitionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


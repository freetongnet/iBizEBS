package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term_line;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_term_lineService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_payment_term_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"付款条款行" })
@RestController("Core-account_payment_term_line")
@RequestMapping("")
public class Account_payment_term_lineResource {

    @Autowired
    public IAccount_payment_term_lineService account_payment_term_lineService;

    @Autowired
    @Lazy
    public Account_payment_term_lineMapping account_payment_term_lineMapping;

    @PreAuthorize("hasPermission(this.account_payment_term_lineMapping.toDomain(#account_payment_term_linedto),'iBizBusinessCentral-Account_payment_term_line-Create')")
    @ApiOperation(value = "新建付款条款行", tags = {"付款条款行" },  notes = "新建付款条款行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines")
    public ResponseEntity<Account_payment_term_lineDTO> create(@Validated @RequestBody Account_payment_term_lineDTO account_payment_term_linedto) {
        Account_payment_term_line domain = account_payment_term_lineMapping.toDomain(account_payment_term_linedto);
		account_payment_term_lineService.create(domain);
        Account_payment_term_lineDTO dto = account_payment_term_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_payment_term_lineMapping.toDomain(#account_payment_term_linedtos),'iBizBusinessCentral-Account_payment_term_line-Create')")
    @ApiOperation(value = "批量新建付款条款行", tags = {"付款条款行" },  notes = "批量新建付款条款行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_payment_term_lineDTO> account_payment_term_linedtos) {
        account_payment_term_lineService.createBatch(account_payment_term_lineMapping.toDomain(account_payment_term_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_payment_term_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_payment_term_lineService.get(#account_payment_term_line_id),'iBizBusinessCentral-Account_payment_term_line-Update')")
    @ApiOperation(value = "更新付款条款行", tags = {"付款条款行" },  notes = "更新付款条款行")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_term_lines/{account_payment_term_line_id}")
    public ResponseEntity<Account_payment_term_lineDTO> update(@PathVariable("account_payment_term_line_id") Long account_payment_term_line_id, @RequestBody Account_payment_term_lineDTO account_payment_term_linedto) {
		Account_payment_term_line domain  = account_payment_term_lineMapping.toDomain(account_payment_term_linedto);
        domain .setId(account_payment_term_line_id);
		account_payment_term_lineService.update(domain );
		Account_payment_term_lineDTO dto = account_payment_term_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_payment_term_lineService.getAccountPaymentTermLineByEntities(this.account_payment_term_lineMapping.toDomain(#account_payment_term_linedtos)),'iBizBusinessCentral-Account_payment_term_line-Update')")
    @ApiOperation(value = "批量更新付款条款行", tags = {"付款条款行" },  notes = "批量更新付款条款行")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_term_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_payment_term_lineDTO> account_payment_term_linedtos) {
        account_payment_term_lineService.updateBatch(account_payment_term_lineMapping.toDomain(account_payment_term_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_payment_term_lineService.get(#account_payment_term_line_id),'iBizBusinessCentral-Account_payment_term_line-Remove')")
    @ApiOperation(value = "删除付款条款行", tags = {"付款条款行" },  notes = "删除付款条款行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_term_lines/{account_payment_term_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_term_line_id") Long account_payment_term_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_payment_term_lineService.remove(account_payment_term_line_id));
    }

    @PreAuthorize("hasPermission(this.account_payment_term_lineService.getAccountPaymentTermLineByIds(#ids),'iBizBusinessCentral-Account_payment_term_line-Remove')")
    @ApiOperation(value = "批量删除付款条款行", tags = {"付款条款行" },  notes = "批量删除付款条款行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_term_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_payment_term_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_payment_term_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_payment_term_line-Get')")
    @ApiOperation(value = "获取付款条款行", tags = {"付款条款行" },  notes = "获取付款条款行")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_term_lines/{account_payment_term_line_id}")
    public ResponseEntity<Account_payment_term_lineDTO> get(@PathVariable("account_payment_term_line_id") Long account_payment_term_line_id) {
        Account_payment_term_line domain = account_payment_term_lineService.get(account_payment_term_line_id);
        Account_payment_term_lineDTO dto = account_payment_term_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取付款条款行草稿", tags = {"付款条款行" },  notes = "获取付款条款行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_term_lines/getdraft")
    public ResponseEntity<Account_payment_term_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_payment_term_lineMapping.toDto(account_payment_term_lineService.getDraft(new Account_payment_term_line())));
    }

    @ApiOperation(value = "检查付款条款行", tags = {"付款条款行" },  notes = "检查付款条款行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_payment_term_lineDTO account_payment_term_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_payment_term_lineService.checkKey(account_payment_term_lineMapping.toDomain(account_payment_term_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_payment_term_lineMapping.toDomain(#account_payment_term_linedto),'iBizBusinessCentral-Account_payment_term_line-Save')")
    @ApiOperation(value = "保存付款条款行", tags = {"付款条款行" },  notes = "保存付款条款行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_payment_term_lineDTO account_payment_term_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_payment_term_lineService.save(account_payment_term_lineMapping.toDomain(account_payment_term_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_payment_term_lineMapping.toDomain(#account_payment_term_linedtos),'iBizBusinessCentral-Account_payment_term_line-Save')")
    @ApiOperation(value = "批量保存付款条款行", tags = {"付款条款行" },  notes = "批量保存付款条款行")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_term_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_payment_term_lineDTO> account_payment_term_linedtos) {
        account_payment_term_lineService.saveBatch(account_payment_term_lineMapping.toDomain(account_payment_term_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_payment_term_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_payment_term_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"付款条款行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_payment_term_lines/fetchdefault")
	public ResponseEntity<List<Account_payment_term_lineDTO>> fetchDefault(Account_payment_term_lineSearchContext context) {
        Page<Account_payment_term_line> domains = account_payment_term_lineService.searchDefault(context) ;
        List<Account_payment_term_lineDTO> list = account_payment_term_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_payment_term_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_payment_term_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"付款条款行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_payment_term_lines/searchdefault")
	public ResponseEntity<Page<Account_payment_term_lineDTO>> searchDefault(@RequestBody Account_payment_term_lineSearchContext context) {
        Page<Account_payment_term_line> domains = account_payment_term_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_payment_term_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


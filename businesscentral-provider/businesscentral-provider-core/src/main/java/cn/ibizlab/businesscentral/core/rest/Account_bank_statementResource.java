package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statementService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statementSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"银行对账单" })
@RestController("Core-account_bank_statement")
@RequestMapping("")
public class Account_bank_statementResource {

    @Autowired
    public IAccount_bank_statementService account_bank_statementService;

    @Autowired
    @Lazy
    public Account_bank_statementMapping account_bank_statementMapping;

    @PreAuthorize("hasPermission(this.account_bank_statementMapping.toDomain(#account_bank_statementdto),'iBizBusinessCentral-Account_bank_statement-Create')")
    @ApiOperation(value = "新建银行对账单", tags = {"银行对账单" },  notes = "新建银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements")
    public ResponseEntity<Account_bank_statementDTO> create(@Validated @RequestBody Account_bank_statementDTO account_bank_statementdto) {
        Account_bank_statement domain = account_bank_statementMapping.toDomain(account_bank_statementdto);
		account_bank_statementService.create(domain);
        Account_bank_statementDTO dto = account_bank_statementMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statementMapping.toDomain(#account_bank_statementdtos),'iBizBusinessCentral-Account_bank_statement-Create')")
    @ApiOperation(value = "批量新建银行对账单", tags = {"银行对账单" },  notes = "批量新建银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statementDTO> account_bank_statementdtos) {
        account_bank_statementService.createBatch(account_bank_statementMapping.toDomain(account_bank_statementdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_bank_statement" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_bank_statementService.get(#account_bank_statement_id),'iBizBusinessCentral-Account_bank_statement-Update')")
    @ApiOperation(value = "更新银行对账单", tags = {"银行对账单" },  notes = "更新银行对账单")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statements/{account_bank_statement_id}")
    public ResponseEntity<Account_bank_statementDTO> update(@PathVariable("account_bank_statement_id") Long account_bank_statement_id, @RequestBody Account_bank_statementDTO account_bank_statementdto) {
		Account_bank_statement domain  = account_bank_statementMapping.toDomain(account_bank_statementdto);
        domain .setId(account_bank_statement_id);
		account_bank_statementService.update(domain );
		Account_bank_statementDTO dto = account_bank_statementMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_bank_statementService.getAccountBankStatementByEntities(this.account_bank_statementMapping.toDomain(#account_bank_statementdtos)),'iBizBusinessCentral-Account_bank_statement-Update')")
    @ApiOperation(value = "批量更新银行对账单", tags = {"银行对账单" },  notes = "批量更新银行对账单")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statements/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statementDTO> account_bank_statementdtos) {
        account_bank_statementService.updateBatch(account_bank_statementMapping.toDomain(account_bank_statementdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_bank_statementService.get(#account_bank_statement_id),'iBizBusinessCentral-Account_bank_statement-Remove')")
    @ApiOperation(value = "删除银行对账单", tags = {"银行对账单" },  notes = "删除银行对账单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statements/{account_bank_statement_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_id") Long account_bank_statement_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statementService.remove(account_bank_statement_id));
    }

    @PreAuthorize("hasPermission(this.account_bank_statementService.getAccountBankStatementByIds(#ids),'iBizBusinessCentral-Account_bank_statement-Remove')")
    @ApiOperation(value = "批量删除银行对账单", tags = {"银行对账单" },  notes = "批量删除银行对账单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statements/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_bank_statementService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_bank_statementMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_bank_statement-Get')")
    @ApiOperation(value = "获取银行对账单", tags = {"银行对账单" },  notes = "获取银行对账单")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statements/{account_bank_statement_id}")
    public ResponseEntity<Account_bank_statementDTO> get(@PathVariable("account_bank_statement_id") Long account_bank_statement_id) {
        Account_bank_statement domain = account_bank_statementService.get(account_bank_statement_id);
        Account_bank_statementDTO dto = account_bank_statementMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取银行对账单草稿", tags = {"银行对账单" },  notes = "获取银行对账单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statements/getdraft")
    public ResponseEntity<Account_bank_statementDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statementMapping.toDto(account_bank_statementService.getDraft(new Account_bank_statement())));
    }

    @ApiOperation(value = "检查银行对账单", tags = {"银行对账单" },  notes = "检查银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_bank_statementDTO account_bank_statementdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_bank_statementService.checkKey(account_bank_statementMapping.toDomain(account_bank_statementdto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statementMapping.toDomain(#account_bank_statementdto),'iBizBusinessCentral-Account_bank_statement-Save')")
    @ApiOperation(value = "保存银行对账单", tags = {"银行对账单" },  notes = "保存银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_bank_statementDTO account_bank_statementdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_bank_statementService.save(account_bank_statementMapping.toDomain(account_bank_statementdto)));
    }

    @PreAuthorize("hasPermission(this.account_bank_statementMapping.toDomain(#account_bank_statementdtos),'iBizBusinessCentral-Account_bank_statement-Save')")
    @ApiOperation(value = "批量保存银行对账单", tags = {"银行对账单" },  notes = "批量保存银行对账单")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statements/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_bank_statementDTO> account_bank_statementdtos) {
        account_bank_statementService.saveBatch(account_bank_statementMapping.toDomain(account_bank_statementdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement-Get')")
	@ApiOperation(value = "获取数据集", tags = {"银行对账单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statements/fetchdefault")
	public ResponseEntity<List<Account_bank_statementDTO>> fetchDefault(Account_bank_statementSearchContext context) {
        Page<Account_bank_statement> domains = account_bank_statementService.searchDefault(context) ;
        List<Account_bank_statementDTO> list = account_bank_statementMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_bank_statement-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_bank_statement-Get')")
	@ApiOperation(value = "查询数据集", tags = {"银行对账单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_bank_statements/searchdefault")
	public ResponseEntity<Page<Account_bank_statementDTO>> searchDefault(@RequestBody Account_bank_statementSearchContext context) {
        Page<Account_bank_statement> domains = account_bank_statementService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statementMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


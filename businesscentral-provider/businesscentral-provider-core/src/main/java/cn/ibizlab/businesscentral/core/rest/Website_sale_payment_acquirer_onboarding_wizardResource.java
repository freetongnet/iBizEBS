package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_sale_payment_acquirer_onboarding_wizardService;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"website.sale.payment.acquirer.onboarding.wizard" })
@RestController("Core-website_sale_payment_acquirer_onboarding_wizard")
@RequestMapping("")
public class Website_sale_payment_acquirer_onboarding_wizardResource {

    @Autowired
    public IWebsite_sale_payment_acquirer_onboarding_wizardService website_sale_payment_acquirer_onboarding_wizardService;

    @Autowired
    @Lazy
    public Website_sale_payment_acquirer_onboarding_wizardMapping website_sale_payment_acquirer_onboarding_wizardMapping;

    @PreAuthorize("hasPermission(this.website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(#website_sale_payment_acquirer_onboarding_wizarddto),'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Create')")
    @ApiOperation(value = "新建website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "新建website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards")
    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> create(@Validated @RequestBody Website_sale_payment_acquirer_onboarding_wizardDTO website_sale_payment_acquirer_onboarding_wizarddto) {
        Website_sale_payment_acquirer_onboarding_wizard domain = website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddto);
		website_sale_payment_acquirer_onboarding_wizardService.create(domain);
        Website_sale_payment_acquirer_onboarding_wizardDTO dto = website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(#website_sale_payment_acquirer_onboarding_wizarddtos),'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Create')")
    @ApiOperation(value = "批量新建website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "批量新建website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizardDTO> website_sale_payment_acquirer_onboarding_wizarddtos) {
        website_sale_payment_acquirer_onboarding_wizardService.createBatch(website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "website_sale_payment_acquirer_onboarding_wizard" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.website_sale_payment_acquirer_onboarding_wizardService.get(#website_sale_payment_acquirer_onboarding_wizard_id),'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Update')")
    @ApiOperation(value = "更新website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "更新website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_sale_payment_acquirer_onboarding_wizards/{website_sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> update(@PathVariable("website_sale_payment_acquirer_onboarding_wizard_id") Long website_sale_payment_acquirer_onboarding_wizard_id, @RequestBody Website_sale_payment_acquirer_onboarding_wizardDTO website_sale_payment_acquirer_onboarding_wizarddto) {
		Website_sale_payment_acquirer_onboarding_wizard domain  = website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddto);
        domain .setId(website_sale_payment_acquirer_onboarding_wizard_id);
		website_sale_payment_acquirer_onboarding_wizardService.update(domain );
		Website_sale_payment_acquirer_onboarding_wizardDTO dto = website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.website_sale_payment_acquirer_onboarding_wizardService.getWebsiteSalePaymentAcquirerOnboardingWizardByEntities(this.website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(#website_sale_payment_acquirer_onboarding_wizarddtos)),'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Update')")
    @ApiOperation(value = "批量更新website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "批量更新website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizardDTO> website_sale_payment_acquirer_onboarding_wizarddtos) {
        website_sale_payment_acquirer_onboarding_wizardService.updateBatch(website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.website_sale_payment_acquirer_onboarding_wizardService.get(#website_sale_payment_acquirer_onboarding_wizard_id),'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Remove')")
    @ApiOperation(value = "删除website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "删除website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_sale_payment_acquirer_onboarding_wizards/{website_sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("website_sale_payment_acquirer_onboarding_wizard_id") Long website_sale_payment_acquirer_onboarding_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_sale_payment_acquirer_onboarding_wizardService.remove(website_sale_payment_acquirer_onboarding_wizard_id));
    }

    @PreAuthorize("hasPermission(this.website_sale_payment_acquirer_onboarding_wizardService.getWebsiteSalePaymentAcquirerOnboardingWizardByIds(#ids),'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Remove')")
    @ApiOperation(value = "批量删除website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "批量删除website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        website_sale_payment_acquirer_onboarding_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(returnObject.body),'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Get')")
    @ApiOperation(value = "获取website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "获取website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.GET, value = "/website_sale_payment_acquirer_onboarding_wizards/{website_sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> get(@PathVariable("website_sale_payment_acquirer_onboarding_wizard_id") Long website_sale_payment_acquirer_onboarding_wizard_id) {
        Website_sale_payment_acquirer_onboarding_wizard domain = website_sale_payment_acquirer_onboarding_wizardService.get(website_sale_payment_acquirer_onboarding_wizard_id);
        Website_sale_payment_acquirer_onboarding_wizardDTO dto = website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取website.sale.payment.acquirer.onboarding.wizard草稿", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "获取website.sale.payment.acquirer.onboarding.wizard草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/website_sale_payment_acquirer_onboarding_wizards/getdraft")
    public ResponseEntity<Website_sale_payment_acquirer_onboarding_wizardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(website_sale_payment_acquirer_onboarding_wizardMapping.toDto(website_sale_payment_acquirer_onboarding_wizardService.getDraft(new Website_sale_payment_acquirer_onboarding_wizard())));
    }

    @ApiOperation(value = "检查website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "检查website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Website_sale_payment_acquirer_onboarding_wizardDTO website_sale_payment_acquirer_onboarding_wizarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(website_sale_payment_acquirer_onboarding_wizardService.checkKey(website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(#website_sale_payment_acquirer_onboarding_wizarddto),'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Save')")
    @ApiOperation(value = "保存website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "保存website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/save")
    public ResponseEntity<Boolean> save(@RequestBody Website_sale_payment_acquirer_onboarding_wizardDTO website_sale_payment_acquirer_onboarding_wizarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(website_sale_payment_acquirer_onboarding_wizardService.save(website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(#website_sale_payment_acquirer_onboarding_wizarddtos),'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Save')")
    @ApiOperation(value = "批量保存website.sale.payment.acquirer.onboarding.wizard", tags = {"website.sale.payment.acquirer.onboarding.wizard" },  notes = "批量保存website.sale.payment.acquirer.onboarding.wizard")
	@RequestMapping(method = RequestMethod.POST, value = "/website_sale_payment_acquirer_onboarding_wizards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Website_sale_payment_acquirer_onboarding_wizardDTO> website_sale_payment_acquirer_onboarding_wizarddtos) {
        website_sale_payment_acquirer_onboarding_wizardService.saveBatch(website_sale_payment_acquirer_onboarding_wizardMapping.toDomain(website_sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Get')")
	@ApiOperation(value = "获取数据集", tags = {"website.sale.payment.acquirer.onboarding.wizard" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/website_sale_payment_acquirer_onboarding_wizards/fetchdefault")
	public ResponseEntity<List<Website_sale_payment_acquirer_onboarding_wizardDTO>> fetchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Website_sale_payment_acquirer_onboarding_wizard> domains = website_sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
        List<Website_sale_payment_acquirer_onboarding_wizardDTO> list = website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Website_sale_payment_acquirer_onboarding_wizard-Get')")
	@ApiOperation(value = "查询数据集", tags = {"website.sale.payment.acquirer.onboarding.wizard" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/website_sale_payment_acquirer_onboarding_wizards/searchdefault")
	public ResponseEntity<Page<Website_sale_payment_acquirer_onboarding_wizardDTO>> searchDefault(@RequestBody Website_sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Website_sale_payment_acquirer_onboarding_wizard> domains = website_sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_sale_payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


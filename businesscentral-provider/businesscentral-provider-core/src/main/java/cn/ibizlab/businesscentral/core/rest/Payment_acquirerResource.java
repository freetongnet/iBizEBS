package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_acquirerService;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_acquirerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"付款收单方" })
@RestController("Core-payment_acquirer")
@RequestMapping("")
public class Payment_acquirerResource {

    @Autowired
    public IPayment_acquirerService payment_acquirerService;

    @Autowired
    @Lazy
    public Payment_acquirerMapping payment_acquirerMapping;

    @PreAuthorize("hasPermission(this.payment_acquirerMapping.toDomain(#payment_acquirerdto),'iBizBusinessCentral-Payment_acquirer-Create')")
    @ApiOperation(value = "新建付款收单方", tags = {"付款收单方" },  notes = "新建付款收单方")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers")
    public ResponseEntity<Payment_acquirerDTO> create(@Validated @RequestBody Payment_acquirerDTO payment_acquirerdto) {
        Payment_acquirer domain = payment_acquirerMapping.toDomain(payment_acquirerdto);
		payment_acquirerService.create(domain);
        Payment_acquirerDTO dto = payment_acquirerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_acquirerMapping.toDomain(#payment_acquirerdtos),'iBizBusinessCentral-Payment_acquirer-Create')")
    @ApiOperation(value = "批量新建付款收单方", tags = {"付款收单方" },  notes = "批量新建付款收单方")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_acquirerDTO> payment_acquirerdtos) {
        payment_acquirerService.createBatch(payment_acquirerMapping.toDomain(payment_acquirerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "payment_acquirer" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.payment_acquirerService.get(#payment_acquirer_id),'iBizBusinessCentral-Payment_acquirer-Update')")
    @ApiOperation(value = "更新付款收单方", tags = {"付款收单方" },  notes = "更新付款收单方")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirers/{payment_acquirer_id}")
    public ResponseEntity<Payment_acquirerDTO> update(@PathVariable("payment_acquirer_id") Long payment_acquirer_id, @RequestBody Payment_acquirerDTO payment_acquirerdto) {
		Payment_acquirer domain  = payment_acquirerMapping.toDomain(payment_acquirerdto);
        domain .setId(payment_acquirer_id);
		payment_acquirerService.update(domain );
		Payment_acquirerDTO dto = payment_acquirerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.payment_acquirerService.getPaymentAcquirerByEntities(this.payment_acquirerMapping.toDomain(#payment_acquirerdtos)),'iBizBusinessCentral-Payment_acquirer-Update')")
    @ApiOperation(value = "批量更新付款收单方", tags = {"付款收单方" },  notes = "批量更新付款收单方")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_acquirerDTO> payment_acquirerdtos) {
        payment_acquirerService.updateBatch(payment_acquirerMapping.toDomain(payment_acquirerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.payment_acquirerService.get(#payment_acquirer_id),'iBizBusinessCentral-Payment_acquirer-Remove')")
    @ApiOperation(value = "删除付款收单方", tags = {"付款收单方" },  notes = "删除付款收单方")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirers/{payment_acquirer_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("payment_acquirer_id") Long payment_acquirer_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_acquirerService.remove(payment_acquirer_id));
    }

    @PreAuthorize("hasPermission(this.payment_acquirerService.getPaymentAcquirerByIds(#ids),'iBizBusinessCentral-Payment_acquirer-Remove')")
    @ApiOperation(value = "批量删除付款收单方", tags = {"付款收单方" },  notes = "批量删除付款收单方")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        payment_acquirerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.payment_acquirerMapping.toDomain(returnObject.body),'iBizBusinessCentral-Payment_acquirer-Get')")
    @ApiOperation(value = "获取付款收单方", tags = {"付款收单方" },  notes = "获取付款收单方")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_acquirers/{payment_acquirer_id}")
    public ResponseEntity<Payment_acquirerDTO> get(@PathVariable("payment_acquirer_id") Long payment_acquirer_id) {
        Payment_acquirer domain = payment_acquirerService.get(payment_acquirer_id);
        Payment_acquirerDTO dto = payment_acquirerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取付款收单方草稿", tags = {"付款收单方" },  notes = "获取付款收单方草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_acquirers/getdraft")
    public ResponseEntity<Payment_acquirerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(payment_acquirerMapping.toDto(payment_acquirerService.getDraft(new Payment_acquirer())));
    }

    @ApiOperation(value = "检查付款收单方", tags = {"付款收单方" },  notes = "检查付款收单方")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Payment_acquirerDTO payment_acquirerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(payment_acquirerService.checkKey(payment_acquirerMapping.toDomain(payment_acquirerdto)));
    }

    @PreAuthorize("hasPermission(this.payment_acquirerMapping.toDomain(#payment_acquirerdto),'iBizBusinessCentral-Payment_acquirer-Save')")
    @ApiOperation(value = "保存付款收单方", tags = {"付款收单方" },  notes = "保存付款收单方")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/save")
    public ResponseEntity<Boolean> save(@RequestBody Payment_acquirerDTO payment_acquirerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(payment_acquirerService.save(payment_acquirerMapping.toDomain(payment_acquirerdto)));
    }

    @PreAuthorize("hasPermission(this.payment_acquirerMapping.toDomain(#payment_acquirerdtos),'iBizBusinessCentral-Payment_acquirer-Save')")
    @ApiOperation(value = "批量保存付款收单方", tags = {"付款收单方" },  notes = "批量保存付款收单方")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Payment_acquirerDTO> payment_acquirerdtos) {
        payment_acquirerService.saveBatch(payment_acquirerMapping.toDomain(payment_acquirerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_acquirer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_acquirer-Get')")
	@ApiOperation(value = "获取数据集", tags = {"付款收单方" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/payment_acquirers/fetchdefault")
	public ResponseEntity<List<Payment_acquirerDTO>> fetchDefault(Payment_acquirerSearchContext context) {
        Page<Payment_acquirer> domains = payment_acquirerService.searchDefault(context) ;
        List<Payment_acquirerDTO> list = payment_acquirerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Payment_acquirer-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Payment_acquirer-Get')")
	@ApiOperation(value = "查询数据集", tags = {"付款收单方" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/payment_acquirers/searchdefault")
	public ResponseEntity<Page<Payment_acquirerDTO>> searchDefault(@RequestBody Payment_acquirerSearchContext context) {
        Page<Payment_acquirer> domains = payment_acquirerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_acquirerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_requisitionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"采购申请" })
@RestController("Core-purchase_requisition")
@RequestMapping("")
public class Purchase_requisitionResource {

    @Autowired
    public IPurchase_requisitionService purchase_requisitionService;

    @Autowired
    @Lazy
    public Purchase_requisitionMapping purchase_requisitionMapping;

    @PreAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondto),'iBizBusinessCentral-Purchase_requisition-Create')")
    @ApiOperation(value = "新建采购申请", tags = {"采购申请" },  notes = "新建采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions")
    public ResponseEntity<Purchase_requisitionDTO> create(@Validated @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
		purchase_requisitionService.create(domain);
        Purchase_requisitionDTO dto = purchase_requisitionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondtos),'iBizBusinessCentral-Purchase_requisition-Create')")
    @ApiOperation(value = "批量新建采购申请", tags = {"采购申请" },  notes = "批量新建采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_requisitionDTO> purchase_requisitiondtos) {
        purchase_requisitionService.createBatch(purchase_requisitionMapping.toDomain(purchase_requisitiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionService.get(#purchase_requisition_id),'iBizBusinessCentral-Purchase_requisition-Update')")
    @ApiOperation(value = "更新采购申请", tags = {"采购申请" },  notes = "更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisitions/{purchase_requisition_id}")
    public ResponseEntity<Purchase_requisitionDTO> update(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
		Purchase_requisition domain  = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain .setId(purchase_requisition_id);
		purchase_requisitionService.update(domain );
		Purchase_requisitionDTO dto = purchase_requisitionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionService.getPurchaseRequisitionByEntities(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondtos)),'iBizBusinessCentral-Purchase_requisition-Update')")
    @ApiOperation(value = "批量更新采购申请", tags = {"采购申请" },  notes = "批量更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_requisitions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_requisitionDTO> purchase_requisitiondtos) {
        purchase_requisitionService.updateBatch(purchase_requisitionMapping.toDomain(purchase_requisitiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionService.get(#purchase_requisition_id),'iBizBusinessCentral-Purchase_requisition-Remove')")
    @ApiOperation(value = "删除采购申请", tags = {"采购申请" },  notes = "删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisitions/{purchase_requisition_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("purchase_requisition_id") Long purchase_requisition_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitionService.remove(purchase_requisition_id));
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionService.getPurchaseRequisitionByIds(#ids),'iBizBusinessCentral-Purchase_requisition-Remove')")
    @ApiOperation(value = "批量删除采购申请", tags = {"采购申请" },  notes = "批量删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_requisitions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        purchase_requisitionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_requisition-Get')")
    @ApiOperation(value = "获取采购申请", tags = {"采购申请" },  notes = "获取采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisitions/{purchase_requisition_id}")
    public ResponseEntity<Purchase_requisitionDTO> get(@PathVariable("purchase_requisition_id") Long purchase_requisition_id) {
        Purchase_requisition domain = purchase_requisitionService.get(purchase_requisition_id);
        Purchase_requisitionDTO dto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取采购申请草稿", tags = {"采购申请" },  notes = "获取采购申请草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_requisitions/getdraft")
    public ResponseEntity<Purchase_requisitionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitionMapping.toDto(purchase_requisitionService.getDraft(new Purchase_requisition())));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_cancel-all')")
    @ApiOperation(value = "取消", tags = {"采购申请" },  notes = "取消")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/action_cancel")
    public ResponseEntity<Purchase_requisitionDTO> action_cancel(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setId(purchase_requisition_id);
        domain = purchase_requisitionService.action_cancel(domain);
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_done-all')")
    @ApiOperation(value = "关闭", tags = {"采购申请" },  notes = "关闭")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/action_done")
    public ResponseEntity<Purchase_requisitionDTO> action_done(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setId(purchase_requisition_id);
        domain = purchase_requisitionService.action_done(domain);
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_draft-all')")
    @ApiOperation(value = "重置为草稿", tags = {"采购申请" },  notes = "重置为草稿")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/action_draft")
    public ResponseEntity<Purchase_requisitionDTO> action_draft(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setId(purchase_requisition_id);
        domain = purchase_requisitionService.action_draft(domain);
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_in_progress-all')")
    @ApiOperation(value = "确认", tags = {"采购申请" },  notes = "确认")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/action_in_progress")
    public ResponseEntity<Purchase_requisitionDTO> action_in_progress(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setId(purchase_requisition_id);
        domain = purchase_requisitionService.action_in_progress(domain);
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_open-all')")
    @ApiOperation(value = "验证", tags = {"采购申请" },  notes = "验证")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/action_open")
    public ResponseEntity<Purchase_requisitionDTO> action_open(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setId(purchase_requisition_id);
        domain = purchase_requisitionService.action_open(domain);
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @ApiOperation(value = "检查采购申请", tags = {"采购申请" },  notes = "检查采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_requisitionService.checkKey(purchase_requisitionMapping.toDomain(purchase_requisitiondto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-MasterTabCount-all')")
    @ApiOperation(value = "主数据分页计数", tags = {"采购申请" },  notes = "主数据分页计数")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/{purchase_requisition_id}/mastertabcount")
    public ResponseEntity<Purchase_requisitionDTO> masterTabCount(@PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setId(purchase_requisition_id);
        domain = purchase_requisitionService.masterTabCount(domain);
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondto),'iBizBusinessCentral-Purchase_requisition-Save')")
    @ApiOperation(value = "保存采购申请", tags = {"采购申请" },  notes = "保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/save")
    public ResponseEntity<Boolean> save(@RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitionService.save(purchase_requisitionMapping.toDomain(purchase_requisitiondto)));
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondtos),'iBizBusinessCentral-Purchase_requisition-Save')")
    @ApiOperation(value = "批量保存采购申请", tags = {"采购申请" },  notes = "批量保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_requisitions/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Purchase_requisitionDTO> purchase_requisitiondtos) {
        purchase_requisitionService.saveBatch(purchase_requisitionMapping.toDomain(purchase_requisitiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition-Get')")
	@ApiOperation(value = "获取数据集", tags = {"采购申请" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisitions/fetchdefault")
	public ResponseEntity<List<Purchase_requisitionDTO>> fetchDefault(Purchase_requisitionSearchContext context) {
        Page<Purchase_requisition> domains = purchase_requisitionService.searchDefault(context) ;
        List<Purchase_requisitionDTO> list = purchase_requisitionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition-Get')")
	@ApiOperation(value = "查询数据集", tags = {"采购申请" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisitions/searchdefault")
	public ResponseEntity<Page<Purchase_requisitionDTO>> searchDefault(@RequestBody Purchase_requisitionSearchContext context) {
        Page<Purchase_requisition> domains = purchase_requisitionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisitionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition-Get')")
	@ApiOperation(value = "获取首选表格", tags = {"采购申请" } ,notes = "获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_requisitions/fetchmaster")
	public ResponseEntity<List<Purchase_requisitionDTO>> fetchMaster(Purchase_requisitionSearchContext context) {
        Page<Purchase_requisition> domains = purchase_requisitionService.searchMaster(context) ;
        List<Purchase_requisitionDTO> list = purchase_requisitionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition-Get')")
	@ApiOperation(value = "查询首选表格", tags = {"采购申请" } ,notes = "查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_requisitions/searchmaster")
	public ResponseEntity<Page<Purchase_requisitionDTO>> searchMaster(@RequestBody Purchase_requisitionSearchContext context) {
        Page<Purchase_requisition> domains = purchase_requisitionService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisitionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondto),'iBizBusinessCentral-Purchase_requisition-Create')")
    @ApiOperation(value = "根据供应商建立采购申请", tags = {"采购申请" },  notes = "根据供应商建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions")
    public ResponseEntity<Purchase_requisitionDTO> createByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setVendorId(res_supplier_id);
		purchase_requisitionService.create(domain);
        Purchase_requisitionDTO dto = purchase_requisitionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondtos),'iBizBusinessCentral-Purchase_requisition-Create')")
    @ApiOperation(value = "根据供应商批量建立采购申请", tags = {"采购申请" },  notes = "根据供应商批量建立采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/batch")
    public ResponseEntity<Boolean> createBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Purchase_requisitionDTO> purchase_requisitiondtos) {
        List<Purchase_requisition> domainlist=purchase_requisitionMapping.toDomain(purchase_requisitiondtos);
        for(Purchase_requisition domain:domainlist){
            domain.setVendorId(res_supplier_id);
        }
        purchase_requisitionService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionService.get(#purchase_requisition_id),'iBizBusinessCentral-Purchase_requisition-Update')")
    @ApiOperation(value = "根据供应商更新采购申请", tags = {"采购申请" },  notes = "根据供应商更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}")
    public ResponseEntity<Purchase_requisitionDTO> updateByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setVendorId(res_supplier_id);
        domain.setId(purchase_requisition_id);
		purchase_requisitionService.update(domain);
        Purchase_requisitionDTO dto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionService.getPurchaseRequisitionByEntities(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondtos)),'iBizBusinessCentral-Purchase_requisition-Update')")
    @ApiOperation(value = "根据供应商批量更新采购申请", tags = {"采购申请" },  notes = "根据供应商批量更新采购申请")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/batch")
    public ResponseEntity<Boolean> updateBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Purchase_requisitionDTO> purchase_requisitiondtos) {
        List<Purchase_requisition> domainlist=purchase_requisitionMapping.toDomain(purchase_requisitiondtos);
        for(Purchase_requisition domain:domainlist){
            domain.setVendorId(res_supplier_id);
        }
        purchase_requisitionService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionService.get(#purchase_requisition_id),'iBizBusinessCentral-Purchase_requisition-Remove')")
    @ApiOperation(value = "根据供应商删除采购申请", tags = {"采购申请" },  notes = "根据供应商删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}")
    public ResponseEntity<Boolean> removeByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id) {
		return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitionService.remove(purchase_requisition_id));
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionService.getPurchaseRequisitionByIds(#ids),'iBizBusinessCentral-Purchase_requisition-Remove')")
    @ApiOperation(value = "根据供应商批量删除采购申请", tags = {"采购申请" },  notes = "根据供应商批量删除采购申请")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/batch")
    public ResponseEntity<Boolean> removeBatchByRes_supplier(@RequestBody List<Long> ids) {
        purchase_requisitionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Purchase_requisition-Get')")
    @ApiOperation(value = "根据供应商获取采购申请", tags = {"采购申请" },  notes = "根据供应商获取采购申请")
	@RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}")
    public ResponseEntity<Purchase_requisitionDTO> getByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id) {
        Purchase_requisition domain = purchase_requisitionService.get(purchase_requisition_id);
        Purchase_requisitionDTO dto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据供应商获取采购申请草稿", tags = {"采购申请" },  notes = "根据供应商获取采购申请草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/getdraft")
    public ResponseEntity<Purchase_requisitionDTO> getDraftByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id) {
        Purchase_requisition domain = new Purchase_requisition();
        domain.setVendorId(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitionMapping.toDto(purchase_requisitionService.getDraft(domain)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_cancel-all')")
    @ApiOperation(value = "根据供应商采购申请", tags = {"采购申请" },  notes = "根据供应商采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/action_cancel")
    public ResponseEntity<Purchase_requisitionDTO> action_cancelByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setVendorId(res_supplier_id);
        domain = purchase_requisitionService.action_cancel(domain) ;
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_done-all')")
    @ApiOperation(value = "根据供应商采购申请", tags = {"采购申请" },  notes = "根据供应商采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/action_done")
    public ResponseEntity<Purchase_requisitionDTO> action_doneByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setVendorId(res_supplier_id);
        domain = purchase_requisitionService.action_done(domain) ;
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_draft-all')")
    @ApiOperation(value = "根据供应商采购申请", tags = {"采购申请" },  notes = "根据供应商采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/action_draft")
    public ResponseEntity<Purchase_requisitionDTO> action_draftByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setVendorId(res_supplier_id);
        domain = purchase_requisitionService.action_draft(domain) ;
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_in_progress-all')")
    @ApiOperation(value = "根据供应商采购申请", tags = {"采购申请" },  notes = "根据供应商采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/action_in_progress")
    public ResponseEntity<Purchase_requisitionDTO> action_in_progressByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setVendorId(res_supplier_id);
        domain = purchase_requisitionService.action_in_progress(domain) ;
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-Action_open-all')")
    @ApiOperation(value = "根据供应商采购申请", tags = {"采购申请" },  notes = "根据供应商采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/action_open")
    public ResponseEntity<Purchase_requisitionDTO> action_openByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setVendorId(res_supplier_id);
        domain = purchase_requisitionService.action_open(domain) ;
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @ApiOperation(value = "根据供应商检查采购申请", tags = {"采购申请" },  notes = "根据供应商检查采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/checkkey")
    public ResponseEntity<Boolean> checkKeyByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_requisitionService.checkKey(purchase_requisitionMapping.toDomain(purchase_requisitiondto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-MasterTabCount-all')")
    @ApiOperation(value = "根据供应商采购申请", tags = {"采购申请" },  notes = "根据供应商采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/{purchase_requisition_id}/mastertabcount")
    public ResponseEntity<Purchase_requisitionDTO> masterTabCountByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @PathVariable("purchase_requisition_id") Long purchase_requisition_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setVendorId(res_supplier_id);
        domain = purchase_requisitionService.masterTabCount(domain) ;
        purchase_requisitiondto = purchase_requisitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitiondto);
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondto),'iBizBusinessCentral-Purchase_requisition-Save')")
    @ApiOperation(value = "根据供应商保存采购申请", tags = {"采购申请" },  notes = "根据供应商保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/save")
    public ResponseEntity<Boolean> saveByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_requisitionDTO purchase_requisitiondto) {
        Purchase_requisition domain = purchase_requisitionMapping.toDomain(purchase_requisitiondto);
        domain.setVendorId(res_supplier_id);
        return ResponseEntity.status(HttpStatus.OK).body(purchase_requisitionService.save(domain));
    }

    @PreAuthorize("hasPermission(this.purchase_requisitionMapping.toDomain(#purchase_requisitiondtos),'iBizBusinessCentral-Purchase_requisition-Save')")
    @ApiOperation(value = "根据供应商批量保存采购申请", tags = {"采购申请" },  notes = "根据供应商批量保存采购申请")
	@RequestMapping(method = RequestMethod.POST, value = "/res_suppliers/{res_supplier_id}/purchase_requisitions/savebatch")
    public ResponseEntity<Boolean> saveBatchByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody List<Purchase_requisitionDTO> purchase_requisitiondtos) {
        List<Purchase_requisition> domainlist=purchase_requisitionMapping.toDomain(purchase_requisitiondtos);
        for(Purchase_requisition domain:domainlist){
             domain.setVendorId(res_supplier_id);
        }
        purchase_requisitionService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition-Get')")
	@ApiOperation(value = "根据供应商获取数据集", tags = {"采购申请" } ,notes = "根据供应商获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/fetchdefault")
	public ResponseEntity<List<Purchase_requisitionDTO>> fetchPurchase_requisitionDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id,Purchase_requisitionSearchContext context) {
        context.setN_vendor_id_eq(res_supplier_id);
        Page<Purchase_requisition> domains = purchase_requisitionService.searchDefault(context) ;
        List<Purchase_requisitionDTO> list = purchase_requisitionMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition-Get')")
	@ApiOperation(value = "根据供应商查询数据集", tags = {"采购申请" } ,notes = "根据供应商查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/searchdefault")
	public ResponseEntity<Page<Purchase_requisitionDTO>> searchPurchase_requisitionDefaultByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_requisitionSearchContext context) {
        context.setN_vendor_id_eq(res_supplier_id);
        Page<Purchase_requisition> domains = purchase_requisitionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisitionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition-Get')")
	@ApiOperation(value = "根据供应商获取首选表格", tags = {"采购申请" } ,notes = "根据供应商获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/fetchmaster")
	public ResponseEntity<List<Purchase_requisitionDTO>> fetchPurchase_requisitionMasterByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id,Purchase_requisitionSearchContext context) {
        context.setN_vendor_id_eq(res_supplier_id);
        Page<Purchase_requisition> domains = purchase_requisitionService.searchMaster(context) ;
        List<Purchase_requisitionDTO> list = purchase_requisitionMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_requisition-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Purchase_requisition-Get')")
	@ApiOperation(value = "根据供应商查询首选表格", tags = {"采购申请" } ,notes = "根据供应商查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/res_suppliers/{res_supplier_id}/purchase_requisitions/searchmaster")
	public ResponseEntity<Page<Purchase_requisitionDTO>> searchPurchase_requisitionMasterByRes_supplier(@PathVariable("res_supplier_id") Long res_supplier_id, @RequestBody Purchase_requisitionSearchContext context) {
        context.setN_vendor_id_eq(res_supplier_id);
        Page<Purchase_requisition> domains = purchase_requisitionService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_requisitionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_year;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_yearService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_yearSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"会计年度" })
@RestController("Core-account_fiscal_year")
@RequestMapping("")
public class Account_fiscal_yearResource {

    @Autowired
    public IAccount_fiscal_yearService account_fiscal_yearService;

    @Autowired
    @Lazy
    public Account_fiscal_yearMapping account_fiscal_yearMapping;

    @PreAuthorize("hasPermission(this.account_fiscal_yearMapping.toDomain(#account_fiscal_yeardto),'iBizBusinessCentral-Account_fiscal_year-Create')")
    @ApiOperation(value = "新建会计年度", tags = {"会计年度" },  notes = "新建会计年度")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years")
    public ResponseEntity<Account_fiscal_yearDTO> create(@Validated @RequestBody Account_fiscal_yearDTO account_fiscal_yeardto) {
        Account_fiscal_year domain = account_fiscal_yearMapping.toDomain(account_fiscal_yeardto);
		account_fiscal_yearService.create(domain);
        Account_fiscal_yearDTO dto = account_fiscal_yearMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_yearMapping.toDomain(#account_fiscal_yeardtos),'iBizBusinessCentral-Account_fiscal_year-Create')")
    @ApiOperation(value = "批量新建会计年度", tags = {"会计年度" },  notes = "批量新建会计年度")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_fiscal_yearDTO> account_fiscal_yeardtos) {
        account_fiscal_yearService.createBatch(account_fiscal_yearMapping.toDomain(account_fiscal_yeardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_fiscal_year" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_fiscal_yearService.get(#account_fiscal_year_id),'iBizBusinessCentral-Account_fiscal_year-Update')")
    @ApiOperation(value = "更新会计年度", tags = {"会计年度" },  notes = "更新会计年度")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_years/{account_fiscal_year_id}")
    public ResponseEntity<Account_fiscal_yearDTO> update(@PathVariable("account_fiscal_year_id") Long account_fiscal_year_id, @RequestBody Account_fiscal_yearDTO account_fiscal_yeardto) {
		Account_fiscal_year domain  = account_fiscal_yearMapping.toDomain(account_fiscal_yeardto);
        domain .setId(account_fiscal_year_id);
		account_fiscal_yearService.update(domain );
		Account_fiscal_yearDTO dto = account_fiscal_yearMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_yearService.getAccountFiscalYearByEntities(this.account_fiscal_yearMapping.toDomain(#account_fiscal_yeardtos)),'iBizBusinessCentral-Account_fiscal_year-Update')")
    @ApiOperation(value = "批量更新会计年度", tags = {"会计年度" },  notes = "批量更新会计年度")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_years/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_fiscal_yearDTO> account_fiscal_yeardtos) {
        account_fiscal_yearService.updateBatch(account_fiscal_yearMapping.toDomain(account_fiscal_yeardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_fiscal_yearService.get(#account_fiscal_year_id),'iBizBusinessCentral-Account_fiscal_year-Remove')")
    @ApiOperation(value = "删除会计年度", tags = {"会计年度" },  notes = "删除会计年度")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_years/{account_fiscal_year_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_fiscal_year_id") Long account_fiscal_year_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_yearService.remove(account_fiscal_year_id));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_yearService.getAccountFiscalYearByIds(#ids),'iBizBusinessCentral-Account_fiscal_year-Remove')")
    @ApiOperation(value = "批量删除会计年度", tags = {"会计年度" },  notes = "批量删除会计年度")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_years/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_fiscal_yearService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_fiscal_yearMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_fiscal_year-Get')")
    @ApiOperation(value = "获取会计年度", tags = {"会计年度" },  notes = "获取会计年度")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_years/{account_fiscal_year_id}")
    public ResponseEntity<Account_fiscal_yearDTO> get(@PathVariable("account_fiscal_year_id") Long account_fiscal_year_id) {
        Account_fiscal_year domain = account_fiscal_yearService.get(account_fiscal_year_id);
        Account_fiscal_yearDTO dto = account_fiscal_yearMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取会计年度草稿", tags = {"会计年度" },  notes = "获取会计年度草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_years/getdraft")
    public ResponseEntity<Account_fiscal_yearDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_yearMapping.toDto(account_fiscal_yearService.getDraft(new Account_fiscal_year())));
    }

    @ApiOperation(value = "检查会计年度", tags = {"会计年度" },  notes = "检查会计年度")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_fiscal_yearDTO account_fiscal_yeardto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_fiscal_yearService.checkKey(account_fiscal_yearMapping.toDomain(account_fiscal_yeardto)));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_yearMapping.toDomain(#account_fiscal_yeardto),'iBizBusinessCentral-Account_fiscal_year-Save')")
    @ApiOperation(value = "保存会计年度", tags = {"会计年度" },  notes = "保存会计年度")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_fiscal_yearDTO account_fiscal_yeardto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_yearService.save(account_fiscal_yearMapping.toDomain(account_fiscal_yeardto)));
    }

    @PreAuthorize("hasPermission(this.account_fiscal_yearMapping.toDomain(#account_fiscal_yeardtos),'iBizBusinessCentral-Account_fiscal_year-Save')")
    @ApiOperation(value = "批量保存会计年度", tags = {"会计年度" },  notes = "批量保存会计年度")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_years/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_fiscal_yearDTO> account_fiscal_yeardtos) {
        account_fiscal_yearService.saveBatch(account_fiscal_yearMapping.toDomain(account_fiscal_yeardtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_fiscal_year-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_fiscal_year-Get')")
	@ApiOperation(value = "获取数据集", tags = {"会计年度" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_fiscal_years/fetchdefault")
	public ResponseEntity<List<Account_fiscal_yearDTO>> fetchDefault(Account_fiscal_yearSearchContext context) {
        Page<Account_fiscal_year> domains = account_fiscal_yearService.searchDefault(context) ;
        List<Account_fiscal_yearDTO> list = account_fiscal_yearMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_fiscal_year-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_fiscal_year-Get')")
	@ApiOperation(value = "查询数据集", tags = {"会计年度" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_fiscal_years/searchdefault")
	public ResponseEntity<Page<Account_fiscal_yearDTO>> searchDefault(@RequestBody Account_fiscal_yearSearchContext context) {
        Page<Account_fiscal_year> domains = account_fiscal_yearService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_fiscal_yearMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_common_report;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_common_reportService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_common_reportSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"账户通用报表" })
@RestController("Core-account_common_report")
@RequestMapping("")
public class Account_common_reportResource {

    @Autowired
    public IAccount_common_reportService account_common_reportService;

    @Autowired
    @Lazy
    public Account_common_reportMapping account_common_reportMapping;

    @PreAuthorize("hasPermission(this.account_common_reportMapping.toDomain(#account_common_reportdto),'iBizBusinessCentral-Account_common_report-Create')")
    @ApiOperation(value = "新建账户通用报表", tags = {"账户通用报表" },  notes = "新建账户通用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_common_reports")
    public ResponseEntity<Account_common_reportDTO> create(@Validated @RequestBody Account_common_reportDTO account_common_reportdto) {
        Account_common_report domain = account_common_reportMapping.toDomain(account_common_reportdto);
		account_common_reportService.create(domain);
        Account_common_reportDTO dto = account_common_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_common_reportMapping.toDomain(#account_common_reportdtos),'iBizBusinessCentral-Account_common_report-Create')")
    @ApiOperation(value = "批量新建账户通用报表", tags = {"账户通用报表" },  notes = "批量新建账户通用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_common_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_common_reportDTO> account_common_reportdtos) {
        account_common_reportService.createBatch(account_common_reportMapping.toDomain(account_common_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_common_report" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_common_reportService.get(#account_common_report_id),'iBizBusinessCentral-Account_common_report-Update')")
    @ApiOperation(value = "更新账户通用报表", tags = {"账户通用报表" },  notes = "更新账户通用报表")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_common_reports/{account_common_report_id}")
    public ResponseEntity<Account_common_reportDTO> update(@PathVariable("account_common_report_id") Long account_common_report_id, @RequestBody Account_common_reportDTO account_common_reportdto) {
		Account_common_report domain  = account_common_reportMapping.toDomain(account_common_reportdto);
        domain .setId(account_common_report_id);
		account_common_reportService.update(domain );
		Account_common_reportDTO dto = account_common_reportMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_common_reportService.getAccountCommonReportByEntities(this.account_common_reportMapping.toDomain(#account_common_reportdtos)),'iBizBusinessCentral-Account_common_report-Update')")
    @ApiOperation(value = "批量更新账户通用报表", tags = {"账户通用报表" },  notes = "批量更新账户通用报表")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_common_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_common_reportDTO> account_common_reportdtos) {
        account_common_reportService.updateBatch(account_common_reportMapping.toDomain(account_common_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_common_reportService.get(#account_common_report_id),'iBizBusinessCentral-Account_common_report-Remove')")
    @ApiOperation(value = "删除账户通用报表", tags = {"账户通用报表" },  notes = "删除账户通用报表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_common_reports/{account_common_report_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_common_report_id") Long account_common_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_common_reportService.remove(account_common_report_id));
    }

    @PreAuthorize("hasPermission(this.account_common_reportService.getAccountCommonReportByIds(#ids),'iBizBusinessCentral-Account_common_report-Remove')")
    @ApiOperation(value = "批量删除账户通用报表", tags = {"账户通用报表" },  notes = "批量删除账户通用报表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_common_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_common_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_common_reportMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_common_report-Get')")
    @ApiOperation(value = "获取账户通用报表", tags = {"账户通用报表" },  notes = "获取账户通用报表")
	@RequestMapping(method = RequestMethod.GET, value = "/account_common_reports/{account_common_report_id}")
    public ResponseEntity<Account_common_reportDTO> get(@PathVariable("account_common_report_id") Long account_common_report_id) {
        Account_common_report domain = account_common_reportService.get(account_common_report_id);
        Account_common_reportDTO dto = account_common_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取账户通用报表草稿", tags = {"账户通用报表" },  notes = "获取账户通用报表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_common_reports/getdraft")
    public ResponseEntity<Account_common_reportDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_common_reportMapping.toDto(account_common_reportService.getDraft(new Account_common_report())));
    }

    @ApiOperation(value = "检查账户通用报表", tags = {"账户通用报表" },  notes = "检查账户通用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_common_reports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_common_reportDTO account_common_reportdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_common_reportService.checkKey(account_common_reportMapping.toDomain(account_common_reportdto)));
    }

    @PreAuthorize("hasPermission(this.account_common_reportMapping.toDomain(#account_common_reportdto),'iBizBusinessCentral-Account_common_report-Save')")
    @ApiOperation(value = "保存账户通用报表", tags = {"账户通用报表" },  notes = "保存账户通用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_common_reports/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_common_reportDTO account_common_reportdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_common_reportService.save(account_common_reportMapping.toDomain(account_common_reportdto)));
    }

    @PreAuthorize("hasPermission(this.account_common_reportMapping.toDomain(#account_common_reportdtos),'iBizBusinessCentral-Account_common_report-Save')")
    @ApiOperation(value = "批量保存账户通用报表", tags = {"账户通用报表" },  notes = "批量保存账户通用报表")
	@RequestMapping(method = RequestMethod.POST, value = "/account_common_reports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_common_reportDTO> account_common_reportdtos) {
        account_common_reportService.saveBatch(account_common_reportMapping.toDomain(account_common_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_common_report-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_common_report-Get')")
	@ApiOperation(value = "获取数据集", tags = {"账户通用报表" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_common_reports/fetchdefault")
	public ResponseEntity<List<Account_common_reportDTO>> fetchDefault(Account_common_reportSearchContext context) {
        Page<Account_common_report> domains = account_common_reportService.searchDefault(context) ;
        List<Account_common_reportDTO> list = account_common_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_common_report-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_common_report-Get')")
	@ApiOperation(value = "查询数据集", tags = {"账户通用报表" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_common_reports/searchdefault")
	public ResponseEntity<Page<Account_common_reportDTO>> searchDefault(@RequestBody Account_common_reportSearchContext context) {
        Page<Account_common_report> domains = account_common_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_common_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


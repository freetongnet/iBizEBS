package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_stageService;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_stageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"调查阶段" })
@RestController("Core-survey_stage")
@RequestMapping("")
public class Survey_stageResource {

    @Autowired
    public ISurvey_stageService survey_stageService;

    @Autowired
    @Lazy
    public Survey_stageMapping survey_stageMapping;

    @PreAuthorize("hasPermission(this.survey_stageMapping.toDomain(#survey_stagedto),'iBizBusinessCentral-Survey_stage-Create')")
    @ApiOperation(value = "新建调查阶段", tags = {"调查阶段" },  notes = "新建调查阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_stages")
    public ResponseEntity<Survey_stageDTO> create(@Validated @RequestBody Survey_stageDTO survey_stagedto) {
        Survey_stage domain = survey_stageMapping.toDomain(survey_stagedto);
		survey_stageService.create(domain);
        Survey_stageDTO dto = survey_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_stageMapping.toDomain(#survey_stagedtos),'iBizBusinessCentral-Survey_stage-Create')")
    @ApiOperation(value = "批量新建调查阶段", tags = {"调查阶段" },  notes = "批量新建调查阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_stageDTO> survey_stagedtos) {
        survey_stageService.createBatch(survey_stageMapping.toDomain(survey_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "survey_stage" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.survey_stageService.get(#survey_stage_id),'iBizBusinessCentral-Survey_stage-Update')")
    @ApiOperation(value = "更新调查阶段", tags = {"调查阶段" },  notes = "更新调查阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_stages/{survey_stage_id}")
    public ResponseEntity<Survey_stageDTO> update(@PathVariable("survey_stage_id") Long survey_stage_id, @RequestBody Survey_stageDTO survey_stagedto) {
		Survey_stage domain  = survey_stageMapping.toDomain(survey_stagedto);
        domain .setId(survey_stage_id);
		survey_stageService.update(domain );
		Survey_stageDTO dto = survey_stageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_stageService.getSurveyStageByEntities(this.survey_stageMapping.toDomain(#survey_stagedtos)),'iBizBusinessCentral-Survey_stage-Update')")
    @ApiOperation(value = "批量更新调查阶段", tags = {"调查阶段" },  notes = "批量更新调查阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_stageDTO> survey_stagedtos) {
        survey_stageService.updateBatch(survey_stageMapping.toDomain(survey_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.survey_stageService.get(#survey_stage_id),'iBizBusinessCentral-Survey_stage-Remove')")
    @ApiOperation(value = "删除调查阶段", tags = {"调查阶段" },  notes = "删除调查阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_stages/{survey_stage_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("survey_stage_id") Long survey_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_stageService.remove(survey_stage_id));
    }

    @PreAuthorize("hasPermission(this.survey_stageService.getSurveyStageByIds(#ids),'iBizBusinessCentral-Survey_stage-Remove')")
    @ApiOperation(value = "批量删除调查阶段", tags = {"调查阶段" },  notes = "批量删除调查阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        survey_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.survey_stageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Survey_stage-Get')")
    @ApiOperation(value = "获取调查阶段", tags = {"调查阶段" },  notes = "获取调查阶段")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_stages/{survey_stage_id}")
    public ResponseEntity<Survey_stageDTO> get(@PathVariable("survey_stage_id") Long survey_stage_id) {
        Survey_stage domain = survey_stageService.get(survey_stage_id);
        Survey_stageDTO dto = survey_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取调查阶段草稿", tags = {"调查阶段" },  notes = "获取调查阶段草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_stages/getdraft")
    public ResponseEntity<Survey_stageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(survey_stageMapping.toDto(survey_stageService.getDraft(new Survey_stage())));
    }

    @ApiOperation(value = "检查调查阶段", tags = {"调查阶段" },  notes = "检查调查阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_stages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Survey_stageDTO survey_stagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(survey_stageService.checkKey(survey_stageMapping.toDomain(survey_stagedto)));
    }

    @PreAuthorize("hasPermission(this.survey_stageMapping.toDomain(#survey_stagedto),'iBizBusinessCentral-Survey_stage-Save')")
    @ApiOperation(value = "保存调查阶段", tags = {"调查阶段" },  notes = "保存调查阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_stages/save")
    public ResponseEntity<Boolean> save(@RequestBody Survey_stageDTO survey_stagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(survey_stageService.save(survey_stageMapping.toDomain(survey_stagedto)));
    }

    @PreAuthorize("hasPermission(this.survey_stageMapping.toDomain(#survey_stagedtos),'iBizBusinessCentral-Survey_stage-Save')")
    @ApiOperation(value = "批量保存调查阶段", tags = {"调查阶段" },  notes = "批量保存调查阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_stages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Survey_stageDTO> survey_stagedtos) {
        survey_stageService.saveBatch(survey_stageMapping.toDomain(survey_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_stage-Get')")
	@ApiOperation(value = "获取数据集", tags = {"调查阶段" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/survey_stages/fetchdefault")
	public ResponseEntity<List<Survey_stageDTO>> fetchDefault(Survey_stageSearchContext context) {
        Page<Survey_stage> domains = survey_stageService.searchDefault(context) ;
        List<Survey_stageDTO> list = survey_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_stage-Get')")
	@ApiOperation(value = "查询数据集", tags = {"调查阶段" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/survey_stages/searchdefault")
	public ResponseEntity<Page<Survey_stageDTO>> searchDefault(@RequestBody Survey_stageSearchContext context) {
        Page<Survey_stage> domains = survey_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


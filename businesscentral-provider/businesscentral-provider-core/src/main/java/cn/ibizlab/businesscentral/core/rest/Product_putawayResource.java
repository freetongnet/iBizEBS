package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_putawayService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_putawaySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"上架策略" })
@RestController("Core-product_putaway")
@RequestMapping("")
public class Product_putawayResource {

    @Autowired
    public IProduct_putawayService product_putawayService;

    @Autowired
    @Lazy
    public Product_putawayMapping product_putawayMapping;

    @PreAuthorize("hasPermission(this.product_putawayMapping.toDomain(#product_putawaydto),'iBizBusinessCentral-Product_putaway-Create')")
    @ApiOperation(value = "新建上架策略", tags = {"上架策略" },  notes = "新建上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_putaways")
    public ResponseEntity<Product_putawayDTO> create(@Validated @RequestBody Product_putawayDTO product_putawaydto) {
        Product_putaway domain = product_putawayMapping.toDomain(product_putawaydto);
		product_putawayService.create(domain);
        Product_putawayDTO dto = product_putawayMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_putawayMapping.toDomain(#product_putawaydtos),'iBizBusinessCentral-Product_putaway-Create')")
    @ApiOperation(value = "批量新建上架策略", tags = {"上架策略" },  notes = "批量新建上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_putaways/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_putawayDTO> product_putawaydtos) {
        product_putawayService.createBatch(product_putawayMapping.toDomain(product_putawaydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_putaway" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_putawayService.get(#product_putaway_id),'iBizBusinessCentral-Product_putaway-Update')")
    @ApiOperation(value = "更新上架策略", tags = {"上架策略" },  notes = "更新上架策略")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_putaways/{product_putaway_id}")
    public ResponseEntity<Product_putawayDTO> update(@PathVariable("product_putaway_id") Long product_putaway_id, @RequestBody Product_putawayDTO product_putawaydto) {
		Product_putaway domain  = product_putawayMapping.toDomain(product_putawaydto);
        domain .setId(product_putaway_id);
		product_putawayService.update(domain );
		Product_putawayDTO dto = product_putawayMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_putawayService.getProductPutawayByEntities(this.product_putawayMapping.toDomain(#product_putawaydtos)),'iBizBusinessCentral-Product_putaway-Update')")
    @ApiOperation(value = "批量更新上架策略", tags = {"上架策略" },  notes = "批量更新上架策略")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_putaways/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_putawayDTO> product_putawaydtos) {
        product_putawayService.updateBatch(product_putawayMapping.toDomain(product_putawaydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_putawayService.get(#product_putaway_id),'iBizBusinessCentral-Product_putaway-Remove')")
    @ApiOperation(value = "删除上架策略", tags = {"上架策略" },  notes = "删除上架策略")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_putaways/{product_putaway_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_putaway_id") Long product_putaway_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_putawayService.remove(product_putaway_id));
    }

    @PreAuthorize("hasPermission(this.product_putawayService.getProductPutawayByIds(#ids),'iBizBusinessCentral-Product_putaway-Remove')")
    @ApiOperation(value = "批量删除上架策略", tags = {"上架策略" },  notes = "批量删除上架策略")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_putaways/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_putawayService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_putawayMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_putaway-Get')")
    @ApiOperation(value = "获取上架策略", tags = {"上架策略" },  notes = "获取上架策略")
	@RequestMapping(method = RequestMethod.GET, value = "/product_putaways/{product_putaway_id}")
    public ResponseEntity<Product_putawayDTO> get(@PathVariable("product_putaway_id") Long product_putaway_id) {
        Product_putaway domain = product_putawayService.get(product_putaway_id);
        Product_putawayDTO dto = product_putawayMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取上架策略草稿", tags = {"上架策略" },  notes = "获取上架策略草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_putaways/getdraft")
    public ResponseEntity<Product_putawayDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_putawayMapping.toDto(product_putawayService.getDraft(new Product_putaway())));
    }

    @ApiOperation(value = "检查上架策略", tags = {"上架策略" },  notes = "检查上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_putaways/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_putawayDTO product_putawaydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_putawayService.checkKey(product_putawayMapping.toDomain(product_putawaydto)));
    }

    @PreAuthorize("hasPermission(this.product_putawayMapping.toDomain(#product_putawaydto),'iBizBusinessCentral-Product_putaway-Save')")
    @ApiOperation(value = "保存上架策略", tags = {"上架策略" },  notes = "保存上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_putaways/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_putawayDTO product_putawaydto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_putawayService.save(product_putawayMapping.toDomain(product_putawaydto)));
    }

    @PreAuthorize("hasPermission(this.product_putawayMapping.toDomain(#product_putawaydtos),'iBizBusinessCentral-Product_putaway-Save')")
    @ApiOperation(value = "批量保存上架策略", tags = {"上架策略" },  notes = "批量保存上架策略")
	@RequestMapping(method = RequestMethod.POST, value = "/product_putaways/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_putawayDTO> product_putawaydtos) {
        product_putawayService.saveBatch(product_putawayMapping.toDomain(product_putawaydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_putaway-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_putaway-Get')")
	@ApiOperation(value = "获取数据集", tags = {"上架策略" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_putaways/fetchdefault")
	public ResponseEntity<List<Product_putawayDTO>> fetchDefault(Product_putawaySearchContext context) {
        Page<Product_putaway> domains = product_putawayService.searchDefault(context) ;
        List<Product_putawayDTO> list = product_putawayMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_putaway-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_putaway-Get')")
	@ApiOperation(value = "查询数据集", tags = {"上架策略" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_putaways/searchdefault")
	public ResponseEntity<Page<Product_putawayDTO>> searchDefault(@RequestBody Product_putawaySearchContext context) {
        Page<Product_putaway> domains = product_putawayService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_putawayMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


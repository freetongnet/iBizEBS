package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_char_states;
import cn.ibizlab.businesscentral.core.odoo_base_import.service.IBase_import_tests_models_char_statesService;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_char_statesSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试:基本导入模型，字符状态" })
@RestController("Core-base_import_tests_models_char_states")
@RequestMapping("")
public class Base_import_tests_models_char_statesResource {

    @Autowired
    public IBase_import_tests_models_char_statesService base_import_tests_models_char_statesService;

    @Autowired
    @Lazy
    public Base_import_tests_models_char_statesMapping base_import_tests_models_char_statesMapping;

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_statesMapping.toDomain(#base_import_tests_models_char_statesdto),'iBizBusinessCentral-Base_import_tests_models_char_states-Create')")
    @ApiOperation(value = "新建测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "新建测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states")
    public ResponseEntity<Base_import_tests_models_char_statesDTO> create(@Validated @RequestBody Base_import_tests_models_char_statesDTO base_import_tests_models_char_statesdto) {
        Base_import_tests_models_char_states domain = base_import_tests_models_char_statesMapping.toDomain(base_import_tests_models_char_statesdto);
		base_import_tests_models_char_statesService.create(domain);
        Base_import_tests_models_char_statesDTO dto = base_import_tests_models_char_statesMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_statesMapping.toDomain(#base_import_tests_models_char_statesdtos),'iBizBusinessCentral-Base_import_tests_models_char_states-Create')")
    @ApiOperation(value = "批量新建测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "批量新建测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_char_statesDTO> base_import_tests_models_char_statesdtos) {
        base_import_tests_models_char_statesService.createBatch(base_import_tests_models_char_statesMapping.toDomain(base_import_tests_models_char_statesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_import_tests_models_char_states" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_import_tests_models_char_statesService.get(#base_import_tests_models_char_states_id),'iBizBusinessCentral-Base_import_tests_models_char_states-Update')")
    @ApiOperation(value = "更新测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "更新测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_states/{base_import_tests_models_char_states_id}")
    public ResponseEntity<Base_import_tests_models_char_statesDTO> update(@PathVariable("base_import_tests_models_char_states_id") Long base_import_tests_models_char_states_id, @RequestBody Base_import_tests_models_char_statesDTO base_import_tests_models_char_statesdto) {
		Base_import_tests_models_char_states domain  = base_import_tests_models_char_statesMapping.toDomain(base_import_tests_models_char_statesdto);
        domain .setId(base_import_tests_models_char_states_id);
		base_import_tests_models_char_statesService.update(domain );
		Base_import_tests_models_char_statesDTO dto = base_import_tests_models_char_statesMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_statesService.getBaseImportTestsModelsCharStatesByEntities(this.base_import_tests_models_char_statesMapping.toDomain(#base_import_tests_models_char_statesdtos)),'iBizBusinessCentral-Base_import_tests_models_char_states-Update')")
    @ApiOperation(value = "批量更新测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "批量更新测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_char_states/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_char_statesDTO> base_import_tests_models_char_statesdtos) {
        base_import_tests_models_char_statesService.updateBatch(base_import_tests_models_char_statesMapping.toDomain(base_import_tests_models_char_statesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_statesService.get(#base_import_tests_models_char_states_id),'iBizBusinessCentral-Base_import_tests_models_char_states-Remove')")
    @ApiOperation(value = "删除测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "删除测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_states/{base_import_tests_models_char_states_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_states_id") Long base_import_tests_models_char_states_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_statesService.remove(base_import_tests_models_char_states_id));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_statesService.getBaseImportTestsModelsCharStatesByIds(#ids),'iBizBusinessCentral-Base_import_tests_models_char_states-Remove')")
    @ApiOperation(value = "批量删除测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "批量删除测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_char_states/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_import_tests_models_char_statesService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_import_tests_models_char_statesMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_import_tests_models_char_states-Get')")
    @ApiOperation(value = "获取测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "获取测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_states/{base_import_tests_models_char_states_id}")
    public ResponseEntity<Base_import_tests_models_char_statesDTO> get(@PathVariable("base_import_tests_models_char_states_id") Long base_import_tests_models_char_states_id) {
        Base_import_tests_models_char_states domain = base_import_tests_models_char_statesService.get(base_import_tests_models_char_states_id);
        Base_import_tests_models_char_statesDTO dto = base_import_tests_models_char_statesMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试:基本导入模型，字符状态草稿", tags = {"测试:基本导入模型，字符状态" },  notes = "获取测试:基本导入模型，字符状态草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_char_states/getdraft")
    public ResponseEntity<Base_import_tests_models_char_statesDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_statesMapping.toDto(base_import_tests_models_char_statesService.getDraft(new Base_import_tests_models_char_states())));
    }

    @ApiOperation(value = "检查测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "检查测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_import_tests_models_char_statesDTO base_import_tests_models_char_statesdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_statesService.checkKey(base_import_tests_models_char_statesMapping.toDomain(base_import_tests_models_char_statesdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_statesMapping.toDomain(#base_import_tests_models_char_statesdto),'iBizBusinessCentral-Base_import_tests_models_char_states-Save')")
    @ApiOperation(value = "保存测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "保存测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_import_tests_models_char_statesDTO base_import_tests_models_char_statesdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_char_statesService.save(base_import_tests_models_char_statesMapping.toDomain(base_import_tests_models_char_statesdto)));
    }

    @PreAuthorize("hasPermission(this.base_import_tests_models_char_statesMapping.toDomain(#base_import_tests_models_char_statesdtos),'iBizBusinessCentral-Base_import_tests_models_char_states-Save')")
    @ApiOperation(value = "批量保存测试:基本导入模型，字符状态", tags = {"测试:基本导入模型，字符状态" },  notes = "批量保存测试:基本导入模型，字符状态")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_char_states/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_import_tests_models_char_statesDTO> base_import_tests_models_char_statesdtos) {
        base_import_tests_models_char_statesService.saveBatch(base_import_tests_models_char_statesMapping.toDomain(base_import_tests_models_char_statesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_char_states-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_char_states-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试:基本导入模型，字符状态" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_char_states/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_char_statesDTO>> fetchDefault(Base_import_tests_models_char_statesSearchContext context) {
        Page<Base_import_tests_models_char_states> domains = base_import_tests_models_char_statesService.searchDefault(context) ;
        List<Base_import_tests_models_char_statesDTO> list = base_import_tests_models_char_statesMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_import_tests_models_char_states-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_import_tests_models_char_states-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试:基本导入模型，字符状态" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_import_tests_models_char_states/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_char_statesDTO>> searchDefault(@RequestBody Base_import_tests_models_char_statesSearchContext context) {
        Page<Base_import_tests_models_char_states> domains = base_import_tests_models_char_statesService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_char_statesMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


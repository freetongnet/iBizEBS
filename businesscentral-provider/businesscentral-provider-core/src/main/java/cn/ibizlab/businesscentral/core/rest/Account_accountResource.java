package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_accountSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"科目" })
@RestController("Core-account_account")
@RequestMapping("")
public class Account_accountResource {

    @Autowired
    public IAccount_accountService account_accountService;

    @Autowired
    @Lazy
    public Account_accountMapping account_accountMapping;

    @PreAuthorize("hasPermission(this.account_accountMapping.toDomain(#account_accountdto),'iBizBusinessCentral-Account_account-Create')")
    @ApiOperation(value = "新建科目", tags = {"科目" },  notes = "新建科目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_accounts")
    public ResponseEntity<Account_accountDTO> create(@Validated @RequestBody Account_accountDTO account_accountdto) {
        Account_account domain = account_accountMapping.toDomain(account_accountdto);
		account_accountService.create(domain);
        Account_accountDTO dto = account_accountMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_accountMapping.toDomain(#account_accountdtos),'iBizBusinessCentral-Account_account-Create')")
    @ApiOperation(value = "批量新建科目", tags = {"科目" },  notes = "批量新建科目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_accounts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_accountDTO> account_accountdtos) {
        account_accountService.createBatch(account_accountMapping.toDomain(account_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_account" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_accountService.get(#account_account_id),'iBizBusinessCentral-Account_account-Update')")
    @ApiOperation(value = "更新科目", tags = {"科目" },  notes = "更新科目")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_accounts/{account_account_id}")
    public ResponseEntity<Account_accountDTO> update(@PathVariable("account_account_id") Long account_account_id, @RequestBody Account_accountDTO account_accountdto) {
		Account_account domain  = account_accountMapping.toDomain(account_accountdto);
        domain .setId(account_account_id);
		account_accountService.update(domain );
		Account_accountDTO dto = account_accountMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_accountService.getAccountAccountByEntities(this.account_accountMapping.toDomain(#account_accountdtos)),'iBizBusinessCentral-Account_account-Update')")
    @ApiOperation(value = "批量更新科目", tags = {"科目" },  notes = "批量更新科目")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_accounts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_accountDTO> account_accountdtos) {
        account_accountService.updateBatch(account_accountMapping.toDomain(account_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_accountService.get(#account_account_id),'iBizBusinessCentral-Account_account-Remove')")
    @ApiOperation(value = "删除科目", tags = {"科目" },  notes = "删除科目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_accounts/{account_account_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_account_id") Long account_account_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_accountService.remove(account_account_id));
    }

    @PreAuthorize("hasPermission(this.account_accountService.getAccountAccountByIds(#ids),'iBizBusinessCentral-Account_account-Remove')")
    @ApiOperation(value = "批量删除科目", tags = {"科目" },  notes = "批量删除科目")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_accounts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_accountService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_accountMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_account-Get')")
    @ApiOperation(value = "获取科目", tags = {"科目" },  notes = "获取科目")
	@RequestMapping(method = RequestMethod.GET, value = "/account_accounts/{account_account_id}")
    public ResponseEntity<Account_accountDTO> get(@PathVariable("account_account_id") Long account_account_id) {
        Account_account domain = account_accountService.get(account_account_id);
        Account_accountDTO dto = account_accountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取科目草稿", tags = {"科目" },  notes = "获取科目草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_accounts/getdraft")
    public ResponseEntity<Account_accountDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_accountMapping.toDto(account_accountService.getDraft(new Account_account())));
    }

    @ApiOperation(value = "检查科目", tags = {"科目" },  notes = "检查科目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_accounts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_accountDTO account_accountdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_accountService.checkKey(account_accountMapping.toDomain(account_accountdto)));
    }

    @PreAuthorize("hasPermission(this.account_accountMapping.toDomain(#account_accountdto),'iBizBusinessCentral-Account_account-Save')")
    @ApiOperation(value = "保存科目", tags = {"科目" },  notes = "保存科目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_accounts/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_accountDTO account_accountdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_accountService.save(account_accountMapping.toDomain(account_accountdto)));
    }

    @PreAuthorize("hasPermission(this.account_accountMapping.toDomain(#account_accountdtos),'iBizBusinessCentral-Account_account-Save')")
    @ApiOperation(value = "批量保存科目", tags = {"科目" },  notes = "批量保存科目")
	@RequestMapping(method = RequestMethod.POST, value = "/account_accounts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_accountDTO> account_accountdtos) {
        account_accountService.saveBatch(account_accountMapping.toDomain(account_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_account-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_account-Get')")
	@ApiOperation(value = "获取数据集", tags = {"科目" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_accounts/fetchdefault")
	public ResponseEntity<List<Account_accountDTO>> fetchDefault(Account_accountSearchContext context) {
        Page<Account_account> domains = account_accountService.searchDefault(context) ;
        List<Account_accountDTO> list = account_accountMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_account-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_account-Get')")
	@ApiOperation(value = "查询数据集", tags = {"科目" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_accounts/searchdefault")
	public ResponseEntity<Page<Account_accountDTO>> searchDefault(@RequestBody Account_accountSearchContext context) {
        Page<Account_account> domains = account_accountService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_accountMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


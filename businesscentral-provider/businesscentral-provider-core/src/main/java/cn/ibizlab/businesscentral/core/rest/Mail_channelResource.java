package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channelService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_channelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"讨论频道" })
@RestController("Core-mail_channel")
@RequestMapping("")
public class Mail_channelResource {

    @Autowired
    public IMail_channelService mail_channelService;

    @Autowired
    @Lazy
    public Mail_channelMapping mail_channelMapping;

    @PreAuthorize("hasPermission(this.mail_channelMapping.toDomain(#mail_channeldto),'iBizBusinessCentral-Mail_channel-Create')")
    @ApiOperation(value = "新建讨论频道", tags = {"讨论频道" },  notes = "新建讨论频道")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channels")
    public ResponseEntity<Mail_channelDTO> create(@Validated @RequestBody Mail_channelDTO mail_channeldto) {
        Mail_channel domain = mail_channelMapping.toDomain(mail_channeldto);
		mail_channelService.create(domain);
        Mail_channelDTO dto = mail_channelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_channelMapping.toDomain(#mail_channeldtos),'iBizBusinessCentral-Mail_channel-Create')")
    @ApiOperation(value = "批量新建讨论频道", tags = {"讨论频道" },  notes = "批量新建讨论频道")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_channelDTO> mail_channeldtos) {
        mail_channelService.createBatch(mail_channelMapping.toDomain(mail_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_channel" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_channelService.get(#mail_channel_id),'iBizBusinessCentral-Mail_channel-Update')")
    @ApiOperation(value = "更新讨论频道", tags = {"讨论频道" },  notes = "更新讨论频道")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_channels/{mail_channel_id}")
    public ResponseEntity<Mail_channelDTO> update(@PathVariable("mail_channel_id") Long mail_channel_id, @RequestBody Mail_channelDTO mail_channeldto) {
		Mail_channel domain  = mail_channelMapping.toDomain(mail_channeldto);
        domain .setId(mail_channel_id);
		mail_channelService.update(domain );
		Mail_channelDTO dto = mail_channelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_channelService.getMailChannelByEntities(this.mail_channelMapping.toDomain(#mail_channeldtos)),'iBizBusinessCentral-Mail_channel-Update')")
    @ApiOperation(value = "批量更新讨论频道", tags = {"讨论频道" },  notes = "批量更新讨论频道")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_channels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_channelDTO> mail_channeldtos) {
        mail_channelService.updateBatch(mail_channelMapping.toDomain(mail_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_channelService.get(#mail_channel_id),'iBizBusinessCentral-Mail_channel-Remove')")
    @ApiOperation(value = "删除讨论频道", tags = {"讨论频道" },  notes = "删除讨论频道")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_channels/{mail_channel_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_channel_id") Long mail_channel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_channelService.remove(mail_channel_id));
    }

    @PreAuthorize("hasPermission(this.mail_channelService.getMailChannelByIds(#ids),'iBizBusinessCentral-Mail_channel-Remove')")
    @ApiOperation(value = "批量删除讨论频道", tags = {"讨论频道" },  notes = "批量删除讨论频道")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_channels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_channelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_channelMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_channel-Get')")
    @ApiOperation(value = "获取讨论频道", tags = {"讨论频道" },  notes = "获取讨论频道")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_channels/{mail_channel_id}")
    public ResponseEntity<Mail_channelDTO> get(@PathVariable("mail_channel_id") Long mail_channel_id) {
        Mail_channel domain = mail_channelService.get(mail_channel_id);
        Mail_channelDTO dto = mail_channelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取讨论频道草稿", tags = {"讨论频道" },  notes = "获取讨论频道草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_channels/getdraft")
    public ResponseEntity<Mail_channelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_channelMapping.toDto(mail_channelService.getDraft(new Mail_channel())));
    }

    @ApiOperation(value = "检查讨论频道", tags = {"讨论频道" },  notes = "检查讨论频道")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_channelDTO mail_channeldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_channelService.checkKey(mail_channelMapping.toDomain(mail_channeldto)));
    }

    @PreAuthorize("hasPermission(this.mail_channelMapping.toDomain(#mail_channeldto),'iBizBusinessCentral-Mail_channel-Save')")
    @ApiOperation(value = "保存讨论频道", tags = {"讨论频道" },  notes = "保存讨论频道")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channels/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_channelDTO mail_channeldto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_channelService.save(mail_channelMapping.toDomain(mail_channeldto)));
    }

    @PreAuthorize("hasPermission(this.mail_channelMapping.toDomain(#mail_channeldtos),'iBizBusinessCentral-Mail_channel-Save')")
    @ApiOperation(value = "批量保存讨论频道", tags = {"讨论频道" },  notes = "批量保存讨论频道")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_channelDTO> mail_channeldtos) {
        mail_channelService.saveBatch(mail_channelMapping.toDomain(mail_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_channel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_channel-Get')")
	@ApiOperation(value = "获取数据集", tags = {"讨论频道" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_channels/fetchdefault")
	public ResponseEntity<List<Mail_channelDTO>> fetchDefault(Mail_channelSearchContext context) {
        Page<Mail_channel> domains = mail_channelService.searchDefault(context) ;
        List<Mail_channelDTO> list = mail_channelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_channel-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_channel-Get')")
	@ApiOperation(value = "查询数据集", tags = {"讨论频道" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_channels/searchdefault")
	public ResponseEntity<Page<Mail_channelDTO>> searchDefault(@RequestBody Mail_channelSearchContext context) {
        Page<Mail_channel> domains = mail_channelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_channelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_cash_rounding;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_cash_roundingService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_cash_roundingSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"帐户现金舍入" })
@RestController("Core-account_cash_rounding")
@RequestMapping("")
public class Account_cash_roundingResource {

    @Autowired
    public IAccount_cash_roundingService account_cash_roundingService;

    @Autowired
    @Lazy
    public Account_cash_roundingMapping account_cash_roundingMapping;

    @PreAuthorize("hasPermission(this.account_cash_roundingMapping.toDomain(#account_cash_roundingdto),'iBizBusinessCentral-Account_cash_rounding-Create')")
    @ApiOperation(value = "新建帐户现金舍入", tags = {"帐户现金舍入" },  notes = "新建帐户现金舍入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings")
    public ResponseEntity<Account_cash_roundingDTO> create(@Validated @RequestBody Account_cash_roundingDTO account_cash_roundingdto) {
        Account_cash_rounding domain = account_cash_roundingMapping.toDomain(account_cash_roundingdto);
		account_cash_roundingService.create(domain);
        Account_cash_roundingDTO dto = account_cash_roundingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_cash_roundingMapping.toDomain(#account_cash_roundingdtos),'iBizBusinessCentral-Account_cash_rounding-Create')")
    @ApiOperation(value = "批量新建帐户现金舍入", tags = {"帐户现金舍入" },  notes = "批量新建帐户现金舍入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_cash_roundingDTO> account_cash_roundingdtos) {
        account_cash_roundingService.createBatch(account_cash_roundingMapping.toDomain(account_cash_roundingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_cash_rounding" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_cash_roundingService.get(#account_cash_rounding_id),'iBizBusinessCentral-Account_cash_rounding-Update')")
    @ApiOperation(value = "更新帐户现金舍入", tags = {"帐户现金舍入" },  notes = "更新帐户现金舍入")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_cash_roundings/{account_cash_rounding_id}")
    public ResponseEntity<Account_cash_roundingDTO> update(@PathVariable("account_cash_rounding_id") Long account_cash_rounding_id, @RequestBody Account_cash_roundingDTO account_cash_roundingdto) {
		Account_cash_rounding domain  = account_cash_roundingMapping.toDomain(account_cash_roundingdto);
        domain .setId(account_cash_rounding_id);
		account_cash_roundingService.update(domain );
		Account_cash_roundingDTO dto = account_cash_roundingMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_cash_roundingService.getAccountCashRoundingByEntities(this.account_cash_roundingMapping.toDomain(#account_cash_roundingdtos)),'iBizBusinessCentral-Account_cash_rounding-Update')")
    @ApiOperation(value = "批量更新帐户现金舍入", tags = {"帐户现金舍入" },  notes = "批量更新帐户现金舍入")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_cash_roundings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_cash_roundingDTO> account_cash_roundingdtos) {
        account_cash_roundingService.updateBatch(account_cash_roundingMapping.toDomain(account_cash_roundingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_cash_roundingService.get(#account_cash_rounding_id),'iBizBusinessCentral-Account_cash_rounding-Remove')")
    @ApiOperation(value = "删除帐户现金舍入", tags = {"帐户现金舍入" },  notes = "删除帐户现金舍入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_cash_roundings/{account_cash_rounding_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_cash_rounding_id") Long account_cash_rounding_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_cash_roundingService.remove(account_cash_rounding_id));
    }

    @PreAuthorize("hasPermission(this.account_cash_roundingService.getAccountCashRoundingByIds(#ids),'iBizBusinessCentral-Account_cash_rounding-Remove')")
    @ApiOperation(value = "批量删除帐户现金舍入", tags = {"帐户现金舍入" },  notes = "批量删除帐户现金舍入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_cash_roundings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_cash_roundingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_cash_roundingMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_cash_rounding-Get')")
    @ApiOperation(value = "获取帐户现金舍入", tags = {"帐户现金舍入" },  notes = "获取帐户现金舍入")
	@RequestMapping(method = RequestMethod.GET, value = "/account_cash_roundings/{account_cash_rounding_id}")
    public ResponseEntity<Account_cash_roundingDTO> get(@PathVariable("account_cash_rounding_id") Long account_cash_rounding_id) {
        Account_cash_rounding domain = account_cash_roundingService.get(account_cash_rounding_id);
        Account_cash_roundingDTO dto = account_cash_roundingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取帐户现金舍入草稿", tags = {"帐户现金舍入" },  notes = "获取帐户现金舍入草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_cash_roundings/getdraft")
    public ResponseEntity<Account_cash_roundingDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_cash_roundingMapping.toDto(account_cash_roundingService.getDraft(new Account_cash_rounding())));
    }

    @ApiOperation(value = "检查帐户现金舍入", tags = {"帐户现金舍入" },  notes = "检查帐户现金舍入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_cash_roundingDTO account_cash_roundingdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_cash_roundingService.checkKey(account_cash_roundingMapping.toDomain(account_cash_roundingdto)));
    }

    @PreAuthorize("hasPermission(this.account_cash_roundingMapping.toDomain(#account_cash_roundingdto),'iBizBusinessCentral-Account_cash_rounding-Save')")
    @ApiOperation(value = "保存帐户现金舍入", tags = {"帐户现金舍入" },  notes = "保存帐户现金舍入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_cash_roundingDTO account_cash_roundingdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_cash_roundingService.save(account_cash_roundingMapping.toDomain(account_cash_roundingdto)));
    }

    @PreAuthorize("hasPermission(this.account_cash_roundingMapping.toDomain(#account_cash_roundingdtos),'iBizBusinessCentral-Account_cash_rounding-Save')")
    @ApiOperation(value = "批量保存帐户现金舍入", tags = {"帐户现金舍入" },  notes = "批量保存帐户现金舍入")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_cash_roundingDTO> account_cash_roundingdtos) {
        account_cash_roundingService.saveBatch(account_cash_roundingMapping.toDomain(account_cash_roundingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_cash_rounding-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_cash_rounding-Get')")
	@ApiOperation(value = "获取数据集", tags = {"帐户现金舍入" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_cash_roundings/fetchdefault")
	public ResponseEntity<List<Account_cash_roundingDTO>> fetchDefault(Account_cash_roundingSearchContext context) {
        Page<Account_cash_rounding> domains = account_cash_roundingService.searchDefault(context) ;
        List<Account_cash_roundingDTO> list = account_cash_roundingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_cash_rounding-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_cash_rounding-Get')")
	@ApiOperation(value = "查询数据集", tags = {"帐户现金舍入" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_cash_roundings/searchdefault")
	public ResponseEntity<Page<Account_cash_roundingDTO>> searchDefault(@RequestBody Account_cash_roundingSearchContext context) {
        Page<Account_cash_rounding> domains = account_cash_roundingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_cash_roundingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_leadService;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_leadSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"线索/商机" })
@RestController("Core-crm_lead")
@RequestMapping("")
public class Crm_leadResource {

    @Autowired
    public ICrm_leadService crm_leadService;

    @Autowired
    @Lazy
    public Crm_leadMapping crm_leadMapping;

    @PreAuthorize("hasPermission(this.crm_leadMapping.toDomain(#crm_leaddto),'iBizBusinessCentral-Crm_lead-Create')")
    @ApiOperation(value = "新建线索/商机", tags = {"线索/商机" },  notes = "新建线索/商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads")
    public ResponseEntity<Crm_leadDTO> create(@Validated @RequestBody Crm_leadDTO crm_leaddto) {
        Crm_lead domain = crm_leadMapping.toDomain(crm_leaddto);
		crm_leadService.create(domain);
        Crm_leadDTO dto = crm_leadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_leadMapping.toDomain(#crm_leaddtos),'iBizBusinessCentral-Crm_lead-Create')")
    @ApiOperation(value = "批量新建线索/商机", tags = {"线索/商机" },  notes = "批量新建线索/商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        crm_leadService.createBatch(crm_leadMapping.toDomain(crm_leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "crm_lead" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.crm_leadService.get(#crm_lead_id),'iBizBusinessCentral-Crm_lead-Update')")
    @ApiOperation(value = "更新线索/商机", tags = {"线索/商机" },  notes = "更新线索/商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/{crm_lead_id}")
    public ResponseEntity<Crm_leadDTO> update(@PathVariable("crm_lead_id") Long crm_lead_id, @RequestBody Crm_leadDTO crm_leaddto) {
		Crm_lead domain  = crm_leadMapping.toDomain(crm_leaddto);
        domain .setId(crm_lead_id);
		crm_leadService.update(domain );
		Crm_leadDTO dto = crm_leadMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_leadService.getCrmLeadByEntities(this.crm_leadMapping.toDomain(#crm_leaddtos)),'iBizBusinessCentral-Crm_lead-Update')")
    @ApiOperation(value = "批量更新线索/商机", tags = {"线索/商机" },  notes = "批量更新线索/商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        crm_leadService.updateBatch(crm_leadMapping.toDomain(crm_leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.crm_leadService.get(#crm_lead_id),'iBizBusinessCentral-Crm_lead-Remove')")
    @ApiOperation(value = "删除线索/商机", tags = {"线索/商机" },  notes = "删除线索/商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/{crm_lead_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_id") Long crm_lead_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_leadService.remove(crm_lead_id));
    }

    @PreAuthorize("hasPermission(this.crm_leadService.getCrmLeadByIds(#ids),'iBizBusinessCentral-Crm_lead-Remove')")
    @ApiOperation(value = "批量删除线索/商机", tags = {"线索/商机" },  notes = "批量删除线索/商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        crm_leadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.crm_leadMapping.toDomain(returnObject.body),'iBizBusinessCentral-Crm_lead-Get')")
    @ApiOperation(value = "获取线索/商机", tags = {"线索/商机" },  notes = "获取线索/商机")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_leads/{crm_lead_id}")
    public ResponseEntity<Crm_leadDTO> get(@PathVariable("crm_lead_id") Long crm_lead_id) {
        Crm_lead domain = crm_leadService.get(crm_lead_id);
        Crm_leadDTO dto = crm_leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取线索/商机草稿", tags = {"线索/商机" },  notes = "获取线索/商机草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_leads/getdraft")
    public ResponseEntity<Crm_leadDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_leadMapping.toDto(crm_leadService.getDraft(new Crm_lead())));
    }

    @ApiOperation(value = "检查线索/商机", tags = {"线索/商机" },  notes = "检查线索/商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_leadDTO crm_leaddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_leadService.checkKey(crm_leadMapping.toDomain(crm_leaddto)));
    }

    @PreAuthorize("hasPermission(this.crm_leadMapping.toDomain(#crm_leaddto),'iBizBusinessCentral-Crm_lead-Save')")
    @ApiOperation(value = "保存线索/商机", tags = {"线索/商机" },  notes = "保存线索/商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_leadDTO crm_leaddto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_leadService.save(crm_leadMapping.toDomain(crm_leaddto)));
    }

    @PreAuthorize("hasPermission(this.crm_leadMapping.toDomain(#crm_leaddtos),'iBizBusinessCentral-Crm_lead-Save')")
    @ApiOperation(value = "批量保存线索/商机", tags = {"线索/商机" },  notes = "批量保存线索/商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        crm_leadService.saveBatch(crm_leadMapping.toDomain(crm_leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead-Get')")
	@ApiOperation(value = "获取数据集", tags = {"线索/商机" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/crm_leads/fetchdefault")
	public ResponseEntity<List<Crm_leadDTO>> fetchDefault(Crm_leadSearchContext context) {
        Page<Crm_lead> domains = crm_leadService.searchDefault(context) ;
        List<Crm_leadDTO> list = crm_leadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead-Get')")
	@ApiOperation(value = "查询数据集", tags = {"线索/商机" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/crm_leads/searchdefault")
	public ResponseEntity<Page<Crm_leadDTO>> searchDefault(@RequestBody Crm_leadSearchContext context) {
        Page<Crm_lead> domains = crm_leadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


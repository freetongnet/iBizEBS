package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_production_lotSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"批次/序列号" })
@RestController("Core-stock_production_lot")
@RequestMapping("")
public class Stock_production_lotResource {

    @Autowired
    public IStock_production_lotService stock_production_lotService;

    @Autowired
    @Lazy
    public Stock_production_lotMapping stock_production_lotMapping;

    @PreAuthorize("hasPermission(this.stock_production_lotMapping.toDomain(#stock_production_lotdto),'iBizBusinessCentral-Stock_production_lot-Create')")
    @ApiOperation(value = "新建批次/序列号", tags = {"批次/序列号" },  notes = "新建批次/序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots")
    public ResponseEntity<Stock_production_lotDTO> create(@Validated @RequestBody Stock_production_lotDTO stock_production_lotdto) {
        Stock_production_lot domain = stock_production_lotMapping.toDomain(stock_production_lotdto);
		stock_production_lotService.create(domain);
        Stock_production_lotDTO dto = stock_production_lotMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_production_lotMapping.toDomain(#stock_production_lotdtos),'iBizBusinessCentral-Stock_production_lot-Create')")
    @ApiOperation(value = "批量新建批次/序列号", tags = {"批次/序列号" },  notes = "批量新建批次/序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_production_lotDTO> stock_production_lotdtos) {
        stock_production_lotService.createBatch(stock_production_lotMapping.toDomain(stock_production_lotdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_production_lot" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_production_lotService.get(#stock_production_lot_id),'iBizBusinessCentral-Stock_production_lot-Update')")
    @ApiOperation(value = "更新批次/序列号", tags = {"批次/序列号" },  notes = "更新批次/序列号")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_production_lots/{stock_production_lot_id}")
    public ResponseEntity<Stock_production_lotDTO> update(@PathVariable("stock_production_lot_id") Long stock_production_lot_id, @RequestBody Stock_production_lotDTO stock_production_lotdto) {
		Stock_production_lot domain  = stock_production_lotMapping.toDomain(stock_production_lotdto);
        domain .setId(stock_production_lot_id);
		stock_production_lotService.update(domain );
		Stock_production_lotDTO dto = stock_production_lotMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_production_lotService.getStockProductionLotByEntities(this.stock_production_lotMapping.toDomain(#stock_production_lotdtos)),'iBizBusinessCentral-Stock_production_lot-Update')")
    @ApiOperation(value = "批量更新批次/序列号", tags = {"批次/序列号" },  notes = "批量更新批次/序列号")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_production_lots/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_production_lotDTO> stock_production_lotdtos) {
        stock_production_lotService.updateBatch(stock_production_lotMapping.toDomain(stock_production_lotdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_production_lotService.get(#stock_production_lot_id),'iBizBusinessCentral-Stock_production_lot-Remove')")
    @ApiOperation(value = "删除批次/序列号", tags = {"批次/序列号" },  notes = "删除批次/序列号")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_production_lots/{stock_production_lot_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_production_lot_id") Long stock_production_lot_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_production_lotService.remove(stock_production_lot_id));
    }

    @PreAuthorize("hasPermission(this.stock_production_lotService.getStockProductionLotByIds(#ids),'iBizBusinessCentral-Stock_production_lot-Remove')")
    @ApiOperation(value = "批量删除批次/序列号", tags = {"批次/序列号" },  notes = "批量删除批次/序列号")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_production_lots/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_production_lotService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_production_lotMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_production_lot-Get')")
    @ApiOperation(value = "获取批次/序列号", tags = {"批次/序列号" },  notes = "获取批次/序列号")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_production_lots/{stock_production_lot_id}")
    public ResponseEntity<Stock_production_lotDTO> get(@PathVariable("stock_production_lot_id") Long stock_production_lot_id) {
        Stock_production_lot domain = stock_production_lotService.get(stock_production_lot_id);
        Stock_production_lotDTO dto = stock_production_lotMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取批次/序列号草稿", tags = {"批次/序列号" },  notes = "获取批次/序列号草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_production_lots/getdraft")
    public ResponseEntity<Stock_production_lotDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_production_lotMapping.toDto(stock_production_lotService.getDraft(new Stock_production_lot())));
    }

    @ApiOperation(value = "检查批次/序列号", tags = {"批次/序列号" },  notes = "检查批次/序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_production_lotDTO stock_production_lotdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_production_lotService.checkKey(stock_production_lotMapping.toDomain(stock_production_lotdto)));
    }

    @PreAuthorize("hasPermission(this.stock_production_lotMapping.toDomain(#stock_production_lotdto),'iBizBusinessCentral-Stock_production_lot-Save')")
    @ApiOperation(value = "保存批次/序列号", tags = {"批次/序列号" },  notes = "保存批次/序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_production_lotDTO stock_production_lotdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_production_lotService.save(stock_production_lotMapping.toDomain(stock_production_lotdto)));
    }

    @PreAuthorize("hasPermission(this.stock_production_lotMapping.toDomain(#stock_production_lotdtos),'iBizBusinessCentral-Stock_production_lot-Save')")
    @ApiOperation(value = "批量保存批次/序列号", tags = {"批次/序列号" },  notes = "批量保存批次/序列号")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_production_lots/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_production_lotDTO> stock_production_lotdtos) {
        stock_production_lotService.saveBatch(stock_production_lotMapping.toDomain(stock_production_lotdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_production_lot-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_production_lot-Get')")
	@ApiOperation(value = "获取数据集", tags = {"批次/序列号" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_production_lots/fetchdefault")
	public ResponseEntity<List<Stock_production_lotDTO>> fetchDefault(Stock_production_lotSearchContext context) {
        Page<Stock_production_lot> domains = stock_production_lotService.searchDefault(context) ;
        List<Stock_production_lotDTO> list = stock_production_lotMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_production_lot-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_production_lot-Get')")
	@ApiOperation(value = "查询数据集", tags = {"批次/序列号" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_production_lots/searchdefault")
	public ResponseEntity<Page<Stock_production_lotDTO>> searchDefault(@RequestBody Stock_production_lotSearchContext context) {
        Page<Stock_production_lot> domains = stock_production_lotService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_production_lotMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


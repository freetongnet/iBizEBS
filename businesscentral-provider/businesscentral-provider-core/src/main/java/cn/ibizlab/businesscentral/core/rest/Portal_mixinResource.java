package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.businesscentral.core.odoo_portal.service.IPortal_mixinService;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"门户Mixin" })
@RestController("Core-portal_mixin")
@RequestMapping("")
public class Portal_mixinResource {

    @Autowired
    public IPortal_mixinService portal_mixinService;

    @Autowired
    @Lazy
    public Portal_mixinMapping portal_mixinMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-Create-all')")
    @ApiOperation(value = "新建门户Mixin", tags = {"门户Mixin" },  notes = "新建门户Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_mixins")
    public ResponseEntity<Portal_mixinDTO> create(@Validated @RequestBody Portal_mixinDTO portal_mixindto) {
        Portal_mixin domain = portal_mixinMapping.toDomain(portal_mixindto);
		portal_mixinService.create(domain);
        Portal_mixinDTO dto = portal_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-Create-all')")
    @ApiOperation(value = "批量新建门户Mixin", tags = {"门户Mixin" },  notes = "批量新建门户Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Portal_mixinDTO> portal_mixindtos) {
        portal_mixinService.createBatch(portal_mixinMapping.toDomain(portal_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-Update-all')")
    @ApiOperation(value = "更新门户Mixin", tags = {"门户Mixin" },  notes = "更新门户Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_mixins/{portal_mixin_id}")
    public ResponseEntity<Portal_mixinDTO> update(@PathVariable("portal_mixin_id") Long portal_mixin_id, @RequestBody Portal_mixinDTO portal_mixindto) {
		Portal_mixin domain  = portal_mixinMapping.toDomain(portal_mixindto);
        domain .setId(portal_mixin_id);
		portal_mixinService.update(domain );
		Portal_mixinDTO dto = portal_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-Update-all')")
    @ApiOperation(value = "批量更新门户Mixin", tags = {"门户Mixin" },  notes = "批量更新门户Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_mixinDTO> portal_mixindtos) {
        portal_mixinService.updateBatch(portal_mixinMapping.toDomain(portal_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-Remove-all')")
    @ApiOperation(value = "删除门户Mixin", tags = {"门户Mixin" },  notes = "删除门户Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_mixins/{portal_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("portal_mixin_id") Long portal_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(portal_mixinService.remove(portal_mixin_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-Remove-all')")
    @ApiOperation(value = "批量删除门户Mixin", tags = {"门户Mixin" },  notes = "批量删除门户Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        portal_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-Get-all')")
    @ApiOperation(value = "获取门户Mixin", tags = {"门户Mixin" },  notes = "获取门户Mixin")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_mixins/{portal_mixin_id}")
    public ResponseEntity<Portal_mixinDTO> get(@PathVariable("portal_mixin_id") Long portal_mixin_id) {
        Portal_mixin domain = portal_mixinService.get(portal_mixin_id);
        Portal_mixinDTO dto = portal_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取门户Mixin草稿", tags = {"门户Mixin" },  notes = "获取门户Mixin草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_mixins/getdraft")
    public ResponseEntity<Portal_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(portal_mixinMapping.toDto(portal_mixinService.getDraft(new Portal_mixin())));
    }

    @ApiOperation(value = "检查门户Mixin", tags = {"门户Mixin" },  notes = "检查门户Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Portal_mixinDTO portal_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(portal_mixinService.checkKey(portal_mixinMapping.toDomain(portal_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-Save-all')")
    @ApiOperation(value = "保存门户Mixin", tags = {"门户Mixin" },  notes = "保存门户Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Portal_mixinDTO portal_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(portal_mixinService.save(portal_mixinMapping.toDomain(portal_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-Save-all')")
    @ApiOperation(value = "批量保存门户Mixin", tags = {"门户Mixin" },  notes = "批量保存门户Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Portal_mixinDTO> portal_mixindtos) {
        portal_mixinService.saveBatch(portal_mixinMapping.toDomain(portal_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"门户Mixin" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/portal_mixins/fetchdefault")
	public ResponseEntity<List<Portal_mixinDTO>> fetchDefault(Portal_mixinSearchContext context) {
        Page<Portal_mixin> domains = portal_mixinService.searchDefault(context) ;
        List<Portal_mixinDTO> list = portal_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_mixin-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"门户Mixin" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/portal_mixins/searchdefault")
	public ResponseEntity<Page<Portal_mixinDTO>> searchDefault(@RequestBody Portal_mixinSearchContext context) {
        Page<Portal_mixin> domains = portal_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(portal_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


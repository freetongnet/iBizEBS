package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_statistics_report;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_statistics_reportService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_statistics_reportSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"群发邮件阶段" })
@RestController("Core-mail_statistics_report")
@RequestMapping("")
public class Mail_statistics_reportResource {

    @Autowired
    public IMail_statistics_reportService mail_statistics_reportService;

    @Autowired
    @Lazy
    public Mail_statistics_reportMapping mail_statistics_reportMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-Create-all')")
    @ApiOperation(value = "新建群发邮件阶段", tags = {"群发邮件阶段" },  notes = "新建群发邮件阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports")
    public ResponseEntity<Mail_statistics_reportDTO> create(@Validated @RequestBody Mail_statistics_reportDTO mail_statistics_reportdto) {
        Mail_statistics_report domain = mail_statistics_reportMapping.toDomain(mail_statistics_reportdto);
		mail_statistics_reportService.create(domain);
        Mail_statistics_reportDTO dto = mail_statistics_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-Create-all')")
    @ApiOperation(value = "批量新建群发邮件阶段", tags = {"群发邮件阶段" },  notes = "批量新建群发邮件阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_statistics_reportDTO> mail_statistics_reportdtos) {
        mail_statistics_reportService.createBatch(mail_statistics_reportMapping.toDomain(mail_statistics_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-Update-all')")
    @ApiOperation(value = "更新群发邮件阶段", tags = {"群发邮件阶段" },  notes = "更新群发邮件阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_statistics_reports/{mail_statistics_report_id}")
    public ResponseEntity<Mail_statistics_reportDTO> update(@PathVariable("mail_statistics_report_id") Long mail_statistics_report_id, @RequestBody Mail_statistics_reportDTO mail_statistics_reportdto) {
		Mail_statistics_report domain  = mail_statistics_reportMapping.toDomain(mail_statistics_reportdto);
        domain .setId(mail_statistics_report_id);
		mail_statistics_reportService.update(domain );
		Mail_statistics_reportDTO dto = mail_statistics_reportMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-Update-all')")
    @ApiOperation(value = "批量更新群发邮件阶段", tags = {"群发邮件阶段" },  notes = "批量更新群发邮件阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_statistics_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_statistics_reportDTO> mail_statistics_reportdtos) {
        mail_statistics_reportService.updateBatch(mail_statistics_reportMapping.toDomain(mail_statistics_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-Remove-all')")
    @ApiOperation(value = "删除群发邮件阶段", tags = {"群发邮件阶段" },  notes = "删除群发邮件阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_statistics_reports/{mail_statistics_report_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_statistics_report_id") Long mail_statistics_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_statistics_reportService.remove(mail_statistics_report_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-Remove-all')")
    @ApiOperation(value = "批量删除群发邮件阶段", tags = {"群发邮件阶段" },  notes = "批量删除群发邮件阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_statistics_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_statistics_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-Get-all')")
    @ApiOperation(value = "获取群发邮件阶段", tags = {"群发邮件阶段" },  notes = "获取群发邮件阶段")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_statistics_reports/{mail_statistics_report_id}")
    public ResponseEntity<Mail_statistics_reportDTO> get(@PathVariable("mail_statistics_report_id") Long mail_statistics_report_id) {
        Mail_statistics_report domain = mail_statistics_reportService.get(mail_statistics_report_id);
        Mail_statistics_reportDTO dto = mail_statistics_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取群发邮件阶段草稿", tags = {"群发邮件阶段" },  notes = "获取群发邮件阶段草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_statistics_reports/getdraft")
    public ResponseEntity<Mail_statistics_reportDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_statistics_reportMapping.toDto(mail_statistics_reportService.getDraft(new Mail_statistics_report())));
    }

    @ApiOperation(value = "检查群发邮件阶段", tags = {"群发邮件阶段" },  notes = "检查群发邮件阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_statistics_reportDTO mail_statistics_reportdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_statistics_reportService.checkKey(mail_statistics_reportMapping.toDomain(mail_statistics_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-Save-all')")
    @ApiOperation(value = "保存群发邮件阶段", tags = {"群发邮件阶段" },  notes = "保存群发邮件阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_statistics_reportDTO mail_statistics_reportdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_statistics_reportService.save(mail_statistics_reportMapping.toDomain(mail_statistics_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-Save-all')")
    @ApiOperation(value = "批量保存群发邮件阶段", tags = {"群发邮件阶段" },  notes = "批量保存群发邮件阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_statistics_reportDTO> mail_statistics_reportdtos) {
        mail_statistics_reportService.saveBatch(mail_statistics_reportMapping.toDomain(mail_statistics_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"群发邮件阶段" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_statistics_reports/fetchdefault")
	public ResponseEntity<List<Mail_statistics_reportDTO>> fetchDefault(Mail_statistics_reportSearchContext context) {
        Page<Mail_statistics_report> domains = mail_statistics_reportService.searchDefault(context) ;
        List<Mail_statistics_reportDTO> list = mail_statistics_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_statistics_report-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"群发邮件阶段" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_statistics_reports/searchdefault")
	public ResponseEntity<Page<Mail_statistics_reportDTO>> searchDefault(@RequestBody Mail_statistics_reportSearchContext context) {
        Page<Mail_statistics_report> domains = mail_statistics_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_statistics_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


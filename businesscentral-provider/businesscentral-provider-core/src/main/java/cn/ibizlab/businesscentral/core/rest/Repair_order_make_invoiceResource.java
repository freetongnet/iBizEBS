package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_order_make_invoiceService;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"创建批量发票 (修理)" })
@RestController("Core-repair_order_make_invoice")
@RequestMapping("")
public class Repair_order_make_invoiceResource {

    @Autowired
    public IRepair_order_make_invoiceService repair_order_make_invoiceService;

    @Autowired
    @Lazy
    public Repair_order_make_invoiceMapping repair_order_make_invoiceMapping;

    @PreAuthorize("hasPermission(this.repair_order_make_invoiceMapping.toDomain(#repair_order_make_invoicedto),'iBizBusinessCentral-Repair_order_make_invoice-Create')")
    @ApiOperation(value = "新建创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "新建创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices")
    public ResponseEntity<Repair_order_make_invoiceDTO> create(@Validated @RequestBody Repair_order_make_invoiceDTO repair_order_make_invoicedto) {
        Repair_order_make_invoice domain = repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedto);
		repair_order_make_invoiceService.create(domain);
        Repair_order_make_invoiceDTO dto = repair_order_make_invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_order_make_invoiceMapping.toDomain(#repair_order_make_invoicedtos),'iBizBusinessCentral-Repair_order_make_invoice-Create')")
    @ApiOperation(value = "批量新建创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "批量新建创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_order_make_invoiceDTO> repair_order_make_invoicedtos) {
        repair_order_make_invoiceService.createBatch(repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "repair_order_make_invoice" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.repair_order_make_invoiceService.get(#repair_order_make_invoice_id),'iBizBusinessCentral-Repair_order_make_invoice-Update')")
    @ApiOperation(value = "更新创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "更新创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_order_make_invoices/{repair_order_make_invoice_id}")
    public ResponseEntity<Repair_order_make_invoiceDTO> update(@PathVariable("repair_order_make_invoice_id") Long repair_order_make_invoice_id, @RequestBody Repair_order_make_invoiceDTO repair_order_make_invoicedto) {
		Repair_order_make_invoice domain  = repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedto);
        domain .setId(repair_order_make_invoice_id);
		repair_order_make_invoiceService.update(domain );
		Repair_order_make_invoiceDTO dto = repair_order_make_invoiceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.repair_order_make_invoiceService.getRepairOrderMakeInvoiceByEntities(this.repair_order_make_invoiceMapping.toDomain(#repair_order_make_invoicedtos)),'iBizBusinessCentral-Repair_order_make_invoice-Update')")
    @ApiOperation(value = "批量更新创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "批量更新创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_order_make_invoices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_order_make_invoiceDTO> repair_order_make_invoicedtos) {
        repair_order_make_invoiceService.updateBatch(repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.repair_order_make_invoiceService.get(#repair_order_make_invoice_id),'iBizBusinessCentral-Repair_order_make_invoice-Remove')")
    @ApiOperation(value = "删除创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "删除创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_order_make_invoices/{repair_order_make_invoice_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("repair_order_make_invoice_id") Long repair_order_make_invoice_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_order_make_invoiceService.remove(repair_order_make_invoice_id));
    }

    @PreAuthorize("hasPermission(this.repair_order_make_invoiceService.getRepairOrderMakeInvoiceByIds(#ids),'iBizBusinessCentral-Repair_order_make_invoice-Remove')")
    @ApiOperation(value = "批量删除创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "批量删除创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_order_make_invoices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        repair_order_make_invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.repair_order_make_invoiceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Repair_order_make_invoice-Get')")
    @ApiOperation(value = "获取创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "获取创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_order_make_invoices/{repair_order_make_invoice_id}")
    public ResponseEntity<Repair_order_make_invoiceDTO> get(@PathVariable("repair_order_make_invoice_id") Long repair_order_make_invoice_id) {
        Repair_order_make_invoice domain = repair_order_make_invoiceService.get(repair_order_make_invoice_id);
        Repair_order_make_invoiceDTO dto = repair_order_make_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取创建批量发票 (修理)草稿", tags = {"创建批量发票 (修理)" },  notes = "获取创建批量发票 (修理)草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_order_make_invoices/getdraft")
    public ResponseEntity<Repair_order_make_invoiceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(repair_order_make_invoiceMapping.toDto(repair_order_make_invoiceService.getDraft(new Repair_order_make_invoice())));
    }

    @ApiOperation(value = "检查创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "检查创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Repair_order_make_invoiceDTO repair_order_make_invoicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(repair_order_make_invoiceService.checkKey(repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedto)));
    }

    @PreAuthorize("hasPermission(this.repair_order_make_invoiceMapping.toDomain(#repair_order_make_invoicedto),'iBizBusinessCentral-Repair_order_make_invoice-Save')")
    @ApiOperation(value = "保存创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "保存创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices/save")
    public ResponseEntity<Boolean> save(@RequestBody Repair_order_make_invoiceDTO repair_order_make_invoicedto) {
        return ResponseEntity.status(HttpStatus.OK).body(repair_order_make_invoiceService.save(repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedto)));
    }

    @PreAuthorize("hasPermission(this.repair_order_make_invoiceMapping.toDomain(#repair_order_make_invoicedtos),'iBizBusinessCentral-Repair_order_make_invoice-Save')")
    @ApiOperation(value = "批量保存创建批量发票 (修理)", tags = {"创建批量发票 (修理)" },  notes = "批量保存创建批量发票 (修理)")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Repair_order_make_invoiceDTO> repair_order_make_invoicedtos) {
        repair_order_make_invoiceService.saveBatch(repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_order_make_invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_order_make_invoice-Get')")
	@ApiOperation(value = "获取数据集", tags = {"创建批量发票 (修理)" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/repair_order_make_invoices/fetchdefault")
	public ResponseEntity<List<Repair_order_make_invoiceDTO>> fetchDefault(Repair_order_make_invoiceSearchContext context) {
        Page<Repair_order_make_invoice> domains = repair_order_make_invoiceService.searchDefault(context) ;
        List<Repair_order_make_invoiceDTO> list = repair_order_make_invoiceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Repair_order_make_invoice-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Repair_order_make_invoice-Get')")
	@ApiOperation(value = "查询数据集", tags = {"创建批量发票 (修理)" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/repair_order_make_invoices/searchdefault")
	public ResponseEntity<Page<Repair_order_make_invoiceDTO>> searchDefault(@RequestBody Repair_order_make_invoiceSearchContext context) {
        Page<Repair_order_make_invoice> domains = repair_order_make_invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_order_make_invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_moveSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存移动" })
@RestController("Core-stock_move")
@RequestMapping("")
public class Stock_moveResource {

    @Autowired
    public IStock_moveService stock_moveService;

    @Autowired
    @Lazy
    public Stock_moveMapping stock_moveMapping;

    @PreAuthorize("hasPermission(this.stock_moveMapping.toDomain(#stock_movedto),'iBizBusinessCentral-Stock_move-Create')")
    @ApiOperation(value = "新建库存移动", tags = {"库存移动" },  notes = "新建库存移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_moves")
    public ResponseEntity<Stock_moveDTO> create(@Validated @RequestBody Stock_moveDTO stock_movedto) {
        Stock_move domain = stock_moveMapping.toDomain(stock_movedto);
		stock_moveService.create(domain);
        Stock_moveDTO dto = stock_moveMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_moveMapping.toDomain(#stock_movedtos),'iBizBusinessCentral-Stock_move-Create')")
    @ApiOperation(value = "批量新建库存移动", tags = {"库存移动" },  notes = "批量新建库存移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_moves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_moveDTO> stock_movedtos) {
        stock_moveService.createBatch(stock_moveMapping.toDomain(stock_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_move" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_moveService.get(#stock_move_id),'iBizBusinessCentral-Stock_move-Update')")
    @ApiOperation(value = "更新库存移动", tags = {"库存移动" },  notes = "更新库存移动")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_moves/{stock_move_id}")
    public ResponseEntity<Stock_moveDTO> update(@PathVariable("stock_move_id") Long stock_move_id, @RequestBody Stock_moveDTO stock_movedto) {
		Stock_move domain  = stock_moveMapping.toDomain(stock_movedto);
        domain .setId(stock_move_id);
		stock_moveService.update(domain );
		Stock_moveDTO dto = stock_moveMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_moveService.getStockMoveByEntities(this.stock_moveMapping.toDomain(#stock_movedtos)),'iBizBusinessCentral-Stock_move-Update')")
    @ApiOperation(value = "批量更新库存移动", tags = {"库存移动" },  notes = "批量更新库存移动")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_moves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_moveDTO> stock_movedtos) {
        stock_moveService.updateBatch(stock_moveMapping.toDomain(stock_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_moveService.get(#stock_move_id),'iBizBusinessCentral-Stock_move-Remove')")
    @ApiOperation(value = "删除库存移动", tags = {"库存移动" },  notes = "删除库存移动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_moves/{stock_move_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_move_id") Long stock_move_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_moveService.remove(stock_move_id));
    }

    @PreAuthorize("hasPermission(this.stock_moveService.getStockMoveByIds(#ids),'iBizBusinessCentral-Stock_move-Remove')")
    @ApiOperation(value = "批量删除库存移动", tags = {"库存移动" },  notes = "批量删除库存移动")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_moves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_moveService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_moveMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_move-Get')")
    @ApiOperation(value = "获取库存移动", tags = {"库存移动" },  notes = "获取库存移动")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_moves/{stock_move_id}")
    public ResponseEntity<Stock_moveDTO> get(@PathVariable("stock_move_id") Long stock_move_id) {
        Stock_move domain = stock_moveService.get(stock_move_id);
        Stock_moveDTO dto = stock_moveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存移动草稿", tags = {"库存移动" },  notes = "获取库存移动草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_moves/getdraft")
    public ResponseEntity<Stock_moveDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_moveMapping.toDto(stock_moveService.getDraft(new Stock_move())));
    }

    @ApiOperation(value = "检查库存移动", tags = {"库存移动" },  notes = "检查库存移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_moves/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_moveDTO stock_movedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_moveService.checkKey(stock_moveMapping.toDomain(stock_movedto)));
    }

    @PreAuthorize("hasPermission(this.stock_moveMapping.toDomain(#stock_movedto),'iBizBusinessCentral-Stock_move-Save')")
    @ApiOperation(value = "保存库存移动", tags = {"库存移动" },  notes = "保存库存移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_moves/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_moveDTO stock_movedto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_moveService.save(stock_moveMapping.toDomain(stock_movedto)));
    }

    @PreAuthorize("hasPermission(this.stock_moveMapping.toDomain(#stock_movedtos),'iBizBusinessCentral-Stock_move-Save')")
    @ApiOperation(value = "批量保存库存移动", tags = {"库存移动" },  notes = "批量保存库存移动")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_moves/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_moveDTO> stock_movedtos) {
        stock_moveService.saveBatch(stock_moveMapping.toDomain(stock_movedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_move-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_move-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存移动" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_moves/fetchdefault")
	public ResponseEntity<List<Stock_moveDTO>> fetchDefault(Stock_moveSearchContext context) {
        Page<Stock_move> domains = stock_moveService.searchDefault(context) ;
        List<Stock_moveDTO> list = stock_moveMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_move-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_move-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存移动" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_moves/searchdefault")
	public ResponseEntity<Page<Stock_moveDTO>> searchDefault(@RequestBody Stock_moveSearchContext context) {
        Page<Stock_move> domains = stock_moveService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_moveMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


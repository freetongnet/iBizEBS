package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mail_followers_mail_message_subtype_relDTO]
 */
@Data
public class Mail_followers_mail_message_subtype_relDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String id;

    /**
     * 属性 [MAIL_MESSAGE_SUBTYPE_ID]
     *
     */
    @JSONField(name = "mail_message_subtype_id")
    @JsonProperty("mail_message_subtype_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long mailMessageSubtypeId;

    /**
     * 属性 [MAIL_FOLLOWERS_ID]
     *
     */
    @JSONField(name = "mail_followers_id")
    @JsonProperty("mail_followers_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long mailFollowersId;


    /**
     * 设置 [MAIL_MESSAGE_SUBTYPE_ID]
     */
    public void setMailMessageSubtypeId(Long  mailMessageSubtypeId){
        this.mailMessageSubtypeId = mailMessageSubtypeId ;
        this.modify("mail_message_subtype_id",mailMessageSubtypeId);
    }

    /**
     * 设置 [MAIL_FOLLOWERS_ID]
     */
    public void setMailFollowersId(Long  mailFollowersId){
        this.mailFollowersId = mailFollowersId ;
        this.modify("mail_followers_id",mailFollowersId);
    }


}



package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_abstract_paymentService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_abstract_paymentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"包含在允许登记付款的模块之间共享的逻辑" })
@RestController("Core-account_abstract_payment")
@RequestMapping("")
public class Account_abstract_paymentResource {

    @Autowired
    public IAccount_abstract_paymentService account_abstract_paymentService;

    @Autowired
    @Lazy
    public Account_abstract_paymentMapping account_abstract_paymentMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-Create-all')")
    @ApiOperation(value = "新建包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "新建包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments")
    public ResponseEntity<Account_abstract_paymentDTO> create(@Validated @RequestBody Account_abstract_paymentDTO account_abstract_paymentdto) {
        Account_abstract_payment domain = account_abstract_paymentMapping.toDomain(account_abstract_paymentdto);
		account_abstract_paymentService.create(domain);
        Account_abstract_paymentDTO dto = account_abstract_paymentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-Create-all')")
    @ApiOperation(value = "批量新建包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "批量新建包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_abstract_paymentDTO> account_abstract_paymentdtos) {
        account_abstract_paymentService.createBatch(account_abstract_paymentMapping.toDomain(account_abstract_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-Update-all')")
    @ApiOperation(value = "更新包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "更新包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_abstract_payments/{account_abstract_payment_id}")
    public ResponseEntity<Account_abstract_paymentDTO> update(@PathVariable("account_abstract_payment_id") Long account_abstract_payment_id, @RequestBody Account_abstract_paymentDTO account_abstract_paymentdto) {
		Account_abstract_payment domain  = account_abstract_paymentMapping.toDomain(account_abstract_paymentdto);
        domain .setId(account_abstract_payment_id);
		account_abstract_paymentService.update(domain );
		Account_abstract_paymentDTO dto = account_abstract_paymentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-Update-all')")
    @ApiOperation(value = "批量更新包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "批量更新包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_abstract_payments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_abstract_paymentDTO> account_abstract_paymentdtos) {
        account_abstract_paymentService.updateBatch(account_abstract_paymentMapping.toDomain(account_abstract_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-Remove-all')")
    @ApiOperation(value = "删除包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "删除包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_abstract_payments/{account_abstract_payment_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_abstract_payment_id") Long account_abstract_payment_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_abstract_paymentService.remove(account_abstract_payment_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-Remove-all')")
    @ApiOperation(value = "批量删除包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "批量删除包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_abstract_payments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_abstract_paymentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-Get-all')")
    @ApiOperation(value = "获取包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "获取包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.GET, value = "/account_abstract_payments/{account_abstract_payment_id}")
    public ResponseEntity<Account_abstract_paymentDTO> get(@PathVariable("account_abstract_payment_id") Long account_abstract_payment_id) {
        Account_abstract_payment domain = account_abstract_paymentService.get(account_abstract_payment_id);
        Account_abstract_paymentDTO dto = account_abstract_paymentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取包含在允许登记付款的模块之间共享的逻辑草稿", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "获取包含在允许登记付款的模块之间共享的逻辑草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_abstract_payments/getdraft")
    public ResponseEntity<Account_abstract_paymentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_abstract_paymentMapping.toDto(account_abstract_paymentService.getDraft(new Account_abstract_payment())));
    }

    @ApiOperation(value = "检查包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "检查包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_abstract_paymentDTO account_abstract_paymentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_abstract_paymentService.checkKey(account_abstract_paymentMapping.toDomain(account_abstract_paymentdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-Save-all')")
    @ApiOperation(value = "保存包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "保存包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_abstract_paymentDTO account_abstract_paymentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_abstract_paymentService.save(account_abstract_paymentMapping.toDomain(account_abstract_paymentdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-Save-all')")
    @ApiOperation(value = "批量保存包含在允许登记付款的模块之间共享的逻辑", tags = {"包含在允许登记付款的模块之间共享的逻辑" },  notes = "批量保存包含在允许登记付款的模块之间共享的逻辑")
	@RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_abstract_paymentDTO> account_abstract_paymentdtos) {
        account_abstract_paymentService.saveBatch(account_abstract_paymentMapping.toDomain(account_abstract_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"包含在允许登记付款的模块之间共享的逻辑" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_abstract_payments/fetchdefault")
	public ResponseEntity<List<Account_abstract_paymentDTO>> fetchDefault(Account_abstract_paymentSearchContext context) {
        Page<Account_abstract_payment> domains = account_abstract_paymentService.searchDefault(context) ;
        List<Account_abstract_paymentDTO> list = account_abstract_paymentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_abstract_payment-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"包含在允许登记付款的模块之间共享的逻辑" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_abstract_payments/searchdefault")
	public ResponseEntity<Page<Account_abstract_paymentDTO>> searchDefault(@RequestBody Account_abstract_paymentSearchContext context) {
        Page<Account_abstract_payment> domains = account_abstract_paymentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_abstract_paymentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


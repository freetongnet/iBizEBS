package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_bank_statement_import_journal_creationDTO]
 */
@Data
public class Account_bank_statement_import_journal_creationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [OUTBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @JSONField(name = "outbound_payment_method_ids")
    @JsonProperty("outbound_payment_method_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String outboundPaymentMethodIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [TYPE_CONTROL_IDS]
     *
     */
    @JSONField(name = "type_control_ids")
    @JsonProperty("type_control_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String typeControlIds;

    /**
     * 属性 [INBOUND_PAYMENT_METHOD_IDS]
     *
     */
    @JSONField(name = "inbound_payment_method_ids")
    @JsonProperty("inbound_payment_method_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String inboundPaymentMethodIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [ACCOUNT_CONTROL_IDS]
     *
     */
    @JSONField(name = "account_control_ids")
    @JsonProperty("account_control_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String accountControlIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String aliasDomain;

    /**
     * 属性 [REFUND_SEQUENCE_ID]
     *
     */
    @JSONField(name = "refund_sequence_id")
    @JsonProperty("refund_sequence_id")
    private Integer refundSequenceId;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [BANK_ID]
     *
     */
    @JSONField(name = "bank_id")
    @JsonProperty("bank_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long bankId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [LOSS_ACCOUNT_ID]
     *
     */
    @JSONField(name = "loss_account_id")
    @JsonProperty("loss_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lossAccountId;

    /**
     * 属性 [DEFAULT_CREDIT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "default_credit_account_id")
    @JsonProperty("default_credit_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long defaultCreditAccountId;

    /**
     * 属性 [REFUND_SEQUENCE]
     *
     */
    @JSONField(name = "refund_sequence")
    @JsonProperty("refund_sequence")
    private Boolean refundSequence;

    /**
     * 属性 [AT_LEAST_ONE_OUTBOUND]
     *
     */
    @JSONField(name = "at_least_one_outbound")
    @JsonProperty("at_least_one_outbound")
    private Boolean atLeastOneOutbound;

    /**
     * 属性 [PROFIT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "profit_account_id")
    @JsonProperty("profit_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long profitAccountId;

    /**
     * 属性 [REFUND_SEQUENCE_NUMBER_NEXT]
     *
     */
    @JSONField(name = "refund_sequence_number_next")
    @JsonProperty("refund_sequence_number_next")
    private Integer refundSequenceNumberNext;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long aliasId;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    @NotBlank(message = "[简码]不允许为空!")
    @Size(min = 0, max = 5, message = "内容长度必须小于等于[5]")
    private String code;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String aliasName;

    /**
     * 属性 [BANK_ACCOUNT_ID]
     *
     */
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long bankAccountId;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [AT_LEAST_ONE_INBOUND]
     *
     */
    @JSONField(name = "at_least_one_inbound")
    @JsonProperty("at_least_one_inbound")
    private Boolean atLeastOneInbound;

    /**
     * 属性 [BANK_ACC_NUMBER]
     *
     */
    @JSONField(name = "bank_acc_number")
    @JsonProperty("bank_acc_number")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String bankAccNumber;

    /**
     * 属性 [SEQUENCE_NUMBER_NEXT]
     *
     */
    @JSONField(name = "sequence_number_next")
    @JsonProperty("sequence_number_next")
    private Integer sequenceNumberNext;

    /**
     * 属性 [COMPANY_PARTNER_ID]
     *
     */
    @JSONField(name = "company_partner_id")
    @JsonProperty("company_partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyPartnerId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[公司]不允许为空!")
    private Long companyId;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [DEFAULT_DEBIT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "default_debit_account_id")
    @JsonProperty("default_debit_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long defaultDebitAccountId;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [BANK_STATEMENTS_SOURCE]
     *
     */
    @JSONField(name = "bank_statements_source")
    @JsonProperty("bank_statements_source")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String bankStatementsSource;

    /**
     * 属性 [KANBAN_DASHBOARD]
     *
     */
    @JSONField(name = "kanban_dashboard")
    @JsonProperty("kanban_dashboard")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String kanbanDashboard;

    /**
     * 属性 [KANBAN_DASHBOARD_GRAPH]
     *
     */
    @JSONField(name = "kanban_dashboard_graph")
    @JsonProperty("kanban_dashboard_graph")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String kanbanDashboardGraph;

    /**
     * 属性 [SHOW_ON_DASHBOARD]
     *
     */
    @JSONField(name = "show_on_dashboard")
    @JsonProperty("show_on_dashboard")
    private Boolean showOnDashboard;

    /**
     * 属性 [BELONGS_TO_COMPANY]
     *
     */
    @JSONField(name = "belongs_to_company")
    @JsonProperty("belongs_to_company")
    private Boolean belongsToCompany;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[日记账名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    @NotBlank(message = "[类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String type;

    /**
     * 属性 [SEQUENCE_ID]
     *
     */
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    @NotNull(message = "[分录序列]不允许为空!")
    private Integer sequenceId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[日记账]不允许为空!")
    private Long journalId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Long  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }


}



package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_contact;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_contactService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_contactSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"群发邮件联系人" })
@RestController("Core-mail_mass_mailing_contact")
@RequestMapping("")
public class Mail_mass_mailing_contactResource {

    @Autowired
    public IMail_mass_mailing_contactService mail_mass_mailing_contactService;

    @Autowired
    @Lazy
    public Mail_mass_mailing_contactMapping mail_mass_mailing_contactMapping;

    @PreAuthorize("hasPermission(this.mail_mass_mailing_contactMapping.toDomain(#mail_mass_mailing_contactdto),'iBizBusinessCentral-Mail_mass_mailing_contact-Create')")
    @ApiOperation(value = "新建群发邮件联系人", tags = {"群发邮件联系人" },  notes = "新建群发邮件联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts")
    public ResponseEntity<Mail_mass_mailing_contactDTO> create(@Validated @RequestBody Mail_mass_mailing_contactDTO mail_mass_mailing_contactdto) {
        Mail_mass_mailing_contact domain = mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdto);
		mail_mass_mailing_contactService.create(domain);
        Mail_mass_mailing_contactDTO dto = mail_mass_mailing_contactMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_contactMapping.toDomain(#mail_mass_mailing_contactdtos),'iBizBusinessCentral-Mail_mass_mailing_contact-Create')")
    @ApiOperation(value = "批量新建群发邮件联系人", tags = {"群发邮件联系人" },  notes = "批量新建群发邮件联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_contactDTO> mail_mass_mailing_contactdtos) {
        mail_mass_mailing_contactService.createBatch(mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_mass_mailing_contact" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_mass_mailing_contactService.get(#mail_mass_mailing_contact_id),'iBizBusinessCentral-Mail_mass_mailing_contact-Update')")
    @ApiOperation(value = "更新群发邮件联系人", tags = {"群发邮件联系人" },  notes = "更新群发邮件联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_contacts/{mail_mass_mailing_contact_id}")
    public ResponseEntity<Mail_mass_mailing_contactDTO> update(@PathVariable("mail_mass_mailing_contact_id") Long mail_mass_mailing_contact_id, @RequestBody Mail_mass_mailing_contactDTO mail_mass_mailing_contactdto) {
		Mail_mass_mailing_contact domain  = mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdto);
        domain .setId(mail_mass_mailing_contact_id);
		mail_mass_mailing_contactService.update(domain );
		Mail_mass_mailing_contactDTO dto = mail_mass_mailing_contactMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_contactService.getMailMassMailingContactByEntities(this.mail_mass_mailing_contactMapping.toDomain(#mail_mass_mailing_contactdtos)),'iBizBusinessCentral-Mail_mass_mailing_contact-Update')")
    @ApiOperation(value = "批量更新群发邮件联系人", tags = {"群发邮件联系人" },  notes = "批量更新群发邮件联系人")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_contacts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_contactDTO> mail_mass_mailing_contactdtos) {
        mail_mass_mailing_contactService.updateBatch(mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_contactService.get(#mail_mass_mailing_contact_id),'iBizBusinessCentral-Mail_mass_mailing_contact-Remove')")
    @ApiOperation(value = "删除群发邮件联系人", tags = {"群发邮件联系人" },  notes = "删除群发邮件联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_contacts/{mail_mass_mailing_contact_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_contact_id") Long mail_mass_mailing_contact_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_contactService.remove(mail_mass_mailing_contact_id));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_contactService.getMailMassMailingContactByIds(#ids),'iBizBusinessCentral-Mail_mass_mailing_contact-Remove')")
    @ApiOperation(value = "批量删除群发邮件联系人", tags = {"群发邮件联系人" },  notes = "批量删除群发邮件联系人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_contacts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_mass_mailing_contactService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_mass_mailing_contactMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_mass_mailing_contact-Get')")
    @ApiOperation(value = "获取群发邮件联系人", tags = {"群发邮件联系人" },  notes = "获取群发邮件联系人")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_contacts/{mail_mass_mailing_contact_id}")
    public ResponseEntity<Mail_mass_mailing_contactDTO> get(@PathVariable("mail_mass_mailing_contact_id") Long mail_mass_mailing_contact_id) {
        Mail_mass_mailing_contact domain = mail_mass_mailing_contactService.get(mail_mass_mailing_contact_id);
        Mail_mass_mailing_contactDTO dto = mail_mass_mailing_contactMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取群发邮件联系人草稿", tags = {"群发邮件联系人" },  notes = "获取群发邮件联系人草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_contacts/getdraft")
    public ResponseEntity<Mail_mass_mailing_contactDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_contactMapping.toDto(mail_mass_mailing_contactService.getDraft(new Mail_mass_mailing_contact())));
    }

    @ApiOperation(value = "检查群发邮件联系人", tags = {"群发邮件联系人" },  notes = "检查群发邮件联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_mass_mailing_contactDTO mail_mass_mailing_contactdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_contactService.checkKey(mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_contactMapping.toDomain(#mail_mass_mailing_contactdto),'iBizBusinessCentral-Mail_mass_mailing_contact-Save')")
    @ApiOperation(value = "保存群发邮件联系人", tags = {"群发邮件联系人" },  notes = "保存群发邮件联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_mass_mailing_contactDTO mail_mass_mailing_contactdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_contactService.save(mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_contactMapping.toDomain(#mail_mass_mailing_contactdtos),'iBizBusinessCentral-Mail_mass_mailing_contact-Save')")
    @ApiOperation(value = "批量保存群发邮件联系人", tags = {"群发邮件联系人" },  notes = "批量保存群发邮件联系人")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_contacts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_mass_mailing_contactDTO> mail_mass_mailing_contactdtos) {
        mail_mass_mailing_contactService.saveBatch(mail_mass_mailing_contactMapping.toDomain(mail_mass_mailing_contactdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_contact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_contact-Get')")
	@ApiOperation(value = "获取数据集", tags = {"群发邮件联系人" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_contacts/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_contactDTO>> fetchDefault(Mail_mass_mailing_contactSearchContext context) {
        Page<Mail_mass_mailing_contact> domains = mail_mass_mailing_contactService.searchDefault(context) ;
        List<Mail_mass_mailing_contactDTO> list = mail_mass_mailing_contactMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_contact-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_contact-Get')")
	@ApiOperation(value = "查询数据集", tags = {"群发邮件联系人" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_mass_mailing_contacts/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_contactDTO>> searchDefault(@RequestBody Mail_mass_mailing_contactSearchContext context) {
        Page<Mail_mass_mailing_contact> domains = mail_mass_mailing_contactService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_contactMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


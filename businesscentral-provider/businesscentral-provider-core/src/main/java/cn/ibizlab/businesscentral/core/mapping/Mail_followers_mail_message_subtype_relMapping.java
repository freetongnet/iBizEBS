package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers_mail_message_subtype_rel;
import cn.ibizlab.businesscentral.core.dto.Mail_followers_mail_message_subtype_relDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMail_followers_mail_message_subtype_relMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_followers_mail_message_subtype_relMapping extends MappingBase<Mail_followers_mail_message_subtype_relDTO, Mail_followers_mail_message_subtype_rel> {


}


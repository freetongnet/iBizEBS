package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.businesscentral.core.dto.Hr_holidays_summary_deptDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreHr_holidays_summary_deptMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_holidays_summary_deptMapping extends MappingBase<Hr_holidays_summary_deptDTO, Hr_holidays_summary_dept> {


}


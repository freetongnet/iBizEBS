package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_template_attribute_exclusionService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品模板属性排除" })
@RestController("Core-product_template_attribute_exclusion")
@RequestMapping("")
public class Product_template_attribute_exclusionResource {

    @Autowired
    public IProduct_template_attribute_exclusionService product_template_attribute_exclusionService;

    @Autowired
    @Lazy
    public Product_template_attribute_exclusionMapping product_template_attribute_exclusionMapping;

    @PreAuthorize("hasPermission(this.product_template_attribute_exclusionMapping.toDomain(#product_template_attribute_exclusiondto),'iBizBusinessCentral-Product_template_attribute_exclusion-Create')")
    @ApiOperation(value = "新建产品模板属性排除", tags = {"产品模板属性排除" },  notes = "新建产品模板属性排除")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions")
    public ResponseEntity<Product_template_attribute_exclusionDTO> create(@Validated @RequestBody Product_template_attribute_exclusionDTO product_template_attribute_exclusiondto) {
        Product_template_attribute_exclusion domain = product_template_attribute_exclusionMapping.toDomain(product_template_attribute_exclusiondto);
		product_template_attribute_exclusionService.create(domain);
        Product_template_attribute_exclusionDTO dto = product_template_attribute_exclusionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_exclusionMapping.toDomain(#product_template_attribute_exclusiondtos),'iBizBusinessCentral-Product_template_attribute_exclusion-Create')")
    @ApiOperation(value = "批量新建产品模板属性排除", tags = {"产品模板属性排除" },  notes = "批量新建产品模板属性排除")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_template_attribute_exclusionDTO> product_template_attribute_exclusiondtos) {
        product_template_attribute_exclusionService.createBatch(product_template_attribute_exclusionMapping.toDomain(product_template_attribute_exclusiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_template_attribute_exclusion" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_template_attribute_exclusionService.get(#product_template_attribute_exclusion_id),'iBizBusinessCentral-Product_template_attribute_exclusion-Update')")
    @ApiOperation(value = "更新产品模板属性排除", tags = {"产品模板属性排除" },  notes = "更新产品模板属性排除")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_exclusions/{product_template_attribute_exclusion_id}")
    public ResponseEntity<Product_template_attribute_exclusionDTO> update(@PathVariable("product_template_attribute_exclusion_id") Long product_template_attribute_exclusion_id, @RequestBody Product_template_attribute_exclusionDTO product_template_attribute_exclusiondto) {
		Product_template_attribute_exclusion domain  = product_template_attribute_exclusionMapping.toDomain(product_template_attribute_exclusiondto);
        domain .setId(product_template_attribute_exclusion_id);
		product_template_attribute_exclusionService.update(domain );
		Product_template_attribute_exclusionDTO dto = product_template_attribute_exclusionMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_exclusionService.getProductTemplateAttributeExclusionByEntities(this.product_template_attribute_exclusionMapping.toDomain(#product_template_attribute_exclusiondtos)),'iBizBusinessCentral-Product_template_attribute_exclusion-Update')")
    @ApiOperation(value = "批量更新产品模板属性排除", tags = {"产品模板属性排除" },  notes = "批量更新产品模板属性排除")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_template_attribute_exclusions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_template_attribute_exclusionDTO> product_template_attribute_exclusiondtos) {
        product_template_attribute_exclusionService.updateBatch(product_template_attribute_exclusionMapping.toDomain(product_template_attribute_exclusiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_exclusionService.get(#product_template_attribute_exclusion_id),'iBizBusinessCentral-Product_template_attribute_exclusion-Remove')")
    @ApiOperation(value = "删除产品模板属性排除", tags = {"产品模板属性排除" },  notes = "删除产品模板属性排除")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_exclusions/{product_template_attribute_exclusion_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_template_attribute_exclusion_id") Long product_template_attribute_exclusion_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_exclusionService.remove(product_template_attribute_exclusion_id));
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_exclusionService.getProductTemplateAttributeExclusionByIds(#ids),'iBizBusinessCentral-Product_template_attribute_exclusion-Remove')")
    @ApiOperation(value = "批量删除产品模板属性排除", tags = {"产品模板属性排除" },  notes = "批量删除产品模板属性排除")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_template_attribute_exclusions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_template_attribute_exclusionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_template_attribute_exclusionMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_template_attribute_exclusion-Get')")
    @ApiOperation(value = "获取产品模板属性排除", tags = {"产品模板属性排除" },  notes = "获取产品模板属性排除")
	@RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_exclusions/{product_template_attribute_exclusion_id}")
    public ResponseEntity<Product_template_attribute_exclusionDTO> get(@PathVariable("product_template_attribute_exclusion_id") Long product_template_attribute_exclusion_id) {
        Product_template_attribute_exclusion domain = product_template_attribute_exclusionService.get(product_template_attribute_exclusion_id);
        Product_template_attribute_exclusionDTO dto = product_template_attribute_exclusionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品模板属性排除草稿", tags = {"产品模板属性排除" },  notes = "获取产品模板属性排除草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_template_attribute_exclusions/getdraft")
    public ResponseEntity<Product_template_attribute_exclusionDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_exclusionMapping.toDto(product_template_attribute_exclusionService.getDraft(new Product_template_attribute_exclusion())));
    }

    @ApiOperation(value = "检查产品模板属性排除", tags = {"产品模板属性排除" },  notes = "检查产品模板属性排除")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_template_attribute_exclusionDTO product_template_attribute_exclusiondto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_exclusionService.checkKey(product_template_attribute_exclusionMapping.toDomain(product_template_attribute_exclusiondto)));
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_exclusionMapping.toDomain(#product_template_attribute_exclusiondto),'iBizBusinessCentral-Product_template_attribute_exclusion-Save')")
    @ApiOperation(value = "保存产品模板属性排除", tags = {"产品模板属性排除" },  notes = "保存产品模板属性排除")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_template_attribute_exclusionDTO product_template_attribute_exclusiondto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_template_attribute_exclusionService.save(product_template_attribute_exclusionMapping.toDomain(product_template_attribute_exclusiondto)));
    }

    @PreAuthorize("hasPermission(this.product_template_attribute_exclusionMapping.toDomain(#product_template_attribute_exclusiondtos),'iBizBusinessCentral-Product_template_attribute_exclusion-Save')")
    @ApiOperation(value = "批量保存产品模板属性排除", tags = {"产品模板属性排除" },  notes = "批量保存产品模板属性排除")
	@RequestMapping(method = RequestMethod.POST, value = "/product_template_attribute_exclusions/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_template_attribute_exclusionDTO> product_template_attribute_exclusiondtos) {
        product_template_attribute_exclusionService.saveBatch(product_template_attribute_exclusionMapping.toDomain(product_template_attribute_exclusiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template_attribute_exclusion-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_template_attribute_exclusion-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品模板属性排除" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_template_attribute_exclusions/fetchdefault")
	public ResponseEntity<List<Product_template_attribute_exclusionDTO>> fetchDefault(Product_template_attribute_exclusionSearchContext context) {
        Page<Product_template_attribute_exclusion> domains = product_template_attribute_exclusionService.searchDefault(context) ;
        List<Product_template_attribute_exclusionDTO> list = product_template_attribute_exclusionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_template_attribute_exclusion-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_template_attribute_exclusion-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品模板属性排除" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_template_attribute_exclusions/searchdefault")
	public ResponseEntity<Page<Product_template_attribute_exclusionDTO>> searchDefault(@RequestBody Product_template_attribute_exclusionSearchContext context) {
        Page<Product_template_attribute_exclusion> domains = product_template_attribute_exclusionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_template_attribute_exclusionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Fleet_vehicle_log_contractDTO]
 */
@Data
public class Fleet_vehicle_log_contractDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [INS_REF]
     *
     */
    @JSONField(name = "ins_ref")
    @JsonProperty("ins_ref")
    @Size(min = 0, max = 64, message = "内容长度必须小于等于[64]")
    private String insRef;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [SUM_COST]
     *
     */
    @JSONField(name = "sum_cost")
    @JsonProperty("sum_cost")
    private Double sumCost;

    /**
     * 属性 [NOTES]
     *
     */
    @JSONField(name = "notes")
    @JsonProperty("notes")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String notes;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [EXPIRATION_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expiration_date" , format="yyyy-MM-dd")
    @JsonProperty("expiration_date")
    private Timestamp expirationDate;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [DAYS_LEFT]
     *
     */
    @JSONField(name = "days_left")
    @JsonProperty("days_left")
    private Integer daysLeft;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [ODOMETER]
     *
     */
    @JSONField(name = "odometer")
    @JsonProperty("odometer")
    private Double odometer;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [COST_IDS]
     *
     */
    @JSONField(name = "cost_ids")
    @JsonProperty("cost_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String costIds;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [COST_GENERATED]
     *
     */
    @JSONField(name = "cost_generated")
    @JsonProperty("cost_generated")
    private Double costGenerated;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [GENERATED_COST_IDS]
     *
     */
    @JSONField(name = "generated_cost_ids")
    @JsonProperty("generated_cost_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String generatedCostIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String name;

    /**
     * 属性 [COST_FREQUENCY]
     *
     */
    @JSONField(name = "cost_frequency")
    @JsonProperty("cost_frequency")
    @NotBlank(message = "[经常成本频率]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costFrequency;

    /**
     * 属性 [START_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [VEHICLE_ID]
     *
     */
    @JSONField(name = "vehicle_id")
    @JsonProperty("vehicle_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[车辆]不允许为空!")
    private Long vehicleId;

    /**
     * 属性 [AUTO_GENERATED]
     *
     */
    @JSONField(name = "auto_generated")
    @JsonProperty("auto_generated")
    private Boolean autoGenerated;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [ODOMETER_ID]
     *
     */
    @JSONField(name = "odometer_id")
    @JsonProperty("odometer_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long odometerId;

    /**
     * 属性 [COST_TYPE]
     *
     */
    @JSONField(name = "cost_type")
    @JsonProperty("cost_type")
    @NotBlank(message = "[费用所属类别]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costType;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
     * 属性 [PURCHASER_ID_TEXT]
     *
     */
    @JSONField(name = "purchaser_id_text")
    @JsonProperty("purchaser_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String purchaserIdText;

    /**
     * 属性 [COST_SUBTYPE_ID]
     *
     */
    @JSONField(name = "cost_subtype_id")
    @JsonProperty("cost_subtype_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long costSubtypeId;

    /**
     * 属性 [COST_AMOUNT]
     *
     */
    @JSONField(name = "cost_amount")
    @JsonProperty("cost_amount")
    private Double costAmount;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [INSURER_ID_TEXT]
     *
     */
    @JSONField(name = "insurer_id_text")
    @JsonProperty("insurer_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String insurerIdText;

    /**
     * 属性 [COST_ID_TEXT]
     *
     */
    @JSONField(name = "cost_id_text")
    @JsonProperty("cost_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String costIdText;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String description;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [ODOMETER_UNIT]
     *
     */
    @JSONField(name = "odometer_unit")
    @JsonProperty("odometer_unit")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String odometerUnit;

    /**
     * 属性 [CONTRACT_ID]
     *
     */
    @JSONField(name = "contract_id")
    @JsonProperty("contract_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long contractId;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [COST_ID]
     *
     */
    @JSONField(name = "cost_id")
    @JsonProperty("cost_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[成本]不允许为空!")
    private Long costId;

    /**
     * 属性 [INSURER_ID]
     *
     */
    @JSONField(name = "insurer_id")
    @JsonProperty("insurer_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long insurerId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PURCHASER_ID]
     *
     */
    @JSONField(name = "purchaser_id")
    @JsonProperty("purchaser_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long purchaserId;


    /**
     * 设置 [INS_REF]
     */
    public void setInsRef(String  insRef){
        this.insRef = insRef ;
        this.modify("ins_ref",insRef);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [NOTES]
     */
    public void setNotes(String  notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }

    /**
     * 设置 [EXPIRATION_DATE]
     */
    public void setExpirationDate(Timestamp  expirationDate){
        this.expirationDate = expirationDate ;
        this.modify("expiration_date",expirationDate);
    }

    /**
     * 设置 [ODOMETER]
     */
    public void setOdometer(Double  odometer){
        this.odometer = odometer ;
        this.modify("odometer",odometer);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [COST_GENERATED]
     */
    public void setCostGenerated(Double  costGenerated){
        this.costGenerated = costGenerated ;
        this.modify("cost_generated",costGenerated);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [COST_FREQUENCY]
     */
    public void setCostFrequency(String  costFrequency){
        this.costFrequency = costFrequency ;
        this.modify("cost_frequency",costFrequency);
    }

    /**
     * 设置 [START_DATE]
     */
    public void setStartDate(Timestamp  startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 设置 [COST_ID]
     */
    public void setCostId(Long  costId){
        this.costId = costId ;
        this.modify("cost_id",costId);
    }

    /**
     * 设置 [INSURER_ID]
     */
    public void setInsurerId(Long  insurerId){
        this.insurerId = insurerId ;
        this.modify("insurer_id",insurerId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [PURCHASER_ID]
     */
    public void setPurchaserId(Long  purchaserId){
        this.purchaserId = purchaserId ;
        this.modify("purchaser_id",purchaserId);
    }


}



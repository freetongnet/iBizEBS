package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Stock_locationDTO]
 */
@Data
public class Stock_locationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [POSX]
     *
     */
    @JSONField(name = "posx")
    @JsonProperty("posx")
    private Integer posx;

    /**
     * 属性 [POSY]
     *
     */
    @JSONField(name = "posy")
    @JsonProperty("posy")
    private Integer posy;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [BARCODE]
     *
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String barcode;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [POSZ]
     *
     */
    @JSONField(name = "posz")
    @JsonProperty("posz")
    private Integer posz;

    /**
     * 属性 [COMPLETE_NAME]
     *
     */
    @JSONField(name = "complete_name")
    @JsonProperty("complete_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String completeName;

    /**
     * 属性 [SCRAP_LOCATION]
     *
     */
    @JSONField(name = "scrap_location")
    @JsonProperty("scrap_location")
    private Boolean scrapLocation;

    /**
     * 属性 [RETURN_LOCATION]
     *
     */
    @JSONField(name = "return_location")
    @JsonProperty("return_location")
    private Boolean returnLocation;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[位置名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PARENT_PATH]
     *
     */
    @JSONField(name = "parent_path")
    @JsonProperty("parent_path")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String parentPath;

    /**
     * 属性 [QUANT_IDS]
     *
     */
    @JSONField(name = "quant_ids")
    @JsonProperty("quant_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String quantIds;

    /**
     * 属性 [USAGE]
     *
     */
    @JSONField(name = "usage")
    @JsonProperty("usage")
    @NotBlank(message = "[位置类型]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String usage;

    /**
     * 属性 [COMMENT]
     *
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String comment;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String childIds;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [VALUATION_OUT_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "valuation_out_account_id_text")
    @JsonProperty("valuation_out_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String valuationOutAccountIdText;

    /**
     * 属性 [REMOVAL_STRATEGY_ID_TEXT]
     *
     */
    @JSONField(name = "removal_strategy_id_text")
    @JsonProperty("removal_strategy_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String removalStrategyIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [VALUATION_IN_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "valuation_in_account_id_text")
    @JsonProperty("valuation_in_account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String valuationInAccountIdText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [VALUATION_IN_ACCOUNT_ID]
     *
     */
    @JSONField(name = "valuation_in_account_id")
    @JsonProperty("valuation_in_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long valuationInAccountId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [VALUATION_OUT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "valuation_out_account_id")
    @JsonProperty("valuation_out_account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long valuationOutAccountId;

    /**
     * 属性 [REMOVAL_STRATEGY_ID]
     *
     */
    @JSONField(name = "removal_strategy_id")
    @JsonProperty("removal_strategy_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long removalStrategyId;


    /**
     * 设置 [POSX]
     */
    public void setPosx(Integer  posx){
        this.posx = posx ;
        this.modify("posx",posx);
    }

    /**
     * 设置 [POSY]
     */
    public void setPosy(Integer  posy){
        this.posy = posy ;
        this.modify("posy",posy);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(Boolean  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [BARCODE]
     */
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [POSZ]
     */
    public void setPosz(Integer  posz){
        this.posz = posz ;
        this.modify("posz",posz);
    }

    /**
     * 设置 [COMPLETE_NAME]
     */
    public void setCompleteName(String  completeName){
        this.completeName = completeName ;
        this.modify("complete_name",completeName);
    }

    /**
     * 设置 [SCRAP_LOCATION]
     */
    public void setScrapLocation(Boolean  scrapLocation){
        this.scrapLocation = scrapLocation ;
        this.modify("scrap_location",scrapLocation);
    }

    /**
     * 设置 [RETURN_LOCATION]
     */
    public void setReturnLocation(Boolean  returnLocation){
        this.returnLocation = returnLocation ;
        this.modify("return_location",returnLocation);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PARENT_PATH]
     */
    public void setParentPath(String  parentPath){
        this.parentPath = parentPath ;
        this.modify("parent_path",parentPath);
    }

    /**
     * 设置 [USAGE]
     */
    public void setUsage(String  usage){
        this.usage = usage ;
        this.modify("usage",usage);
    }

    /**
     * 设置 [COMMENT]
     */
    public void setComment(String  comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }

    /**
     * 设置 [VALUATION_IN_ACCOUNT_ID]
     */
    public void setValuationInAccountId(Long  valuationInAccountId){
        this.valuationInAccountId = valuationInAccountId ;
        this.modify("valuation_in_account_id",valuationInAccountId);
    }

    /**
     * 设置 [VALUATION_OUT_ACCOUNT_ID]
     */
    public void setValuationOutAccountId(Long  valuationOutAccountId){
        this.valuationOutAccountId = valuationOutAccountId ;
        this.modify("valuation_out_account_id",valuationOutAccountId);
    }

    /**
     * 设置 [REMOVAL_STRATEGY_ID]
     */
    public void setRemovalStrategyId(Long  removalStrategyId){
        this.removalStrategyId = removalStrategyId ;
        this.modify("removal_strategy_id",removalStrategyId);
    }


}



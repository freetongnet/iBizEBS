package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_merge_opportunityService;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"合并商机" })
@RestController("Core-crm_merge_opportunity")
@RequestMapping("")
public class Crm_merge_opportunityResource {

    @Autowired
    public ICrm_merge_opportunityService crm_merge_opportunityService;

    @Autowired
    @Lazy
    public Crm_merge_opportunityMapping crm_merge_opportunityMapping;

    @PreAuthorize("hasPermission(this.crm_merge_opportunityMapping.toDomain(#crm_merge_opportunitydto),'iBizBusinessCentral-Crm_merge_opportunity-Create')")
    @ApiOperation(value = "新建合并商机", tags = {"合并商机" },  notes = "新建合并商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities")
    public ResponseEntity<Crm_merge_opportunityDTO> create(@Validated @RequestBody Crm_merge_opportunityDTO crm_merge_opportunitydto) {
        Crm_merge_opportunity domain = crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydto);
		crm_merge_opportunityService.create(domain);
        Crm_merge_opportunityDTO dto = crm_merge_opportunityMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_merge_opportunityMapping.toDomain(#crm_merge_opportunitydtos),'iBizBusinessCentral-Crm_merge_opportunity-Create')")
    @ApiOperation(value = "批量新建合并商机", tags = {"合并商机" },  notes = "批量新建合并商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_merge_opportunityDTO> crm_merge_opportunitydtos) {
        crm_merge_opportunityService.createBatch(crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "crm_merge_opportunity" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.crm_merge_opportunityService.get(#crm_merge_opportunity_id),'iBizBusinessCentral-Crm_merge_opportunity-Update')")
    @ApiOperation(value = "更新合并商机", tags = {"合并商机" },  notes = "更新合并商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}")
    public ResponseEntity<Crm_merge_opportunityDTO> update(@PathVariable("crm_merge_opportunity_id") Long crm_merge_opportunity_id, @RequestBody Crm_merge_opportunityDTO crm_merge_opportunitydto) {
		Crm_merge_opportunity domain  = crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydto);
        domain .setId(crm_merge_opportunity_id);
		crm_merge_opportunityService.update(domain );
		Crm_merge_opportunityDTO dto = crm_merge_opportunityMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_merge_opportunityService.getCrmMergeOpportunityByEntities(this.crm_merge_opportunityMapping.toDomain(#crm_merge_opportunitydtos)),'iBizBusinessCentral-Crm_merge_opportunity-Update')")
    @ApiOperation(value = "批量更新合并商机", tags = {"合并商机" },  notes = "批量更新合并商机")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_merge_opportunityDTO> crm_merge_opportunitydtos) {
        crm_merge_opportunityService.updateBatch(crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.crm_merge_opportunityService.get(#crm_merge_opportunity_id),'iBizBusinessCentral-Crm_merge_opportunity-Remove')")
    @ApiOperation(value = "删除合并商机", tags = {"合并商机" },  notes = "删除合并商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_merge_opportunity_id") Long crm_merge_opportunity_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunityService.remove(crm_merge_opportunity_id));
    }

    @PreAuthorize("hasPermission(this.crm_merge_opportunityService.getCrmMergeOpportunityByIds(#ids),'iBizBusinessCentral-Crm_merge_opportunity-Remove')")
    @ApiOperation(value = "批量删除合并商机", tags = {"合并商机" },  notes = "批量删除合并商机")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        crm_merge_opportunityService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.crm_merge_opportunityMapping.toDomain(returnObject.body),'iBizBusinessCentral-Crm_merge_opportunity-Get')")
    @ApiOperation(value = "获取合并商机", tags = {"合并商机" },  notes = "获取合并商机")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/{crm_merge_opportunity_id}")
    public ResponseEntity<Crm_merge_opportunityDTO> get(@PathVariable("crm_merge_opportunity_id") Long crm_merge_opportunity_id) {
        Crm_merge_opportunity domain = crm_merge_opportunityService.get(crm_merge_opportunity_id);
        Crm_merge_opportunityDTO dto = crm_merge_opportunityMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取合并商机草稿", tags = {"合并商机" },  notes = "获取合并商机草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/getdraft")
    public ResponseEntity<Crm_merge_opportunityDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunityMapping.toDto(crm_merge_opportunityService.getDraft(new Crm_merge_opportunity())));
    }

    @ApiOperation(value = "检查合并商机", tags = {"合并商机" },  notes = "检查合并商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_merge_opportunityDTO crm_merge_opportunitydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunityService.checkKey(crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydto)));
    }

    @PreAuthorize("hasPermission(this.crm_merge_opportunityMapping.toDomain(#crm_merge_opportunitydto),'iBizBusinessCentral-Crm_merge_opportunity-Save')")
    @ApiOperation(value = "保存合并商机", tags = {"合并商机" },  notes = "保存合并商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_merge_opportunityDTO crm_merge_opportunitydto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_merge_opportunityService.save(crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydto)));
    }

    @PreAuthorize("hasPermission(this.crm_merge_opportunityMapping.toDomain(#crm_merge_opportunitydtos),'iBizBusinessCentral-Crm_merge_opportunity-Save')")
    @ApiOperation(value = "批量保存合并商机", tags = {"合并商机" },  notes = "批量保存合并商机")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_merge_opportunityDTO> crm_merge_opportunitydtos) {
        crm_merge_opportunityService.saveBatch(crm_merge_opportunityMapping.toDomain(crm_merge_opportunitydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_merge_opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_merge_opportunity-Get')")
	@ApiOperation(value = "获取数据集", tags = {"合并商机" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/crm_merge_opportunities/fetchdefault")
	public ResponseEntity<List<Crm_merge_opportunityDTO>> fetchDefault(Crm_merge_opportunitySearchContext context) {
        Page<Crm_merge_opportunity> domains = crm_merge_opportunityService.searchDefault(context) ;
        List<Crm_merge_opportunityDTO> list = crm_merge_opportunityMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_merge_opportunity-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_merge_opportunity-Get')")
	@ApiOperation(value = "查询数据集", tags = {"合并商机" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/crm_merge_opportunities/searchdefault")
	public ResponseEntity<Page<Crm_merge_opportunityDTO>> searchDefault(@RequestBody Crm_merge_opportunitySearchContext context) {
        Page<Crm_merge_opportunity> domains = crm_merge_opportunityService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_merge_opportunityMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


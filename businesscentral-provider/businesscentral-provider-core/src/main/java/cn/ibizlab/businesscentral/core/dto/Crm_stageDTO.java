package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Crm_stageDTO]
 */
@Data
public class Crm_stageDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PROBABILITY]
     *
     */
    @JSONField(name = "probability")
    @JsonProperty("probability")
    @NotNull(message = "[概率(%)]不允许为空!")
    private Double probability;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [REQUIREMENTS]
     *
     */
    @JSONField(name = "requirements")
    @JsonProperty("requirements")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String requirements;

    /**
     * 属性 [FOLD]
     *
     */
    @JSONField(name = "fold")
    @JsonProperty("fold")
    private Boolean fold;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [ON_CHANGE]
     *
     */
    @JSONField(name = "on_change")
    @JsonProperty("on_change")
    private Boolean onChange;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [LEGEND_PRIORITY]
     *
     */
    @JSONField(name = "legend_priority")
    @JsonProperty("legend_priority")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String legendPriority;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[阶段名称]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [TEAM_COUNT]
     *
     */
    @JSONField(name = "team_count")
    @JsonProperty("team_count")
    private Integer teamCount;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teamId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [PROBABILITY]
     */
    public void setProbability(Double  probability){
        this.probability = probability ;
        this.modify("probability",probability);
    }

    /**
     * 设置 [REQUIREMENTS]
     */
    public void setRequirements(String  requirements){
        this.requirements = requirements ;
        this.modify("requirements",requirements);
    }

    /**
     * 设置 [FOLD]
     */
    public void setFold(Boolean  fold){
        this.fold = fold ;
        this.modify("fold",fold);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [ON_CHANGE]
     */
    public void setOnChange(Boolean  onChange){
        this.onChange = onChange ;
        this.modify("on_change",onChange);
    }

    /**
     * 设置 [LEGEND_PRIORITY]
     */
    public void setLegendPriority(String  legendPriority){
        this.legendPriority = legendPriority ;
        this.modify("legend_priority",legendPriority);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [TEAM_ID]
     */
    public void setTeamId(Long  teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }


}



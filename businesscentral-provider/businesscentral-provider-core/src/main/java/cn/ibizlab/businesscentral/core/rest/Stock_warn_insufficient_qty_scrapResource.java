package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_scrapService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qty_scrapSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"报废数量不足发出警告" })
@RestController("Core-stock_warn_insufficient_qty_scrap")
@RequestMapping("")
public class Stock_warn_insufficient_qty_scrapResource {

    @Autowired
    public IStock_warn_insufficient_qty_scrapService stock_warn_insufficient_qty_scrapService;

    @Autowired
    @Lazy
    public Stock_warn_insufficient_qty_scrapMapping stock_warn_insufficient_qty_scrapMapping;

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_scrapMapping.toDomain(#stock_warn_insufficient_qty_scrapdto),'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Create')")
    @ApiOperation(value = "新建报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "新建报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_scraps")
    public ResponseEntity<Stock_warn_insufficient_qty_scrapDTO> create(@Validated @RequestBody Stock_warn_insufficient_qty_scrapDTO stock_warn_insufficient_qty_scrapdto) {
        Stock_warn_insufficient_qty_scrap domain = stock_warn_insufficient_qty_scrapMapping.toDomain(stock_warn_insufficient_qty_scrapdto);
		stock_warn_insufficient_qty_scrapService.create(domain);
        Stock_warn_insufficient_qty_scrapDTO dto = stock_warn_insufficient_qty_scrapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_scrapMapping.toDomain(#stock_warn_insufficient_qty_scrapdtos),'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Create')")
    @ApiOperation(value = "批量新建报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "批量新建报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_scraps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_warn_insufficient_qty_scrapDTO> stock_warn_insufficient_qty_scrapdtos) {
        stock_warn_insufficient_qty_scrapService.createBatch(stock_warn_insufficient_qty_scrapMapping.toDomain(stock_warn_insufficient_qty_scrapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_warn_insufficient_qty_scrap" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_scrapService.get(#stock_warn_insufficient_qty_scrap_id),'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Update')")
    @ApiOperation(value = "更新报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "更新报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_scraps/{stock_warn_insufficient_qty_scrap_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_scrapDTO> update(@PathVariable("stock_warn_insufficient_qty_scrap_id") Long stock_warn_insufficient_qty_scrap_id, @RequestBody Stock_warn_insufficient_qty_scrapDTO stock_warn_insufficient_qty_scrapdto) {
		Stock_warn_insufficient_qty_scrap domain  = stock_warn_insufficient_qty_scrapMapping.toDomain(stock_warn_insufficient_qty_scrapdto);
        domain .setId(stock_warn_insufficient_qty_scrap_id);
		stock_warn_insufficient_qty_scrapService.update(domain );
		Stock_warn_insufficient_qty_scrapDTO dto = stock_warn_insufficient_qty_scrapMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_scrapService.getStockWarnInsufficientQtyScrapByEntities(this.stock_warn_insufficient_qty_scrapMapping.toDomain(#stock_warn_insufficient_qty_scrapdtos)),'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Update')")
    @ApiOperation(value = "批量更新报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "批量更新报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_scraps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warn_insufficient_qty_scrapDTO> stock_warn_insufficient_qty_scrapdtos) {
        stock_warn_insufficient_qty_scrapService.updateBatch(stock_warn_insufficient_qty_scrapMapping.toDomain(stock_warn_insufficient_qty_scrapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_scrapService.get(#stock_warn_insufficient_qty_scrap_id),'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Remove')")
    @ApiOperation(value = "删除报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "删除报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_scraps/{stock_warn_insufficient_qty_scrap_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_warn_insufficient_qty_scrap_id") Long stock_warn_insufficient_qty_scrap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_scrapService.remove(stock_warn_insufficient_qty_scrap_id));
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_scrapService.getStockWarnInsufficientQtyScrapByIds(#ids),'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Remove')")
    @ApiOperation(value = "批量删除报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "批量删除报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_scraps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_warn_insufficient_qty_scrapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_warn_insufficient_qty_scrapMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Get')")
    @ApiOperation(value = "获取报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "获取报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_scraps/{stock_warn_insufficient_qty_scrap_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_scrapDTO> get(@PathVariable("stock_warn_insufficient_qty_scrap_id") Long stock_warn_insufficient_qty_scrap_id) {
        Stock_warn_insufficient_qty_scrap domain = stock_warn_insufficient_qty_scrapService.get(stock_warn_insufficient_qty_scrap_id);
        Stock_warn_insufficient_qty_scrapDTO dto = stock_warn_insufficient_qty_scrapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取报废数量不足发出警告草稿", tags = {"报废数量不足发出警告" },  notes = "获取报废数量不足发出警告草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_scraps/getdraft")
    public ResponseEntity<Stock_warn_insufficient_qty_scrapDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_scrapMapping.toDto(stock_warn_insufficient_qty_scrapService.getDraft(new Stock_warn_insufficient_qty_scrap())));
    }

    @ApiOperation(value = "检查报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "检查报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_scraps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_warn_insufficient_qty_scrapDTO stock_warn_insufficient_qty_scrapdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_scrapService.checkKey(stock_warn_insufficient_qty_scrapMapping.toDomain(stock_warn_insufficient_qty_scrapdto)));
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_scrapMapping.toDomain(#stock_warn_insufficient_qty_scrapdto),'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Save')")
    @ApiOperation(value = "保存报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "保存报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_scraps/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_warn_insufficient_qty_scrapDTO stock_warn_insufficient_qty_scrapdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_scrapService.save(stock_warn_insufficient_qty_scrapMapping.toDomain(stock_warn_insufficient_qty_scrapdto)));
    }

    @PreAuthorize("hasPermission(this.stock_warn_insufficient_qty_scrapMapping.toDomain(#stock_warn_insufficient_qty_scrapdtos),'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Save')")
    @ApiOperation(value = "批量保存报废数量不足发出警告", tags = {"报废数量不足发出警告" },  notes = "批量保存报废数量不足发出警告")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_scraps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_warn_insufficient_qty_scrapDTO> stock_warn_insufficient_qty_scrapdtos) {
        stock_warn_insufficient_qty_scrapService.saveBatch(stock_warn_insufficient_qty_scrapMapping.toDomain(stock_warn_insufficient_qty_scrapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Get')")
	@ApiOperation(value = "获取数据集", tags = {"报废数量不足发出警告" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warn_insufficient_qty_scraps/fetchdefault")
	public ResponseEntity<List<Stock_warn_insufficient_qty_scrapDTO>> fetchDefault(Stock_warn_insufficient_qty_scrapSearchContext context) {
        Page<Stock_warn_insufficient_qty_scrap> domains = stock_warn_insufficient_qty_scrapService.searchDefault(context) ;
        List<Stock_warn_insufficient_qty_scrapDTO> list = stock_warn_insufficient_qty_scrapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_warn_insufficient_qty_scrap-Get')")
	@ApiOperation(value = "查询数据集", tags = {"报废数量不足发出警告" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_warn_insufficient_qty_scraps/searchdefault")
	public ResponseEntity<Page<Stock_warn_insufficient_qty_scrapDTO>> searchDefault(@RequestBody Stock_warn_insufficient_qty_scrapSearchContext context) {
        Page<Stock_warn_insufficient_qty_scrap> domains = stock_warn_insufficient_qty_scrapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_warn_insufficient_qty_scrapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


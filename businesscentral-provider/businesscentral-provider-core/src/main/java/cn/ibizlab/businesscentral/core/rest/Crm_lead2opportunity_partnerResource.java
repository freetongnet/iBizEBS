package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead2opportunity_partnerService;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"转化线索为商机（单个）" })
@RestController("Core-crm_lead2opportunity_partner")
@RequestMapping("")
public class Crm_lead2opportunity_partnerResource {

    @Autowired
    public ICrm_lead2opportunity_partnerService crm_lead2opportunity_partnerService;

    @Autowired
    @Lazy
    public Crm_lead2opportunity_partnerMapping crm_lead2opportunity_partnerMapping;

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partnerMapping.toDomain(#crm_lead2opportunity_partnerdto),'iBizBusinessCentral-Crm_lead2opportunity_partner-Create')")
    @ApiOperation(value = "新建转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "新建转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners")
    public ResponseEntity<Crm_lead2opportunity_partnerDTO> create(@Validated @RequestBody Crm_lead2opportunity_partnerDTO crm_lead2opportunity_partnerdto) {
        Crm_lead2opportunity_partner domain = crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdto);
		crm_lead2opportunity_partnerService.create(domain);
        Crm_lead2opportunity_partnerDTO dto = crm_lead2opportunity_partnerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partnerMapping.toDomain(#crm_lead2opportunity_partnerdtos),'iBizBusinessCentral-Crm_lead2opportunity_partner-Create')")
    @ApiOperation(value = "批量新建转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "批量新建转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_lead2opportunity_partnerDTO> crm_lead2opportunity_partnerdtos) {
        crm_lead2opportunity_partnerService.createBatch(crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "crm_lead2opportunity_partner" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partnerService.get(#crm_lead2opportunity_partner_id),'iBizBusinessCentral-Crm_lead2opportunity_partner-Update')")
    @ApiOperation(value = "更新转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "更新转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    public ResponseEntity<Crm_lead2opportunity_partnerDTO> update(@PathVariable("crm_lead2opportunity_partner_id") Long crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partnerDTO crm_lead2opportunity_partnerdto) {
		Crm_lead2opportunity_partner domain  = crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdto);
        domain .setId(crm_lead2opportunity_partner_id);
		crm_lead2opportunity_partnerService.update(domain );
		Crm_lead2opportunity_partnerDTO dto = crm_lead2opportunity_partnerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partnerService.getCrmLead2opportunityPartnerByEntities(this.crm_lead2opportunity_partnerMapping.toDomain(#crm_lead2opportunity_partnerdtos)),'iBizBusinessCentral-Crm_lead2opportunity_partner-Update')")
    @ApiOperation(value = "批量更新转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "批量更新转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_lead2opportunity_partners/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead2opportunity_partnerDTO> crm_lead2opportunity_partnerdtos) {
        crm_lead2opportunity_partnerService.updateBatch(crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partnerService.get(#crm_lead2opportunity_partner_id),'iBizBusinessCentral-Crm_lead2opportunity_partner-Remove')")
    @ApiOperation(value = "删除转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "删除转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead2opportunity_partner_id") Long crm_lead2opportunity_partner_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partnerService.remove(crm_lead2opportunity_partner_id));
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partnerService.getCrmLead2opportunityPartnerByIds(#ids),'iBizBusinessCentral-Crm_lead2opportunity_partner-Remove')")
    @ApiOperation(value = "批量删除转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "批量删除转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_lead2opportunity_partners/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        crm_lead2opportunity_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.crm_lead2opportunity_partnerMapping.toDomain(returnObject.body),'iBizBusinessCentral-Crm_lead2opportunity_partner-Get')")
    @ApiOperation(value = "获取转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "获取转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    public ResponseEntity<Crm_lead2opportunity_partnerDTO> get(@PathVariable("crm_lead2opportunity_partner_id") Long crm_lead2opportunity_partner_id) {
        Crm_lead2opportunity_partner domain = crm_lead2opportunity_partnerService.get(crm_lead2opportunity_partner_id);
        Crm_lead2opportunity_partnerDTO dto = crm_lead2opportunity_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取转化线索为商机（单个）草稿", tags = {"转化线索为商机（单个）" },  notes = "获取转化线索为商机（单个）草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_lead2opportunity_partners/getdraft")
    public ResponseEntity<Crm_lead2opportunity_partnerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partnerMapping.toDto(crm_lead2opportunity_partnerService.getDraft(new Crm_lead2opportunity_partner())));
    }

    @ApiOperation(value = "检查转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "检查转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Crm_lead2opportunity_partnerDTO crm_lead2opportunity_partnerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partnerService.checkKey(crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdto)));
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partnerMapping.toDomain(#crm_lead2opportunity_partnerdto),'iBizBusinessCentral-Crm_lead2opportunity_partner-Save')")
    @ApiOperation(value = "保存转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "保存转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/save")
    public ResponseEntity<Boolean> save(@RequestBody Crm_lead2opportunity_partnerDTO crm_lead2opportunity_partnerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(crm_lead2opportunity_partnerService.save(crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdto)));
    }

    @PreAuthorize("hasPermission(this.crm_lead2opportunity_partnerMapping.toDomain(#crm_lead2opportunity_partnerdtos),'iBizBusinessCentral-Crm_lead2opportunity_partner-Save')")
    @ApiOperation(value = "批量保存转化线索为商机（单个）", tags = {"转化线索为商机（单个）" },  notes = "批量保存转化线索为商机（单个）")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_lead2opportunity_partners/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Crm_lead2opportunity_partnerDTO> crm_lead2opportunity_partnerdtos) {
        crm_lead2opportunity_partnerService.saveBatch(crm_lead2opportunity_partnerMapping.toDomain(crm_lead2opportunity_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead2opportunity_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead2opportunity_partner-Get')")
	@ApiOperation(value = "获取数据集", tags = {"转化线索为商机（单个）" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/crm_lead2opportunity_partners/fetchdefault")
	public ResponseEntity<List<Crm_lead2opportunity_partnerDTO>> fetchDefault(Crm_lead2opportunity_partnerSearchContext context) {
        Page<Crm_lead2opportunity_partner> domains = crm_lead2opportunity_partnerService.searchDefault(context) ;
        List<Crm_lead2opportunity_partnerDTO> list = crm_lead2opportunity_partnerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Crm_lead2opportunity_partner-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Crm_lead2opportunity_partner-Get')")
	@ApiOperation(value = "查询数据集", tags = {"转化线索为商机（单个）" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/crm_lead2opportunity_partners/searchdefault")
	public ResponseEntity<Page<Crm_lead2opportunity_partnerDTO>> searchDefault(@RequestBody Crm_lead2opportunity_partnerSearchContext context) {
        Page<Crm_lead2opportunity_partner> domains = crm_lead2opportunity_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_lead2opportunity_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


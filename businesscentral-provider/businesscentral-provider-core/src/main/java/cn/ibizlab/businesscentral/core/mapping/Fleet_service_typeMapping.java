package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.businesscentral.core.dto.Fleet_service_typeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreFleet_service_typeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Fleet_service_typeMapping extends MappingBase<Fleet_service_typeDTO, Fleet_service_type> {


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.businesscentral.core.dto.Mail_mass_mailing_list_contact_relDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMail_mass_mailing_list_contact_relMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mail_mass_mailing_list_contact_relMapping extends MappingBase<Mail_mass_mailing_list_contact_relDTO, Mail_mass_mailing_list_contact_rel> {


}


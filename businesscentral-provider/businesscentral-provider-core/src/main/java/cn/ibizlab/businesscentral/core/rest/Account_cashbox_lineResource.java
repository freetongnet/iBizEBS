package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_cashbox_line;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_cashbox_lineService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_cashbox_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"钱箱明细" })
@RestController("Core-account_cashbox_line")
@RequestMapping("")
public class Account_cashbox_lineResource {

    @Autowired
    public IAccount_cashbox_lineService account_cashbox_lineService;

    @Autowired
    @Lazy
    public Account_cashbox_lineMapping account_cashbox_lineMapping;

    @PreAuthorize("hasPermission(this.account_cashbox_lineMapping.toDomain(#account_cashbox_linedto),'iBizBusinessCentral-Account_cashbox_line-Create')")
    @ApiOperation(value = "新建钱箱明细", tags = {"钱箱明细" },  notes = "新建钱箱明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cashbox_lines")
    public ResponseEntity<Account_cashbox_lineDTO> create(@Validated @RequestBody Account_cashbox_lineDTO account_cashbox_linedto) {
        Account_cashbox_line domain = account_cashbox_lineMapping.toDomain(account_cashbox_linedto);
		account_cashbox_lineService.create(domain);
        Account_cashbox_lineDTO dto = account_cashbox_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_cashbox_lineMapping.toDomain(#account_cashbox_linedtos),'iBizBusinessCentral-Account_cashbox_line-Create')")
    @ApiOperation(value = "批量新建钱箱明细", tags = {"钱箱明细" },  notes = "批量新建钱箱明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cashbox_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_cashbox_lineDTO> account_cashbox_linedtos) {
        account_cashbox_lineService.createBatch(account_cashbox_lineMapping.toDomain(account_cashbox_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_cashbox_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_cashbox_lineService.get(#account_cashbox_line_id),'iBizBusinessCentral-Account_cashbox_line-Update')")
    @ApiOperation(value = "更新钱箱明细", tags = {"钱箱明细" },  notes = "更新钱箱明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_cashbox_lines/{account_cashbox_line_id}")
    public ResponseEntity<Account_cashbox_lineDTO> update(@PathVariable("account_cashbox_line_id") Long account_cashbox_line_id, @RequestBody Account_cashbox_lineDTO account_cashbox_linedto) {
		Account_cashbox_line domain  = account_cashbox_lineMapping.toDomain(account_cashbox_linedto);
        domain .setId(account_cashbox_line_id);
		account_cashbox_lineService.update(domain );
		Account_cashbox_lineDTO dto = account_cashbox_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_cashbox_lineService.getAccountCashboxLineByEntities(this.account_cashbox_lineMapping.toDomain(#account_cashbox_linedtos)),'iBizBusinessCentral-Account_cashbox_line-Update')")
    @ApiOperation(value = "批量更新钱箱明细", tags = {"钱箱明细" },  notes = "批量更新钱箱明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_cashbox_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_cashbox_lineDTO> account_cashbox_linedtos) {
        account_cashbox_lineService.updateBatch(account_cashbox_lineMapping.toDomain(account_cashbox_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_cashbox_lineService.get(#account_cashbox_line_id),'iBizBusinessCentral-Account_cashbox_line-Remove')")
    @ApiOperation(value = "删除钱箱明细", tags = {"钱箱明细" },  notes = "删除钱箱明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_cashbox_lines/{account_cashbox_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_cashbox_line_id") Long account_cashbox_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_cashbox_lineService.remove(account_cashbox_line_id));
    }

    @PreAuthorize("hasPermission(this.account_cashbox_lineService.getAccountCashboxLineByIds(#ids),'iBizBusinessCentral-Account_cashbox_line-Remove')")
    @ApiOperation(value = "批量删除钱箱明细", tags = {"钱箱明细" },  notes = "批量删除钱箱明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_cashbox_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_cashbox_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_cashbox_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_cashbox_line-Get')")
    @ApiOperation(value = "获取钱箱明细", tags = {"钱箱明细" },  notes = "获取钱箱明细")
	@RequestMapping(method = RequestMethod.GET, value = "/account_cashbox_lines/{account_cashbox_line_id}")
    public ResponseEntity<Account_cashbox_lineDTO> get(@PathVariable("account_cashbox_line_id") Long account_cashbox_line_id) {
        Account_cashbox_line domain = account_cashbox_lineService.get(account_cashbox_line_id);
        Account_cashbox_lineDTO dto = account_cashbox_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取钱箱明细草稿", tags = {"钱箱明细" },  notes = "获取钱箱明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_cashbox_lines/getdraft")
    public ResponseEntity<Account_cashbox_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_cashbox_lineMapping.toDto(account_cashbox_lineService.getDraft(new Account_cashbox_line())));
    }

    @ApiOperation(value = "检查钱箱明细", tags = {"钱箱明细" },  notes = "检查钱箱明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cashbox_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_cashbox_lineDTO account_cashbox_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_cashbox_lineService.checkKey(account_cashbox_lineMapping.toDomain(account_cashbox_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_cashbox_lineMapping.toDomain(#account_cashbox_linedto),'iBizBusinessCentral-Account_cashbox_line-Save')")
    @ApiOperation(value = "保存钱箱明细", tags = {"钱箱明细" },  notes = "保存钱箱明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cashbox_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_cashbox_lineDTO account_cashbox_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_cashbox_lineService.save(account_cashbox_lineMapping.toDomain(account_cashbox_linedto)));
    }

    @PreAuthorize("hasPermission(this.account_cashbox_lineMapping.toDomain(#account_cashbox_linedtos),'iBizBusinessCentral-Account_cashbox_line-Save')")
    @ApiOperation(value = "批量保存钱箱明细", tags = {"钱箱明细" },  notes = "批量保存钱箱明细")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cashbox_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_cashbox_lineDTO> account_cashbox_linedtos) {
        account_cashbox_lineService.saveBatch(account_cashbox_lineMapping.toDomain(account_cashbox_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_cashbox_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_cashbox_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"钱箱明细" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_cashbox_lines/fetchdefault")
	public ResponseEntity<List<Account_cashbox_lineDTO>> fetchDefault(Account_cashbox_lineSearchContext context) {
        Page<Account_cashbox_line> domains = account_cashbox_lineService.searchDefault(context) ;
        List<Account_cashbox_lineDTO> list = account_cashbox_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_cashbox_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_cashbox_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"钱箱明细" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_cashbox_lines/searchdefault")
	public ResponseEntity<Page<Account_cashbox_lineDTO>> searchDefault(@RequestBody Account_cashbox_lineSearchContext context) {
        Page<Account_cashbox_line> domains = account_cashbox_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_cashbox_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_productService;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_productSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工作餐产品" })
@RestController("Core-lunch_product")
@RequestMapping("")
public class Lunch_productResource {

    @Autowired
    public ILunch_productService lunch_productService;

    @Autowired
    @Lazy
    public Lunch_productMapping lunch_productMapping;

    @PreAuthorize("hasPermission(this.lunch_productMapping.toDomain(#lunch_productdto),'iBizBusinessCentral-Lunch_product-Create')")
    @ApiOperation(value = "新建工作餐产品", tags = {"工作餐产品" },  notes = "新建工作餐产品")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_products")
    public ResponseEntity<Lunch_productDTO> create(@Validated @RequestBody Lunch_productDTO lunch_productdto) {
        Lunch_product domain = lunch_productMapping.toDomain(lunch_productdto);
		lunch_productService.create(domain);
        Lunch_productDTO dto = lunch_productMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_productMapping.toDomain(#lunch_productdtos),'iBizBusinessCentral-Lunch_product-Create')")
    @ApiOperation(value = "批量新建工作餐产品", tags = {"工作餐产品" },  notes = "批量新建工作餐产品")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_products/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_productDTO> lunch_productdtos) {
        lunch_productService.createBatch(lunch_productMapping.toDomain(lunch_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lunch_product" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.lunch_productService.get(#lunch_product_id),'iBizBusinessCentral-Lunch_product-Update')")
    @ApiOperation(value = "更新工作餐产品", tags = {"工作餐产品" },  notes = "更新工作餐产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_products/{lunch_product_id}")
    public ResponseEntity<Lunch_productDTO> update(@PathVariable("lunch_product_id") Long lunch_product_id, @RequestBody Lunch_productDTO lunch_productdto) {
		Lunch_product domain  = lunch_productMapping.toDomain(lunch_productdto);
        domain .setId(lunch_product_id);
		lunch_productService.update(domain );
		Lunch_productDTO dto = lunch_productMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_productService.getLunchProductByEntities(this.lunch_productMapping.toDomain(#lunch_productdtos)),'iBizBusinessCentral-Lunch_product-Update')")
    @ApiOperation(value = "批量更新工作餐产品", tags = {"工作餐产品" },  notes = "批量更新工作餐产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_products/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_productDTO> lunch_productdtos) {
        lunch_productService.updateBatch(lunch_productMapping.toDomain(lunch_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.lunch_productService.get(#lunch_product_id),'iBizBusinessCentral-Lunch_product-Remove')")
    @ApiOperation(value = "删除工作餐产品", tags = {"工作餐产品" },  notes = "删除工作餐产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_products/{lunch_product_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("lunch_product_id") Long lunch_product_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_productService.remove(lunch_product_id));
    }

    @PreAuthorize("hasPermission(this.lunch_productService.getLunchProductByIds(#ids),'iBizBusinessCentral-Lunch_product-Remove')")
    @ApiOperation(value = "批量删除工作餐产品", tags = {"工作餐产品" },  notes = "批量删除工作餐产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_products/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        lunch_productService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.lunch_productMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lunch_product-Get')")
    @ApiOperation(value = "获取工作餐产品", tags = {"工作餐产品" },  notes = "获取工作餐产品")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_products/{lunch_product_id}")
    public ResponseEntity<Lunch_productDTO> get(@PathVariable("lunch_product_id") Long lunch_product_id) {
        Lunch_product domain = lunch_productService.get(lunch_product_id);
        Lunch_productDTO dto = lunch_productMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工作餐产品草稿", tags = {"工作餐产品" },  notes = "获取工作餐产品草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_products/getdraft")
    public ResponseEntity<Lunch_productDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_productMapping.toDto(lunch_productService.getDraft(new Lunch_product())));
    }

    @ApiOperation(value = "检查工作餐产品", tags = {"工作餐产品" },  notes = "检查工作餐产品")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_products/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Lunch_productDTO lunch_productdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(lunch_productService.checkKey(lunch_productMapping.toDomain(lunch_productdto)));
    }

    @PreAuthorize("hasPermission(this.lunch_productMapping.toDomain(#lunch_productdto),'iBizBusinessCentral-Lunch_product-Save')")
    @ApiOperation(value = "保存工作餐产品", tags = {"工作餐产品" },  notes = "保存工作餐产品")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_products/save")
    public ResponseEntity<Boolean> save(@RequestBody Lunch_productDTO lunch_productdto) {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_productService.save(lunch_productMapping.toDomain(lunch_productdto)));
    }

    @PreAuthorize("hasPermission(this.lunch_productMapping.toDomain(#lunch_productdtos),'iBizBusinessCentral-Lunch_product-Save')")
    @ApiOperation(value = "批量保存工作餐产品", tags = {"工作餐产品" },  notes = "批量保存工作餐产品")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_products/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Lunch_productDTO> lunch_productdtos) {
        lunch_productService.saveBatch(lunch_productMapping.toDomain(lunch_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_product-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_product-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工作餐产品" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_products/fetchdefault")
	public ResponseEntity<List<Lunch_productDTO>> fetchDefault(Lunch_productSearchContext context) {
        Page<Lunch_product> domains = lunch_productService.searchDefault(context) ;
        List<Lunch_productDTO> list = lunch_productMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_product-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_product-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工作餐产品" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/lunch_products/searchdefault")
	public ResponseEntity<Page<Lunch_productDTO>> searchDefault(@RequestBody Lunch_productSearchContext context) {
        Page<Lunch_product> domains = lunch_productService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_productMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


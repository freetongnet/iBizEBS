package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_cashbox_lineDTO]
 */
@Data
public class Account_cashbox_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DEFAULT_POS_ID]
     *
     */
    @JSONField(name = "default_pos_id")
    @JsonProperty("default_pos_id")
    private Integer defaultPosId;

    /**
     * 属性 [SUBTOTAL]
     *
     */
    @JSONField(name = "subtotal")
    @JsonProperty("subtotal")
    private Double subtotal;

    /**
     * 属性 [COIN_VALUE]
     *
     */
    @JSONField(name = "coin_value")
    @JsonProperty("coin_value")
    @NotNull(message = "[硬币／账单　价值]不允许为空!")
    private Double coinValue;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [NUMBER]
     *
     */
    @JSONField(name = "number")
    @JsonProperty("number")
    private Integer number;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CASHBOX_ID]
     *
     */
    @JSONField(name = "cashbox_id")
    @JsonProperty("cashbox_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long cashboxId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;


    /**
     * 设置 [DEFAULT_POS_ID]
     */
    public void setDefaultPosId(Integer  defaultPosId){
        this.defaultPosId = defaultPosId ;
        this.modify("default_pos_id",defaultPosId);
    }

    /**
     * 设置 [COIN_VALUE]
     */
    public void setCoinValue(Double  coinValue){
        this.coinValue = coinValue ;
        this.modify("coin_value",coinValue);
    }

    /**
     * 设置 [NUMBER]
     */
    public void setNumber(Integer  number){
        this.number = number ;
        this.modify("number",number);
    }

    /**
     * 设置 [CASHBOX_ID]
     */
    public void setCashboxId(Long  cashboxId){
        this.cashboxId = cashboxId ;
        this.modify("cashbox_id",cashboxId);
    }


}



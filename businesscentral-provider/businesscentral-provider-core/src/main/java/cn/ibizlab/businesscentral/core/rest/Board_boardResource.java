package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_board.domain.Board_board;
import cn.ibizlab.businesscentral.core.odoo_board.service.IBoard_boardService;
import cn.ibizlab.businesscentral.core.odoo_board.filter.Board_boardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"仪表板" })
@RestController("Core-board_board")
@RequestMapping("")
public class Board_boardResource {

    @Autowired
    public IBoard_boardService board_boardService;

    @Autowired
    @Lazy
    public Board_boardMapping board_boardMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-Create-all')")
    @ApiOperation(value = "新建仪表板", tags = {"仪表板" },  notes = "新建仪表板")
	@RequestMapping(method = RequestMethod.POST, value = "/board_boards")
    public ResponseEntity<Board_boardDTO> create(@Validated @RequestBody Board_boardDTO board_boarddto) {
        Board_board domain = board_boardMapping.toDomain(board_boarddto);
		board_boardService.create(domain);
        Board_boardDTO dto = board_boardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-Create-all')")
    @ApiOperation(value = "批量新建仪表板", tags = {"仪表板" },  notes = "批量新建仪表板")
	@RequestMapping(method = RequestMethod.POST, value = "/board_boards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Board_boardDTO> board_boarddtos) {
        board_boardService.createBatch(board_boardMapping.toDomain(board_boarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-Update-all')")
    @ApiOperation(value = "更新仪表板", tags = {"仪表板" },  notes = "更新仪表板")
	@RequestMapping(method = RequestMethod.PUT, value = "/board_boards/{board_board_id}")
    public ResponseEntity<Board_boardDTO> update(@PathVariable("board_board_id") Long board_board_id, @RequestBody Board_boardDTO board_boarddto) {
		Board_board domain  = board_boardMapping.toDomain(board_boarddto);
        domain .setId(board_board_id);
		board_boardService.update(domain );
		Board_boardDTO dto = board_boardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-Update-all')")
    @ApiOperation(value = "批量更新仪表板", tags = {"仪表板" },  notes = "批量更新仪表板")
	@RequestMapping(method = RequestMethod.PUT, value = "/board_boards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Board_boardDTO> board_boarddtos) {
        board_boardService.updateBatch(board_boardMapping.toDomain(board_boarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-Remove-all')")
    @ApiOperation(value = "删除仪表板", tags = {"仪表板" },  notes = "删除仪表板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/board_boards/{board_board_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("board_board_id") Long board_board_id) {
         return ResponseEntity.status(HttpStatus.OK).body(board_boardService.remove(board_board_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-Remove-all')")
    @ApiOperation(value = "批量删除仪表板", tags = {"仪表板" },  notes = "批量删除仪表板")
	@RequestMapping(method = RequestMethod.DELETE, value = "/board_boards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        board_boardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-Get-all')")
    @ApiOperation(value = "获取仪表板", tags = {"仪表板" },  notes = "获取仪表板")
	@RequestMapping(method = RequestMethod.GET, value = "/board_boards/{board_board_id}")
    public ResponseEntity<Board_boardDTO> get(@PathVariable("board_board_id") Long board_board_id) {
        Board_board domain = board_boardService.get(board_board_id);
        Board_boardDTO dto = board_boardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取仪表板草稿", tags = {"仪表板" },  notes = "获取仪表板草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/board_boards/getdraft")
    public ResponseEntity<Board_boardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(board_boardMapping.toDto(board_boardService.getDraft(new Board_board())));
    }

    @ApiOperation(value = "检查仪表板", tags = {"仪表板" },  notes = "检查仪表板")
	@RequestMapping(method = RequestMethod.POST, value = "/board_boards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Board_boardDTO board_boarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(board_boardService.checkKey(board_boardMapping.toDomain(board_boarddto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-Save-all')")
    @ApiOperation(value = "保存仪表板", tags = {"仪表板" },  notes = "保存仪表板")
	@RequestMapping(method = RequestMethod.POST, value = "/board_boards/save")
    public ResponseEntity<Boolean> save(@RequestBody Board_boardDTO board_boarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(board_boardService.save(board_boardMapping.toDomain(board_boarddto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-Save-all')")
    @ApiOperation(value = "批量保存仪表板", tags = {"仪表板" },  notes = "批量保存仪表板")
	@RequestMapping(method = RequestMethod.POST, value = "/board_boards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Board_boardDTO> board_boarddtos) {
        board_boardService.saveBatch(board_boardMapping.toDomain(board_boarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"仪表板" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/board_boards/fetchdefault")
	public ResponseEntity<List<Board_boardDTO>> fetchDefault(Board_boardSearchContext context) {
        Page<Board_board> domains = board_boardService.searchDefault(context) ;
        List<Board_boardDTO> list = board_boardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Board_board-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"仪表板" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/board_boards/searchdefault")
	public ResponseEntity<Page<Board_boardDTO>> searchDefault(@RequestBody Board_boardSearchContext context) {
        Page<Board_board> domains = board_boardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(board_boardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


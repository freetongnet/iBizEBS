package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Survey_user_input_lineDTO]
 */
@Data
public class Survey_user_input_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [VALUE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "value_date" , format="yyyy-MM-dd")
    @JsonProperty("value_date")
    private Timestamp valueDate;

    /**
     * 属性 [VALUE_FREE_TEXT]
     *
     */
    @JSONField(name = "value_free_text")
    @JsonProperty("value_free_text")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String valueFreeText;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [DATE_CREATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_create" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_create")
    @NotNull(message = "[创建日期]不允许为空!")
    private Timestamp dateCreate;

    /**
     * 属性 [SKIPPED]
     *
     */
    @JSONField(name = "skipped")
    @JsonProperty("skipped")
    private Boolean skipped;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [QUIZZ_MARK]
     *
     */
    @JSONField(name = "quizz_mark")
    @JsonProperty("quizz_mark")
    private Double quizzMark;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [ANSWER_TYPE]
     *
     */
    @JSONField(name = "answer_type")
    @JsonProperty("answer_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String answerType;

    /**
     * 属性 [VALUE_TEXT]
     *
     */
    @JSONField(name = "value_text")
    @JsonProperty("value_text")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String valueText;

    /**
     * 属性 [VALUE_NUMBER]
     *
     */
    @JSONField(name = "value_number")
    @JsonProperty("value_number")
    private Double valueNumber;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [PAGE_ID]
     *
     */
    @JSONField(name = "page_id")
    @JsonProperty("page_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pageId;

    /**
     * 属性 [USER_INPUT_ID]
     *
     */
    @JSONField(name = "user_input_id")
    @JsonProperty("user_input_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[用户输入]不允许为空!")
    private Long userInputId;

    /**
     * 属性 [VALUE_SUGGESTED]
     *
     */
    @JSONField(name = "value_suggested")
    @JsonProperty("value_suggested")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long valueSuggested;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [VALUE_SUGGESTED_ROW]
     *
     */
    @JSONField(name = "value_suggested_row")
    @JsonProperty("value_suggested_row")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long valueSuggestedRow;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [QUESTION_ID]
     *
     */
    @JSONField(name = "question_id")
    @JsonProperty("question_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[疑问]不允许为空!")
    private Long questionId;

    /**
     * 属性 [SURVEY_ID]
     *
     */
    @JSONField(name = "survey_id")
    @JsonProperty("survey_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long surveyId;


    /**
     * 设置 [VALUE_DATE]
     */
    public void setValueDate(Timestamp  valueDate){
        this.valueDate = valueDate ;
        this.modify("value_date",valueDate);
    }

    /**
     * 设置 [VALUE_FREE_TEXT]
     */
    public void setValueFreeText(String  valueFreeText){
        this.valueFreeText = valueFreeText ;
        this.modify("value_free_text",valueFreeText);
    }

    /**
     * 设置 [DATE_CREATE]
     */
    public void setDateCreate(Timestamp  dateCreate){
        this.dateCreate = dateCreate ;
        this.modify("date_create",dateCreate);
    }

    /**
     * 设置 [SKIPPED]
     */
    public void setSkipped(Boolean  skipped){
        this.skipped = skipped ;
        this.modify("skipped",skipped);
    }

    /**
     * 设置 [QUIZZ_MARK]
     */
    public void setQuizzMark(Double  quizzMark){
        this.quizzMark = quizzMark ;
        this.modify("quizz_mark",quizzMark);
    }

    /**
     * 设置 [ANSWER_TYPE]
     */
    public void setAnswerType(String  answerType){
        this.answerType = answerType ;
        this.modify("answer_type",answerType);
    }

    /**
     * 设置 [VALUE_TEXT]
     */
    public void setValueText(String  valueText){
        this.valueText = valueText ;
        this.modify("value_text",valueText);
    }

    /**
     * 设置 [VALUE_NUMBER]
     */
    public void setValueNumber(Double  valueNumber){
        this.valueNumber = valueNumber ;
        this.modify("value_number",valueNumber);
    }

    /**
     * 设置 [USER_INPUT_ID]
     */
    public void setUserInputId(Long  userInputId){
        this.userInputId = userInputId ;
        this.modify("user_input_id",userInputId);
    }

    /**
     * 设置 [VALUE_SUGGESTED]
     */
    public void setValueSuggested(Long  valueSuggested){
        this.valueSuggested = valueSuggested ;
        this.modify("value_suggested",valueSuggested);
    }

    /**
     * 设置 [VALUE_SUGGESTED_ROW]
     */
    public void setValueSuggestedRow(Long  valueSuggestedRow){
        this.valueSuggestedRow = valueSuggestedRow ;
        this.modify("value_suggested_row",valueSuggestedRow);
    }

    /**
     * 设置 [QUESTION_ID]
     */
    public void setQuestionId(Long  questionId){
        this.questionId = questionId ;
        this.modify("question_id",questionId);
    }

    /**
     * 设置 [SURVEY_ID]
     */
    public void setSurveyId(Long  surveyId){
        this.surveyId = surveyId ;
        this.modify("survey_id",surveyId);
    }


}



package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.businesscentral.core.dto.Event_event_ticketDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreEvent_event_ticketMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Event_event_ticketMapping extends MappingBase<Event_event_ticketDTO, Event_event_ticket> {


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_messageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"消息" })
@RestController("Core-mail_message")
@RequestMapping("")
public class Mail_messageResource {

    @Autowired
    public IMail_messageService mail_messageService;

    @Autowired
    @Lazy
    public Mail_messageMapping mail_messageMapping;

    @PreAuthorize("hasPermission(this.mail_messageMapping.toDomain(#mail_messagedto),'iBizBusinessCentral-Mail_message-Create')")
    @ApiOperation(value = "新建消息", tags = {"消息" },  notes = "新建消息")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages")
    public ResponseEntity<Mail_messageDTO> create(@Validated @RequestBody Mail_messageDTO mail_messagedto) {
        Mail_message domain = mail_messageMapping.toDomain(mail_messagedto);
		mail_messageService.create(domain);
        Mail_messageDTO dto = mail_messageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_messageMapping.toDomain(#mail_messagedtos),'iBizBusinessCentral-Mail_message-Create')")
    @ApiOperation(value = "批量新建消息", tags = {"消息" },  notes = "批量新建消息")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_messageDTO> mail_messagedtos) {
        mail_messageService.createBatch(mail_messageMapping.toDomain(mail_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_message" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_messageService.get(#mail_message_id),'iBizBusinessCentral-Mail_message-Update')")
    @ApiOperation(value = "更新消息", tags = {"消息" },  notes = "更新消息")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/{mail_message_id}")
    public ResponseEntity<Mail_messageDTO> update(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody Mail_messageDTO mail_messagedto) {
		Mail_message domain  = mail_messageMapping.toDomain(mail_messagedto);
        domain .setId(mail_message_id);
		mail_messageService.update(domain );
		Mail_messageDTO dto = mail_messageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_messageService.getMailMessageByEntities(this.mail_messageMapping.toDomain(#mail_messagedtos)),'iBizBusinessCentral-Mail_message-Update')")
    @ApiOperation(value = "批量更新消息", tags = {"消息" },  notes = "批量更新消息")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_messageDTO> mail_messagedtos) {
        mail_messageService.updateBatch(mail_messageMapping.toDomain(mail_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_messageService.get(#mail_message_id),'iBizBusinessCentral-Mail_message-Remove')")
    @ApiOperation(value = "删除消息", tags = {"消息" },  notes = "删除消息")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/{mail_message_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_message_id") Long mail_message_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_messageService.remove(mail_message_id));
    }

    @PreAuthorize("hasPermission(this.mail_messageService.getMailMessageByIds(#ids),'iBizBusinessCentral-Mail_message-Remove')")
    @ApiOperation(value = "批量删除消息", tags = {"消息" },  notes = "批量删除消息")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_messageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_messageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_message-Get')")
    @ApiOperation(value = "获取消息", tags = {"消息" },  notes = "获取消息")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_messages/{mail_message_id}")
    public ResponseEntity<Mail_messageDTO> get(@PathVariable("mail_message_id") Long mail_message_id) {
        Mail_message domain = mail_messageService.get(mail_message_id);
        Mail_messageDTO dto = mail_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取消息草稿", tags = {"消息" },  notes = "获取消息草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_messages/getdraft")
    public ResponseEntity<Mail_messageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_messageMapping.toDto(mail_messageService.getDraft(new Mail_message())));
    }

    @ApiOperation(value = "检查消息", tags = {"消息" },  notes = "检查消息")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_messageDTO mail_messagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_messageService.checkKey(mail_messageMapping.toDomain(mail_messagedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message-Generate_tracking_message_id-all')")
    @ApiOperation(value = "generate_tracking_message_id", tags = {"消息" },  notes = "generate_tracking_message_id")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{mail_message_id}/generate_tracking_message_id")
    public ResponseEntity<Mail_messageDTO> generate_tracking_message_id(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody Mail_messageDTO mail_messagedto) {
        Mail_message domain = mail_messageMapping.toDomain(mail_messagedto);
        domain.setId(mail_message_id);
        domain = mail_messageService.generate_tracking_message_id(domain);
        mail_messagedto = mail_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(mail_messagedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message-Post_message-all')")
    @ApiOperation(value = "发送消息", tags = {"消息" },  notes = "发送消息")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{mail_message_id}/post_message")
    public ResponseEntity<Mail_messageDTO> post_message(@PathVariable("mail_message_id") Long mail_message_id, @RequestBody Mail_messageDTO mail_messagedto) {
        Mail_message domain = mail_messageMapping.toDomain(mail_messagedto);
        domain.setId(mail_message_id);
        domain = mail_messageService.post_message(domain);
        mail_messagedto = mail_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(mail_messagedto);
    }

    @PreAuthorize("hasPermission(this.mail_messageMapping.toDomain(#mail_messagedto),'iBizBusinessCentral-Mail_message-Save')")
    @ApiOperation(value = "保存消息", tags = {"消息" },  notes = "保存消息")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_messageDTO mail_messagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_messageService.save(mail_messageMapping.toDomain(mail_messagedto)));
    }

    @PreAuthorize("hasPermission(this.mail_messageMapping.toDomain(#mail_messagedtos),'iBizBusinessCentral-Mail_message-Save')")
    @ApiOperation(value = "批量保存消息", tags = {"消息" },  notes = "批量保存消息")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_messages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_messageDTO> mail_messagedtos) {
        mail_messageService.saveBatch(mail_messageMapping.toDomain(mail_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message-searchByRes-all') and hasPermission(#context,'iBizBusinessCentral-Mail_message-Get')")
	@ApiOperation(value = "获取ByRes", tags = {"消息" } ,notes = "获取ByRes")
    @RequestMapping(method= RequestMethod.GET , value="/mail_messages/fetchbyres")
	public ResponseEntity<List<Mail_messageDTO>> fetchByRes(Mail_messageSearchContext context) {
        Page<Mail_message> domains = mail_messageService.searchByRes(context) ;
        List<Mail_messageDTO> list = mail_messageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message-searchByRes-all') and hasPermission(#context,'iBizBusinessCentral-Mail_message-Get')")
	@ApiOperation(value = "查询ByRes", tags = {"消息" } ,notes = "查询ByRes")
    @RequestMapping(method= RequestMethod.POST , value="/mail_messages/searchbyres")
	public ResponseEntity<Page<Mail_messageDTO>> searchByRes(@RequestBody Mail_messageSearchContext context) {
        Page<Mail_message> domains = mail_messageService.searchByRes(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_messageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_message-Get')")
	@ApiOperation(value = "获取数据集", tags = {"消息" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_messages/fetchdefault")
	public ResponseEntity<List<Mail_messageDTO>> fetchDefault(Mail_messageSearchContext context) {
        Page<Mail_message> domains = mail_messageService.searchDefault(context) ;
        List<Mail_messageDTO> list = mail_messageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_message-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_message-Get')")
	@ApiOperation(value = "查询数据集", tags = {"消息" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_messages/searchdefault")
	public ResponseEntity<Page<Mail_messageDTO>> searchDefault(@RequestBody Mail_messageSearchContext context) {
        Page<Mail_message> domains = mail_messageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_messageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


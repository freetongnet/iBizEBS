package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_taskService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_taskSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Maintenance Task" })
@RestController("Core-mro_task")
@RequestMapping("")
public class Mro_taskResource {

    @Autowired
    public IMro_taskService mro_taskService;

    @Autowired
    @Lazy
    public Mro_taskMapping mro_taskMapping;

    @PreAuthorize("hasPermission(this.mro_taskMapping.toDomain(#mro_taskdto),'iBizBusinessCentral-Mro_task-Create')")
    @ApiOperation(value = "新建Maintenance Task", tags = {"Maintenance Task" },  notes = "新建Maintenance Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_tasks")
    public ResponseEntity<Mro_taskDTO> create(@Validated @RequestBody Mro_taskDTO mro_taskdto) {
        Mro_task domain = mro_taskMapping.toDomain(mro_taskdto);
		mro_taskService.create(domain);
        Mro_taskDTO dto = mro_taskMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_taskMapping.toDomain(#mro_taskdtos),'iBizBusinessCentral-Mro_task-Create')")
    @ApiOperation(value = "批量新建Maintenance Task", tags = {"Maintenance Task" },  notes = "批量新建Maintenance Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_tasks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_taskDTO> mro_taskdtos) {
        mro_taskService.createBatch(mro_taskMapping.toDomain(mro_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_task" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_taskService.get(#mro_task_id),'iBizBusinessCentral-Mro_task-Update')")
    @ApiOperation(value = "更新Maintenance Task", tags = {"Maintenance Task" },  notes = "更新Maintenance Task")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_tasks/{mro_task_id}")
    public ResponseEntity<Mro_taskDTO> update(@PathVariable("mro_task_id") Long mro_task_id, @RequestBody Mro_taskDTO mro_taskdto) {
		Mro_task domain  = mro_taskMapping.toDomain(mro_taskdto);
        domain .setId(mro_task_id);
		mro_taskService.update(domain );
		Mro_taskDTO dto = mro_taskMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_taskService.getMroTaskByEntities(this.mro_taskMapping.toDomain(#mro_taskdtos)),'iBizBusinessCentral-Mro_task-Update')")
    @ApiOperation(value = "批量更新Maintenance Task", tags = {"Maintenance Task" },  notes = "批量更新Maintenance Task")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_tasks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_taskDTO> mro_taskdtos) {
        mro_taskService.updateBatch(mro_taskMapping.toDomain(mro_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_taskService.get(#mro_task_id),'iBizBusinessCentral-Mro_task-Remove')")
    @ApiOperation(value = "删除Maintenance Task", tags = {"Maintenance Task" },  notes = "删除Maintenance Task")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_tasks/{mro_task_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_task_id") Long mro_task_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_taskService.remove(mro_task_id));
    }

    @PreAuthorize("hasPermission(this.mro_taskService.getMroTaskByIds(#ids),'iBizBusinessCentral-Mro_task-Remove')")
    @ApiOperation(value = "批量删除Maintenance Task", tags = {"Maintenance Task" },  notes = "批量删除Maintenance Task")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_tasks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_taskService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_taskMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_task-Get')")
    @ApiOperation(value = "获取Maintenance Task", tags = {"Maintenance Task" },  notes = "获取Maintenance Task")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_tasks/{mro_task_id}")
    public ResponseEntity<Mro_taskDTO> get(@PathVariable("mro_task_id") Long mro_task_id) {
        Mro_task domain = mro_taskService.get(mro_task_id);
        Mro_taskDTO dto = mro_taskMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Maintenance Task草稿", tags = {"Maintenance Task" },  notes = "获取Maintenance Task草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_tasks/getdraft")
    public ResponseEntity<Mro_taskDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_taskMapping.toDto(mro_taskService.getDraft(new Mro_task())));
    }

    @ApiOperation(value = "检查Maintenance Task", tags = {"Maintenance Task" },  notes = "检查Maintenance Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_tasks/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_taskDTO mro_taskdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_taskService.checkKey(mro_taskMapping.toDomain(mro_taskdto)));
    }

    @PreAuthorize("hasPermission(this.mro_taskMapping.toDomain(#mro_taskdto),'iBizBusinessCentral-Mro_task-Save')")
    @ApiOperation(value = "保存Maintenance Task", tags = {"Maintenance Task" },  notes = "保存Maintenance Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_tasks/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_taskDTO mro_taskdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_taskService.save(mro_taskMapping.toDomain(mro_taskdto)));
    }

    @PreAuthorize("hasPermission(this.mro_taskMapping.toDomain(#mro_taskdtos),'iBizBusinessCentral-Mro_task-Save')")
    @ApiOperation(value = "批量保存Maintenance Task", tags = {"Maintenance Task" },  notes = "批量保存Maintenance Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_tasks/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_taskDTO> mro_taskdtos) {
        mro_taskService.saveBatch(mro_taskMapping.toDomain(mro_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_task-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_task-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Maintenance Task" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_tasks/fetchdefault")
	public ResponseEntity<List<Mro_taskDTO>> fetchDefault(Mro_taskSearchContext context) {
        Page<Mro_task> domains = mro_taskService.searchDefault(context) ;
        List<Mro_taskDTO> list = mro_taskMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_task-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_task-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Maintenance Task" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_tasks/searchdefault")
	public ResponseEntity<Page<Mro_taskDTO>> searchDefault(@RequestBody Mro_taskSearchContext context) {
        Page<Mro_task> domains = mro_taskService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_taskMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


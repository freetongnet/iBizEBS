package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Base_partner_merge_automatic_wizardDTO]
 */
@Data
public class Base_partner_merge_automatic_wizardDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MAXIMUM_GROUP]
     *
     */
    @JSONField(name = "maximum_group")
    @JsonProperty("maximum_group")
    private Integer maximumGroup;

    /**
     * 属性 [GROUP_BY_PARENT_ID]
     *
     */
    @JSONField(name = "group_by_parent_id")
    @JsonProperty("group_by_parent_id")
    private Boolean groupByParentId;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [NUMBER_GROUP]
     *
     */
    @JSONField(name = "number_group")
    @JsonProperty("number_group")
    private Integer numberGroup;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String lineIds;

    /**
     * 属性 [GROUP_BY_VAT]
     *
     */
    @JSONField(name = "group_by_vat")
    @JsonProperty("group_by_vat")
    private Boolean groupByVat;

    /**
     * 属性 [EXCLUDE_CONTACT]
     *
     */
    @JSONField(name = "exclude_contact")
    @JsonProperty("exclude_contact")
    private Boolean excludeContact;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String partnerIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [GROUP_BY_EMAIL]
     *
     */
    @JSONField(name = "group_by_email")
    @JsonProperty("group_by_email")
    private Boolean groupByEmail;

    /**
     * 属性 [GROUP_BY_IS_COMPANY]
     *
     */
    @JSONField(name = "group_by_is_company")
    @JsonProperty("group_by_is_company")
    private Boolean groupByIsCompany;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @NotBlank(message = "[省/ 州]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [EXCLUDE_JOURNAL_ITEM]
     *
     */
    @JSONField(name = "exclude_journal_item")
    @JsonProperty("exclude_journal_item")
    private Boolean excludeJournalItem;

    /**
     * 属性 [GROUP_BY_NAME]
     *
     */
    @JSONField(name = "group_by_name")
    @JsonProperty("group_by_name")
    private Boolean groupByName;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [DST_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "dst_partner_id_text")
    @JsonProperty("dst_partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String dstPartnerIdText;

    /**
     * 属性 [DST_PARTNER_ID]
     *
     */
    @JSONField(name = "dst_partner_id")
    @JsonProperty("dst_partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long dstPartnerId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CURRENT_LINE_ID]
     *
     */
    @JSONField(name = "current_line_id")
    @JsonProperty("current_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currentLineId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [MAXIMUM_GROUP]
     */
    public void setMaximumGroup(Integer  maximumGroup){
        this.maximumGroup = maximumGroup ;
        this.modify("maximum_group",maximumGroup);
    }

    /**
     * 设置 [GROUP_BY_PARENT_ID]
     */
    public void setGroupByParentId(Boolean  groupByParentId){
        this.groupByParentId = groupByParentId ;
        this.modify("group_by_parent_id",groupByParentId);
    }

    /**
     * 设置 [NUMBER_GROUP]
     */
    public void setNumberGroup(Integer  numberGroup){
        this.numberGroup = numberGroup ;
        this.modify("number_group",numberGroup);
    }

    /**
     * 设置 [GROUP_BY_VAT]
     */
    public void setGroupByVat(Boolean  groupByVat){
        this.groupByVat = groupByVat ;
        this.modify("group_by_vat",groupByVat);
    }

    /**
     * 设置 [EXCLUDE_CONTACT]
     */
    public void setExcludeContact(Boolean  excludeContact){
        this.excludeContact = excludeContact ;
        this.modify("exclude_contact",excludeContact);
    }

    /**
     * 设置 [GROUP_BY_EMAIL]
     */
    public void setGroupByEmail(Boolean  groupByEmail){
        this.groupByEmail = groupByEmail ;
        this.modify("group_by_email",groupByEmail);
    }

    /**
     * 设置 [GROUP_BY_IS_COMPANY]
     */
    public void setGroupByIsCompany(Boolean  groupByIsCompany){
        this.groupByIsCompany = groupByIsCompany ;
        this.modify("group_by_is_company",groupByIsCompany);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [EXCLUDE_JOURNAL_ITEM]
     */
    public void setExcludeJournalItem(Boolean  excludeJournalItem){
        this.excludeJournalItem = excludeJournalItem ;
        this.modify("exclude_journal_item",excludeJournalItem);
    }

    /**
     * 设置 [GROUP_BY_NAME]
     */
    public void setGroupByName(Boolean  groupByName){
        this.groupByName = groupByName ;
        this.modify("group_by_name",groupByName);
    }

    /**
     * 设置 [DST_PARTNER_ID]
     */
    public void setDstPartnerId(Long  dstPartnerId){
        this.dstPartnerId = dstPartnerId ;
        this.modify("dst_partner_id",dstPartnerId);
    }

    /**
     * 设置 [CURRENT_LINE_ID]
     */
    public void setCurrentLineId(Long  currentLineId){
        this.currentLineId = currentLineId ;
        this.modify("current_line_id",currentLineId);
    }


}



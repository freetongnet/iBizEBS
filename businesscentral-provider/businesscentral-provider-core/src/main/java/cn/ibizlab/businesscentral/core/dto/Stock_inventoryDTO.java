package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Stock_inventoryDTO]
 */
@Data
public class Stock_inventoryDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String lineIds;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [FILTER]
     *
     */
    @JSONField(name = "filter")
    @JsonProperty("filter")
    @NotBlank(message = "[库存]不允许为空!")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String filter;

    /**
     * 属性 [EXHAUSTED]
     *
     */
    @JSONField(name = "exhausted")
    @JsonProperty("exhausted")
    private Boolean exhausted;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[库存编号]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [TOTAL_QTY]
     *
     */
    @JSONField(name = "total_qty")
    @JsonProperty("total_qty")
    private Double totalQty;

    /**
     * 属性 [ACCOUNTING_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "accounting_date" , format="yyyy-MM-dd")
    @JsonProperty("accounting_date")
    private Timestamp accountingDate;

    /**
     * 属性 [MOVE_IDS]
     *
     */
    @JSONField(name = "move_ids")
    @JsonProperty("move_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String moveIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    @NotNull(message = "[库存日期]不允许为空!")
    private Timestamp date;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String categoryIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [LOT_ID_TEXT]
     *
     */
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String lotIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String locationIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [PACKAGE_ID_TEXT]
     *
     */
    @JSONField(name = "package_id_text")
    @JsonProperty("package_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String packageIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[公司]不允许为空!")
    private Long companyId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[已盘点位置]不允许为空!")
    private Long locationId;

    /**
     * 属性 [LOT_ID]
     *
     */
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long lotId;

    /**
     * 属性 [PACKAGE_ID]
     *
     */
    @JSONField(name = "package_id")
    @JsonProperty("package_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long packageId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categoryId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;


    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [FILTER]
     */
    public void setFilter(String  filter){
        this.filter = filter ;
        this.modify("filter",filter);
    }

    /**
     * 设置 [EXHAUSTED]
     */
    public void setExhausted(Boolean  exhausted){
        this.exhausted = exhausted ;
        this.modify("exhausted",exhausted);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [ACCOUNTING_DATE]
     */
    public void setAccountingDate(Timestamp  accountingDate){
        this.accountingDate = accountingDate ;
        this.modify("accounting_date",accountingDate);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [LOCATION_ID]
     */
    public void setLocationId(Long  locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [LOT_ID]
     */
    public void setLotId(Long  lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }

    /**
     * 设置 [PACKAGE_ID]
     */
    public void setPackageId(Long  packageId){
        this.packageId = packageId ;
        this.modify("package_id",packageId);
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    public void setCategoryId(Long  categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }


}



package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_module_uninstallService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_module_uninstallSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"模块卸载" })
@RestController("Core-base_module_uninstall")
@RequestMapping("")
public class Base_module_uninstallResource {

    @Autowired
    public IBase_module_uninstallService base_module_uninstallService;

    @Autowired
    @Lazy
    public Base_module_uninstallMapping base_module_uninstallMapping;

    @PreAuthorize("hasPermission(this.base_module_uninstallMapping.toDomain(#base_module_uninstalldto),'iBizBusinessCentral-Base_module_uninstall-Create')")
    @ApiOperation(value = "新建模块卸载", tags = {"模块卸载" },  notes = "新建模块卸载")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls")
    public ResponseEntity<Base_module_uninstallDTO> create(@Validated @RequestBody Base_module_uninstallDTO base_module_uninstalldto) {
        Base_module_uninstall domain = base_module_uninstallMapping.toDomain(base_module_uninstalldto);
		base_module_uninstallService.create(domain);
        Base_module_uninstallDTO dto = base_module_uninstallMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_module_uninstallMapping.toDomain(#base_module_uninstalldtos),'iBizBusinessCentral-Base_module_uninstall-Create')")
    @ApiOperation(value = "批量新建模块卸载", tags = {"模块卸载" },  notes = "批量新建模块卸载")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_module_uninstallDTO> base_module_uninstalldtos) {
        base_module_uninstallService.createBatch(base_module_uninstallMapping.toDomain(base_module_uninstalldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_module_uninstall" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_module_uninstallService.get(#base_module_uninstall_id),'iBizBusinessCentral-Base_module_uninstall-Update')")
    @ApiOperation(value = "更新模块卸载", tags = {"模块卸载" },  notes = "更新模块卸载")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_uninstalls/{base_module_uninstall_id}")
    public ResponseEntity<Base_module_uninstallDTO> update(@PathVariable("base_module_uninstall_id") Long base_module_uninstall_id, @RequestBody Base_module_uninstallDTO base_module_uninstalldto) {
		Base_module_uninstall domain  = base_module_uninstallMapping.toDomain(base_module_uninstalldto);
        domain .setId(base_module_uninstall_id);
		base_module_uninstallService.update(domain );
		Base_module_uninstallDTO dto = base_module_uninstallMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_module_uninstallService.getBaseModuleUninstallByEntities(this.base_module_uninstallMapping.toDomain(#base_module_uninstalldtos)),'iBizBusinessCentral-Base_module_uninstall-Update')")
    @ApiOperation(value = "批量更新模块卸载", tags = {"模块卸载" },  notes = "批量更新模块卸载")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_module_uninstalls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_module_uninstallDTO> base_module_uninstalldtos) {
        base_module_uninstallService.updateBatch(base_module_uninstallMapping.toDomain(base_module_uninstalldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_module_uninstallService.get(#base_module_uninstall_id),'iBizBusinessCentral-Base_module_uninstall-Remove')")
    @ApiOperation(value = "删除模块卸载", tags = {"模块卸载" },  notes = "删除模块卸载")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_uninstalls/{base_module_uninstall_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_module_uninstall_id") Long base_module_uninstall_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_module_uninstallService.remove(base_module_uninstall_id));
    }

    @PreAuthorize("hasPermission(this.base_module_uninstallService.getBaseModuleUninstallByIds(#ids),'iBizBusinessCentral-Base_module_uninstall-Remove')")
    @ApiOperation(value = "批量删除模块卸载", tags = {"模块卸载" },  notes = "批量删除模块卸载")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_module_uninstalls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_module_uninstallService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_module_uninstallMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_module_uninstall-Get')")
    @ApiOperation(value = "获取模块卸载", tags = {"模块卸载" },  notes = "获取模块卸载")
	@RequestMapping(method = RequestMethod.GET, value = "/base_module_uninstalls/{base_module_uninstall_id}")
    public ResponseEntity<Base_module_uninstallDTO> get(@PathVariable("base_module_uninstall_id") Long base_module_uninstall_id) {
        Base_module_uninstall domain = base_module_uninstallService.get(base_module_uninstall_id);
        Base_module_uninstallDTO dto = base_module_uninstallMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取模块卸载草稿", tags = {"模块卸载" },  notes = "获取模块卸载草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_module_uninstalls/getdraft")
    public ResponseEntity<Base_module_uninstallDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_module_uninstallMapping.toDto(base_module_uninstallService.getDraft(new Base_module_uninstall())));
    }

    @ApiOperation(value = "检查模块卸载", tags = {"模块卸载" },  notes = "检查模块卸载")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_module_uninstallDTO base_module_uninstalldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_module_uninstallService.checkKey(base_module_uninstallMapping.toDomain(base_module_uninstalldto)));
    }

    @PreAuthorize("hasPermission(this.base_module_uninstallMapping.toDomain(#base_module_uninstalldto),'iBizBusinessCentral-Base_module_uninstall-Save')")
    @ApiOperation(value = "保存模块卸载", tags = {"模块卸载" },  notes = "保存模块卸载")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_module_uninstallDTO base_module_uninstalldto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_module_uninstallService.save(base_module_uninstallMapping.toDomain(base_module_uninstalldto)));
    }

    @PreAuthorize("hasPermission(this.base_module_uninstallMapping.toDomain(#base_module_uninstalldtos),'iBizBusinessCentral-Base_module_uninstall-Save')")
    @ApiOperation(value = "批量保存模块卸载", tags = {"模块卸载" },  notes = "批量保存模块卸载")
	@RequestMapping(method = RequestMethod.POST, value = "/base_module_uninstalls/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_module_uninstallDTO> base_module_uninstalldtos) {
        base_module_uninstallService.saveBatch(base_module_uninstallMapping.toDomain(base_module_uninstalldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_module_uninstall-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_module_uninstall-Get')")
	@ApiOperation(value = "获取数据集", tags = {"模块卸载" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_module_uninstalls/fetchdefault")
	public ResponseEntity<List<Base_module_uninstallDTO>> fetchDefault(Base_module_uninstallSearchContext context) {
        Page<Base_module_uninstall> domains = base_module_uninstallService.searchDefault(context) ;
        List<Base_module_uninstallDTO> list = base_module_uninstallMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_module_uninstall-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_module_uninstall-Get')")
	@ApiOperation(value = "查询数据集", tags = {"模块卸载" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_module_uninstalls/searchdefault")
	public ResponseEntity<Page<Base_module_uninstallDTO>> searchDefault(@RequestBody Base_module_uninstallSearchContext context) {
        Page<Base_module_uninstall> domains = base_module_uninstallService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_module_uninstallMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence_date_range;
import cn.ibizlab.businesscentral.core.dto.Ir_sequence_date_rangeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreIr_sequence_date_rangeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Ir_sequence_date_rangeMapping extends MappingBase<Ir_sequence_date_rangeDTO, Ir_sequence_date_range> {


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_type;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_typeService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_account_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"科目类型" })
@RestController("Core-account_account_type")
@RequestMapping("")
public class Account_account_typeResource {

    @Autowired
    public IAccount_account_typeService account_account_typeService;

    @Autowired
    @Lazy
    public Account_account_typeMapping account_account_typeMapping;

    @PreAuthorize("hasPermission(this.account_account_typeMapping.toDomain(#account_account_typedto),'iBizBusinessCentral-Account_account_type-Create')")
    @ApiOperation(value = "新建科目类型", tags = {"科目类型" },  notes = "新建科目类型")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_types")
    public ResponseEntity<Account_account_typeDTO> create(@Validated @RequestBody Account_account_typeDTO account_account_typedto) {
        Account_account_type domain = account_account_typeMapping.toDomain(account_account_typedto);
		account_account_typeService.create(domain);
        Account_account_typeDTO dto = account_account_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_account_typeMapping.toDomain(#account_account_typedtos),'iBizBusinessCentral-Account_account_type-Create')")
    @ApiOperation(value = "批量新建科目类型", tags = {"科目类型" },  notes = "批量新建科目类型")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_account_typeDTO> account_account_typedtos) {
        account_account_typeService.createBatch(account_account_typeMapping.toDomain(account_account_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_account_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_account_typeService.get(#account_account_type_id),'iBizBusinessCentral-Account_account_type-Update')")
    @ApiOperation(value = "更新科目类型", tags = {"科目类型" },  notes = "更新科目类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_account_types/{account_account_type_id}")
    public ResponseEntity<Account_account_typeDTO> update(@PathVariable("account_account_type_id") Long account_account_type_id, @RequestBody Account_account_typeDTO account_account_typedto) {
		Account_account_type domain  = account_account_typeMapping.toDomain(account_account_typedto);
        domain .setId(account_account_type_id);
		account_account_typeService.update(domain );
		Account_account_typeDTO dto = account_account_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_account_typeService.getAccountAccountTypeByEntities(this.account_account_typeMapping.toDomain(#account_account_typedtos)),'iBizBusinessCentral-Account_account_type-Update')")
    @ApiOperation(value = "批量更新科目类型", tags = {"科目类型" },  notes = "批量更新科目类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_account_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_account_typeDTO> account_account_typedtos) {
        account_account_typeService.updateBatch(account_account_typeMapping.toDomain(account_account_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_account_typeService.get(#account_account_type_id),'iBizBusinessCentral-Account_account_type-Remove')")
    @ApiOperation(value = "删除科目类型", tags = {"科目类型" },  notes = "删除科目类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_account_types/{account_account_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_account_type_id") Long account_account_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_account_typeService.remove(account_account_type_id));
    }

    @PreAuthorize("hasPermission(this.account_account_typeService.getAccountAccountTypeByIds(#ids),'iBizBusinessCentral-Account_account_type-Remove')")
    @ApiOperation(value = "批量删除科目类型", tags = {"科目类型" },  notes = "批量删除科目类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_account_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_account_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_account_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_account_type-Get')")
    @ApiOperation(value = "获取科目类型", tags = {"科目类型" },  notes = "获取科目类型")
	@RequestMapping(method = RequestMethod.GET, value = "/account_account_types/{account_account_type_id}")
    public ResponseEntity<Account_account_typeDTO> get(@PathVariable("account_account_type_id") Long account_account_type_id) {
        Account_account_type domain = account_account_typeService.get(account_account_type_id);
        Account_account_typeDTO dto = account_account_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取科目类型草稿", tags = {"科目类型" },  notes = "获取科目类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_account_types/getdraft")
    public ResponseEntity<Account_account_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_account_typeMapping.toDto(account_account_typeService.getDraft(new Account_account_type())));
    }

    @ApiOperation(value = "检查科目类型", tags = {"科目类型" },  notes = "检查科目类型")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_account_typeDTO account_account_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_account_typeService.checkKey(account_account_typeMapping.toDomain(account_account_typedto)));
    }

    @PreAuthorize("hasPermission(this.account_account_typeMapping.toDomain(#account_account_typedto),'iBizBusinessCentral-Account_account_type-Save')")
    @ApiOperation(value = "保存科目类型", tags = {"科目类型" },  notes = "保存科目类型")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_account_typeDTO account_account_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_account_typeService.save(account_account_typeMapping.toDomain(account_account_typedto)));
    }

    @PreAuthorize("hasPermission(this.account_account_typeMapping.toDomain(#account_account_typedtos),'iBizBusinessCentral-Account_account_type-Save')")
    @ApiOperation(value = "批量保存科目类型", tags = {"科目类型" },  notes = "批量保存科目类型")
	@RequestMapping(method = RequestMethod.POST, value = "/account_account_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_account_typeDTO> account_account_typedtos) {
        account_account_typeService.saveBatch(account_account_typeMapping.toDomain(account_account_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_account_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_account_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"科目类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_account_types/fetchdefault")
	public ResponseEntity<List<Account_account_typeDTO>> fetchDefault(Account_account_typeSearchContext context) {
        Page<Account_account_type> domains = account_account_typeService.searchDefault(context) ;
        List<Account_account_typeDTO> list = account_account_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_account_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_account_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"科目类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_account_types/searchdefault")
	public ResponseEntity<Page<Account_account_typeDTO>> searchDefault(@RequestBody Account_account_typeSearchContext context) {
        Page<Account_account_type> domains = account_account_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_account_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_mixinService;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"资源装饰" })
@RestController("Core-resource_mixin")
@RequestMapping("")
public class Resource_mixinResource {

    @Autowired
    public IResource_mixinService resource_mixinService;

    @Autowired
    @Lazy
    public Resource_mixinMapping resource_mixinMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-Create-all')")
    @ApiOperation(value = "新建资源装饰", tags = {"资源装饰" },  notes = "新建资源装饰")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_mixins")
    public ResponseEntity<Resource_mixinDTO> create(@Validated @RequestBody Resource_mixinDTO resource_mixindto) {
        Resource_mixin domain = resource_mixinMapping.toDomain(resource_mixindto);
		resource_mixinService.create(domain);
        Resource_mixinDTO dto = resource_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-Create-all')")
    @ApiOperation(value = "批量新建资源装饰", tags = {"资源装饰" },  notes = "批量新建资源装饰")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_mixinDTO> resource_mixindtos) {
        resource_mixinService.createBatch(resource_mixinMapping.toDomain(resource_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-Update-all')")
    @ApiOperation(value = "更新资源装饰", tags = {"资源装饰" },  notes = "更新资源装饰")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_mixins/{resource_mixin_id}")
    public ResponseEntity<Resource_mixinDTO> update(@PathVariable("resource_mixin_id") Long resource_mixin_id, @RequestBody Resource_mixinDTO resource_mixindto) {
		Resource_mixin domain  = resource_mixinMapping.toDomain(resource_mixindto);
        domain .setId(resource_mixin_id);
		resource_mixinService.update(domain );
		Resource_mixinDTO dto = resource_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-Update-all')")
    @ApiOperation(value = "批量更新资源装饰", tags = {"资源装饰" },  notes = "批量更新资源装饰")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_mixinDTO> resource_mixindtos) {
        resource_mixinService.updateBatch(resource_mixinMapping.toDomain(resource_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-Remove-all')")
    @ApiOperation(value = "删除资源装饰", tags = {"资源装饰" },  notes = "删除资源装饰")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_mixins/{resource_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("resource_mixin_id") Long resource_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_mixinService.remove(resource_mixin_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-Remove-all')")
    @ApiOperation(value = "批量删除资源装饰", tags = {"资源装饰" },  notes = "批量删除资源装饰")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        resource_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-Get-all')")
    @ApiOperation(value = "获取资源装饰", tags = {"资源装饰" },  notes = "获取资源装饰")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_mixins/{resource_mixin_id}")
    public ResponseEntity<Resource_mixinDTO> get(@PathVariable("resource_mixin_id") Long resource_mixin_id) {
        Resource_mixin domain = resource_mixinService.get(resource_mixin_id);
        Resource_mixinDTO dto = resource_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取资源装饰草稿", tags = {"资源装饰" },  notes = "获取资源装饰草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_mixins/getdraft")
    public ResponseEntity<Resource_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(resource_mixinMapping.toDto(resource_mixinService.getDraft(new Resource_mixin())));
    }

    @ApiOperation(value = "检查资源装饰", tags = {"资源装饰" },  notes = "检查资源装饰")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Resource_mixinDTO resource_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(resource_mixinService.checkKey(resource_mixinMapping.toDomain(resource_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-Save-all')")
    @ApiOperation(value = "保存资源装饰", tags = {"资源装饰" },  notes = "保存资源装饰")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Resource_mixinDTO resource_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(resource_mixinService.save(resource_mixinMapping.toDomain(resource_mixindto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-Save-all')")
    @ApiOperation(value = "批量保存资源装饰", tags = {"资源装饰" },  notes = "批量保存资源装饰")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Resource_mixinDTO> resource_mixindtos) {
        resource_mixinService.saveBatch(resource_mixinMapping.toDomain(resource_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"资源装饰" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/resource_mixins/fetchdefault")
	public ResponseEntity<List<Resource_mixinDTO>> fetchDefault(Resource_mixinSearchContext context) {
        Page<Resource_mixin> domains = resource_mixinService.searchDefault(context) ;
        List<Resource_mixinDTO> list = resource_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_mixin-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"资源装饰" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/resource_mixins/searchdefault")
	public ResponseEntity<Page<Resource_mixinDTO>> searchDefault(@RequestBody Resource_mixinSearchContext context) {
        Page<Resource_mixin> domains = resource_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.businesscentral.core.dto.Website_seo_metadataDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreWebsite_seo_metadataMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Website_seo_metadataMapping extends MappingBase<Website_seo_metadataDTO, Website_seo_metadata> {


}


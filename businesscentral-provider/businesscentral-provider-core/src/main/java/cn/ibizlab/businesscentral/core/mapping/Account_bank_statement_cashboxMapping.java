package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_cashbox;
import cn.ibizlab.businesscentral.core.dto.Account_bank_statement_cashboxDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_bank_statement_cashboxMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_bank_statement_cashboxMapping extends MappingBase<Account_bank_statement_cashboxDTO, Account_bank_statement_cashbox> {


}


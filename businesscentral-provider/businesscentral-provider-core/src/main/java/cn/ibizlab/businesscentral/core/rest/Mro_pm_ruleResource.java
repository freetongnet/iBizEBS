package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_rule;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_ruleService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_ruleSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Preventive Maintenance Rule" })
@RestController("Core-mro_pm_rule")
@RequestMapping("")
public class Mro_pm_ruleResource {

    @Autowired
    public IMro_pm_ruleService mro_pm_ruleService;

    @Autowired
    @Lazy
    public Mro_pm_ruleMapping mro_pm_ruleMapping;

    @PreAuthorize("hasPermission(this.mro_pm_ruleMapping.toDomain(#mro_pm_ruledto),'iBizBusinessCentral-Mro_pm_rule-Create')")
    @ApiOperation(value = "新建Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "新建Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules")
    public ResponseEntity<Mro_pm_ruleDTO> create(@Validated @RequestBody Mro_pm_ruleDTO mro_pm_ruledto) {
        Mro_pm_rule domain = mro_pm_ruleMapping.toDomain(mro_pm_ruledto);
		mro_pm_ruleService.create(domain);
        Mro_pm_ruleDTO dto = mro_pm_ruleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_ruleMapping.toDomain(#mro_pm_ruledtos),'iBizBusinessCentral-Mro_pm_rule-Create')")
    @ApiOperation(value = "批量新建Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "批量新建Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_ruleDTO> mro_pm_ruledtos) {
        mro_pm_ruleService.createBatch(mro_pm_ruleMapping.toDomain(mro_pm_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_pm_rule" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_pm_ruleService.get(#mro_pm_rule_id),'iBizBusinessCentral-Mro_pm_rule-Update')")
    @ApiOperation(value = "更新Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "更新Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rules/{mro_pm_rule_id}")
    public ResponseEntity<Mro_pm_ruleDTO> update(@PathVariable("mro_pm_rule_id") Long mro_pm_rule_id, @RequestBody Mro_pm_ruleDTO mro_pm_ruledto) {
		Mro_pm_rule domain  = mro_pm_ruleMapping.toDomain(mro_pm_ruledto);
        domain .setId(mro_pm_rule_id);
		mro_pm_ruleService.update(domain );
		Mro_pm_ruleDTO dto = mro_pm_ruleMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_ruleService.getMroPmRuleByEntities(this.mro_pm_ruleMapping.toDomain(#mro_pm_ruledtos)),'iBizBusinessCentral-Mro_pm_rule-Update')")
    @ApiOperation(value = "批量更新Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "批量更新Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_rules/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_ruleDTO> mro_pm_ruledtos) {
        mro_pm_ruleService.updateBatch(mro_pm_ruleMapping.toDomain(mro_pm_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_pm_ruleService.get(#mro_pm_rule_id),'iBizBusinessCentral-Mro_pm_rule-Remove')")
    @ApiOperation(value = "删除Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "删除Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rules/{mro_pm_rule_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_rule_id") Long mro_pm_rule_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_ruleService.remove(mro_pm_rule_id));
    }

    @PreAuthorize("hasPermission(this.mro_pm_ruleService.getMroPmRuleByIds(#ids),'iBizBusinessCentral-Mro_pm_rule-Remove')")
    @ApiOperation(value = "批量删除Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "批量删除Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_rules/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_pm_ruleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_pm_ruleMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_pm_rule-Get')")
    @ApiOperation(value = "获取Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "获取Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rules/{mro_pm_rule_id}")
    public ResponseEntity<Mro_pm_ruleDTO> get(@PathVariable("mro_pm_rule_id") Long mro_pm_rule_id) {
        Mro_pm_rule domain = mro_pm_ruleService.get(mro_pm_rule_id);
        Mro_pm_ruleDTO dto = mro_pm_ruleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Preventive Maintenance Rule草稿", tags = {"Preventive Maintenance Rule" },  notes = "获取Preventive Maintenance Rule草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_rules/getdraft")
    public ResponseEntity<Mro_pm_ruleDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_ruleMapping.toDto(mro_pm_ruleService.getDraft(new Mro_pm_rule())));
    }

    @ApiOperation(value = "检查Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "检查Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_pm_ruleDTO mro_pm_ruledto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_pm_ruleService.checkKey(mro_pm_ruleMapping.toDomain(mro_pm_ruledto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_ruleMapping.toDomain(#mro_pm_ruledto),'iBizBusinessCentral-Mro_pm_rule-Save')")
    @ApiOperation(value = "保存Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "保存Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_pm_ruleDTO mro_pm_ruledto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_ruleService.save(mro_pm_ruleMapping.toDomain(mro_pm_ruledto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_ruleMapping.toDomain(#mro_pm_ruledtos),'iBizBusinessCentral-Mro_pm_rule-Save')")
    @ApiOperation(value = "批量保存Preventive Maintenance Rule", tags = {"Preventive Maintenance Rule" },  notes = "批量保存Preventive Maintenance Rule")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_rules/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_pm_ruleDTO> mro_pm_ruledtos) {
        mro_pm_ruleService.saveBatch(mro_pm_ruleMapping.toDomain(mro_pm_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_rule-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_rule-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Preventive Maintenance Rule" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_rules/fetchdefault")
	public ResponseEntity<List<Mro_pm_ruleDTO>> fetchDefault(Mro_pm_ruleSearchContext context) {
        Page<Mro_pm_rule> domains = mro_pm_ruleService.searchDefault(context) ;
        List<Mro_pm_ruleDTO> list = mro_pm_ruleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_rule-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_rule-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Preventive Maintenance Rule" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_pm_rules/searchdefault")
	public ResponseEntity<Page<Mro_pm_ruleDTO>> searchDefault(@RequestBody Mro_pm_ruleSearchContext context) {
        Page<Mro_pm_rule> domains = mro_pm_ruleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_ruleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_mass_mailing_campaignService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"群发邮件营销" })
@RestController("Core-mail_mass_mailing_campaign")
@RequestMapping("")
public class Mail_mass_mailing_campaignResource {

    @Autowired
    public IMail_mass_mailing_campaignService mail_mass_mailing_campaignService;

    @Autowired
    @Lazy
    public Mail_mass_mailing_campaignMapping mail_mass_mailing_campaignMapping;

    @PreAuthorize("hasPermission(this.mail_mass_mailing_campaignMapping.toDomain(#mail_mass_mailing_campaigndto),'iBizBusinessCentral-Mail_mass_mailing_campaign-Create')")
    @ApiOperation(value = "新建群发邮件营销", tags = {"群发邮件营销" },  notes = "新建群发邮件营销")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns")
    public ResponseEntity<Mail_mass_mailing_campaignDTO> create(@Validated @RequestBody Mail_mass_mailing_campaignDTO mail_mass_mailing_campaigndto) {
        Mail_mass_mailing_campaign domain = mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndto);
		mail_mass_mailing_campaignService.create(domain);
        Mail_mass_mailing_campaignDTO dto = mail_mass_mailing_campaignMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_campaignMapping.toDomain(#mail_mass_mailing_campaigndtos),'iBizBusinessCentral-Mail_mass_mailing_campaign-Create')")
    @ApiOperation(value = "批量新建群发邮件营销", tags = {"群发邮件营销" },  notes = "批量新建群发邮件营销")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_campaignDTO> mail_mass_mailing_campaigndtos) {
        mail_mass_mailing_campaignService.createBatch(mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_mass_mailing_campaign" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_mass_mailing_campaignService.get(#mail_mass_mailing_campaign_id),'iBizBusinessCentral-Mail_mass_mailing_campaign-Update')")
    @ApiOperation(value = "更新群发邮件营销", tags = {"群发邮件营销" },  notes = "更新群发邮件营销")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_campaigns/{mail_mass_mailing_campaign_id}")
    public ResponseEntity<Mail_mass_mailing_campaignDTO> update(@PathVariable("mail_mass_mailing_campaign_id") Long mail_mass_mailing_campaign_id, @RequestBody Mail_mass_mailing_campaignDTO mail_mass_mailing_campaigndto) {
		Mail_mass_mailing_campaign domain  = mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndto);
        domain .setId(mail_mass_mailing_campaign_id);
		mail_mass_mailing_campaignService.update(domain );
		Mail_mass_mailing_campaignDTO dto = mail_mass_mailing_campaignMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_campaignService.getMailMassMailingCampaignByEntities(this.mail_mass_mailing_campaignMapping.toDomain(#mail_mass_mailing_campaigndtos)),'iBizBusinessCentral-Mail_mass_mailing_campaign-Update')")
    @ApiOperation(value = "批量更新群发邮件营销", tags = {"群发邮件营销" },  notes = "批量更新群发邮件营销")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_campaigns/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_campaignDTO> mail_mass_mailing_campaigndtos) {
        mail_mass_mailing_campaignService.updateBatch(mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_campaignService.get(#mail_mass_mailing_campaign_id),'iBizBusinessCentral-Mail_mass_mailing_campaign-Remove')")
    @ApiOperation(value = "删除群发邮件营销", tags = {"群发邮件营销" },  notes = "删除群发邮件营销")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_campaigns/{mail_mass_mailing_campaign_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_campaign_id") Long mail_mass_mailing_campaign_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_campaignService.remove(mail_mass_mailing_campaign_id));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_campaignService.getMailMassMailingCampaignByIds(#ids),'iBizBusinessCentral-Mail_mass_mailing_campaign-Remove')")
    @ApiOperation(value = "批量删除群发邮件营销", tags = {"群发邮件营销" },  notes = "批量删除群发邮件营销")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_campaigns/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_mass_mailing_campaignService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_mass_mailing_campaignMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_mass_mailing_campaign-Get')")
    @ApiOperation(value = "获取群发邮件营销", tags = {"群发邮件营销" },  notes = "获取群发邮件营销")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_campaigns/{mail_mass_mailing_campaign_id}")
    public ResponseEntity<Mail_mass_mailing_campaignDTO> get(@PathVariable("mail_mass_mailing_campaign_id") Long mail_mass_mailing_campaign_id) {
        Mail_mass_mailing_campaign domain = mail_mass_mailing_campaignService.get(mail_mass_mailing_campaign_id);
        Mail_mass_mailing_campaignDTO dto = mail_mass_mailing_campaignMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取群发邮件营销草稿", tags = {"群发邮件营销" },  notes = "获取群发邮件营销草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_campaigns/getdraft")
    public ResponseEntity<Mail_mass_mailing_campaignDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_campaignMapping.toDto(mail_mass_mailing_campaignService.getDraft(new Mail_mass_mailing_campaign())));
    }

    @ApiOperation(value = "检查群发邮件营销", tags = {"群发邮件营销" },  notes = "检查群发邮件营销")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_mass_mailing_campaignDTO mail_mass_mailing_campaigndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_campaignService.checkKey(mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_campaignMapping.toDomain(#mail_mass_mailing_campaigndto),'iBizBusinessCentral-Mail_mass_mailing_campaign-Save')")
    @ApiOperation(value = "保存群发邮件营销", tags = {"群发邮件营销" },  notes = "保存群发邮件营销")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_mass_mailing_campaignDTO mail_mass_mailing_campaigndto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_campaignService.save(mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndto)));
    }

    @PreAuthorize("hasPermission(this.mail_mass_mailing_campaignMapping.toDomain(#mail_mass_mailing_campaigndtos),'iBizBusinessCentral-Mail_mass_mailing_campaign-Save')")
    @ApiOperation(value = "批量保存群发邮件营销", tags = {"群发邮件营销" },  notes = "批量保存群发邮件营销")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_mass_mailing_campaignDTO> mail_mass_mailing_campaigndtos) {
        mail_mass_mailing_campaignService.saveBatch(mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_campaign-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_campaign-Get')")
	@ApiOperation(value = "获取数据集", tags = {"群发邮件营销" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_campaigns/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_campaignDTO>> fetchDefault(Mail_mass_mailing_campaignSearchContext context) {
        Page<Mail_mass_mailing_campaign> domains = mail_mass_mailing_campaignService.searchDefault(context) ;
        List<Mail_mass_mailing_campaignDTO> list = mail_mass_mailing_campaignMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_mass_mailing_campaign-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_mass_mailing_campaign-Get')")
	@ApiOperation(value = "查询数据集", tags = {"群发邮件营销" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_mass_mailing_campaigns/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_campaignDTO>> searchDefault(@RequestBody Mail_mass_mailing_campaignSearchContext context) {
        Page<Mail_mass_mailing_campaign> domains = mail_mass_mailing_campaignService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_campaignMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


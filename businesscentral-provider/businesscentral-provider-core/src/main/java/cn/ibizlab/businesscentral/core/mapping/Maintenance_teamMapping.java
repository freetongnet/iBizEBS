package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.businesscentral.core.dto.Maintenance_teamDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMaintenance_teamMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Maintenance_teamMapping extends MappingBase<Maintenance_teamDTO, Maintenance_team> {


}


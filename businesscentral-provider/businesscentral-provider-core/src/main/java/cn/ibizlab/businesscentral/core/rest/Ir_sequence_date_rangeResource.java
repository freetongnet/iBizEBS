package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence_date_range;
import cn.ibizlab.businesscentral.core.odoo_ir.service.IIr_sequence_date_rangeService;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_sequence_date_rangeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"序列日期范围" })
@RestController("Core-ir_sequence_date_range")
@RequestMapping("")
public class Ir_sequence_date_rangeResource {

    @Autowired
    public IIr_sequence_date_rangeService ir_sequence_date_rangeService;

    @Autowired
    @Lazy
    public Ir_sequence_date_rangeMapping ir_sequence_date_rangeMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Create-all')")
    @ApiOperation(value = "新建序列日期范围", tags = {"序列日期范围" },  notes = "新建序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequence_date_ranges")
    public ResponseEntity<Ir_sequence_date_rangeDTO> create(@Validated @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
		ir_sequence_date_rangeService.create(domain);
        Ir_sequence_date_rangeDTO dto = ir_sequence_date_rangeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Create-all')")
    @ApiOperation(value = "批量新建序列日期范围", tags = {"序列日期范围" },  notes = "批量新建序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequence_date_ranges/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Ir_sequence_date_rangeDTO> ir_sequence_date_rangedtos) {
        ir_sequence_date_rangeService.createBatch(ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Update-all')")
    @ApiOperation(value = "更新序列日期范围", tags = {"序列日期范围" },  notes = "更新序列日期范围")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_sequence_date_ranges/{ir_sequence_date_range_id}")
    public ResponseEntity<Ir_sequence_date_rangeDTO> update(@PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
		Ir_sequence_date_range domain  = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain .setId(ir_sequence_date_range_id);
		ir_sequence_date_rangeService.update(domain );
		Ir_sequence_date_rangeDTO dto = ir_sequence_date_rangeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Update-all')")
    @ApiOperation(value = "批量更新序列日期范围", tags = {"序列日期范围" },  notes = "批量更新序列日期范围")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_sequence_date_ranges/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Ir_sequence_date_rangeDTO> ir_sequence_date_rangedtos) {
        ir_sequence_date_rangeService.updateBatch(ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Remove-all')")
    @ApiOperation(value = "删除序列日期范围", tags = {"序列日期范围" },  notes = "删除序列日期范围")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ir_sequence_date_ranges/{ir_sequence_date_range_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id) {
         return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangeService.remove(ir_sequence_date_range_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Remove-all')")
    @ApiOperation(value = "批量删除序列日期范围", tags = {"序列日期范围" },  notes = "批量删除序列日期范围")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ir_sequence_date_ranges/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        ir_sequence_date_rangeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Get-all')")
    @ApiOperation(value = "获取序列日期范围", tags = {"序列日期范围" },  notes = "获取序列日期范围")
	@RequestMapping(method = RequestMethod.GET, value = "/ir_sequence_date_ranges/{ir_sequence_date_range_id}")
    public ResponseEntity<Ir_sequence_date_rangeDTO> get(@PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeService.get(ir_sequence_date_range_id);
        Ir_sequence_date_rangeDTO dto = ir_sequence_date_rangeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取序列日期范围草稿", tags = {"序列日期范围" },  notes = "获取序列日期范围草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/ir_sequence_date_ranges/getdraft")
    public ResponseEntity<Ir_sequence_date_rangeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangeMapping.toDto(ir_sequence_date_rangeService.getDraft(new Ir_sequence_date_range())));
    }

    @ApiOperation(value = "检查序列日期范围", tags = {"序列日期范围" },  notes = "检查序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequence_date_ranges/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangeService.checkKey(ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Create_sequence-all')")
    @ApiOperation(value = "创建序列", tags = {"序列日期范围" },  notes = "创建序列")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequence_date_ranges/{ir_sequence_date_range_id}/create_sequence")
    public ResponseEntity<Ir_sequence_date_rangeDTO> create_sequence(@PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain.setId(ir_sequence_date_range_id);
        domain = ir_sequence_date_rangeService.create_sequence(domain);
        ir_sequence_date_rangedto = ir_sequence_date_rangeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Save-all')")
    @ApiOperation(value = "保存序列日期范围", tags = {"序列日期范围" },  notes = "保存序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequence_date_ranges/save")
    public ResponseEntity<Boolean> save(@RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangeService.save(ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Save-all')")
    @ApiOperation(value = "批量保存序列日期范围", tags = {"序列日期范围" },  notes = "批量保存序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequence_date_ranges/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Ir_sequence_date_rangeDTO> ir_sequence_date_rangedtos) {
        ir_sequence_date_rangeService.saveBatch(ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Select_nextval-all')")
    @ApiOperation(value = "获取nextval", tags = {"序列日期范围" },  notes = "获取nextval")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequence_date_ranges/{ir_sequence_date_range_id}/select_nextval")
    public ResponseEntity<Ir_sequence_date_rangeDTO> select_nextval(@PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain.setId(ir_sequence_date_range_id);
        domain = ir_sequence_date_rangeService.select_nextval(domain);
        ir_sequence_date_rangedto = ir_sequence_date_rangeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Update_nogap-all')")
    @ApiOperation(value = "update_nogap", tags = {"序列日期范围" },  notes = "update_nogap")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_sequence_date_ranges/{ir_sequence_date_range_id}/update_nogap")
    public ResponseEntity<Ir_sequence_date_rangeDTO> update_nogap(@PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain.setId(ir_sequence_date_range_id);
        domain = ir_sequence_date_rangeService.update_nogap(domain);
        ir_sequence_date_rangedto = ir_sequence_date_rangeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"序列日期范围" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/ir_sequence_date_ranges/fetchdefault")
	public ResponseEntity<List<Ir_sequence_date_rangeDTO>> fetchDefault(Ir_sequence_date_rangeSearchContext context) {
        Page<Ir_sequence_date_range> domains = ir_sequence_date_rangeService.searchDefault(context) ;
        List<Ir_sequence_date_rangeDTO> list = ir_sequence_date_rangeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"序列日期范围" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/ir_sequence_date_ranges/searchdefault")
	public ResponseEntity<Page<Ir_sequence_date_rangeDTO>> searchDefault(@RequestBody Ir_sequence_date_rangeSearchContext context) {
        Page<Ir_sequence_date_range> domains = ir_sequence_date_rangeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(ir_sequence_date_rangeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Create-all')")
    @ApiOperation(value = "根据序列建立序列日期范围", tags = {"序列日期范围" },  notes = "根据序列建立序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges")
    public ResponseEntity<Ir_sequence_date_rangeDTO> createByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain.setSequenceId(ir_sequence_id);
		ir_sequence_date_rangeService.create(domain);
        Ir_sequence_date_rangeDTO dto = ir_sequence_date_rangeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Create-all')")
    @ApiOperation(value = "根据序列批量建立序列日期范围", tags = {"序列日期范围" },  notes = "根据序列批量建立序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/batch")
    public ResponseEntity<Boolean> createBatchByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody List<Ir_sequence_date_rangeDTO> ir_sequence_date_rangedtos) {
        List<Ir_sequence_date_range> domainlist=ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedtos);
        for(Ir_sequence_date_range domain:domainlist){
            domain.setSequenceId(ir_sequence_id);
        }
        ir_sequence_date_rangeService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Update-all')")
    @ApiOperation(value = "根据序列更新序列日期范围", tags = {"序列日期范围" },  notes = "根据序列更新序列日期范围")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/{ir_sequence_date_range_id}")
    public ResponseEntity<Ir_sequence_date_rangeDTO> updateByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain.setSequenceId(ir_sequence_id);
        domain.setId(ir_sequence_date_range_id);
		ir_sequence_date_rangeService.update(domain);
        Ir_sequence_date_rangeDTO dto = ir_sequence_date_rangeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Update-all')")
    @ApiOperation(value = "根据序列批量更新序列日期范围", tags = {"序列日期范围" },  notes = "根据序列批量更新序列日期范围")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/batch")
    public ResponseEntity<Boolean> updateBatchByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody List<Ir_sequence_date_rangeDTO> ir_sequence_date_rangedtos) {
        List<Ir_sequence_date_range> domainlist=ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedtos);
        for(Ir_sequence_date_range domain:domainlist){
            domain.setSequenceId(ir_sequence_id);
        }
        ir_sequence_date_rangeService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Remove-all')")
    @ApiOperation(value = "根据序列删除序列日期范围", tags = {"序列日期范围" },  notes = "根据序列删除序列日期范围")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/{ir_sequence_date_range_id}")
    public ResponseEntity<Boolean> removeByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id) {
		return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangeService.remove(ir_sequence_date_range_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Remove-all')")
    @ApiOperation(value = "根据序列批量删除序列日期范围", tags = {"序列日期范围" },  notes = "根据序列批量删除序列日期范围")
	@RequestMapping(method = RequestMethod.DELETE, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/batch")
    public ResponseEntity<Boolean> removeBatchByIr_sequence(@RequestBody List<Long> ids) {
        ir_sequence_date_rangeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Get-all')")
    @ApiOperation(value = "根据序列获取序列日期范围", tags = {"序列日期范围" },  notes = "根据序列获取序列日期范围")
	@RequestMapping(method = RequestMethod.GET, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/{ir_sequence_date_range_id}")
    public ResponseEntity<Ir_sequence_date_rangeDTO> getByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeService.get(ir_sequence_date_range_id);
        Ir_sequence_date_rangeDTO dto = ir_sequence_date_rangeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据序列获取序列日期范围草稿", tags = {"序列日期范围" },  notes = "根据序列获取序列日期范围草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/getdraft")
    public ResponseEntity<Ir_sequence_date_rangeDTO> getDraftByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id) {
        Ir_sequence_date_range domain = new Ir_sequence_date_range();
        domain.setSequenceId(ir_sequence_id);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangeMapping.toDto(ir_sequence_date_rangeService.getDraft(domain)));
    }

    @ApiOperation(value = "根据序列检查序列日期范围", tags = {"序列日期范围" },  notes = "根据序列检查序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/checkkey")
    public ResponseEntity<Boolean> checkKeyByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangeService.checkKey(ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Create_sequence-all')")
    @ApiOperation(value = "根据序列序列日期范围", tags = {"序列日期范围" },  notes = "根据序列序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/{ir_sequence_date_range_id}/create_sequence")
    public ResponseEntity<Ir_sequence_date_rangeDTO> create_sequenceByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain.setSequenceId(ir_sequence_id);
        domain = ir_sequence_date_rangeService.create_sequence(domain) ;
        ir_sequence_date_rangedto = ir_sequence_date_rangeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Save-all')")
    @ApiOperation(value = "根据序列保存序列日期范围", tags = {"序列日期范围" },  notes = "根据序列保存序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/save")
    public ResponseEntity<Boolean> saveByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain.setSequenceId(ir_sequence_id);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangeService.save(domain));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Save-all')")
    @ApiOperation(value = "根据序列批量保存序列日期范围", tags = {"序列日期范围" },  notes = "根据序列批量保存序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/savebatch")
    public ResponseEntity<Boolean> saveBatchByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody List<Ir_sequence_date_rangeDTO> ir_sequence_date_rangedtos) {
        List<Ir_sequence_date_range> domainlist=ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedtos);
        for(Ir_sequence_date_range domain:domainlist){
             domain.setSequenceId(ir_sequence_id);
        }
        ir_sequence_date_rangeService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Select_nextval-all')")
    @ApiOperation(value = "根据序列序列日期范围", tags = {"序列日期范围" },  notes = "根据序列序列日期范围")
	@RequestMapping(method = RequestMethod.POST, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/{ir_sequence_date_range_id}/select_nextval")
    public ResponseEntity<Ir_sequence_date_rangeDTO> select_nextvalByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain.setSequenceId(ir_sequence_id);
        domain = ir_sequence_date_rangeService.select_nextval(domain) ;
        ir_sequence_date_rangedto = ir_sequence_date_rangeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-Update_nogap-all')")
    @ApiOperation(value = "根据序列序列日期范围", tags = {"序列日期范围" },  notes = "根据序列序列日期范围")
	@RequestMapping(method = RequestMethod.PUT, value = "/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/{ir_sequence_date_range_id}/update_nogap")
    public ResponseEntity<Ir_sequence_date_rangeDTO> update_nogapByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @PathVariable("ir_sequence_date_range_id") Long ir_sequence_date_range_id, @RequestBody Ir_sequence_date_rangeDTO ir_sequence_date_rangedto) {
        Ir_sequence_date_range domain = ir_sequence_date_rangeMapping.toDomain(ir_sequence_date_rangedto);
        domain.setSequenceId(ir_sequence_id);
        domain = ir_sequence_date_rangeService.update_nogap(domain) ;
        ir_sequence_date_rangedto = ir_sequence_date_rangeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(ir_sequence_date_rangedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-searchDefault-all')")
	@ApiOperation(value = "根据序列获取数据集", tags = {"序列日期范围" } ,notes = "根据序列获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/fetchdefault")
	public ResponseEntity<List<Ir_sequence_date_rangeDTO>> fetchIr_sequence_date_rangeDefaultByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id,Ir_sequence_date_rangeSearchContext context) {
        context.setN_sequence_id_eq(ir_sequence_id);
        Page<Ir_sequence_date_range> domains = ir_sequence_date_rangeService.searchDefault(context) ;
        List<Ir_sequence_date_rangeDTO> list = ir_sequence_date_rangeMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Ir_sequence_date_range-searchDefault-all')")
	@ApiOperation(value = "根据序列查询数据集", tags = {"序列日期范围" } ,notes = "根据序列查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/ir_sequences/{ir_sequence_id}/ir_sequence_date_ranges/searchdefault")
	public ResponseEntity<Page<Ir_sequence_date_rangeDTO>> searchIr_sequence_date_rangeDefaultByIr_sequence(@PathVariable("ir_sequence_id") Long ir_sequence_id, @RequestBody Ir_sequence_date_rangeSearchContext context) {
        context.setN_sequence_id_eq(ir_sequence_id);
        Page<Ir_sequence_date_range> domains = ir_sequence_date_rangeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(ir_sequence_date_rangeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


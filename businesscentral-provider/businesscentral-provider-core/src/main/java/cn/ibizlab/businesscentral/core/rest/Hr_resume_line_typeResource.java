package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_resume_line_type;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_resume_line_typeService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_resume_line_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"简历类型" })
@RestController("Core-hr_resume_line_type")
@RequestMapping("")
public class Hr_resume_line_typeResource {

    @Autowired
    public IHr_resume_line_typeService hr_resume_line_typeService;

    @Autowired
    @Lazy
    public Hr_resume_line_typeMapping hr_resume_line_typeMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-Create-all')")
    @ApiOperation(value = "新建简历类型", tags = {"简历类型" },  notes = "新建简历类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_line_types")
    public ResponseEntity<Hr_resume_line_typeDTO> create(@Validated @RequestBody Hr_resume_line_typeDTO hr_resume_line_typedto) {
        Hr_resume_line_type domain = hr_resume_line_typeMapping.toDomain(hr_resume_line_typedto);
		hr_resume_line_typeService.create(domain);
        Hr_resume_line_typeDTO dto = hr_resume_line_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-Create-all')")
    @ApiOperation(value = "批量新建简历类型", tags = {"简历类型" },  notes = "批量新建简历类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_line_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_resume_line_typeDTO> hr_resume_line_typedtos) {
        hr_resume_line_typeService.createBatch(hr_resume_line_typeMapping.toDomain(hr_resume_line_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-Update-all')")
    @ApiOperation(value = "更新简历类型", tags = {"简历类型" },  notes = "更新简历类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_resume_line_types/{hr_resume_line_type_id}")
    public ResponseEntity<Hr_resume_line_typeDTO> update(@PathVariable("hr_resume_line_type_id") Long hr_resume_line_type_id, @RequestBody Hr_resume_line_typeDTO hr_resume_line_typedto) {
		Hr_resume_line_type domain  = hr_resume_line_typeMapping.toDomain(hr_resume_line_typedto);
        domain .setId(hr_resume_line_type_id);
		hr_resume_line_typeService.update(domain );
		Hr_resume_line_typeDTO dto = hr_resume_line_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-Update-all')")
    @ApiOperation(value = "批量更新简历类型", tags = {"简历类型" },  notes = "批量更新简历类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_resume_line_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_resume_line_typeDTO> hr_resume_line_typedtos) {
        hr_resume_line_typeService.updateBatch(hr_resume_line_typeMapping.toDomain(hr_resume_line_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-Remove-all')")
    @ApiOperation(value = "删除简历类型", tags = {"简历类型" },  notes = "删除简历类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_resume_line_types/{hr_resume_line_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_resume_line_type_id") Long hr_resume_line_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_resume_line_typeService.remove(hr_resume_line_type_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-Remove-all')")
    @ApiOperation(value = "批量删除简历类型", tags = {"简历类型" },  notes = "批量删除简历类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_resume_line_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_resume_line_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-Get-all')")
    @ApiOperation(value = "获取简历类型", tags = {"简历类型" },  notes = "获取简历类型")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_resume_line_types/{hr_resume_line_type_id}")
    public ResponseEntity<Hr_resume_line_typeDTO> get(@PathVariable("hr_resume_line_type_id") Long hr_resume_line_type_id) {
        Hr_resume_line_type domain = hr_resume_line_typeService.get(hr_resume_line_type_id);
        Hr_resume_line_typeDTO dto = hr_resume_line_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取简历类型草稿", tags = {"简历类型" },  notes = "获取简历类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_resume_line_types/getdraft")
    public ResponseEntity<Hr_resume_line_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_resume_line_typeMapping.toDto(hr_resume_line_typeService.getDraft(new Hr_resume_line_type())));
    }

    @ApiOperation(value = "检查简历类型", tags = {"简历类型" },  notes = "检查简历类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_line_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_resume_line_typeDTO hr_resume_line_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_resume_line_typeService.checkKey(hr_resume_line_typeMapping.toDomain(hr_resume_line_typedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-Save-all')")
    @ApiOperation(value = "保存简历类型", tags = {"简历类型" },  notes = "保存简历类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_line_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_resume_line_typeDTO hr_resume_line_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_resume_line_typeService.save(hr_resume_line_typeMapping.toDomain(hr_resume_line_typedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-Save-all')")
    @ApiOperation(value = "批量保存简历类型", tags = {"简历类型" },  notes = "批量保存简历类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_resume_line_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_resume_line_typeDTO> hr_resume_line_typedtos) {
        hr_resume_line_typeService.saveBatch(hr_resume_line_typeMapping.toDomain(hr_resume_line_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"简历类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_resume_line_types/fetchdefault")
	public ResponseEntity<List<Hr_resume_line_typeDTO>> fetchDefault(Hr_resume_line_typeSearchContext context) {
        Page<Hr_resume_line_type> domains = hr_resume_line_typeService.searchDefault(context) ;
        List<Hr_resume_line_typeDTO> list = hr_resume_line_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_resume_line_type-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"简历类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_resume_line_types/searchdefault")
	public ResponseEntity<Page<Hr_resume_line_typeDTO>> searchDefault(@RequestBody Hr_resume_line_typeSearchContext context) {
        Page<Hr_resume_line_type> domains = hr_resume_line_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_resume_line_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


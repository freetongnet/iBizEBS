package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_testService;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_testSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"测试资源模型" })
@RestController("Core-resource_test")
@RequestMapping("")
public class Resource_testResource {

    @Autowired
    public IResource_testService resource_testService;

    @Autowired
    @Lazy
    public Resource_testMapping resource_testMapping;

    @PreAuthorize("hasPermission(this.resource_testMapping.toDomain(#resource_testdto),'iBizBusinessCentral-Resource_test-Create')")
    @ApiOperation(value = "新建测试资源模型", tags = {"测试资源模型" },  notes = "新建测试资源模型")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_tests")
    public ResponseEntity<Resource_testDTO> create(@Validated @RequestBody Resource_testDTO resource_testdto) {
        Resource_test domain = resource_testMapping.toDomain(resource_testdto);
		resource_testService.create(domain);
        Resource_testDTO dto = resource_testMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_testMapping.toDomain(#resource_testdtos),'iBizBusinessCentral-Resource_test-Create')")
    @ApiOperation(value = "批量新建测试资源模型", tags = {"测试资源模型" },  notes = "批量新建测试资源模型")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_tests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_testDTO> resource_testdtos) {
        resource_testService.createBatch(resource_testMapping.toDomain(resource_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "resource_test" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.resource_testService.get(#resource_test_id),'iBizBusinessCentral-Resource_test-Update')")
    @ApiOperation(value = "更新测试资源模型", tags = {"测试资源模型" },  notes = "更新测试资源模型")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_tests/{resource_test_id}")
    public ResponseEntity<Resource_testDTO> update(@PathVariable("resource_test_id") Long resource_test_id, @RequestBody Resource_testDTO resource_testdto) {
		Resource_test domain  = resource_testMapping.toDomain(resource_testdto);
        domain .setId(resource_test_id);
		resource_testService.update(domain );
		Resource_testDTO dto = resource_testMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_testService.getResourceTestByEntities(this.resource_testMapping.toDomain(#resource_testdtos)),'iBizBusinessCentral-Resource_test-Update')")
    @ApiOperation(value = "批量更新测试资源模型", tags = {"测试资源模型" },  notes = "批量更新测试资源模型")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_tests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_testDTO> resource_testdtos) {
        resource_testService.updateBatch(resource_testMapping.toDomain(resource_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.resource_testService.get(#resource_test_id),'iBizBusinessCentral-Resource_test-Remove')")
    @ApiOperation(value = "删除测试资源模型", tags = {"测试资源模型" },  notes = "删除测试资源模型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_tests/{resource_test_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("resource_test_id") Long resource_test_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_testService.remove(resource_test_id));
    }

    @PreAuthorize("hasPermission(this.resource_testService.getResourceTestByIds(#ids),'iBizBusinessCentral-Resource_test-Remove')")
    @ApiOperation(value = "批量删除测试资源模型", tags = {"测试资源模型" },  notes = "批量删除测试资源模型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_tests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        resource_testService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.resource_testMapping.toDomain(returnObject.body),'iBizBusinessCentral-Resource_test-Get')")
    @ApiOperation(value = "获取测试资源模型", tags = {"测试资源模型" },  notes = "获取测试资源模型")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_tests/{resource_test_id}")
    public ResponseEntity<Resource_testDTO> get(@PathVariable("resource_test_id") Long resource_test_id) {
        Resource_test domain = resource_testService.get(resource_test_id);
        Resource_testDTO dto = resource_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取测试资源模型草稿", tags = {"测试资源模型" },  notes = "获取测试资源模型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_tests/getdraft")
    public ResponseEntity<Resource_testDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(resource_testMapping.toDto(resource_testService.getDraft(new Resource_test())));
    }

    @ApiOperation(value = "检查测试资源模型", tags = {"测试资源模型" },  notes = "检查测试资源模型")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_tests/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Resource_testDTO resource_testdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(resource_testService.checkKey(resource_testMapping.toDomain(resource_testdto)));
    }

    @PreAuthorize("hasPermission(this.resource_testMapping.toDomain(#resource_testdto),'iBizBusinessCentral-Resource_test-Save')")
    @ApiOperation(value = "保存测试资源模型", tags = {"测试资源模型" },  notes = "保存测试资源模型")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_tests/save")
    public ResponseEntity<Boolean> save(@RequestBody Resource_testDTO resource_testdto) {
        return ResponseEntity.status(HttpStatus.OK).body(resource_testService.save(resource_testMapping.toDomain(resource_testdto)));
    }

    @PreAuthorize("hasPermission(this.resource_testMapping.toDomain(#resource_testdtos),'iBizBusinessCentral-Resource_test-Save')")
    @ApiOperation(value = "批量保存测试资源模型", tags = {"测试资源模型" },  notes = "批量保存测试资源模型")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_tests/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Resource_testDTO> resource_testdtos) {
        resource_testService.saveBatch(resource_testMapping.toDomain(resource_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_test-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_test-Get')")
	@ApiOperation(value = "获取数据集", tags = {"测试资源模型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/resource_tests/fetchdefault")
	public ResponseEntity<List<Resource_testDTO>> fetchDefault(Resource_testSearchContext context) {
        Page<Resource_test> domains = resource_testService.searchDefault(context) ;
        List<Resource_testDTO> list = resource_testMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_test-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_test-Get')")
	@ApiOperation(value = "查询数据集", tags = {"测试资源模型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/resource_tests/searchdefault")
	public ResponseEntity<Page<Resource_testDTO>> searchDefault(@RequestBody Resource_testSearchContext context) {
        Page<Resource_test> domains = resource_testService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_testMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.businesscentral.core.dto.Mrp_unbuildDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMrp_unbuildMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mrp_unbuildMapping extends MappingBase<Mrp_unbuildDTO, Mrp_unbuild> {


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.businesscentral.core.dto.Sale_advance_payment_invDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreSale_advance_payment_invMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Sale_advance_payment_invMapping extends MappingBase<Sale_advance_payment_invDTO, Sale_advance_payment_inv> {


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.businesscentral.core.dto.Hr_holidays_summary_employeeDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreHr_holidays_summary_employeeMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_holidays_summary_employeeMapping extends MappingBase<Hr_holidays_summary_employeeDTO, Hr_holidays_summary_employee> {


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_group;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_tax_groupService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_tax_groupSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"税组" })
@RestController("Core-account_tax_group")
@RequestMapping("")
public class Account_tax_groupResource {

    @Autowired
    public IAccount_tax_groupService account_tax_groupService;

    @Autowired
    @Lazy
    public Account_tax_groupMapping account_tax_groupMapping;

    @PreAuthorize("hasPermission(this.account_tax_groupMapping.toDomain(#account_tax_groupdto),'iBizBusinessCentral-Account_tax_group-Create')")
    @ApiOperation(value = "新建税组", tags = {"税组" },  notes = "新建税组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups")
    public ResponseEntity<Account_tax_groupDTO> create(@Validated @RequestBody Account_tax_groupDTO account_tax_groupdto) {
        Account_tax_group domain = account_tax_groupMapping.toDomain(account_tax_groupdto);
		account_tax_groupService.create(domain);
        Account_tax_groupDTO dto = account_tax_groupMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_tax_groupMapping.toDomain(#account_tax_groupdtos),'iBizBusinessCentral-Account_tax_group-Create')")
    @ApiOperation(value = "批量新建税组", tags = {"税组" },  notes = "批量新建税组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_tax_groupDTO> account_tax_groupdtos) {
        account_tax_groupService.createBatch(account_tax_groupMapping.toDomain(account_tax_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_tax_group" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_tax_groupService.get(#account_tax_group_id),'iBizBusinessCentral-Account_tax_group-Update')")
    @ApiOperation(value = "更新税组", tags = {"税组" },  notes = "更新税组")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_tax_groups/{account_tax_group_id}")
    public ResponseEntity<Account_tax_groupDTO> update(@PathVariable("account_tax_group_id") Long account_tax_group_id, @RequestBody Account_tax_groupDTO account_tax_groupdto) {
		Account_tax_group domain  = account_tax_groupMapping.toDomain(account_tax_groupdto);
        domain .setId(account_tax_group_id);
		account_tax_groupService.update(domain );
		Account_tax_groupDTO dto = account_tax_groupMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_tax_groupService.getAccountTaxGroupByEntities(this.account_tax_groupMapping.toDomain(#account_tax_groupdtos)),'iBizBusinessCentral-Account_tax_group-Update')")
    @ApiOperation(value = "批量更新税组", tags = {"税组" },  notes = "批量更新税组")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_tax_groups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_tax_groupDTO> account_tax_groupdtos) {
        account_tax_groupService.updateBatch(account_tax_groupMapping.toDomain(account_tax_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_tax_groupService.get(#account_tax_group_id),'iBizBusinessCentral-Account_tax_group-Remove')")
    @ApiOperation(value = "删除税组", tags = {"税组" },  notes = "删除税组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_groups/{account_tax_group_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_tax_group_id") Long account_tax_group_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_tax_groupService.remove(account_tax_group_id));
    }

    @PreAuthorize("hasPermission(this.account_tax_groupService.getAccountTaxGroupByIds(#ids),'iBizBusinessCentral-Account_tax_group-Remove')")
    @ApiOperation(value = "批量删除税组", tags = {"税组" },  notes = "批量删除税组")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_tax_groups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_tax_groupService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_tax_groupMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_tax_group-Get')")
    @ApiOperation(value = "获取税组", tags = {"税组" },  notes = "获取税组")
	@RequestMapping(method = RequestMethod.GET, value = "/account_tax_groups/{account_tax_group_id}")
    public ResponseEntity<Account_tax_groupDTO> get(@PathVariable("account_tax_group_id") Long account_tax_group_id) {
        Account_tax_group domain = account_tax_groupService.get(account_tax_group_id);
        Account_tax_groupDTO dto = account_tax_groupMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取税组草稿", tags = {"税组" },  notes = "获取税组草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_tax_groups/getdraft")
    public ResponseEntity<Account_tax_groupDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_tax_groupMapping.toDto(account_tax_groupService.getDraft(new Account_tax_group())));
    }

    @ApiOperation(value = "检查税组", tags = {"税组" },  notes = "检查税组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_tax_groupDTO account_tax_groupdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_tax_groupService.checkKey(account_tax_groupMapping.toDomain(account_tax_groupdto)));
    }

    @PreAuthorize("hasPermission(this.account_tax_groupMapping.toDomain(#account_tax_groupdto),'iBizBusinessCentral-Account_tax_group-Save')")
    @ApiOperation(value = "保存税组", tags = {"税组" },  notes = "保存税组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_tax_groupDTO account_tax_groupdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_tax_groupService.save(account_tax_groupMapping.toDomain(account_tax_groupdto)));
    }

    @PreAuthorize("hasPermission(this.account_tax_groupMapping.toDomain(#account_tax_groupdtos),'iBizBusinessCentral-Account_tax_group-Save')")
    @ApiOperation(value = "批量保存税组", tags = {"税组" },  notes = "批量保存税组")
	@RequestMapping(method = RequestMethod.POST, value = "/account_tax_groups/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_tax_groupDTO> account_tax_groupdtos) {
        account_tax_groupService.saveBatch(account_tax_groupMapping.toDomain(account_tax_groupdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_tax_group-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_tax_group-Get')")
	@ApiOperation(value = "获取数据集", tags = {"税组" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_tax_groups/fetchdefault")
	public ResponseEntity<List<Account_tax_groupDTO>> fetchDefault(Account_tax_groupSearchContext context) {
        Page<Account_tax_group> domains = account_tax_groupService.searchDefault(context) ;
        List<Account_tax_groupDTO> list = account_tax_groupMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_tax_group-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_tax_group-Get')")
	@ApiOperation(value = "查询数据集", tags = {"税组" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_tax_groups/searchdefault")
	public ResponseEntity<Page<Account_tax_groupDTO>> searchDefault(@RequestBody Account_tax_groupSearchContext context) {
        Page<Account_tax_group> domains = account_tax_groupService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_tax_groupMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


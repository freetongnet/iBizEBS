package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce_line;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produce_lineService;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"记录生产明细" })
@RestController("Core-mrp_product_produce_line")
@RequestMapping("")
public class Mrp_product_produce_lineResource {

    @Autowired
    public IMrp_product_produce_lineService mrp_product_produce_lineService;

    @Autowired
    @Lazy
    public Mrp_product_produce_lineMapping mrp_product_produce_lineMapping;

    @PreAuthorize("hasPermission(this.mrp_product_produce_lineMapping.toDomain(#mrp_product_produce_linedto),'iBizBusinessCentral-Mrp_product_produce_line-Create')")
    @ApiOperation(value = "新建记录生产明细", tags = {"记录生产明细" },  notes = "新建记录生产明细")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines")
    public ResponseEntity<Mrp_product_produce_lineDTO> create(@Validated @RequestBody Mrp_product_produce_lineDTO mrp_product_produce_linedto) {
        Mrp_product_produce_line domain = mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedto);
		mrp_product_produce_lineService.create(domain);
        Mrp_product_produce_lineDTO dto = mrp_product_produce_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_product_produce_lineMapping.toDomain(#mrp_product_produce_linedtos),'iBizBusinessCentral-Mrp_product_produce_line-Create')")
    @ApiOperation(value = "批量新建记录生产明细", tags = {"记录生产明细" },  notes = "批量新建记录生产明细")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_product_produce_lineDTO> mrp_product_produce_linedtos) {
        mrp_product_produce_lineService.createBatch(mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mrp_product_produce_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mrp_product_produce_lineService.get(#mrp_product_produce_line_id),'iBizBusinessCentral-Mrp_product_produce_line-Update')")
    @ApiOperation(value = "更新记录生产明细", tags = {"记录生产明细" },  notes = "更新记录生产明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produce_lines/{mrp_product_produce_line_id}")
    public ResponseEntity<Mrp_product_produce_lineDTO> update(@PathVariable("mrp_product_produce_line_id") Long mrp_product_produce_line_id, @RequestBody Mrp_product_produce_lineDTO mrp_product_produce_linedto) {
		Mrp_product_produce_line domain  = mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedto);
        domain .setId(mrp_product_produce_line_id);
		mrp_product_produce_lineService.update(domain );
		Mrp_product_produce_lineDTO dto = mrp_product_produce_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mrp_product_produce_lineService.getMrpProductProduceLineByEntities(this.mrp_product_produce_lineMapping.toDomain(#mrp_product_produce_linedtos)),'iBizBusinessCentral-Mrp_product_produce_line-Update')")
    @ApiOperation(value = "批量更新记录生产明细", tags = {"记录生产明细" },  notes = "批量更新记录生产明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produce_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_product_produce_lineDTO> mrp_product_produce_linedtos) {
        mrp_product_produce_lineService.updateBatch(mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mrp_product_produce_lineService.get(#mrp_product_produce_line_id),'iBizBusinessCentral-Mrp_product_produce_line-Remove')")
    @ApiOperation(value = "删除记录生产明细", tags = {"记录生产明细" },  notes = "删除记录生产明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produce_lines/{mrp_product_produce_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mrp_product_produce_line_id") Long mrp_product_produce_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_product_produce_lineService.remove(mrp_product_produce_line_id));
    }

    @PreAuthorize("hasPermission(this.mrp_product_produce_lineService.getMrpProductProduceLineByIds(#ids),'iBizBusinessCentral-Mrp_product_produce_line-Remove')")
    @ApiOperation(value = "批量删除记录生产明细", tags = {"记录生产明细" },  notes = "批量删除记录生产明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produce_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mrp_product_produce_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mrp_product_produce_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mrp_product_produce_line-Get')")
    @ApiOperation(value = "获取记录生产明细", tags = {"记录生产明细" },  notes = "获取记录生产明细")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produce_lines/{mrp_product_produce_line_id}")
    public ResponseEntity<Mrp_product_produce_lineDTO> get(@PathVariable("mrp_product_produce_line_id") Long mrp_product_produce_line_id) {
        Mrp_product_produce_line domain = mrp_product_produce_lineService.get(mrp_product_produce_line_id);
        Mrp_product_produce_lineDTO dto = mrp_product_produce_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取记录生产明细草稿", tags = {"记录生产明细" },  notes = "获取记录生产明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produce_lines/getdraft")
    public ResponseEntity<Mrp_product_produce_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_product_produce_lineMapping.toDto(mrp_product_produce_lineService.getDraft(new Mrp_product_produce_line())));
    }

    @ApiOperation(value = "检查记录生产明细", tags = {"记录生产明细" },  notes = "检查记录生产明细")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mrp_product_produce_lineDTO mrp_product_produce_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mrp_product_produce_lineService.checkKey(mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedto)));
    }

    @PreAuthorize("hasPermission(this.mrp_product_produce_lineMapping.toDomain(#mrp_product_produce_linedto),'iBizBusinessCentral-Mrp_product_produce_line-Save')")
    @ApiOperation(value = "保存记录生产明细", tags = {"记录生产明细" },  notes = "保存记录生产明细")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Mrp_product_produce_lineDTO mrp_product_produce_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mrp_product_produce_lineService.save(mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedto)));
    }

    @PreAuthorize("hasPermission(this.mrp_product_produce_lineMapping.toDomain(#mrp_product_produce_linedtos),'iBizBusinessCentral-Mrp_product_produce_line-Save')")
    @ApiOperation(value = "批量保存记录生产明细", tags = {"记录生产明细" },  notes = "批量保存记录生产明细")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mrp_product_produce_lineDTO> mrp_product_produce_linedtos) {
        mrp_product_produce_lineService.saveBatch(mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_product_produce_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_product_produce_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"记录生产明细" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_product_produce_lines/fetchdefault")
	public ResponseEntity<List<Mrp_product_produce_lineDTO>> fetchDefault(Mrp_product_produce_lineSearchContext context) {
        Page<Mrp_product_produce_line> domains = mrp_product_produce_lineService.searchDefault(context) ;
        List<Mrp_product_produce_lineDTO> list = mrp_product_produce_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mrp_product_produce_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mrp_product_produce_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"记录生产明细" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mrp_product_produce_lines/searchdefault")
	public ResponseEntity<Page<Mrp_product_produce_lineDTO>> searchDefault(@RequestBody Mrp_product_produce_lineSearchContext context) {
        Page<Mrp_product_produce_line> domains = mrp_product_produce_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_product_produce_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


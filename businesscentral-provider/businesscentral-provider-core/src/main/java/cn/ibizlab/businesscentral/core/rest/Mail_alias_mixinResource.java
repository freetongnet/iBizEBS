package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias_mixin;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_alias_mixinService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_alias_mixinSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"EMail别名 Mixin" })
@RestController("Core-mail_alias_mixin")
@RequestMapping("")
public class Mail_alias_mixinResource {

    @Autowired
    public IMail_alias_mixinService mail_alias_mixinService;

    @Autowired
    @Lazy
    public Mail_alias_mixinMapping mail_alias_mixinMapping;

    @PreAuthorize("hasPermission(this.mail_alias_mixinMapping.toDomain(#mail_alias_mixindto),'iBizBusinessCentral-Mail_alias_mixin-Create')")
    @ApiOperation(value = "新建EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "新建EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins")
    public ResponseEntity<Mail_alias_mixinDTO> create(@Validated @RequestBody Mail_alias_mixinDTO mail_alias_mixindto) {
        Mail_alias_mixin domain = mail_alias_mixinMapping.toDomain(mail_alias_mixindto);
		mail_alias_mixinService.create(domain);
        Mail_alias_mixinDTO dto = mail_alias_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_alias_mixinMapping.toDomain(#mail_alias_mixindtos),'iBizBusinessCentral-Mail_alias_mixin-Create')")
    @ApiOperation(value = "批量新建EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "批量新建EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_alias_mixinDTO> mail_alias_mixindtos) {
        mail_alias_mixinService.createBatch(mail_alias_mixinMapping.toDomain(mail_alias_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_alias_mixin" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_alias_mixinService.get(#mail_alias_mixin_id),'iBizBusinessCentral-Mail_alias_mixin-Update')")
    @ApiOperation(value = "更新EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "更新EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_alias_mixins/{mail_alias_mixin_id}")
    public ResponseEntity<Mail_alias_mixinDTO> update(@PathVariable("mail_alias_mixin_id") Long mail_alias_mixin_id, @RequestBody Mail_alias_mixinDTO mail_alias_mixindto) {
		Mail_alias_mixin domain  = mail_alias_mixinMapping.toDomain(mail_alias_mixindto);
        domain .setId(mail_alias_mixin_id);
		mail_alias_mixinService.update(domain );
		Mail_alias_mixinDTO dto = mail_alias_mixinMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_alias_mixinService.getMailAliasMixinByEntities(this.mail_alias_mixinMapping.toDomain(#mail_alias_mixindtos)),'iBizBusinessCentral-Mail_alias_mixin-Update')")
    @ApiOperation(value = "批量更新EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "批量更新EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_alias_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_alias_mixinDTO> mail_alias_mixindtos) {
        mail_alias_mixinService.updateBatch(mail_alias_mixinMapping.toDomain(mail_alias_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_alias_mixinService.get(#mail_alias_mixin_id),'iBizBusinessCentral-Mail_alias_mixin-Remove')")
    @ApiOperation(value = "删除EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "删除EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_alias_mixins/{mail_alias_mixin_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_alias_mixin_id") Long mail_alias_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_alias_mixinService.remove(mail_alias_mixin_id));
    }

    @PreAuthorize("hasPermission(this.mail_alias_mixinService.getMailAliasMixinByIds(#ids),'iBizBusinessCentral-Mail_alias_mixin-Remove')")
    @ApiOperation(value = "批量删除EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "批量删除EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_alias_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_alias_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_alias_mixinMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_alias_mixin-Get')")
    @ApiOperation(value = "获取EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "获取EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_alias_mixins/{mail_alias_mixin_id}")
    public ResponseEntity<Mail_alias_mixinDTO> get(@PathVariable("mail_alias_mixin_id") Long mail_alias_mixin_id) {
        Mail_alias_mixin domain = mail_alias_mixinService.get(mail_alias_mixin_id);
        Mail_alias_mixinDTO dto = mail_alias_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取EMail别名 Mixin草稿", tags = {"EMail别名 Mixin" },  notes = "获取EMail别名 Mixin草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_alias_mixins/getdraft")
    public ResponseEntity<Mail_alias_mixinDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_alias_mixinMapping.toDto(mail_alias_mixinService.getDraft(new Mail_alias_mixin())));
    }

    @ApiOperation(value = "检查EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "检查EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_alias_mixinDTO mail_alias_mixindto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_alias_mixinService.checkKey(mail_alias_mixinMapping.toDomain(mail_alias_mixindto)));
    }

    @PreAuthorize("hasPermission(this.mail_alias_mixinMapping.toDomain(#mail_alias_mixindto),'iBizBusinessCentral-Mail_alias_mixin-Save')")
    @ApiOperation(value = "保存EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "保存EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_alias_mixinDTO mail_alias_mixindto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_alias_mixinService.save(mail_alias_mixinMapping.toDomain(mail_alias_mixindto)));
    }

    @PreAuthorize("hasPermission(this.mail_alias_mixinMapping.toDomain(#mail_alias_mixindtos),'iBizBusinessCentral-Mail_alias_mixin-Save')")
    @ApiOperation(value = "批量保存EMail别名 Mixin", tags = {"EMail别名 Mixin" },  notes = "批量保存EMail别名 Mixin")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_alias_mixinDTO> mail_alias_mixindtos) {
        mail_alias_mixinService.saveBatch(mail_alias_mixinMapping.toDomain(mail_alias_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_alias_mixin-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_alias_mixin-Get')")
	@ApiOperation(value = "获取数据集", tags = {"EMail别名 Mixin" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_alias_mixins/fetchdefault")
	public ResponseEntity<List<Mail_alias_mixinDTO>> fetchDefault(Mail_alias_mixinSearchContext context) {
        Page<Mail_alias_mixin> domains = mail_alias_mixinService.searchDefault(context) ;
        List<Mail_alias_mixinDTO> list = mail_alias_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_alias_mixin-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_alias_mixin-Get')")
	@ApiOperation(value = "查询数据集", tags = {"EMail别名 Mixin" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_alias_mixins/searchdefault")
	public ResponseEntity<Page<Mail_alias_mixinDTO>> searchDefault(@RequestBody Mail_alias_mixinSearchContext context) {
        Page<Mail_alias_mixin> domains = mail_alias_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_alias_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model_brand;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_model_brandService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_model_brandSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆品牌" })
@RestController("Core-fleet_vehicle_model_brand")
@RequestMapping("")
public class Fleet_vehicle_model_brandResource {

    @Autowired
    public IFleet_vehicle_model_brandService fleet_vehicle_model_brandService;

    @Autowired
    @Lazy
    public Fleet_vehicle_model_brandMapping fleet_vehicle_model_brandMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_model_brandMapping.toDomain(#fleet_vehicle_model_branddto),'iBizBusinessCentral-Fleet_vehicle_model_brand-Create')")
    @ApiOperation(value = "新建车辆品牌", tags = {"车辆品牌" },  notes = "新建车辆品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands")
    public ResponseEntity<Fleet_vehicle_model_brandDTO> create(@Validated @RequestBody Fleet_vehicle_model_brandDTO fleet_vehicle_model_branddto) {
        Fleet_vehicle_model_brand domain = fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddto);
		fleet_vehicle_model_brandService.create(domain);
        Fleet_vehicle_model_brandDTO dto = fleet_vehicle_model_brandMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_model_brandMapping.toDomain(#fleet_vehicle_model_branddtos),'iBizBusinessCentral-Fleet_vehicle_model_brand-Create')")
    @ApiOperation(value = "批量新建车辆品牌", tags = {"车辆品牌" },  notes = "批量新建车辆品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_model_brandDTO> fleet_vehicle_model_branddtos) {
        fleet_vehicle_model_brandService.createBatch(fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_model_brand" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_model_brandService.get(#fleet_vehicle_model_brand_id),'iBizBusinessCentral-Fleet_vehicle_model_brand-Update')")
    @ApiOperation(value = "更新车辆品牌", tags = {"车辆品牌" },  notes = "更新车辆品牌")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_model_brands/{fleet_vehicle_model_brand_id}")
    public ResponseEntity<Fleet_vehicle_model_brandDTO> update(@PathVariable("fleet_vehicle_model_brand_id") Long fleet_vehicle_model_brand_id, @RequestBody Fleet_vehicle_model_brandDTO fleet_vehicle_model_branddto) {
		Fleet_vehicle_model_brand domain  = fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddto);
        domain .setId(fleet_vehicle_model_brand_id);
		fleet_vehicle_model_brandService.update(domain );
		Fleet_vehicle_model_brandDTO dto = fleet_vehicle_model_brandMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_model_brandService.getFleetVehicleModelBrandByEntities(this.fleet_vehicle_model_brandMapping.toDomain(#fleet_vehicle_model_branddtos)),'iBizBusinessCentral-Fleet_vehicle_model_brand-Update')")
    @ApiOperation(value = "批量更新车辆品牌", tags = {"车辆品牌" },  notes = "批量更新车辆品牌")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_model_brands/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_model_brandDTO> fleet_vehicle_model_branddtos) {
        fleet_vehicle_model_brandService.updateBatch(fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_model_brandService.get(#fleet_vehicle_model_brand_id),'iBizBusinessCentral-Fleet_vehicle_model_brand-Remove')")
    @ApiOperation(value = "删除车辆品牌", tags = {"车辆品牌" },  notes = "删除车辆品牌")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_model_brands/{fleet_vehicle_model_brand_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_model_brand_id") Long fleet_vehicle_model_brand_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_model_brandService.remove(fleet_vehicle_model_brand_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_model_brandService.getFleetVehicleModelBrandByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_model_brand-Remove')")
    @ApiOperation(value = "批量删除车辆品牌", tags = {"车辆品牌" },  notes = "批量删除车辆品牌")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_model_brands/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_model_brandService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_model_brandMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_model_brand-Get')")
    @ApiOperation(value = "获取车辆品牌", tags = {"车辆品牌" },  notes = "获取车辆品牌")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_model_brands/{fleet_vehicle_model_brand_id}")
    public ResponseEntity<Fleet_vehicle_model_brandDTO> get(@PathVariable("fleet_vehicle_model_brand_id") Long fleet_vehicle_model_brand_id) {
        Fleet_vehicle_model_brand domain = fleet_vehicle_model_brandService.get(fleet_vehicle_model_brand_id);
        Fleet_vehicle_model_brandDTO dto = fleet_vehicle_model_brandMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆品牌草稿", tags = {"车辆品牌" },  notes = "获取车辆品牌草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_model_brands/getdraft")
    public ResponseEntity<Fleet_vehicle_model_brandDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_model_brandMapping.toDto(fleet_vehicle_model_brandService.getDraft(new Fleet_vehicle_model_brand())));
    }

    @ApiOperation(value = "检查车辆品牌", tags = {"车辆品牌" },  notes = "检查车辆品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_model_brandDTO fleet_vehicle_model_branddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_model_brandService.checkKey(fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_model_brandMapping.toDomain(#fleet_vehicle_model_branddto),'iBizBusinessCentral-Fleet_vehicle_model_brand-Save')")
    @ApiOperation(value = "保存车辆品牌", tags = {"车辆品牌" },  notes = "保存车辆品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_model_brandDTO fleet_vehicle_model_branddto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_model_brandService.save(fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_model_brandMapping.toDomain(#fleet_vehicle_model_branddtos),'iBizBusinessCentral-Fleet_vehicle_model_brand-Save')")
    @ApiOperation(value = "批量保存车辆品牌", tags = {"车辆品牌" },  notes = "批量保存车辆品牌")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_model_brands/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_model_brandDTO> fleet_vehicle_model_branddtos) {
        fleet_vehicle_model_brandService.saveBatch(fleet_vehicle_model_brandMapping.toDomain(fleet_vehicle_model_branddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_model_brand-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_model_brand-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆品牌" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_model_brands/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_model_brandDTO>> fetchDefault(Fleet_vehicle_model_brandSearchContext context) {
        Page<Fleet_vehicle_model_brand> domains = fleet_vehicle_model_brandService.searchDefault(context) ;
        List<Fleet_vehicle_model_brandDTO> list = fleet_vehicle_model_brandMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_model_brand-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_model_brand-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆品牌" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_model_brands/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_model_brandDTO>> searchDefault(@RequestBody Fleet_vehicle_model_brandSearchContext context) {
        Page<Fleet_vehicle_model_brand> domains = fleet_vehicle_model_brandService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_model_brandMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_request;
import cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_requestService;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_requestSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"保养请求" })
@RestController("Core-maintenance_request")
@RequestMapping("")
public class Maintenance_requestResource {

    @Autowired
    public IMaintenance_requestService maintenance_requestService;

    @Autowired
    @Lazy
    public Maintenance_requestMapping maintenance_requestMapping;

    @PreAuthorize("hasPermission(this.maintenance_requestMapping.toDomain(#maintenance_requestdto),'iBizBusinessCentral-Maintenance_request-Create')")
    @ApiOperation(value = "新建保养请求", tags = {"保养请求" },  notes = "新建保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests")
    public ResponseEntity<Maintenance_requestDTO> create(@Validated @RequestBody Maintenance_requestDTO maintenance_requestdto) {
        Maintenance_request domain = maintenance_requestMapping.toDomain(maintenance_requestdto);
		maintenance_requestService.create(domain);
        Maintenance_requestDTO dto = maintenance_requestMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_requestMapping.toDomain(#maintenance_requestdtos),'iBizBusinessCentral-Maintenance_request-Create')")
    @ApiOperation(value = "批量新建保养请求", tags = {"保养请求" },  notes = "批量新建保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_requestDTO> maintenance_requestdtos) {
        maintenance_requestService.createBatch(maintenance_requestMapping.toDomain(maintenance_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "maintenance_request" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.maintenance_requestService.get(#maintenance_request_id),'iBizBusinessCentral-Maintenance_request-Update')")
    @ApiOperation(value = "更新保养请求", tags = {"保养请求" },  notes = "更新保养请求")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_requests/{maintenance_request_id}")
    public ResponseEntity<Maintenance_requestDTO> update(@PathVariable("maintenance_request_id") Long maintenance_request_id, @RequestBody Maintenance_requestDTO maintenance_requestdto) {
		Maintenance_request domain  = maintenance_requestMapping.toDomain(maintenance_requestdto);
        domain .setId(maintenance_request_id);
		maintenance_requestService.update(domain );
		Maintenance_requestDTO dto = maintenance_requestMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.maintenance_requestService.getMaintenanceRequestByEntities(this.maintenance_requestMapping.toDomain(#maintenance_requestdtos)),'iBizBusinessCentral-Maintenance_request-Update')")
    @ApiOperation(value = "批量更新保养请求", tags = {"保养请求" },  notes = "批量更新保养请求")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_requests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_requestDTO> maintenance_requestdtos) {
        maintenance_requestService.updateBatch(maintenance_requestMapping.toDomain(maintenance_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.maintenance_requestService.get(#maintenance_request_id),'iBizBusinessCentral-Maintenance_request-Remove')")
    @ApiOperation(value = "删除保养请求", tags = {"保养请求" },  notes = "删除保养请求")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_requests/{maintenance_request_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_request_id") Long maintenance_request_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_requestService.remove(maintenance_request_id));
    }

    @PreAuthorize("hasPermission(this.maintenance_requestService.getMaintenanceRequestByIds(#ids),'iBizBusinessCentral-Maintenance_request-Remove')")
    @ApiOperation(value = "批量删除保养请求", tags = {"保养请求" },  notes = "批量删除保养请求")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_requests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        maintenance_requestService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.maintenance_requestMapping.toDomain(returnObject.body),'iBizBusinessCentral-Maintenance_request-Get')")
    @ApiOperation(value = "获取保养请求", tags = {"保养请求" },  notes = "获取保养请求")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_requests/{maintenance_request_id}")
    public ResponseEntity<Maintenance_requestDTO> get(@PathVariable("maintenance_request_id") Long maintenance_request_id) {
        Maintenance_request domain = maintenance_requestService.get(maintenance_request_id);
        Maintenance_requestDTO dto = maintenance_requestMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取保养请求草稿", tags = {"保养请求" },  notes = "获取保养请求草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_requests/getdraft")
    public ResponseEntity<Maintenance_requestDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_requestMapping.toDto(maintenance_requestService.getDraft(new Maintenance_request())));
    }

    @ApiOperation(value = "检查保养请求", tags = {"保养请求" },  notes = "检查保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Maintenance_requestDTO maintenance_requestdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(maintenance_requestService.checkKey(maintenance_requestMapping.toDomain(maintenance_requestdto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_requestMapping.toDomain(#maintenance_requestdto),'iBizBusinessCentral-Maintenance_request-Save')")
    @ApiOperation(value = "保存保养请求", tags = {"保养请求" },  notes = "保存保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests/save")
    public ResponseEntity<Boolean> save(@RequestBody Maintenance_requestDTO maintenance_requestdto) {
        return ResponseEntity.status(HttpStatus.OK).body(maintenance_requestService.save(maintenance_requestMapping.toDomain(maintenance_requestdto)));
    }

    @PreAuthorize("hasPermission(this.maintenance_requestMapping.toDomain(#maintenance_requestdtos),'iBizBusinessCentral-Maintenance_request-Save')")
    @ApiOperation(value = "批量保存保养请求", tags = {"保养请求" },  notes = "批量保存保养请求")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_requests/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Maintenance_requestDTO> maintenance_requestdtos) {
        maintenance_requestService.saveBatch(maintenance_requestMapping.toDomain(maintenance_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_request-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_request-Get')")
	@ApiOperation(value = "获取数据集", tags = {"保养请求" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_requests/fetchdefault")
	public ResponseEntity<List<Maintenance_requestDTO>> fetchDefault(Maintenance_requestSearchContext context) {
        Page<Maintenance_request> domains = maintenance_requestService.searchDefault(context) ;
        List<Maintenance_requestDTO> list = maintenance_requestMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Maintenance_request-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Maintenance_request-Get')")
	@ApiOperation(value = "查询数据集", tags = {"保养请求" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/maintenance_requests/searchdefault")
	public ResponseEntity<Page<Maintenance_requestDTO>> searchDefault(@RequestBody Maintenance_requestSearchContext context) {
        Page<Maintenance_request> domains = maintenance_requestService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_requestMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


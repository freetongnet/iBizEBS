package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_invoiceDTO]
 */
@Data
public class Account_invoiceDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [OUTSTANDING_CREDITS_DEBITS_WIDGET]
     *
     */
    @JSONField(name = "outstanding_credits_debits_widget")
    @JsonProperty("outstanding_credits_debits_widget")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String outstandingCreditsDebitsWidget;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageChannelIds;

    /**
     * 属性 [MOVE_NAME]
     *
     */
    @JSONField(name = "move_name")
    @JsonProperty("move_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String moveName;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String activitySummary;

    /**
     * 属性 [ACCESS_URL]
     *
     */
    @JSONField(name = "access_url")
    @JsonProperty("access_url")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String accessUrl;

    /**
     * 属性 [PAYMENT_MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "payment_move_line_ids")
    @JsonProperty("payment_move_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String paymentMoveLineIds;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String origin;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [AMOUNT_TOTAL_COMPANY_SIGNED]
     *
     */
    @JSONField(name = "amount_total_company_signed")
    @JsonProperty("amount_total_company_signed")
    private BigDecimal amountTotalCompanySigned;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [AMOUNT_UNTAXED_SIGNED]
     *
     */
    @JSONField(name = "amount_untaxed_signed")
    @JsonProperty("amount_untaxed_signed")
    private BigDecimal amountUntaxedSigned;

    /**
     * 属性 [RESIDUAL]
     *
     */
    @JSONField(name = "residual")
    @JsonProperty("residual")
    private BigDecimal residual;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [HAS_OUTSTANDING]
     *
     */
    @JSONField(name = "has_outstanding")
    @JsonProperty("has_outstanding")
    private Boolean hasOutstanding;

    /**
     * 属性 [COMMENT]
     *
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String comment;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [AMOUNT_BY_GROUP]
     *
     */
    @JSONField(name = "amount_by_group")
    @JsonProperty("amount_by_group")
    private byte[] amountByGroup;

    /**
     * 属性 [PAYMENTS_WIDGET]
     *
     */
    @JSONField(name = "payments_widget")
    @JsonProperty("payments_widget")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String paymentsWidget;

    /**
     * 属性 [RECONCILED]
     *
     */
    @JSONField(name = "reconciled")
    @JsonProperty("reconciled")
    private Boolean reconciled;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [SENT]
     *
     */
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private Boolean sent;

    /**
     * 属性 [AMOUNT_TOTAL_SIGNED]
     *
     */
    @JSONField(name = "amount_total_signed")
    @JsonProperty("amount_total_signed")
    private BigDecimal amountTotalSigned;

    /**
     * 属性 [INVOICE_LINE_IDS]
     *
     */
    @JSONField(name = "invoice_line_ids")
    @JsonProperty("invoice_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceLineIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ACCESS_WARNING]
     *
     */
    @JSONField(name = "access_warning")
    @JsonProperty("access_warning")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String accessWarning;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String activityState;

    /**
     * 属性 [RESIDUAL_SIGNED]
     *
     */
    @JSONField(name = "residual_signed")
    @JsonProperty("residual_signed")
    private BigDecimal residualSigned;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String accessToken;

    /**
     * 属性 [VENDOR_DISPLAY_NAME]
     *
     */
    @JSONField(name = "vendor_display_name")
    @JsonProperty("vendor_display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String vendorDisplayName;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageIds;

    /**
     * 属性 [AMOUNT_UNTAXED]
     *
     */
    @JSONField(name = "amount_untaxed")
    @JsonProperty("amount_untaxed")
    private BigDecimal amountUntaxed;

    /**
     * 属性 [TRANSACTION_IDS]
     *
     */
    @JSONField(name = "transaction_ids")
    @JsonProperty("transaction_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String transactionIds;

    /**
     * 属性 [DATE_DUE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_due" , format="yyyy-MM-dd")
    @JsonProperty("date_due")
    private Timestamp dateDue;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messagePartnerIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [AUTHORIZED_TRANSACTION_IDS]
     *
     */
    @JSONField(name = "authorized_transaction_ids")
    @JsonProperty("authorized_transaction_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String authorizedTransactionIds;

    /**
     * 属性 [TAX_LINE_IDS]
     *
     */
    @JSONField(name = "tax_line_ids")
    @JsonProperty("tax_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String taxLineIds;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String messageFollowerIds;

    /**
     * 属性 [SOURCE_EMAIL]
     *
     */
    @JSONField(name = "source_email")
    @JsonProperty("source_email")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sourceEmail;

    /**
     * 属性 [AMOUNT_TAX]
     *
     */
    @JSONField(name = "amount_tax")
    @JsonProperty("amount_tax")
    private BigDecimal amountTax;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;

    /**
     * 属性 [AMOUNT_UNTAXED_INVOICE_SIGNED]
     *
     */
    @JSONField(name = "amount_untaxed_invoice_signed")
    @JsonProperty("amount_untaxed_invoice_signed")
    private BigDecimal amountUntaxedInvoiceSigned;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;

    /**
     * 属性 [SEQUENCE_NUMBER_NEXT_PREFIX]
     *
     */
    @JSONField(name = "sequence_number_next_prefix")
    @JsonProperty("sequence_number_next_prefix")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sequenceNumberNextPrefix;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [REFUND_INVOICE_IDS]
     *
     */
    @JSONField(name = "refund_invoice_ids")
    @JsonProperty("refund_invoice_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String refundInvoiceIds;

    /**
     * 属性 [RESIDUAL_COMPANY_SIGNED]
     *
     */
    @JSONField(name = "residual_company_signed")
    @JsonProperty("residual_company_signed")
    private BigDecimal residualCompanySigned;

    /**
     * 属性 [DATE_INVOICE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_invoice" , format="yyyy-MM-dd")
    @JsonProperty("date_invoice")
    private Timestamp dateInvoice;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String name;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [REFERENCE]
     *
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String reference;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [SEQUENCE_NUMBER_NEXT]
     *
     */
    @JSONField(name = "sequence_number_next")
    @JsonProperty("sequence_number_next")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String sequenceNumberNext;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String websiteMessageIds;

    /**
     * 属性 [AMOUNT_TAX_SIGNED]
     *
     */
    @JSONField(name = "amount_tax_signed")
    @JsonProperty("amount_tax_signed")
    private BigDecimal amountTaxSigned;

    /**
     * 属性 [INVOICE_ICON]
     *
     */
    @JSONField(name = "invoice_icon")
    @JsonProperty("invoice_icon")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String invoiceIcon;

    /**
     * 属性 [PAYMENT_IDS]
     *
     */
    @JSONField(name = "payment_ids")
    @JsonProperty("payment_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String paymentIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [AMOUNT_TOTAL]
     *
     */
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private BigDecimal amountTotal;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String activityIds;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String type;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String journalIdText;

    /**
     * 属性 [PURCHASE_ID_TEXT]
     *
     */
    @JSONField(name = "purchase_id_text")
    @JsonProperty("purchase_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String purchaseIdText;

    /**
     * 属性 [VENDOR_BILL_PURCHASE_ID_TEXT]
     *
     */
    @JSONField(name = "vendor_bill_purchase_id_text")
    @JsonProperty("vendor_bill_purchase_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String vendorBillPurchaseIdText;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String sourceIdText;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String campaignIdText;

    /**
     * 属性 [PARTNER_SHIPPING_ID_TEXT]
     *
     */
    @JSONField(name = "partner_shipping_id_text")
    @JsonProperty("partner_shipping_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerShippingIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [INCOTERMS_ID_TEXT]
     *
     */
    @JSONField(name = "incoterms_id_text")
    @JsonProperty("incoterms_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String incotermsIdText;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyCurrencyId;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountIdText;

    /**
     * 属性 [INCOTERM_ID_TEXT]
     *
     */
    @JSONField(name = "incoterm_id_text")
    @JsonProperty("incoterm_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String incotermIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String mediumIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [CASH_ROUNDING_ID_TEXT]
     *
     */
    @JSONField(name = "cash_rounding_id_text")
    @JsonProperty("cash_rounding_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String cashRoundingIdText;

    /**
     * 属性 [VENDOR_BILL_ID_TEXT]
     *
     */
    @JSONField(name = "vendor_bill_id_text")
    @JsonProperty("vendor_bill_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String vendorBillIdText;

    /**
     * 属性 [REFUND_INVOICE_ID_TEXT]
     *
     */
    @JSONField(name = "refund_invoice_id_text")
    @JsonProperty("refund_invoice_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String refundInvoiceIdText;

    /**
     * 属性 [FISCAL_POSITION_ID_TEXT]
     *
     */
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String fiscalPositionIdText;

    /**
     * 属性 [NUMBER]
     *
     */
    @JSONField(name = "number")
    @JsonProperty("number")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String number;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String commercialPartnerIdText;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String teamIdText;

    /**
     * 属性 [PAYMENT_TERM_ID_TEXT]
     *
     */
    @JSONField(name = "payment_term_id_text")
    @JsonProperty("payment_term_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String paymentTermIdText;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teamId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountId;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long mediumId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[公司]不允许为空!")
    private Long companyId;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long moveId;

    /**
     * 属性 [PAYMENT_TERM_ID]
     *
     */
    @JSONField(name = "payment_term_id")
    @JsonProperty("payment_term_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long paymentTermId;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[日记账]不允许为空!")
    private Long journalId;

    /**
     * 属性 [INCOTERM_ID]
     *
     */
    @JSONField(name = "incoterm_id")
    @JsonProperty("incoterm_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long incotermId;

    /**
     * 属性 [PURCHASE_ID]
     *
     */
    @JSONField(name = "purchase_id")
    @JsonProperty("purchase_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long purchaseId;

    /**
     * 属性 [FISCAL_POSITION_ID]
     *
     */
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long fiscalPositionId;

    /**
     * 属性 [REFUND_INVOICE_ID]
     *
     */
    @JSONField(name = "refund_invoice_id")
    @JsonProperty("refund_invoice_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long refundInvoiceId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[币种]不允许为空!")
    private Long currencyId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [PARTNER_BANK_ID]
     *
     */
    @JSONField(name = "partner_bank_id")
    @JsonProperty("partner_bank_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerBankId;

    /**
     * 属性 [CASH_ROUNDING_ID]
     *
     */
    @JSONField(name = "cash_rounding_id")
    @JsonProperty("cash_rounding_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long cashRoundingId;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long commercialPartnerId;

    /**
     * 属性 [PARTNER_SHIPPING_ID]
     *
     */
    @JSONField(name = "partner_shipping_id")
    @JsonProperty("partner_shipping_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerShippingId;

    /**
     * 属性 [INCOTERMS_ID]
     *
     */
    @JSONField(name = "incoterms_id")
    @JsonProperty("incoterms_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long incotermsId;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sourceId;

    /**
     * 属性 [VENDOR_BILL_ID]
     *
     */
    @JSONField(name = "vendor_bill_id")
    @JsonProperty("vendor_bill_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long vendorBillId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [VENDOR_BILL_PURCHASE_ID]
     *
     */
    @JSONField(name = "vendor_bill_purchase_id")
    @JsonProperty("vendor_bill_purchase_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long vendorBillPurchaseId;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long campaignId;


    /**
     * 设置 [MOVE_NAME]
     */
    public void setMoveName(String  moveName){
        this.moveName = moveName ;
        this.modify("move_name",moveName);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [AMOUNT_TOTAL_COMPANY_SIGNED]
     */
    public void setAmountTotalCompanySigned(BigDecimal  amountTotalCompanySigned){
        this.amountTotalCompanySigned = amountTotalCompanySigned ;
        this.modify("amount_total_company_signed",amountTotalCompanySigned);
    }

    /**
     * 设置 [AMOUNT_UNTAXED_SIGNED]
     */
    public void setAmountUntaxedSigned(BigDecimal  amountUntaxedSigned){
        this.amountUntaxedSigned = amountUntaxedSigned ;
        this.modify("amount_untaxed_signed",amountUntaxedSigned);
    }

    /**
     * 设置 [RESIDUAL]
     */
    public void setResidual(BigDecimal  residual){
        this.residual = residual ;
        this.modify("residual",residual);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [COMMENT]
     */
    public void setComment(String  comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }

    /**
     * 设置 [RECONCILED]
     */
    public void setReconciled(Boolean  reconciled){
        this.reconciled = reconciled ;
        this.modify("reconciled",reconciled);
    }

    /**
     * 设置 [SENT]
     */
    public void setSent(Boolean  sent){
        this.sent = sent ;
        this.modify("sent",sent);
    }

    /**
     * 设置 [AMOUNT_TOTAL_SIGNED]
     */
    public void setAmountTotalSigned(BigDecimal  amountTotalSigned){
        this.amountTotalSigned = amountTotalSigned ;
        this.modify("amount_total_signed",amountTotalSigned);
    }

    /**
     * 设置 [RESIDUAL_SIGNED]
     */
    public void setResidualSigned(BigDecimal  residualSigned){
        this.residualSigned = residualSigned ;
        this.modify("residual_signed",residualSigned);
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    public void setAccessToken(String  accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [VENDOR_DISPLAY_NAME]
     */
    public void setVendorDisplayName(String  vendorDisplayName){
        this.vendorDisplayName = vendorDisplayName ;
        this.modify("vendor_display_name",vendorDisplayName);
    }

    /**
     * 设置 [AMOUNT_UNTAXED]
     */
    public void setAmountUntaxed(BigDecimal  amountUntaxed){
        this.amountUntaxed = amountUntaxed ;
        this.modify("amount_untaxed",amountUntaxed);
    }

    /**
     * 设置 [DATE_DUE]
     */
    public void setDateDue(Timestamp  dateDue){
        this.dateDue = dateDue ;
        this.modify("date_due",dateDue);
    }

    /**
     * 设置 [SOURCE_EMAIL]
     */
    public void setSourceEmail(String  sourceEmail){
        this.sourceEmail = sourceEmail ;
        this.modify("source_email",sourceEmail);
    }

    /**
     * 设置 [AMOUNT_TAX]
     */
    public void setAmountTax(BigDecimal  amountTax){
        this.amountTax = amountTax ;
        this.modify("amount_tax",amountTax);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [RESIDUAL_COMPANY_SIGNED]
     */
    public void setResidualCompanySigned(BigDecimal  residualCompanySigned){
        this.residualCompanySigned = residualCompanySigned ;
        this.modify("residual_company_signed",residualCompanySigned);
    }

    /**
     * 设置 [DATE_INVOICE]
     */
    public void setDateInvoice(Timestamp  dateInvoice){
        this.dateInvoice = dateInvoice ;
        this.modify("date_invoice",dateInvoice);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [REFERENCE]
     */
    public void setReference(String  reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [AMOUNT_TOTAL]
     */
    public void setAmountTotal(BigDecimal  amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [TEAM_ID]
     */
    public void setTeamId(Long  teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Long  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    public void setMediumId(Long  mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [MOVE_ID]
     */
    public void setMoveId(Long  moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [PAYMENT_TERM_ID]
     */
    public void setPaymentTermId(Long  paymentTermId){
        this.paymentTermId = paymentTermId ;
        this.modify("payment_term_id",paymentTermId);
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Long  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [INCOTERM_ID]
     */
    public void setIncotermId(Long  incotermId){
        this.incotermId = incotermId ;
        this.modify("incoterm_id",incotermId);
    }

    /**
     * 设置 [PURCHASE_ID]
     */
    public void setPurchaseId(Long  purchaseId){
        this.purchaseId = purchaseId ;
        this.modify("purchase_id",purchaseId);
    }

    /**
     * 设置 [FISCAL_POSITION_ID]
     */
    public void setFiscalPositionId(Long  fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }

    /**
     * 设置 [REFUND_INVOICE_ID]
     */
    public void setRefundInvoiceId(Long  refundInvoiceId){
        this.refundInvoiceId = refundInvoiceId ;
        this.modify("refund_invoice_id",refundInvoiceId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [PARTNER_BANK_ID]
     */
    public void setPartnerBankId(Long  partnerBankId){
        this.partnerBankId = partnerBankId ;
        this.modify("partner_bank_id",partnerBankId);
    }

    /**
     * 设置 [CASH_ROUNDING_ID]
     */
    public void setCashRoundingId(Long  cashRoundingId){
        this.cashRoundingId = cashRoundingId ;
        this.modify("cash_rounding_id",cashRoundingId);
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    public void setCommercialPartnerId(Long  commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }

    /**
     * 设置 [PARTNER_SHIPPING_ID]
     */
    public void setPartnerShippingId(Long  partnerShippingId){
        this.partnerShippingId = partnerShippingId ;
        this.modify("partner_shipping_id",partnerShippingId);
    }

    /**
     * 设置 [INCOTERMS_ID]
     */
    public void setIncotermsId(Long  incotermsId){
        this.incotermsId = incotermsId ;
        this.modify("incoterms_id",incotermsId);
    }

    /**
     * 设置 [SOURCE_ID]
     */
    public void setSourceId(Long  sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [VENDOR_BILL_ID]
     */
    public void setVendorBillId(Long  vendorBillId){
        this.vendorBillId = vendorBillId ;
        this.modify("vendor_bill_id",vendorBillId);
    }

    /**
     * 设置 [VENDOR_BILL_PURCHASE_ID]
     */
    public void setVendorBillPurchaseId(Long  vendorBillPurchaseId){
        this.vendorBillPurchaseId = vendorBillPurchaseId ;
        this.modify("vendor_bill_purchase_id",vendorBillPurchaseId);
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    public void setCampaignId(Long  campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }


}



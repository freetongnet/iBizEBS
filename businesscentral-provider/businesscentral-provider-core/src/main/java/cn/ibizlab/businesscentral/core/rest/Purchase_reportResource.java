package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_reportService;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_reportSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"采购报表" })
@RestController("Core-purchase_report")
@RequestMapping("")
public class Purchase_reportResource {

    @Autowired
    public IPurchase_reportService purchase_reportService;

    @Autowired
    @Lazy
    public Purchase_reportMapping purchase_reportMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-Create-all')")
    @ApiOperation(value = "新建采购报表", tags = {"采购报表" },  notes = "新建采购报表")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_reports")
    public ResponseEntity<Purchase_reportDTO> create(@Validated @RequestBody Purchase_reportDTO purchase_reportdto) {
        Purchase_report domain = purchase_reportMapping.toDomain(purchase_reportdto);
		purchase_reportService.create(domain);
        Purchase_reportDTO dto = purchase_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-Create-all')")
    @ApiOperation(value = "批量新建采购报表", tags = {"采购报表" },  notes = "批量新建采购报表")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_reportDTO> purchase_reportdtos) {
        purchase_reportService.createBatch(purchase_reportMapping.toDomain(purchase_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-Update-all')")
    @ApiOperation(value = "更新采购报表", tags = {"采购报表" },  notes = "更新采购报表")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_reports/{purchase_report_id}")
    public ResponseEntity<Purchase_reportDTO> update(@PathVariable("purchase_report_id") Long purchase_report_id, @RequestBody Purchase_reportDTO purchase_reportdto) {
		Purchase_report domain  = purchase_reportMapping.toDomain(purchase_reportdto);
        domain .setId(purchase_report_id);
		purchase_reportService.update(domain );
		Purchase_reportDTO dto = purchase_reportMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-Update-all')")
    @ApiOperation(value = "批量更新采购报表", tags = {"采购报表" },  notes = "批量更新采购报表")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_reportDTO> purchase_reportdtos) {
        purchase_reportService.updateBatch(purchase_reportMapping.toDomain(purchase_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-Remove-all')")
    @ApiOperation(value = "删除采购报表", tags = {"采购报表" },  notes = "删除采购报表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_reports/{purchase_report_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("purchase_report_id") Long purchase_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_reportService.remove(purchase_report_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-Remove-all')")
    @ApiOperation(value = "批量删除采购报表", tags = {"采购报表" },  notes = "批量删除采购报表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        purchase_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-Get-all')")
    @ApiOperation(value = "获取采购报表", tags = {"采购报表" },  notes = "获取采购报表")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_reports/{purchase_report_id}")
    public ResponseEntity<Purchase_reportDTO> get(@PathVariable("purchase_report_id") Long purchase_report_id) {
        Purchase_report domain = purchase_reportService.get(purchase_report_id);
        Purchase_reportDTO dto = purchase_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取采购报表草稿", tags = {"采购报表" },  notes = "获取采购报表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_reports/getdraft")
    public ResponseEntity<Purchase_reportDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_reportMapping.toDto(purchase_reportService.getDraft(new Purchase_report())));
    }

    @ApiOperation(value = "检查采购报表", tags = {"采购报表" },  notes = "检查采购报表")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_reports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Purchase_reportDTO purchase_reportdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(purchase_reportService.checkKey(purchase_reportMapping.toDomain(purchase_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-Save-all')")
    @ApiOperation(value = "保存采购报表", tags = {"采购报表" },  notes = "保存采购报表")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_reports/save")
    public ResponseEntity<Boolean> save(@RequestBody Purchase_reportDTO purchase_reportdto) {
        return ResponseEntity.status(HttpStatus.OK).body(purchase_reportService.save(purchase_reportMapping.toDomain(purchase_reportdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-Save-all')")
    @ApiOperation(value = "批量保存采购报表", tags = {"采购报表" },  notes = "批量保存采购报表")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_reports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Purchase_reportDTO> purchase_reportdtos) {
        purchase_reportService.saveBatch(purchase_reportMapping.toDomain(purchase_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"采购报表" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_reports/fetchdefault")
	public ResponseEntity<List<Purchase_reportDTO>> fetchDefault(Purchase_reportSearchContext context) {
        Page<Purchase_report> domains = purchase_reportService.searchDefault(context) ;
        List<Purchase_reportDTO> list = purchase_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Purchase_report-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"采购报表" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/purchase_reports/searchdefault")
	public ResponseEntity<Page<Purchase_reportDTO>> searchDefault(@RequestBody Purchase_reportSearchContext context) {
        Page<Purchase_report> domains = purchase_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


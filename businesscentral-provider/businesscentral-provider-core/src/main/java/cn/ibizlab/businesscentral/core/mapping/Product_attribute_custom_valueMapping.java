package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_attribute_custom_value;
import cn.ibizlab.businesscentral.core.dto.Product_attribute_custom_valueDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreProduct_attribute_custom_valueMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Product_attribute_custom_valueMapping extends MappingBase<Product_attribute_custom_valueDTO, Product_attribute_custom_value> {


}


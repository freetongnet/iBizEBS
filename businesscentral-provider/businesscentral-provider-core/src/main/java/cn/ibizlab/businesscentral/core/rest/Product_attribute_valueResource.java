package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_attribute_value;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_attribute_valueService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_attribute_valueSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"属性值" })
@RestController("Core-product_attribute_value")
@RequestMapping("")
public class Product_attribute_valueResource {

    @Autowired
    public IProduct_attribute_valueService product_attribute_valueService;

    @Autowired
    @Lazy
    public Product_attribute_valueMapping product_attribute_valueMapping;

    @PreAuthorize("hasPermission(this.product_attribute_valueMapping.toDomain(#product_attribute_valuedto),'iBizBusinessCentral-Product_attribute_value-Create')")
    @ApiOperation(value = "新建属性值", tags = {"属性值" },  notes = "新建属性值")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attribute_values")
    public ResponseEntity<Product_attribute_valueDTO> create(@Validated @RequestBody Product_attribute_valueDTO product_attribute_valuedto) {
        Product_attribute_value domain = product_attribute_valueMapping.toDomain(product_attribute_valuedto);
		product_attribute_valueService.create(domain);
        Product_attribute_valueDTO dto = product_attribute_valueMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_attribute_valueMapping.toDomain(#product_attribute_valuedtos),'iBizBusinessCentral-Product_attribute_value-Create')")
    @ApiOperation(value = "批量新建属性值", tags = {"属性值" },  notes = "批量新建属性值")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attribute_values/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_attribute_valueDTO> product_attribute_valuedtos) {
        product_attribute_valueService.createBatch(product_attribute_valueMapping.toDomain(product_attribute_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_attribute_value" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_attribute_valueService.get(#product_attribute_value_id),'iBizBusinessCentral-Product_attribute_value-Update')")
    @ApiOperation(value = "更新属性值", tags = {"属性值" },  notes = "更新属性值")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_attribute_values/{product_attribute_value_id}")
    public ResponseEntity<Product_attribute_valueDTO> update(@PathVariable("product_attribute_value_id") Long product_attribute_value_id, @RequestBody Product_attribute_valueDTO product_attribute_valuedto) {
		Product_attribute_value domain  = product_attribute_valueMapping.toDomain(product_attribute_valuedto);
        domain .setId(product_attribute_value_id);
		product_attribute_valueService.update(domain );
		Product_attribute_valueDTO dto = product_attribute_valueMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_attribute_valueService.getProductAttributeValueByEntities(this.product_attribute_valueMapping.toDomain(#product_attribute_valuedtos)),'iBizBusinessCentral-Product_attribute_value-Update')")
    @ApiOperation(value = "批量更新属性值", tags = {"属性值" },  notes = "批量更新属性值")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_attribute_values/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_attribute_valueDTO> product_attribute_valuedtos) {
        product_attribute_valueService.updateBatch(product_attribute_valueMapping.toDomain(product_attribute_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_attribute_valueService.get(#product_attribute_value_id),'iBizBusinessCentral-Product_attribute_value-Remove')")
    @ApiOperation(value = "删除属性值", tags = {"属性值" },  notes = "删除属性值")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_attribute_values/{product_attribute_value_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_attribute_value_id") Long product_attribute_value_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_attribute_valueService.remove(product_attribute_value_id));
    }

    @PreAuthorize("hasPermission(this.product_attribute_valueService.getProductAttributeValueByIds(#ids),'iBizBusinessCentral-Product_attribute_value-Remove')")
    @ApiOperation(value = "批量删除属性值", tags = {"属性值" },  notes = "批量删除属性值")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_attribute_values/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_attribute_valueService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_attribute_valueMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_attribute_value-Get')")
    @ApiOperation(value = "获取属性值", tags = {"属性值" },  notes = "获取属性值")
	@RequestMapping(method = RequestMethod.GET, value = "/product_attribute_values/{product_attribute_value_id}")
    public ResponseEntity<Product_attribute_valueDTO> get(@PathVariable("product_attribute_value_id") Long product_attribute_value_id) {
        Product_attribute_value domain = product_attribute_valueService.get(product_attribute_value_id);
        Product_attribute_valueDTO dto = product_attribute_valueMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取属性值草稿", tags = {"属性值" },  notes = "获取属性值草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_attribute_values/getdraft")
    public ResponseEntity<Product_attribute_valueDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_attribute_valueMapping.toDto(product_attribute_valueService.getDraft(new Product_attribute_value())));
    }

    @ApiOperation(value = "检查属性值", tags = {"属性值" },  notes = "检查属性值")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attribute_values/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_attribute_valueDTO product_attribute_valuedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_attribute_valueService.checkKey(product_attribute_valueMapping.toDomain(product_attribute_valuedto)));
    }

    @PreAuthorize("hasPermission(this.product_attribute_valueMapping.toDomain(#product_attribute_valuedto),'iBizBusinessCentral-Product_attribute_value-Save')")
    @ApiOperation(value = "保存属性值", tags = {"属性值" },  notes = "保存属性值")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attribute_values/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_attribute_valueDTO product_attribute_valuedto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_attribute_valueService.save(product_attribute_valueMapping.toDomain(product_attribute_valuedto)));
    }

    @PreAuthorize("hasPermission(this.product_attribute_valueMapping.toDomain(#product_attribute_valuedtos),'iBizBusinessCentral-Product_attribute_value-Save')")
    @ApiOperation(value = "批量保存属性值", tags = {"属性值" },  notes = "批量保存属性值")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attribute_values/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_attribute_valueDTO> product_attribute_valuedtos) {
        product_attribute_valueService.saveBatch(product_attribute_valueMapping.toDomain(product_attribute_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_attribute_value-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_attribute_value-Get')")
	@ApiOperation(value = "获取数据集", tags = {"属性值" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_attribute_values/fetchdefault")
	public ResponseEntity<List<Product_attribute_valueDTO>> fetchDefault(Product_attribute_valueSearchContext context) {
        Page<Product_attribute_value> domains = product_attribute_valueService.searchDefault(context) ;
        List<Product_attribute_valueDTO> list = product_attribute_valueMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_attribute_value-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_attribute_value-Get')")
	@ApiOperation(value = "查询数据集", tags = {"属性值" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_attribute_values/searchdefault")
	public ResponseEntity<Page<Product_attribute_valueDTO>> searchDefault(@RequestBody Product_attribute_valueSearchContext context) {
        Page<Product_attribute_value> domains = product_attribute_valueService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_attribute_valueMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


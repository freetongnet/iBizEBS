package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_state;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_stateService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_stateSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆状态" })
@RestController("Core-fleet_vehicle_state")
@RequestMapping("")
public class Fleet_vehicle_stateResource {

    @Autowired
    public IFleet_vehicle_stateService fleet_vehicle_stateService;

    @Autowired
    @Lazy
    public Fleet_vehicle_stateMapping fleet_vehicle_stateMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_stateMapping.toDomain(#fleet_vehicle_statedto),'iBizBusinessCentral-Fleet_vehicle_state-Create')")
    @ApiOperation(value = "新建车辆状态", tags = {"车辆状态" },  notes = "新建车辆状态")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states")
    public ResponseEntity<Fleet_vehicle_stateDTO> create(@Validated @RequestBody Fleet_vehicle_stateDTO fleet_vehicle_statedto) {
        Fleet_vehicle_state domain = fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedto);
		fleet_vehicle_stateService.create(domain);
        Fleet_vehicle_stateDTO dto = fleet_vehicle_stateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_stateMapping.toDomain(#fleet_vehicle_statedtos),'iBizBusinessCentral-Fleet_vehicle_state-Create')")
    @ApiOperation(value = "批量新建车辆状态", tags = {"车辆状态" },  notes = "批量新建车辆状态")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_stateDTO> fleet_vehicle_statedtos) {
        fleet_vehicle_stateService.createBatch(fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_state" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_stateService.get(#fleet_vehicle_state_id),'iBizBusinessCentral-Fleet_vehicle_state-Update')")
    @ApiOperation(value = "更新车辆状态", tags = {"车辆状态" },  notes = "更新车辆状态")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_states/{fleet_vehicle_state_id}")
    public ResponseEntity<Fleet_vehicle_stateDTO> update(@PathVariable("fleet_vehicle_state_id") Long fleet_vehicle_state_id, @RequestBody Fleet_vehicle_stateDTO fleet_vehicle_statedto) {
		Fleet_vehicle_state domain  = fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedto);
        domain .setId(fleet_vehicle_state_id);
		fleet_vehicle_stateService.update(domain );
		Fleet_vehicle_stateDTO dto = fleet_vehicle_stateMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_stateService.getFleetVehicleStateByEntities(this.fleet_vehicle_stateMapping.toDomain(#fleet_vehicle_statedtos)),'iBizBusinessCentral-Fleet_vehicle_state-Update')")
    @ApiOperation(value = "批量更新车辆状态", tags = {"车辆状态" },  notes = "批量更新车辆状态")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_states/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_stateDTO> fleet_vehicle_statedtos) {
        fleet_vehicle_stateService.updateBatch(fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_stateService.get(#fleet_vehicle_state_id),'iBizBusinessCentral-Fleet_vehicle_state-Remove')")
    @ApiOperation(value = "删除车辆状态", tags = {"车辆状态" },  notes = "删除车辆状态")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_states/{fleet_vehicle_state_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_state_id") Long fleet_vehicle_state_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_stateService.remove(fleet_vehicle_state_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_stateService.getFleetVehicleStateByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_state-Remove')")
    @ApiOperation(value = "批量删除车辆状态", tags = {"车辆状态" },  notes = "批量删除车辆状态")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_states/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_stateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_stateMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_state-Get')")
    @ApiOperation(value = "获取车辆状态", tags = {"车辆状态" },  notes = "获取车辆状态")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_states/{fleet_vehicle_state_id}")
    public ResponseEntity<Fleet_vehicle_stateDTO> get(@PathVariable("fleet_vehicle_state_id") Long fleet_vehicle_state_id) {
        Fleet_vehicle_state domain = fleet_vehicle_stateService.get(fleet_vehicle_state_id);
        Fleet_vehicle_stateDTO dto = fleet_vehicle_stateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆状态草稿", tags = {"车辆状态" },  notes = "获取车辆状态草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_states/getdraft")
    public ResponseEntity<Fleet_vehicle_stateDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_stateMapping.toDto(fleet_vehicle_stateService.getDraft(new Fleet_vehicle_state())));
    }

    @ApiOperation(value = "检查车辆状态", tags = {"车辆状态" },  notes = "检查车辆状态")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_stateDTO fleet_vehicle_statedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_stateService.checkKey(fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_stateMapping.toDomain(#fleet_vehicle_statedto),'iBizBusinessCentral-Fleet_vehicle_state-Save')")
    @ApiOperation(value = "保存车辆状态", tags = {"车辆状态" },  notes = "保存车辆状态")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_stateDTO fleet_vehicle_statedto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_stateService.save(fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_stateMapping.toDomain(#fleet_vehicle_statedtos),'iBizBusinessCentral-Fleet_vehicle_state-Save')")
    @ApiOperation(value = "批量保存车辆状态", tags = {"车辆状态" },  notes = "批量保存车辆状态")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_stateDTO> fleet_vehicle_statedtos) {
        fleet_vehicle_stateService.saveBatch(fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_state-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_state-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆状态" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_states/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_stateDTO>> fetchDefault(Fleet_vehicle_stateSearchContext context) {
        Page<Fleet_vehicle_state> domains = fleet_vehicle_stateService.searchDefault(context) ;
        List<Fleet_vehicle_stateDTO> list = fleet_vehicle_stateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_state-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_state-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆状态" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_states/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_stateDTO>> searchDefault(@RequestBody Fleet_vehicle_stateSearchContext context) {
        Page<Fleet_vehicle_state> domains = fleet_vehicle_stateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_stateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


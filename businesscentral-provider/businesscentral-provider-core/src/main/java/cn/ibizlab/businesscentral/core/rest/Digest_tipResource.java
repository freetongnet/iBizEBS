package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.businesscentral.core.odoo_digest.service.IDigest_tipService;
import cn.ibizlab.businesscentral.core.odoo_digest.filter.Digest_tipSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"摘要提示" })
@RestController("Core-digest_tip")
@RequestMapping("")
public class Digest_tipResource {

    @Autowired
    public IDigest_tipService digest_tipService;

    @Autowired
    @Lazy
    public Digest_tipMapping digest_tipMapping;

    @PreAuthorize("hasPermission(this.digest_tipMapping.toDomain(#digest_tipdto),'iBizBusinessCentral-Digest_tip-Create')")
    @ApiOperation(value = "新建摘要提示", tags = {"摘要提示" },  notes = "新建摘要提示")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_tips")
    public ResponseEntity<Digest_tipDTO> create(@Validated @RequestBody Digest_tipDTO digest_tipdto) {
        Digest_tip domain = digest_tipMapping.toDomain(digest_tipdto);
		digest_tipService.create(domain);
        Digest_tipDTO dto = digest_tipMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.digest_tipMapping.toDomain(#digest_tipdtos),'iBizBusinessCentral-Digest_tip-Create')")
    @ApiOperation(value = "批量新建摘要提示", tags = {"摘要提示" },  notes = "批量新建摘要提示")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_tips/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Digest_tipDTO> digest_tipdtos) {
        digest_tipService.createBatch(digest_tipMapping.toDomain(digest_tipdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "digest_tip" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.digest_tipService.get(#digest_tip_id),'iBizBusinessCentral-Digest_tip-Update')")
    @ApiOperation(value = "更新摘要提示", tags = {"摘要提示" },  notes = "更新摘要提示")
	@RequestMapping(method = RequestMethod.PUT, value = "/digest_tips/{digest_tip_id}")
    public ResponseEntity<Digest_tipDTO> update(@PathVariable("digest_tip_id") Long digest_tip_id, @RequestBody Digest_tipDTO digest_tipdto) {
		Digest_tip domain  = digest_tipMapping.toDomain(digest_tipdto);
        domain .setId(digest_tip_id);
		digest_tipService.update(domain );
		Digest_tipDTO dto = digest_tipMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.digest_tipService.getDigestTipByEntities(this.digest_tipMapping.toDomain(#digest_tipdtos)),'iBizBusinessCentral-Digest_tip-Update')")
    @ApiOperation(value = "批量更新摘要提示", tags = {"摘要提示" },  notes = "批量更新摘要提示")
	@RequestMapping(method = RequestMethod.PUT, value = "/digest_tips/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Digest_tipDTO> digest_tipdtos) {
        digest_tipService.updateBatch(digest_tipMapping.toDomain(digest_tipdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.digest_tipService.get(#digest_tip_id),'iBizBusinessCentral-Digest_tip-Remove')")
    @ApiOperation(value = "删除摘要提示", tags = {"摘要提示" },  notes = "删除摘要提示")
	@RequestMapping(method = RequestMethod.DELETE, value = "/digest_tips/{digest_tip_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("digest_tip_id") Long digest_tip_id) {
         return ResponseEntity.status(HttpStatus.OK).body(digest_tipService.remove(digest_tip_id));
    }

    @PreAuthorize("hasPermission(this.digest_tipService.getDigestTipByIds(#ids),'iBizBusinessCentral-Digest_tip-Remove')")
    @ApiOperation(value = "批量删除摘要提示", tags = {"摘要提示" },  notes = "批量删除摘要提示")
	@RequestMapping(method = RequestMethod.DELETE, value = "/digest_tips/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        digest_tipService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.digest_tipMapping.toDomain(returnObject.body),'iBizBusinessCentral-Digest_tip-Get')")
    @ApiOperation(value = "获取摘要提示", tags = {"摘要提示" },  notes = "获取摘要提示")
	@RequestMapping(method = RequestMethod.GET, value = "/digest_tips/{digest_tip_id}")
    public ResponseEntity<Digest_tipDTO> get(@PathVariable("digest_tip_id") Long digest_tip_id) {
        Digest_tip domain = digest_tipService.get(digest_tip_id);
        Digest_tipDTO dto = digest_tipMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取摘要提示草稿", tags = {"摘要提示" },  notes = "获取摘要提示草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/digest_tips/getdraft")
    public ResponseEntity<Digest_tipDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(digest_tipMapping.toDto(digest_tipService.getDraft(new Digest_tip())));
    }

    @ApiOperation(value = "检查摘要提示", tags = {"摘要提示" },  notes = "检查摘要提示")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_tips/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Digest_tipDTO digest_tipdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(digest_tipService.checkKey(digest_tipMapping.toDomain(digest_tipdto)));
    }

    @PreAuthorize("hasPermission(this.digest_tipMapping.toDomain(#digest_tipdto),'iBizBusinessCentral-Digest_tip-Save')")
    @ApiOperation(value = "保存摘要提示", tags = {"摘要提示" },  notes = "保存摘要提示")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_tips/save")
    public ResponseEntity<Boolean> save(@RequestBody Digest_tipDTO digest_tipdto) {
        return ResponseEntity.status(HttpStatus.OK).body(digest_tipService.save(digest_tipMapping.toDomain(digest_tipdto)));
    }

    @PreAuthorize("hasPermission(this.digest_tipMapping.toDomain(#digest_tipdtos),'iBizBusinessCentral-Digest_tip-Save')")
    @ApiOperation(value = "批量保存摘要提示", tags = {"摘要提示" },  notes = "批量保存摘要提示")
	@RequestMapping(method = RequestMethod.POST, value = "/digest_tips/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Digest_tipDTO> digest_tipdtos) {
        digest_tipService.saveBatch(digest_tipMapping.toDomain(digest_tipdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Digest_tip-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Digest_tip-Get')")
	@ApiOperation(value = "获取数据集", tags = {"摘要提示" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/digest_tips/fetchdefault")
	public ResponseEntity<List<Digest_tipDTO>> fetchDefault(Digest_tipSearchContext context) {
        Page<Digest_tip> domains = digest_tipService.searchDefault(context) ;
        List<Digest_tipDTO> list = digest_tipMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Digest_tip-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Digest_tip-Get')")
	@ApiOperation(value = "查询数据集", tags = {"摘要提示" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/digest_tips/searchdefault")
	public ResponseEntity<Page<Digest_tipDTO>> searchDefault(@RequestBody Digest_tipSearchContext context) {
        Page<Digest_tip> domains = digest_tipService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(digest_tipMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


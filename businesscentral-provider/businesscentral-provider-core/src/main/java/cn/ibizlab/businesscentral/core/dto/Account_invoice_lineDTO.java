package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Account_invoice_lineDTO]
 */
@Data
public class Account_invoice_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_TYPE]
     *
     */
    @JSONField(name = "display_type")
    @JsonProperty("display_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String displayType;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    @NotBlank(message = "[说明]不允许为空!")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String name;

    /**
     * 属性 [PRICE_SUBTOTAL]
     *
     */
    @JSONField(name = "price_subtotal")
    @JsonProperty("price_subtotal")
    private BigDecimal priceSubtotal;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String origin;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SALE_LINE_IDS]
     *
     */
    @JSONField(name = "sale_line_ids")
    @JsonProperty("sale_line_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String saleLineIds;

    /**
     * 属性 [DISCOUNT]
     *
     */
    @JSONField(name = "discount")
    @JsonProperty("discount")
    private Double discount;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private BigDecimal priceTotal;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [QUANTITY]
     *
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    @NotNull(message = "[数量]不允许为空!")
    private Double quantity;

    /**
     * 属性 [PRICE_TAX]
     *
     */
    @JSONField(name = "price_tax")
    @JsonProperty("price_tax")
    private BigDecimal priceTax;

    /**
     * 属性 [PRICE_SUBTOTAL_SIGNED]
     *
     */
    @JSONField(name = "price_subtotal_signed")
    @JsonProperty("price_subtotal_signed")
    private BigDecimal priceSubtotalSigned;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    @NotNull(message = "[单价]不允许为空!")
    private Double priceUnit;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [INVOICE_LINE_TAX_IDS]
     *
     */
    @JSONField(name = "invoice_line_tax_ids")
    @JsonProperty("invoice_line_tax_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String invoiceLineTaxIds;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    private String analyticTagIds;

    /**
     * 属性 [IS_ROUNDING_LINE]
     *
     */
    @JSONField(name = "is_rounding_line")
    @JsonProperty("is_rounding_line")
    private Boolean isRoundingLine;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID_TEXT]
     *
     */
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountAnalyticIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [INVOICE_ID_TEXT]
     *
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String invoiceIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [INVOICE_TYPE]
     *
     */
    @JSONField(name = "invoice_type")
    @JsonProperty("invoice_type")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String invoiceType;

    /**
     * 属性 [PRODUCT_IMAGE]
     *
     */
    @JSONField(name = "product_image")
    @JsonProperty("product_image")
    private byte[] productImage;

    /**
     * 属性 [UOM_ID_TEXT]
     *
     */
    @JSONField(name = "uom_id_text")
    @JsonProperty("uom_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String uomIdText;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyCurrencyId;

    /**
     * 属性 [PURCHASE_ID]
     *
     */
    @JSONField(name = "purchase_id")
    @JsonProperty("purchase_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long purchaseId;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountIdText;

    /**
     * 属性 [PURCHASE_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "purchase_line_id_text")
    @JsonProperty("purchase_line_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String purchaseLineIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [INVOICE_ID]
     *
     */
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long invoiceId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountId;

    /**
     * 属性 [PURCHASE_LINE_ID]
     *
     */
    @JSONField(name = "purchase_line_id")
    @JsonProperty("purchase_line_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long purchaseLineId;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountAnalyticId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [UOM_ID]
     *
     */
    @JSONField(name = "uom_id")
    @JsonProperty("uom_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long uomId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;


    /**
     * 设置 [DISPLAY_TYPE]
     */
    public void setDisplayType(String  displayType){
        this.displayType = displayType ;
        this.modify("display_type",displayType);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PRICE_SUBTOTAL]
     */
    public void setPriceSubtotal(BigDecimal  priceSubtotal){
        this.priceSubtotal = priceSubtotal ;
        this.modify("price_subtotal",priceSubtotal);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [DISCOUNT]
     */
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.modify("discount",discount);
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    public void setPriceTotal(BigDecimal  priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }

    /**
     * 设置 [QUANTITY]
     */
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [PRICE_SUBTOTAL_SIGNED]
     */
    public void setPriceSubtotalSigned(BigDecimal  priceSubtotalSigned){
        this.priceSubtotalSigned = priceSubtotalSigned ;
        this.modify("price_subtotal_signed",priceSubtotalSigned);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    public void setPriceUnit(Double  priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [IS_ROUNDING_LINE]
     */
    public void setIsRoundingLine(Boolean  isRoundingLine){
        this.isRoundingLine = isRoundingLine ;
        this.modify("is_rounding_line",isRoundingLine);
    }

    /**
     * 设置 [INVOICE_ID]
     */
    public void setInvoiceId(Long  invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Long  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [PURCHASE_LINE_ID]
     */
    public void setPurchaseLineId(Long  purchaseLineId){
        this.purchaseLineId = purchaseLineId ;
        this.modify("purchase_line_id",purchaseLineId);
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    public void setAccountAnalyticId(Long  accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [UOM_ID]
     */
    public void setUomId(Long  uomId){
        this.uomId = uomId ;
        this.modify("uom_id",uomId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }


}



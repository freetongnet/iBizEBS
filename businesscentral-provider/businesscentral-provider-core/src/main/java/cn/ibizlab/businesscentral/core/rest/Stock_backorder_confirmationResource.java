package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_backorder_confirmation;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_backorder_confirmationService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_backorder_confirmationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"欠单确认" })
@RestController("Core-stock_backorder_confirmation")
@RequestMapping("")
public class Stock_backorder_confirmationResource {

    @Autowired
    public IStock_backorder_confirmationService stock_backorder_confirmationService;

    @Autowired
    @Lazy
    public Stock_backorder_confirmationMapping stock_backorder_confirmationMapping;

    @PreAuthorize("hasPermission(this.stock_backorder_confirmationMapping.toDomain(#stock_backorder_confirmationdto),'iBizBusinessCentral-Stock_backorder_confirmation-Create')")
    @ApiOperation(value = "新建欠单确认", tags = {"欠单确认" },  notes = "新建欠单确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations")
    public ResponseEntity<Stock_backorder_confirmationDTO> create(@Validated @RequestBody Stock_backorder_confirmationDTO stock_backorder_confirmationdto) {
        Stock_backorder_confirmation domain = stock_backorder_confirmationMapping.toDomain(stock_backorder_confirmationdto);
		stock_backorder_confirmationService.create(domain);
        Stock_backorder_confirmationDTO dto = stock_backorder_confirmationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_backorder_confirmationMapping.toDomain(#stock_backorder_confirmationdtos),'iBizBusinessCentral-Stock_backorder_confirmation-Create')")
    @ApiOperation(value = "批量新建欠单确认", tags = {"欠单确认" },  notes = "批量新建欠单确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_backorder_confirmationDTO> stock_backorder_confirmationdtos) {
        stock_backorder_confirmationService.createBatch(stock_backorder_confirmationMapping.toDomain(stock_backorder_confirmationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_backorder_confirmation" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_backorder_confirmationService.get(#stock_backorder_confirmation_id),'iBizBusinessCentral-Stock_backorder_confirmation-Update')")
    @ApiOperation(value = "更新欠单确认", tags = {"欠单确认" },  notes = "更新欠单确认")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_backorder_confirmations/{stock_backorder_confirmation_id}")
    public ResponseEntity<Stock_backorder_confirmationDTO> update(@PathVariable("stock_backorder_confirmation_id") Long stock_backorder_confirmation_id, @RequestBody Stock_backorder_confirmationDTO stock_backorder_confirmationdto) {
		Stock_backorder_confirmation domain  = stock_backorder_confirmationMapping.toDomain(stock_backorder_confirmationdto);
        domain .setId(stock_backorder_confirmation_id);
		stock_backorder_confirmationService.update(domain );
		Stock_backorder_confirmationDTO dto = stock_backorder_confirmationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_backorder_confirmationService.getStockBackorderConfirmationByEntities(this.stock_backorder_confirmationMapping.toDomain(#stock_backorder_confirmationdtos)),'iBizBusinessCentral-Stock_backorder_confirmation-Update')")
    @ApiOperation(value = "批量更新欠单确认", tags = {"欠单确认" },  notes = "批量更新欠单确认")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_backorder_confirmations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_backorder_confirmationDTO> stock_backorder_confirmationdtos) {
        stock_backorder_confirmationService.updateBatch(stock_backorder_confirmationMapping.toDomain(stock_backorder_confirmationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_backorder_confirmationService.get(#stock_backorder_confirmation_id),'iBizBusinessCentral-Stock_backorder_confirmation-Remove')")
    @ApiOperation(value = "删除欠单确认", tags = {"欠单确认" },  notes = "删除欠单确认")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_backorder_confirmations/{stock_backorder_confirmation_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_backorder_confirmation_id") Long stock_backorder_confirmation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_backorder_confirmationService.remove(stock_backorder_confirmation_id));
    }

    @PreAuthorize("hasPermission(this.stock_backorder_confirmationService.getStockBackorderConfirmationByIds(#ids),'iBizBusinessCentral-Stock_backorder_confirmation-Remove')")
    @ApiOperation(value = "批量删除欠单确认", tags = {"欠单确认" },  notes = "批量删除欠单确认")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_backorder_confirmations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_backorder_confirmationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_backorder_confirmationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_backorder_confirmation-Get')")
    @ApiOperation(value = "获取欠单确认", tags = {"欠单确认" },  notes = "获取欠单确认")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_backorder_confirmations/{stock_backorder_confirmation_id}")
    public ResponseEntity<Stock_backorder_confirmationDTO> get(@PathVariable("stock_backorder_confirmation_id") Long stock_backorder_confirmation_id) {
        Stock_backorder_confirmation domain = stock_backorder_confirmationService.get(stock_backorder_confirmation_id);
        Stock_backorder_confirmationDTO dto = stock_backorder_confirmationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取欠单确认草稿", tags = {"欠单确认" },  notes = "获取欠单确认草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_backorder_confirmations/getdraft")
    public ResponseEntity<Stock_backorder_confirmationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_backorder_confirmationMapping.toDto(stock_backorder_confirmationService.getDraft(new Stock_backorder_confirmation())));
    }

    @ApiOperation(value = "检查欠单确认", tags = {"欠单确认" },  notes = "检查欠单确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_backorder_confirmationDTO stock_backorder_confirmationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_backorder_confirmationService.checkKey(stock_backorder_confirmationMapping.toDomain(stock_backorder_confirmationdto)));
    }

    @PreAuthorize("hasPermission(this.stock_backorder_confirmationMapping.toDomain(#stock_backorder_confirmationdto),'iBizBusinessCentral-Stock_backorder_confirmation-Save')")
    @ApiOperation(value = "保存欠单确认", tags = {"欠单确认" },  notes = "保存欠单确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_backorder_confirmationDTO stock_backorder_confirmationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_backorder_confirmationService.save(stock_backorder_confirmationMapping.toDomain(stock_backorder_confirmationdto)));
    }

    @PreAuthorize("hasPermission(this.stock_backorder_confirmationMapping.toDomain(#stock_backorder_confirmationdtos),'iBizBusinessCentral-Stock_backorder_confirmation-Save')")
    @ApiOperation(value = "批量保存欠单确认", tags = {"欠单确认" },  notes = "批量保存欠单确认")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_backorder_confirmations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_backorder_confirmationDTO> stock_backorder_confirmationdtos) {
        stock_backorder_confirmationService.saveBatch(stock_backorder_confirmationMapping.toDomain(stock_backorder_confirmationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_backorder_confirmation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_backorder_confirmation-Get')")
	@ApiOperation(value = "获取数据集", tags = {"欠单确认" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_backorder_confirmations/fetchdefault")
	public ResponseEntity<List<Stock_backorder_confirmationDTO>> fetchDefault(Stock_backorder_confirmationSearchContext context) {
        Page<Stock_backorder_confirmation> domains = stock_backorder_confirmationService.searchDefault(context) ;
        List<Stock_backorder_confirmationDTO> list = stock_backorder_confirmationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_backorder_confirmation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_backorder_confirmation-Get')")
	@ApiOperation(value = "查询数据集", tags = {"欠单确认" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_backorder_confirmations/searchdefault")
	public ResponseEntity<Page<Stock_backorder_confirmationDTO>> searchDefault(@RequestBody Stock_backorder_confirmationSearchContext context) {
        Page<Stock_backorder_confirmation> domains = stock_backorder_confirmationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_backorder_confirmationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


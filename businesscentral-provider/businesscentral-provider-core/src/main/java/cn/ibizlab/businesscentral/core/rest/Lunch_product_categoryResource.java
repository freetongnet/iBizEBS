package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_product_categoryService;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_product_categorySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"工作餐产品类别" })
@RestController("Core-lunch_product_category")
@RequestMapping("")
public class Lunch_product_categoryResource {

    @Autowired
    public ILunch_product_categoryService lunch_product_categoryService;

    @Autowired
    @Lazy
    public Lunch_product_categoryMapping lunch_product_categoryMapping;

    @PreAuthorize("hasPermission(this.lunch_product_categoryMapping.toDomain(#lunch_product_categorydto),'iBizBusinessCentral-Lunch_product_category-Create')")
    @ApiOperation(value = "新建工作餐产品类别", tags = {"工作餐产品类别" },  notes = "新建工作餐产品类别")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories")
    public ResponseEntity<Lunch_product_categoryDTO> create(@Validated @RequestBody Lunch_product_categoryDTO lunch_product_categorydto) {
        Lunch_product_category domain = lunch_product_categoryMapping.toDomain(lunch_product_categorydto);
		lunch_product_categoryService.create(domain);
        Lunch_product_categoryDTO dto = lunch_product_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_product_categoryMapping.toDomain(#lunch_product_categorydtos),'iBizBusinessCentral-Lunch_product_category-Create')")
    @ApiOperation(value = "批量新建工作餐产品类别", tags = {"工作餐产品类别" },  notes = "批量新建工作餐产品类别")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_product_categoryDTO> lunch_product_categorydtos) {
        lunch_product_categoryService.createBatch(lunch_product_categoryMapping.toDomain(lunch_product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lunch_product_category" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.lunch_product_categoryService.get(#lunch_product_category_id),'iBizBusinessCentral-Lunch_product_category-Update')")
    @ApiOperation(value = "更新工作餐产品类别", tags = {"工作餐产品类别" },  notes = "更新工作餐产品类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_product_categories/{lunch_product_category_id}")
    public ResponseEntity<Lunch_product_categoryDTO> update(@PathVariable("lunch_product_category_id") Long lunch_product_category_id, @RequestBody Lunch_product_categoryDTO lunch_product_categorydto) {
		Lunch_product_category domain  = lunch_product_categoryMapping.toDomain(lunch_product_categorydto);
        domain .setId(lunch_product_category_id);
		lunch_product_categoryService.update(domain );
		Lunch_product_categoryDTO dto = lunch_product_categoryMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_product_categoryService.getLunchProductCategoryByEntities(this.lunch_product_categoryMapping.toDomain(#lunch_product_categorydtos)),'iBizBusinessCentral-Lunch_product_category-Update')")
    @ApiOperation(value = "批量更新工作餐产品类别", tags = {"工作餐产品类别" },  notes = "批量更新工作餐产品类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_product_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_product_categoryDTO> lunch_product_categorydtos) {
        lunch_product_categoryService.updateBatch(lunch_product_categoryMapping.toDomain(lunch_product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.lunch_product_categoryService.get(#lunch_product_category_id),'iBizBusinessCentral-Lunch_product_category-Remove')")
    @ApiOperation(value = "删除工作餐产品类别", tags = {"工作餐产品类别" },  notes = "删除工作餐产品类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_product_categories/{lunch_product_category_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("lunch_product_category_id") Long lunch_product_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_product_categoryService.remove(lunch_product_category_id));
    }

    @PreAuthorize("hasPermission(this.lunch_product_categoryService.getLunchProductCategoryByIds(#ids),'iBizBusinessCentral-Lunch_product_category-Remove')")
    @ApiOperation(value = "批量删除工作餐产品类别", tags = {"工作餐产品类别" },  notes = "批量删除工作餐产品类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_product_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        lunch_product_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.lunch_product_categoryMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lunch_product_category-Get')")
    @ApiOperation(value = "获取工作餐产品类别", tags = {"工作餐产品类别" },  notes = "获取工作餐产品类别")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_product_categories/{lunch_product_category_id}")
    public ResponseEntity<Lunch_product_categoryDTO> get(@PathVariable("lunch_product_category_id") Long lunch_product_category_id) {
        Lunch_product_category domain = lunch_product_categoryService.get(lunch_product_category_id);
        Lunch_product_categoryDTO dto = lunch_product_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取工作餐产品类别草稿", tags = {"工作餐产品类别" },  notes = "获取工作餐产品类别草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_product_categories/getdraft")
    public ResponseEntity<Lunch_product_categoryDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_product_categoryMapping.toDto(lunch_product_categoryService.getDraft(new Lunch_product_category())));
    }

    @ApiOperation(value = "检查工作餐产品类别", tags = {"工作餐产品类别" },  notes = "检查工作餐产品类别")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Lunch_product_categoryDTO lunch_product_categorydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(lunch_product_categoryService.checkKey(lunch_product_categoryMapping.toDomain(lunch_product_categorydto)));
    }

    @PreAuthorize("hasPermission(this.lunch_product_categoryMapping.toDomain(#lunch_product_categorydto),'iBizBusinessCentral-Lunch_product_category-Save')")
    @ApiOperation(value = "保存工作餐产品类别", tags = {"工作餐产品类别" },  notes = "保存工作餐产品类别")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories/save")
    public ResponseEntity<Boolean> save(@RequestBody Lunch_product_categoryDTO lunch_product_categorydto) {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_product_categoryService.save(lunch_product_categoryMapping.toDomain(lunch_product_categorydto)));
    }

    @PreAuthorize("hasPermission(this.lunch_product_categoryMapping.toDomain(#lunch_product_categorydtos),'iBizBusinessCentral-Lunch_product_category-Save')")
    @ApiOperation(value = "批量保存工作餐产品类别", tags = {"工作餐产品类别" },  notes = "批量保存工作餐产品类别")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_product_categories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Lunch_product_categoryDTO> lunch_product_categorydtos) {
        lunch_product_categoryService.saveBatch(lunch_product_categoryMapping.toDomain(lunch_product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_product_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_product_category-Get')")
	@ApiOperation(value = "获取数据集", tags = {"工作餐产品类别" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_product_categories/fetchdefault")
	public ResponseEntity<List<Lunch_product_categoryDTO>> fetchDefault(Lunch_product_categorySearchContext context) {
        Page<Lunch_product_category> domains = lunch_product_categoryService.searchDefault(context) ;
        List<Lunch_product_categoryDTO> list = lunch_product_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_product_category-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_product_category-Get')")
	@ApiOperation(value = "查询数据集", tags = {"工作餐产品类别" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/lunch_product_categories/searchdefault")
	public ResponseEntity<Page<Lunch_product_categoryDTO>> searchDefault(@RequestBody Lunch_product_categorySearchContext context) {
        Page<Lunch_product_category> domains = lunch_product_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_product_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.businesscentral.core.dto.Purchase_order_lineDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CorePurchase_order_lineMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Purchase_order_lineMapping extends MappingBase<Purchase_order_lineDTO, Purchase_order_line> {


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_print_journal;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_print_journalService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_print_journalSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"会计打印日记账" })
@RestController("Core-account_print_journal")
@RequestMapping("")
public class Account_print_journalResource {

    @Autowired
    public IAccount_print_journalService account_print_journalService;

    @Autowired
    @Lazy
    public Account_print_journalMapping account_print_journalMapping;

    @PreAuthorize("hasPermission(this.account_print_journalMapping.toDomain(#account_print_journaldto),'iBizBusinessCentral-Account_print_journal-Create')")
    @ApiOperation(value = "新建会计打印日记账", tags = {"会计打印日记账" },  notes = "新建会计打印日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_print_journals")
    public ResponseEntity<Account_print_journalDTO> create(@Validated @RequestBody Account_print_journalDTO account_print_journaldto) {
        Account_print_journal domain = account_print_journalMapping.toDomain(account_print_journaldto);
		account_print_journalService.create(domain);
        Account_print_journalDTO dto = account_print_journalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_print_journalMapping.toDomain(#account_print_journaldtos),'iBizBusinessCentral-Account_print_journal-Create')")
    @ApiOperation(value = "批量新建会计打印日记账", tags = {"会计打印日记账" },  notes = "批量新建会计打印日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_print_journals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_print_journalDTO> account_print_journaldtos) {
        account_print_journalService.createBatch(account_print_journalMapping.toDomain(account_print_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_print_journal" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_print_journalService.get(#account_print_journal_id),'iBizBusinessCentral-Account_print_journal-Update')")
    @ApiOperation(value = "更新会计打印日记账", tags = {"会计打印日记账" },  notes = "更新会计打印日记账")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_print_journals/{account_print_journal_id}")
    public ResponseEntity<Account_print_journalDTO> update(@PathVariable("account_print_journal_id") Long account_print_journal_id, @RequestBody Account_print_journalDTO account_print_journaldto) {
		Account_print_journal domain  = account_print_journalMapping.toDomain(account_print_journaldto);
        domain .setId(account_print_journal_id);
		account_print_journalService.update(domain );
		Account_print_journalDTO dto = account_print_journalMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_print_journalService.getAccountPrintJournalByEntities(this.account_print_journalMapping.toDomain(#account_print_journaldtos)),'iBizBusinessCentral-Account_print_journal-Update')")
    @ApiOperation(value = "批量更新会计打印日记账", tags = {"会计打印日记账" },  notes = "批量更新会计打印日记账")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_print_journals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_print_journalDTO> account_print_journaldtos) {
        account_print_journalService.updateBatch(account_print_journalMapping.toDomain(account_print_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_print_journalService.get(#account_print_journal_id),'iBizBusinessCentral-Account_print_journal-Remove')")
    @ApiOperation(value = "删除会计打印日记账", tags = {"会计打印日记账" },  notes = "删除会计打印日记账")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_print_journals/{account_print_journal_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_print_journal_id") Long account_print_journal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_print_journalService.remove(account_print_journal_id));
    }

    @PreAuthorize("hasPermission(this.account_print_journalService.getAccountPrintJournalByIds(#ids),'iBizBusinessCentral-Account_print_journal-Remove')")
    @ApiOperation(value = "批量删除会计打印日记账", tags = {"会计打印日记账" },  notes = "批量删除会计打印日记账")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_print_journals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_print_journalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_print_journalMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_print_journal-Get')")
    @ApiOperation(value = "获取会计打印日记账", tags = {"会计打印日记账" },  notes = "获取会计打印日记账")
	@RequestMapping(method = RequestMethod.GET, value = "/account_print_journals/{account_print_journal_id}")
    public ResponseEntity<Account_print_journalDTO> get(@PathVariable("account_print_journal_id") Long account_print_journal_id) {
        Account_print_journal domain = account_print_journalService.get(account_print_journal_id);
        Account_print_journalDTO dto = account_print_journalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取会计打印日记账草稿", tags = {"会计打印日记账" },  notes = "获取会计打印日记账草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_print_journals/getdraft")
    public ResponseEntity<Account_print_journalDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_print_journalMapping.toDto(account_print_journalService.getDraft(new Account_print_journal())));
    }

    @ApiOperation(value = "检查会计打印日记账", tags = {"会计打印日记账" },  notes = "检查会计打印日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_print_journals/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_print_journalDTO account_print_journaldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_print_journalService.checkKey(account_print_journalMapping.toDomain(account_print_journaldto)));
    }

    @PreAuthorize("hasPermission(this.account_print_journalMapping.toDomain(#account_print_journaldto),'iBizBusinessCentral-Account_print_journal-Save')")
    @ApiOperation(value = "保存会计打印日记账", tags = {"会计打印日记账" },  notes = "保存会计打印日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_print_journals/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_print_journalDTO account_print_journaldto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_print_journalService.save(account_print_journalMapping.toDomain(account_print_journaldto)));
    }

    @PreAuthorize("hasPermission(this.account_print_journalMapping.toDomain(#account_print_journaldtos),'iBizBusinessCentral-Account_print_journal-Save')")
    @ApiOperation(value = "批量保存会计打印日记账", tags = {"会计打印日记账" },  notes = "批量保存会计打印日记账")
	@RequestMapping(method = RequestMethod.POST, value = "/account_print_journals/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_print_journalDTO> account_print_journaldtos) {
        account_print_journalService.saveBatch(account_print_journalMapping.toDomain(account_print_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_print_journal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_print_journal-Get')")
	@ApiOperation(value = "获取数据集", tags = {"会计打印日记账" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_print_journals/fetchdefault")
	public ResponseEntity<List<Account_print_journalDTO>> fetchDefault(Account_print_journalSearchContext context) {
        Page<Account_print_journal> domains = account_print_journalService.searchDefault(context) ;
        List<Account_print_journalDTO> list = account_print_journalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_print_journal-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_print_journal-Get')")
	@ApiOperation(value = "查询数据集", tags = {"会计打印日记账" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_print_journals/searchdefault")
	public ResponseEntity<Page<Account_print_journalDTO>> searchDefault(@RequestBody Account_print_journalSearchContext context) {
        Page<Account_print_journal> domains = account_print_journalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_print_journalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


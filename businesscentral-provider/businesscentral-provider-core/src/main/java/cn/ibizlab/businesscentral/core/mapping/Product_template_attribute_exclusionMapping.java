package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.businesscentral.core.dto.Product_template_attribute_exclusionDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreProduct_template_attribute_exclusionMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Product_template_attribute_exclusionMapping extends MappingBase<Product_template_attribute_exclusionDTO, Product_template_attribute_exclusion> {


}


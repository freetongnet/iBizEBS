package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_alarmService;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_alarmSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动提醒" })
@RestController("Core-calendar_alarm")
@RequestMapping("")
public class Calendar_alarmResource {

    @Autowired
    public ICalendar_alarmService calendar_alarmService;

    @Autowired
    @Lazy
    public Calendar_alarmMapping calendar_alarmMapping;

    @PreAuthorize("hasPermission(this.calendar_alarmMapping.toDomain(#calendar_alarmdto),'iBizBusinessCentral-Calendar_alarm-Create')")
    @ApiOperation(value = "新建活动提醒", tags = {"活动提醒" },  notes = "新建活动提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms")
    public ResponseEntity<Calendar_alarmDTO> create(@Validated @RequestBody Calendar_alarmDTO calendar_alarmdto) {
        Calendar_alarm domain = calendar_alarmMapping.toDomain(calendar_alarmdto);
		calendar_alarmService.create(domain);
        Calendar_alarmDTO dto = calendar_alarmMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_alarmMapping.toDomain(#calendar_alarmdtos),'iBizBusinessCentral-Calendar_alarm-Create')")
    @ApiOperation(value = "批量新建活动提醒", tags = {"活动提醒" },  notes = "批量新建活动提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_alarmDTO> calendar_alarmdtos) {
        calendar_alarmService.createBatch(calendar_alarmMapping.toDomain(calendar_alarmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "calendar_alarm" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.calendar_alarmService.get(#calendar_alarm_id),'iBizBusinessCentral-Calendar_alarm-Update')")
    @ApiOperation(value = "更新活动提醒", tags = {"活动提醒" },  notes = "更新活动提醒")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarms/{calendar_alarm_id}")
    public ResponseEntity<Calendar_alarmDTO> update(@PathVariable("calendar_alarm_id") Long calendar_alarm_id, @RequestBody Calendar_alarmDTO calendar_alarmdto) {
		Calendar_alarm domain  = calendar_alarmMapping.toDomain(calendar_alarmdto);
        domain .setId(calendar_alarm_id);
		calendar_alarmService.update(domain );
		Calendar_alarmDTO dto = calendar_alarmMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.calendar_alarmService.getCalendarAlarmByEntities(this.calendar_alarmMapping.toDomain(#calendar_alarmdtos)),'iBizBusinessCentral-Calendar_alarm-Update')")
    @ApiOperation(value = "批量更新活动提醒", tags = {"活动提醒" },  notes = "批量更新活动提醒")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_alarmDTO> calendar_alarmdtos) {
        calendar_alarmService.updateBatch(calendar_alarmMapping.toDomain(calendar_alarmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.calendar_alarmService.get(#calendar_alarm_id),'iBizBusinessCentral-Calendar_alarm-Remove')")
    @ApiOperation(value = "删除活动提醒", tags = {"活动提醒" },  notes = "删除活动提醒")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarms/{calendar_alarm_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("calendar_alarm_id") Long calendar_alarm_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_alarmService.remove(calendar_alarm_id));
    }

    @PreAuthorize("hasPermission(this.calendar_alarmService.getCalendarAlarmByIds(#ids),'iBizBusinessCentral-Calendar_alarm-Remove')")
    @ApiOperation(value = "批量删除活动提醒", tags = {"活动提醒" },  notes = "批量删除活动提醒")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        calendar_alarmService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.calendar_alarmMapping.toDomain(returnObject.body),'iBizBusinessCentral-Calendar_alarm-Get')")
    @ApiOperation(value = "获取活动提醒", tags = {"活动提醒" },  notes = "获取活动提醒")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_alarms/{calendar_alarm_id}")
    public ResponseEntity<Calendar_alarmDTO> get(@PathVariable("calendar_alarm_id") Long calendar_alarm_id) {
        Calendar_alarm domain = calendar_alarmService.get(calendar_alarm_id);
        Calendar_alarmDTO dto = calendar_alarmMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动提醒草稿", tags = {"活动提醒" },  notes = "获取活动提醒草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_alarms/getdraft")
    public ResponseEntity<Calendar_alarmDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_alarmMapping.toDto(calendar_alarmService.getDraft(new Calendar_alarm())));
    }

    @ApiOperation(value = "检查活动提醒", tags = {"活动提醒" },  notes = "检查活动提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Calendar_alarmDTO calendar_alarmdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(calendar_alarmService.checkKey(calendar_alarmMapping.toDomain(calendar_alarmdto)));
    }

    @PreAuthorize("hasPermission(this.calendar_alarmMapping.toDomain(#calendar_alarmdto),'iBizBusinessCentral-Calendar_alarm-Save')")
    @ApiOperation(value = "保存活动提醒", tags = {"活动提醒" },  notes = "保存活动提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/save")
    public ResponseEntity<Boolean> save(@RequestBody Calendar_alarmDTO calendar_alarmdto) {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_alarmService.save(calendar_alarmMapping.toDomain(calendar_alarmdto)));
    }

    @PreAuthorize("hasPermission(this.calendar_alarmMapping.toDomain(#calendar_alarmdtos),'iBizBusinessCentral-Calendar_alarm-Save')")
    @ApiOperation(value = "批量保存活动提醒", tags = {"活动提醒" },  notes = "批量保存活动提醒")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Calendar_alarmDTO> calendar_alarmdtos) {
        calendar_alarmService.saveBatch(calendar_alarmMapping.toDomain(calendar_alarmdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_alarm-Get')")
	@ApiOperation(value = "获取数据集", tags = {"活动提醒" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_alarms/fetchdefault")
	public ResponseEntity<List<Calendar_alarmDTO>> fetchDefault(Calendar_alarmSearchContext context) {
        Page<Calendar_alarm> domains = calendar_alarmService.searchDefault(context) ;
        List<Calendar_alarmDTO> list = calendar_alarmMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Calendar_alarm-Get')")
	@ApiOperation(value = "查询数据集", tags = {"活动提醒" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/calendar_alarms/searchdefault")
	public ResponseEntity<Page<Calendar_alarmDTO>> searchDefault(@RequestBody Calendar_alarmSearchContext context) {
        Page<Calendar_alarm> domains = calendar_alarmService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_alarmMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.businesscentral.core.dto.Mrp_product_produceDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreMrp_product_produceMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mrp_product_produceMapping extends MappingBase<Mrp_product_produceDTO, Mrp_product_produce> {


}


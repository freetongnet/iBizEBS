package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_orderSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"销售订单" })
@RestController("Core-sale_order")
@RequestMapping("")
public class Sale_orderResource {

    @Autowired
    public ISale_orderService sale_orderService;

    @Autowired
    @Lazy
    public Sale_orderMapping sale_orderMapping;

    @PreAuthorize("hasPermission(this.sale_orderMapping.toDomain(#sale_orderdto),'iBizBusinessCentral-Sale_order-Create')")
    @ApiOperation(value = "新建销售订单", tags = {"销售订单" },  notes = "新建销售订单")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_orders")
    public ResponseEntity<Sale_orderDTO> create(@Validated @RequestBody Sale_orderDTO sale_orderdto) {
        Sale_order domain = sale_orderMapping.toDomain(sale_orderdto);
		sale_orderService.create(domain);
        Sale_orderDTO dto = sale_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_orderMapping.toDomain(#sale_orderdtos),'iBizBusinessCentral-Sale_order-Create')")
    @ApiOperation(value = "批量新建销售订单", tags = {"销售订单" },  notes = "批量新建销售订单")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_orderDTO> sale_orderdtos) {
        sale_orderService.createBatch(sale_orderMapping.toDomain(sale_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sale_order" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.sale_orderService.get(#sale_order_id),'iBizBusinessCentral-Sale_order-Update')")
    @ApiOperation(value = "更新销售订单", tags = {"销售订单" },  notes = "更新销售订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_orders/{sale_order_id}")
    public ResponseEntity<Sale_orderDTO> update(@PathVariable("sale_order_id") Long sale_order_id, @RequestBody Sale_orderDTO sale_orderdto) {
		Sale_order domain  = sale_orderMapping.toDomain(sale_orderdto);
        domain .setId(sale_order_id);
		sale_orderService.update(domain );
		Sale_orderDTO dto = sale_orderMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sale_orderService.getSaleOrderByEntities(this.sale_orderMapping.toDomain(#sale_orderdtos)),'iBizBusinessCentral-Sale_order-Update')")
    @ApiOperation(value = "批量更新销售订单", tags = {"销售订单" },  notes = "批量更新销售订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_orderDTO> sale_orderdtos) {
        sale_orderService.updateBatch(sale_orderMapping.toDomain(sale_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sale_orderService.get(#sale_order_id),'iBizBusinessCentral-Sale_order-Remove')")
    @ApiOperation(value = "删除销售订单", tags = {"销售订单" },  notes = "删除销售订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_orders/{sale_order_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_id") Long sale_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_orderService.remove(sale_order_id));
    }

    @PreAuthorize("hasPermission(this.sale_orderService.getSaleOrderByIds(#ids),'iBizBusinessCentral-Sale_order-Remove')")
    @ApiOperation(value = "批量删除销售订单", tags = {"销售订单" },  notes = "批量删除销售订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sale_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sale_orderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Sale_order-Get')")
    @ApiOperation(value = "获取销售订单", tags = {"销售订单" },  notes = "获取销售订单")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_orders/{sale_order_id}")
    public ResponseEntity<Sale_orderDTO> get(@PathVariable("sale_order_id") Long sale_order_id) {
        Sale_order domain = sale_orderService.get(sale_order_id);
        Sale_orderDTO dto = sale_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取销售订单草稿", tags = {"销售订单" },  notes = "获取销售订单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_orders/getdraft")
    public ResponseEntity<Sale_orderDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sale_orderMapping.toDto(sale_orderService.getDraft(new Sale_order())));
    }

    @ApiOperation(value = "检查销售订单", tags = {"销售订单" },  notes = "检查销售订单")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_orders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sale_orderDTO sale_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sale_orderService.checkKey(sale_orderMapping.toDomain(sale_orderdto)));
    }

    @PreAuthorize("hasPermission(this.sale_orderMapping.toDomain(#sale_orderdto),'iBizBusinessCentral-Sale_order-Save')")
    @ApiOperation(value = "保存销售订单", tags = {"销售订单" },  notes = "保存销售订单")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_orders/save")
    public ResponseEntity<Boolean> save(@RequestBody Sale_orderDTO sale_orderdto) {
        return ResponseEntity.status(HttpStatus.OK).body(sale_orderService.save(sale_orderMapping.toDomain(sale_orderdto)));
    }

    @PreAuthorize("hasPermission(this.sale_orderMapping.toDomain(#sale_orderdtos),'iBizBusinessCentral-Sale_order-Save')")
    @ApiOperation(value = "批量保存销售订单", tags = {"销售订单" },  notes = "批量保存销售订单")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_orders/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sale_orderDTO> sale_orderdtos) {
        sale_orderService.saveBatch(sale_orderMapping.toDomain(sale_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order-Get')")
	@ApiOperation(value = "获取数据集", tags = {"销售订单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sale_orders/fetchdefault")
	public ResponseEntity<List<Sale_orderDTO>> fetchDefault(Sale_orderSearchContext context) {
        Page<Sale_order> domains = sale_orderService.searchDefault(context) ;
        List<Sale_orderDTO> list = sale_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sale_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sale_order-Get')")
	@ApiOperation(value = "查询数据集", tags = {"销售订单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sale_orders/searchdefault")
	public ResponseEntity<Page<Sale_orderDTO>> searchDefault(@RequestBody Sale_orderSearchContext context) {
        Page<Sale_order> domains = sale_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


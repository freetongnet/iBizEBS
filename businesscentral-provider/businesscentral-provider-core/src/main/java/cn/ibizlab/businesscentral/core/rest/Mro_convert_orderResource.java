package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_convert_orderService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_convert_orderSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Convert Order to Task" })
@RestController("Core-mro_convert_order")
@RequestMapping("")
public class Mro_convert_orderResource {

    @Autowired
    public IMro_convert_orderService mro_convert_orderService;

    @Autowired
    @Lazy
    public Mro_convert_orderMapping mro_convert_orderMapping;

    @PreAuthorize("hasPermission(this.mro_convert_orderMapping.toDomain(#mro_convert_orderdto),'iBizBusinessCentral-Mro_convert_order-Create')")
    @ApiOperation(value = "新建Convert Order to Task", tags = {"Convert Order to Task" },  notes = "新建Convert Order to Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders")
    public ResponseEntity<Mro_convert_orderDTO> create(@Validated @RequestBody Mro_convert_orderDTO mro_convert_orderdto) {
        Mro_convert_order domain = mro_convert_orderMapping.toDomain(mro_convert_orderdto);
		mro_convert_orderService.create(domain);
        Mro_convert_orderDTO dto = mro_convert_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_convert_orderMapping.toDomain(#mro_convert_orderdtos),'iBizBusinessCentral-Mro_convert_order-Create')")
    @ApiOperation(value = "批量新建Convert Order to Task", tags = {"Convert Order to Task" },  notes = "批量新建Convert Order to Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_convert_orderDTO> mro_convert_orderdtos) {
        mro_convert_orderService.createBatch(mro_convert_orderMapping.toDomain(mro_convert_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_convert_order" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_convert_orderService.get(#mro_convert_order_id),'iBizBusinessCentral-Mro_convert_order-Update')")
    @ApiOperation(value = "更新Convert Order to Task", tags = {"Convert Order to Task" },  notes = "更新Convert Order to Task")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_convert_orders/{mro_convert_order_id}")
    public ResponseEntity<Mro_convert_orderDTO> update(@PathVariable("mro_convert_order_id") Long mro_convert_order_id, @RequestBody Mro_convert_orderDTO mro_convert_orderdto) {
		Mro_convert_order domain  = mro_convert_orderMapping.toDomain(mro_convert_orderdto);
        domain .setId(mro_convert_order_id);
		mro_convert_orderService.update(domain );
		Mro_convert_orderDTO dto = mro_convert_orderMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_convert_orderService.getMroConvertOrderByEntities(this.mro_convert_orderMapping.toDomain(#mro_convert_orderdtos)),'iBizBusinessCentral-Mro_convert_order-Update')")
    @ApiOperation(value = "批量更新Convert Order to Task", tags = {"Convert Order to Task" },  notes = "批量更新Convert Order to Task")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_convert_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_convert_orderDTO> mro_convert_orderdtos) {
        mro_convert_orderService.updateBatch(mro_convert_orderMapping.toDomain(mro_convert_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_convert_orderService.get(#mro_convert_order_id),'iBizBusinessCentral-Mro_convert_order-Remove')")
    @ApiOperation(value = "删除Convert Order to Task", tags = {"Convert Order to Task" },  notes = "删除Convert Order to Task")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_convert_orders/{mro_convert_order_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_convert_order_id") Long mro_convert_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_convert_orderService.remove(mro_convert_order_id));
    }

    @PreAuthorize("hasPermission(this.mro_convert_orderService.getMroConvertOrderByIds(#ids),'iBizBusinessCentral-Mro_convert_order-Remove')")
    @ApiOperation(value = "批量删除Convert Order to Task", tags = {"Convert Order to Task" },  notes = "批量删除Convert Order to Task")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_convert_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_convert_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_convert_orderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_convert_order-Get')")
    @ApiOperation(value = "获取Convert Order to Task", tags = {"Convert Order to Task" },  notes = "获取Convert Order to Task")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_convert_orders/{mro_convert_order_id}")
    public ResponseEntity<Mro_convert_orderDTO> get(@PathVariable("mro_convert_order_id") Long mro_convert_order_id) {
        Mro_convert_order domain = mro_convert_orderService.get(mro_convert_order_id);
        Mro_convert_orderDTO dto = mro_convert_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Convert Order to Task草稿", tags = {"Convert Order to Task" },  notes = "获取Convert Order to Task草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_convert_orders/getdraft")
    public ResponseEntity<Mro_convert_orderDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_convert_orderMapping.toDto(mro_convert_orderService.getDraft(new Mro_convert_order())));
    }

    @ApiOperation(value = "检查Convert Order to Task", tags = {"Convert Order to Task" },  notes = "检查Convert Order to Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_convert_orderDTO mro_convert_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_convert_orderService.checkKey(mro_convert_orderMapping.toDomain(mro_convert_orderdto)));
    }

    @PreAuthorize("hasPermission(this.mro_convert_orderMapping.toDomain(#mro_convert_orderdto),'iBizBusinessCentral-Mro_convert_order-Save')")
    @ApiOperation(value = "保存Convert Order to Task", tags = {"Convert Order to Task" },  notes = "保存Convert Order to Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_convert_orderDTO mro_convert_orderdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_convert_orderService.save(mro_convert_orderMapping.toDomain(mro_convert_orderdto)));
    }

    @PreAuthorize("hasPermission(this.mro_convert_orderMapping.toDomain(#mro_convert_orderdtos),'iBizBusinessCentral-Mro_convert_order-Save')")
    @ApiOperation(value = "批量保存Convert Order to Task", tags = {"Convert Order to Task" },  notes = "批量保存Convert Order to Task")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_convert_orders/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_convert_orderDTO> mro_convert_orderdtos) {
        mro_convert_orderService.saveBatch(mro_convert_orderMapping.toDomain(mro_convert_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_convert_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_convert_order-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Convert Order to Task" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_convert_orders/fetchdefault")
	public ResponseEntity<List<Mro_convert_orderDTO>> fetchDefault(Mro_convert_orderSearchContext context) {
        Page<Mro_convert_order> domains = mro_convert_orderService.searchDefault(context) ;
        List<Mro_convert_orderDTO> list = mro_convert_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_convert_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_convert_order-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Convert Order to Task" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_convert_orders/searchdefault")
	public ResponseEntity<Page<Mro_convert_orderDTO>> searchDefault(@RequestBody Mro_convert_orderSearchContext context) {
        Page<Mro_convert_order> domains = mro_convert_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_convert_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


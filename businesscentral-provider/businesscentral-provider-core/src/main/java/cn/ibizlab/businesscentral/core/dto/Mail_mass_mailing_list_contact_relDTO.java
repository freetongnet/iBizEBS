package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Mail_mass_mailing_list_contact_relDTO]
 */
@Data
public class Mail_mass_mailing_list_contact_relDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [UNSUBSCRIPTION_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "unsubscription_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("unsubscription_date")
    private Timestamp unsubscriptionDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [OPT_OUT]
     *
     */
    @JSONField(name = "opt_out")
    @JsonProperty("opt_out")
    private Boolean optOut;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE_BOUNCE]
     *
     */
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;

    /**
     * 属性 [IS_BLACKLISTED]
     *
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private Boolean isBlacklisted;

    /**
     * 属性 [LIST_ID_TEXT]
     *
     */
    @JSONField(name = "list_id_text")
    @JsonProperty("list_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String listIdText;

    /**
     * 属性 [CONTACT_ID_TEXT]
     *
     */
    @JSONField(name = "contact_id_text")
    @JsonProperty("contact_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String contactIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String createUidText;

    /**
     * 属性 [CONTACT_COUNT]
     *
     */
    @JSONField(name = "contact_count")
    @JsonProperty("contact_count")
    private Integer contactCount;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;

    /**
     * 属性 [CONTACT_ID]
     *
     */
    @JSONField(name = "contact_id")
    @JsonProperty("contact_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[联系]不允许为空!")
    private Long contactId;

    /**
     * 属性 [LIST_ID]
     *
     */
    @JSONField(name = "list_id")
    @JsonProperty("list_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[邮件列表]不允许为空!")
    private Long listId;


    /**
     * 设置 [UNSUBSCRIPTION_DATE]
     */
    public void setUnsubscriptionDate(Timestamp  unsubscriptionDate){
        this.unsubscriptionDate = unsubscriptionDate ;
        this.modify("unsubscription_date",unsubscriptionDate);
    }

    /**
     * 设置 [OPT_OUT]
     */
    public void setOptOut(Boolean  optOut){
        this.optOut = optOut ;
        this.modify("opt_out",optOut);
    }

    /**
     * 设置 [CONTACT_ID]
     */
    public void setContactId(Long  contactId){
        this.contactId = contactId ;
        this.modify("contact_id",contactId);
    }

    /**
     * 设置 [LIST_ID]
     */
    public void setListId(Long  listId){
        this.listId = listId ;
        this.modify("list_id",listId);
    }


}



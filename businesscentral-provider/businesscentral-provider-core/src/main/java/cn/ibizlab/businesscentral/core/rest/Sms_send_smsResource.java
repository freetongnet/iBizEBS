package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.businesscentral.core.odoo_sms.service.ISms_send_smsService;
import cn.ibizlab.businesscentral.core.odoo_sms.filter.Sms_send_smsSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"发送短信" })
@RestController("Core-sms_send_sms")
@RequestMapping("")
public class Sms_send_smsResource {

    @Autowired
    public ISms_send_smsService sms_send_smsService;

    @Autowired
    @Lazy
    public Sms_send_smsMapping sms_send_smsMapping;

    @PreAuthorize("hasPermission(this.sms_send_smsMapping.toDomain(#sms_send_smsdto),'iBizBusinessCentral-Sms_send_sms-Create')")
    @ApiOperation(value = "新建发送短信", tags = {"发送短信" },  notes = "新建发送短信")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms")
    public ResponseEntity<Sms_send_smsDTO> create(@Validated @RequestBody Sms_send_smsDTO sms_send_smsdto) {
        Sms_send_sms domain = sms_send_smsMapping.toDomain(sms_send_smsdto);
		sms_send_smsService.create(domain);
        Sms_send_smsDTO dto = sms_send_smsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sms_send_smsMapping.toDomain(#sms_send_smsdtos),'iBizBusinessCentral-Sms_send_sms-Create')")
    @ApiOperation(value = "批量新建发送短信", tags = {"发送短信" },  notes = "批量新建发送短信")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sms_send_smsDTO> sms_send_smsdtos) {
        sms_send_smsService.createBatch(sms_send_smsMapping.toDomain(sms_send_smsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "sms_send_sms" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.sms_send_smsService.get(#sms_send_sms_id),'iBizBusinessCentral-Sms_send_sms-Update')")
    @ApiOperation(value = "更新发送短信", tags = {"发送短信" },  notes = "更新发送短信")
	@RequestMapping(method = RequestMethod.PUT, value = "/sms_send_sms/{sms_send_sms_id}")
    public ResponseEntity<Sms_send_smsDTO> update(@PathVariable("sms_send_sms_id") Long sms_send_sms_id, @RequestBody Sms_send_smsDTO sms_send_smsdto) {
		Sms_send_sms domain  = sms_send_smsMapping.toDomain(sms_send_smsdto);
        domain .setId(sms_send_sms_id);
		sms_send_smsService.update(domain );
		Sms_send_smsDTO dto = sms_send_smsMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.sms_send_smsService.getSmsSendSmsByEntities(this.sms_send_smsMapping.toDomain(#sms_send_smsdtos)),'iBizBusinessCentral-Sms_send_sms-Update')")
    @ApiOperation(value = "批量更新发送短信", tags = {"发送短信" },  notes = "批量更新发送短信")
	@RequestMapping(method = RequestMethod.PUT, value = "/sms_send_sms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sms_send_smsDTO> sms_send_smsdtos) {
        sms_send_smsService.updateBatch(sms_send_smsMapping.toDomain(sms_send_smsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.sms_send_smsService.get(#sms_send_sms_id),'iBizBusinessCentral-Sms_send_sms-Remove')")
    @ApiOperation(value = "删除发送短信", tags = {"发送短信" },  notes = "删除发送短信")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sms_send_sms/{sms_send_sms_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("sms_send_sms_id") Long sms_send_sms_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sms_send_smsService.remove(sms_send_sms_id));
    }

    @PreAuthorize("hasPermission(this.sms_send_smsService.getSmsSendSmsByIds(#ids),'iBizBusinessCentral-Sms_send_sms-Remove')")
    @ApiOperation(value = "批量删除发送短信", tags = {"发送短信" },  notes = "批量删除发送短信")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sms_send_sms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        sms_send_smsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.sms_send_smsMapping.toDomain(returnObject.body),'iBizBusinessCentral-Sms_send_sms-Get')")
    @ApiOperation(value = "获取发送短信", tags = {"发送短信" },  notes = "获取发送短信")
	@RequestMapping(method = RequestMethod.GET, value = "/sms_send_sms/{sms_send_sms_id}")
    public ResponseEntity<Sms_send_smsDTO> get(@PathVariable("sms_send_sms_id") Long sms_send_sms_id) {
        Sms_send_sms domain = sms_send_smsService.get(sms_send_sms_id);
        Sms_send_smsDTO dto = sms_send_smsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取发送短信草稿", tags = {"发送短信" },  notes = "获取发送短信草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/sms_send_sms/getdraft")
    public ResponseEntity<Sms_send_smsDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(sms_send_smsMapping.toDto(sms_send_smsService.getDraft(new Sms_send_sms())));
    }

    @ApiOperation(value = "检查发送短信", tags = {"发送短信" },  notes = "检查发送短信")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Sms_send_smsDTO sms_send_smsdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(sms_send_smsService.checkKey(sms_send_smsMapping.toDomain(sms_send_smsdto)));
    }

    @PreAuthorize("hasPermission(this.sms_send_smsMapping.toDomain(#sms_send_smsdto),'iBizBusinessCentral-Sms_send_sms-Save')")
    @ApiOperation(value = "保存发送短信", tags = {"发送短信" },  notes = "保存发送短信")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms/save")
    public ResponseEntity<Boolean> save(@RequestBody Sms_send_smsDTO sms_send_smsdto) {
        return ResponseEntity.status(HttpStatus.OK).body(sms_send_smsService.save(sms_send_smsMapping.toDomain(sms_send_smsdto)));
    }

    @PreAuthorize("hasPermission(this.sms_send_smsMapping.toDomain(#sms_send_smsdtos),'iBizBusinessCentral-Sms_send_sms-Save')")
    @ApiOperation(value = "批量保存发送短信", tags = {"发送短信" },  notes = "批量保存发送短信")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Sms_send_smsDTO> sms_send_smsdtos) {
        sms_send_smsService.saveBatch(sms_send_smsMapping.toDomain(sms_send_smsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_send_sms-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sms_send_sms-Get')")
	@ApiOperation(value = "获取数据集", tags = {"发送短信" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/sms_send_sms/fetchdefault")
	public ResponseEntity<List<Sms_send_smsDTO>> fetchDefault(Sms_send_smsSearchContext context) {
        Page<Sms_send_sms> domains = sms_send_smsService.searchDefault(context) ;
        List<Sms_send_smsDTO> list = sms_send_smsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Sms_send_sms-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Sms_send_sms-Get')")
	@ApiOperation(value = "查询数据集", tags = {"发送短信" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/sms_send_sms/searchdefault")
	public ResponseEntity<Page<Sms_send_smsDTO>> searchDefault(@RequestBody Sms_send_smsSearchContext context) {
        Page<Sms_send_sms> domains = sms_send_smsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sms_send_smsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


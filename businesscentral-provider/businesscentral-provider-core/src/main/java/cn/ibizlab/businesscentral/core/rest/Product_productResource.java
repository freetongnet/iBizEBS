package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_productSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品" })
@RestController("Core-product_product")
@RequestMapping("")
public class Product_productResource {

    @Autowired
    public IProduct_productService product_productService;

    @Autowired
    @Lazy
    public Product_productMapping product_productMapping;

    @PreAuthorize("hasPermission(this.product_productMapping.toDomain(#product_productdto),'iBizBusinessCentral-Product_product-Create')")
    @ApiOperation(value = "新建产品", tags = {"产品" },  notes = "新建产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products")
    public ResponseEntity<Product_productDTO> create(@Validated @RequestBody Product_productDTO product_productdto) {
        Product_product domain = product_productMapping.toDomain(product_productdto);
		product_productService.create(domain);
        Product_productDTO dto = product_productMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_productMapping.toDomain(#product_productdtos),'iBizBusinessCentral-Product_product-Create')")
    @ApiOperation(value = "批量新建产品", tags = {"产品" },  notes = "批量新建产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_productDTO> product_productdtos) {
        product_productService.createBatch(product_productMapping.toDomain(product_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_product" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_productService.get(#product_product_id),'iBizBusinessCentral-Product_product-Update')")
    @ApiOperation(value = "更新产品", tags = {"产品" },  notes = "更新产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/{product_product_id}")
    public ResponseEntity<Product_productDTO> update(@PathVariable("product_product_id") Long product_product_id, @RequestBody Product_productDTO product_productdto) {
		Product_product domain  = product_productMapping.toDomain(product_productdto);
        domain .setId(product_product_id);
		product_productService.update(domain );
		Product_productDTO dto = product_productMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_productService.getProductProductByEntities(this.product_productMapping.toDomain(#product_productdtos)),'iBizBusinessCentral-Product_product-Update')")
    @ApiOperation(value = "批量更新产品", tags = {"产品" },  notes = "批量更新产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_products/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_productDTO> product_productdtos) {
        product_productService.updateBatch(product_productMapping.toDomain(product_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_productService.get(#product_product_id),'iBizBusinessCentral-Product_product-Remove')")
    @ApiOperation(value = "删除产品", tags = {"产品" },  notes = "删除产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/{product_product_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_product_id") Long product_product_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_productService.remove(product_product_id));
    }

    @PreAuthorize("hasPermission(this.product_productService.getProductProductByIds(#ids),'iBizBusinessCentral-Product_product-Remove')")
    @ApiOperation(value = "批量删除产品", tags = {"产品" },  notes = "批量删除产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_products/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_productService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_productMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_product-Get')")
    @ApiOperation(value = "获取产品", tags = {"产品" },  notes = "获取产品")
	@RequestMapping(method = RequestMethod.GET, value = "/product_products/{product_product_id}")
    public ResponseEntity<Product_productDTO> get(@PathVariable("product_product_id") Long product_product_id) {
        Product_product domain = product_productService.get(product_product_id);
        Product_productDTO dto = product_productMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品草稿", tags = {"产品" },  notes = "获取产品草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_products/getdraft")
    public ResponseEntity<Product_productDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_productMapping.toDto(product_productService.getDraft(new Product_product())));
    }

    @ApiOperation(value = "检查产品", tags = {"产品" },  notes = "检查产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_productDTO product_productdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_productService.checkKey(product_productMapping.toDomain(product_productdto)));
    }

    @PreAuthorize("hasPermission(this.product_productMapping.toDomain(#product_productdto),'iBizBusinessCentral-Product_product-Save')")
    @ApiOperation(value = "保存产品", tags = {"产品" },  notes = "保存产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_productDTO product_productdto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_productService.save(product_productMapping.toDomain(product_productdto)));
    }

    @PreAuthorize("hasPermission(this.product_productMapping.toDomain(#product_productdtos),'iBizBusinessCentral-Product_product-Save')")
    @ApiOperation(value = "批量保存产品", tags = {"产品" },  notes = "批量保存产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_products/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_productDTO> product_productdtos) {
        product_productService.saveBatch(product_productMapping.toDomain(product_productdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_product-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_product-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_products/fetchdefault")
	public ResponseEntity<List<Product_productDTO>> fetchDefault(Product_productSearchContext context) {
        Page<Product_product> domains = product_productService.searchDefault(context) ;
        List<Product_productDTO> list = product_productMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_product-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_product-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_products/searchdefault")
	public ResponseEntity<Page<Product_productDTO>> searchDefault(@RequestBody Product_productSearchContext context) {
        Page<Product_product> domains = product_productService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_productMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_product-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Product_product-Get')")
	@ApiOperation(value = "获取首选表格", tags = {"产品" } ,notes = "获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/product_products/fetchmaster")
	public ResponseEntity<List<Product_productDTO>> fetchMaster(Product_productSearchContext context) {
        Page<Product_product> domains = product_productService.searchMaster(context) ;
        List<Product_productDTO> list = product_productMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_product-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Product_product-Get')")
	@ApiOperation(value = "查询首选表格", tags = {"产品" } ,notes = "查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/product_products/searchmaster")
	public ResponseEntity<Page<Product_productDTO>> searchMaster(@RequestBody Product_productSearchContext context) {
        Page<Product_product> domains = product_productService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_productMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasPermission(this.product_productMapping.toDomain(#product_productdto),'iBizBusinessCentral-Product_product-Create')")
    @ApiOperation(value = "根据产品模板建立产品", tags = {"产品" },  notes = "根据产品模板建立产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products")
    public ResponseEntity<Product_productDTO> createByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_productDTO product_productdto) {
        Product_product domain = product_productMapping.toDomain(product_productdto);
        domain.setProductTmplId(product_template_id);
		product_productService.create(domain);
        Product_productDTO dto = product_productMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_productMapping.toDomain(#product_productdtos),'iBizBusinessCentral-Product_product-Create')")
    @ApiOperation(value = "根据产品模板批量建立产品", tags = {"产品" },  notes = "根据产品模板批量建立产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/batch")
    public ResponseEntity<Boolean> createBatchByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody List<Product_productDTO> product_productdtos) {
        List<Product_product> domainlist=product_productMapping.toDomain(product_productdtos);
        for(Product_product domain:domainlist){
            domain.setProductTmplId(product_template_id);
        }
        product_productService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_product" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_productService.get(#product_product_id),'iBizBusinessCentral-Product_product-Update')")
    @ApiOperation(value = "根据产品模板更新产品", tags = {"产品" },  notes = "根据产品模板更新产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_products/{product_product_id}")
    public ResponseEntity<Product_productDTO> updateByProduct_template(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id, @RequestBody Product_productDTO product_productdto) {
        Product_product domain = product_productMapping.toDomain(product_productdto);
        domain.setProductTmplId(product_template_id);
        domain.setId(product_product_id);
		product_productService.update(domain);
        Product_productDTO dto = product_productMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_productService.getProductProductByEntities(this.product_productMapping.toDomain(#product_productdtos)),'iBizBusinessCentral-Product_product-Update')")
    @ApiOperation(value = "根据产品模板批量更新产品", tags = {"产品" },  notes = "根据产品模板批量更新产品")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{product_template_id}/product_products/batch")
    public ResponseEntity<Boolean> updateBatchByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody List<Product_productDTO> product_productdtos) {
        List<Product_product> domainlist=product_productMapping.toDomain(product_productdtos);
        for(Product_product domain:domainlist){
            domain.setProductTmplId(product_template_id);
        }
        product_productService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_productService.get(#product_product_id),'iBizBusinessCentral-Product_product-Remove')")
    @ApiOperation(value = "根据产品模板删除产品", tags = {"产品" },  notes = "根据产品模板删除产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_products/{product_product_id}")
    public ResponseEntity<Boolean> removeByProduct_template(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id) {
		return ResponseEntity.status(HttpStatus.OK).body(product_productService.remove(product_product_id));
    }

    @PreAuthorize("hasPermission(this.product_productService.getProductProductByIds(#ids),'iBizBusinessCentral-Product_product-Remove')")
    @ApiOperation(value = "根据产品模板批量删除产品", tags = {"产品" },  notes = "根据产品模板批量删除产品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{product_template_id}/product_products/batch")
    public ResponseEntity<Boolean> removeBatchByProduct_template(@RequestBody List<Long> ids) {
        product_productService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_productMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_product-Get')")
    @ApiOperation(value = "根据产品模板获取产品", tags = {"产品" },  notes = "根据产品模板获取产品")
	@RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_products/{product_product_id}")
    public ResponseEntity<Product_productDTO> getByProduct_template(@PathVariable("product_template_id") Long product_template_id, @PathVariable("product_product_id") Long product_product_id) {
        Product_product domain = product_productService.get(product_product_id);
        Product_productDTO dto = product_productMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据产品模板获取产品草稿", tags = {"产品" },  notes = "根据产品模板获取产品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/product_templates/{product_template_id}/product_products/getdraft")
    public ResponseEntity<Product_productDTO> getDraftByProduct_template(@PathVariable("product_template_id") Long product_template_id) {
        Product_product domain = new Product_product();
        domain.setProductTmplId(product_template_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_productMapping.toDto(product_productService.getDraft(domain)));
    }

    @ApiOperation(value = "根据产品模板检查产品", tags = {"产品" },  notes = "根据产品模板检查产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/checkkey")
    public ResponseEntity<Boolean> checkKeyByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_productDTO product_productdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_productService.checkKey(product_productMapping.toDomain(product_productdto)));
    }

    @PreAuthorize("hasPermission(this.product_productMapping.toDomain(#product_productdto),'iBizBusinessCentral-Product_product-Save')")
    @ApiOperation(value = "根据产品模板保存产品", tags = {"产品" },  notes = "根据产品模板保存产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/save")
    public ResponseEntity<Boolean> saveByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_productDTO product_productdto) {
        Product_product domain = product_productMapping.toDomain(product_productdto);
        domain.setProductTmplId(product_template_id);
        return ResponseEntity.status(HttpStatus.OK).body(product_productService.save(domain));
    }

    @PreAuthorize("hasPermission(this.product_productMapping.toDomain(#product_productdtos),'iBizBusinessCentral-Product_product-Save')")
    @ApiOperation(value = "根据产品模板批量保存产品", tags = {"产品" },  notes = "根据产品模板批量保存产品")
	@RequestMapping(method = RequestMethod.POST, value = "/product_templates/{product_template_id}/product_products/savebatch")
    public ResponseEntity<Boolean> saveBatchByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody List<Product_productDTO> product_productdtos) {
        List<Product_product> domainlist=product_productMapping.toDomain(product_productdtos);
        for(Product_product domain:domainlist){
             domain.setProductTmplId(product_template_id);
        }
        product_productService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_product-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_product-Get')")
	@ApiOperation(value = "根据产品模板获取数据集", tags = {"产品" } ,notes = "根据产品模板获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/{product_template_id}/product_products/fetchdefault")
	public ResponseEntity<List<Product_productDTO>> fetchProduct_productDefaultByProduct_template(@PathVariable("product_template_id") Long product_template_id,Product_productSearchContext context) {
        context.setN_product_tmpl_id_eq(product_template_id);
        Page<Product_product> domains = product_productService.searchDefault(context) ;
        List<Product_productDTO> list = product_productMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_product-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_product-Get')")
	@ApiOperation(value = "根据产品模板查询数据集", tags = {"产品" } ,notes = "根据产品模板查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/{product_template_id}/product_products/searchdefault")
	public ResponseEntity<Page<Product_productDTO>> searchProduct_productDefaultByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_productSearchContext context) {
        context.setN_product_tmpl_id_eq(product_template_id);
        Page<Product_product> domains = product_productService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_productMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_product-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Product_product-Get')")
	@ApiOperation(value = "根据产品模板获取首选表格", tags = {"产品" } ,notes = "根据产品模板获取首选表格")
    @RequestMapping(method= RequestMethod.GET , value="/product_templates/{product_template_id}/product_products/fetchmaster")
	public ResponseEntity<List<Product_productDTO>> fetchProduct_productMasterByProduct_template(@PathVariable("product_template_id") Long product_template_id,Product_productSearchContext context) {
        context.setN_product_tmpl_id_eq(product_template_id);
        Page<Product_product> domains = product_productService.searchMaster(context) ;
        List<Product_productDTO> list = product_productMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_product-searchMaster-all') and hasPermission(#context,'iBizBusinessCentral-Product_product-Get')")
	@ApiOperation(value = "根据产品模板查询首选表格", tags = {"产品" } ,notes = "根据产品模板查询首选表格")
    @RequestMapping(method= RequestMethod.POST , value="/product_templates/{product_template_id}/product_products/searchmaster")
	public ResponseEntity<Page<Product_productDTO>> searchProduct_productMasterByProduct_template(@PathVariable("product_template_id") Long product_template_id, @RequestBody Product_productSearchContext context) {
        context.setN_product_tmpl_id_eq(product_template_id);
        Page<Product_product> domains = product_productService.searchMaster(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_productMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


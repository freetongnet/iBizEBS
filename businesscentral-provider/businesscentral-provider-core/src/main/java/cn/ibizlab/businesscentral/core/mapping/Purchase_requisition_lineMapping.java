package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition_line;
import cn.ibizlab.businesscentral.core.dto.Purchase_requisition_lineDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CorePurchase_requisition_lineMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Purchase_requisition_lineMapping extends MappingBase<Purchase_requisition_lineDTO, Purchase_requisition_line> {


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task_parts_line;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_task_parts_lineService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_task_parts_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Maintenance Planned Parts" })
@RestController("Core-mro_task_parts_line")
@RequestMapping("")
public class Mro_task_parts_lineResource {

    @Autowired
    public IMro_task_parts_lineService mro_task_parts_lineService;

    @Autowired
    @Lazy
    public Mro_task_parts_lineMapping mro_task_parts_lineMapping;

    @PreAuthorize("hasPermission(this.mro_task_parts_lineMapping.toDomain(#mro_task_parts_linedto),'iBizBusinessCentral-Mro_task_parts_line-Create')")
    @ApiOperation(value = "新建Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "新建Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_task_parts_lines")
    public ResponseEntity<Mro_task_parts_lineDTO> create(@Validated @RequestBody Mro_task_parts_lineDTO mro_task_parts_linedto) {
        Mro_task_parts_line domain = mro_task_parts_lineMapping.toDomain(mro_task_parts_linedto);
		mro_task_parts_lineService.create(domain);
        Mro_task_parts_lineDTO dto = mro_task_parts_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_task_parts_lineMapping.toDomain(#mro_task_parts_linedtos),'iBizBusinessCentral-Mro_task_parts_line-Create')")
    @ApiOperation(value = "批量新建Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "批量新建Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_task_parts_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_task_parts_lineDTO> mro_task_parts_linedtos) {
        mro_task_parts_lineService.createBatch(mro_task_parts_lineMapping.toDomain(mro_task_parts_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_task_parts_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_task_parts_lineService.get(#mro_task_parts_line_id),'iBizBusinessCentral-Mro_task_parts_line-Update')")
    @ApiOperation(value = "更新Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "更新Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_task_parts_lines/{mro_task_parts_line_id}")
    public ResponseEntity<Mro_task_parts_lineDTO> update(@PathVariable("mro_task_parts_line_id") Long mro_task_parts_line_id, @RequestBody Mro_task_parts_lineDTO mro_task_parts_linedto) {
		Mro_task_parts_line domain  = mro_task_parts_lineMapping.toDomain(mro_task_parts_linedto);
        domain .setId(mro_task_parts_line_id);
		mro_task_parts_lineService.update(domain );
		Mro_task_parts_lineDTO dto = mro_task_parts_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_task_parts_lineService.getMroTaskPartsLineByEntities(this.mro_task_parts_lineMapping.toDomain(#mro_task_parts_linedtos)),'iBizBusinessCentral-Mro_task_parts_line-Update')")
    @ApiOperation(value = "批量更新Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "批量更新Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_task_parts_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_task_parts_lineDTO> mro_task_parts_linedtos) {
        mro_task_parts_lineService.updateBatch(mro_task_parts_lineMapping.toDomain(mro_task_parts_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_task_parts_lineService.get(#mro_task_parts_line_id),'iBizBusinessCentral-Mro_task_parts_line-Remove')")
    @ApiOperation(value = "删除Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "删除Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_task_parts_lines/{mro_task_parts_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_task_parts_line_id") Long mro_task_parts_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_task_parts_lineService.remove(mro_task_parts_line_id));
    }

    @PreAuthorize("hasPermission(this.mro_task_parts_lineService.getMroTaskPartsLineByIds(#ids),'iBizBusinessCentral-Mro_task_parts_line-Remove')")
    @ApiOperation(value = "批量删除Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "批量删除Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_task_parts_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_task_parts_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_task_parts_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_task_parts_line-Get')")
    @ApiOperation(value = "获取Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "获取Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_task_parts_lines/{mro_task_parts_line_id}")
    public ResponseEntity<Mro_task_parts_lineDTO> get(@PathVariable("mro_task_parts_line_id") Long mro_task_parts_line_id) {
        Mro_task_parts_line domain = mro_task_parts_lineService.get(mro_task_parts_line_id);
        Mro_task_parts_lineDTO dto = mro_task_parts_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Maintenance Planned Parts草稿", tags = {"Maintenance Planned Parts" },  notes = "获取Maintenance Planned Parts草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_task_parts_lines/getdraft")
    public ResponseEntity<Mro_task_parts_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_task_parts_lineMapping.toDto(mro_task_parts_lineService.getDraft(new Mro_task_parts_line())));
    }

    @ApiOperation(value = "检查Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "检查Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_task_parts_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_task_parts_lineDTO mro_task_parts_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_task_parts_lineService.checkKey(mro_task_parts_lineMapping.toDomain(mro_task_parts_linedto)));
    }

    @PreAuthorize("hasPermission(this.mro_task_parts_lineMapping.toDomain(#mro_task_parts_linedto),'iBizBusinessCentral-Mro_task_parts_line-Save')")
    @ApiOperation(value = "保存Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "保存Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_task_parts_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_task_parts_lineDTO mro_task_parts_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_task_parts_lineService.save(mro_task_parts_lineMapping.toDomain(mro_task_parts_linedto)));
    }

    @PreAuthorize("hasPermission(this.mro_task_parts_lineMapping.toDomain(#mro_task_parts_linedtos),'iBizBusinessCentral-Mro_task_parts_line-Save')")
    @ApiOperation(value = "批量保存Maintenance Planned Parts", tags = {"Maintenance Planned Parts" },  notes = "批量保存Maintenance Planned Parts")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_task_parts_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_task_parts_lineDTO> mro_task_parts_linedtos) {
        mro_task_parts_lineService.saveBatch(mro_task_parts_lineMapping.toDomain(mro_task_parts_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_task_parts_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_task_parts_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Maintenance Planned Parts" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_task_parts_lines/fetchdefault")
	public ResponseEntity<List<Mro_task_parts_lineDTO>> fetchDefault(Mro_task_parts_lineSearchContext context) {
        Page<Mro_task_parts_line> domains = mro_task_parts_lineService.searchDefault(context) ;
        List<Mro_task_parts_lineDTO> list = mro_task_parts_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_task_parts_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_task_parts_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Maintenance Planned Parts" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_task_parts_lines/searchdefault")
	public ResponseEntity<Page<Mro_task_parts_lineDTO>> searchDefault(@RequestBody Mro_task_parts_lineSearchContext context) {
        Page<Mro_task_parts_line> domains = mro_task_parts_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_task_parts_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


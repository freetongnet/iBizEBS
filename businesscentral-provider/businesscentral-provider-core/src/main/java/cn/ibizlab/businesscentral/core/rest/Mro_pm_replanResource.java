package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_replan;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_replanService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_replanSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Replan PM" })
@RestController("Core-mro_pm_replan")
@RequestMapping("")
public class Mro_pm_replanResource {

    @Autowired
    public IMro_pm_replanService mro_pm_replanService;

    @Autowired
    @Lazy
    public Mro_pm_replanMapping mro_pm_replanMapping;

    @PreAuthorize("hasPermission(this.mro_pm_replanMapping.toDomain(#mro_pm_replandto),'iBizBusinessCentral-Mro_pm_replan-Create')")
    @ApiOperation(value = "新建Replan PM", tags = {"Replan PM" },  notes = "新建Replan PM")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans")
    public ResponseEntity<Mro_pm_replanDTO> create(@Validated @RequestBody Mro_pm_replanDTO mro_pm_replandto) {
        Mro_pm_replan domain = mro_pm_replanMapping.toDomain(mro_pm_replandto);
		mro_pm_replanService.create(domain);
        Mro_pm_replanDTO dto = mro_pm_replanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_replanMapping.toDomain(#mro_pm_replandtos),'iBizBusinessCentral-Mro_pm_replan-Create')")
    @ApiOperation(value = "批量新建Replan PM", tags = {"Replan PM" },  notes = "批量新建Replan PM")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_replanDTO> mro_pm_replandtos) {
        mro_pm_replanService.createBatch(mro_pm_replanMapping.toDomain(mro_pm_replandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_pm_replan" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_pm_replanService.get(#mro_pm_replan_id),'iBizBusinessCentral-Mro_pm_replan-Update')")
    @ApiOperation(value = "更新Replan PM", tags = {"Replan PM" },  notes = "更新Replan PM")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_replans/{mro_pm_replan_id}")
    public ResponseEntity<Mro_pm_replanDTO> update(@PathVariable("mro_pm_replan_id") Long mro_pm_replan_id, @RequestBody Mro_pm_replanDTO mro_pm_replandto) {
		Mro_pm_replan domain  = mro_pm_replanMapping.toDomain(mro_pm_replandto);
        domain .setId(mro_pm_replan_id);
		mro_pm_replanService.update(domain );
		Mro_pm_replanDTO dto = mro_pm_replanMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_replanService.getMroPmReplanByEntities(this.mro_pm_replanMapping.toDomain(#mro_pm_replandtos)),'iBizBusinessCentral-Mro_pm_replan-Update')")
    @ApiOperation(value = "批量更新Replan PM", tags = {"Replan PM" },  notes = "批量更新Replan PM")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_replans/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_replanDTO> mro_pm_replandtos) {
        mro_pm_replanService.updateBatch(mro_pm_replanMapping.toDomain(mro_pm_replandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_pm_replanService.get(#mro_pm_replan_id),'iBizBusinessCentral-Mro_pm_replan-Remove')")
    @ApiOperation(value = "删除Replan PM", tags = {"Replan PM" },  notes = "删除Replan PM")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_replans/{mro_pm_replan_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_replan_id") Long mro_pm_replan_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_replanService.remove(mro_pm_replan_id));
    }

    @PreAuthorize("hasPermission(this.mro_pm_replanService.getMroPmReplanByIds(#ids),'iBizBusinessCentral-Mro_pm_replan-Remove')")
    @ApiOperation(value = "批量删除Replan PM", tags = {"Replan PM" },  notes = "批量删除Replan PM")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_replans/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_pm_replanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_pm_replanMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_pm_replan-Get')")
    @ApiOperation(value = "获取Replan PM", tags = {"Replan PM" },  notes = "获取Replan PM")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_replans/{mro_pm_replan_id}")
    public ResponseEntity<Mro_pm_replanDTO> get(@PathVariable("mro_pm_replan_id") Long mro_pm_replan_id) {
        Mro_pm_replan domain = mro_pm_replanService.get(mro_pm_replan_id);
        Mro_pm_replanDTO dto = mro_pm_replanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Replan PM草稿", tags = {"Replan PM" },  notes = "获取Replan PM草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_replans/getdraft")
    public ResponseEntity<Mro_pm_replanDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_replanMapping.toDto(mro_pm_replanService.getDraft(new Mro_pm_replan())));
    }

    @ApiOperation(value = "检查Replan PM", tags = {"Replan PM" },  notes = "检查Replan PM")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_pm_replanDTO mro_pm_replandto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_pm_replanService.checkKey(mro_pm_replanMapping.toDomain(mro_pm_replandto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_replanMapping.toDomain(#mro_pm_replandto),'iBizBusinessCentral-Mro_pm_replan-Save')")
    @ApiOperation(value = "保存Replan PM", tags = {"Replan PM" },  notes = "保存Replan PM")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_pm_replanDTO mro_pm_replandto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_replanService.save(mro_pm_replanMapping.toDomain(mro_pm_replandto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_replanMapping.toDomain(#mro_pm_replandtos),'iBizBusinessCentral-Mro_pm_replan-Save')")
    @ApiOperation(value = "批量保存Replan PM", tags = {"Replan PM" },  notes = "批量保存Replan PM")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_pm_replanDTO> mro_pm_replandtos) {
        mro_pm_replanService.saveBatch(mro_pm_replanMapping.toDomain(mro_pm_replandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_replan-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_replan-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Replan PM" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_replans/fetchdefault")
	public ResponseEntity<List<Mro_pm_replanDTO>> fetchDefault(Mro_pm_replanSearchContext context) {
        Page<Mro_pm_replan> domains = mro_pm_replanService.searchDefault(context) ;
        List<Mro_pm_replanDTO> list = mro_pm_replanMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_replan-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_replan-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Replan PM" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_pm_replans/searchdefault")
	public ResponseEntity<Page<Mro_pm_replanDTO>> searchDefault(@RequestBody Mro_pm_replanSearchContext context) {
        Page<Mro_pm_replan> domains = mro_pm_replanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_replanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


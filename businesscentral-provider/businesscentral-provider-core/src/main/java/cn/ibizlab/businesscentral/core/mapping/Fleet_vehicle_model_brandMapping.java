package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model_brand;
import cn.ibizlab.businesscentral.core.dto.Fleet_vehicle_model_brandDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreFleet_vehicle_model_brandMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Fleet_vehicle_model_brandMapping extends MappingBase<Fleet_vehicle_model_brandDTO, Fleet_vehicle_model_brand> {


}


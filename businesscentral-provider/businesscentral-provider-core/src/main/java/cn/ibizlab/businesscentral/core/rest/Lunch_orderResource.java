package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_orderService;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_orderSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"午餐订单" })
@RestController("Core-lunch_order")
@RequestMapping("")
public class Lunch_orderResource {

    @Autowired
    public ILunch_orderService lunch_orderService;

    @Autowired
    @Lazy
    public Lunch_orderMapping lunch_orderMapping;

    @PreAuthorize("hasPermission(this.lunch_orderMapping.toDomain(#lunch_orderdto),'iBizBusinessCentral-Lunch_order-Create')")
    @ApiOperation(value = "新建午餐订单", tags = {"午餐订单" },  notes = "新建午餐订单")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_orders")
    public ResponseEntity<Lunch_orderDTO> create(@Validated @RequestBody Lunch_orderDTO lunch_orderdto) {
        Lunch_order domain = lunch_orderMapping.toDomain(lunch_orderdto);
		lunch_orderService.create(domain);
        Lunch_orderDTO dto = lunch_orderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_orderMapping.toDomain(#lunch_orderdtos),'iBizBusinessCentral-Lunch_order-Create')")
    @ApiOperation(value = "批量新建午餐订单", tags = {"午餐订单" },  notes = "批量新建午餐订单")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_orderDTO> lunch_orderdtos) {
        lunch_orderService.createBatch(lunch_orderMapping.toDomain(lunch_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "lunch_order" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.lunch_orderService.get(#lunch_order_id),'iBizBusinessCentral-Lunch_order-Update')")
    @ApiOperation(value = "更新午餐订单", tags = {"午餐订单" },  notes = "更新午餐订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_orders/{lunch_order_id}")
    public ResponseEntity<Lunch_orderDTO> update(@PathVariable("lunch_order_id") Long lunch_order_id, @RequestBody Lunch_orderDTO lunch_orderdto) {
		Lunch_order domain  = lunch_orderMapping.toDomain(lunch_orderdto);
        domain .setId(lunch_order_id);
		lunch_orderService.update(domain );
		Lunch_orderDTO dto = lunch_orderMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.lunch_orderService.getLunchOrderByEntities(this.lunch_orderMapping.toDomain(#lunch_orderdtos)),'iBizBusinessCentral-Lunch_order-Update')")
    @ApiOperation(value = "批量更新午餐订单", tags = {"午餐订单" },  notes = "批量更新午餐订单")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_orders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_orderDTO> lunch_orderdtos) {
        lunch_orderService.updateBatch(lunch_orderMapping.toDomain(lunch_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.lunch_orderService.get(#lunch_order_id),'iBizBusinessCentral-Lunch_order-Remove')")
    @ApiOperation(value = "删除午餐订单", tags = {"午餐订单" },  notes = "删除午餐订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_orders/{lunch_order_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("lunch_order_id") Long lunch_order_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_orderService.remove(lunch_order_id));
    }

    @PreAuthorize("hasPermission(this.lunch_orderService.getLunchOrderByIds(#ids),'iBizBusinessCentral-Lunch_order-Remove')")
    @ApiOperation(value = "批量删除午餐订单", tags = {"午餐订单" },  notes = "批量删除午餐订单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_orders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        lunch_orderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.lunch_orderMapping.toDomain(returnObject.body),'iBizBusinessCentral-Lunch_order-Get')")
    @ApiOperation(value = "获取午餐订单", tags = {"午餐订单" },  notes = "获取午餐订单")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_orders/{lunch_order_id}")
    public ResponseEntity<Lunch_orderDTO> get(@PathVariable("lunch_order_id") Long lunch_order_id) {
        Lunch_order domain = lunch_orderService.get(lunch_order_id);
        Lunch_orderDTO dto = lunch_orderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取午餐订单草稿", tags = {"午餐订单" },  notes = "获取午餐订单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_orders/getdraft")
    public ResponseEntity<Lunch_orderDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_orderMapping.toDto(lunch_orderService.getDraft(new Lunch_order())));
    }

    @ApiOperation(value = "检查午餐订单", tags = {"午餐订单" },  notes = "检查午餐订单")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Lunch_orderDTO lunch_orderdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(lunch_orderService.checkKey(lunch_orderMapping.toDomain(lunch_orderdto)));
    }

    @PreAuthorize("hasPermission(this.lunch_orderMapping.toDomain(#lunch_orderdto),'iBizBusinessCentral-Lunch_order-Save')")
    @ApiOperation(value = "保存午餐订单", tags = {"午餐订单" },  notes = "保存午餐订单")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/save")
    public ResponseEntity<Boolean> save(@RequestBody Lunch_orderDTO lunch_orderdto) {
        return ResponseEntity.status(HttpStatus.OK).body(lunch_orderService.save(lunch_orderMapping.toDomain(lunch_orderdto)));
    }

    @PreAuthorize("hasPermission(this.lunch_orderMapping.toDomain(#lunch_orderdtos),'iBizBusinessCentral-Lunch_order-Save')")
    @ApiOperation(value = "批量保存午餐订单", tags = {"午餐订单" },  notes = "批量保存午餐订单")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_orders/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Lunch_orderDTO> lunch_orderdtos) {
        lunch_orderService.saveBatch(lunch_orderMapping.toDomain(lunch_orderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_order-Get')")
	@ApiOperation(value = "获取数据集", tags = {"午餐订单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_orders/fetchdefault")
	public ResponseEntity<List<Lunch_orderDTO>> fetchDefault(Lunch_orderSearchContext context) {
        Page<Lunch_order> domains = lunch_orderService.searchDefault(context) ;
        List<Lunch_orderDTO> list = lunch_orderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Lunch_order-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Lunch_order-Get')")
	@ApiOperation(value = "查询数据集", tags = {"午餐订单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/lunch_orders/searchdefault")
	public ResponseEntity<Page<Lunch_orderDTO>> searchDefault(@RequestBody Lunch_orderSearchContext context) {
        Page<Lunch_order> domains = lunch_orderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_orderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


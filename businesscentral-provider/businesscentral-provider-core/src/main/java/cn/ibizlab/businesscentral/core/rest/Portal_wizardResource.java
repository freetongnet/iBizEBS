package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.businesscentral.core.odoo_portal.service.IPortal_wizardService;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_wizardSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"授予门户访问" })
@RestController("Core-portal_wizard")
@RequestMapping("")
public class Portal_wizardResource {

    @Autowired
    public IPortal_wizardService portal_wizardService;

    @Autowired
    @Lazy
    public Portal_wizardMapping portal_wizardMapping;

    @PreAuthorize("hasPermission(this.portal_wizardMapping.toDomain(#portal_wizarddto),'iBizBusinessCentral-Portal_wizard-Create')")
    @ApiOperation(value = "新建授予门户访问", tags = {"授予门户访问" },  notes = "新建授予门户访问")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizards")
    public ResponseEntity<Portal_wizardDTO> create(@Validated @RequestBody Portal_wizardDTO portal_wizarddto) {
        Portal_wizard domain = portal_wizardMapping.toDomain(portal_wizarddto);
		portal_wizardService.create(domain);
        Portal_wizardDTO dto = portal_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.portal_wizardMapping.toDomain(#portal_wizarddtos),'iBizBusinessCentral-Portal_wizard-Create')")
    @ApiOperation(value = "批量新建授予门户访问", tags = {"授予门户访问" },  notes = "批量新建授予门户访问")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Portal_wizardDTO> portal_wizarddtos) {
        portal_wizardService.createBatch(portal_wizardMapping.toDomain(portal_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "portal_wizard" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.portal_wizardService.get(#portal_wizard_id),'iBizBusinessCentral-Portal_wizard-Update')")
    @ApiOperation(value = "更新授予门户访问", tags = {"授予门户访问" },  notes = "更新授予门户访问")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_wizards/{portal_wizard_id}")
    public ResponseEntity<Portal_wizardDTO> update(@PathVariable("portal_wizard_id") Long portal_wizard_id, @RequestBody Portal_wizardDTO portal_wizarddto) {
		Portal_wizard domain  = portal_wizardMapping.toDomain(portal_wizarddto);
        domain .setId(portal_wizard_id);
		portal_wizardService.update(domain );
		Portal_wizardDTO dto = portal_wizardMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.portal_wizardService.getPortalWizardByEntities(this.portal_wizardMapping.toDomain(#portal_wizarddtos)),'iBizBusinessCentral-Portal_wizard-Update')")
    @ApiOperation(value = "批量更新授予门户访问", tags = {"授予门户访问" },  notes = "批量更新授予门户访问")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_wizardDTO> portal_wizarddtos) {
        portal_wizardService.updateBatch(portal_wizardMapping.toDomain(portal_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.portal_wizardService.get(#portal_wizard_id),'iBizBusinessCentral-Portal_wizard-Remove')")
    @ApiOperation(value = "删除授予门户访问", tags = {"授予门户访问" },  notes = "删除授予门户访问")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizards/{portal_wizard_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("portal_wizard_id") Long portal_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(portal_wizardService.remove(portal_wizard_id));
    }

    @PreAuthorize("hasPermission(this.portal_wizardService.getPortalWizardByIds(#ids),'iBizBusinessCentral-Portal_wizard-Remove')")
    @ApiOperation(value = "批量删除授予门户访问", tags = {"授予门户访问" },  notes = "批量删除授予门户访问")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        portal_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.portal_wizardMapping.toDomain(returnObject.body),'iBizBusinessCentral-Portal_wizard-Get')")
    @ApiOperation(value = "获取授予门户访问", tags = {"授予门户访问" },  notes = "获取授予门户访问")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_wizards/{portal_wizard_id}")
    public ResponseEntity<Portal_wizardDTO> get(@PathVariable("portal_wizard_id") Long portal_wizard_id) {
        Portal_wizard domain = portal_wizardService.get(portal_wizard_id);
        Portal_wizardDTO dto = portal_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取授予门户访问草稿", tags = {"授予门户访问" },  notes = "获取授予门户访问草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_wizards/getdraft")
    public ResponseEntity<Portal_wizardDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(portal_wizardMapping.toDto(portal_wizardService.getDraft(new Portal_wizard())));
    }

    @ApiOperation(value = "检查授予门户访问", tags = {"授予门户访问" },  notes = "检查授予门户访问")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Portal_wizardDTO portal_wizarddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(portal_wizardService.checkKey(portal_wizardMapping.toDomain(portal_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.portal_wizardMapping.toDomain(#portal_wizarddto),'iBizBusinessCentral-Portal_wizard-Save')")
    @ApiOperation(value = "保存授予门户访问", tags = {"授予门户访问" },  notes = "保存授予门户访问")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/save")
    public ResponseEntity<Boolean> save(@RequestBody Portal_wizardDTO portal_wizarddto) {
        return ResponseEntity.status(HttpStatus.OK).body(portal_wizardService.save(portal_wizardMapping.toDomain(portal_wizarddto)));
    }

    @PreAuthorize("hasPermission(this.portal_wizardMapping.toDomain(#portal_wizarddtos),'iBizBusinessCentral-Portal_wizard-Save')")
    @ApiOperation(value = "批量保存授予门户访问", tags = {"授予门户访问" },  notes = "批量保存授予门户访问")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Portal_wizardDTO> portal_wizarddtos) {
        portal_wizardService.saveBatch(portal_wizardMapping.toDomain(portal_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Portal_wizard-Get')")
	@ApiOperation(value = "获取数据集", tags = {"授予门户访问" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/portal_wizards/fetchdefault")
	public ResponseEntity<List<Portal_wizardDTO>> fetchDefault(Portal_wizardSearchContext context) {
        Page<Portal_wizard> domains = portal_wizardService.searchDefault(context) ;
        List<Portal_wizardDTO> list = portal_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_wizard-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Portal_wizard-Get')")
	@ApiOperation(value = "查询数据集", tags = {"授予门户访问" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/portal_wizards/searchdefault")
	public ResponseEntity<Page<Portal_wizardDTO>> searchDefault(@RequestBody Portal_wizardSearchContext context) {
        Page<Portal_wizard> domains = portal_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(portal_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


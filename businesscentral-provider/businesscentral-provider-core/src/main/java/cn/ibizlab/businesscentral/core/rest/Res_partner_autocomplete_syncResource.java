package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_autocomplete_sync;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_autocomplete_syncService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_autocomplete_syncSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"合作伙伴自动完成同步" })
@RestController("Core-res_partner_autocomplete_sync")
@RequestMapping("")
public class Res_partner_autocomplete_syncResource {

    @Autowired
    public IRes_partner_autocomplete_syncService res_partner_autocomplete_syncService;

    @Autowired
    @Lazy
    public Res_partner_autocomplete_syncMapping res_partner_autocomplete_syncMapping;

    @PreAuthorize("hasPermission(this.res_partner_autocomplete_syncMapping.toDomain(#res_partner_autocomplete_syncdto),'iBizBusinessCentral-Res_partner_autocomplete_sync-Create')")
    @ApiOperation(value = "新建合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "新建合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs")
    public ResponseEntity<Res_partner_autocomplete_syncDTO> create(@Validated @RequestBody Res_partner_autocomplete_syncDTO res_partner_autocomplete_syncdto) {
        Res_partner_autocomplete_sync domain = res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdto);
		res_partner_autocomplete_syncService.create(domain);
        Res_partner_autocomplete_syncDTO dto = res_partner_autocomplete_syncMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_autocomplete_syncMapping.toDomain(#res_partner_autocomplete_syncdtos),'iBizBusinessCentral-Res_partner_autocomplete_sync-Create')")
    @ApiOperation(value = "批量新建合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "批量新建合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_autocomplete_syncDTO> res_partner_autocomplete_syncdtos) {
        res_partner_autocomplete_syncService.createBatch(res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_partner_autocomplete_sync" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_partner_autocomplete_syncService.get(#res_partner_autocomplete_sync_id),'iBizBusinessCentral-Res_partner_autocomplete_sync-Update')")
    @ApiOperation(value = "更新合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "更新合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_autocomplete_syncs/{res_partner_autocomplete_sync_id}")
    public ResponseEntity<Res_partner_autocomplete_syncDTO> update(@PathVariable("res_partner_autocomplete_sync_id") Long res_partner_autocomplete_sync_id, @RequestBody Res_partner_autocomplete_syncDTO res_partner_autocomplete_syncdto) {
		Res_partner_autocomplete_sync domain  = res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdto);
        domain .setId(res_partner_autocomplete_sync_id);
		res_partner_autocomplete_syncService.update(domain );
		Res_partner_autocomplete_syncDTO dto = res_partner_autocomplete_syncMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_partner_autocomplete_syncService.getResPartnerAutocompleteSyncByEntities(this.res_partner_autocomplete_syncMapping.toDomain(#res_partner_autocomplete_syncdtos)),'iBizBusinessCentral-Res_partner_autocomplete_sync-Update')")
    @ApiOperation(value = "批量更新合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "批量更新合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_autocomplete_syncs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_autocomplete_syncDTO> res_partner_autocomplete_syncdtos) {
        res_partner_autocomplete_syncService.updateBatch(res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_partner_autocomplete_syncService.get(#res_partner_autocomplete_sync_id),'iBizBusinessCentral-Res_partner_autocomplete_sync-Remove')")
    @ApiOperation(value = "删除合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "删除合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_autocomplete_syncs/{res_partner_autocomplete_sync_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_autocomplete_sync_id") Long res_partner_autocomplete_sync_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partner_autocomplete_syncService.remove(res_partner_autocomplete_sync_id));
    }

    @PreAuthorize("hasPermission(this.res_partner_autocomplete_syncService.getResPartnerAutocompleteSyncByIds(#ids),'iBizBusinessCentral-Res_partner_autocomplete_sync-Remove')")
    @ApiOperation(value = "批量删除合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "批量删除合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_autocomplete_syncs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_partner_autocomplete_syncService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_partner_autocomplete_syncMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_partner_autocomplete_sync-Get')")
    @ApiOperation(value = "获取合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "获取合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_autocomplete_syncs/{res_partner_autocomplete_sync_id}")
    public ResponseEntity<Res_partner_autocomplete_syncDTO> get(@PathVariable("res_partner_autocomplete_sync_id") Long res_partner_autocomplete_sync_id) {
        Res_partner_autocomplete_sync domain = res_partner_autocomplete_syncService.get(res_partner_autocomplete_sync_id);
        Res_partner_autocomplete_syncDTO dto = res_partner_autocomplete_syncMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取合作伙伴自动完成同步草稿", tags = {"合作伙伴自动完成同步" },  notes = "获取合作伙伴自动完成同步草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_autocomplete_syncs/getdraft")
    public ResponseEntity<Res_partner_autocomplete_syncDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_autocomplete_syncMapping.toDto(res_partner_autocomplete_syncService.getDraft(new Res_partner_autocomplete_sync())));
    }

    @ApiOperation(value = "检查合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "检查合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_partner_autocomplete_syncDTO res_partner_autocomplete_syncdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_partner_autocomplete_syncService.checkKey(res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_autocomplete_syncMapping.toDomain(#res_partner_autocomplete_syncdto),'iBizBusinessCentral-Res_partner_autocomplete_sync-Save')")
    @ApiOperation(value = "保存合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "保存合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_partner_autocomplete_syncDTO res_partner_autocomplete_syncdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_partner_autocomplete_syncService.save(res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdto)));
    }

    @PreAuthorize("hasPermission(this.res_partner_autocomplete_syncMapping.toDomain(#res_partner_autocomplete_syncdtos),'iBizBusinessCentral-Res_partner_autocomplete_sync-Save')")
    @ApiOperation(value = "批量保存合作伙伴自动完成同步", tags = {"合作伙伴自动完成同步" },  notes = "批量保存合作伙伴自动完成同步")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_partner_autocomplete_syncDTO> res_partner_autocomplete_syncdtos) {
        res_partner_autocomplete_syncService.saveBatch(res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_autocomplete_sync-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_autocomplete_sync-Get')")
	@ApiOperation(value = "获取数据集", tags = {"合作伙伴自动完成同步" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_autocomplete_syncs/fetchdefault")
	public ResponseEntity<List<Res_partner_autocomplete_syncDTO>> fetchDefault(Res_partner_autocomplete_syncSearchContext context) {
        Page<Res_partner_autocomplete_sync> domains = res_partner_autocomplete_syncService.searchDefault(context) ;
        List<Res_partner_autocomplete_syncDTO> list = res_partner_autocomplete_syncMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_partner_autocomplete_sync-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_partner_autocomplete_sync-Get')")
	@ApiOperation(value = "查询数据集", tags = {"合作伙伴自动完成同步" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_partner_autocomplete_syncs/searchdefault")
	public ResponseEntity<Page<Res_partner_autocomplete_syncDTO>> searchDefault(@RequestBody Res_partner_autocomplete_syncSearchContext context) {
        Page<Res_partner_autocomplete_sync> domains = res_partner_autocomplete_syncService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_autocomplete_syncMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


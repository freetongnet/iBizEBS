package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_attributeService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_attributeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品属性" })
@RestController("Core-product_attribute")
@RequestMapping("")
public class Product_attributeResource {

    @Autowired
    public IProduct_attributeService product_attributeService;

    @Autowired
    @Lazy
    public Product_attributeMapping product_attributeMapping;

    @PreAuthorize("hasPermission(this.product_attributeMapping.toDomain(#product_attributedto),'iBizBusinessCentral-Product_attribute-Create')")
    @ApiOperation(value = "新建产品属性", tags = {"产品属性" },  notes = "新建产品属性")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attributes")
    public ResponseEntity<Product_attributeDTO> create(@Validated @RequestBody Product_attributeDTO product_attributedto) {
        Product_attribute domain = product_attributeMapping.toDomain(product_attributedto);
		product_attributeService.create(domain);
        Product_attributeDTO dto = product_attributeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_attributeMapping.toDomain(#product_attributedtos),'iBizBusinessCentral-Product_attribute-Create')")
    @ApiOperation(value = "批量新建产品属性", tags = {"产品属性" },  notes = "批量新建产品属性")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attributes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_attributeDTO> product_attributedtos) {
        product_attributeService.createBatch(product_attributeMapping.toDomain(product_attributedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_attribute" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_attributeService.get(#product_attribute_id),'iBizBusinessCentral-Product_attribute-Update')")
    @ApiOperation(value = "更新产品属性", tags = {"产品属性" },  notes = "更新产品属性")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_attributes/{product_attribute_id}")
    public ResponseEntity<Product_attributeDTO> update(@PathVariable("product_attribute_id") Long product_attribute_id, @RequestBody Product_attributeDTO product_attributedto) {
		Product_attribute domain  = product_attributeMapping.toDomain(product_attributedto);
        domain .setId(product_attribute_id);
		product_attributeService.update(domain );
		Product_attributeDTO dto = product_attributeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_attributeService.getProductAttributeByEntities(this.product_attributeMapping.toDomain(#product_attributedtos)),'iBizBusinessCentral-Product_attribute-Update')")
    @ApiOperation(value = "批量更新产品属性", tags = {"产品属性" },  notes = "批量更新产品属性")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_attributes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_attributeDTO> product_attributedtos) {
        product_attributeService.updateBatch(product_attributeMapping.toDomain(product_attributedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_attributeService.get(#product_attribute_id),'iBizBusinessCentral-Product_attribute-Remove')")
    @ApiOperation(value = "删除产品属性", tags = {"产品属性" },  notes = "删除产品属性")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_attributes/{product_attribute_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_attribute_id") Long product_attribute_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_attributeService.remove(product_attribute_id));
    }

    @PreAuthorize("hasPermission(this.product_attributeService.getProductAttributeByIds(#ids),'iBizBusinessCentral-Product_attribute-Remove')")
    @ApiOperation(value = "批量删除产品属性", tags = {"产品属性" },  notes = "批量删除产品属性")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_attributes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_attributeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_attributeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_attribute-Get')")
    @ApiOperation(value = "获取产品属性", tags = {"产品属性" },  notes = "获取产品属性")
	@RequestMapping(method = RequestMethod.GET, value = "/product_attributes/{product_attribute_id}")
    public ResponseEntity<Product_attributeDTO> get(@PathVariable("product_attribute_id") Long product_attribute_id) {
        Product_attribute domain = product_attributeService.get(product_attribute_id);
        Product_attributeDTO dto = product_attributeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品属性草稿", tags = {"产品属性" },  notes = "获取产品属性草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_attributes/getdraft")
    public ResponseEntity<Product_attributeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_attributeMapping.toDto(product_attributeService.getDraft(new Product_attribute())));
    }

    @ApiOperation(value = "检查产品属性", tags = {"产品属性" },  notes = "检查产品属性")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attributes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_attributeDTO product_attributedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_attributeService.checkKey(product_attributeMapping.toDomain(product_attributedto)));
    }

    @PreAuthorize("hasPermission(this.product_attributeMapping.toDomain(#product_attributedto),'iBizBusinessCentral-Product_attribute-Save')")
    @ApiOperation(value = "保存产品属性", tags = {"产品属性" },  notes = "保存产品属性")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attributes/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_attributeDTO product_attributedto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_attributeService.save(product_attributeMapping.toDomain(product_attributedto)));
    }

    @PreAuthorize("hasPermission(this.product_attributeMapping.toDomain(#product_attributedtos),'iBizBusinessCentral-Product_attribute-Save')")
    @ApiOperation(value = "批量保存产品属性", tags = {"产品属性" },  notes = "批量保存产品属性")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attributes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_attributeDTO> product_attributedtos) {
        product_attributeService.saveBatch(product_attributeMapping.toDomain(product_attributedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_attribute-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_attribute-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品属性" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_attributes/fetchdefault")
	public ResponseEntity<List<Product_attributeDTO>> fetchDefault(Product_attributeSearchContext context) {
        Page<Product_attribute> domains = product_attributeService.searchDefault(context) ;
        List<Product_attributeDTO> list = product_attributeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_attribute-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_attribute-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品属性" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_attributes/searchdefault")
	public ResponseEntity<Page<Product_attributeDTO>> searchDefault(@RequestBody Product_attributeSearchContext context) {
        Page<Product_attribute> domains = product_attributeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_attributeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_language_import;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_language_importService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_language_importSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"语言导入" })
@RestController("Core-base_language_import")
@RequestMapping("")
public class Base_language_importResource {

    @Autowired
    public IBase_language_importService base_language_importService;

    @Autowired
    @Lazy
    public Base_language_importMapping base_language_importMapping;

    @PreAuthorize("hasPermission(this.base_language_importMapping.toDomain(#base_language_importdto),'iBizBusinessCentral-Base_language_import-Create')")
    @ApiOperation(value = "新建语言导入", tags = {"语言导入" },  notes = "新建语言导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_imports")
    public ResponseEntity<Base_language_importDTO> create(@Validated @RequestBody Base_language_importDTO base_language_importdto) {
        Base_language_import domain = base_language_importMapping.toDomain(base_language_importdto);
		base_language_importService.create(domain);
        Base_language_importDTO dto = base_language_importMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_language_importMapping.toDomain(#base_language_importdtos),'iBizBusinessCentral-Base_language_import-Create')")
    @ApiOperation(value = "批量新建语言导入", tags = {"语言导入" },  notes = "批量新建语言导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_language_importDTO> base_language_importdtos) {
        base_language_importService.createBatch(base_language_importMapping.toDomain(base_language_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_language_import" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_language_importService.get(#base_language_import_id),'iBizBusinessCentral-Base_language_import-Update')")
    @ApiOperation(value = "更新语言导入", tags = {"语言导入" },  notes = "更新语言导入")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_imports/{base_language_import_id}")
    public ResponseEntity<Base_language_importDTO> update(@PathVariable("base_language_import_id") Long base_language_import_id, @RequestBody Base_language_importDTO base_language_importdto) {
		Base_language_import domain  = base_language_importMapping.toDomain(base_language_importdto);
        domain .setId(base_language_import_id);
		base_language_importService.update(domain );
		Base_language_importDTO dto = base_language_importMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_language_importService.getBaseLanguageImportByEntities(this.base_language_importMapping.toDomain(#base_language_importdtos)),'iBizBusinessCentral-Base_language_import-Update')")
    @ApiOperation(value = "批量更新语言导入", tags = {"语言导入" },  notes = "批量更新语言导入")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_imports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_language_importDTO> base_language_importdtos) {
        base_language_importService.updateBatch(base_language_importMapping.toDomain(base_language_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_language_importService.get(#base_language_import_id),'iBizBusinessCentral-Base_language_import-Remove')")
    @ApiOperation(value = "删除语言导入", tags = {"语言导入" },  notes = "删除语言导入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_imports/{base_language_import_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_language_import_id") Long base_language_import_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_language_importService.remove(base_language_import_id));
    }

    @PreAuthorize("hasPermission(this.base_language_importService.getBaseLanguageImportByIds(#ids),'iBizBusinessCentral-Base_language_import-Remove')")
    @ApiOperation(value = "批量删除语言导入", tags = {"语言导入" },  notes = "批量删除语言导入")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_imports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_language_importService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_language_importMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_language_import-Get')")
    @ApiOperation(value = "获取语言导入", tags = {"语言导入" },  notes = "获取语言导入")
	@RequestMapping(method = RequestMethod.GET, value = "/base_language_imports/{base_language_import_id}")
    public ResponseEntity<Base_language_importDTO> get(@PathVariable("base_language_import_id") Long base_language_import_id) {
        Base_language_import domain = base_language_importService.get(base_language_import_id);
        Base_language_importDTO dto = base_language_importMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取语言导入草稿", tags = {"语言导入" },  notes = "获取语言导入草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_language_imports/getdraft")
    public ResponseEntity<Base_language_importDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_language_importMapping.toDto(base_language_importService.getDraft(new Base_language_import())));
    }

    @ApiOperation(value = "检查语言导入", tags = {"语言导入" },  notes = "检查语言导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_language_importDTO base_language_importdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_language_importService.checkKey(base_language_importMapping.toDomain(base_language_importdto)));
    }

    @PreAuthorize("hasPermission(this.base_language_importMapping.toDomain(#base_language_importdto),'iBizBusinessCentral-Base_language_import-Save')")
    @ApiOperation(value = "保存语言导入", tags = {"语言导入" },  notes = "保存语言导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_language_importDTO base_language_importdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_language_importService.save(base_language_importMapping.toDomain(base_language_importdto)));
    }

    @PreAuthorize("hasPermission(this.base_language_importMapping.toDomain(#base_language_importdtos),'iBizBusinessCentral-Base_language_import-Save')")
    @ApiOperation(value = "批量保存语言导入", tags = {"语言导入" },  notes = "批量保存语言导入")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_language_importDTO> base_language_importdtos) {
        base_language_importService.saveBatch(base_language_importMapping.toDomain(base_language_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_language_import-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_language_import-Get')")
	@ApiOperation(value = "获取数据集", tags = {"语言导入" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_language_imports/fetchdefault")
	public ResponseEntity<List<Base_language_importDTO>> fetchDefault(Base_language_importSearchContext context) {
        Page<Base_language_import> domains = base_language_importService.searchDefault(context) ;
        List<Base_language_importDTO> list = base_language_importMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_language_import-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_language_import-Get')")
	@ApiOperation(value = "查询数据集", tags = {"语言导入" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_language_imports/searchdefault")
	public ResponseEntity<Page<Base_language_importDTO>> searchDefault(@RequestBody Base_language_importSearchContext context) {
        Page<Base_language_import> domains = base_language_importService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_language_importMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_automation_lead_test;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_automation_lead_testService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_automation_lead_testSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"自动化规则测试" })
@RestController("Core-base_automation_lead_test")
@RequestMapping("")
public class Base_automation_lead_testResource {

    @Autowired
    public IBase_automation_lead_testService base_automation_lead_testService;

    @Autowired
    @Lazy
    public Base_automation_lead_testMapping base_automation_lead_testMapping;

    @PreAuthorize("hasPermission(this.base_automation_lead_testMapping.toDomain(#base_automation_lead_testdto),'iBizBusinessCentral-Base_automation_lead_test-Create')")
    @ApiOperation(value = "新建自动化规则测试", tags = {"自动化规则测试" },  notes = "新建自动化规则测试")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests")
    public ResponseEntity<Base_automation_lead_testDTO> create(@Validated @RequestBody Base_automation_lead_testDTO base_automation_lead_testdto) {
        Base_automation_lead_test domain = base_automation_lead_testMapping.toDomain(base_automation_lead_testdto);
		base_automation_lead_testService.create(domain);
        Base_automation_lead_testDTO dto = base_automation_lead_testMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_automation_lead_testMapping.toDomain(#base_automation_lead_testdtos),'iBizBusinessCentral-Base_automation_lead_test-Create')")
    @ApiOperation(value = "批量新建自动化规则测试", tags = {"自动化规则测试" },  notes = "批量新建自动化规则测试")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_automation_lead_testDTO> base_automation_lead_testdtos) {
        base_automation_lead_testService.createBatch(base_automation_lead_testMapping.toDomain(base_automation_lead_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "base_automation_lead_test" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.base_automation_lead_testService.get(#base_automation_lead_test_id),'iBizBusinessCentral-Base_automation_lead_test-Update')")
    @ApiOperation(value = "更新自动化规则测试", tags = {"自动化规则测试" },  notes = "更新自动化规则测试")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_automation_lead_tests/{base_automation_lead_test_id}")
    public ResponseEntity<Base_automation_lead_testDTO> update(@PathVariable("base_automation_lead_test_id") Long base_automation_lead_test_id, @RequestBody Base_automation_lead_testDTO base_automation_lead_testdto) {
		Base_automation_lead_test domain  = base_automation_lead_testMapping.toDomain(base_automation_lead_testdto);
        domain .setId(base_automation_lead_test_id);
		base_automation_lead_testService.update(domain );
		Base_automation_lead_testDTO dto = base_automation_lead_testMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.base_automation_lead_testService.getBaseAutomationLeadTestByEntities(this.base_automation_lead_testMapping.toDomain(#base_automation_lead_testdtos)),'iBizBusinessCentral-Base_automation_lead_test-Update')")
    @ApiOperation(value = "批量更新自动化规则测试", tags = {"自动化规则测试" },  notes = "批量更新自动化规则测试")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_automation_lead_tests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_automation_lead_testDTO> base_automation_lead_testdtos) {
        base_automation_lead_testService.updateBatch(base_automation_lead_testMapping.toDomain(base_automation_lead_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.base_automation_lead_testService.get(#base_automation_lead_test_id),'iBizBusinessCentral-Base_automation_lead_test-Remove')")
    @ApiOperation(value = "删除自动化规则测试", tags = {"自动化规则测试" },  notes = "删除自动化规则测试")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_automation_lead_tests/{base_automation_lead_test_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("base_automation_lead_test_id") Long base_automation_lead_test_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_automation_lead_testService.remove(base_automation_lead_test_id));
    }

    @PreAuthorize("hasPermission(this.base_automation_lead_testService.getBaseAutomationLeadTestByIds(#ids),'iBizBusinessCentral-Base_automation_lead_test-Remove')")
    @ApiOperation(value = "批量删除自动化规则测试", tags = {"自动化规则测试" },  notes = "批量删除自动化规则测试")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_automation_lead_tests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        base_automation_lead_testService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.base_automation_lead_testMapping.toDomain(returnObject.body),'iBizBusinessCentral-Base_automation_lead_test-Get')")
    @ApiOperation(value = "获取自动化规则测试", tags = {"自动化规则测试" },  notes = "获取自动化规则测试")
	@RequestMapping(method = RequestMethod.GET, value = "/base_automation_lead_tests/{base_automation_lead_test_id}")
    public ResponseEntity<Base_automation_lead_testDTO> get(@PathVariable("base_automation_lead_test_id") Long base_automation_lead_test_id) {
        Base_automation_lead_test domain = base_automation_lead_testService.get(base_automation_lead_test_id);
        Base_automation_lead_testDTO dto = base_automation_lead_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取自动化规则测试草稿", tags = {"自动化规则测试" },  notes = "获取自动化规则测试草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/base_automation_lead_tests/getdraft")
    public ResponseEntity<Base_automation_lead_testDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(base_automation_lead_testMapping.toDto(base_automation_lead_testService.getDraft(new Base_automation_lead_test())));
    }

    @ApiOperation(value = "检查自动化规则测试", tags = {"自动化规则测试" },  notes = "检查自动化规则测试")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Base_automation_lead_testDTO base_automation_lead_testdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(base_automation_lead_testService.checkKey(base_automation_lead_testMapping.toDomain(base_automation_lead_testdto)));
    }

    @PreAuthorize("hasPermission(this.base_automation_lead_testMapping.toDomain(#base_automation_lead_testdto),'iBizBusinessCentral-Base_automation_lead_test-Save')")
    @ApiOperation(value = "保存自动化规则测试", tags = {"自动化规则测试" },  notes = "保存自动化规则测试")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests/save")
    public ResponseEntity<Boolean> save(@RequestBody Base_automation_lead_testDTO base_automation_lead_testdto) {
        return ResponseEntity.status(HttpStatus.OK).body(base_automation_lead_testService.save(base_automation_lead_testMapping.toDomain(base_automation_lead_testdto)));
    }

    @PreAuthorize("hasPermission(this.base_automation_lead_testMapping.toDomain(#base_automation_lead_testdtos),'iBizBusinessCentral-Base_automation_lead_test-Save')")
    @ApiOperation(value = "批量保存自动化规则测试", tags = {"自动化规则测试" },  notes = "批量保存自动化规则测试")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Base_automation_lead_testDTO> base_automation_lead_testdtos) {
        base_automation_lead_testService.saveBatch(base_automation_lead_testMapping.toDomain(base_automation_lead_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_automation_lead_test-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_automation_lead_test-Get')")
	@ApiOperation(value = "获取数据集", tags = {"自动化规则测试" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/base_automation_lead_tests/fetchdefault")
	public ResponseEntity<List<Base_automation_lead_testDTO>> fetchDefault(Base_automation_lead_testSearchContext context) {
        Page<Base_automation_lead_test> domains = base_automation_lead_testService.searchDefault(context) ;
        List<Base_automation_lead_testDTO> list = base_automation_lead_testMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Base_automation_lead_test-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Base_automation_lead_test-Get')")
	@ApiOperation(value = "查询数据集", tags = {"自动化规则测试" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/base_automation_lead_tests/searchdefault")
	public ResponseEntity<Page<Base_automation_lead_testDTO>> searchDefault(@RequestBody Base_automation_lead_testSearchContext context) {
        Page<Base_automation_lead_test> domains = base_automation_lead_testService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_automation_lead_testMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_assignation_log;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_assignation_logService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"交通工具驾驶历史" })
@RestController("Core-fleet_vehicle_assignation_log")
@RequestMapping("")
public class Fleet_vehicle_assignation_logResource {

    @Autowired
    public IFleet_vehicle_assignation_logService fleet_vehicle_assignation_logService;

    @Autowired
    @Lazy
    public Fleet_vehicle_assignation_logMapping fleet_vehicle_assignation_logMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_assignation_logMapping.toDomain(#fleet_vehicle_assignation_logdto),'iBizBusinessCentral-Fleet_vehicle_assignation_log-Create')")
    @ApiOperation(value = "新建交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "新建交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs")
    public ResponseEntity<Fleet_vehicle_assignation_logDTO> create(@Validated @RequestBody Fleet_vehicle_assignation_logDTO fleet_vehicle_assignation_logdto) {
        Fleet_vehicle_assignation_log domain = fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdto);
		fleet_vehicle_assignation_logService.create(domain);
        Fleet_vehicle_assignation_logDTO dto = fleet_vehicle_assignation_logMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_assignation_logMapping.toDomain(#fleet_vehicle_assignation_logdtos),'iBizBusinessCentral-Fleet_vehicle_assignation_log-Create')")
    @ApiOperation(value = "批量新建交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "批量新建交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_assignation_logDTO> fleet_vehicle_assignation_logdtos) {
        fleet_vehicle_assignation_logService.createBatch(fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_assignation_log" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_assignation_logService.get(#fleet_vehicle_assignation_log_id),'iBizBusinessCentral-Fleet_vehicle_assignation_log-Update')")
    @ApiOperation(value = "更新交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "更新交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_assignation_logs/{fleet_vehicle_assignation_log_id}")
    public ResponseEntity<Fleet_vehicle_assignation_logDTO> update(@PathVariable("fleet_vehicle_assignation_log_id") Long fleet_vehicle_assignation_log_id, @RequestBody Fleet_vehicle_assignation_logDTO fleet_vehicle_assignation_logdto) {
		Fleet_vehicle_assignation_log domain  = fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdto);
        domain .setId(fleet_vehicle_assignation_log_id);
		fleet_vehicle_assignation_logService.update(domain );
		Fleet_vehicle_assignation_logDTO dto = fleet_vehicle_assignation_logMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_assignation_logService.getFleetVehicleAssignationLogByEntities(this.fleet_vehicle_assignation_logMapping.toDomain(#fleet_vehicle_assignation_logdtos)),'iBizBusinessCentral-Fleet_vehicle_assignation_log-Update')")
    @ApiOperation(value = "批量更新交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "批量更新交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_assignation_logs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_assignation_logDTO> fleet_vehicle_assignation_logdtos) {
        fleet_vehicle_assignation_logService.updateBatch(fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_assignation_logService.get(#fleet_vehicle_assignation_log_id),'iBizBusinessCentral-Fleet_vehicle_assignation_log-Remove')")
    @ApiOperation(value = "删除交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "删除交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_assignation_logs/{fleet_vehicle_assignation_log_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_assignation_log_id") Long fleet_vehicle_assignation_log_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_assignation_logService.remove(fleet_vehicle_assignation_log_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_assignation_logService.getFleetVehicleAssignationLogByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_assignation_log-Remove')")
    @ApiOperation(value = "批量删除交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "批量删除交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_assignation_logs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_assignation_logService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_assignation_logMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_assignation_log-Get')")
    @ApiOperation(value = "获取交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "获取交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_assignation_logs/{fleet_vehicle_assignation_log_id}")
    public ResponseEntity<Fleet_vehicle_assignation_logDTO> get(@PathVariable("fleet_vehicle_assignation_log_id") Long fleet_vehicle_assignation_log_id) {
        Fleet_vehicle_assignation_log domain = fleet_vehicle_assignation_logService.get(fleet_vehicle_assignation_log_id);
        Fleet_vehicle_assignation_logDTO dto = fleet_vehicle_assignation_logMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取交通工具驾驶历史草稿", tags = {"交通工具驾驶历史" },  notes = "获取交通工具驾驶历史草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_assignation_logs/getdraft")
    public ResponseEntity<Fleet_vehicle_assignation_logDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_assignation_logMapping.toDto(fleet_vehicle_assignation_logService.getDraft(new Fleet_vehicle_assignation_log())));
    }

    @ApiOperation(value = "检查交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "检查交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_assignation_logDTO fleet_vehicle_assignation_logdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_assignation_logService.checkKey(fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_assignation_logMapping.toDomain(#fleet_vehicle_assignation_logdto),'iBizBusinessCentral-Fleet_vehicle_assignation_log-Save')")
    @ApiOperation(value = "保存交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "保存交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_assignation_logDTO fleet_vehicle_assignation_logdto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_assignation_logService.save(fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_assignation_logMapping.toDomain(#fleet_vehicle_assignation_logdtos),'iBizBusinessCentral-Fleet_vehicle_assignation_log-Save')")
    @ApiOperation(value = "批量保存交通工具驾驶历史", tags = {"交通工具驾驶历史" },  notes = "批量保存交通工具驾驶历史")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_assignation_logs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_assignation_logDTO> fleet_vehicle_assignation_logdtos) {
        fleet_vehicle_assignation_logService.saveBatch(fleet_vehicle_assignation_logMapping.toDomain(fleet_vehicle_assignation_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_assignation_log-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_assignation_log-Get')")
	@ApiOperation(value = "获取数据集", tags = {"交通工具驾驶历史" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_assignation_logs/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_assignation_logDTO>> fetchDefault(Fleet_vehicle_assignation_logSearchContext context) {
        Page<Fleet_vehicle_assignation_log> domains = fleet_vehicle_assignation_logService.searchDefault(context) ;
        List<Fleet_vehicle_assignation_logDTO> list = fleet_vehicle_assignation_logMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_assignation_log-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_assignation_log-Get')")
	@ApiOperation(value = "查询数据集", tags = {"交通工具驾驶历史" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_assignation_logs/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_assignation_logDTO>> searchDefault(@RequestBody Fleet_vehicle_assignation_logSearchContext context) {
        Page<Fleet_vehicle_assignation_log> domains = fleet_vehicle_assignation_logService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_assignation_logMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


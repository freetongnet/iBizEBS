package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meter_intervalService;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"Meter interval" })
@RestController("Core-mro_pm_meter_interval")
@RequestMapping("")
public class Mro_pm_meter_intervalResource {

    @Autowired
    public IMro_pm_meter_intervalService mro_pm_meter_intervalService;

    @Autowired
    @Lazy
    public Mro_pm_meter_intervalMapping mro_pm_meter_intervalMapping;

    @PreAuthorize("hasPermission(this.mro_pm_meter_intervalMapping.toDomain(#mro_pm_meter_intervaldto),'iBizBusinessCentral-Mro_pm_meter_interval-Create')")
    @ApiOperation(value = "新建Meter interval", tags = {"Meter interval" },  notes = "新建Meter interval")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals")
    public ResponseEntity<Mro_pm_meter_intervalDTO> create(@Validated @RequestBody Mro_pm_meter_intervalDTO mro_pm_meter_intervaldto) {
        Mro_pm_meter_interval domain = mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldto);
		mro_pm_meter_intervalService.create(domain);
        Mro_pm_meter_intervalDTO dto = mro_pm_meter_intervalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_intervalMapping.toDomain(#mro_pm_meter_intervaldtos),'iBizBusinessCentral-Mro_pm_meter_interval-Create')")
    @ApiOperation(value = "批量新建Meter interval", tags = {"Meter interval" },  notes = "批量新建Meter interval")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_meter_intervalDTO> mro_pm_meter_intervaldtos) {
        mro_pm_meter_intervalService.createBatch(mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mro_pm_meter_interval" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mro_pm_meter_intervalService.get(#mro_pm_meter_interval_id),'iBizBusinessCentral-Mro_pm_meter_interval-Update')")
    @ApiOperation(value = "更新Meter interval", tags = {"Meter interval" },  notes = "更新Meter interval")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_intervals/{mro_pm_meter_interval_id}")
    public ResponseEntity<Mro_pm_meter_intervalDTO> update(@PathVariable("mro_pm_meter_interval_id") Long mro_pm_meter_interval_id, @RequestBody Mro_pm_meter_intervalDTO mro_pm_meter_intervaldto) {
		Mro_pm_meter_interval domain  = mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldto);
        domain .setId(mro_pm_meter_interval_id);
		mro_pm_meter_intervalService.update(domain );
		Mro_pm_meter_intervalDTO dto = mro_pm_meter_intervalMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_intervalService.getMroPmMeterIntervalByEntities(this.mro_pm_meter_intervalMapping.toDomain(#mro_pm_meter_intervaldtos)),'iBizBusinessCentral-Mro_pm_meter_interval-Update')")
    @ApiOperation(value = "批量更新Meter interval", tags = {"Meter interval" },  notes = "批量更新Meter interval")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_intervals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_meter_intervalDTO> mro_pm_meter_intervaldtos) {
        mro_pm_meter_intervalService.updateBatch(mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_intervalService.get(#mro_pm_meter_interval_id),'iBizBusinessCentral-Mro_pm_meter_interval-Remove')")
    @ApiOperation(value = "删除Meter interval", tags = {"Meter interval" },  notes = "删除Meter interval")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_intervals/{mro_pm_meter_interval_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_meter_interval_id") Long mro_pm_meter_interval_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_intervalService.remove(mro_pm_meter_interval_id));
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_intervalService.getMroPmMeterIntervalByIds(#ids),'iBizBusinessCentral-Mro_pm_meter_interval-Remove')")
    @ApiOperation(value = "批量删除Meter interval", tags = {"Meter interval" },  notes = "批量删除Meter interval")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_intervals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mro_pm_meter_intervalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mro_pm_meter_intervalMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mro_pm_meter_interval-Get')")
    @ApiOperation(value = "获取Meter interval", tags = {"Meter interval" },  notes = "获取Meter interval")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_intervals/{mro_pm_meter_interval_id}")
    public ResponseEntity<Mro_pm_meter_intervalDTO> get(@PathVariable("mro_pm_meter_interval_id") Long mro_pm_meter_interval_id) {
        Mro_pm_meter_interval domain = mro_pm_meter_intervalService.get(mro_pm_meter_interval_id);
        Mro_pm_meter_intervalDTO dto = mro_pm_meter_intervalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取Meter interval草稿", tags = {"Meter interval" },  notes = "获取Meter interval草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_intervals/getdraft")
    public ResponseEntity<Mro_pm_meter_intervalDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_intervalMapping.toDto(mro_pm_meter_intervalService.getDraft(new Mro_pm_meter_interval())));
    }

    @ApiOperation(value = "检查Meter interval", tags = {"Meter interval" },  notes = "检查Meter interval")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mro_pm_meter_intervalDTO mro_pm_meter_intervaldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_intervalService.checkKey(mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_intervalMapping.toDomain(#mro_pm_meter_intervaldto),'iBizBusinessCentral-Mro_pm_meter_interval-Save')")
    @ApiOperation(value = "保存Meter interval", tags = {"Meter interval" },  notes = "保存Meter interval")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/save")
    public ResponseEntity<Boolean> save(@RequestBody Mro_pm_meter_intervalDTO mro_pm_meter_intervaldto) {
        return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meter_intervalService.save(mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldto)));
    }

    @PreAuthorize("hasPermission(this.mro_pm_meter_intervalMapping.toDomain(#mro_pm_meter_intervaldtos),'iBizBusinessCentral-Mro_pm_meter_interval-Save')")
    @ApiOperation(value = "批量保存Meter interval", tags = {"Meter interval" },  notes = "批量保存Meter interval")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_intervals/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mro_pm_meter_intervalDTO> mro_pm_meter_intervaldtos) {
        mro_pm_meter_intervalService.saveBatch(mro_pm_meter_intervalMapping.toDomain(mro_pm_meter_intervaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_meter_interval-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_meter_interval-Get')")
	@ApiOperation(value = "获取数据集", tags = {"Meter interval" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_meter_intervals/fetchdefault")
	public ResponseEntity<List<Mro_pm_meter_intervalDTO>> fetchDefault(Mro_pm_meter_intervalSearchContext context) {
        Page<Mro_pm_meter_interval> domains = mro_pm_meter_intervalService.searchDefault(context) ;
        List<Mro_pm_meter_intervalDTO> list = mro_pm_meter_intervalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mro_pm_meter_interval-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mro_pm_meter_interval-Get')")
	@ApiOperation(value = "查询数据集", tags = {"Meter interval" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mro_pm_meter_intervals/searchdefault")
	public ResponseEntity<Page<Mro_pm_meter_intervalDTO>> searchDefault(@RequestBody Mro_pm_meter_intervalSearchContext context) {
        Page<Mro_pm_meter_interval> domains = mro_pm_meter_intervalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_meter_intervalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


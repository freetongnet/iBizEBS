package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_resourceService;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_resourceSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"资源" })
@RestController("Core-resource_resource")
@RequestMapping("")
public class Resource_resourceResource {

    @Autowired
    public IResource_resourceService resource_resourceService;

    @Autowired
    @Lazy
    public Resource_resourceMapping resource_resourceMapping;

    @PreAuthorize("hasPermission(this.resource_resourceMapping.toDomain(#resource_resourcedto),'iBizBusinessCentral-Resource_resource-Create')")
    @ApiOperation(value = "新建资源", tags = {"资源" },  notes = "新建资源")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_resources")
    public ResponseEntity<Resource_resourceDTO> create(@Validated @RequestBody Resource_resourceDTO resource_resourcedto) {
        Resource_resource domain = resource_resourceMapping.toDomain(resource_resourcedto);
		resource_resourceService.create(domain);
        Resource_resourceDTO dto = resource_resourceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_resourceMapping.toDomain(#resource_resourcedtos),'iBizBusinessCentral-Resource_resource-Create')")
    @ApiOperation(value = "批量新建资源", tags = {"资源" },  notes = "批量新建资源")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_resources/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_resourceDTO> resource_resourcedtos) {
        resource_resourceService.createBatch(resource_resourceMapping.toDomain(resource_resourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "resource_resource" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.resource_resourceService.get(#resource_resource_id),'iBizBusinessCentral-Resource_resource-Update')")
    @ApiOperation(value = "更新资源", tags = {"资源" },  notes = "更新资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_resources/{resource_resource_id}")
    public ResponseEntity<Resource_resourceDTO> update(@PathVariable("resource_resource_id") Long resource_resource_id, @RequestBody Resource_resourceDTO resource_resourcedto) {
		Resource_resource domain  = resource_resourceMapping.toDomain(resource_resourcedto);
        domain .setId(resource_resource_id);
		resource_resourceService.update(domain );
		Resource_resourceDTO dto = resource_resourceMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.resource_resourceService.getResourceResourceByEntities(this.resource_resourceMapping.toDomain(#resource_resourcedtos)),'iBizBusinessCentral-Resource_resource-Update')")
    @ApiOperation(value = "批量更新资源", tags = {"资源" },  notes = "批量更新资源")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_resources/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_resourceDTO> resource_resourcedtos) {
        resource_resourceService.updateBatch(resource_resourceMapping.toDomain(resource_resourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.resource_resourceService.get(#resource_resource_id),'iBizBusinessCentral-Resource_resource-Remove')")
    @ApiOperation(value = "删除资源", tags = {"资源" },  notes = "删除资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_resources/{resource_resource_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("resource_resource_id") Long resource_resource_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_resourceService.remove(resource_resource_id));
    }

    @PreAuthorize("hasPermission(this.resource_resourceService.getResourceResourceByIds(#ids),'iBizBusinessCentral-Resource_resource-Remove')")
    @ApiOperation(value = "批量删除资源", tags = {"资源" },  notes = "批量删除资源")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_resources/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        resource_resourceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.resource_resourceMapping.toDomain(returnObject.body),'iBizBusinessCentral-Resource_resource-Get')")
    @ApiOperation(value = "获取资源", tags = {"资源" },  notes = "获取资源")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_resources/{resource_resource_id}")
    public ResponseEntity<Resource_resourceDTO> get(@PathVariable("resource_resource_id") Long resource_resource_id) {
        Resource_resource domain = resource_resourceService.get(resource_resource_id);
        Resource_resourceDTO dto = resource_resourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取资源草稿", tags = {"资源" },  notes = "获取资源草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_resources/getdraft")
    public ResponseEntity<Resource_resourceDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(resource_resourceMapping.toDto(resource_resourceService.getDraft(new Resource_resource())));
    }

    @ApiOperation(value = "检查资源", tags = {"资源" },  notes = "检查资源")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_resources/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Resource_resourceDTO resource_resourcedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(resource_resourceService.checkKey(resource_resourceMapping.toDomain(resource_resourcedto)));
    }

    @PreAuthorize("hasPermission(this.resource_resourceMapping.toDomain(#resource_resourcedto),'iBizBusinessCentral-Resource_resource-Save')")
    @ApiOperation(value = "保存资源", tags = {"资源" },  notes = "保存资源")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_resources/save")
    public ResponseEntity<Boolean> save(@RequestBody Resource_resourceDTO resource_resourcedto) {
        return ResponseEntity.status(HttpStatus.OK).body(resource_resourceService.save(resource_resourceMapping.toDomain(resource_resourcedto)));
    }

    @PreAuthorize("hasPermission(this.resource_resourceMapping.toDomain(#resource_resourcedtos),'iBizBusinessCentral-Resource_resource-Save')")
    @ApiOperation(value = "批量保存资源", tags = {"资源" },  notes = "批量保存资源")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_resources/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Resource_resourceDTO> resource_resourcedtos) {
        resource_resourceService.saveBatch(resource_resourceMapping.toDomain(resource_resourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_resource-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_resource-Get')")
	@ApiOperation(value = "获取数据集", tags = {"资源" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/resource_resources/fetchdefault")
	public ResponseEntity<List<Resource_resourceDTO>> fetchDefault(Resource_resourceSearchContext context) {
        Page<Resource_resource> domains = resource_resourceService.searchDefault(context) ;
        List<Resource_resourceDTO> list = resource_resourceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Resource_resource-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Resource_resource-Get')")
	@ApiOperation(value = "查询数据集", tags = {"资源" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/resource_resources/searchdefault")
	public ResponseEntity<Page<Resource_resourceDTO>> searchDefault(@RequestBody Resource_resourceSearchContext context) {
        Page<Resource_resource> domains = resource_resourceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_resourceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


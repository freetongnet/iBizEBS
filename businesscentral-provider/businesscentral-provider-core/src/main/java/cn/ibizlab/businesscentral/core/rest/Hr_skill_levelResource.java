package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_skill_level;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_skill_levelService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_skill_levelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"技能等级" })
@RestController("Core-hr_skill_level")
@RequestMapping("")
public class Hr_skill_levelResource {

    @Autowired
    public IHr_skill_levelService hr_skill_levelService;

    @Autowired
    @Lazy
    public Hr_skill_levelMapping hr_skill_levelMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Create-all')")
    @ApiOperation(value = "新建技能等级", tags = {"技能等级" },  notes = "新建技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_levels")
    public ResponseEntity<Hr_skill_levelDTO> create(@Validated @RequestBody Hr_skill_levelDTO hr_skill_leveldto) {
        Hr_skill_level domain = hr_skill_levelMapping.toDomain(hr_skill_leveldto);
		hr_skill_levelService.create(domain);
        Hr_skill_levelDTO dto = hr_skill_levelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Create-all')")
    @ApiOperation(value = "批量新建技能等级", tags = {"技能等级" },  notes = "批量新建技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_levels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_skill_levelDTO> hr_skill_leveldtos) {
        hr_skill_levelService.createBatch(hr_skill_levelMapping.toDomain(hr_skill_leveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Update-all')")
    @ApiOperation(value = "更新技能等级", tags = {"技能等级" },  notes = "更新技能等级")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skill_levels/{hr_skill_level_id}")
    public ResponseEntity<Hr_skill_levelDTO> update(@PathVariable("hr_skill_level_id") Long hr_skill_level_id, @RequestBody Hr_skill_levelDTO hr_skill_leveldto) {
		Hr_skill_level domain  = hr_skill_levelMapping.toDomain(hr_skill_leveldto);
        domain .setId(hr_skill_level_id);
		hr_skill_levelService.update(domain );
		Hr_skill_levelDTO dto = hr_skill_levelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Update-all')")
    @ApiOperation(value = "批量更新技能等级", tags = {"技能等级" },  notes = "批量更新技能等级")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skill_levels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_skill_levelDTO> hr_skill_leveldtos) {
        hr_skill_levelService.updateBatch(hr_skill_levelMapping.toDomain(hr_skill_leveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Remove-all')")
    @ApiOperation(value = "删除技能等级", tags = {"技能等级" },  notes = "删除技能等级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skill_levels/{hr_skill_level_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_skill_level_id") Long hr_skill_level_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_skill_levelService.remove(hr_skill_level_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Remove-all')")
    @ApiOperation(value = "批量删除技能等级", tags = {"技能等级" },  notes = "批量删除技能等级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skill_levels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_skill_levelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Get-all')")
    @ApiOperation(value = "获取技能等级", tags = {"技能等级" },  notes = "获取技能等级")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_skill_levels/{hr_skill_level_id}")
    public ResponseEntity<Hr_skill_levelDTO> get(@PathVariable("hr_skill_level_id") Long hr_skill_level_id) {
        Hr_skill_level domain = hr_skill_levelService.get(hr_skill_level_id);
        Hr_skill_levelDTO dto = hr_skill_levelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取技能等级草稿", tags = {"技能等级" },  notes = "获取技能等级草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_skill_levels/getdraft")
    public ResponseEntity<Hr_skill_levelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_skill_levelMapping.toDto(hr_skill_levelService.getDraft(new Hr_skill_level())));
    }

    @ApiOperation(value = "检查技能等级", tags = {"技能等级" },  notes = "检查技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_levels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_skill_levelDTO hr_skill_leveldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_skill_levelService.checkKey(hr_skill_levelMapping.toDomain(hr_skill_leveldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Save-all')")
    @ApiOperation(value = "保存技能等级", tags = {"技能等级" },  notes = "保存技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_levels/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_skill_levelDTO hr_skill_leveldto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_skill_levelService.save(hr_skill_levelMapping.toDomain(hr_skill_leveldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Save-all')")
    @ApiOperation(value = "批量保存技能等级", tags = {"技能等级" },  notes = "批量保存技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_levels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_skill_levelDTO> hr_skill_leveldtos) {
        hr_skill_levelService.saveBatch(hr_skill_levelMapping.toDomain(hr_skill_leveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"技能等级" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_skill_levels/fetchdefault")
	public ResponseEntity<List<Hr_skill_levelDTO>> fetchDefault(Hr_skill_levelSearchContext context) {
        Page<Hr_skill_level> domains = hr_skill_levelService.searchDefault(context) ;
        List<Hr_skill_levelDTO> list = hr_skill_levelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"技能等级" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_skill_levels/searchdefault")
	public ResponseEntity<Page<Hr_skill_levelDTO>> searchDefault(@RequestBody Hr_skill_levelSearchContext context) {
        Page<Hr_skill_level> domains = hr_skill_levelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_skill_levelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Create-all')")
    @ApiOperation(value = "根据技能类型建立技能等级", tags = {"技能等级" },  notes = "根据技能类型建立技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels")
    public ResponseEntity<Hr_skill_levelDTO> createByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody Hr_skill_levelDTO hr_skill_leveldto) {
        Hr_skill_level domain = hr_skill_levelMapping.toDomain(hr_skill_leveldto);
        domain.setSkillTypeId(hr_skill_type_id);
		hr_skill_levelService.create(domain);
        Hr_skill_levelDTO dto = hr_skill_levelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Create-all')")
    @ApiOperation(value = "根据技能类型批量建立技能等级", tags = {"技能等级" },  notes = "根据技能类型批量建立技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/batch")
    public ResponseEntity<Boolean> createBatchByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody List<Hr_skill_levelDTO> hr_skill_leveldtos) {
        List<Hr_skill_level> domainlist=hr_skill_levelMapping.toDomain(hr_skill_leveldtos);
        for(Hr_skill_level domain:domainlist){
            domain.setSkillTypeId(hr_skill_type_id);
        }
        hr_skill_levelService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Update-all')")
    @ApiOperation(value = "根据技能类型更新技能等级", tags = {"技能等级" },  notes = "根据技能类型更新技能等级")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/{hr_skill_level_id}")
    public ResponseEntity<Hr_skill_levelDTO> updateByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @PathVariable("hr_skill_level_id") Long hr_skill_level_id, @RequestBody Hr_skill_levelDTO hr_skill_leveldto) {
        Hr_skill_level domain = hr_skill_levelMapping.toDomain(hr_skill_leveldto);
        domain.setSkillTypeId(hr_skill_type_id);
        domain.setId(hr_skill_level_id);
		hr_skill_levelService.update(domain);
        Hr_skill_levelDTO dto = hr_skill_levelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Update-all')")
    @ApiOperation(value = "根据技能类型批量更新技能等级", tags = {"技能等级" },  notes = "根据技能类型批量更新技能等级")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/batch")
    public ResponseEntity<Boolean> updateBatchByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody List<Hr_skill_levelDTO> hr_skill_leveldtos) {
        List<Hr_skill_level> domainlist=hr_skill_levelMapping.toDomain(hr_skill_leveldtos);
        for(Hr_skill_level domain:domainlist){
            domain.setSkillTypeId(hr_skill_type_id);
        }
        hr_skill_levelService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Remove-all')")
    @ApiOperation(value = "根据技能类型删除技能等级", tags = {"技能等级" },  notes = "根据技能类型删除技能等级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/{hr_skill_level_id}")
    public ResponseEntity<Boolean> removeByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @PathVariable("hr_skill_level_id") Long hr_skill_level_id) {
		return ResponseEntity.status(HttpStatus.OK).body(hr_skill_levelService.remove(hr_skill_level_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Remove-all')")
    @ApiOperation(value = "根据技能类型批量删除技能等级", tags = {"技能等级" },  notes = "根据技能类型批量删除技能等级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/batch")
    public ResponseEntity<Boolean> removeBatchByHr_skill_type(@RequestBody List<Long> ids) {
        hr_skill_levelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Get-all')")
    @ApiOperation(value = "根据技能类型获取技能等级", tags = {"技能等级" },  notes = "根据技能类型获取技能等级")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/{hr_skill_level_id}")
    public ResponseEntity<Hr_skill_levelDTO> getByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @PathVariable("hr_skill_level_id") Long hr_skill_level_id) {
        Hr_skill_level domain = hr_skill_levelService.get(hr_skill_level_id);
        Hr_skill_levelDTO dto = hr_skill_levelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据技能类型获取技能等级草稿", tags = {"技能等级" },  notes = "根据技能类型获取技能等级草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/getdraft")
    public ResponseEntity<Hr_skill_levelDTO> getDraftByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id) {
        Hr_skill_level domain = new Hr_skill_level();
        domain.setSkillTypeId(hr_skill_type_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_skill_levelMapping.toDto(hr_skill_levelService.getDraft(domain)));
    }

    @ApiOperation(value = "根据技能类型检查技能等级", tags = {"技能等级" },  notes = "根据技能类型检查技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/checkkey")
    public ResponseEntity<Boolean> checkKeyByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody Hr_skill_levelDTO hr_skill_leveldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_skill_levelService.checkKey(hr_skill_levelMapping.toDomain(hr_skill_leveldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Save-all')")
    @ApiOperation(value = "根据技能类型保存技能等级", tags = {"技能等级" },  notes = "根据技能类型保存技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/save")
    public ResponseEntity<Boolean> saveByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody Hr_skill_levelDTO hr_skill_leveldto) {
        Hr_skill_level domain = hr_skill_levelMapping.toDomain(hr_skill_leveldto);
        domain.setSkillTypeId(hr_skill_type_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_skill_levelService.save(domain));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-Save-all')")
    @ApiOperation(value = "根据技能类型批量保存技能等级", tags = {"技能等级" },  notes = "根据技能类型批量保存技能等级")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/savebatch")
    public ResponseEntity<Boolean> saveBatchByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody List<Hr_skill_levelDTO> hr_skill_leveldtos) {
        List<Hr_skill_level> domainlist=hr_skill_levelMapping.toDomain(hr_skill_leveldtos);
        for(Hr_skill_level domain:domainlist){
             domain.setSkillTypeId(hr_skill_type_id);
        }
        hr_skill_levelService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-searchDefault-all')")
	@ApiOperation(value = "根据技能类型获取数据集", tags = {"技能等级" } ,notes = "根据技能类型获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/fetchdefault")
	public ResponseEntity<List<Hr_skill_levelDTO>> fetchHr_skill_levelDefaultByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id,Hr_skill_levelSearchContext context) {
        context.setN_skill_type_id_eq(hr_skill_type_id);
        Page<Hr_skill_level> domains = hr_skill_levelService.searchDefault(context) ;
        List<Hr_skill_levelDTO> list = hr_skill_levelMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill_level-searchDefault-all')")
	@ApiOperation(value = "根据技能类型查询数据集", tags = {"技能等级" } ,notes = "根据技能类型查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_skill_types/{hr_skill_type_id}/hr_skill_levels/searchdefault")
	public ResponseEntity<Page<Hr_skill_levelDTO>> searchHr_skill_levelDefaultByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody Hr_skill_levelSearchContext context) {
        context.setN_skill_type_id_eq(hr_skill_type_id);
        Page<Hr_skill_level> domains = hr_skill_levelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_skill_levelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


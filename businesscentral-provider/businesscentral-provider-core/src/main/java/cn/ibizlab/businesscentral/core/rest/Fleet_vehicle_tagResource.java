package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_tag;
import cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_tagService;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_tagSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"车辆标签" })
@RestController("Core-fleet_vehicle_tag")
@RequestMapping("")
public class Fleet_vehicle_tagResource {

    @Autowired
    public IFleet_vehicle_tagService fleet_vehicle_tagService;

    @Autowired
    @Lazy
    public Fleet_vehicle_tagMapping fleet_vehicle_tagMapping;

    @PreAuthorize("hasPermission(this.fleet_vehicle_tagMapping.toDomain(#fleet_vehicle_tagdto),'iBizBusinessCentral-Fleet_vehicle_tag-Create')")
    @ApiOperation(value = "新建车辆标签", tags = {"车辆标签" },  notes = "新建车辆标签")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags")
    public ResponseEntity<Fleet_vehicle_tagDTO> create(@Validated @RequestBody Fleet_vehicle_tagDTO fleet_vehicle_tagdto) {
        Fleet_vehicle_tag domain = fleet_vehicle_tagMapping.toDomain(fleet_vehicle_tagdto);
		fleet_vehicle_tagService.create(domain);
        Fleet_vehicle_tagDTO dto = fleet_vehicle_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_tagMapping.toDomain(#fleet_vehicle_tagdtos),'iBizBusinessCentral-Fleet_vehicle_tag-Create')")
    @ApiOperation(value = "批量新建车辆标签", tags = {"车辆标签" },  notes = "批量新建车辆标签")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_tagDTO> fleet_vehicle_tagdtos) {
        fleet_vehicle_tagService.createBatch(fleet_vehicle_tagMapping.toDomain(fleet_vehicle_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "fleet_vehicle_tag" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.fleet_vehicle_tagService.get(#fleet_vehicle_tag_id),'iBizBusinessCentral-Fleet_vehicle_tag-Update')")
    @ApiOperation(value = "更新车辆标签", tags = {"车辆标签" },  notes = "更新车辆标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_tags/{fleet_vehicle_tag_id}")
    public ResponseEntity<Fleet_vehicle_tagDTO> update(@PathVariable("fleet_vehicle_tag_id") Long fleet_vehicle_tag_id, @RequestBody Fleet_vehicle_tagDTO fleet_vehicle_tagdto) {
		Fleet_vehicle_tag domain  = fleet_vehicle_tagMapping.toDomain(fleet_vehicle_tagdto);
        domain .setId(fleet_vehicle_tag_id);
		fleet_vehicle_tagService.update(domain );
		Fleet_vehicle_tagDTO dto = fleet_vehicle_tagMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_tagService.getFleetVehicleTagByEntities(this.fleet_vehicle_tagMapping.toDomain(#fleet_vehicle_tagdtos)),'iBizBusinessCentral-Fleet_vehicle_tag-Update')")
    @ApiOperation(value = "批量更新车辆标签", tags = {"车辆标签" },  notes = "批量更新车辆标签")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_tagDTO> fleet_vehicle_tagdtos) {
        fleet_vehicle_tagService.updateBatch(fleet_vehicle_tagMapping.toDomain(fleet_vehicle_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_tagService.get(#fleet_vehicle_tag_id),'iBizBusinessCentral-Fleet_vehicle_tag-Remove')")
    @ApiOperation(value = "删除车辆标签", tags = {"车辆标签" },  notes = "删除车辆标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_tags/{fleet_vehicle_tag_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_tag_id") Long fleet_vehicle_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_tagService.remove(fleet_vehicle_tag_id));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_tagService.getFleetVehicleTagByIds(#ids),'iBizBusinessCentral-Fleet_vehicle_tag-Remove')")
    @ApiOperation(value = "批量删除车辆标签", tags = {"车辆标签" },  notes = "批量删除车辆标签")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        fleet_vehicle_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.fleet_vehicle_tagMapping.toDomain(returnObject.body),'iBizBusinessCentral-Fleet_vehicle_tag-Get')")
    @ApiOperation(value = "获取车辆标签", tags = {"车辆标签" },  notes = "获取车辆标签")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_tags/{fleet_vehicle_tag_id}")
    public ResponseEntity<Fleet_vehicle_tagDTO> get(@PathVariable("fleet_vehicle_tag_id") Long fleet_vehicle_tag_id) {
        Fleet_vehicle_tag domain = fleet_vehicle_tagService.get(fleet_vehicle_tag_id);
        Fleet_vehicle_tagDTO dto = fleet_vehicle_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取车辆标签草稿", tags = {"车辆标签" },  notes = "获取车辆标签草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_tags/getdraft")
    public ResponseEntity<Fleet_vehicle_tagDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_tagMapping.toDto(fleet_vehicle_tagService.getDraft(new Fleet_vehicle_tag())));
    }

    @ApiOperation(value = "检查车辆标签", tags = {"车辆标签" },  notes = "检查车辆标签")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Fleet_vehicle_tagDTO fleet_vehicle_tagdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_tagService.checkKey(fleet_vehicle_tagMapping.toDomain(fleet_vehicle_tagdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_tagMapping.toDomain(#fleet_vehicle_tagdto),'iBizBusinessCentral-Fleet_vehicle_tag-Save')")
    @ApiOperation(value = "保存车辆标签", tags = {"车辆标签" },  notes = "保存车辆标签")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/save")
    public ResponseEntity<Boolean> save(@RequestBody Fleet_vehicle_tagDTO fleet_vehicle_tagdto) {
        return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_tagService.save(fleet_vehicle_tagMapping.toDomain(fleet_vehicle_tagdto)));
    }

    @PreAuthorize("hasPermission(this.fleet_vehicle_tagMapping.toDomain(#fleet_vehicle_tagdtos),'iBizBusinessCentral-Fleet_vehicle_tag-Save')")
    @ApiOperation(value = "批量保存车辆标签", tags = {"车辆标签" },  notes = "批量保存车辆标签")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_tags/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Fleet_vehicle_tagDTO> fleet_vehicle_tagdtos) {
        fleet_vehicle_tagService.saveBatch(fleet_vehicle_tagMapping.toDomain(fleet_vehicle_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_tag-Get')")
	@ApiOperation(value = "获取数据集", tags = {"车辆标签" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_tags/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_tagDTO>> fetchDefault(Fleet_vehicle_tagSearchContext context) {
        Page<Fleet_vehicle_tag> domains = fleet_vehicle_tagService.searchDefault(context) ;
        List<Fleet_vehicle_tagDTO> list = fleet_vehicle_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Fleet_vehicle_tag-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Fleet_vehicle_tag-Get')")
	@ApiOperation(value = "查询数据集", tags = {"车辆标签" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/fleet_vehicle_tags/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_tagDTO>> searchDefault(@RequestBody Fleet_vehicle_tagSearchContext context) {
        Page<Fleet_vehicle_tag> domains = fleet_vehicle_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


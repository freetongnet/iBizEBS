package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_redirectService;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_redirectSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"网站重定向" })
@RestController("Core-website_redirect")
@RequestMapping("")
public class Website_redirectResource {

    @Autowired
    public IWebsite_redirectService website_redirectService;

    @Autowired
    @Lazy
    public Website_redirectMapping website_redirectMapping;

    @PreAuthorize("hasPermission(this.website_redirectMapping.toDomain(#website_redirectdto),'iBizBusinessCentral-Website_redirect-Create')")
    @ApiOperation(value = "新建网站重定向", tags = {"网站重定向" },  notes = "新建网站重定向")
	@RequestMapping(method = RequestMethod.POST, value = "/website_redirects")
    public ResponseEntity<Website_redirectDTO> create(@Validated @RequestBody Website_redirectDTO website_redirectdto) {
        Website_redirect domain = website_redirectMapping.toDomain(website_redirectdto);
		website_redirectService.create(domain);
        Website_redirectDTO dto = website_redirectMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.website_redirectMapping.toDomain(#website_redirectdtos),'iBizBusinessCentral-Website_redirect-Create')")
    @ApiOperation(value = "批量新建网站重定向", tags = {"网站重定向" },  notes = "批量新建网站重定向")
	@RequestMapping(method = RequestMethod.POST, value = "/website_redirects/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_redirectDTO> website_redirectdtos) {
        website_redirectService.createBatch(website_redirectMapping.toDomain(website_redirectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "website_redirect" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.website_redirectService.get(#website_redirect_id),'iBizBusinessCentral-Website_redirect-Update')")
    @ApiOperation(value = "更新网站重定向", tags = {"网站重定向" },  notes = "更新网站重定向")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_redirects/{website_redirect_id}")
    public ResponseEntity<Website_redirectDTO> update(@PathVariable("website_redirect_id") Long website_redirect_id, @RequestBody Website_redirectDTO website_redirectdto) {
		Website_redirect domain  = website_redirectMapping.toDomain(website_redirectdto);
        domain .setId(website_redirect_id);
		website_redirectService.update(domain );
		Website_redirectDTO dto = website_redirectMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.website_redirectService.getWebsiteRedirectByEntities(this.website_redirectMapping.toDomain(#website_redirectdtos)),'iBizBusinessCentral-Website_redirect-Update')")
    @ApiOperation(value = "批量更新网站重定向", tags = {"网站重定向" },  notes = "批量更新网站重定向")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_redirects/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_redirectDTO> website_redirectdtos) {
        website_redirectService.updateBatch(website_redirectMapping.toDomain(website_redirectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.website_redirectService.get(#website_redirect_id),'iBizBusinessCentral-Website_redirect-Remove')")
    @ApiOperation(value = "删除网站重定向", tags = {"网站重定向" },  notes = "删除网站重定向")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_redirects/{website_redirect_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("website_redirect_id") Long website_redirect_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_redirectService.remove(website_redirect_id));
    }

    @PreAuthorize("hasPermission(this.website_redirectService.getWebsiteRedirectByIds(#ids),'iBizBusinessCentral-Website_redirect-Remove')")
    @ApiOperation(value = "批量删除网站重定向", tags = {"网站重定向" },  notes = "批量删除网站重定向")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_redirects/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        website_redirectService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.website_redirectMapping.toDomain(returnObject.body),'iBizBusinessCentral-Website_redirect-Get')")
    @ApiOperation(value = "获取网站重定向", tags = {"网站重定向" },  notes = "获取网站重定向")
	@RequestMapping(method = RequestMethod.GET, value = "/website_redirects/{website_redirect_id}")
    public ResponseEntity<Website_redirectDTO> get(@PathVariable("website_redirect_id") Long website_redirect_id) {
        Website_redirect domain = website_redirectService.get(website_redirect_id);
        Website_redirectDTO dto = website_redirectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取网站重定向草稿", tags = {"网站重定向" },  notes = "获取网站重定向草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/website_redirects/getdraft")
    public ResponseEntity<Website_redirectDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(website_redirectMapping.toDto(website_redirectService.getDraft(new Website_redirect())));
    }

    @ApiOperation(value = "检查网站重定向", tags = {"网站重定向" },  notes = "检查网站重定向")
	@RequestMapping(method = RequestMethod.POST, value = "/website_redirects/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Website_redirectDTO website_redirectdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(website_redirectService.checkKey(website_redirectMapping.toDomain(website_redirectdto)));
    }

    @PreAuthorize("hasPermission(this.website_redirectMapping.toDomain(#website_redirectdto),'iBizBusinessCentral-Website_redirect-Save')")
    @ApiOperation(value = "保存网站重定向", tags = {"网站重定向" },  notes = "保存网站重定向")
	@RequestMapping(method = RequestMethod.POST, value = "/website_redirects/save")
    public ResponseEntity<Boolean> save(@RequestBody Website_redirectDTO website_redirectdto) {
        return ResponseEntity.status(HttpStatus.OK).body(website_redirectService.save(website_redirectMapping.toDomain(website_redirectdto)));
    }

    @PreAuthorize("hasPermission(this.website_redirectMapping.toDomain(#website_redirectdtos),'iBizBusinessCentral-Website_redirect-Save')")
    @ApiOperation(value = "批量保存网站重定向", tags = {"网站重定向" },  notes = "批量保存网站重定向")
	@RequestMapping(method = RequestMethod.POST, value = "/website_redirects/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Website_redirectDTO> website_redirectdtos) {
        website_redirectService.saveBatch(website_redirectMapping.toDomain(website_redirectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_redirect-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Website_redirect-Get')")
	@ApiOperation(value = "获取数据集", tags = {"网站重定向" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/website_redirects/fetchdefault")
	public ResponseEntity<List<Website_redirectDTO>> fetchDefault(Website_redirectSearchContext context) {
        Page<Website_redirect> domains = website_redirectService.searchDefault(context) ;
        List<Website_redirectDTO> list = website_redirectMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_redirect-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Website_redirect-Get')")
	@ApiOperation(value = "查询数据集", tags = {"网站重定向" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/website_redirects/searchdefault")
	public ResponseEntity<Page<Website_redirectDTO>> searchDefault(@RequestBody Website_redirectSearchContext context) {
        Page<Website_redirect> domains = website_redirectService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_redirectMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_price_history;
import cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_price_historyService;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_price_historySearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"产品价格历史列表" })
@RestController("Core-product_price_history")
@RequestMapping("")
public class Product_price_historyResource {

    @Autowired
    public IProduct_price_historyService product_price_historyService;

    @Autowired
    @Lazy
    public Product_price_historyMapping product_price_historyMapping;

    @PreAuthorize("hasPermission(this.product_price_historyMapping.toDomain(#product_price_historydto),'iBizBusinessCentral-Product_price_history-Create')")
    @ApiOperation(value = "新建产品价格历史列表", tags = {"产品价格历史列表" },  notes = "新建产品价格历史列表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_histories")
    public ResponseEntity<Product_price_historyDTO> create(@Validated @RequestBody Product_price_historyDTO product_price_historydto) {
        Product_price_history domain = product_price_historyMapping.toDomain(product_price_historydto);
		product_price_historyService.create(domain);
        Product_price_historyDTO dto = product_price_historyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_price_historyMapping.toDomain(#product_price_historydtos),'iBizBusinessCentral-Product_price_history-Create')")
    @ApiOperation(value = "批量新建产品价格历史列表", tags = {"产品价格历史列表" },  notes = "批量新建产品价格历史列表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_histories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_price_historyDTO> product_price_historydtos) {
        product_price_historyService.createBatch(product_price_historyMapping.toDomain(product_price_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "product_price_history" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.product_price_historyService.get(#product_price_history_id),'iBizBusinessCentral-Product_price_history-Update')")
    @ApiOperation(value = "更新产品价格历史列表", tags = {"产品价格历史列表" },  notes = "更新产品价格历史列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_histories/{product_price_history_id}")
    public ResponseEntity<Product_price_historyDTO> update(@PathVariable("product_price_history_id") Long product_price_history_id, @RequestBody Product_price_historyDTO product_price_historydto) {
		Product_price_history domain  = product_price_historyMapping.toDomain(product_price_historydto);
        domain .setId(product_price_history_id);
		product_price_historyService.update(domain );
		Product_price_historyDTO dto = product_price_historyMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.product_price_historyService.getProductPriceHistoryByEntities(this.product_price_historyMapping.toDomain(#product_price_historydtos)),'iBizBusinessCentral-Product_price_history-Update')")
    @ApiOperation(value = "批量更新产品价格历史列表", tags = {"产品价格历史列表" },  notes = "批量更新产品价格历史列表")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_price_histories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_price_historyDTO> product_price_historydtos) {
        product_price_historyService.updateBatch(product_price_historyMapping.toDomain(product_price_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.product_price_historyService.get(#product_price_history_id),'iBizBusinessCentral-Product_price_history-Remove')")
    @ApiOperation(value = "删除产品价格历史列表", tags = {"产品价格历史列表" },  notes = "删除产品价格历史列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_histories/{product_price_history_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("product_price_history_id") Long product_price_history_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_price_historyService.remove(product_price_history_id));
    }

    @PreAuthorize("hasPermission(this.product_price_historyService.getProductPriceHistoryByIds(#ids),'iBizBusinessCentral-Product_price_history-Remove')")
    @ApiOperation(value = "批量删除产品价格历史列表", tags = {"产品价格历史列表" },  notes = "批量删除产品价格历史列表")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_price_histories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        product_price_historyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.product_price_historyMapping.toDomain(returnObject.body),'iBizBusinessCentral-Product_price_history-Get')")
    @ApiOperation(value = "获取产品价格历史列表", tags = {"产品价格历史列表" },  notes = "获取产品价格历史列表")
	@RequestMapping(method = RequestMethod.GET, value = "/product_price_histories/{product_price_history_id}")
    public ResponseEntity<Product_price_historyDTO> get(@PathVariable("product_price_history_id") Long product_price_history_id) {
        Product_price_history domain = product_price_historyService.get(product_price_history_id);
        Product_price_historyDTO dto = product_price_historyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取产品价格历史列表草稿", tags = {"产品价格历史列表" },  notes = "获取产品价格历史列表草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/product_price_histories/getdraft")
    public ResponseEntity<Product_price_historyDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(product_price_historyMapping.toDto(product_price_historyService.getDraft(new Product_price_history())));
    }

    @ApiOperation(value = "检查产品价格历史列表", tags = {"产品价格历史列表" },  notes = "检查产品价格历史列表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_histories/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Product_price_historyDTO product_price_historydto) {
        return  ResponseEntity.status(HttpStatus.OK).body(product_price_historyService.checkKey(product_price_historyMapping.toDomain(product_price_historydto)));
    }

    @PreAuthorize("hasPermission(this.product_price_historyMapping.toDomain(#product_price_historydto),'iBizBusinessCentral-Product_price_history-Save')")
    @ApiOperation(value = "保存产品价格历史列表", tags = {"产品价格历史列表" },  notes = "保存产品价格历史列表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_histories/save")
    public ResponseEntity<Boolean> save(@RequestBody Product_price_historyDTO product_price_historydto) {
        return ResponseEntity.status(HttpStatus.OK).body(product_price_historyService.save(product_price_historyMapping.toDomain(product_price_historydto)));
    }

    @PreAuthorize("hasPermission(this.product_price_historyMapping.toDomain(#product_price_historydtos),'iBizBusinessCentral-Product_price_history-Save')")
    @ApiOperation(value = "批量保存产品价格历史列表", tags = {"产品价格历史列表" },  notes = "批量保存产品价格历史列表")
	@RequestMapping(method = RequestMethod.POST, value = "/product_price_histories/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Product_price_historyDTO> product_price_historydtos) {
        product_price_historyService.saveBatch(product_price_historyMapping.toDomain(product_price_historydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_price_history-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_price_history-Get')")
	@ApiOperation(value = "获取数据集", tags = {"产品价格历史列表" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/product_price_histories/fetchdefault")
	public ResponseEntity<List<Product_price_historyDTO>> fetchDefault(Product_price_historySearchContext context) {
        Page<Product_price_history> domains = product_price_historyService.searchDefault(context) ;
        List<Product_price_historyDTO> list = product_price_historyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Product_price_history-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Product_price_history-Get')")
	@ApiOperation(value = "查询数据集", tags = {"产品价格历史列表" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/product_price_histories/searchdefault")
	public ResponseEntity<Page<Product_price_historyDTO>> searchDefault(@RequestBody Product_price_historySearchContext context) {
        Page<Product_price_history> domains = product_price_historyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_price_historyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


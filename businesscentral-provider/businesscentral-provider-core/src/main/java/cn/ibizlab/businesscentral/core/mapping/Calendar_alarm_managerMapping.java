package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.businesscentral.core.dto.Calendar_alarm_managerDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreCalendar_alarm_managerMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Calendar_alarm_managerMapping extends MappingBase<Calendar_alarm_managerDTO, Calendar_alarm_manager> {


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_package_level;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_levelService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_package_levelSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"库存包装层级" })
@RestController("Core-stock_package_level")
@RequestMapping("")
public class Stock_package_levelResource {

    @Autowired
    public IStock_package_levelService stock_package_levelService;

    @Autowired
    @Lazy
    public Stock_package_levelMapping stock_package_levelMapping;

    @PreAuthorize("hasPermission(this.stock_package_levelMapping.toDomain(#stock_package_leveldto),'iBizBusinessCentral-Stock_package_level-Create')")
    @ApiOperation(value = "新建库存包装层级", tags = {"库存包装层级" },  notes = "新建库存包装层级")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels")
    public ResponseEntity<Stock_package_levelDTO> create(@Validated @RequestBody Stock_package_levelDTO stock_package_leveldto) {
        Stock_package_level domain = stock_package_levelMapping.toDomain(stock_package_leveldto);
		stock_package_levelService.create(domain);
        Stock_package_levelDTO dto = stock_package_levelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_package_levelMapping.toDomain(#stock_package_leveldtos),'iBizBusinessCentral-Stock_package_level-Create')")
    @ApiOperation(value = "批量新建库存包装层级", tags = {"库存包装层级" },  notes = "批量新建库存包装层级")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_package_levelDTO> stock_package_leveldtos) {
        stock_package_levelService.createBatch(stock_package_levelMapping.toDomain(stock_package_leveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "stock_package_level" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.stock_package_levelService.get(#stock_package_level_id),'iBizBusinessCentral-Stock_package_level-Update')")
    @ApiOperation(value = "更新库存包装层级", tags = {"库存包装层级" },  notes = "更新库存包装层级")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_package_levels/{stock_package_level_id}")
    public ResponseEntity<Stock_package_levelDTO> update(@PathVariable("stock_package_level_id") Long stock_package_level_id, @RequestBody Stock_package_levelDTO stock_package_leveldto) {
		Stock_package_level domain  = stock_package_levelMapping.toDomain(stock_package_leveldto);
        domain .setId(stock_package_level_id);
		stock_package_levelService.update(domain );
		Stock_package_levelDTO dto = stock_package_levelMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.stock_package_levelService.getStockPackageLevelByEntities(this.stock_package_levelMapping.toDomain(#stock_package_leveldtos)),'iBizBusinessCentral-Stock_package_level-Update')")
    @ApiOperation(value = "批量更新库存包装层级", tags = {"库存包装层级" },  notes = "批量更新库存包装层级")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_package_levels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_package_levelDTO> stock_package_leveldtos) {
        stock_package_levelService.updateBatch(stock_package_levelMapping.toDomain(stock_package_leveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.stock_package_levelService.get(#stock_package_level_id),'iBizBusinessCentral-Stock_package_level-Remove')")
    @ApiOperation(value = "删除库存包装层级", tags = {"库存包装层级" },  notes = "删除库存包装层级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_levels/{stock_package_level_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("stock_package_level_id") Long stock_package_level_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_package_levelService.remove(stock_package_level_id));
    }

    @PreAuthorize("hasPermission(this.stock_package_levelService.getStockPackageLevelByIds(#ids),'iBizBusinessCentral-Stock_package_level-Remove')")
    @ApiOperation(value = "批量删除库存包装层级", tags = {"库存包装层级" },  notes = "批量删除库存包装层级")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_package_levels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        stock_package_levelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.stock_package_levelMapping.toDomain(returnObject.body),'iBizBusinessCentral-Stock_package_level-Get')")
    @ApiOperation(value = "获取库存包装层级", tags = {"库存包装层级" },  notes = "获取库存包装层级")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_package_levels/{stock_package_level_id}")
    public ResponseEntity<Stock_package_levelDTO> get(@PathVariable("stock_package_level_id") Long stock_package_level_id) {
        Stock_package_level domain = stock_package_levelService.get(stock_package_level_id);
        Stock_package_levelDTO dto = stock_package_levelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取库存包装层级草稿", tags = {"库存包装层级" },  notes = "获取库存包装层级草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_package_levels/getdraft")
    public ResponseEntity<Stock_package_levelDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(stock_package_levelMapping.toDto(stock_package_levelService.getDraft(new Stock_package_level())));
    }

    @ApiOperation(value = "检查库存包装层级", tags = {"库存包装层级" },  notes = "检查库存包装层级")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Stock_package_levelDTO stock_package_leveldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(stock_package_levelService.checkKey(stock_package_levelMapping.toDomain(stock_package_leveldto)));
    }

    @PreAuthorize("hasPermission(this.stock_package_levelMapping.toDomain(#stock_package_leveldto),'iBizBusinessCentral-Stock_package_level-Save')")
    @ApiOperation(value = "保存库存包装层级", tags = {"库存包装层级" },  notes = "保存库存包装层级")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels/save")
    public ResponseEntity<Boolean> save(@RequestBody Stock_package_levelDTO stock_package_leveldto) {
        return ResponseEntity.status(HttpStatus.OK).body(stock_package_levelService.save(stock_package_levelMapping.toDomain(stock_package_leveldto)));
    }

    @PreAuthorize("hasPermission(this.stock_package_levelMapping.toDomain(#stock_package_leveldtos),'iBizBusinessCentral-Stock_package_level-Save')")
    @ApiOperation(value = "批量保存库存包装层级", tags = {"库存包装层级" },  notes = "批量保存库存包装层级")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_package_levels/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Stock_package_levelDTO> stock_package_leveldtos) {
        stock_package_levelService.saveBatch(stock_package_levelMapping.toDomain(stock_package_leveldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_package_level-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_package_level-Get')")
	@ApiOperation(value = "获取数据集", tags = {"库存包装层级" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/stock_package_levels/fetchdefault")
	public ResponseEntity<List<Stock_package_levelDTO>> fetchDefault(Stock_package_levelSearchContext context) {
        Page<Stock_package_level> domains = stock_package_levelService.searchDefault(context) ;
        List<Stock_package_levelDTO> list = stock_package_levelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Stock_package_level-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Stock_package_level-Get')")
	@ApiOperation(value = "查询数据集", tags = {"库存包装层级" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/stock_package_levels/searchdefault")
	public ResponseEntity<Page<Stock_package_levelDTO>> searchDefault(@RequestBody Stock_package_levelSearchContext context) {
        Page<Stock_package_level> domains = stock_package_levelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_package_levelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


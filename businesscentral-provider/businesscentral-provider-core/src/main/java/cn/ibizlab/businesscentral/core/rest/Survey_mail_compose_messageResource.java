package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_mail_compose_messageService;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"调查的功能EMail撰写向导" })
@RestController("Core-survey_mail_compose_message")
@RequestMapping("")
public class Survey_mail_compose_messageResource {

    @Autowired
    public ISurvey_mail_compose_messageService survey_mail_compose_messageService;

    @Autowired
    @Lazy
    public Survey_mail_compose_messageMapping survey_mail_compose_messageMapping;

    @PreAuthorize("hasPermission(this.survey_mail_compose_messageMapping.toDomain(#survey_mail_compose_messagedto),'iBizBusinessCentral-Survey_mail_compose_message-Create')")
    @ApiOperation(value = "新建调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "新建调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages")
    public ResponseEntity<Survey_mail_compose_messageDTO> create(@Validated @RequestBody Survey_mail_compose_messageDTO survey_mail_compose_messagedto) {
        Survey_mail_compose_message domain = survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedto);
		survey_mail_compose_messageService.create(domain);
        Survey_mail_compose_messageDTO dto = survey_mail_compose_messageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_mail_compose_messageMapping.toDomain(#survey_mail_compose_messagedtos),'iBizBusinessCentral-Survey_mail_compose_message-Create')")
    @ApiOperation(value = "批量新建调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "批量新建调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_mail_compose_messageDTO> survey_mail_compose_messagedtos) {
        survey_mail_compose_messageService.createBatch(survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "survey_mail_compose_message" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.survey_mail_compose_messageService.get(#survey_mail_compose_message_id),'iBizBusinessCentral-Survey_mail_compose_message-Update')")
    @ApiOperation(value = "更新调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "更新调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_mail_compose_messages/{survey_mail_compose_message_id}")
    public ResponseEntity<Survey_mail_compose_messageDTO> update(@PathVariable("survey_mail_compose_message_id") Long survey_mail_compose_message_id, @RequestBody Survey_mail_compose_messageDTO survey_mail_compose_messagedto) {
		Survey_mail_compose_message domain  = survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedto);
        domain .setId(survey_mail_compose_message_id);
		survey_mail_compose_messageService.update(domain );
		Survey_mail_compose_messageDTO dto = survey_mail_compose_messageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_mail_compose_messageService.getSurveyMailComposeMessageByEntities(this.survey_mail_compose_messageMapping.toDomain(#survey_mail_compose_messagedtos)),'iBizBusinessCentral-Survey_mail_compose_message-Update')")
    @ApiOperation(value = "批量更新调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "批量更新调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_mail_compose_messages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_mail_compose_messageDTO> survey_mail_compose_messagedtos) {
        survey_mail_compose_messageService.updateBatch(survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.survey_mail_compose_messageService.get(#survey_mail_compose_message_id),'iBizBusinessCentral-Survey_mail_compose_message-Remove')")
    @ApiOperation(value = "删除调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "删除调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_mail_compose_messages/{survey_mail_compose_message_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("survey_mail_compose_message_id") Long survey_mail_compose_message_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_mail_compose_messageService.remove(survey_mail_compose_message_id));
    }

    @PreAuthorize("hasPermission(this.survey_mail_compose_messageService.getSurveyMailComposeMessageByIds(#ids),'iBizBusinessCentral-Survey_mail_compose_message-Remove')")
    @ApiOperation(value = "批量删除调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "批量删除调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_mail_compose_messages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        survey_mail_compose_messageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.survey_mail_compose_messageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Survey_mail_compose_message-Get')")
    @ApiOperation(value = "获取调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "获取调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_mail_compose_messages/{survey_mail_compose_message_id}")
    public ResponseEntity<Survey_mail_compose_messageDTO> get(@PathVariable("survey_mail_compose_message_id") Long survey_mail_compose_message_id) {
        Survey_mail_compose_message domain = survey_mail_compose_messageService.get(survey_mail_compose_message_id);
        Survey_mail_compose_messageDTO dto = survey_mail_compose_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取调查的功能EMail撰写向导草稿", tags = {"调查的功能EMail撰写向导" },  notes = "获取调查的功能EMail撰写向导草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_mail_compose_messages/getdraft")
    public ResponseEntity<Survey_mail_compose_messageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(survey_mail_compose_messageMapping.toDto(survey_mail_compose_messageService.getDraft(new Survey_mail_compose_message())));
    }

    @ApiOperation(value = "检查调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "检查调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Survey_mail_compose_messageDTO survey_mail_compose_messagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(survey_mail_compose_messageService.checkKey(survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedto)));
    }

    @PreAuthorize("hasPermission(this.survey_mail_compose_messageMapping.toDomain(#survey_mail_compose_messagedto),'iBizBusinessCentral-Survey_mail_compose_message-Save')")
    @ApiOperation(value = "保存调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "保存调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/save")
    public ResponseEntity<Boolean> save(@RequestBody Survey_mail_compose_messageDTO survey_mail_compose_messagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(survey_mail_compose_messageService.save(survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedto)));
    }

    @PreAuthorize("hasPermission(this.survey_mail_compose_messageMapping.toDomain(#survey_mail_compose_messagedtos),'iBizBusinessCentral-Survey_mail_compose_message-Save')")
    @ApiOperation(value = "批量保存调查的功能EMail撰写向导", tags = {"调查的功能EMail撰写向导" },  notes = "批量保存调查的功能EMail撰写向导")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_mail_compose_messages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Survey_mail_compose_messageDTO> survey_mail_compose_messagedtos) {
        survey_mail_compose_messageService.saveBatch(survey_mail_compose_messageMapping.toDomain(survey_mail_compose_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_mail_compose_message-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_mail_compose_message-Get')")
	@ApiOperation(value = "获取数据集", tags = {"调查的功能EMail撰写向导" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/survey_mail_compose_messages/fetchdefault")
	public ResponseEntity<List<Survey_mail_compose_messageDTO>> fetchDefault(Survey_mail_compose_messageSearchContext context) {
        Page<Survey_mail_compose_message> domains = survey_mail_compose_messageService.searchDefault(context) ;
        List<Survey_mail_compose_messageDTO> list = survey_mail_compose_messageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_mail_compose_message-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_mail_compose_message-Get')")
	@ApiOperation(value = "查询数据集", tags = {"调查的功能EMail撰写向导" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/survey_mail_compose_messages/searchdefault")
	public ResponseEntity<Page<Survey_mail_compose_messageDTO>> searchDefault(@RequestBody Survey_mail_compose_messageSearchContext context) {
        Page<Survey_mail_compose_message> domains = survey_mail_compose_messageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_mail_compose_messageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.businesscentral.core.odoo_portal.service.IPortal_wizard_userService;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_wizard_userSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"门户用户配置" })
@RestController("Core-portal_wizard_user")
@RequestMapping("")
public class Portal_wizard_userResource {

    @Autowired
    public IPortal_wizard_userService portal_wizard_userService;

    @Autowired
    @Lazy
    public Portal_wizard_userMapping portal_wizard_userMapping;

    @PreAuthorize("hasPermission(this.portal_wizard_userMapping.toDomain(#portal_wizard_userdto),'iBizBusinessCentral-Portal_wizard_user-Create')")
    @ApiOperation(value = "新建门户用户配置", tags = {"门户用户配置" },  notes = "新建门户用户配置")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users")
    public ResponseEntity<Portal_wizard_userDTO> create(@Validated @RequestBody Portal_wizard_userDTO portal_wizard_userdto) {
        Portal_wizard_user domain = portal_wizard_userMapping.toDomain(portal_wizard_userdto);
		portal_wizard_userService.create(domain);
        Portal_wizard_userDTO dto = portal_wizard_userMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.portal_wizard_userMapping.toDomain(#portal_wizard_userdtos),'iBizBusinessCentral-Portal_wizard_user-Create')")
    @ApiOperation(value = "批量新建门户用户配置", tags = {"门户用户配置" },  notes = "批量新建门户用户配置")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Portal_wizard_userDTO> portal_wizard_userdtos) {
        portal_wizard_userService.createBatch(portal_wizard_userMapping.toDomain(portal_wizard_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "portal_wizard_user" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.portal_wizard_userService.get(#portal_wizard_user_id),'iBizBusinessCentral-Portal_wizard_user-Update')")
    @ApiOperation(value = "更新门户用户配置", tags = {"门户用户配置" },  notes = "更新门户用户配置")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_wizard_users/{portal_wizard_user_id}")
    public ResponseEntity<Portal_wizard_userDTO> update(@PathVariable("portal_wizard_user_id") Long portal_wizard_user_id, @RequestBody Portal_wizard_userDTO portal_wizard_userdto) {
		Portal_wizard_user domain  = portal_wizard_userMapping.toDomain(portal_wizard_userdto);
        domain .setId(portal_wizard_user_id);
		portal_wizard_userService.update(domain );
		Portal_wizard_userDTO dto = portal_wizard_userMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.portal_wizard_userService.getPortalWizardUserByEntities(this.portal_wizard_userMapping.toDomain(#portal_wizard_userdtos)),'iBizBusinessCentral-Portal_wizard_user-Update')")
    @ApiOperation(value = "批量更新门户用户配置", tags = {"门户用户配置" },  notes = "批量更新门户用户配置")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_wizard_users/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_wizard_userDTO> portal_wizard_userdtos) {
        portal_wizard_userService.updateBatch(portal_wizard_userMapping.toDomain(portal_wizard_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.portal_wizard_userService.get(#portal_wizard_user_id),'iBizBusinessCentral-Portal_wizard_user-Remove')")
    @ApiOperation(value = "删除门户用户配置", tags = {"门户用户配置" },  notes = "删除门户用户配置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizard_users/{portal_wizard_user_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("portal_wizard_user_id") Long portal_wizard_user_id) {
         return ResponseEntity.status(HttpStatus.OK).body(portal_wizard_userService.remove(portal_wizard_user_id));
    }

    @PreAuthorize("hasPermission(this.portal_wizard_userService.getPortalWizardUserByIds(#ids),'iBizBusinessCentral-Portal_wizard_user-Remove')")
    @ApiOperation(value = "批量删除门户用户配置", tags = {"门户用户配置" },  notes = "批量删除门户用户配置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizard_users/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        portal_wizard_userService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.portal_wizard_userMapping.toDomain(returnObject.body),'iBizBusinessCentral-Portal_wizard_user-Get')")
    @ApiOperation(value = "获取门户用户配置", tags = {"门户用户配置" },  notes = "获取门户用户配置")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_wizard_users/{portal_wizard_user_id}")
    public ResponseEntity<Portal_wizard_userDTO> get(@PathVariable("portal_wizard_user_id") Long portal_wizard_user_id) {
        Portal_wizard_user domain = portal_wizard_userService.get(portal_wizard_user_id);
        Portal_wizard_userDTO dto = portal_wizard_userMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取门户用户配置草稿", tags = {"门户用户配置" },  notes = "获取门户用户配置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_wizard_users/getdraft")
    public ResponseEntity<Portal_wizard_userDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(portal_wizard_userMapping.toDto(portal_wizard_userService.getDraft(new Portal_wizard_user())));
    }

    @ApiOperation(value = "检查门户用户配置", tags = {"门户用户配置" },  notes = "检查门户用户配置")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Portal_wizard_userDTO portal_wizard_userdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(portal_wizard_userService.checkKey(portal_wizard_userMapping.toDomain(portal_wizard_userdto)));
    }

    @PreAuthorize("hasPermission(this.portal_wizard_userMapping.toDomain(#portal_wizard_userdto),'iBizBusinessCentral-Portal_wizard_user-Save')")
    @ApiOperation(value = "保存门户用户配置", tags = {"门户用户配置" },  notes = "保存门户用户配置")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/save")
    public ResponseEntity<Boolean> save(@RequestBody Portal_wizard_userDTO portal_wizard_userdto) {
        return ResponseEntity.status(HttpStatus.OK).body(portal_wizard_userService.save(portal_wizard_userMapping.toDomain(portal_wizard_userdto)));
    }

    @PreAuthorize("hasPermission(this.portal_wizard_userMapping.toDomain(#portal_wizard_userdtos),'iBizBusinessCentral-Portal_wizard_user-Save')")
    @ApiOperation(value = "批量保存门户用户配置", tags = {"门户用户配置" },  notes = "批量保存门户用户配置")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Portal_wizard_userDTO> portal_wizard_userdtos) {
        portal_wizard_userService.saveBatch(portal_wizard_userMapping.toDomain(portal_wizard_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_wizard_user-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Portal_wizard_user-Get')")
	@ApiOperation(value = "获取数据集", tags = {"门户用户配置" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/portal_wizard_users/fetchdefault")
	public ResponseEntity<List<Portal_wizard_userDTO>> fetchDefault(Portal_wizard_userSearchContext context) {
        Page<Portal_wizard_user> domains = portal_wizard_userService.searchDefault(context) ;
        List<Portal_wizard_userDTO> list = portal_wizard_userMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Portal_wizard_user-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Portal_wizard_user-Get')")
	@ApiOperation(value = "查询数据集", tags = {"门户用户配置" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/portal_wizard_users/searchdefault")
	public ResponseEntity<Page<Portal_wizard_userDTO>> searchDefault(@RequestBody Portal_wizard_userSearchContext context) {
        Page<Portal_wizard_user> domains = portal_wizard_userService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(portal_wizard_userMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


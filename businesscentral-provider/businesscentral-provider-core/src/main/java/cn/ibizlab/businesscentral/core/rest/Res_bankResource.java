package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_bank;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_bankService;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_bankSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"银行" })
@RestController("Core-res_bank")
@RequestMapping("")
public class Res_bankResource {

    @Autowired
    public IRes_bankService res_bankService;

    @Autowired
    @Lazy
    public Res_bankMapping res_bankMapping;

    @PreAuthorize("hasPermission(this.res_bankMapping.toDomain(#res_bankdto),'iBizBusinessCentral-Res_bank-Create')")
    @ApiOperation(value = "新建银行", tags = {"银行" },  notes = "新建银行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_banks")
    public ResponseEntity<Res_bankDTO> create(@Validated @RequestBody Res_bankDTO res_bankdto) {
        Res_bank domain = res_bankMapping.toDomain(res_bankdto);
		res_bankService.create(domain);
        Res_bankDTO dto = res_bankMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_bankMapping.toDomain(#res_bankdtos),'iBizBusinessCentral-Res_bank-Create')")
    @ApiOperation(value = "批量新建银行", tags = {"银行" },  notes = "批量新建银行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_banks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_bankDTO> res_bankdtos) {
        res_bankService.createBatch(res_bankMapping.toDomain(res_bankdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "res_bank" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.res_bankService.get(#res_bank_id),'iBizBusinessCentral-Res_bank-Update')")
    @ApiOperation(value = "更新银行", tags = {"银行" },  notes = "更新银行")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_banks/{res_bank_id}")
    public ResponseEntity<Res_bankDTO> update(@PathVariable("res_bank_id") Long res_bank_id, @RequestBody Res_bankDTO res_bankdto) {
		Res_bank domain  = res_bankMapping.toDomain(res_bankdto);
        domain .setId(res_bank_id);
		res_bankService.update(domain );
		Res_bankDTO dto = res_bankMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.res_bankService.getResBankByEntities(this.res_bankMapping.toDomain(#res_bankdtos)),'iBizBusinessCentral-Res_bank-Update')")
    @ApiOperation(value = "批量更新银行", tags = {"银行" },  notes = "批量更新银行")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_banks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_bankDTO> res_bankdtos) {
        res_bankService.updateBatch(res_bankMapping.toDomain(res_bankdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.res_bankService.get(#res_bank_id),'iBizBusinessCentral-Res_bank-Remove')")
    @ApiOperation(value = "删除银行", tags = {"银行" },  notes = "删除银行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_banks/{res_bank_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("res_bank_id") Long res_bank_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_bankService.remove(res_bank_id));
    }

    @PreAuthorize("hasPermission(this.res_bankService.getResBankByIds(#ids),'iBizBusinessCentral-Res_bank-Remove')")
    @ApiOperation(value = "批量删除银行", tags = {"银行" },  notes = "批量删除银行")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_banks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        res_bankService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.res_bankMapping.toDomain(returnObject.body),'iBizBusinessCentral-Res_bank-Get')")
    @ApiOperation(value = "获取银行", tags = {"银行" },  notes = "获取银行")
	@RequestMapping(method = RequestMethod.GET, value = "/res_banks/{res_bank_id}")
    public ResponseEntity<Res_bankDTO> get(@PathVariable("res_bank_id") Long res_bank_id) {
        Res_bank domain = res_bankService.get(res_bank_id);
        Res_bankDTO dto = res_bankMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取银行草稿", tags = {"银行" },  notes = "获取银行草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/res_banks/getdraft")
    public ResponseEntity<Res_bankDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(res_bankMapping.toDto(res_bankService.getDraft(new Res_bank())));
    }

    @ApiOperation(value = "检查银行", tags = {"银行" },  notes = "检查银行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_banks/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Res_bankDTO res_bankdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(res_bankService.checkKey(res_bankMapping.toDomain(res_bankdto)));
    }

    @PreAuthorize("hasPermission(this.res_bankMapping.toDomain(#res_bankdto),'iBizBusinessCentral-Res_bank-Save')")
    @ApiOperation(value = "保存银行", tags = {"银行" },  notes = "保存银行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_banks/save")
    public ResponseEntity<Boolean> save(@RequestBody Res_bankDTO res_bankdto) {
        return ResponseEntity.status(HttpStatus.OK).body(res_bankService.save(res_bankMapping.toDomain(res_bankdto)));
    }

    @PreAuthorize("hasPermission(this.res_bankMapping.toDomain(#res_bankdtos),'iBizBusinessCentral-Res_bank-Save')")
    @ApiOperation(value = "批量保存银行", tags = {"银行" },  notes = "批量保存银行")
	@RequestMapping(method = RequestMethod.POST, value = "/res_banks/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Res_bankDTO> res_bankdtos) {
        res_bankService.saveBatch(res_bankMapping.toDomain(res_bankdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_bank-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_bank-Get')")
	@ApiOperation(value = "获取数据集", tags = {"银行" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/res_banks/fetchdefault")
	public ResponseEntity<List<Res_bankDTO>> fetchDefault(Res_bankSearchContext context) {
        Page<Res_bank> domains = res_bankService.searchDefault(context) ;
        List<Res_bankDTO> list = res_bankMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Res_bank-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Res_bank-Get')")
	@ApiOperation(value = "查询数据集", tags = {"银行" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/res_banks/searchdefault")
	public ResponseEntity<Page<Res_bankDTO>> searchDefault(@RequestBody Res_bankSearchContext context) {
        Page<Res_bank> domains = res_bankService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_bankMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


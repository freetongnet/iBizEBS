package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.businesscentral.core.dto.Account_reconcile_model_templateDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreAccount_reconcile_model_templateMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_reconcile_model_templateMapping extends MappingBase<Account_reconcile_model_templateDTO, Account_reconcile_model_template> {


}


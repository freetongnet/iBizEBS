package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Ir_sequence_date_rangeDTO]
 */
@Data
public class Ir_sequence_date_rangeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DATE_FROM]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_from" , format="yyyy-MM-dd")
    @JsonProperty("date_from")
    private Timestamp dateFrom;

    /**
     * 属性 [DATE_TO]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_to" , format="yyyy-MM-dd")
    @JsonProperty("date_to")
    private Timestamp dateTo;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [NEXT_CHAR]
     *
     */
    @JSONField(name = "next_char")
    @JsonProperty("next_char")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String nextChar;

    /**
     * 属性 [NEXT_VAL]
     *
     */
    @JSONField(name = "next_val")
    @JsonProperty("next_val")
    private Integer nextVal;

    /**
     * 属性 [NUMBER_NEXT]
     *
     */
    @JSONField(name = "number_next")
    @JsonProperty("number_next")
    private Integer numberNext;

    /**
     * 属性 [CREATE_UNAME]
     *
     */
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String createUname;

    /**
     * 属性 [WRITE_UNAME]
     *
     */
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String writeUname;

    /**
     * 属性 [SEQUENCE_ID]
     *
     */
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long sequenceId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUid;


    /**
     * 设置 [CREATE_DATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 设置 [DATE_FROM]
     */
    public void setDateFrom(Timestamp  dateFrom){
        this.dateFrom = dateFrom ;
        this.modify("date_from",dateFrom);
    }

    /**
     * 设置 [DATE_TO]
     */
    public void setDateTo(Timestamp  dateTo){
        this.dateTo = dateTo ;
        this.modify("date_to",dateTo);
    }

    /**
     * 设置 [WRITE_DATE]
     */
    public void setWriteDate(Timestamp  writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 设置 [NUMBER_NEXT]
     */
    public void setNumberNext(Integer  numberNext){
        this.numberNext = numberNext ;
        this.modify("number_next",numberNext);
    }

    /**
     * 设置 [SEQUENCE_ID]
     */
    public void setSequenceId(Long  sequenceId){
        this.sequenceId = sequenceId ;
        this.modify("sequence_id",sequenceId);
    }

    /**
     * 设置 [WRITE_UID]
     */
    public void setWriteUid(Long  writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [CREATE_UID]
     */
    public void setCreateUid(Long  createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }


}



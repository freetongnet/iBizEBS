package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_stageService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"招聘阶段" })
@RestController("Core-hr_recruitment_stage")
@RequestMapping("")
public class Hr_recruitment_stageResource {

    @Autowired
    public IHr_recruitment_stageService hr_recruitment_stageService;

    @Autowired
    @Lazy
    public Hr_recruitment_stageMapping hr_recruitment_stageMapping;

    @PreAuthorize("hasPermission(this.hr_recruitment_stageMapping.toDomain(#hr_recruitment_stagedto),'iBizBusinessCentral-Hr_recruitment_stage-Create')")
    @ApiOperation(value = "新建招聘阶段", tags = {"招聘阶段" },  notes = "新建招聘阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages")
    public ResponseEntity<Hr_recruitment_stageDTO> create(@Validated @RequestBody Hr_recruitment_stageDTO hr_recruitment_stagedto) {
        Hr_recruitment_stage domain = hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedto);
		hr_recruitment_stageService.create(domain);
        Hr_recruitment_stageDTO dto = hr_recruitment_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_stageMapping.toDomain(#hr_recruitment_stagedtos),'iBizBusinessCentral-Hr_recruitment_stage-Create')")
    @ApiOperation(value = "批量新建招聘阶段", tags = {"招聘阶段" },  notes = "批量新建招聘阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_recruitment_stageDTO> hr_recruitment_stagedtos) {
        hr_recruitment_stageService.createBatch(hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_recruitment_stage" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_recruitment_stageService.get(#hr_recruitment_stage_id),'iBizBusinessCentral-Hr_recruitment_stage-Update')")
    @ApiOperation(value = "更新招聘阶段", tags = {"招聘阶段" },  notes = "更新招聘阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_stages/{hr_recruitment_stage_id}")
    public ResponseEntity<Hr_recruitment_stageDTO> update(@PathVariable("hr_recruitment_stage_id") Long hr_recruitment_stage_id, @RequestBody Hr_recruitment_stageDTO hr_recruitment_stagedto) {
		Hr_recruitment_stage domain  = hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedto);
        domain .setId(hr_recruitment_stage_id);
		hr_recruitment_stageService.update(domain );
		Hr_recruitment_stageDTO dto = hr_recruitment_stageMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_stageService.getHrRecruitmentStageByEntities(this.hr_recruitment_stageMapping.toDomain(#hr_recruitment_stagedtos)),'iBizBusinessCentral-Hr_recruitment_stage-Update')")
    @ApiOperation(value = "批量更新招聘阶段", tags = {"招聘阶段" },  notes = "批量更新招聘阶段")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_recruitment_stageDTO> hr_recruitment_stagedtos) {
        hr_recruitment_stageService.updateBatch(hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_stageService.get(#hr_recruitment_stage_id),'iBizBusinessCentral-Hr_recruitment_stage-Remove')")
    @ApiOperation(value = "删除招聘阶段", tags = {"招聘阶段" },  notes = "删除招聘阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_stages/{hr_recruitment_stage_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_recruitment_stage_id") Long hr_recruitment_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_stageService.remove(hr_recruitment_stage_id));
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_stageService.getHrRecruitmentStageByIds(#ids),'iBizBusinessCentral-Hr_recruitment_stage-Remove')")
    @ApiOperation(value = "批量删除招聘阶段", tags = {"招聘阶段" },  notes = "批量删除招聘阶段")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_recruitment_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_recruitment_stageMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_recruitment_stage-Get')")
    @ApiOperation(value = "获取招聘阶段", tags = {"招聘阶段" },  notes = "获取招聘阶段")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_stages/{hr_recruitment_stage_id}")
    public ResponseEntity<Hr_recruitment_stageDTO> get(@PathVariable("hr_recruitment_stage_id") Long hr_recruitment_stage_id) {
        Hr_recruitment_stage domain = hr_recruitment_stageService.get(hr_recruitment_stage_id);
        Hr_recruitment_stageDTO dto = hr_recruitment_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取招聘阶段草稿", tags = {"招聘阶段" },  notes = "获取招聘阶段草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_stages/getdraft")
    public ResponseEntity<Hr_recruitment_stageDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_stageMapping.toDto(hr_recruitment_stageService.getDraft(new Hr_recruitment_stage())));
    }

    @ApiOperation(value = "检查招聘阶段", tags = {"招聘阶段" },  notes = "检查招聘阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_recruitment_stageDTO hr_recruitment_stagedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_stageService.checkKey(hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedto)));
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_stageMapping.toDomain(#hr_recruitment_stagedto),'iBizBusinessCentral-Hr_recruitment_stage-Save')")
    @ApiOperation(value = "保存招聘阶段", tags = {"招聘阶段" },  notes = "保存招聘阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_recruitment_stageDTO hr_recruitment_stagedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_recruitment_stageService.save(hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedto)));
    }

    @PreAuthorize("hasPermission(this.hr_recruitment_stageMapping.toDomain(#hr_recruitment_stagedtos),'iBizBusinessCentral-Hr_recruitment_stage-Save')")
    @ApiOperation(value = "批量保存招聘阶段", tags = {"招聘阶段" },  notes = "批量保存招聘阶段")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_stages/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_recruitment_stageDTO> hr_recruitment_stagedtos) {
        hr_recruitment_stageService.saveBatch(hr_recruitment_stageMapping.toDomain(hr_recruitment_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_recruitment_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_recruitment_stage-Get')")
	@ApiOperation(value = "获取数据集", tags = {"招聘阶段" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_recruitment_stages/fetchdefault")
	public ResponseEntity<List<Hr_recruitment_stageDTO>> fetchDefault(Hr_recruitment_stageSearchContext context) {
        Page<Hr_recruitment_stage> domains = hr_recruitment_stageService.searchDefault(context) ;
        List<Hr_recruitment_stageDTO> list = hr_recruitment_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_recruitment_stage-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_recruitment_stage-Get')")
	@ApiOperation(value = "查询数据集", tags = {"招聘阶段" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_recruitment_stages/searchdefault")
	public ResponseEntity<Page<Hr_recruitment_stageDTO>> searchDefault(@RequestBody Hr_recruitment_stageSearchContext context) {
        Page<Hr_recruitment_stage> domains = hr_recruitment_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_recruitment_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


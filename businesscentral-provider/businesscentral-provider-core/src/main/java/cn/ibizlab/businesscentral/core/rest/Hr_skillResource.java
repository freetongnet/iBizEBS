package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_skill;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_skillService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_skillSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"技能" })
@RestController("Core-hr_skill")
@RequestMapping("")
public class Hr_skillResource {

    @Autowired
    public IHr_skillService hr_skillService;

    @Autowired
    @Lazy
    public Hr_skillMapping hr_skillMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Create-all')")
    @ApiOperation(value = "新建技能", tags = {"技能" },  notes = "新建技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skills")
    public ResponseEntity<Hr_skillDTO> create(@Validated @RequestBody Hr_skillDTO hr_skilldto) {
        Hr_skill domain = hr_skillMapping.toDomain(hr_skilldto);
		hr_skillService.create(domain);
        Hr_skillDTO dto = hr_skillMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Create-all')")
    @ApiOperation(value = "批量新建技能", tags = {"技能" },  notes = "批量新建技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skills/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_skillDTO> hr_skilldtos) {
        hr_skillService.createBatch(hr_skillMapping.toDomain(hr_skilldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Update-all')")
    @ApiOperation(value = "更新技能", tags = {"技能" },  notes = "更新技能")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skills/{hr_skill_id}")
    public ResponseEntity<Hr_skillDTO> update(@PathVariable("hr_skill_id") Long hr_skill_id, @RequestBody Hr_skillDTO hr_skilldto) {
		Hr_skill domain  = hr_skillMapping.toDomain(hr_skilldto);
        domain .setId(hr_skill_id);
		hr_skillService.update(domain );
		Hr_skillDTO dto = hr_skillMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Update-all')")
    @ApiOperation(value = "批量更新技能", tags = {"技能" },  notes = "批量更新技能")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skills/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_skillDTO> hr_skilldtos) {
        hr_skillService.updateBatch(hr_skillMapping.toDomain(hr_skilldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Remove-all')")
    @ApiOperation(value = "删除技能", tags = {"技能" },  notes = "删除技能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skills/{hr_skill_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_skill_id") Long hr_skill_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_skillService.remove(hr_skill_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Remove-all')")
    @ApiOperation(value = "批量删除技能", tags = {"技能" },  notes = "批量删除技能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skills/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_skillService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Get-all')")
    @ApiOperation(value = "获取技能", tags = {"技能" },  notes = "获取技能")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_skills/{hr_skill_id}")
    public ResponseEntity<Hr_skillDTO> get(@PathVariable("hr_skill_id") Long hr_skill_id) {
        Hr_skill domain = hr_skillService.get(hr_skill_id);
        Hr_skillDTO dto = hr_skillMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取技能草稿", tags = {"技能" },  notes = "获取技能草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_skills/getdraft")
    public ResponseEntity<Hr_skillDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_skillMapping.toDto(hr_skillService.getDraft(new Hr_skill())));
    }

    @ApiOperation(value = "检查技能", tags = {"技能" },  notes = "检查技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skills/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_skillDTO hr_skilldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_skillService.checkKey(hr_skillMapping.toDomain(hr_skilldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Save-all')")
    @ApiOperation(value = "保存技能", tags = {"技能" },  notes = "保存技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skills/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_skillDTO hr_skilldto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_skillService.save(hr_skillMapping.toDomain(hr_skilldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Save-all')")
    @ApiOperation(value = "批量保存技能", tags = {"技能" },  notes = "批量保存技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skills/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_skillDTO> hr_skilldtos) {
        hr_skillService.saveBatch(hr_skillMapping.toDomain(hr_skilldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"技能" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_skills/fetchdefault")
	public ResponseEntity<List<Hr_skillDTO>> fetchDefault(Hr_skillSearchContext context) {
        Page<Hr_skill> domains = hr_skillService.searchDefault(context) ;
        List<Hr_skillDTO> list = hr_skillMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"技能" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_skills/searchdefault")
	public ResponseEntity<Page<Hr_skillDTO>> searchDefault(@RequestBody Hr_skillSearchContext context) {
        Page<Hr_skill> domains = hr_skillService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_skillMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Create-all')")
    @ApiOperation(value = "根据技能类型建立技能", tags = {"技能" },  notes = "根据技能类型建立技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills")
    public ResponseEntity<Hr_skillDTO> createByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody Hr_skillDTO hr_skilldto) {
        Hr_skill domain = hr_skillMapping.toDomain(hr_skilldto);
        domain.setSkillTypeId(hr_skill_type_id);
		hr_skillService.create(domain);
        Hr_skillDTO dto = hr_skillMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Create-all')")
    @ApiOperation(value = "根据技能类型批量建立技能", tags = {"技能" },  notes = "根据技能类型批量建立技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/batch")
    public ResponseEntity<Boolean> createBatchByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody List<Hr_skillDTO> hr_skilldtos) {
        List<Hr_skill> domainlist=hr_skillMapping.toDomain(hr_skilldtos);
        for(Hr_skill domain:domainlist){
            domain.setSkillTypeId(hr_skill_type_id);
        }
        hr_skillService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Update-all')")
    @ApiOperation(value = "根据技能类型更新技能", tags = {"技能" },  notes = "根据技能类型更新技能")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/{hr_skill_id}")
    public ResponseEntity<Hr_skillDTO> updateByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @PathVariable("hr_skill_id") Long hr_skill_id, @RequestBody Hr_skillDTO hr_skilldto) {
        Hr_skill domain = hr_skillMapping.toDomain(hr_skilldto);
        domain.setSkillTypeId(hr_skill_type_id);
        domain.setId(hr_skill_id);
		hr_skillService.update(domain);
        Hr_skillDTO dto = hr_skillMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Update-all')")
    @ApiOperation(value = "根据技能类型批量更新技能", tags = {"技能" },  notes = "根据技能类型批量更新技能")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/batch")
    public ResponseEntity<Boolean> updateBatchByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody List<Hr_skillDTO> hr_skilldtos) {
        List<Hr_skill> domainlist=hr_skillMapping.toDomain(hr_skilldtos);
        for(Hr_skill domain:domainlist){
            domain.setSkillTypeId(hr_skill_type_id);
        }
        hr_skillService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Remove-all')")
    @ApiOperation(value = "根据技能类型删除技能", tags = {"技能" },  notes = "根据技能类型删除技能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/{hr_skill_id}")
    public ResponseEntity<Boolean> removeByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @PathVariable("hr_skill_id") Long hr_skill_id) {
		return ResponseEntity.status(HttpStatus.OK).body(hr_skillService.remove(hr_skill_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Remove-all')")
    @ApiOperation(value = "根据技能类型批量删除技能", tags = {"技能" },  notes = "根据技能类型批量删除技能")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/batch")
    public ResponseEntity<Boolean> removeBatchByHr_skill_type(@RequestBody List<Long> ids) {
        hr_skillService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Get-all')")
    @ApiOperation(value = "根据技能类型获取技能", tags = {"技能" },  notes = "根据技能类型获取技能")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/{hr_skill_id}")
    public ResponseEntity<Hr_skillDTO> getByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @PathVariable("hr_skill_id") Long hr_skill_id) {
        Hr_skill domain = hr_skillService.get(hr_skill_id);
        Hr_skillDTO dto = hr_skillMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据技能类型获取技能草稿", tags = {"技能" },  notes = "根据技能类型获取技能草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/getdraft")
    public ResponseEntity<Hr_skillDTO> getDraftByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id) {
        Hr_skill domain = new Hr_skill();
        domain.setSkillTypeId(hr_skill_type_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_skillMapping.toDto(hr_skillService.getDraft(domain)));
    }

    @ApiOperation(value = "根据技能类型检查技能", tags = {"技能" },  notes = "根据技能类型检查技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/checkkey")
    public ResponseEntity<Boolean> checkKeyByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody Hr_skillDTO hr_skilldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_skillService.checkKey(hr_skillMapping.toDomain(hr_skilldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Save-all')")
    @ApiOperation(value = "根据技能类型保存技能", tags = {"技能" },  notes = "根据技能类型保存技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/save")
    public ResponseEntity<Boolean> saveByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody Hr_skillDTO hr_skilldto) {
        Hr_skill domain = hr_skillMapping.toDomain(hr_skilldto);
        domain.setSkillTypeId(hr_skill_type_id);
        return ResponseEntity.status(HttpStatus.OK).body(hr_skillService.save(domain));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-Save-all')")
    @ApiOperation(value = "根据技能类型批量保存技能", tags = {"技能" },  notes = "根据技能类型批量保存技能")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_skill_types/{hr_skill_type_id}/hr_skills/savebatch")
    public ResponseEntity<Boolean> saveBatchByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody List<Hr_skillDTO> hr_skilldtos) {
        List<Hr_skill> domainlist=hr_skillMapping.toDomain(hr_skilldtos);
        for(Hr_skill domain:domainlist){
             domain.setSkillTypeId(hr_skill_type_id);
        }
        hr_skillService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-searchDefault-all')")
	@ApiOperation(value = "根据技能类型获取数据集", tags = {"技能" } ,notes = "根据技能类型获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_skill_types/{hr_skill_type_id}/hr_skills/fetchdefault")
	public ResponseEntity<List<Hr_skillDTO>> fetchHr_skillDefaultByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id,Hr_skillSearchContext context) {
        context.setN_skill_type_id_eq(hr_skill_type_id);
        Page<Hr_skill> domains = hr_skillService.searchDefault(context) ;
        List<Hr_skillDTO> list = hr_skillMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_skill-searchDefault-all')")
	@ApiOperation(value = "根据技能类型查询数据集", tags = {"技能" } ,notes = "根据技能类型查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_skill_types/{hr_skill_type_id}/hr_skills/searchdefault")
	public ResponseEntity<Page<Hr_skillDTO>> searchHr_skillDefaultByHr_skill_type(@PathVariable("hr_skill_type_id") Long hr_skill_type_id, @RequestBody Hr_skillSearchContext context) {
        context.setN_skill_type_id_eq(hr_skill_type_id);
        Page<Hr_skill> domains = hr_skillService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_skillMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}


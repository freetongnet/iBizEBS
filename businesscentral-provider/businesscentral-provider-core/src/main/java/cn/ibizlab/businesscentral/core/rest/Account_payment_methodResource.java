package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_method;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_methodService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_payment_methodSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"付款方法" })
@RestController("Core-account_payment_method")
@RequestMapping("")
public class Account_payment_methodResource {

    @Autowired
    public IAccount_payment_methodService account_payment_methodService;

    @Autowired
    @Lazy
    public Account_payment_methodMapping account_payment_methodMapping;

    @PreAuthorize("hasPermission(this.account_payment_methodMapping.toDomain(#account_payment_methoddto),'iBizBusinessCentral-Account_payment_method-Create')")
    @ApiOperation(value = "新建付款方法", tags = {"付款方法" },  notes = "新建付款方法")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods")
    public ResponseEntity<Account_payment_methodDTO> create(@Validated @RequestBody Account_payment_methodDTO account_payment_methoddto) {
        Account_payment_method domain = account_payment_methodMapping.toDomain(account_payment_methoddto);
		account_payment_methodService.create(domain);
        Account_payment_methodDTO dto = account_payment_methodMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_payment_methodMapping.toDomain(#account_payment_methoddtos),'iBizBusinessCentral-Account_payment_method-Create')")
    @ApiOperation(value = "批量新建付款方法", tags = {"付款方法" },  notes = "批量新建付款方法")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_payment_methodDTO> account_payment_methoddtos) {
        account_payment_methodService.createBatch(account_payment_methodMapping.toDomain(account_payment_methoddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_payment_method" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_payment_methodService.get(#account_payment_method_id),'iBizBusinessCentral-Account_payment_method-Update')")
    @ApiOperation(value = "更新付款方法", tags = {"付款方法" },  notes = "更新付款方法")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_methods/{account_payment_method_id}")
    public ResponseEntity<Account_payment_methodDTO> update(@PathVariable("account_payment_method_id") Long account_payment_method_id, @RequestBody Account_payment_methodDTO account_payment_methoddto) {
		Account_payment_method domain  = account_payment_methodMapping.toDomain(account_payment_methoddto);
        domain .setId(account_payment_method_id);
		account_payment_methodService.update(domain );
		Account_payment_methodDTO dto = account_payment_methodMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_payment_methodService.getAccountPaymentMethodByEntities(this.account_payment_methodMapping.toDomain(#account_payment_methoddtos)),'iBizBusinessCentral-Account_payment_method-Update')")
    @ApiOperation(value = "批量更新付款方法", tags = {"付款方法" },  notes = "批量更新付款方法")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_methods/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_payment_methodDTO> account_payment_methoddtos) {
        account_payment_methodService.updateBatch(account_payment_methodMapping.toDomain(account_payment_methoddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_payment_methodService.get(#account_payment_method_id),'iBizBusinessCentral-Account_payment_method-Remove')")
    @ApiOperation(value = "删除付款方法", tags = {"付款方法" },  notes = "删除付款方法")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_methods/{account_payment_method_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_method_id") Long account_payment_method_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_payment_methodService.remove(account_payment_method_id));
    }

    @PreAuthorize("hasPermission(this.account_payment_methodService.getAccountPaymentMethodByIds(#ids),'iBizBusinessCentral-Account_payment_method-Remove')")
    @ApiOperation(value = "批量删除付款方法", tags = {"付款方法" },  notes = "批量删除付款方法")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_methods/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_payment_methodService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_payment_methodMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_payment_method-Get')")
    @ApiOperation(value = "获取付款方法", tags = {"付款方法" },  notes = "获取付款方法")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_methods/{account_payment_method_id}")
    public ResponseEntity<Account_payment_methodDTO> get(@PathVariable("account_payment_method_id") Long account_payment_method_id) {
        Account_payment_method domain = account_payment_methodService.get(account_payment_method_id);
        Account_payment_methodDTO dto = account_payment_methodMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取付款方法草稿", tags = {"付款方法" },  notes = "获取付款方法草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_methods/getdraft")
    public ResponseEntity<Account_payment_methodDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_payment_methodMapping.toDto(account_payment_methodService.getDraft(new Account_payment_method())));
    }

    @ApiOperation(value = "检查付款方法", tags = {"付款方法" },  notes = "检查付款方法")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_payment_methodDTO account_payment_methoddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_payment_methodService.checkKey(account_payment_methodMapping.toDomain(account_payment_methoddto)));
    }

    @PreAuthorize("hasPermission(this.account_payment_methodMapping.toDomain(#account_payment_methoddto),'iBizBusinessCentral-Account_payment_method-Save')")
    @ApiOperation(value = "保存付款方法", tags = {"付款方法" },  notes = "保存付款方法")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_payment_methodDTO account_payment_methoddto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_payment_methodService.save(account_payment_methodMapping.toDomain(account_payment_methoddto)));
    }

    @PreAuthorize("hasPermission(this.account_payment_methodMapping.toDomain(#account_payment_methoddtos),'iBizBusinessCentral-Account_payment_method-Save')")
    @ApiOperation(value = "批量保存付款方法", tags = {"付款方法" },  notes = "批量保存付款方法")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_payment_methodDTO> account_payment_methoddtos) {
        account_payment_methodService.saveBatch(account_payment_methodMapping.toDomain(account_payment_methoddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_payment_method-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_payment_method-Get')")
	@ApiOperation(value = "获取数据集", tags = {"付款方法" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_payment_methods/fetchdefault")
	public ResponseEntity<List<Account_payment_methodDTO>> fetchDefault(Account_payment_methodSearchContext context) {
        Page<Account_payment_method> domains = account_payment_methodService.searchDefault(context) ;
        List<Account_payment_methodDTO> list = account_payment_methodMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_payment_method-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_payment_method-Get')")
	@ApiOperation(value = "查询数据集", tags = {"付款方法" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_payment_methods/searchdefault")
	public ResponseEntity<Page<Account_payment_methodDTO>> searchDefault(@RequestBody Account_payment_methodSearchContext context) {
        Page<Account_payment_method> domains = account_payment_methodService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_payment_methodMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


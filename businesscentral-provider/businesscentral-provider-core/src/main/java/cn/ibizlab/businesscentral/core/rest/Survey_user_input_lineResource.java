package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_input_lineService;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_user_input_lineSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"调查用户输入明细" })
@RestController("Core-survey_user_input_line")
@RequestMapping("")
public class Survey_user_input_lineResource {

    @Autowired
    public ISurvey_user_input_lineService survey_user_input_lineService;

    @Autowired
    @Lazy
    public Survey_user_input_lineMapping survey_user_input_lineMapping;

    @PreAuthorize("hasPermission(this.survey_user_input_lineMapping.toDomain(#survey_user_input_linedto),'iBizBusinessCentral-Survey_user_input_line-Create')")
    @ApiOperation(value = "新建调查用户输入明细", tags = {"调查用户输入明细" },  notes = "新建调查用户输入明细")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines")
    public ResponseEntity<Survey_user_input_lineDTO> create(@Validated @RequestBody Survey_user_input_lineDTO survey_user_input_linedto) {
        Survey_user_input_line domain = survey_user_input_lineMapping.toDomain(survey_user_input_linedto);
		survey_user_input_lineService.create(domain);
        Survey_user_input_lineDTO dto = survey_user_input_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_user_input_lineMapping.toDomain(#survey_user_input_linedtos),'iBizBusinessCentral-Survey_user_input_line-Create')")
    @ApiOperation(value = "批量新建调查用户输入明细", tags = {"调查用户输入明细" },  notes = "批量新建调查用户输入明细")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_user_input_lineDTO> survey_user_input_linedtos) {
        survey_user_input_lineService.createBatch(survey_user_input_lineMapping.toDomain(survey_user_input_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "survey_user_input_line" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.survey_user_input_lineService.get(#survey_user_input_line_id),'iBizBusinessCentral-Survey_user_input_line-Update')")
    @ApiOperation(value = "更新调查用户输入明细", tags = {"调查用户输入明细" },  notes = "更新调查用户输入明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_user_input_lines/{survey_user_input_line_id}")
    public ResponseEntity<Survey_user_input_lineDTO> update(@PathVariable("survey_user_input_line_id") Long survey_user_input_line_id, @RequestBody Survey_user_input_lineDTO survey_user_input_linedto) {
		Survey_user_input_line domain  = survey_user_input_lineMapping.toDomain(survey_user_input_linedto);
        domain .setId(survey_user_input_line_id);
		survey_user_input_lineService.update(domain );
		Survey_user_input_lineDTO dto = survey_user_input_lineMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.survey_user_input_lineService.getSurveyUserInputLineByEntities(this.survey_user_input_lineMapping.toDomain(#survey_user_input_linedtos)),'iBizBusinessCentral-Survey_user_input_line-Update')")
    @ApiOperation(value = "批量更新调查用户输入明细", tags = {"调查用户输入明细" },  notes = "批量更新调查用户输入明细")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_user_input_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_user_input_lineDTO> survey_user_input_linedtos) {
        survey_user_input_lineService.updateBatch(survey_user_input_lineMapping.toDomain(survey_user_input_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.survey_user_input_lineService.get(#survey_user_input_line_id),'iBizBusinessCentral-Survey_user_input_line-Remove')")
    @ApiOperation(value = "删除调查用户输入明细", tags = {"调查用户输入明细" },  notes = "删除调查用户输入明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_input_lines/{survey_user_input_line_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("survey_user_input_line_id") Long survey_user_input_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_user_input_lineService.remove(survey_user_input_line_id));
    }

    @PreAuthorize("hasPermission(this.survey_user_input_lineService.getSurveyUserInputLineByIds(#ids),'iBizBusinessCentral-Survey_user_input_line-Remove')")
    @ApiOperation(value = "批量删除调查用户输入明细", tags = {"调查用户输入明细" },  notes = "批量删除调查用户输入明细")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_input_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        survey_user_input_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.survey_user_input_lineMapping.toDomain(returnObject.body),'iBizBusinessCentral-Survey_user_input_line-Get')")
    @ApiOperation(value = "获取调查用户输入明细", tags = {"调查用户输入明细" },  notes = "获取调查用户输入明细")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_user_input_lines/{survey_user_input_line_id}")
    public ResponseEntity<Survey_user_input_lineDTO> get(@PathVariable("survey_user_input_line_id") Long survey_user_input_line_id) {
        Survey_user_input_line domain = survey_user_input_lineService.get(survey_user_input_line_id);
        Survey_user_input_lineDTO dto = survey_user_input_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取调查用户输入明细草稿", tags = {"调查用户输入明细" },  notes = "获取调查用户输入明细草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_user_input_lines/getdraft")
    public ResponseEntity<Survey_user_input_lineDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(survey_user_input_lineMapping.toDto(survey_user_input_lineService.getDraft(new Survey_user_input_line())));
    }

    @ApiOperation(value = "检查调查用户输入明细", tags = {"调查用户输入明细" },  notes = "检查调查用户输入明细")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Survey_user_input_lineDTO survey_user_input_linedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(survey_user_input_lineService.checkKey(survey_user_input_lineMapping.toDomain(survey_user_input_linedto)));
    }

    @PreAuthorize("hasPermission(this.survey_user_input_lineMapping.toDomain(#survey_user_input_linedto),'iBizBusinessCentral-Survey_user_input_line-Save')")
    @ApiOperation(value = "保存调查用户输入明细", tags = {"调查用户输入明细" },  notes = "保存调查用户输入明细")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines/save")
    public ResponseEntity<Boolean> save(@RequestBody Survey_user_input_lineDTO survey_user_input_linedto) {
        return ResponseEntity.status(HttpStatus.OK).body(survey_user_input_lineService.save(survey_user_input_lineMapping.toDomain(survey_user_input_linedto)));
    }

    @PreAuthorize("hasPermission(this.survey_user_input_lineMapping.toDomain(#survey_user_input_linedtos),'iBizBusinessCentral-Survey_user_input_line-Save')")
    @ApiOperation(value = "批量保存调查用户输入明细", tags = {"调查用户输入明细" },  notes = "批量保存调查用户输入明细")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Survey_user_input_lineDTO> survey_user_input_linedtos) {
        survey_user_input_lineService.saveBatch(survey_user_input_lineMapping.toDomain(survey_user_input_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_user_input_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_user_input_line-Get')")
	@ApiOperation(value = "获取数据集", tags = {"调查用户输入明细" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/survey_user_input_lines/fetchdefault")
	public ResponseEntity<List<Survey_user_input_lineDTO>> fetchDefault(Survey_user_input_lineSearchContext context) {
        Page<Survey_user_input_line> domains = survey_user_input_lineService.searchDefault(context) ;
        List<Survey_user_input_lineDTO> list = survey_user_input_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Survey_user_input_line-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Survey_user_input_line-Get')")
	@ApiOperation(value = "查询数据集", tags = {"调查用户输入明细" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/survey_user_input_lines/searchdefault")
	public ResponseEntity<Page<Survey_user_input_lineDTO>> searchDefault(@RequestBody Survey_user_input_lineSearchContext context) {
        Page<Survey_user_input_line> domains = survey_user_input_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_user_input_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


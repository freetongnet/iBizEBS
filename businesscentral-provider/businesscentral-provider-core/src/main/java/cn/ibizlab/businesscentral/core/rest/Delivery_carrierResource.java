package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Delivery_carrier;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IDelivery_carrierService;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Delivery_carrierSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"送货方式" })
@RestController("Core-delivery_carrier")
@RequestMapping("")
public class Delivery_carrierResource {

    @Autowired
    public IDelivery_carrierService delivery_carrierService;

    @Autowired
    @Lazy
    public Delivery_carrierMapping delivery_carrierMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-Create-all')")
    @ApiOperation(value = "新建送货方式", tags = {"送货方式" },  notes = "新建送货方式")
	@RequestMapping(method = RequestMethod.POST, value = "/delivery_carriers")
    public ResponseEntity<Delivery_carrierDTO> create(@Validated @RequestBody Delivery_carrierDTO delivery_carrierdto) {
        Delivery_carrier domain = delivery_carrierMapping.toDomain(delivery_carrierdto);
		delivery_carrierService.create(domain);
        Delivery_carrierDTO dto = delivery_carrierMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-Create-all')")
    @ApiOperation(value = "批量新建送货方式", tags = {"送货方式" },  notes = "批量新建送货方式")
	@RequestMapping(method = RequestMethod.POST, value = "/delivery_carriers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Delivery_carrierDTO> delivery_carrierdtos) {
        delivery_carrierService.createBatch(delivery_carrierMapping.toDomain(delivery_carrierdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-Update-all')")
    @ApiOperation(value = "更新送货方式", tags = {"送货方式" },  notes = "更新送货方式")
	@RequestMapping(method = RequestMethod.PUT, value = "/delivery_carriers/{delivery_carrier_id}")
    public ResponseEntity<Delivery_carrierDTO> update(@PathVariable("delivery_carrier_id") Long delivery_carrier_id, @RequestBody Delivery_carrierDTO delivery_carrierdto) {
		Delivery_carrier domain  = delivery_carrierMapping.toDomain(delivery_carrierdto);
        domain .setId(delivery_carrier_id);
		delivery_carrierService.update(domain );
		Delivery_carrierDTO dto = delivery_carrierMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-Update-all')")
    @ApiOperation(value = "批量更新送货方式", tags = {"送货方式" },  notes = "批量更新送货方式")
	@RequestMapping(method = RequestMethod.PUT, value = "/delivery_carriers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Delivery_carrierDTO> delivery_carrierdtos) {
        delivery_carrierService.updateBatch(delivery_carrierMapping.toDomain(delivery_carrierdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-Remove-all')")
    @ApiOperation(value = "删除送货方式", tags = {"送货方式" },  notes = "删除送货方式")
	@RequestMapping(method = RequestMethod.DELETE, value = "/delivery_carriers/{delivery_carrier_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("delivery_carrier_id") Long delivery_carrier_id) {
         return ResponseEntity.status(HttpStatus.OK).body(delivery_carrierService.remove(delivery_carrier_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-Remove-all')")
    @ApiOperation(value = "批量删除送货方式", tags = {"送货方式" },  notes = "批量删除送货方式")
	@RequestMapping(method = RequestMethod.DELETE, value = "/delivery_carriers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        delivery_carrierService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-Get-all')")
    @ApiOperation(value = "获取送货方式", tags = {"送货方式" },  notes = "获取送货方式")
	@RequestMapping(method = RequestMethod.GET, value = "/delivery_carriers/{delivery_carrier_id}")
    public ResponseEntity<Delivery_carrierDTO> get(@PathVariable("delivery_carrier_id") Long delivery_carrier_id) {
        Delivery_carrier domain = delivery_carrierService.get(delivery_carrier_id);
        Delivery_carrierDTO dto = delivery_carrierMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取送货方式草稿", tags = {"送货方式" },  notes = "获取送货方式草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/delivery_carriers/getdraft")
    public ResponseEntity<Delivery_carrierDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(delivery_carrierMapping.toDto(delivery_carrierService.getDraft(new Delivery_carrier())));
    }

    @ApiOperation(value = "检查送货方式", tags = {"送货方式" },  notes = "检查送货方式")
	@RequestMapping(method = RequestMethod.POST, value = "/delivery_carriers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Delivery_carrierDTO delivery_carrierdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(delivery_carrierService.checkKey(delivery_carrierMapping.toDomain(delivery_carrierdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-Save-all')")
    @ApiOperation(value = "保存送货方式", tags = {"送货方式" },  notes = "保存送货方式")
	@RequestMapping(method = RequestMethod.POST, value = "/delivery_carriers/save")
    public ResponseEntity<Boolean> save(@RequestBody Delivery_carrierDTO delivery_carrierdto) {
        return ResponseEntity.status(HttpStatus.OK).body(delivery_carrierService.save(delivery_carrierMapping.toDomain(delivery_carrierdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-Save-all')")
    @ApiOperation(value = "批量保存送货方式", tags = {"送货方式" },  notes = "批量保存送货方式")
	@RequestMapping(method = RequestMethod.POST, value = "/delivery_carriers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Delivery_carrierDTO> delivery_carrierdtos) {
        delivery_carrierService.saveBatch(delivery_carrierMapping.toDomain(delivery_carrierdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"送货方式" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/delivery_carriers/fetchdefault")
	public ResponseEntity<List<Delivery_carrierDTO>> fetchDefault(Delivery_carrierSearchContext context) {
        Page<Delivery_carrier> domains = delivery_carrierService.searchDefault(context) ;
        List<Delivery_carrierDTO> list = delivery_carrierMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Delivery_carrier-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"送货方式" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/delivery_carriers/searchdefault")
	public ResponseEntity<Page<Delivery_carrierDTO>> searchDefault(@RequestBody Delivery_carrierSearchContext context) {
        Page<Delivery_carrier> domains = delivery_carrierService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(delivery_carrierMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


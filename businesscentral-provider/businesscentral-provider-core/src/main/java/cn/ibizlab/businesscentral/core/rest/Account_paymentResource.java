package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_paymentService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_paymentSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"付款" })
@RestController("Core-account_payment")
@RequestMapping("")
public class Account_paymentResource {

    @Autowired
    public IAccount_paymentService account_paymentService;

    @Autowired
    @Lazy
    public Account_paymentMapping account_paymentMapping;

    @PreAuthorize("hasPermission(this.account_paymentMapping.toDomain(#account_paymentdto),'iBizBusinessCentral-Account_payment-Create')")
    @ApiOperation(value = "新建付款", tags = {"付款" },  notes = "新建付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments")
    public ResponseEntity<Account_paymentDTO> create(@Validated @RequestBody Account_paymentDTO account_paymentdto) {
        Account_payment domain = account_paymentMapping.toDomain(account_paymentdto);
		account_paymentService.create(domain);
        Account_paymentDTO dto = account_paymentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_paymentMapping.toDomain(#account_paymentdtos),'iBizBusinessCentral-Account_payment-Create')")
    @ApiOperation(value = "批量新建付款", tags = {"付款" },  notes = "批量新建付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_paymentDTO> account_paymentdtos) {
        account_paymentService.createBatch(account_paymentMapping.toDomain(account_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_payment" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_paymentService.get(#account_payment_id),'iBizBusinessCentral-Account_payment-Update')")
    @ApiOperation(value = "更新付款", tags = {"付款" },  notes = "更新付款")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payments/{account_payment_id}")
    public ResponseEntity<Account_paymentDTO> update(@PathVariable("account_payment_id") Long account_payment_id, @RequestBody Account_paymentDTO account_paymentdto) {
		Account_payment domain  = account_paymentMapping.toDomain(account_paymentdto);
        domain .setId(account_payment_id);
		account_paymentService.update(domain );
		Account_paymentDTO dto = account_paymentMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_paymentService.getAccountPaymentByEntities(this.account_paymentMapping.toDomain(#account_paymentdtos)),'iBizBusinessCentral-Account_payment-Update')")
    @ApiOperation(value = "批量更新付款", tags = {"付款" },  notes = "批量更新付款")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_paymentDTO> account_paymentdtos) {
        account_paymentService.updateBatch(account_paymentMapping.toDomain(account_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_paymentService.get(#account_payment_id),'iBizBusinessCentral-Account_payment-Remove')")
    @ApiOperation(value = "删除付款", tags = {"付款" },  notes = "删除付款")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payments/{account_payment_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_id") Long account_payment_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_paymentService.remove(account_payment_id));
    }

    @PreAuthorize("hasPermission(this.account_paymentService.getAccountPaymentByIds(#ids),'iBizBusinessCentral-Account_payment-Remove')")
    @ApiOperation(value = "批量删除付款", tags = {"付款" },  notes = "批量删除付款")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_paymentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_paymentMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_payment-Get')")
    @ApiOperation(value = "获取付款", tags = {"付款" },  notes = "获取付款")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payments/{account_payment_id}")
    public ResponseEntity<Account_paymentDTO> get(@PathVariable("account_payment_id") Long account_payment_id) {
        Account_payment domain = account_paymentService.get(account_payment_id);
        Account_paymentDTO dto = account_paymentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取付款草稿", tags = {"付款" },  notes = "获取付款草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payments/getdraft")
    public ResponseEntity<Account_paymentDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_paymentMapping.toDto(account_paymentService.getDraft(new Account_payment())));
    }

    @ApiOperation(value = "检查付款", tags = {"付款" },  notes = "检查付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_paymentDTO account_paymentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_paymentService.checkKey(account_paymentMapping.toDomain(account_paymentdto)));
    }

    @PreAuthorize("hasPermission(this.account_paymentMapping.toDomain(#account_paymentdto),'iBizBusinessCentral-Account_payment-Save')")
    @ApiOperation(value = "保存付款", tags = {"付款" },  notes = "保存付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_paymentDTO account_paymentdto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_paymentService.save(account_paymentMapping.toDomain(account_paymentdto)));
    }

    @PreAuthorize("hasPermission(this.account_paymentMapping.toDomain(#account_paymentdtos),'iBizBusinessCentral-Account_payment-Save')")
    @ApiOperation(value = "批量保存付款", tags = {"付款" },  notes = "批量保存付款")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_paymentDTO> account_paymentdtos) {
        account_paymentService.saveBatch(account_paymentMapping.toDomain(account_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_payment-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_payment-Get')")
	@ApiOperation(value = "获取数据集", tags = {"付款" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_payments/fetchdefault")
	public ResponseEntity<List<Account_paymentDTO>> fetchDefault(Account_paymentSearchContext context) {
        Page<Account_payment> domains = account_paymentService.searchDefault(context) ;
        List<Account_paymentDTO> list = account_paymentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_payment-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_payment-Get')")
	@ApiOperation(value = "查询数据集", tags = {"付款" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_payments/searchdefault")
	public ResponseEntity<Page<Account_paymentDTO>> searchDefault(@RequestBody Account_paymentSearchContext context) {
        Page<Account_payment> domains = account_paymentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_paymentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


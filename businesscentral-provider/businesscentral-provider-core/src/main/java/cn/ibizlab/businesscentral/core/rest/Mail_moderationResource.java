package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_moderationService;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_moderationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"渠道黑/白名单" })
@RestController("Core-mail_moderation")
@RequestMapping("")
public class Mail_moderationResource {

    @Autowired
    public IMail_moderationService mail_moderationService;

    @Autowired
    @Lazy
    public Mail_moderationMapping mail_moderationMapping;

    @PreAuthorize("hasPermission(this.mail_moderationMapping.toDomain(#mail_moderationdto),'iBizBusinessCentral-Mail_moderation-Create')")
    @ApiOperation(value = "新建渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "新建渠道黑/白名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_moderations")
    public ResponseEntity<Mail_moderationDTO> create(@Validated @RequestBody Mail_moderationDTO mail_moderationdto) {
        Mail_moderation domain = mail_moderationMapping.toDomain(mail_moderationdto);
		mail_moderationService.create(domain);
        Mail_moderationDTO dto = mail_moderationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_moderationMapping.toDomain(#mail_moderationdtos),'iBizBusinessCentral-Mail_moderation-Create')")
    @ApiOperation(value = "批量新建渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "批量新建渠道黑/白名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_moderations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_moderationDTO> mail_moderationdtos) {
        mail_moderationService.createBatch(mail_moderationMapping.toDomain(mail_moderationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "mail_moderation" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.mail_moderationService.get(#mail_moderation_id),'iBizBusinessCentral-Mail_moderation-Update')")
    @ApiOperation(value = "更新渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "更新渠道黑/白名单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_moderations/{mail_moderation_id}")
    public ResponseEntity<Mail_moderationDTO> update(@PathVariable("mail_moderation_id") Long mail_moderation_id, @RequestBody Mail_moderationDTO mail_moderationdto) {
		Mail_moderation domain  = mail_moderationMapping.toDomain(mail_moderationdto);
        domain .setId(mail_moderation_id);
		mail_moderationService.update(domain );
		Mail_moderationDTO dto = mail_moderationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.mail_moderationService.getMailModerationByEntities(this.mail_moderationMapping.toDomain(#mail_moderationdtos)),'iBizBusinessCentral-Mail_moderation-Update')")
    @ApiOperation(value = "批量更新渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "批量更新渠道黑/白名单")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_moderations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_moderationDTO> mail_moderationdtos) {
        mail_moderationService.updateBatch(mail_moderationMapping.toDomain(mail_moderationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.mail_moderationService.get(#mail_moderation_id),'iBizBusinessCentral-Mail_moderation-Remove')")
    @ApiOperation(value = "删除渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "删除渠道黑/白名单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_moderations/{mail_moderation_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("mail_moderation_id") Long mail_moderation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_moderationService.remove(mail_moderation_id));
    }

    @PreAuthorize("hasPermission(this.mail_moderationService.getMailModerationByIds(#ids),'iBizBusinessCentral-Mail_moderation-Remove')")
    @ApiOperation(value = "批量删除渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "批量删除渠道黑/白名单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_moderations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        mail_moderationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.mail_moderationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Mail_moderation-Get')")
    @ApiOperation(value = "获取渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "获取渠道黑/白名单")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_moderations/{mail_moderation_id}")
    public ResponseEntity<Mail_moderationDTO> get(@PathVariable("mail_moderation_id") Long mail_moderation_id) {
        Mail_moderation domain = mail_moderationService.get(mail_moderation_id);
        Mail_moderationDTO dto = mail_moderationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取渠道黑/白名单草稿", tags = {"渠道黑/白名单" },  notes = "获取渠道黑/白名单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_moderations/getdraft")
    public ResponseEntity<Mail_moderationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(mail_moderationMapping.toDto(mail_moderationService.getDraft(new Mail_moderation())));
    }

    @ApiOperation(value = "检查渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "检查渠道黑/白名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_moderations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Mail_moderationDTO mail_moderationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(mail_moderationService.checkKey(mail_moderationMapping.toDomain(mail_moderationdto)));
    }

    @PreAuthorize("hasPermission(this.mail_moderationMapping.toDomain(#mail_moderationdto),'iBizBusinessCentral-Mail_moderation-Save')")
    @ApiOperation(value = "保存渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "保存渠道黑/白名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_moderations/save")
    public ResponseEntity<Boolean> save(@RequestBody Mail_moderationDTO mail_moderationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(mail_moderationService.save(mail_moderationMapping.toDomain(mail_moderationdto)));
    }

    @PreAuthorize("hasPermission(this.mail_moderationMapping.toDomain(#mail_moderationdtos),'iBizBusinessCentral-Mail_moderation-Save')")
    @ApiOperation(value = "批量保存渠道黑/白名单", tags = {"渠道黑/白名单" },  notes = "批量保存渠道黑/白名单")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_moderations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Mail_moderationDTO> mail_moderationdtos) {
        mail_moderationService.saveBatch(mail_moderationMapping.toDomain(mail_moderationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_moderation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_moderation-Get')")
	@ApiOperation(value = "获取数据集", tags = {"渠道黑/白名单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/mail_moderations/fetchdefault")
	public ResponseEntity<List<Mail_moderationDTO>> fetchDefault(Mail_moderationSearchContext context) {
        Page<Mail_moderation> domains = mail_moderationService.searchDefault(context) ;
        List<Mail_moderationDTO> list = mail_moderationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Mail_moderation-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Mail_moderation-Get')")
	@ApiOperation(value = "查询数据集", tags = {"渠道黑/白名单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/mail_moderations/searchdefault")
	public ResponseEntity<Page<Mail_moderationDTO>> searchDefault(@RequestBody Mail_moderationSearchContext context) {
        Page<Mail_moderation> domains = mail_moderationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_moderationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


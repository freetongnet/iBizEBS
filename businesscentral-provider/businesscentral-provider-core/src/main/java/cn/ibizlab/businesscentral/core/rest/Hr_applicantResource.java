package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicantService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_applicantSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"申请人" })
@RestController("Core-hr_applicant")
@RequestMapping("")
public class Hr_applicantResource {

    @Autowired
    public IHr_applicantService hr_applicantService;

    @Autowired
    @Lazy
    public Hr_applicantMapping hr_applicantMapping;

    @PreAuthorize("hasPermission(this.hr_applicantMapping.toDomain(#hr_applicantdto),'iBizBusinessCentral-Hr_applicant-Create')")
    @ApiOperation(value = "新建申请人", tags = {"申请人" },  notes = "新建申请人")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicants")
    public ResponseEntity<Hr_applicantDTO> create(@Validated @RequestBody Hr_applicantDTO hr_applicantdto) {
        Hr_applicant domain = hr_applicantMapping.toDomain(hr_applicantdto);
		hr_applicantService.create(domain);
        Hr_applicantDTO dto = hr_applicantMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_applicantMapping.toDomain(#hr_applicantdtos),'iBizBusinessCentral-Hr_applicant-Create')")
    @ApiOperation(value = "批量新建申请人", tags = {"申请人" },  notes = "批量新建申请人")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicants/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_applicantDTO> hr_applicantdtos) {
        hr_applicantService.createBatch(hr_applicantMapping.toDomain(hr_applicantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_applicant" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_applicantService.get(#hr_applicant_id),'iBizBusinessCentral-Hr_applicant-Update')")
    @ApiOperation(value = "更新申请人", tags = {"申请人" },  notes = "更新申请人")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_applicants/{hr_applicant_id}")
    public ResponseEntity<Hr_applicantDTO> update(@PathVariable("hr_applicant_id") Long hr_applicant_id, @RequestBody Hr_applicantDTO hr_applicantdto) {
		Hr_applicant domain  = hr_applicantMapping.toDomain(hr_applicantdto);
        domain .setId(hr_applicant_id);
		hr_applicantService.update(domain );
		Hr_applicantDTO dto = hr_applicantMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_applicantService.getHrApplicantByEntities(this.hr_applicantMapping.toDomain(#hr_applicantdtos)),'iBizBusinessCentral-Hr_applicant-Update')")
    @ApiOperation(value = "批量更新申请人", tags = {"申请人" },  notes = "批量更新申请人")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_applicants/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_applicantDTO> hr_applicantdtos) {
        hr_applicantService.updateBatch(hr_applicantMapping.toDomain(hr_applicantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_applicantService.get(#hr_applicant_id),'iBizBusinessCentral-Hr_applicant-Remove')")
    @ApiOperation(value = "删除申请人", tags = {"申请人" },  notes = "删除申请人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicants/{hr_applicant_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_applicant_id") Long hr_applicant_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_applicantService.remove(hr_applicant_id));
    }

    @PreAuthorize("hasPermission(this.hr_applicantService.getHrApplicantByIds(#ids),'iBizBusinessCentral-Hr_applicant-Remove')")
    @ApiOperation(value = "批量删除申请人", tags = {"申请人" },  notes = "批量删除申请人")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicants/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_applicantService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_applicantMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_applicant-Get')")
    @ApiOperation(value = "获取申请人", tags = {"申请人" },  notes = "获取申请人")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_applicants/{hr_applicant_id}")
    public ResponseEntity<Hr_applicantDTO> get(@PathVariable("hr_applicant_id") Long hr_applicant_id) {
        Hr_applicant domain = hr_applicantService.get(hr_applicant_id);
        Hr_applicantDTO dto = hr_applicantMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取申请人草稿", tags = {"申请人" },  notes = "获取申请人草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_applicants/getdraft")
    public ResponseEntity<Hr_applicantDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_applicantMapping.toDto(hr_applicantService.getDraft(new Hr_applicant())));
    }

    @ApiOperation(value = "检查申请人", tags = {"申请人" },  notes = "检查申请人")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicants/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_applicantDTO hr_applicantdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_applicantService.checkKey(hr_applicantMapping.toDomain(hr_applicantdto)));
    }

    @PreAuthorize("hasPermission(this.hr_applicantMapping.toDomain(#hr_applicantdto),'iBizBusinessCentral-Hr_applicant-Save')")
    @ApiOperation(value = "保存申请人", tags = {"申请人" },  notes = "保存申请人")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicants/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_applicantDTO hr_applicantdto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_applicantService.save(hr_applicantMapping.toDomain(hr_applicantdto)));
    }

    @PreAuthorize("hasPermission(this.hr_applicantMapping.toDomain(#hr_applicantdtos),'iBizBusinessCentral-Hr_applicant-Save')")
    @ApiOperation(value = "批量保存申请人", tags = {"申请人" },  notes = "批量保存申请人")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicants/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_applicantDTO> hr_applicantdtos) {
        hr_applicantService.saveBatch(hr_applicantMapping.toDomain(hr_applicantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_applicant-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_applicant-Get')")
	@ApiOperation(value = "获取数据集", tags = {"申请人" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_applicants/fetchdefault")
	public ResponseEntity<List<Hr_applicantDTO>> fetchDefault(Hr_applicantSearchContext context) {
        Page<Hr_applicant> domains = hr_applicantService.searchDefault(context) ;
        List<Hr_applicantDTO> list = hr_applicantMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_applicant-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_applicant-Get')")
	@ApiOperation(value = "查询数据集", tags = {"申请人" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_applicants/searchdefault")
	public ResponseEntity<Page<Hr_applicantDTO>> searchDefault(@RequestBody Hr_applicantSearchContext context) {
        Page<Hr_applicant> domains = hr_applicantService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_applicantMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


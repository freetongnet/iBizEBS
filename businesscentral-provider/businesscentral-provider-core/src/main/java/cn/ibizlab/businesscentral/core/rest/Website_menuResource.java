package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_menu;
import cn.ibizlab.businesscentral.core.odoo_website.service.IWebsite_menuService;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_menuSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"网站菜单" })
@RestController("Core-website_menu")
@RequestMapping("")
public class Website_menuResource {

    @Autowired
    public IWebsite_menuService website_menuService;

    @Autowired
    @Lazy
    public Website_menuMapping website_menuMapping;

    @PreAuthorize("hasPermission(this.website_menuMapping.toDomain(#website_menudto),'iBizBusinessCentral-Website_menu-Create')")
    @ApiOperation(value = "新建网站菜单", tags = {"网站菜单" },  notes = "新建网站菜单")
	@RequestMapping(method = RequestMethod.POST, value = "/website_menus")
    public ResponseEntity<Website_menuDTO> create(@Validated @RequestBody Website_menuDTO website_menudto) {
        Website_menu domain = website_menuMapping.toDomain(website_menudto);
		website_menuService.create(domain);
        Website_menuDTO dto = website_menuMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.website_menuMapping.toDomain(#website_menudtos),'iBizBusinessCentral-Website_menu-Create')")
    @ApiOperation(value = "批量新建网站菜单", tags = {"网站菜单" },  notes = "批量新建网站菜单")
	@RequestMapping(method = RequestMethod.POST, value = "/website_menus/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_menuDTO> website_menudtos) {
        website_menuService.createBatch(website_menuMapping.toDomain(website_menudtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "website_menu" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.website_menuService.get(#website_menu_id),'iBizBusinessCentral-Website_menu-Update')")
    @ApiOperation(value = "更新网站菜单", tags = {"网站菜单" },  notes = "更新网站菜单")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_menus/{website_menu_id}")
    public ResponseEntity<Website_menuDTO> update(@PathVariable("website_menu_id") Long website_menu_id, @RequestBody Website_menuDTO website_menudto) {
		Website_menu domain  = website_menuMapping.toDomain(website_menudto);
        domain .setId(website_menu_id);
		website_menuService.update(domain );
		Website_menuDTO dto = website_menuMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.website_menuService.getWebsiteMenuByEntities(this.website_menuMapping.toDomain(#website_menudtos)),'iBizBusinessCentral-Website_menu-Update')")
    @ApiOperation(value = "批量更新网站菜单", tags = {"网站菜单" },  notes = "批量更新网站菜单")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_menus/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_menuDTO> website_menudtos) {
        website_menuService.updateBatch(website_menuMapping.toDomain(website_menudtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.website_menuService.get(#website_menu_id),'iBizBusinessCentral-Website_menu-Remove')")
    @ApiOperation(value = "删除网站菜单", tags = {"网站菜单" },  notes = "删除网站菜单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_menus/{website_menu_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("website_menu_id") Long website_menu_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_menuService.remove(website_menu_id));
    }

    @PreAuthorize("hasPermission(this.website_menuService.getWebsiteMenuByIds(#ids),'iBizBusinessCentral-Website_menu-Remove')")
    @ApiOperation(value = "批量删除网站菜单", tags = {"网站菜单" },  notes = "批量删除网站菜单")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_menus/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        website_menuService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.website_menuMapping.toDomain(returnObject.body),'iBizBusinessCentral-Website_menu-Get')")
    @ApiOperation(value = "获取网站菜单", tags = {"网站菜单" },  notes = "获取网站菜单")
	@RequestMapping(method = RequestMethod.GET, value = "/website_menus/{website_menu_id}")
    public ResponseEntity<Website_menuDTO> get(@PathVariable("website_menu_id") Long website_menu_id) {
        Website_menu domain = website_menuService.get(website_menu_id);
        Website_menuDTO dto = website_menuMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取网站菜单草稿", tags = {"网站菜单" },  notes = "获取网站菜单草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/website_menus/getdraft")
    public ResponseEntity<Website_menuDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(website_menuMapping.toDto(website_menuService.getDraft(new Website_menu())));
    }

    @ApiOperation(value = "检查网站菜单", tags = {"网站菜单" },  notes = "检查网站菜单")
	@RequestMapping(method = RequestMethod.POST, value = "/website_menus/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Website_menuDTO website_menudto) {
        return  ResponseEntity.status(HttpStatus.OK).body(website_menuService.checkKey(website_menuMapping.toDomain(website_menudto)));
    }

    @PreAuthorize("hasPermission(this.website_menuMapping.toDomain(#website_menudto),'iBizBusinessCentral-Website_menu-Save')")
    @ApiOperation(value = "保存网站菜单", tags = {"网站菜单" },  notes = "保存网站菜单")
	@RequestMapping(method = RequestMethod.POST, value = "/website_menus/save")
    public ResponseEntity<Boolean> save(@RequestBody Website_menuDTO website_menudto) {
        return ResponseEntity.status(HttpStatus.OK).body(website_menuService.save(website_menuMapping.toDomain(website_menudto)));
    }

    @PreAuthorize("hasPermission(this.website_menuMapping.toDomain(#website_menudtos),'iBizBusinessCentral-Website_menu-Save')")
    @ApiOperation(value = "批量保存网站菜单", tags = {"网站菜单" },  notes = "批量保存网站菜单")
	@RequestMapping(method = RequestMethod.POST, value = "/website_menus/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Website_menuDTO> website_menudtos) {
        website_menuService.saveBatch(website_menuMapping.toDomain(website_menudtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_menu-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Website_menu-Get')")
	@ApiOperation(value = "获取数据集", tags = {"网站菜单" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/website_menus/fetchdefault")
	public ResponseEntity<List<Website_menuDTO>> fetchDefault(Website_menuSearchContext context) {
        Page<Website_menu> domains = website_menuService.searchDefault(context) ;
        List<Website_menuDTO> list = website_menuMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Website_menu-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Website_menu-Get')")
	@ApiOperation(value = "查询数据集", tags = {"网站菜单" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/website_menus/searchdefault")
	public ResponseEntity<Page<Website_menuDTO>> searchDefault(@RequestBody Website_menuSearchContext context) {
        Page<Website_menu> domains = website_menuService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_menuMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


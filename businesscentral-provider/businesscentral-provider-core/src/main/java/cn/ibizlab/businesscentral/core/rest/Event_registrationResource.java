package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_registration;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_registrationSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"事件记录" })
@RestController("Core-event_registration")
@RequestMapping("")
public class Event_registrationResource {

    @Autowired
    public IEvent_registrationService event_registrationService;

    @Autowired
    @Lazy
    public Event_registrationMapping event_registrationMapping;

    @PreAuthorize("hasPermission(this.event_registrationMapping.toDomain(#event_registrationdto),'iBizBusinessCentral-Event_registration-Create')")
    @ApiOperation(value = "新建事件记录", tags = {"事件记录" },  notes = "新建事件记录")
	@RequestMapping(method = RequestMethod.POST, value = "/event_registrations")
    public ResponseEntity<Event_registrationDTO> create(@Validated @RequestBody Event_registrationDTO event_registrationdto) {
        Event_registration domain = event_registrationMapping.toDomain(event_registrationdto);
		event_registrationService.create(domain);
        Event_registrationDTO dto = event_registrationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_registrationMapping.toDomain(#event_registrationdtos),'iBizBusinessCentral-Event_registration-Create')")
    @ApiOperation(value = "批量新建事件记录", tags = {"事件记录" },  notes = "批量新建事件记录")
	@RequestMapping(method = RequestMethod.POST, value = "/event_registrations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_registrationDTO> event_registrationdtos) {
        event_registrationService.createBatch(event_registrationMapping.toDomain(event_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "event_registration" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.event_registrationService.get(#event_registration_id),'iBizBusinessCentral-Event_registration-Update')")
    @ApiOperation(value = "更新事件记录", tags = {"事件记录" },  notes = "更新事件记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_registrations/{event_registration_id}")
    public ResponseEntity<Event_registrationDTO> update(@PathVariable("event_registration_id") Long event_registration_id, @RequestBody Event_registrationDTO event_registrationdto) {
		Event_registration domain  = event_registrationMapping.toDomain(event_registrationdto);
        domain .setId(event_registration_id);
		event_registrationService.update(domain );
		Event_registrationDTO dto = event_registrationMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.event_registrationService.getEventRegistrationByEntities(this.event_registrationMapping.toDomain(#event_registrationdtos)),'iBizBusinessCentral-Event_registration-Update')")
    @ApiOperation(value = "批量更新事件记录", tags = {"事件记录" },  notes = "批量更新事件记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_registrations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_registrationDTO> event_registrationdtos) {
        event_registrationService.updateBatch(event_registrationMapping.toDomain(event_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.event_registrationService.get(#event_registration_id),'iBizBusinessCentral-Event_registration-Remove')")
    @ApiOperation(value = "删除事件记录", tags = {"事件记录" },  notes = "删除事件记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_registrations/{event_registration_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("event_registration_id") Long event_registration_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_registrationService.remove(event_registration_id));
    }

    @PreAuthorize("hasPermission(this.event_registrationService.getEventRegistrationByIds(#ids),'iBizBusinessCentral-Event_registration-Remove')")
    @ApiOperation(value = "批量删除事件记录", tags = {"事件记录" },  notes = "批量删除事件记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_registrations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        event_registrationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.event_registrationMapping.toDomain(returnObject.body),'iBizBusinessCentral-Event_registration-Get')")
    @ApiOperation(value = "获取事件记录", tags = {"事件记录" },  notes = "获取事件记录")
	@RequestMapping(method = RequestMethod.GET, value = "/event_registrations/{event_registration_id}")
    public ResponseEntity<Event_registrationDTO> get(@PathVariable("event_registration_id") Long event_registration_id) {
        Event_registration domain = event_registrationService.get(event_registration_id);
        Event_registrationDTO dto = event_registrationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取事件记录草稿", tags = {"事件记录" },  notes = "获取事件记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/event_registrations/getdraft")
    public ResponseEntity<Event_registrationDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(event_registrationMapping.toDto(event_registrationService.getDraft(new Event_registration())));
    }

    @ApiOperation(value = "检查事件记录", tags = {"事件记录" },  notes = "检查事件记录")
	@RequestMapping(method = RequestMethod.POST, value = "/event_registrations/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Event_registrationDTO event_registrationdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(event_registrationService.checkKey(event_registrationMapping.toDomain(event_registrationdto)));
    }

    @PreAuthorize("hasPermission(this.event_registrationMapping.toDomain(#event_registrationdto),'iBizBusinessCentral-Event_registration-Save')")
    @ApiOperation(value = "保存事件记录", tags = {"事件记录" },  notes = "保存事件记录")
	@RequestMapping(method = RequestMethod.POST, value = "/event_registrations/save")
    public ResponseEntity<Boolean> save(@RequestBody Event_registrationDTO event_registrationdto) {
        return ResponseEntity.status(HttpStatus.OK).body(event_registrationService.save(event_registrationMapping.toDomain(event_registrationdto)));
    }

    @PreAuthorize("hasPermission(this.event_registrationMapping.toDomain(#event_registrationdtos),'iBizBusinessCentral-Event_registration-Save')")
    @ApiOperation(value = "批量保存事件记录", tags = {"事件记录" },  notes = "批量保存事件记录")
	@RequestMapping(method = RequestMethod.POST, value = "/event_registrations/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Event_registrationDTO> event_registrationdtos) {
        event_registrationService.saveBatch(event_registrationMapping.toDomain(event_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_registration-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_registration-Get')")
	@ApiOperation(value = "获取数据集", tags = {"事件记录" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/event_registrations/fetchdefault")
	public ResponseEntity<List<Event_registrationDTO>> fetchDefault(Event_registrationSearchContext context) {
        Page<Event_registration> domains = event_registrationService.searchDefault(context) ;
        List<Event_registrationDTO> list = event_registrationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Event_registration-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Event_registration-Get')")
	@ApiOperation(value = "查询数据集", tags = {"事件记录" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/event_registrations/searchdefault")
	public ResponseEntity<Page<Event_registrationDTO>> searchDefault(@RequestBody Event_registrationSearchContext context) {
        Page<Event_registration> domains = event_registrationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_registrationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


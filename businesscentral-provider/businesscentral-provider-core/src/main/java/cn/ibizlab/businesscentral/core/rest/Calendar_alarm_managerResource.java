package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_alarm_managerService;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"活动提醒管理" })
@RestController("Core-calendar_alarm_manager")
@RequestMapping("")
public class Calendar_alarm_managerResource {

    @Autowired
    public ICalendar_alarm_managerService calendar_alarm_managerService;

    @Autowired
    @Lazy
    public Calendar_alarm_managerMapping calendar_alarm_managerMapping;

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-Create-all')")
    @ApiOperation(value = "新建活动提醒管理", tags = {"活动提醒管理" },  notes = "新建活动提醒管理")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers")
    public ResponseEntity<Calendar_alarm_managerDTO> create(@Validated @RequestBody Calendar_alarm_managerDTO calendar_alarm_managerdto) {
        Calendar_alarm_manager domain = calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdto);
		calendar_alarm_managerService.create(domain);
        Calendar_alarm_managerDTO dto = calendar_alarm_managerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-Create-all')")
    @ApiOperation(value = "批量新建活动提醒管理", tags = {"活动提醒管理" },  notes = "批量新建活动提醒管理")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_alarm_managerDTO> calendar_alarm_managerdtos) {
        calendar_alarm_managerService.createBatch(calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-Update-all')")
    @ApiOperation(value = "更新活动提醒管理", tags = {"活动提醒管理" },  notes = "更新活动提醒管理")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarm_managers/{calendar_alarm_manager_id}")
    public ResponseEntity<Calendar_alarm_managerDTO> update(@PathVariable("calendar_alarm_manager_id") Long calendar_alarm_manager_id, @RequestBody Calendar_alarm_managerDTO calendar_alarm_managerdto) {
		Calendar_alarm_manager domain  = calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdto);
        domain .setId(calendar_alarm_manager_id);
		calendar_alarm_managerService.update(domain );
		Calendar_alarm_managerDTO dto = calendar_alarm_managerMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-Update-all')")
    @ApiOperation(value = "批量更新活动提醒管理", tags = {"活动提醒管理" },  notes = "批量更新活动提醒管理")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarm_managers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_alarm_managerDTO> calendar_alarm_managerdtos) {
        calendar_alarm_managerService.updateBatch(calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-Remove-all')")
    @ApiOperation(value = "删除活动提醒管理", tags = {"活动提醒管理" },  notes = "删除活动提醒管理")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarm_managers/{calendar_alarm_manager_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("calendar_alarm_manager_id") Long calendar_alarm_manager_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_alarm_managerService.remove(calendar_alarm_manager_id));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-Remove-all')")
    @ApiOperation(value = "批量删除活动提醒管理", tags = {"活动提醒管理" },  notes = "批量删除活动提醒管理")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarm_managers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        calendar_alarm_managerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-Get-all')")
    @ApiOperation(value = "获取活动提醒管理", tags = {"活动提醒管理" },  notes = "获取活动提醒管理")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_alarm_managers/{calendar_alarm_manager_id}")
    public ResponseEntity<Calendar_alarm_managerDTO> get(@PathVariable("calendar_alarm_manager_id") Long calendar_alarm_manager_id) {
        Calendar_alarm_manager domain = calendar_alarm_managerService.get(calendar_alarm_manager_id);
        Calendar_alarm_managerDTO dto = calendar_alarm_managerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取活动提醒管理草稿", tags = {"活动提醒管理" },  notes = "获取活动提醒管理草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_alarm_managers/getdraft")
    public ResponseEntity<Calendar_alarm_managerDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_alarm_managerMapping.toDto(calendar_alarm_managerService.getDraft(new Calendar_alarm_manager())));
    }

    @ApiOperation(value = "检查活动提醒管理", tags = {"活动提醒管理" },  notes = "检查活动提醒管理")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Calendar_alarm_managerDTO calendar_alarm_managerdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(calendar_alarm_managerService.checkKey(calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-Save-all')")
    @ApiOperation(value = "保存活动提醒管理", tags = {"活动提醒管理" },  notes = "保存活动提醒管理")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/save")
    public ResponseEntity<Boolean> save(@RequestBody Calendar_alarm_managerDTO calendar_alarm_managerdto) {
        return ResponseEntity.status(HttpStatus.OK).body(calendar_alarm_managerService.save(calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-Save-all')")
    @ApiOperation(value = "批量保存活动提醒管理", tags = {"活动提醒管理" },  notes = "批量保存活动提醒管理")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Calendar_alarm_managerDTO> calendar_alarm_managerdtos) {
        calendar_alarm_managerService.saveBatch(calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-searchDefault-all')")
	@ApiOperation(value = "获取数据集", tags = {"活动提醒管理" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_alarm_managers/fetchdefault")
	public ResponseEntity<List<Calendar_alarm_managerDTO>> fetchDefault(Calendar_alarm_managerSearchContext context) {
        Page<Calendar_alarm_manager> domains = calendar_alarm_managerService.searchDefault(context) ;
        List<Calendar_alarm_managerDTO> list = calendar_alarm_managerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Calendar_alarm_manager-searchDefault-all')")
	@ApiOperation(value = "查询数据集", tags = {"活动提醒管理" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/calendar_alarm_managers/searchdefault")
	public ResponseEntity<Page<Calendar_alarm_managerDTO>> searchDefault(@RequestBody Calendar_alarm_managerSearchContext context) {
        Page<Calendar_alarm_manager> domains = calendar_alarm_managerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_alarm_managerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


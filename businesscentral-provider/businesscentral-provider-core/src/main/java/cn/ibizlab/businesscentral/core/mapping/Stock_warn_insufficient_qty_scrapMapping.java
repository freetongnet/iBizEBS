package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap;
import cn.ibizlab.businesscentral.core.dto.Stock_warn_insufficient_qty_scrapDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreStock_warn_insufficient_qty_scrapMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_warn_insufficient_qty_scrapMapping extends MappingBase<Stock_warn_insufficient_qty_scrapDTO, Stock_warn_insufficient_qty_scrap> {


}


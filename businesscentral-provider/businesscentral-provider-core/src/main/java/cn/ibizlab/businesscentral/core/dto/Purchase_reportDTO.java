package cn.ibizlab.businesscentral.core.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.businesscentral.util.domain.DTOBase;
import cn.ibizlab.businesscentral.util.domain.DTOClient;
import lombok.Data;

/**
 * 服务DTO对象[Purchase_reportDTO]
 */
@Data
public class Purchase_reportDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DELAY_PASS]
     *
     */
    @JSONField(name = "delay_pass")
    @JsonProperty("delay_pass")
    private Double delayPass;

    /**
     * 属性 [DATE_APPROVE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_approve" , format="yyyy-MM-dd")
    @JsonProperty("date_approve")
    private Timestamp dateApprove;

    /**
     * 属性 [VOLUME]
     *
     */
    @JSONField(name = "volume")
    @JsonProperty("volume")
    private Double volume;

    /**
     * 属性 [NBR_LINES]
     *
     */
    @JSONField(name = "nbr_lines")
    @JsonProperty("nbr_lines")
    private Integer nbrLines;

    /**
     * 属性 [NEGOCIATION]
     *
     */
    @JSONField(name = "negociation")
    @JsonProperty("negociation")
    private Double negociation;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String state;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    private String displayName;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private Double priceTotal;

    /**
     * 属性 [WEIGHT]
     *
     */
    @JSONField(name = "weight")
    @JsonProperty("weight")
    private Double weight;

    /**
     * 属性 [DATE_ORDER]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_order" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_order")
    private Timestamp dateOrder;

    /**
     * 属性 [PRICE_AVERAGE]
     *
     */
    @JSONField(name = "price_average")
    @JsonProperty("price_average")
    private Double priceAverage;

    /**
     * 属性 [DELAY]
     *
     */
    @JSONField(name = "delay")
    @JsonProperty("delay")
    private Double delay;

    /**
     * 属性 [UNIT_QUANTITY]
     *
     */
    @JSONField(name = "unit_quantity")
    @JsonProperty("unit_quantity")
    private Double unitQuantity;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PRICE_STANDARD]
     *
     */
    @JSONField(name = "price_standard")
    @JsonProperty("price_standard")
    private Double priceStandard;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String categoryIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String companyIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String partnerIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String currencyIdText;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @JSONField(name = "product_tmpl_id_text")
    @JsonProperty("product_tmpl_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productTmplIdText;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String countryIdText;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String pickingTypeIdText;

    /**
     * 属性 [FISCAL_POSITION_ID_TEXT]
     *
     */
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String fiscalPositionIdText;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID_TEXT]
     *
     */
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String accountAnalyticIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String userIdText;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String commercialPartnerIdText;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productUomText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    private String productIdText;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long categoryId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long companyId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long currencyId;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pickingTypeId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long commercialPartnerId;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productTmplId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long partnerId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long productId;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "[参考计量单位]不允许为空!")
    private Long productUom;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countryId;

    /**
     * 属性 [FISCAL_POSITION_ID]
     *
     */
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long fiscalPositionId;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accountAnalyticId;


    /**
     * 设置 [DELAY_PASS]
     */
    public void setDelayPass(Double  delayPass){
        this.delayPass = delayPass ;
        this.modify("delay_pass",delayPass);
    }

    /**
     * 设置 [DATE_APPROVE]
     */
    public void setDateApprove(Timestamp  dateApprove){
        this.dateApprove = dateApprove ;
        this.modify("date_approve",dateApprove);
    }

    /**
     * 设置 [VOLUME]
     */
    public void setVolume(Double  volume){
        this.volume = volume ;
        this.modify("volume",volume);
    }

    /**
     * 设置 [NBR_LINES]
     */
    public void setNbrLines(Integer  nbrLines){
        this.nbrLines = nbrLines ;
        this.modify("nbr_lines",nbrLines);
    }

    /**
     * 设置 [NEGOCIATION]
     */
    public void setNegociation(Double  negociation){
        this.negociation = negociation ;
        this.modify("negociation",negociation);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    public void setPriceTotal(Double  priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }

    /**
     * 设置 [WEIGHT]
     */
    public void setWeight(Double  weight){
        this.weight = weight ;
        this.modify("weight",weight);
    }

    /**
     * 设置 [DATE_ORDER]
     */
    public void setDateOrder(Timestamp  dateOrder){
        this.dateOrder = dateOrder ;
        this.modify("date_order",dateOrder);
    }

    /**
     * 设置 [PRICE_AVERAGE]
     */
    public void setPriceAverage(Double  priceAverage){
        this.priceAverage = priceAverage ;
        this.modify("price_average",priceAverage);
    }

    /**
     * 设置 [DELAY]
     */
    public void setDelay(Double  delay){
        this.delay = delay ;
        this.modify("delay",delay);
    }

    /**
     * 设置 [UNIT_QUANTITY]
     */
    public void setUnitQuantity(Double  unitQuantity){
        this.unitQuantity = unitQuantity ;
        this.modify("unit_quantity",unitQuantity);
    }

    /**
     * 设置 [PRICE_STANDARD]
     */
    public void setPriceStandard(Double  priceStandard){
        this.priceStandard = priceStandard ;
        this.modify("price_standard",priceStandard);
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    public void setCategoryId(Long  categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Long  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Long  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    public void setPickingTypeId(Long  pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Long  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    public void setCommercialPartnerId(Long  commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    public void setProductTmplId(Long  productTmplId){
        this.productTmplId = productTmplId ;
        this.modify("product_tmpl_id",productTmplId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Long  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Long  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    public void setProductUom(Long  productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Long  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [FISCAL_POSITION_ID]
     */
    public void setFiscalPositionId(Long  fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    public void setAccountAnalyticId(Long  accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }


}



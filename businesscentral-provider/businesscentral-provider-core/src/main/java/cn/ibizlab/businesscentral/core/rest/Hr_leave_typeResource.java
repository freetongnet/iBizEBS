package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_typeService;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leave_typeSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"休假类型" })
@RestController("Core-hr_leave_type")
@RequestMapping("")
public class Hr_leave_typeResource {

    @Autowired
    public IHr_leave_typeService hr_leave_typeService;

    @Autowired
    @Lazy
    public Hr_leave_typeMapping hr_leave_typeMapping;

    @PreAuthorize("hasPermission(this.hr_leave_typeMapping.toDomain(#hr_leave_typedto),'iBizBusinessCentral-Hr_leave_type-Create')")
    @ApiOperation(value = "新建休假类型", tags = {"休假类型" },  notes = "新建休假类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types")
    public ResponseEntity<Hr_leave_typeDTO> create(@Validated @RequestBody Hr_leave_typeDTO hr_leave_typedto) {
        Hr_leave_type domain = hr_leave_typeMapping.toDomain(hr_leave_typedto);
		hr_leave_typeService.create(domain);
        Hr_leave_typeDTO dto = hr_leave_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_leave_typeMapping.toDomain(#hr_leave_typedtos),'iBizBusinessCentral-Hr_leave_type-Create')")
    @ApiOperation(value = "批量新建休假类型", tags = {"休假类型" },  notes = "批量新建休假类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_leave_typeDTO> hr_leave_typedtos) {
        hr_leave_typeService.createBatch(hr_leave_typeMapping.toDomain(hr_leave_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "hr_leave_type" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.hr_leave_typeService.get(#hr_leave_type_id),'iBizBusinessCentral-Hr_leave_type-Update')")
    @ApiOperation(value = "更新休假类型", tags = {"休假类型" },  notes = "更新休假类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_types/{hr_leave_type_id}")
    public ResponseEntity<Hr_leave_typeDTO> update(@PathVariable("hr_leave_type_id") Long hr_leave_type_id, @RequestBody Hr_leave_typeDTO hr_leave_typedto) {
		Hr_leave_type domain  = hr_leave_typeMapping.toDomain(hr_leave_typedto);
        domain .setId(hr_leave_type_id);
		hr_leave_typeService.update(domain );
		Hr_leave_typeDTO dto = hr_leave_typeMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.hr_leave_typeService.getHrLeaveTypeByEntities(this.hr_leave_typeMapping.toDomain(#hr_leave_typedtos)),'iBizBusinessCentral-Hr_leave_type-Update')")
    @ApiOperation(value = "批量更新休假类型", tags = {"休假类型" },  notes = "批量更新休假类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leave_typeDTO> hr_leave_typedtos) {
        hr_leave_typeService.updateBatch(hr_leave_typeMapping.toDomain(hr_leave_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.hr_leave_typeService.get(#hr_leave_type_id),'iBizBusinessCentral-Hr_leave_type-Remove')")
    @ApiOperation(value = "删除休假类型", tags = {"休假类型" },  notes = "删除休假类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_types/{hr_leave_type_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_type_id") Long hr_leave_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_leave_typeService.remove(hr_leave_type_id));
    }

    @PreAuthorize("hasPermission(this.hr_leave_typeService.getHrLeaveTypeByIds(#ids),'iBizBusinessCentral-Hr_leave_type-Remove')")
    @ApiOperation(value = "批量删除休假类型", tags = {"休假类型" },  notes = "批量删除休假类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        hr_leave_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.hr_leave_typeMapping.toDomain(returnObject.body),'iBizBusinessCentral-Hr_leave_type-Get')")
    @ApiOperation(value = "获取休假类型", tags = {"休假类型" },  notes = "获取休假类型")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leave_types/{hr_leave_type_id}")
    public ResponseEntity<Hr_leave_typeDTO> get(@PathVariable("hr_leave_type_id") Long hr_leave_type_id) {
        Hr_leave_type domain = hr_leave_typeService.get(hr_leave_type_id);
        Hr_leave_typeDTO dto = hr_leave_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取休假类型草稿", tags = {"休假类型" },  notes = "获取休假类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leave_types/getdraft")
    public ResponseEntity<Hr_leave_typeDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(hr_leave_typeMapping.toDto(hr_leave_typeService.getDraft(new Hr_leave_type())));
    }

    @ApiOperation(value = "检查休假类型", tags = {"休假类型" },  notes = "检查休假类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Hr_leave_typeDTO hr_leave_typedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(hr_leave_typeService.checkKey(hr_leave_typeMapping.toDomain(hr_leave_typedto)));
    }

    @PreAuthorize("hasPermission(this.hr_leave_typeMapping.toDomain(#hr_leave_typedto),'iBizBusinessCentral-Hr_leave_type-Save')")
    @ApiOperation(value = "保存休假类型", tags = {"休假类型" },  notes = "保存休假类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types/save")
    public ResponseEntity<Boolean> save(@RequestBody Hr_leave_typeDTO hr_leave_typedto) {
        return ResponseEntity.status(HttpStatus.OK).body(hr_leave_typeService.save(hr_leave_typeMapping.toDomain(hr_leave_typedto)));
    }

    @PreAuthorize("hasPermission(this.hr_leave_typeMapping.toDomain(#hr_leave_typedtos),'iBizBusinessCentral-Hr_leave_type-Save')")
    @ApiOperation(value = "批量保存休假类型", tags = {"休假类型" },  notes = "批量保存休假类型")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Hr_leave_typeDTO> hr_leave_typedtos) {
        hr_leave_typeService.saveBatch(hr_leave_typeMapping.toDomain(hr_leave_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_leave_type-Get')")
	@ApiOperation(value = "获取数据集", tags = {"休假类型" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leave_types/fetchdefault")
	public ResponseEntity<List<Hr_leave_typeDTO>> fetchDefault(Hr_leave_typeSearchContext context) {
        Page<Hr_leave_type> domains = hr_leave_typeService.searchDefault(context) ;
        List<Hr_leave_typeDTO> list = hr_leave_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Hr_leave_type-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Hr_leave_type-Get')")
	@ApiOperation(value = "查询数据集", tags = {"休假类型" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/hr_leave_types/searchdefault")
	public ResponseEntity<Page<Hr_leave_typeDTO>> searchDefault(@RequestBody Hr_leave_typeSearchContext context) {
        Page<Hr_leave_type> domains = hr_leave_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_leave_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


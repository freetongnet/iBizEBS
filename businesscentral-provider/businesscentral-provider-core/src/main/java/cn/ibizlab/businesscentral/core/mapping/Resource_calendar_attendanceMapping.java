package cn.ibizlab.businesscentral.core.mapping;

import org.mapstruct.*;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.businesscentral.core.dto.Resource_calendar_attendanceDTO;
import cn.ibizlab.businesscentral.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},implementationName="CoreResource_calendar_attendanceMapping",
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Resource_calendar_attendanceMapping extends MappingBase<Resource_calendar_attendanceDTO, Resource_calendar_attendance> {


}


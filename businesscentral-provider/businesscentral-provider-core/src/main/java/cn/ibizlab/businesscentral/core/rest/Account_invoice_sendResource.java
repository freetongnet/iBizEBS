package cn.ibizlab.businesscentral.core.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.businesscentral.core.dto.*;
import cn.ibizlab.businesscentral.core.mapping.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_sendService;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_sendSearchContext;
import cn.ibizlab.businesscentral.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"发送会计发票" })
@RestController("Core-account_invoice_send")
@RequestMapping("")
public class Account_invoice_sendResource {

    @Autowired
    public IAccount_invoice_sendService account_invoice_sendService;

    @Autowired
    @Lazy
    public Account_invoice_sendMapping account_invoice_sendMapping;

    @PreAuthorize("hasPermission(this.account_invoice_sendMapping.toDomain(#account_invoice_senddto),'iBizBusinessCentral-Account_invoice_send-Create')")
    @ApiOperation(value = "新建发送会计发票", tags = {"发送会计发票" },  notes = "新建发送会计发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends")
    public ResponseEntity<Account_invoice_sendDTO> create(@Validated @RequestBody Account_invoice_sendDTO account_invoice_senddto) {
        Account_invoice_send domain = account_invoice_sendMapping.toDomain(account_invoice_senddto);
		account_invoice_sendService.create(domain);
        Account_invoice_sendDTO dto = account_invoice_sendMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_sendMapping.toDomain(#account_invoice_senddtos),'iBizBusinessCentral-Account_invoice_send-Create')")
    @ApiOperation(value = "批量新建发送会计发票", tags = {"发送会计发票" },  notes = "批量新建发送会计发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_sendDTO> account_invoice_senddtos) {
        account_invoice_sendService.createBatch(account_invoice_sendMapping.toDomain(account_invoice_senddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "account_invoice_send" , versionfield = "writeDate")
    @PreAuthorize("hasPermission(this.account_invoice_sendService.get(#account_invoice_send_id),'iBizBusinessCentral-Account_invoice_send-Update')")
    @ApiOperation(value = "更新发送会计发票", tags = {"发送会计发票" },  notes = "更新发送会计发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_sends/{account_invoice_send_id}")
    public ResponseEntity<Account_invoice_sendDTO> update(@PathVariable("account_invoice_send_id") Long account_invoice_send_id, @RequestBody Account_invoice_sendDTO account_invoice_senddto) {
		Account_invoice_send domain  = account_invoice_sendMapping.toDomain(account_invoice_senddto);
        domain .setId(account_invoice_send_id);
		account_invoice_sendService.update(domain );
		Account_invoice_sendDTO dto = account_invoice_sendMapping.toDto(domain );
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.account_invoice_sendService.getAccountInvoiceSendByEntities(this.account_invoice_sendMapping.toDomain(#account_invoice_senddtos)),'iBizBusinessCentral-Account_invoice_send-Update')")
    @ApiOperation(value = "批量更新发送会计发票", tags = {"发送会计发票" },  notes = "批量更新发送会计发票")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_sends/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_sendDTO> account_invoice_senddtos) {
        account_invoice_sendService.updateBatch(account_invoice_sendMapping.toDomain(account_invoice_senddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.account_invoice_sendService.get(#account_invoice_send_id),'iBizBusinessCentral-Account_invoice_send-Remove')")
    @ApiOperation(value = "删除发送会计发票", tags = {"发送会计发票" },  notes = "删除发送会计发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_sends/{account_invoice_send_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_send_id") Long account_invoice_send_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_sendService.remove(account_invoice_send_id));
    }

    @PreAuthorize("hasPermission(this.account_invoice_sendService.getAccountInvoiceSendByIds(#ids),'iBizBusinessCentral-Account_invoice_send-Remove')")
    @ApiOperation(value = "批量删除发送会计发票", tags = {"发送会计发票" },  notes = "批量删除发送会计发票")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_sends/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Long> ids) {
        account_invoice_sendService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.account_invoice_sendMapping.toDomain(returnObject.body),'iBizBusinessCentral-Account_invoice_send-Get')")
    @ApiOperation(value = "获取发送会计发票", tags = {"发送会计发票" },  notes = "获取发送会计发票")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_sends/{account_invoice_send_id}")
    public ResponseEntity<Account_invoice_sendDTO> get(@PathVariable("account_invoice_send_id") Long account_invoice_send_id) {
        Account_invoice_send domain = account_invoice_sendService.get(account_invoice_send_id);
        Account_invoice_sendDTO dto = account_invoice_sendMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取发送会计发票草稿", tags = {"发送会计发票" },  notes = "获取发送会计发票草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_sends/getdraft")
    public ResponseEntity<Account_invoice_sendDTO> getDraft() {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_sendMapping.toDto(account_invoice_sendService.getDraft(new Account_invoice_send())));
    }

    @ApiOperation(value = "检查发送会计发票", tags = {"发送会计发票" },  notes = "检查发送会计发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody Account_invoice_sendDTO account_invoice_senddto) {
        return  ResponseEntity.status(HttpStatus.OK).body(account_invoice_sendService.checkKey(account_invoice_sendMapping.toDomain(account_invoice_senddto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_sendMapping.toDomain(#account_invoice_senddto),'iBizBusinessCentral-Account_invoice_send-Save')")
    @ApiOperation(value = "保存发送会计发票", tags = {"发送会计发票" },  notes = "保存发送会计发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends/save")
    public ResponseEntity<Boolean> save(@RequestBody Account_invoice_sendDTO account_invoice_senddto) {
        return ResponseEntity.status(HttpStatus.OK).body(account_invoice_sendService.save(account_invoice_sendMapping.toDomain(account_invoice_senddto)));
    }

    @PreAuthorize("hasPermission(this.account_invoice_sendMapping.toDomain(#account_invoice_senddtos),'iBizBusinessCentral-Account_invoice_send-Save')")
    @ApiOperation(value = "批量保存发送会计发票", tags = {"发送会计发票" },  notes = "批量保存发送会计发票")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_sends/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<Account_invoice_sendDTO> account_invoice_senddtos) {
        account_invoice_sendService.saveBatch(account_invoice_sendMapping.toDomain(account_invoice_senddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_send-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_send-Get')")
	@ApiOperation(value = "获取数据集", tags = {"发送会计发票" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_sends/fetchdefault")
	public ResponseEntity<List<Account_invoice_sendDTO>> fetchDefault(Account_invoice_sendSearchContext context) {
        Page<Account_invoice_send> domains = account_invoice_sendService.searchDefault(context) ;
        List<Account_invoice_sendDTO> list = account_invoice_sendMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','iBizBusinessCentral-Account_invoice_send-searchDefault-all') and hasPermission(#context,'iBizBusinessCentral-Account_invoice_send-Get')")
	@ApiOperation(value = "查询数据集", tags = {"发送会计发票" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/account_invoice_sends/searchdefault")
	public ResponseEntity<Page<Account_invoice_sendDTO>> searchDefault(@RequestBody Account_invoice_sendSearchContext context) {
        Page<Account_invoice_send> domains = account_invoice_sendService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_sendMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


}


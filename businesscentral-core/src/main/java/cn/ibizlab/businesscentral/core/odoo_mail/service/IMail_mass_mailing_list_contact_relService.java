package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_mass_mailing_list_contact_rel] 服务对象接口
 */
public interface IMail_mass_mailing_list_contact_relService extends IService<Mail_mass_mailing_list_contact_rel>{

    boolean create(Mail_mass_mailing_list_contact_rel et) ;
    void createBatch(List<Mail_mass_mailing_list_contact_rel> list) ;
    boolean update(Mail_mass_mailing_list_contact_rel et) ;
    void updateBatch(List<Mail_mass_mailing_list_contact_rel> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_mass_mailing_list_contact_rel get(Long key) ;
    Mail_mass_mailing_list_contact_rel getDraft(Mail_mass_mailing_list_contact_rel et) ;
    boolean checkKey(Mail_mass_mailing_list_contact_rel et) ;
    boolean save(Mail_mass_mailing_list_contact_rel et) ;
    void saveBatch(List<Mail_mass_mailing_list_contact_rel> list) ;
    Page<Mail_mass_mailing_list_contact_rel> searchDefault(Mail_mass_mailing_list_contact_relSearchContext context) ;
    List<Mail_mass_mailing_list_contact_rel> selectByContactId(Long id);
    void removeByContactId(Collection<Long> ids);
    void removeByContactId(Long id);
    List<Mail_mass_mailing_list_contact_rel> selectByListId(Long id);
    void removeByListId(Collection<Long> ids);
    void removeByListId(Long id);
    List<Mail_mass_mailing_list_contact_rel> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_mass_mailing_list_contact_rel> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_mass_mailing_list_contact_rel> getMailMassMailingListContactRelByIds(List<Long> ids) ;
    List<Mail_mass_mailing_list_contact_rel> getMailMassMailingListContactRelByEntities(List<Mail_mass_mailing_list_contact_rel> entities) ;
}



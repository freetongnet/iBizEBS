package cn.ibizlab.businesscentral.core.odoo_sale.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order;
/**
 * 关系型数据实体[Sale_order] 查询条件对象
 */
@Slf4j
@Data
public class Sale_orderSearchContext extends QueryWrapperContext<Sale_order> {

	private String n_picking_policy_eq;//[送货策略]
	public void setN_picking_policy_eq(String n_picking_policy_eq) {
        this.n_picking_policy_eq = n_picking_policy_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_policy_eq)){
            this.getSearchCond().eq("picking_policy", n_picking_policy_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_invoice_status_eq;//[发票状态]
	public void setN_invoice_status_eq(String n_invoice_status_eq) {
        this.n_invoice_status_eq = n_invoice_status_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_status_eq)){
            this.getSearchCond().eq("invoice_status", n_invoice_status_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_name_like;//[订单关联]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_partner_invoice_id_text_eq;//[发票地址]
	public void setN_partner_invoice_id_text_eq(String n_partner_invoice_id_text_eq) {
        this.n_partner_invoice_id_text_eq = n_partner_invoice_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_invoice_id_text_eq)){
            this.getSearchCond().eq("partner_invoice_id_text", n_partner_invoice_id_text_eq);
        }
    }
	private String n_partner_invoice_id_text_like;//[发票地址]
	public void setN_partner_invoice_id_text_like(String n_partner_invoice_id_text_like) {
        this.n_partner_invoice_id_text_like = n_partner_invoice_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_invoice_id_text_like)){
            this.getSearchCond().like("partner_invoice_id_text", n_partner_invoice_id_text_like);
        }
    }
	private String n_incoterm_text_eq;//[贸易条款]
	public void setN_incoterm_text_eq(String n_incoterm_text_eq) {
        this.n_incoterm_text_eq = n_incoterm_text_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterm_text_eq)){
            this.getSearchCond().eq("incoterm_text", n_incoterm_text_eq);
        }
    }
	private String n_incoterm_text_like;//[贸易条款]
	public void setN_incoterm_text_like(String n_incoterm_text_like) {
        this.n_incoterm_text_like = n_incoterm_text_like;
        if(!ObjectUtils.isEmpty(this.n_incoterm_text_like)){
            this.getSearchCond().like("incoterm_text", n_incoterm_text_like);
        }
    }
	private String n_team_id_text_eq;//[销售团队]
	public void setN_team_id_text_eq(String n_team_id_text_eq) {
        this.n_team_id_text_eq = n_team_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_eq)){
            this.getSearchCond().eq("team_id_text", n_team_id_text_eq);
        }
    }
	private String n_team_id_text_like;//[销售团队]
	public void setN_team_id_text_like(String n_team_id_text_like) {
        this.n_team_id_text_like = n_team_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_like)){
            this.getSearchCond().like("team_id_text", n_team_id_text_like);
        }
    }
	private String n_analytic_account_id_text_eq;//[分析账户]
	public void setN_analytic_account_id_text_eq(String n_analytic_account_id_text_eq) {
        this.n_analytic_account_id_text_eq = n_analytic_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_eq)){
            this.getSearchCond().eq("analytic_account_id_text", n_analytic_account_id_text_eq);
        }
    }
	private String n_analytic_account_id_text_like;//[分析账户]
	public void setN_analytic_account_id_text_like(String n_analytic_account_id_text_like) {
        this.n_analytic_account_id_text_like = n_analytic_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_like)){
            this.getSearchCond().like("analytic_account_id_text", n_analytic_account_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[客户]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[客户]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_campaign_id_text_eq;//[营销]
	public void setN_campaign_id_text_eq(String n_campaign_id_text_eq) {
        this.n_campaign_id_text_eq = n_campaign_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_eq)){
            this.getSearchCond().eq("campaign_id_text", n_campaign_id_text_eq);
        }
    }
	private String n_campaign_id_text_like;//[营销]
	public void setN_campaign_id_text_like(String n_campaign_id_text_like) {
        this.n_campaign_id_text_like = n_campaign_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_like)){
            this.getSearchCond().like("campaign_id_text", n_campaign_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_partner_shipping_id_text_eq;//[送货地址]
	public void setN_partner_shipping_id_text_eq(String n_partner_shipping_id_text_eq) {
        this.n_partner_shipping_id_text_eq = n_partner_shipping_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_shipping_id_text_eq)){
            this.getSearchCond().eq("partner_shipping_id_text", n_partner_shipping_id_text_eq);
        }
    }
	private String n_partner_shipping_id_text_like;//[送货地址]
	public void setN_partner_shipping_id_text_like(String n_partner_shipping_id_text_like) {
        this.n_partner_shipping_id_text_like = n_partner_shipping_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_shipping_id_text_like)){
            this.getSearchCond().like("partner_shipping_id_text", n_partner_shipping_id_text_like);
        }
    }
	private String n_warehouse_id_text_eq;//[仓库]
	public void setN_warehouse_id_text_eq(String n_warehouse_id_text_eq) {
        this.n_warehouse_id_text_eq = n_warehouse_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_text_eq)){
            this.getSearchCond().eq("warehouse_id_text", n_warehouse_id_text_eq);
        }
    }
	private String n_warehouse_id_text_like;//[仓库]
	public void setN_warehouse_id_text_like(String n_warehouse_id_text_like) {
        this.n_warehouse_id_text_like = n_warehouse_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_text_like)){
            this.getSearchCond().like("warehouse_id_text", n_warehouse_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[销售员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[销售员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_opportunity_id_text_eq;//[商机]
	public void setN_opportunity_id_text_eq(String n_opportunity_id_text_eq) {
        this.n_opportunity_id_text_eq = n_opportunity_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_opportunity_id_text_eq)){
            this.getSearchCond().eq("opportunity_id_text", n_opportunity_id_text_eq);
        }
    }
	private String n_opportunity_id_text_like;//[商机]
	public void setN_opportunity_id_text_like(String n_opportunity_id_text_like) {
        this.n_opportunity_id_text_like = n_opportunity_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_opportunity_id_text_like)){
            this.getSearchCond().like("opportunity_id_text", n_opportunity_id_text_like);
        }
    }
	private String n_source_id_text_eq;//[来源]
	public void setN_source_id_text_eq(String n_source_id_text_eq) {
        this.n_source_id_text_eq = n_source_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_eq)){
            this.getSearchCond().eq("source_id_text", n_source_id_text_eq);
        }
    }
	private String n_source_id_text_like;//[来源]
	public void setN_source_id_text_like(String n_source_id_text_like) {
        this.n_source_id_text_like = n_source_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_like)){
            this.getSearchCond().like("source_id_text", n_source_id_text_like);
        }
    }
	private String n_medium_id_text_eq;//[媒体]
	public void setN_medium_id_text_eq(String n_medium_id_text_eq) {
        this.n_medium_id_text_eq = n_medium_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_eq)){
            this.getSearchCond().eq("medium_id_text", n_medium_id_text_eq);
        }
    }
	private String n_medium_id_text_like;//[媒体]
	public void setN_medium_id_text_like(String n_medium_id_text_like) {
        this.n_medium_id_text_like = n_medium_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_like)){
            this.getSearchCond().like("medium_id_text", n_medium_id_text_like);
        }
    }
	private String n_fiscal_position_id_text_eq;//[税科目调整]
	public void setN_fiscal_position_id_text_eq(String n_fiscal_position_id_text_eq) {
        this.n_fiscal_position_id_text_eq = n_fiscal_position_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_eq)){
            this.getSearchCond().eq("fiscal_position_id_text", n_fiscal_position_id_text_eq);
        }
    }
	private String n_fiscal_position_id_text_like;//[税科目调整]
	public void setN_fiscal_position_id_text_like(String n_fiscal_position_id_text_like) {
        this.n_fiscal_position_id_text_like = n_fiscal_position_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_text_like)){
            this.getSearchCond().like("fiscal_position_id_text", n_fiscal_position_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_pricelist_id_text_eq;//[价格表]
	public void setN_pricelist_id_text_eq(String n_pricelist_id_text_eq) {
        this.n_pricelist_id_text_eq = n_pricelist_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_eq)){
            this.getSearchCond().eq("pricelist_id_text", n_pricelist_id_text_eq);
        }
    }
	private String n_pricelist_id_text_like;//[价格表]
	public void setN_pricelist_id_text_like(String n_pricelist_id_text_like) {
        this.n_pricelist_id_text_like = n_pricelist_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_like)){
            this.getSearchCond().like("pricelist_id_text", n_pricelist_id_text_like);
        }
    }
	private String n_payment_term_id_text_eq;//[付款条款]
	public void setN_payment_term_id_text_eq(String n_payment_term_id_text_eq) {
        this.n_payment_term_id_text_eq = n_payment_term_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_text_eq)){
            this.getSearchCond().eq("payment_term_id_text", n_payment_term_id_text_eq);
        }
    }
	private String n_payment_term_id_text_like;//[付款条款]
	public void setN_payment_term_id_text_like(String n_payment_term_id_text_like) {
        this.n_payment_term_id_text_like = n_payment_term_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_text_like)){
            this.getSearchCond().like("payment_term_id_text", n_payment_term_id_text_like);
        }
    }
	private String n_sale_order_template_id_text_eq;//[报价单模板]
	public void setN_sale_order_template_id_text_eq(String n_sale_order_template_id_text_eq) {
        this.n_sale_order_template_id_text_eq = n_sale_order_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_template_id_text_eq)){
            this.getSearchCond().eq("sale_order_template_id_text", n_sale_order_template_id_text_eq);
        }
    }
	private String n_sale_order_template_id_text_like;//[报价单模板]
	public void setN_sale_order_template_id_text_like(String n_sale_order_template_id_text_like) {
        this.n_sale_order_template_id_text_like = n_sale_order_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sale_order_template_id_text_like)){
            this.getSearchCond().like("sale_order_template_id_text", n_sale_order_template_id_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_team_id_eq;//[销售团队]
	public void setN_team_id_eq(Long n_team_id_eq) {
        this.n_team_id_eq = n_team_id_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_eq)){
            this.getSearchCond().eq("team_id", n_team_id_eq);
        }
    }
	private Long n_partner_invoice_id_eq;//[发票地址]
	public void setN_partner_invoice_id_eq(Long n_partner_invoice_id_eq) {
        this.n_partner_invoice_id_eq = n_partner_invoice_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_invoice_id_eq)){
            this.getSearchCond().eq("partner_invoice_id", n_partner_invoice_id_eq);
        }
    }
	private Long n_fiscal_position_id_eq;//[税科目调整]
	public void setN_fiscal_position_id_eq(Long n_fiscal_position_id_eq) {
        this.n_fiscal_position_id_eq = n_fiscal_position_id_eq;
        if(!ObjectUtils.isEmpty(this.n_fiscal_position_id_eq)){
            this.getSearchCond().eq("fiscal_position_id", n_fiscal_position_id_eq);
        }
    }
	private Long n_partner_id_eq;//[客户]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_campaign_id_eq;//[营销]
	public void setN_campaign_id_eq(Long n_campaign_id_eq) {
        this.n_campaign_id_eq = n_campaign_id_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_eq)){
            this.getSearchCond().eq("campaign_id", n_campaign_id_eq);
        }
    }
	private Long n_source_id_eq;//[来源]
	public void setN_source_id_eq(Long n_source_id_eq) {
        this.n_source_id_eq = n_source_id_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_eq)){
            this.getSearchCond().eq("source_id", n_source_id_eq);
        }
    }
	private Long n_sale_order_template_id_eq;//[报价单模板]
	public void setN_sale_order_template_id_eq(Long n_sale_order_template_id_eq) {
        this.n_sale_order_template_id_eq = n_sale_order_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_template_id_eq)){
            this.getSearchCond().eq("sale_order_template_id", n_sale_order_template_id_eq);
        }
    }
	private Long n_user_id_eq;//[销售员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_medium_id_eq;//[媒体]
	public void setN_medium_id_eq(Long n_medium_id_eq) {
        this.n_medium_id_eq = n_medium_id_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_eq)){
            this.getSearchCond().eq("medium_id", n_medium_id_eq);
        }
    }
	private Long n_analytic_account_id_eq;//[分析账户]
	public void setN_analytic_account_id_eq(Long n_analytic_account_id_eq) {
        this.n_analytic_account_id_eq = n_analytic_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_eq)){
            this.getSearchCond().eq("analytic_account_id", n_analytic_account_id_eq);
        }
    }
	private Long n_payment_term_id_eq;//[付款条款]
	public void setN_payment_term_id_eq(Long n_payment_term_id_eq) {
        this.n_payment_term_id_eq = n_payment_term_id_eq;
        if(!ObjectUtils.isEmpty(this.n_payment_term_id_eq)){
            this.getSearchCond().eq("payment_term_id", n_payment_term_id_eq);
        }
    }
	private Long n_partner_shipping_id_eq;//[送货地址]
	public void setN_partner_shipping_id_eq(Long n_partner_shipping_id_eq) {
        this.n_partner_shipping_id_eq = n_partner_shipping_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_shipping_id_eq)){
            this.getSearchCond().eq("partner_shipping_id", n_partner_shipping_id_eq);
        }
    }
	private Long n_opportunity_id_eq;//[商机]
	public void setN_opportunity_id_eq(Long n_opportunity_id_eq) {
        this.n_opportunity_id_eq = n_opportunity_id_eq;
        if(!ObjectUtils.isEmpty(this.n_opportunity_id_eq)){
            this.getSearchCond().eq("opportunity_id", n_opportunity_id_eq);
        }
    }
	private Long n_incoterm_eq;//[贸易条款]
	public void setN_incoterm_eq(Long n_incoterm_eq) {
        this.n_incoterm_eq = n_incoterm_eq;
        if(!ObjectUtils.isEmpty(this.n_incoterm_eq)){
            this.getSearchCond().eq("incoterm", n_incoterm_eq);
        }
    }
	private Long n_pricelist_id_eq;//[价格表]
	public void setN_pricelist_id_eq(Long n_pricelist_id_eq) {
        this.n_pricelist_id_eq = n_pricelist_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_eq)){
            this.getSearchCond().eq("pricelist_id", n_pricelist_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_warehouse_id_eq;//[仓库]
	public void setN_warehouse_id_eq(Long n_warehouse_id_eq) {
        this.n_warehouse_id_eq = n_warehouse_id_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_eq)){
            this.getSearchCond().eq("warehouse_id", n_warehouse_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




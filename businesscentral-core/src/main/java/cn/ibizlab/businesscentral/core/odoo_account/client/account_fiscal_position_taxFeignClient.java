package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position_tax;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_fiscal_position_taxSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_fiscal_position_tax] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-fiscal-position-tax", fallback = account_fiscal_position_taxFallback.class)
public interface account_fiscal_position_taxFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/search")
    Page<Account_fiscal_position_tax> search(@RequestBody Account_fiscal_position_taxSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_taxes/{id}")
    Account_fiscal_position_tax get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_taxes/{id}")
    Account_fiscal_position_tax update(@PathVariable("id") Long id,@RequestBody Account_fiscal_position_tax account_fiscal_position_tax);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_position_taxes/batch")
    Boolean updateBatch(@RequestBody List<Account_fiscal_position_tax> account_fiscal_position_taxes);



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes")
    Account_fiscal_position_tax create(@RequestBody Account_fiscal_position_tax account_fiscal_position_tax);

    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/batch")
    Boolean createBatch(@RequestBody List<Account_fiscal_position_tax> account_fiscal_position_taxes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_taxes/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_position_taxes/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_taxes/select")
    Page<Account_fiscal_position_tax> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_position_taxes/getdraft")
    Account_fiscal_position_tax getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/checkkey")
    Boolean checkKey(@RequestBody Account_fiscal_position_tax account_fiscal_position_tax);


    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/save")
    Boolean save(@RequestBody Account_fiscal_position_tax account_fiscal_position_tax);

    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/savebatch")
    Boolean saveBatch(@RequestBody List<Account_fiscal_position_tax> account_fiscal_position_taxes);



    @RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_position_taxes/searchdefault")
    Page<Account_fiscal_position_tax> searchDefault(@RequestBody Account_fiscal_position_taxSearchContext context);


}

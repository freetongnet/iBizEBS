package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_attendance;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_attendanceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_attendance] 服务对象接口
 */
public interface IHr_attendanceService extends IService<Hr_attendance>{

    boolean create(Hr_attendance et) ;
    void createBatch(List<Hr_attendance> list) ;
    boolean update(Hr_attendance et) ;
    void updateBatch(List<Hr_attendance> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_attendance get(Long key) ;
    Hr_attendance getDraft(Hr_attendance et) ;
    boolean checkKey(Hr_attendance et) ;
    boolean save(Hr_attendance et) ;
    void saveBatch(List<Hr_attendance> list) ;
    Page<Hr_attendance> searchDefault(Hr_attendanceSearchContext context) ;
    List<Hr_attendance> selectByEmployeeId(Long id);
    void removeByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Hr_attendance> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_attendance> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_attendance> getHrAttendanceByIds(List<Long> ids) ;
    List<Hr_attendance> getHrAttendanceByEntities(List<Hr_attendance> entities) ;
}



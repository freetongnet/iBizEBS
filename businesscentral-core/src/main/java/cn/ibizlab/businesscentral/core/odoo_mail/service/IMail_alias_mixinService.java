package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias_mixin;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_alias_mixinSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_alias_mixin] 服务对象接口
 */
public interface IMail_alias_mixinService extends IService<Mail_alias_mixin>{

    boolean create(Mail_alias_mixin et) ;
    void createBatch(List<Mail_alias_mixin> list) ;
    boolean update(Mail_alias_mixin et) ;
    void updateBatch(List<Mail_alias_mixin> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_alias_mixin get(Long key) ;
    Mail_alias_mixin getDraft(Mail_alias_mixin et) ;
    boolean checkKey(Mail_alias_mixin et) ;
    boolean save(Mail_alias_mixin et) ;
    void saveBatch(List<Mail_alias_mixin> list) ;
    Page<Mail_alias_mixin> searchDefault(Mail_alias_mixinSearchContext context) ;
    List<Mail_alias_mixin> selectByAliasId(Long id);
    List<Mail_alias_mixin> selectByAliasId(Collection<Long> ids);
    void removeByAliasId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_alias_mixin> getMailAliasMixinByIds(List<Long> ids) ;
    List<Mail_alias_mixin> getMailAliasMixinByEntities(List<Mail_alias_mixin> entities) ;
}



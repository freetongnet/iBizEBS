package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_image;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_imageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[product_image] 服务对象接口
 */
@Component
public class product_imageFallback implements product_imageFeignClient{

    public Product_image create(Product_image product_image){
            return null;
     }
    public Boolean createBatch(List<Product_image> product_images){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Product_image update(Long id, Product_image product_image){
            return null;
     }
    public Boolean updateBatch(List<Product_image> product_images){
            return false;
     }


    public Page<Product_image> search(Product_imageSearchContext context){
            return null;
     }




    public Product_image get(Long id){
            return null;
     }



    public Page<Product_image> select(){
            return null;
     }

    public Product_image getDraft(){
            return null;
    }



    public Boolean checkKey(Product_image product_image){
            return false;
     }


    public Boolean save(Product_image product_image){
            return false;
     }
    public Boolean saveBatch(List<Product_image> product_images){
            return false;
     }

    public Page<Product_image> searchDefault(Product_imageSearchContext context){
            return null;
     }


}

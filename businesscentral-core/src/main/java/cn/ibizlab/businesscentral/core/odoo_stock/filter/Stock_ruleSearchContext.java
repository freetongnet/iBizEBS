package cn.ibizlab.businesscentral.core.odoo_stock.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule;
/**
 * 关系型数据实体[Stock_rule] 查询条件对象
 */
@Slf4j
@Data
public class Stock_ruleSearchContext extends QueryWrapperContext<Stock_rule> {

	private String n_procure_method_eq;//[移动供应方法]
	public void setN_procure_method_eq(String n_procure_method_eq) {
        this.n_procure_method_eq = n_procure_method_eq;
        if(!ObjectUtils.isEmpty(this.n_procure_method_eq)){
            this.getSearchCond().eq("procure_method", n_procure_method_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_auto_eq;//[自动移动]
	public void setN_auto_eq(String n_auto_eq) {
        this.n_auto_eq = n_auto_eq;
        if(!ObjectUtils.isEmpty(this.n_auto_eq)){
            this.getSearchCond().eq("auto", n_auto_eq);
        }
    }
	private String n_group_propagation_option_eq;//[补货组的传播]
	public void setN_group_propagation_option_eq(String n_group_propagation_option_eq) {
        this.n_group_propagation_option_eq = n_group_propagation_option_eq;
        if(!ObjectUtils.isEmpty(this.n_group_propagation_option_eq)){
            this.getSearchCond().eq("group_propagation_option", n_group_propagation_option_eq);
        }
    }
	private String n_action_eq;//[动作]
	public void setN_action_eq(String n_action_eq) {
        this.n_action_eq = n_action_eq;
        if(!ObjectUtils.isEmpty(this.n_action_eq)){
            this.getSearchCond().eq("action", n_action_eq);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_partner_address_id_text_eq;//[业务伙伴地址]
	public void setN_partner_address_id_text_eq(String n_partner_address_id_text_eq) {
        this.n_partner_address_id_text_eq = n_partner_address_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_address_id_text_eq)){
            this.getSearchCond().eq("partner_address_id_text", n_partner_address_id_text_eq);
        }
    }
	private String n_partner_address_id_text_like;//[业务伙伴地址]
	public void setN_partner_address_id_text_like(String n_partner_address_id_text_like) {
        this.n_partner_address_id_text_like = n_partner_address_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_address_id_text_like)){
            this.getSearchCond().like("partner_address_id_text", n_partner_address_id_text_like);
        }
    }
	private String n_location_id_text_eq;//[目的位置]
	public void setN_location_id_text_eq(String n_location_id_text_eq) {
        this.n_location_id_text_eq = n_location_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_eq)){
            this.getSearchCond().eq("location_id_text", n_location_id_text_eq);
        }
    }
	private String n_location_id_text_like;//[目的位置]
	public void setN_location_id_text_like(String n_location_id_text_like) {
        this.n_location_id_text_like = n_location_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_like)){
            this.getSearchCond().like("location_id_text", n_location_id_text_like);
        }
    }
	private String n_route_id_text_eq;//[路线]
	public void setN_route_id_text_eq(String n_route_id_text_eq) {
        this.n_route_id_text_eq = n_route_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_route_id_text_eq)){
            this.getSearchCond().eq("route_id_text", n_route_id_text_eq);
        }
    }
	private String n_route_id_text_like;//[路线]
	public void setN_route_id_text_like(String n_route_id_text_like) {
        this.n_route_id_text_like = n_route_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_route_id_text_like)){
            this.getSearchCond().like("route_id_text", n_route_id_text_like);
        }
    }
	private String n_picking_type_id_text_eq;//[作业类型]
	public void setN_picking_type_id_text_eq(String n_picking_type_id_text_eq) {
        this.n_picking_type_id_text_eq = n_picking_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_eq)){
            this.getSearchCond().eq("picking_type_id_text", n_picking_type_id_text_eq);
        }
    }
	private String n_picking_type_id_text_like;//[作业类型]
	public void setN_picking_type_id_text_like(String n_picking_type_id_text_like) {
        this.n_picking_type_id_text_like = n_picking_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_like)){
            this.getSearchCond().like("picking_type_id_text", n_picking_type_id_text_like);
        }
    }
	private String n_propagate_warehouse_id_text_eq;//[传播的仓库]
	public void setN_propagate_warehouse_id_text_eq(String n_propagate_warehouse_id_text_eq) {
        this.n_propagate_warehouse_id_text_eq = n_propagate_warehouse_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_propagate_warehouse_id_text_eq)){
            this.getSearchCond().eq("propagate_warehouse_id_text", n_propagate_warehouse_id_text_eq);
        }
    }
	private String n_propagate_warehouse_id_text_like;//[传播的仓库]
	public void setN_propagate_warehouse_id_text_like(String n_propagate_warehouse_id_text_like) {
        this.n_propagate_warehouse_id_text_like = n_propagate_warehouse_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_propagate_warehouse_id_text_like)){
            this.getSearchCond().like("propagate_warehouse_id_text", n_propagate_warehouse_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_location_src_id_text_eq;//[源位置]
	public void setN_location_src_id_text_eq(String n_location_src_id_text_eq) {
        this.n_location_src_id_text_eq = n_location_src_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_src_id_text_eq)){
            this.getSearchCond().eq("location_src_id_text", n_location_src_id_text_eq);
        }
    }
	private String n_location_src_id_text_like;//[源位置]
	public void setN_location_src_id_text_like(String n_location_src_id_text_like) {
        this.n_location_src_id_text_like = n_location_src_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_src_id_text_like)){
            this.getSearchCond().like("location_src_id_text", n_location_src_id_text_like);
        }
    }
	private String n_warehouse_id_text_eq;//[仓库]
	public void setN_warehouse_id_text_eq(String n_warehouse_id_text_eq) {
        this.n_warehouse_id_text_eq = n_warehouse_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_text_eq)){
            this.getSearchCond().eq("warehouse_id_text", n_warehouse_id_text_eq);
        }
    }
	private String n_warehouse_id_text_like;//[仓库]
	public void setN_warehouse_id_text_like(String n_warehouse_id_text_like) {
        this.n_warehouse_id_text_like = n_warehouse_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_text_like)){
            this.getSearchCond().like("warehouse_id_text", n_warehouse_id_text_like);
        }
    }
	private Long n_location_id_eq;//[目的位置]
	public void setN_location_id_eq(Long n_location_id_eq) {
        this.n_location_id_eq = n_location_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_eq)){
            this.getSearchCond().eq("location_id", n_location_id_eq);
        }
    }
	private Long n_partner_address_id_eq;//[业务伙伴地址]
	public void setN_partner_address_id_eq(Long n_partner_address_id_eq) {
        this.n_partner_address_id_eq = n_partner_address_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_address_id_eq)){
            this.getSearchCond().eq("partner_address_id", n_partner_address_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_warehouse_id_eq;//[仓库]
	public void setN_warehouse_id_eq(Long n_warehouse_id_eq) {
        this.n_warehouse_id_eq = n_warehouse_id_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_eq)){
            this.getSearchCond().eq("warehouse_id", n_warehouse_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_picking_type_id_eq;//[作业类型]
	public void setN_picking_type_id_eq(Long n_picking_type_id_eq) {
        this.n_picking_type_id_eq = n_picking_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_eq)){
            this.getSearchCond().eq("picking_type_id", n_picking_type_id_eq);
        }
    }
	private Long n_route_id_eq;//[路线]
	public void setN_route_id_eq(Long n_route_id_eq) {
        this.n_route_id_eq = n_route_id_eq;
        if(!ObjectUtils.isEmpty(this.n_route_id_eq)){
            this.getSearchCond().eq("route_id", n_route_id_eq);
        }
    }
	private Long n_location_src_id_eq;//[源位置]
	public void setN_location_src_id_eq(Long n_location_src_id_eq) {
        this.n_location_src_id_eq = n_location_src_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_src_id_eq)){
            this.getSearchCond().eq("location_src_id", n_location_src_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_propagate_warehouse_id_eq;//[传播的仓库]
	public void setN_propagate_warehouse_id_eq(Long n_propagate_warehouse_id_eq) {
        this.n_propagate_warehouse_id_eq = n_propagate_warehouse_id_eq;
        if(!ObjectUtils.isEmpty(this.n_propagate_warehouse_id_eq)){
            this.getSearchCond().eq("propagate_warehouse_id", n_propagate_warehouse_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




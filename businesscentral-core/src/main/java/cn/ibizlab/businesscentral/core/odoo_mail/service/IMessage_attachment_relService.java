package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Message_attachment_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Message_attachment_relSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Message_attachment_rel] 服务对象接口
 */
public interface IMessage_attachment_relService extends IService<Message_attachment_rel>{

    boolean create(Message_attachment_rel et) ;
    void createBatch(List<Message_attachment_rel> list) ;
    boolean update(Message_attachment_rel et) ;
    void updateBatch(List<Message_attachment_rel> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Message_attachment_rel get(Long key) ;
    Message_attachment_rel getDraft(Message_attachment_rel et) ;
    boolean checkKey(Message_attachment_rel et) ;
    boolean save(Message_attachment_rel et) ;
    void saveBatch(List<Message_attachment_rel> list) ;
    Page<Message_attachment_rel> searchDefault(Message_attachment_relSearchContext context) ;
    List<Message_attachment_rel> selectByAttachmentId(Long id);
    void removeByAttachmentId(Long id);
    List<Message_attachment_rel> selectByMessageId(Long id);
    void removeByMessageId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



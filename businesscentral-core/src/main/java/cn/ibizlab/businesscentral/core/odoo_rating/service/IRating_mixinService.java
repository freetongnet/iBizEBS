package cn.ibizlab.businesscentral.core.odoo_rating.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.businesscentral.core.odoo_rating.filter.Rating_mixinSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Rating_mixin] 服务对象接口
 */
public interface IRating_mixinService extends IService<Rating_mixin>{

    boolean create(Rating_mixin et) ;
    void createBatch(List<Rating_mixin> list) ;
    boolean update(Rating_mixin et) ;
    void updateBatch(List<Rating_mixin> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Rating_mixin get(Long key) ;
    Rating_mixin getDraft(Rating_mixin et) ;
    boolean checkKey(Rating_mixin et) ;
    boolean save(Rating_mixin et) ;
    void saveBatch(List<Rating_mixin> list) ;
    Page<Rating_mixin> searchDefault(Rating_mixinSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



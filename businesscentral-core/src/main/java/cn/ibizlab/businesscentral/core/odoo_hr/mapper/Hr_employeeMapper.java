package cn.ibizlab.businesscentral.core.odoo_hr.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_employeeSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Hr_employeeMapper extends BaseMapper<Hr_employee>{

    Page<Hr_employee> searchDefault(IPage page, @Param("srf") Hr_employeeSearchContext context, @Param("ew") Wrapper<Hr_employee> wrapper) ;
    Page<Hr_employee> searchMaster(IPage page, @Param("srf") Hr_employeeSearchContext context, @Param("ew") Wrapper<Hr_employee> wrapper) ;
    @Override
    Hr_employee selectById(Serializable id);
    @Override
    int insert(Hr_employee entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Hr_employee entity);
    @Override
    int update(@Param(Constants.ENTITY) Hr_employee entity, @Param("ew") Wrapper<Hr_employee> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Hr_employee> selectByLeaveManagerId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByLastAttendanceId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByDepartmentId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByCoachId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByParentId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByJobId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByResourceCalendarId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByResourceId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByCompanyId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByCountryId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByCountryOfBirth(@Param("id") Serializable id) ;

    List<Hr_employee> selectByBankAccountId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByAddressHomeId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByAddressId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByCreateUid(@Param("id") Serializable id) ;

    List<Hr_employee> selectByExpenseManagerId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByUserId(@Param("id") Serializable id) ;

    List<Hr_employee> selectByWriteUid(@Param("id") Serializable id) ;


}

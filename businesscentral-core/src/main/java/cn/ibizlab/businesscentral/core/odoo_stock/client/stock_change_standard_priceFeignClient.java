package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_change_standard_price;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_change_standard_price] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-change-standard-price", fallback = stock_change_standard_priceFallback.class)
public interface stock_change_standard_priceFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/stock_change_standard_prices/{id}")
    Stock_change_standard_price get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices")
    Stock_change_standard_price create(@RequestBody Stock_change_standard_price stock_change_standard_price);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/batch")
    Boolean createBatch(@RequestBody List<Stock_change_standard_price> stock_change_standard_prices);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_standard_prices/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_change_standard_prices/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/search")
    Page<Stock_change_standard_price> search(@RequestBody Stock_change_standard_priceSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_change_standard_prices/{id}")
    Stock_change_standard_price update(@PathVariable("id") Long id,@RequestBody Stock_change_standard_price stock_change_standard_price);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_change_standard_prices/batch")
    Boolean updateBatch(@RequestBody List<Stock_change_standard_price> stock_change_standard_prices);




    @RequestMapping(method = RequestMethod.GET, value = "/stock_change_standard_prices/select")
    Page<Stock_change_standard_price> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_change_standard_prices/getdraft")
    Stock_change_standard_price getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/checkkey")
    Boolean checkKey(@RequestBody Stock_change_standard_price stock_change_standard_price);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/save")
    Boolean save(@RequestBody Stock_change_standard_price stock_change_standard_price);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_change_standard_price> stock_change_standard_prices);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_change_standard_prices/searchdefault")
    Page<Stock_change_standard_price> searchDefault(@RequestBody Stock_change_standard_priceSearchContext context);


}

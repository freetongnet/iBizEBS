package cn.ibizlab.businesscentral.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_order_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Purchase_order_line] 服务对象接口
 */
public interface IPurchase_order_lineService extends IService<Purchase_order_line>{

    boolean create(Purchase_order_line et) ;
    void createBatch(List<Purchase_order_line> list) ;
    boolean update(Purchase_order_line et) ;
    void updateBatch(List<Purchase_order_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Purchase_order_line get(Long key) ;
    Purchase_order_line getDraft(Purchase_order_line et) ;
    Purchase_order_line calc_amount(Purchase_order_line et) ;
    Purchase_order_line calc_price(Purchase_order_line et) ;
    boolean checkKey(Purchase_order_line et) ;
    Purchase_order_line product_change(Purchase_order_line et) ;
    boolean save(Purchase_order_line et) ;
    void saveBatch(List<Purchase_order_line> list) ;
    Page<HashMap> searchCalc_order_amount(Purchase_order_lineSearchContext context) ;
    Page<Purchase_order_line> searchDefault(Purchase_order_lineSearchContext context) ;
    List<Purchase_order_line> selectByAccountAnalyticId(Long id);
    void resetByAccountAnalyticId(Long id);
    void resetByAccountAnalyticId(Collection<Long> ids);
    void removeByAccountAnalyticId(Long id);
    List<Purchase_order_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Purchase_order_line> selectByOrderId(Long id);
    void removeByOrderId(Collection<Long> ids);
    void removeByOrderId(Long id);
    List<Purchase_order_line> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Purchase_order_line> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Purchase_order_line> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Purchase_order_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Purchase_order_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Purchase_order_line> selectBySaleLineId(Long id);
    void resetBySaleLineId(Long id);
    void resetBySaleLineId(Collection<Long> ids);
    void removeBySaleLineId(Long id);
    List<Purchase_order_line> selectBySaleOrderId(Long id);
    void resetBySaleOrderId(Long id);
    void resetBySaleOrderId(Collection<Long> ids);
    void removeBySaleOrderId(Long id);
    List<Purchase_order_line> selectByOrderpointId(Long id);
    void resetByOrderpointId(Long id);
    void resetByOrderpointId(Collection<Long> ids);
    void removeByOrderpointId(Long id);
    List<Purchase_order_line> selectByProductUom(Long id);
    void resetByProductUom(Long id);
    void resetByProductUom(Collection<Long> ids);
    void removeByProductUom(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Purchase_order_line> getPurchaseOrderLineByIds(List<Long> ids) ;
    List<Purchase_order_line> getPurchaseOrderLineByEntities(List<Purchase_order_line> entities) ;
}



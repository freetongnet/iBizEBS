package cn.ibizlab.businesscentral.core.odoo_payment.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.businesscentral.core.odoo_payment.filter.Payment_acquirerSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[payment_acquirer] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-payment:odoo-payment}", contextId = "payment-acquirer", fallback = payment_acquirerFallback.class)
public interface payment_acquirerFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirers/{id}")
    Payment_acquirer update(@PathVariable("id") Long id,@RequestBody Payment_acquirer payment_acquirer);

    @RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirers/batch")
    Boolean updateBatch(@RequestBody List<Payment_acquirer> payment_acquirers);


    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers")
    Payment_acquirer create(@RequestBody Payment_acquirer payment_acquirer);

    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/batch")
    Boolean createBatch(@RequestBody List<Payment_acquirer> payment_acquirers);



    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/search")
    Page<Payment_acquirer> search(@RequestBody Payment_acquirerSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/payment_acquirers/{id}")
    Payment_acquirer get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirers/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirers/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/payment_acquirers/select")
    Page<Payment_acquirer> select();


    @RequestMapping(method = RequestMethod.GET, value = "/payment_acquirers/getdraft")
    Payment_acquirer getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/checkkey")
    Boolean checkKey(@RequestBody Payment_acquirer payment_acquirer);


    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/save")
    Boolean save(@RequestBody Payment_acquirer payment_acquirer);

    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/savebatch")
    Boolean saveBatch(@RequestBody List<Payment_acquirer> payment_acquirers);



    @RequestMapping(method = RequestMethod.POST, value = "/payment_acquirers/searchdefault")
    Page<Payment_acquirer> searchDefault(@RequestBody Payment_acquirerSearchContext context);


}

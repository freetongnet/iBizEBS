package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_modelSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_reconcile_model] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-reconcile-model", fallback = account_reconcile_modelFallback.class)
public interface account_reconcile_modelFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_models/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_models/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models")
    Account_reconcile_model create(@RequestBody Account_reconcile_model account_reconcile_model);

    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/batch")
    Boolean createBatch(@RequestBody List<Account_reconcile_model> account_reconcile_models);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_models/{id}")
    Account_reconcile_model update(@PathVariable("id") Long id,@RequestBody Account_reconcile_model account_reconcile_model);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_models/batch")
    Boolean updateBatch(@RequestBody List<Account_reconcile_model> account_reconcile_models);





    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/search")
    Page<Account_reconcile_model> search(@RequestBody Account_reconcile_modelSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_models/{id}")
    Account_reconcile_model get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_models/select")
    Page<Account_reconcile_model> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_models/getdraft")
    Account_reconcile_model getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/checkkey")
    Boolean checkKey(@RequestBody Account_reconcile_model account_reconcile_model);


    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/save")
    Boolean save(@RequestBody Account_reconcile_model account_reconcile_model);

    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/savebatch")
    Boolean saveBatch(@RequestBody List<Account_reconcile_model> account_reconcile_models);



    @RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/searchdefault")
    Page<Account_reconcile_model> searchDefault(@RequestBody Account_reconcile_modelSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_warn_insufficient_qty_unbuild] 服务对象接口
 */
public interface IStock_warn_insufficient_qty_unbuildService extends IService<Stock_warn_insufficient_qty_unbuild>{

    boolean create(Stock_warn_insufficient_qty_unbuild et) ;
    void createBatch(List<Stock_warn_insufficient_qty_unbuild> list) ;
    boolean update(Stock_warn_insufficient_qty_unbuild et) ;
    void updateBatch(List<Stock_warn_insufficient_qty_unbuild> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_warn_insufficient_qty_unbuild get(Long key) ;
    Stock_warn_insufficient_qty_unbuild getDraft(Stock_warn_insufficient_qty_unbuild et) ;
    boolean checkKey(Stock_warn_insufficient_qty_unbuild et) ;
    boolean save(Stock_warn_insufficient_qty_unbuild et) ;
    void saveBatch(List<Stock_warn_insufficient_qty_unbuild> list) ;
    Page<Stock_warn_insufficient_qty_unbuild> searchDefault(Stock_warn_insufficient_qty_unbuildSearchContext context) ;
    List<Stock_warn_insufficient_qty_unbuild> selectByUnbuildId(Long id);
    void resetByUnbuildId(Long id);
    void resetByUnbuildId(Collection<Long> ids);
    void removeByUnbuildId(Long id);
    List<Stock_warn_insufficient_qty_unbuild> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_warn_insufficient_qty_unbuild> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_warn_insufficient_qty_unbuild> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_warn_insufficient_qty_unbuild> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_warn_insufficient_qty_unbuild> getStockWarnInsufficientQtyUnbuildByIds(List<Long> ids) ;
    List<Stock_warn_insufficient_qty_unbuild> getStockWarnInsufficientQtyUnbuildByEntities(List<Stock_warn_insufficient_qty_unbuild> entities) ;
}



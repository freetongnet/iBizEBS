package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_usersSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_users] 服务对象接口
 */
public interface IRes_usersService extends IService<Res_users>{

    boolean create(Res_users et) ;
    void createBatch(List<Res_users> list) ;
    boolean update(Res_users et) ;
    void updateBatch(List<Res_users> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_users get(Long key) ;
    Res_users getDraft(Res_users et) ;
    Res_users a(Res_users et) ;
    boolean checkKey(Res_users et) ;
    boolean save(Res_users et) ;
    void saveBatch(List<Res_users> list) ;
    Page<Res_users> searchActive(Res_usersSearchContext context) ;
    Page<Res_users> searchDefault(Res_usersSearchContext context) ;
    List<Res_users> selectBySaleTeamId(Long id);
    void resetBySaleTeamId(Long id);
    void resetBySaleTeamId(Collection<Long> ids);
    void removeBySaleTeamId(Long id);
    List<Res_users> selectByAliasId(Long id);
    void resetByAliasId(Long id);
    void resetByAliasId(Collection<Long> ids);
    void removeByAliasId(Long id);
    List<Res_users> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Res_users> selectByPartnerId(Long id);
    List<Res_users> selectByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Res_users> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_users> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_users> getResUsersByIds(List<Long> ids) ;
    List<Res_users> getResUsersByEntities(List<Res_users> entities) ;
}



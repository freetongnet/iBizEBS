package cn.ibizlab.businesscentral.core.odoo_utm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_sourceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[utm_source] 服务对象接口
 */
@Component
public class utm_sourceFallback implements utm_sourceFeignClient{



    public Utm_source create(Utm_source utm_source){
            return null;
     }
    public Boolean createBatch(List<Utm_source> utm_sources){
            return false;
     }

    public Utm_source get(Long id){
            return null;
     }



    public Page<Utm_source> search(Utm_sourceSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Utm_source update(Long id, Utm_source utm_source){
            return null;
     }
    public Boolean updateBatch(List<Utm_source> utm_sources){
            return false;
     }


    public Page<Utm_source> select(){
            return null;
     }

    public Utm_source getDraft(){
            return null;
    }



    public Boolean checkKey(Utm_source utm_source){
            return false;
     }


    public Boolean save(Utm_source utm_source){
            return false;
     }
    public Boolean saveBatch(List<Utm_source> utm_sources){
            return false;
     }

    public Page<Utm_source> searchDefault(Utm_sourceSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_journalSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_journalMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[日记账] 服务对象接口实现
 */
@Slf4j
@Service("Account_journalServiceImpl")
public class Account_journalServiceImpl extends EBSServiceImpl<Account_journalMapper, Account_journal> implements IAccount_journalService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_abstract_paymentService accountAbstractPaymentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_import_journal_creationService accountBankStatementImportJournalCreationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_lineService accountBankStatementLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statementService accountBankStatementService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_import_wizardService accountInvoiceImportWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_reportService accountInvoiceReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_reversalService accountMoveReversalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService accountMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_paymentService accountPaymentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_modelService accountReconcileModelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_register_paymentsService accountRegisterPaymentsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheet_register_payment_wizardService hrExpenseSheetRegisterPaymentWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService hrExpenseSheetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_acquirerService paymentAcquirerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_aliasService mailAliasService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_bankService resPartnerBankService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.journal" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_journal et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_journalService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_journal> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_journal et) {
        Account_journal old = new Account_journal() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_journalService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_journalService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_journal> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountAbstractPaymentService.resetByJournalId(key);
        accountBankStatementImportJournalCreationService.removeByJournalId(key);
        accountBankStatementLineService.resetByJournalId(key);
        accountBankStatementService.resetByJournalId(key);
        accountInvoiceImportWizardService.resetByJournalId(key);
        accountInvoiceService.resetByJournalId(key);
        accountMoveLineService.resetByJournalId(key);
        accountMoveReversalService.resetByJournalId(key);
        accountMoveService.resetByJournalId(key);
        accountPaymentService.resetByDestinationJournalId(key);
        accountPaymentService.resetByJournalId(key);
        accountReconcileModelService.removeByJournalId(key);
        accountReconcileModelService.removeBySecondJournalId(key);
        accountRegisterPaymentsService.resetByJournalId(key);
        hrExpenseSheetRegisterPaymentWizardService.resetByJournalId(key);
        hrExpenseSheetService.resetByBankJournalId(key);
        hrExpenseSheetService.resetByJournalId(key);
        paymentAcquirerService.resetByJournalId(key);
        resCompanyService.resetByCurrencyExchangeJournalId(key);
        resCompanyService.resetByTaxCashBasisJournalId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountAbstractPaymentService.resetByJournalId(idList);
        accountBankStatementImportJournalCreationService.removeByJournalId(idList);
        accountBankStatementLineService.resetByJournalId(idList);
        accountBankStatementService.resetByJournalId(idList);
        accountInvoiceImportWizardService.resetByJournalId(idList);
        accountInvoiceService.resetByJournalId(idList);
        accountMoveLineService.resetByJournalId(idList);
        accountMoveReversalService.resetByJournalId(idList);
        accountMoveService.resetByJournalId(idList);
        accountPaymentService.resetByDestinationJournalId(idList);
        accountPaymentService.resetByJournalId(idList);
        accountReconcileModelService.removeByJournalId(idList);
        accountReconcileModelService.removeBySecondJournalId(idList);
        accountRegisterPaymentsService.resetByJournalId(idList);
        hrExpenseSheetRegisterPaymentWizardService.resetByJournalId(idList);
        hrExpenseSheetService.resetByBankJournalId(idList);
        hrExpenseSheetService.resetByJournalId(idList);
        paymentAcquirerService.resetByJournalId(idList);
        resCompanyService.resetByCurrencyExchangeJournalId(idList);
        resCompanyService.resetByTaxCashBasisJournalId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_journal get(Long key) {
        Account_journal et = getById(key);
        if(et==null){
            et=new Account_journal();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_journal getDraft(Account_journal et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_journal et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_journal et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_journal et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_journal> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_journal> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_journal> selectByDefaultCreditAccountId(Long id) {
        return baseMapper.selectByDefaultCreditAccountId(id);
    }
    @Override
    public void resetByDefaultCreditAccountId(Long id) {
        this.update(new UpdateWrapper<Account_journal>().set("default_credit_account_id",null).eq("default_credit_account_id",id));
    }

    @Override
    public void resetByDefaultCreditAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_journal>().set("default_credit_account_id",null).in("default_credit_account_id",ids));
    }

    @Override
    public void removeByDefaultCreditAccountId(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("default_credit_account_id",id));
    }

	@Override
    public List<Account_journal> selectByDefaultDebitAccountId(Long id) {
        return baseMapper.selectByDefaultDebitAccountId(id);
    }
    @Override
    public void resetByDefaultDebitAccountId(Long id) {
        this.update(new UpdateWrapper<Account_journal>().set("default_debit_account_id",null).eq("default_debit_account_id",id));
    }

    @Override
    public void resetByDefaultDebitAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_journal>().set("default_debit_account_id",null).in("default_debit_account_id",ids));
    }

    @Override
    public void removeByDefaultDebitAccountId(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("default_debit_account_id",id));
    }

	@Override
    public List<Account_journal> selectByLossAccountId(Long id) {
        return baseMapper.selectByLossAccountId(id);
    }
    @Override
    public void resetByLossAccountId(Long id) {
        this.update(new UpdateWrapper<Account_journal>().set("loss_account_id",null).eq("loss_account_id",id));
    }

    @Override
    public void resetByLossAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_journal>().set("loss_account_id",null).in("loss_account_id",ids));
    }

    @Override
    public void removeByLossAccountId(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("loss_account_id",id));
    }

	@Override
    public List<Account_journal> selectByProfitAccountId(Long id) {
        return baseMapper.selectByProfitAccountId(id);
    }
    @Override
    public void resetByProfitAccountId(Long id) {
        this.update(new UpdateWrapper<Account_journal>().set("profit_account_id",null).eq("profit_account_id",id));
    }

    @Override
    public void resetByProfitAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_journal>().set("profit_account_id",null).in("profit_account_id",ids));
    }

    @Override
    public void removeByProfitAccountId(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("profit_account_id",id));
    }

	@Override
    public List<Account_journal> selectByAliasId(Long id) {
        return baseMapper.selectByAliasId(id);
    }
    @Override
    public void resetByAliasId(Long id) {
        this.update(new UpdateWrapper<Account_journal>().set("alias_id",null).eq("alias_id",id));
    }

    @Override
    public void resetByAliasId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_journal>().set("alias_id",null).in("alias_id",ids));
    }

    @Override
    public void removeByAliasId(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("alias_id",id));
    }

	@Override
    public List<Account_journal> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_journal>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_journal>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("company_id",id));
    }

	@Override
    public List<Account_journal> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Account_journal>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_journal>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("currency_id",id));
    }

	@Override
    public List<Account_journal> selectByBankAccountId(Long id) {
        return baseMapper.selectByBankAccountId(id);
    }
    @Override
    public List<Account_journal> selectByBankAccountId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_journal>().in("id",ids));
    }

    @Override
    public void removeByBankAccountId(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("bank_account_id",id));
    }

	@Override
    public List<Account_journal> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("create_uid",id));
    }

	@Override
    public List<Account_journal> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_journal>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_journal> searchDefault(Account_journalSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_journal> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_journal>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_journal et){
        //实体关系[DER1N_ACCOUNT_JOURNAL__ACCOUNT_ACCOUNT__DEFAULT_CREDIT_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getDefaultCreditAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooDefaultCreditAccount=et.getOdooDefaultCreditAccount();
            if(ObjectUtils.isEmpty(odooDefaultCreditAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getDefaultCreditAccountId());
                et.setOdooDefaultCreditAccount(majorEntity);
                odooDefaultCreditAccount=majorEntity;
            }
            et.setDefaultCreditAccountIdText(odooDefaultCreditAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_JOURNAL__ACCOUNT_ACCOUNT__DEFAULT_DEBIT_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getDefaultDebitAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooDefaultDebitAccount=et.getOdooDefaultDebitAccount();
            if(ObjectUtils.isEmpty(odooDefaultDebitAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getDefaultDebitAccountId());
                et.setOdooDefaultDebitAccount(majorEntity);
                odooDefaultDebitAccount=majorEntity;
            }
            et.setDefaultDebitAccountIdText(odooDefaultDebitAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_JOURNAL__ACCOUNT_ACCOUNT__LOSS_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getLossAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooLossAccount=et.getOdooLossAccount();
            if(ObjectUtils.isEmpty(odooLossAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getLossAccountId());
                et.setOdooLossAccount(majorEntity);
                odooLossAccount=majorEntity;
            }
            et.setLossAccountIdText(odooLossAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_JOURNAL__ACCOUNT_ACCOUNT__PROFIT_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getProfitAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooProfitAccount=et.getOdooProfitAccount();
            if(ObjectUtils.isEmpty(odooProfitAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getProfitAccountId());
                et.setOdooProfitAccount(majorEntity);
                odooProfitAccount=majorEntity;
            }
            et.setProfitAccountIdText(odooProfitAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_JOURNAL__MAIL_ALIAS__ALIAS_ID]
        if(!ObjectUtils.isEmpty(et.getAliasId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias=et.getOdooAlias();
            if(ObjectUtils.isEmpty(odooAlias)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias majorEntity=mailAliasService.get(et.getAliasId());
                et.setOdooAlias(majorEntity);
                odooAlias=majorEntity;
            }
            et.setAliasName(odooAlias.getAliasName());
        }
        //实体关系[DER1N_ACCOUNT_JOURNAL__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyPartnerId(odooCompany.getPartnerId());
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_JOURNAL__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_JOURNAL__RES_PARTNER_BANK__BANK_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getBankAccountId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank odooBankAccount=et.getOdooBankAccount();
            if(ObjectUtils.isEmpty(odooBankAccount)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank majorEntity=resPartnerBankService.get(et.getBankAccountId());
                et.setOdooBankAccount(majorEntity);
                odooBankAccount=majorEntity;
            }
            et.setBankAccNumber(odooBankAccount.getAccNumber());
            et.setBankId(odooBankAccount.getBankId());
        }
        //实体关系[DER1N_ACCOUNT_JOURNAL__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_JOURNAL__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_journal> getAccountJournalByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_journal> getAccountJournalByEntities(List<Account_journal> entities) {
        List ids =new ArrayList();
        for(Account_journal entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




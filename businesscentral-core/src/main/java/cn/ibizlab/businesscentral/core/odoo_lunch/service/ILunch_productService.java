package cn.ibizlab.businesscentral.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_productSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Lunch_product] 服务对象接口
 */
public interface ILunch_productService extends IService<Lunch_product>{

    boolean create(Lunch_product et) ;
    void createBatch(List<Lunch_product> list) ;
    boolean update(Lunch_product et) ;
    void updateBatch(List<Lunch_product> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Lunch_product get(Long key) ;
    Lunch_product getDraft(Lunch_product et) ;
    boolean checkKey(Lunch_product et) ;
    boolean save(Lunch_product et) ;
    void saveBatch(List<Lunch_product> list) ;
    Page<Lunch_product> searchDefault(Lunch_productSearchContext context) ;
    List<Lunch_product> selectByCategoryId(Long id);
    void resetByCategoryId(Long id);
    void resetByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Lunch_product> selectBySupplier(Long id);
    void resetBySupplier(Long id);
    void resetBySupplier(Collection<Long> ids);
    void removeBySupplier(Long id);
    List<Lunch_product> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Lunch_product> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Lunch_product> getLunchProductByIds(List<Long> ids) ;
    List<Lunch_product> getLunchProductByEntities(List<Lunch_product> entities) ;
}



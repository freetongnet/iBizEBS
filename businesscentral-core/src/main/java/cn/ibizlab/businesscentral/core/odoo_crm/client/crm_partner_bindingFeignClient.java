package cn.ibizlab.businesscentral.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_partner_bindingSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_partner_binding] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-crm:odoo-crm}", contextId = "crm-partner-binding", fallback = crm_partner_bindingFallback.class)
public interface crm_partner_bindingFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings/search")
    Page<Crm_partner_binding> search(@RequestBody Crm_partner_bindingSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/crm_partner_bindings/{id}")
    Crm_partner_binding update(@PathVariable("id") Long id,@RequestBody Crm_partner_binding crm_partner_binding);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_partner_bindings/batch")
    Boolean updateBatch(@RequestBody List<Crm_partner_binding> crm_partner_bindings);




    @RequestMapping(method = RequestMethod.GET, value = "/crm_partner_bindings/{id}")
    Crm_partner_binding get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_partner_bindings/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_partner_bindings/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings")
    Crm_partner_binding create(@RequestBody Crm_partner_binding crm_partner_binding);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings/batch")
    Boolean createBatch(@RequestBody List<Crm_partner_binding> crm_partner_bindings);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_partner_bindings/select")
    Page<Crm_partner_binding> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_partner_bindings/getdraft")
    Crm_partner_binding getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings/checkkey")
    Boolean checkKey(@RequestBody Crm_partner_binding crm_partner_binding);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings/save")
    Boolean save(@RequestBody Crm_partner_binding crm_partner_binding);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings/savebatch")
    Boolean saveBatch(@RequestBody List<Crm_partner_binding> crm_partner_bindings);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_partner_bindings/searchdefault")
    Page<Crm_partner_binding> searchDefault(@RequestBody Crm_partner_bindingSearchContext context);


}

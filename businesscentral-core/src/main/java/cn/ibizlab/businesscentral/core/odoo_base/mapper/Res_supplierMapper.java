package cn.ibizlab.businesscentral.core.odoo_base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_supplierSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Res_supplierMapper extends BaseMapper<Res_supplier>{

    Page<Res_supplier> searchDefault(IPage page, @Param("srf") Res_supplierSearchContext context, @Param("ew") Wrapper<Res_supplier> wrapper) ;
    Page<Res_supplier> searchMaster(IPage page, @Param("srf") Res_supplierSearchContext context, @Param("ew") Wrapper<Res_supplier> wrapper) ;
    @Override
    Res_supplier selectById(Serializable id);
    @Override
    int insert(Res_supplier entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Res_supplier entity);
    @Override
    int update(@Param(Constants.ENTITY) Res_supplier entity, @Param("ew") Wrapper<Res_supplier> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Res_supplier> selectByPropertyAccountPositionId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByPropertyPaymentTermId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByPropertySupplierPaymentTermId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByPropertyDeliveryCarrierId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByPropertyProductPricelist(@Param("id") Serializable id) ;

    List<Res_supplier> selectByCompanyId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByCountryId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByStateId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByPropertyPurchaseCurrencyId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByParentId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByTitle(@Param("id") Serializable id) ;

    List<Res_supplier> selectByUserId(@Param("id") Serializable id) ;

    List<Res_supplier> selectByPropertyStockCustomer(@Param("id") Serializable id) ;

    List<Res_supplier> selectByPropertyStockSubcontractor(@Param("id") Serializable id) ;

    List<Res_supplier> selectByPropertyStockSupplier(@Param("id") Serializable id) ;


        
    boolean saveRelByCategoryId(@Param("partner_id") Long partner_id, List<cn.ibizlab.businesscentral.util.domain.MultiSelectItem> res_partner_categories);

}

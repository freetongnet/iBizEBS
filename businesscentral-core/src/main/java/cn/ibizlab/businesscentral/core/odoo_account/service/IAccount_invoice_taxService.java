package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_taxSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_invoice_tax] 服务对象接口
 */
public interface IAccount_invoice_taxService extends IService<Account_invoice_tax>{

    boolean create(Account_invoice_tax et) ;
    void createBatch(List<Account_invoice_tax> list) ;
    boolean update(Account_invoice_tax et) ;
    void updateBatch(List<Account_invoice_tax> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_invoice_tax get(Long key) ;
    Account_invoice_tax getDraft(Account_invoice_tax et) ;
    boolean checkKey(Account_invoice_tax et) ;
    boolean save(Account_invoice_tax et) ;
    void saveBatch(List<Account_invoice_tax> list) ;
    Page<Account_invoice_tax> searchDefault(Account_invoice_taxSearchContext context) ;
    List<Account_invoice_tax> selectByAccountId(Long id);
    void resetByAccountId(Long id);
    void resetByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_invoice_tax> selectByAccountAnalyticId(Long id);
    void resetByAccountAnalyticId(Long id);
    void resetByAccountAnalyticId(Collection<Long> ids);
    void removeByAccountAnalyticId(Long id);
    List<Account_invoice_tax> selectByInvoiceId(Long id);
    void removeByInvoiceId(Collection<Long> ids);
    void removeByInvoiceId(Long id);
    List<Account_invoice_tax> selectByTaxId(Long id);
    List<Account_invoice_tax> selectByTaxId(Collection<Long> ids);
    void removeByTaxId(Long id);
    List<Account_invoice_tax> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_invoice_tax> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_invoice_tax> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_invoice_tax> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_invoice_tax> getAccountInvoiceTaxByIds(List<Long> ids) ;
    List<Account_invoice_tax> getAccountInvoiceTaxByEntities(List<Account_invoice_tax> entities) ;
}



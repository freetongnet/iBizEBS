package cn.ibizlab.businesscentral.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[lunch_cashmove] 服务对象接口
 */
@Component
public class lunch_cashmoveFallback implements lunch_cashmoveFeignClient{




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Lunch_cashmove create(Lunch_cashmove lunch_cashmove){
            return null;
     }
    public Boolean createBatch(List<Lunch_cashmove> lunch_cashmoves){
            return false;
     }

    public Lunch_cashmove update(Long id, Lunch_cashmove lunch_cashmove){
            return null;
     }
    public Boolean updateBatch(List<Lunch_cashmove> lunch_cashmoves){
            return false;
     }


    public Lunch_cashmove get(Long id){
            return null;
     }


    public Page<Lunch_cashmove> search(Lunch_cashmoveSearchContext context){
            return null;
     }


    public Page<Lunch_cashmove> select(){
            return null;
     }

    public Lunch_cashmove getDraft(){
            return null;
    }



    public Boolean checkKey(Lunch_cashmove lunch_cashmove){
            return false;
     }


    public Boolean save(Lunch_cashmove lunch_cashmove){
            return false;
     }
    public Boolean saveBatch(List<Lunch_cashmove> lunch_cashmoves){
            return false;
     }

    public Page<Lunch_cashmove> searchDefault(Lunch_cashmoveSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_ir.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_attachment;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_attachmentSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Ir_attachmentMapper extends BaseMapper<Ir_attachment>{

    Page<Ir_attachment> searchDefault(IPage page, @Param("srf") Ir_attachmentSearchContext context, @Param("ew") Wrapper<Ir_attachment> wrapper) ;
    @Override
    Ir_attachment selectById(Serializable id);
    @Override
    int insert(Ir_attachment entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Ir_attachment entity);
    @Override
    int update(@Param(Constants.ENTITY) Ir_attachment entity, @Param("ew") Wrapper<Ir_attachment> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Ir_attachment> selectByCompanyId(@Param("id") Serializable id) ;


}

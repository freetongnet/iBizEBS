package cn.ibizlab.businesscentral.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[币种]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RES_CURRENCY",resultMap = "Res_currencyResultMap")
public class Res_currency extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 小数点位置
     */
    @DEField(name = "decimal_places")
    @TableField(value = "decimal_places")
    @JSONField(name = "decimal_places")
    @JsonProperty("decimal_places")
    private Integer decimalPlaces;
    /**
     * 币种
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 货币单位
     */
    @DEField(name = "currency_unit_label")
    @TableField(value = "currency_unit_label")
    @JSONField(name = "currency_unit_label")
    @JsonProperty("currency_unit_label")
    private String currencyUnitLabel;
    /**
     * 舍入系数
     */
    @TableField(value = "rounding")
    @JSONField(name = "rounding")
    @JsonProperty("rounding")
    private Double rounding;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 符号位置
     */
    @TableField(value = "position")
    @JSONField(name = "position")
    @JsonProperty("position")
    private String position;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 当前汇率
     */
    @TableField(exist = false)
    @JSONField(name = "rate")
    @JsonProperty("rate")
    private Double rate;
    /**
     * 比率
     */
    @TableField(exist = false)
    @JSONField(name = "rate_ids")
    @JsonProperty("rate_ids")
    private String rateIds;
    /**
     * 货币符号
     */
    @TableField(value = "symbol")
    @JSONField(name = "symbol")
    @JsonProperty("symbol")
    private String symbol;
    /**
     * 货币子单位
     */
    @DEField(name = "currency_subunit_label")
    @TableField(value = "currency_subunit_label")
    @JSONField(name = "currency_subunit_label")
    @JsonProperty("currency_subunit_label")
    private String currencySubunitLabel;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [小数点位置]
     */
    public void setDecimalPlaces(Integer decimalPlaces){
        this.decimalPlaces = decimalPlaces ;
        this.modify("decimal_places",decimalPlaces);
    }

    /**
     * 设置 [币种]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [货币单位]
     */
    public void setCurrencyUnitLabel(String currencyUnitLabel){
        this.currencyUnitLabel = currencyUnitLabel ;
        this.modify("currency_unit_label",currencyUnitLabel);
    }

    /**
     * 设置 [舍入系数]
     */
    public void setRounding(Double rounding){
        this.rounding = rounding ;
        this.modify("rounding",rounding);
    }

    /**
     * 设置 [符号位置]
     */
    public void setPosition(String position){
        this.position = position ;
        this.modify("position",position);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [货币符号]
     */
    public void setSymbol(String symbol){
        this.symbol = symbol ;
        this.modify("symbol",symbol);
    }

    /**
     * 设置 [货币子单位]
     */
    public void setCurrencySubunitLabel(String currencySubunitLabel){
        this.currencySubunitLabel = currencySubunitLabel ;
        this.modify("currency_subunit_label",currencySubunitLabel);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



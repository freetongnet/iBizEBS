package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_recruitment_degree] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-recruitment-degree", fallback = hr_recruitment_degreeFallback.class)
public interface hr_recruitment_degreeFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/search")
    Page<Hr_recruitment_degree> search(@RequestBody Hr_recruitment_degreeSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees")
    Hr_recruitment_degree create(@RequestBody Hr_recruitment_degree hr_recruitment_degree);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/batch")
    Boolean createBatch(@RequestBody List<Hr_recruitment_degree> hr_recruitment_degrees);




    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_degrees/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_recruitment_degrees/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_degrees/{id}")
    Hr_recruitment_degree update(@PathVariable("id") Long id,@RequestBody Hr_recruitment_degree hr_recruitment_degree);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_recruitment_degrees/batch")
    Boolean updateBatch(@RequestBody List<Hr_recruitment_degree> hr_recruitment_degrees);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_degrees/{id}")
    Hr_recruitment_degree get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_degrees/select")
    Page<Hr_recruitment_degree> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_recruitment_degrees/getdraft")
    Hr_recruitment_degree getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/checkkey")
    Boolean checkKey(@RequestBody Hr_recruitment_degree hr_recruitment_degree);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/save")
    Boolean save(@RequestBody Hr_recruitment_degree hr_recruitment_degree);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_recruitment_degree> hr_recruitment_degrees);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_recruitment_degrees/searchdefault")
    Page<Hr_recruitment_degree> searchDefault(@RequestBody Hr_recruitment_degreeSearchContext context);


}

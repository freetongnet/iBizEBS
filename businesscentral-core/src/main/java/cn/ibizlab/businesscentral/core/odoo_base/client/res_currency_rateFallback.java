package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_currency_rateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[res_currency_rate] 服务对象接口
 */
@Component
public class res_currency_rateFallback implements res_currency_rateFeignClient{

    public Res_currency_rate get(Long id){
            return null;
     }



    public Res_currency_rate create(Res_currency_rate res_currency_rate){
            return null;
     }
    public Boolean createBatch(List<Res_currency_rate> res_currency_rates){
            return false;
     }

    public Res_currency_rate update(Long id, Res_currency_rate res_currency_rate){
            return null;
     }
    public Boolean updateBatch(List<Res_currency_rate> res_currency_rates){
            return false;
     }


    public Page<Res_currency_rate> search(Res_currency_rateSearchContext context){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Res_currency_rate> select(){
            return null;
     }

    public Res_currency_rate getDraft(){
            return null;
    }



    public Boolean checkKey(Res_currency_rate res_currency_rate){
            return false;
     }


    public Boolean save(Res_currency_rate res_currency_rate){
            return false;
     }
    public Boolean saveBatch(List<Res_currency_rate> res_currency_rates){
            return false;
     }

    public Page<Res_currency_rate> searchDefault(Res_currency_rateSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_service_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_service_type] 服务对象接口
 */
@Component
public class fleet_service_typeFallback implements fleet_service_typeFeignClient{


    public Fleet_service_type update(Long id, Fleet_service_type fleet_service_type){
            return null;
     }
    public Boolean updateBatch(List<Fleet_service_type> fleet_service_types){
            return false;
     }



    public Fleet_service_type get(Long id){
            return null;
     }



    public Fleet_service_type create(Fleet_service_type fleet_service_type){
            return null;
     }
    public Boolean createBatch(List<Fleet_service_type> fleet_service_types){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Fleet_service_type> search(Fleet_service_typeSearchContext context){
            return null;
     }


    public Page<Fleet_service_type> select(){
            return null;
     }

    public Fleet_service_type getDraft(){
            return null;
    }



    public Boolean checkKey(Fleet_service_type fleet_service_type){
            return false;
     }


    public Boolean save(Fleet_service_type fleet_service_type){
            return false;
     }
    public Boolean saveBatch(List<Fleet_service_type> fleet_service_types){
            return false;
     }

    public Page<Fleet_service_type> searchDefault(Fleet_service_typeSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_calendar.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event;
/**
 * 关系型数据实体[Calendar_event] 查询条件对象
 */
@Slf4j
@Data
public class Calendar_eventSearchContext extends QueryWrapperContext<Calendar_event> {

	private String n_month_by_eq;//[选项]
	public void setN_month_by_eq(String n_month_by_eq) {
        this.n_month_by_eq = n_month_by_eq;
        if(!ObjectUtils.isEmpty(this.n_month_by_eq)){
            this.getSearchCond().eq("month_by", n_month_by_eq);
        }
    }
	private String n_week_list_eq;//[工作日]
	public void setN_week_list_eq(String n_week_list_eq) {
        this.n_week_list_eq = n_week_list_eq;
        if(!ObjectUtils.isEmpty(this.n_week_list_eq)){
            this.getSearchCond().eq("week_list", n_week_list_eq);
        }
    }
	private String n_end_type_eq;//[重复终止]
	public void setN_end_type_eq(String n_end_type_eq) {
        this.n_end_type_eq = n_end_type_eq;
        if(!ObjectUtils.isEmpty(this.n_end_type_eq)){
            this.getSearchCond().eq("end_type", n_end_type_eq);
        }
    }
	private String n_attendee_status_eq;//[出席者状态]
	public void setN_attendee_status_eq(String n_attendee_status_eq) {
        this.n_attendee_status_eq = n_attendee_status_eq;
        if(!ObjectUtils.isEmpty(this.n_attendee_status_eq)){
            this.getSearchCond().eq("attendee_status", n_attendee_status_eq);
        }
    }
	private String n_rrule_type_eq;//[重新提起]
	public void setN_rrule_type_eq(String n_rrule_type_eq) {
        this.n_rrule_type_eq = n_rrule_type_eq;
        if(!ObjectUtils.isEmpty(this.n_rrule_type_eq)){
            this.getSearchCond().eq("rrule_type", n_rrule_type_eq);
        }
    }
	private String n_privacy_eq;//[隐私]
	public void setN_privacy_eq(String n_privacy_eq) {
        this.n_privacy_eq = n_privacy_eq;
        if(!ObjectUtils.isEmpty(this.n_privacy_eq)){
            this.getSearchCond().eq("privacy", n_privacy_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_show_as_eq;//[显示时间为]
	public void setN_show_as_eq(String n_show_as_eq) {
        this.n_show_as_eq = n_show_as_eq;
        if(!ObjectUtils.isEmpty(this.n_show_as_eq)){
            this.getSearchCond().eq("show_as", n_show_as_eq);
        }
    }
	private String n_byday_eq;//[按 天]
	public void setN_byday_eq(String n_byday_eq) {
        this.n_byday_eq = n_byday_eq;
        if(!ObjectUtils.isEmpty(this.n_byday_eq)){
            this.getSearchCond().eq("byday", n_byday_eq);
        }
    }
	private String n_name_like;//[会议主题]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_opportunity_id_text_eq;//[商机]
	public void setN_opportunity_id_text_eq(String n_opportunity_id_text_eq) {
        this.n_opportunity_id_text_eq = n_opportunity_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_opportunity_id_text_eq)){
            this.getSearchCond().eq("opportunity_id_text", n_opportunity_id_text_eq);
        }
    }
	private String n_opportunity_id_text_like;//[商机]
	public void setN_opportunity_id_text_like(String n_opportunity_id_text_like) {
        this.n_opportunity_id_text_like = n_opportunity_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_opportunity_id_text_like)){
            this.getSearchCond().like("opportunity_id_text", n_opportunity_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_user_id_text_eq;//[所有者]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[所有者]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_applicant_id_text_eq;//[申请人]
	public void setN_applicant_id_text_eq(String n_applicant_id_text_eq) {
        this.n_applicant_id_text_eq = n_applicant_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_applicant_id_text_eq)){
            this.getSearchCond().eq("applicant_id_text", n_applicant_id_text_eq);
        }
    }
	private String n_applicant_id_text_like;//[申请人]
	public void setN_applicant_id_text_like(String n_applicant_id_text_like) {
        this.n_applicant_id_text_like = n_applicant_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_applicant_id_text_like)){
            this.getSearchCond().like("applicant_id_text", n_applicant_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_applicant_id_eq;//[申请人]
	public void setN_applicant_id_eq(Long n_applicant_id_eq) {
        this.n_applicant_id_eq = n_applicant_id_eq;
        if(!ObjectUtils.isEmpty(this.n_applicant_id_eq)){
            this.getSearchCond().eq("applicant_id", n_applicant_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_user_id_eq;//[所有者]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_opportunity_id_eq;//[商机]
	public void setN_opportunity_id_eq(Long n_opportunity_id_eq) {
        this.n_opportunity_id_eq = n_opportunity_id_eq;
        if(!ObjectUtils.isEmpty(this.n_opportunity_id_eq)){
            this.getSearchCond().eq("opportunity_id", n_opportunity_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warehouseSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_warehouse] 服务对象接口
 */
public interface IStock_warehouseService extends IService<Stock_warehouse>{

    boolean create(Stock_warehouse et) ;
    void createBatch(List<Stock_warehouse> list) ;
    boolean update(Stock_warehouse et) ;
    void updateBatch(List<Stock_warehouse> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_warehouse get(Long key) ;
    Stock_warehouse getDraft(Stock_warehouse et) ;
    boolean checkKey(Stock_warehouse et) ;
    boolean save(Stock_warehouse et) ;
    void saveBatch(List<Stock_warehouse> list) ;
    Page<Stock_warehouse> searchDefault(Stock_warehouseSearchContext context) ;
    List<Stock_warehouse> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_warehouse> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Stock_warehouse> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_warehouse> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_warehouse> selectByCrossdockRouteId(Long id);
    List<Stock_warehouse> selectByCrossdockRouteId(Collection<Long> ids);
    void removeByCrossdockRouteId(Long id);
    List<Stock_warehouse> selectByDeliveryRouteId(Long id);
    List<Stock_warehouse> selectByDeliveryRouteId(Collection<Long> ids);
    void removeByDeliveryRouteId(Long id);
    List<Stock_warehouse> selectByPbmRouteId(Long id);
    List<Stock_warehouse> selectByPbmRouteId(Collection<Long> ids);
    void removeByPbmRouteId(Long id);
    List<Stock_warehouse> selectByReceptionRouteId(Long id);
    List<Stock_warehouse> selectByReceptionRouteId(Collection<Long> ids);
    void removeByReceptionRouteId(Long id);
    List<Stock_warehouse> selectByLotStockId(Long id);
    void resetByLotStockId(Long id);
    void resetByLotStockId(Collection<Long> ids);
    void removeByLotStockId(Long id);
    List<Stock_warehouse> selectByPbmLocId(Long id);
    void resetByPbmLocId(Long id);
    void resetByPbmLocId(Collection<Long> ids);
    void removeByPbmLocId(Long id);
    List<Stock_warehouse> selectBySamLocId(Long id);
    void resetBySamLocId(Long id);
    void resetBySamLocId(Collection<Long> ids);
    void removeBySamLocId(Long id);
    List<Stock_warehouse> selectByViewLocationId(Long id);
    void resetByViewLocationId(Long id);
    void resetByViewLocationId(Collection<Long> ids);
    void removeByViewLocationId(Long id);
    List<Stock_warehouse> selectByWhInputStockLocId(Long id);
    void resetByWhInputStockLocId(Long id);
    void resetByWhInputStockLocId(Collection<Long> ids);
    void removeByWhInputStockLocId(Long id);
    List<Stock_warehouse> selectByWhOutputStockLocId(Long id);
    void resetByWhOutputStockLocId(Long id);
    void resetByWhOutputStockLocId(Collection<Long> ids);
    void removeByWhOutputStockLocId(Long id);
    List<Stock_warehouse> selectByWhPackStockLocId(Long id);
    void resetByWhPackStockLocId(Long id);
    void resetByWhPackStockLocId(Collection<Long> ids);
    void removeByWhPackStockLocId(Long id);
    List<Stock_warehouse> selectByWhQcStockLocId(Long id);
    void resetByWhQcStockLocId(Long id);
    void resetByWhQcStockLocId(Collection<Long> ids);
    void removeByWhQcStockLocId(Long id);
    List<Stock_warehouse> selectByIntTypeId(Long id);
    void resetByIntTypeId(Long id);
    void resetByIntTypeId(Collection<Long> ids);
    void removeByIntTypeId(Long id);
    List<Stock_warehouse> selectByInTypeId(Long id);
    void resetByInTypeId(Long id);
    void resetByInTypeId(Collection<Long> ids);
    void removeByInTypeId(Long id);
    List<Stock_warehouse> selectByManuTypeId(Long id);
    void resetByManuTypeId(Long id);
    void resetByManuTypeId(Collection<Long> ids);
    void removeByManuTypeId(Long id);
    List<Stock_warehouse> selectByOutTypeId(Long id);
    void resetByOutTypeId(Long id);
    void resetByOutTypeId(Collection<Long> ids);
    void removeByOutTypeId(Long id);
    List<Stock_warehouse> selectByPackTypeId(Long id);
    void resetByPackTypeId(Long id);
    void resetByPackTypeId(Collection<Long> ids);
    void removeByPackTypeId(Long id);
    List<Stock_warehouse> selectByPbmTypeId(Long id);
    void resetByPbmTypeId(Long id);
    void resetByPbmTypeId(Collection<Long> ids);
    void removeByPbmTypeId(Long id);
    List<Stock_warehouse> selectByPickTypeId(Long id);
    void resetByPickTypeId(Long id);
    void resetByPickTypeId(Collection<Long> ids);
    void removeByPickTypeId(Long id);
    List<Stock_warehouse> selectBySamTypeId(Long id);
    void resetBySamTypeId(Long id);
    void resetBySamTypeId(Collection<Long> ids);
    void removeBySamTypeId(Long id);
    List<Stock_warehouse> selectByBuyPullId(Long id);
    void resetByBuyPullId(Long id);
    void resetByBuyPullId(Collection<Long> ids);
    void removeByBuyPullId(Long id);
    List<Stock_warehouse> selectByManufacturePullId(Long id);
    void resetByManufacturePullId(Long id);
    void resetByManufacturePullId(Collection<Long> ids);
    void removeByManufacturePullId(Long id);
    List<Stock_warehouse> selectByMtoPullId(Long id);
    void resetByMtoPullId(Long id);
    void resetByMtoPullId(Collection<Long> ids);
    void removeByMtoPullId(Long id);
    List<Stock_warehouse> selectByPbmMtoPullId(Long id);
    void resetByPbmMtoPullId(Long id);
    void resetByPbmMtoPullId(Collection<Long> ids);
    void removeByPbmMtoPullId(Long id);
    List<Stock_warehouse> selectBySamRuleId(Long id);
    void resetBySamRuleId(Long id);
    void resetBySamRuleId(Collection<Long> ids);
    void removeBySamRuleId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_warehouse> getStockWarehouseByIds(List<Long> ids) ;
    List<Stock_warehouse> getStockWarehouseByEntities(List<Stock_warehouse> entities) ;
}



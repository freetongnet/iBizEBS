package cn.ibizlab.businesscentral.core.odoo_repair.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_cancelSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[repair_cancel] 服务对象接口
 */
@Component
public class repair_cancelFallback implements repair_cancelFeignClient{

    public Repair_cancel create(Repair_cancel repair_cancel){
            return null;
     }
    public Boolean createBatch(List<Repair_cancel> repair_cancels){
            return false;
     }


    public Page<Repair_cancel> search(Repair_cancelSearchContext context){
            return null;
     }


    public Repair_cancel get(Long id){
            return null;
     }


    public Repair_cancel update(Long id, Repair_cancel repair_cancel){
            return null;
     }
    public Boolean updateBatch(List<Repair_cancel> repair_cancels){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Repair_cancel> select(){
            return null;
     }

    public Repair_cancel getDraft(){
            return null;
    }



    public Boolean checkKey(Repair_cancel repair_cancel){
            return false;
     }


    public Boolean save(Repair_cancel repair_cancel){
            return false;
     }
    public Boolean saveBatch(List<Repair_cancel> repair_cancels){
            return false;
     }

    public Page<Repair_cancel> searchDefault(Repair_cancelSearchContext context){
            return null;
     }


}

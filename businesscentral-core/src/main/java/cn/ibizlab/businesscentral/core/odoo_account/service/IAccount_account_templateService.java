package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_account_templateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_account_template] 服务对象接口
 */
public interface IAccount_account_templateService extends IService<Account_account_template>{

    boolean create(Account_account_template et) ;
    void createBatch(List<Account_account_template> list) ;
    boolean update(Account_account_template et) ;
    void updateBatch(List<Account_account_template> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_account_template get(Long key) ;
    Account_account_template getDraft(Account_account_template et) ;
    boolean checkKey(Account_account_template et) ;
    boolean save(Account_account_template et) ;
    void saveBatch(List<Account_account_template> list) ;
    Page<Account_account_template> searchDefault(Account_account_templateSearchContext context) ;
    List<Account_account_template> selectByUserTypeId(Long id);
    void resetByUserTypeId(Long id);
    void resetByUserTypeId(Collection<Long> ids);
    void removeByUserTypeId(Long id);
    List<Account_account_template> selectByChartTemplateId(Long id);
    void resetByChartTemplateId(Long id);
    void resetByChartTemplateId(Collection<Long> ids);
    void removeByChartTemplateId(Long id);
    List<Account_account_template> selectByGroupId(Long id);
    void resetByGroupId(Long id);
    void resetByGroupId(Collection<Long> ids);
    void removeByGroupId(Long id);
    List<Account_account_template> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_account_template> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_account_template> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_account_template> getAccountAccountTemplateByIds(List<Long> ids) ;
    List<Account_account_template> getAccountAccountTemplateByEntities(List<Account_account_template> entities) ;
}



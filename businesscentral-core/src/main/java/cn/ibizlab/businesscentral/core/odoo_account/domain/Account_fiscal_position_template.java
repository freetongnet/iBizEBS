package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[税科目调整模板]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_FISCAL_POSITION_TEMPLATE",resultMap = "Account_fiscal_position_templateResultMap")
public class Account_fiscal_position_template extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 税科目调整模版
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 联邦政府
     */
    @TableField(exist = false)
    @JSONField(name = "state_ids")
    @JsonProperty("state_ids")
    private String stateIds;
    /**
     * 备注
     */
    @TableField(value = "note")
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 自动检测
     */
    @DEField(name = "auto_apply")
    @TableField(value = "auto_apply")
    @JSONField(name = "auto_apply")
    @JsonProperty("auto_apply")
    private Boolean autoApply;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 科目映射
     */
    @TableField(exist = false)
    @JSONField(name = "account_ids")
    @JsonProperty("account_ids")
    private String accountIds;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 邮编范围到
     */
    @DEField(name = "zip_to")
    @TableField(value = "zip_to")
    @JSONField(name = "zip_to")
    @JsonProperty("zip_to")
    private Integer zipTo;
    /**
     * 邮编范围从
     */
    @DEField(name = "zip_from")
    @TableField(value = "zip_from")
    @JSONField(name = "zip_from")
    @JsonProperty("zip_from")
    private Integer zipFrom;
    /**
     * 税映射
     */
    @TableField(exist = false)
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    private String taxIds;
    /**
     * VAT必须
     */
    @DEField(name = "vat_required")
    @TableField(value = "vat_required")
    @JSONField(name = "vat_required")
    @JsonProperty("vat_required")
    private Boolean vatRequired;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 表模板
     */
    @TableField(exist = false)
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;
    /**
     * 国家
     */
    @TableField(exist = false)
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 国家群组
     */
    @TableField(exist = false)
    @JSONField(name = "country_group_id_text")
    @JsonProperty("country_group_id_text")
    private String countryGroupIdText;
    /**
     * 国家群组
     */
    @DEField(name = "country_group_id")
    @TableField(value = "country_group_id")
    @JSONField(name = "country_group_id")
    @JsonProperty("country_group_id")
    private Long countryGroupId;
    /**
     * 国家
     */
    @DEField(name = "country_id")
    @TableField(value = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Long countryId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 表模板
     */
    @DEField(name = "chart_template_id")
    @TableField(value = "chart_template_id")
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Long chartTemplateId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooChartTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_group odooCountryGroup;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [税科目调整模版]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [备注]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [自动检测]
     */
    public void setAutoApply(Boolean autoApply){
        this.autoApply = autoApply ;
        this.modify("auto_apply",autoApply);
    }

    /**
     * 设置 [邮编范围到]
     */
    public void setZipTo(Integer zipTo){
        this.zipTo = zipTo ;
        this.modify("zip_to",zipTo);
    }

    /**
     * 设置 [邮编范围从]
     */
    public void setZipFrom(Integer zipFrom){
        this.zipFrom = zipFrom ;
        this.modify("zip_from",zipFrom);
    }

    /**
     * 设置 [VAT必须]
     */
    public void setVatRequired(Boolean vatRequired){
        this.vatRequired = vatRequired ;
        this.modify("vat_required",vatRequired);
    }

    /**
     * 设置 [国家群组]
     */
    public void setCountryGroupId(Long countryGroupId){
        this.countryGroupId = countryGroupId ;
        this.modify("country_group_id",countryGroupId);
    }

    /**
     * 设置 [国家]
     */
    public void setCountryId(Long countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [表模板]
     */
    public void setChartTemplateId(Long chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



package cn.ibizlab.businesscentral.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_templateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sale_order_template] 服务对象接口
 */
public interface ISale_order_templateService extends IService<Sale_order_template>{

    boolean create(Sale_order_template et) ;
    void createBatch(List<Sale_order_template> list) ;
    boolean update(Sale_order_template et) ;
    void updateBatch(List<Sale_order_template> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sale_order_template get(Long key) ;
    Sale_order_template getDraft(Sale_order_template et) ;
    boolean checkKey(Sale_order_template et) ;
    boolean save(Sale_order_template et) ;
    void saveBatch(List<Sale_order_template> list) ;
    Page<Sale_order_template> searchDefault(Sale_order_templateSearchContext context) ;
    List<Sale_order_template> selectByMailTemplateId(Long id);
    void resetByMailTemplateId(Long id);
    void resetByMailTemplateId(Collection<Long> ids);
    void removeByMailTemplateId(Long id);
    List<Sale_order_template> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Sale_order_template> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sale_order_template> getSaleOrderTemplateByIds(List<Long> ids) ;
    List<Sale_order_template> getSaleOrderTemplateByEntities(List<Sale_order_template> entities) ;
}



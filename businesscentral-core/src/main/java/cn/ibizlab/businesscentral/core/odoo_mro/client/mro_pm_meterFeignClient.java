package cn.ibizlab.businesscentral.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meterSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_pm_meter] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mro:odoo-mro}", contextId = "mro-pm-meter", fallback = mro_pm_meterFallback.class)
public interface mro_pm_meterFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meters/{id}")
    Mro_pm_meter update(@PathVariable("id") Long id,@RequestBody Mro_pm_meter mro_pm_meter);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meters/batch")
    Boolean updateBatch(@RequestBody List<Mro_pm_meter> mro_pm_meters);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters")
    Mro_pm_meter create(@RequestBody Mro_pm_meter mro_pm_meter);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/batch")
    Boolean createBatch(@RequestBody List<Mro_pm_meter> mro_pm_meters);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meters/{id}")
    Mro_pm_meter get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/search")
    Page<Mro_pm_meter> search(@RequestBody Mro_pm_meterSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meters/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meters/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meters/select")
    Page<Mro_pm_meter> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meters/getdraft")
    Mro_pm_meter getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/checkkey")
    Boolean checkKey(@RequestBody Mro_pm_meter mro_pm_meter);


    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/save")
    Boolean save(@RequestBody Mro_pm_meter mro_pm_meter);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/savebatch")
    Boolean saveBatch(@RequestBody List<Mro_pm_meter> mro_pm_meters);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/searchdefault")
    Page<Mro_pm_meter> searchDefault(@RequestBody Mro_pm_meterSearchContext context);


}

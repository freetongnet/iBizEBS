package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_country_stateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_country_state] 服务对象接口
 */
public interface IRes_country_stateService extends IService<Res_country_state>{

    boolean create(Res_country_state et) ;
    void createBatch(List<Res_country_state> list) ;
    boolean update(Res_country_state et) ;
    void updateBatch(List<Res_country_state> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_country_state get(Long key) ;
    Res_country_state getDraft(Res_country_state et) ;
    boolean checkKey(Res_country_state et) ;
    boolean save(Res_country_state et) ;
    void saveBatch(List<Res_country_state> list) ;
    Page<Res_country_state> searchDefault(Res_country_stateSearchContext context) ;
    List<Res_country_state> selectByCountryId(Long id);
    void resetByCountryId(Long id);
    void resetByCountryId(Collection<Long> ids);
    void removeByCountryId(Long id);
    List<Res_country_state> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_country_state> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_country_state> getResCountryStateByIds(List<Long> ids) ;
    List<Res_country_state> getResCountryStateByEntities(List<Res_country_state> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_accountSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_account] 服务对象接口
 */
@Component
public class account_accountFallback implements account_accountFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Account_account> search(Account_accountSearchContext context){
            return null;
     }



    public Account_account get(Long id){
            return null;
     }


    public Account_account update(Long id, Account_account account_account){
            return null;
     }
    public Boolean updateBatch(List<Account_account> account_accounts){
            return false;
     }



    public Account_account create(Account_account account_account){
            return null;
     }
    public Boolean createBatch(List<Account_account> account_accounts){
            return false;
     }


    public Page<Account_account> select(){
            return null;
     }

    public Account_account getDraft(){
            return null;
    }



    public Boolean checkKey(Account_account account_account){
            return false;
     }


    public Boolean save(Account_account account_account){
            return false;
     }
    public Boolean saveBatch(List<Account_account> account_accounts){
            return false;
     }

    public Page<Account_account> searchDefault(Account_accountSearchContext context){
            return null;
     }


}

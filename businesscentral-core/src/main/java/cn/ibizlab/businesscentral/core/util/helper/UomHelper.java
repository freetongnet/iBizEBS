package cn.ibizlab.businesscentral.core.util.helper;

import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.businesscentral.core.odoo_uom.filter.Uom_uomSearchContext;
import cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService;
import cn.ibizlab.businesscentral.util.dict.StaticDict;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UomHelper {

    @Autowired
    IUom_uomService uom_uomService;

    private Map<Long, Uom_uom> uom_cache = new HashMap<>();


    /**
     * 转换
     *
     * @param num
     * @param from
     * @param to
     * @return
     */
    public double convert(double num, Uom_uom from, Uom_uom to) {
        if (from.getCategoryId() != to.getCategoryId())
            throw new RuntimeException("计量单位类别不同，无法进行转换。");
        if (from.getId() == to.getId())
            return num;
        //先转换为参考计量单位
        double _num = convert2reference(num, from);
        //再转换成目标单位
        return convert2other(_num, to);
    }

    /**
     * 转换
     * @param num
     * @param from
     * @param to
     * @return
     */
    public double convert(double num, long from, long to) {
        Uom_uom fromUom = this.getUom(from);
        Uom_uom toUom = this.getUom(to);
        return convert(num, fromUom, toUom);
    }

    /**
     * 转换成标准计量单位
     *
     * @param num
     * @param uom
     * @return
     */
    public double convert2reference(double num, Uom_uom uom) {
        //参考计量单位
        if (StringUtils.compare(uom.getUomType(), StaticDict.UOM_UOM__UOM_TYPE.REFERENCE.getValue()) == 0)
            return num;

        return num / uom.getFactor();
    }
    
    /**
     * 转换成其他计量单位
     * @param num
     * @param uom
     * @return
     */
    public double convert2other(double num, Uom_uom uom) {
        //参考计量单位
        if (StringUtils.compare(uom.getUomType(), StaticDict.UOM_UOM__UOM_TYPE.REFERENCE.getValue()) == 0)
            return num;

        return num / uom.getFactor();
    }

    private Uom_uom getUom(long id) {
        if (this.uom_cache.containsKey(id)) {
            return uom_cache.get(id);
        }
        Uom_uom uom = uom_uomService.get(id);
        uom_cache.put(id, uom);
        return uom;

    }

}

package cn.ibizlab.businesscentral.core.odoo_ir.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_model_fields;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_model_fieldsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Ir_model_fields] 服务对象接口
 */
public interface IIr_model_fieldsService extends IService<Ir_model_fields>{

    boolean create(Ir_model_fields et) ;
    void createBatch(List<Ir_model_fields> list) ;
    boolean update(Ir_model_fields et) ;
    void updateBatch(List<Ir_model_fields> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Ir_model_fields get(Long key) ;
    Ir_model_fields getDraft(Ir_model_fields et) ;
    boolean checkKey(Ir_model_fields et) ;
    boolean save(Ir_model_fields et) ;
    void saveBatch(List<Ir_model_fields> list) ;
    Page<Ir_model_fields> searchDefault(Ir_model_fieldsSearchContext context) ;
    Page<Ir_model_fields> searchTrackingFields(Ir_model_fieldsSearchContext context) ;
    List<Ir_model_fields> selectByModelId(Long id);
    void removeByModelId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



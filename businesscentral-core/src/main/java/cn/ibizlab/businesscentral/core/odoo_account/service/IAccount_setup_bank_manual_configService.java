package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_setup_bank_manual_config] 服务对象接口
 */
public interface IAccount_setup_bank_manual_configService extends IService<Account_setup_bank_manual_config>{

    boolean create(Account_setup_bank_manual_config et) ;
    void createBatch(List<Account_setup_bank_manual_config> list) ;
    boolean update(Account_setup_bank_manual_config et) ;
    void updateBatch(List<Account_setup_bank_manual_config> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_setup_bank_manual_config get(Long key) ;
    Account_setup_bank_manual_config getDraft(Account_setup_bank_manual_config et) ;
    boolean checkKey(Account_setup_bank_manual_config et) ;
    boolean save(Account_setup_bank_manual_config et) ;
    void saveBatch(List<Account_setup_bank_manual_config> list) ;
    Page<Account_setup_bank_manual_config> searchDefault(Account_setup_bank_manual_configSearchContext context) ;
    List<Account_setup_bank_manual_config> selectByResPartnerBankId(Long id);
    void removeByResPartnerBankId(Collection<Long> ids);
    void removeByResPartnerBankId(Long id);
    List<Account_setup_bank_manual_config> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_setup_bank_manual_config> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_setup_bank_manual_config> getAccountSetupBankManualConfigByIds(List<Long> ids) ;
    List<Account_setup_bank_manual_config> getAccountSetupBankManualConfigByEntities(List<Account_setup_bank_manual_config> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_botSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_bot] 服务对象接口
 */
@Component
public class mail_botFallback implements mail_botFeignClient{

    public Mail_bot update(Long id, Mail_bot mail_bot){
            return null;
     }
    public Boolean updateBatch(List<Mail_bot> mail_bots){
            return false;
     }


    public Mail_bot create(Mail_bot mail_bot){
            return null;
     }
    public Boolean createBatch(List<Mail_bot> mail_bots){
            return false;
     }


    public Mail_bot get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Mail_bot> search(Mail_botSearchContext context){
            return null;
     }


    public Page<Mail_bot> select(){
            return null;
     }

    public Mail_bot getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_bot mail_bot){
            return false;
     }


    public Boolean save(Mail_bot mail_bot){
            return false;
     }
    public Boolean saveBatch(List<Mail_bot> mail_bots){
            return false;
     }

    public Page<Mail_bot> searchDefault(Mail_botSearchContext context){
            return null;
     }


}

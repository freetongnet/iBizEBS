package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_preview;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_previewSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_preview] 服务对象接口
 */
@Component
public class base_import_tests_models_previewFallback implements base_import_tests_models_previewFeignClient{

    public Page<Base_import_tests_models_preview> search(Base_import_tests_models_previewSearchContext context){
            return null;
     }




    public Base_import_tests_models_preview create(Base_import_tests_models_preview base_import_tests_models_preview){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_preview> base_import_tests_models_previews){
            return false;
     }

    public Base_import_tests_models_preview update(Long id, Base_import_tests_models_preview base_import_tests_models_preview){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_preview> base_import_tests_models_previews){
            return false;
     }


    public Base_import_tests_models_preview get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Base_import_tests_models_preview> select(){
            return null;
     }

    public Base_import_tests_models_preview getDraft(){
            return null;
    }



    public Boolean checkKey(Base_import_tests_models_preview base_import_tests_models_preview){
            return false;
     }


    public Boolean save(Base_import_tests_models_preview base_import_tests_models_preview){
            return false;
     }
    public Boolean saveBatch(List<Base_import_tests_models_preview> base_import_tests_models_previews){
            return false;
     }

    public Page<Base_import_tests_models_preview> searchDefault(Base_import_tests_models_previewSearchContext context){
            return null;
     }


}

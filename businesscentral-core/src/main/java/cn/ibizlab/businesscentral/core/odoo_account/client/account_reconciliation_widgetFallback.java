package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_reconciliation_widget] 服务对象接口
 */
@Component
public class account_reconciliation_widgetFallback implements account_reconciliation_widgetFeignClient{


    public Account_reconciliation_widget update(Long id, Account_reconciliation_widget account_reconciliation_widget){
            return null;
     }
    public Boolean updateBatch(List<Account_reconciliation_widget> account_reconciliation_widgets){
            return false;
     }


    public Page<Account_reconciliation_widget> search(Account_reconciliation_widgetSearchContext context){
            return null;
     }



    public Account_reconciliation_widget get(Long id){
            return null;
     }


    public Account_reconciliation_widget create(Account_reconciliation_widget account_reconciliation_widget){
            return null;
     }
    public Boolean createBatch(List<Account_reconciliation_widget> account_reconciliation_widgets){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Account_reconciliation_widget> select(){
            return null;
     }

    public Account_reconciliation_widget getDraft(){
            return null;
    }



    public Boolean checkKey(Account_reconciliation_widget account_reconciliation_widget){
            return false;
     }


    public Boolean save(Account_reconciliation_widget account_reconciliation_widget){
            return false;
     }
    public Boolean saveBatch(List<Account_reconciliation_widget> account_reconciliation_widgets){
            return false;
     }

    public Page<Account_reconciliation_widget> searchDefault(Account_reconciliation_widgetSearchContext context){
            return null;
     }


}

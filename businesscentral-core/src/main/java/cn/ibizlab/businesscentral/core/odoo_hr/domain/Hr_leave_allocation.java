package cn.ibizlab.businesscentral.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[休假分配]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "HR_LEAVE_ALLOCATION",resultMap = "Hr_leave_allocationResultMap")
public class Hr_leave_allocation extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 说明
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 结束日期
     */
    @DEField(name = "date_to")
    @TableField(value = "date_to")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_to" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_to")
    private Timestamp dateTo;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 每个间隔的单位数
     */
    @DEField(name = "number_per_interval")
    @TableField(value = "number_per_interval")
    @JSONField(name = "number_per_interval")
    @JsonProperty("number_per_interval")
    private Double numberPerInterval;
    /**
     * 持续时间（天）
     */
    @TableField(exist = false)
    @JSONField(name = "number_of_days_display")
    @JsonProperty("number_of_days_display")
    private Double numberOfDaysDisplay;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 分配模式
     */
    @DEField(name = "holiday_type")
    @TableField(value = "holiday_type")
    @JSONField(name = "holiday_type")
    @JsonProperty("holiday_type")
    private String holidayType;
    /**
     * 两个间隔之间的单位数
     */
    @DEField(name = "interval_number")
    @TableField(value = "interval_number")
    @JSONField(name = "interval_number")
    @JsonProperty("interval_number")
    private Integer intervalNumber;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 时长(小时)
     */
    @TableField(exist = false)
    @JSONField(name = "number_of_hours_display")
    @JsonProperty("number_of_hours_display")
    private Double numberOfHoursDisplay;
    /**
     * 权责发生制
     */
    @TableField(value = "accrual")
    @JSONField(name = "accrual")
    @JsonProperty("accrual")
    private Boolean accrual;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 链接申请
     */
    @TableField(exist = false)
    @JSONField(name = "linked_request_ids")
    @JsonProperty("linked_request_ids")
    private String linkedRequestIds;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 信息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 下一次应计分配的日期
     */
    @TableField(value = "nextcall")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "nextcall" , format="yyyy-MM-dd")
    @JsonProperty("nextcall")
    private Timestamp nextcall;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 天数
     */
    @DEField(name = "number_of_days")
    @TableField(value = "number_of_days")
    @JSONField(name = "number_of_days")
    @JsonProperty("number_of_days")
    private Double numberOfDays;
    /**
     * 理由
     */
    @TableField(value = "notes")
    @JSONField(name = "notes")
    @JsonProperty("notes")
    private String notes;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 两个区间之间的时间单位
     */
    @DEField(name = "interval_unit")
    @TableField(value = "interval_unit")
    @JSONField(name = "interval_unit")
    @JsonProperty("interval_unit")
    private String intervalUnit;
    /**
     * 分配 （天/小时）
     */
    @TableField(exist = false)
    @JSONField(name = "duration_display")
    @JsonProperty("duration_display")
    private String durationDisplay;
    /**
     * 能批准
     */
    @TableField(exist = false)
    @JSONField(name = "can_approve")
    @JsonProperty("can_approve")
    private Boolean canApprove;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 能重置
     */
    @TableField(exist = false)
    @JSONField(name = "can_reset")
    @JsonProperty("can_reset")
    private Boolean canReset;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 在每个区间添加的时间单位
     */
    @DEField(name = "unit_per_interval")
    @TableField(value = "unit_per_interval")
    @JSONField(name = "unit_per_interval")
    @JsonProperty("unit_per_interval")
    private String unitPerInterval;
    /**
     * 余额限额
     */
    @DEField(name = "accrual_limit")
    @TableField(value = "accrual_limit")
    @JSONField(name = "accrual_limit")
    @JsonProperty("accrual_limit")
    private Integer accrualLimit;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 开始日期
     */
    @DEField(name = "date_from")
    @TableField(value = "date_from")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_from" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_from")
    private Timestamp dateFrom;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 部门
     */
    @TableField(exist = false)
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;
    /**
     * 员工
     */
    @TableField(exist = false)
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;
    /**
     * 验证人
     */
    @TableField(exist = false)
    @JSONField(name = "validation_type")
    @JsonProperty("validation_type")
    private String validationType;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "mode_company_id_text")
    @JsonProperty("mode_company_id_text")
    private String modeCompanyIdText;
    /**
     * 首次审批
     */
    @TableField(exist = false)
    @JSONField(name = "first_approver_id_text")
    @JsonProperty("first_approver_id_text")
    private String firstApproverIdText;
    /**
     * 休假类型
     */
    @TableField(exist = false)
    @JSONField(name = "holiday_status_id_text")
    @JsonProperty("holiday_status_id_text")
    private String holidayStatusIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 休假
     */
    @TableField(exist = false)
    @JSONField(name = "type_request_unit")
    @JsonProperty("type_request_unit")
    private String typeRequestUnit;
    /**
     * 上级
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;
    /**
     * 第二次审批
     */
    @TableField(exist = false)
    @JSONField(name = "second_approver_id_text")
    @JsonProperty("second_approver_id_text")
    private String secondApproverIdText;
    /**
     * 员工标签
     */
    @TableField(exist = false)
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;
    /**
     * 休假类型
     */
    @DEField(name = "holiday_status_id")
    @TableField(value = "holiday_status_id")
    @JSONField(name = "holiday_status_id")
    @JsonProperty("holiday_status_id")
    private Long holidayStatusId;
    /**
     * 部门
     */
    @DEField(name = "department_id")
    @TableField(value = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Long departmentId;
    /**
     * 上级
     */
    @DEField(name = "parent_id")
    @TableField(value = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 第二次审批
     */
    @DEField(name = "second_approver_id")
    @TableField(value = "second_approver_id")
    @JSONField(name = "second_approver_id")
    @JsonProperty("second_approver_id")
    private Long secondApproverId;
    /**
     * 公司
     */
    @DEField(name = "mode_company_id")
    @TableField(value = "mode_company_id")
    @JSONField(name = "mode_company_id")
    @JsonProperty("mode_company_id")
    private Long modeCompanyId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 首次审批
     */
    @DEField(name = "first_approver_id")
    @TableField(value = "first_approver_id")
    @JSONField(name = "first_approver_id")
    @JsonProperty("first_approver_id")
    private Long firstApproverId;
    /**
     * 员工标签
     */
    @DEField(name = "category_id")
    @TableField(value = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Long categoryId;
    /**
     * 员工
     */
    @DEField(name = "employee_id")
    @TableField(value = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Long employeeId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee_category odooCategory;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooFirstApprover;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooSecondApprover;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_allocation odooParent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_type odooHolidayStatus;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooModeCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [结束日期]
     */
    public void setDateTo(Timestamp dateTo){
        this.dateTo = dateTo ;
        this.modify("date_to",dateTo);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatDateTo(){
        if (this.dateTo == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateTo);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [每个间隔的单位数]
     */
    public void setNumberPerInterval(Double numberPerInterval){
        this.numberPerInterval = numberPerInterval ;
        this.modify("number_per_interval",numberPerInterval);
    }

    /**
     * 设置 [分配模式]
     */
    public void setHolidayType(String holidayType){
        this.holidayType = holidayType ;
        this.modify("holiday_type",holidayType);
    }

    /**
     * 设置 [两个间隔之间的单位数]
     */
    public void setIntervalNumber(Integer intervalNumber){
        this.intervalNumber = intervalNumber ;
        this.modify("interval_number",intervalNumber);
    }

    /**
     * 设置 [权责发生制]
     */
    public void setAccrual(Boolean accrual){
        this.accrual = accrual ;
        this.modify("accrual",accrual);
    }

    /**
     * 设置 [下一次应计分配的日期]
     */
    public void setNextcall(Timestamp nextcall){
        this.nextcall = nextcall ;
        this.modify("nextcall",nextcall);
    }

    /**
     * 格式化日期 [下一次应计分配的日期]
     */
    public String formatNextcall(){
        if (this.nextcall == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(nextcall);
    }
    /**
     * 设置 [天数]
     */
    public void setNumberOfDays(Double numberOfDays){
        this.numberOfDays = numberOfDays ;
        this.modify("number_of_days",numberOfDays);
    }

    /**
     * 设置 [理由]
     */
    public void setNotes(String notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [两个区间之间的时间单位]
     */
    public void setIntervalUnit(String intervalUnit){
        this.intervalUnit = intervalUnit ;
        this.modify("interval_unit",intervalUnit);
    }

    /**
     * 设置 [在每个区间添加的时间单位]
     */
    public void setUnitPerInterval(String unitPerInterval){
        this.unitPerInterval = unitPerInterval ;
        this.modify("unit_per_interval",unitPerInterval);
    }

    /**
     * 设置 [余额限额]
     */
    public void setAccrualLimit(Integer accrualLimit){
        this.accrualLimit = accrualLimit ;
        this.modify("accrual_limit",accrualLimit);
    }

    /**
     * 设置 [开始日期]
     */
    public void setDateFrom(Timestamp dateFrom){
        this.dateFrom = dateFrom ;
        this.modify("date_from",dateFrom);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatDateFrom(){
        if (this.dateFrom == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateFrom);
    }
    /**
     * 设置 [休假类型]
     */
    public void setHolidayStatusId(Long holidayStatusId){
        this.holidayStatusId = holidayStatusId ;
        this.modify("holiday_status_id",holidayStatusId);
    }

    /**
     * 设置 [部门]
     */
    public void setDepartmentId(Long departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }

    /**
     * 设置 [上级]
     */
    public void setParentId(Long parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [第二次审批]
     */
    public void setSecondApproverId(Long secondApproverId){
        this.secondApproverId = secondApproverId ;
        this.modify("second_approver_id",secondApproverId);
    }

    /**
     * 设置 [公司]
     */
    public void setModeCompanyId(Long modeCompanyId){
        this.modeCompanyId = modeCompanyId ;
        this.modify("mode_company_id",modeCompanyId);
    }

    /**
     * 设置 [首次审批]
     */
    public void setFirstApproverId(Long firstApproverId){
        this.firstApproverId = firstApproverId ;
        this.modify("first_approver_id",firstApproverId);
    }

    /**
     * 设置 [员工标签]
     */
    public void setCategoryId(Long categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

    /**
     * 设置 [员工]
     */
    public void setEmployeeId(Long employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



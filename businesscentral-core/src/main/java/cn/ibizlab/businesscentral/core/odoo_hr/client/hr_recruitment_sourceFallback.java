package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_source;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_recruitment_source] 服务对象接口
 */
@Component
public class hr_recruitment_sourceFallback implements hr_recruitment_sourceFeignClient{


    public Hr_recruitment_source get(Long id){
            return null;
     }


    public Page<Hr_recruitment_source> search(Hr_recruitment_sourceSearchContext context){
            return null;
     }


    public Hr_recruitment_source update(Long id, Hr_recruitment_source hr_recruitment_source){
            return null;
     }
    public Boolean updateBatch(List<Hr_recruitment_source> hr_recruitment_sources){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Hr_recruitment_source create(Hr_recruitment_source hr_recruitment_source){
            return null;
     }
    public Boolean createBatch(List<Hr_recruitment_source> hr_recruitment_sources){
            return false;
     }

    public Page<Hr_recruitment_source> select(){
            return null;
     }

    public Hr_recruitment_source getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_recruitment_source hr_recruitment_source){
            return false;
     }


    public Boolean save(Hr_recruitment_source hr_recruitment_source){
            return false;
     }
    public Boolean saveBatch(List<Hr_recruitment_source> hr_recruitment_sources){
            return false;
     }

    public Page<Hr_recruitment_source> searchDefault(Hr_recruitment_sourceSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_float;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_floatSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_float] 服务对象接口
 */
@Component
public class base_import_tests_models_floatFallback implements base_import_tests_models_floatFeignClient{

    public Base_import_tests_models_float create(Base_import_tests_models_float base_import_tests_models_float){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_float> base_import_tests_models_floats){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Base_import_tests_models_float get(Long id){
            return null;
     }




    public Base_import_tests_models_float update(Long id, Base_import_tests_models_float base_import_tests_models_float){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_float> base_import_tests_models_floats){
            return false;
     }



    public Page<Base_import_tests_models_float> search(Base_import_tests_models_floatSearchContext context){
            return null;
     }


    public Page<Base_import_tests_models_float> select(){
            return null;
     }

    public Base_import_tests_models_float getDraft(){
            return null;
    }



    public Boolean checkKey(Base_import_tests_models_float base_import_tests_models_float){
            return false;
     }


    public Boolean save(Base_import_tests_models_float base_import_tests_models_float){
            return false;
     }
    public Boolean saveBatch(List<Base_import_tests_models_float> base_import_tests_models_floats){
            return false;
     }

    public Page<Base_import_tests_models_float> searchDefault(Base_import_tests_models_floatSearchContext context){
            return null;
     }


}

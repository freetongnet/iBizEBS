package cn.ibizlab.businesscentral.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Gamification_goal_definition] 服务对象接口
 */
public interface IGamification_goal_definitionService extends IService<Gamification_goal_definition>{

    boolean create(Gamification_goal_definition et) ;
    void createBatch(List<Gamification_goal_definition> list) ;
    boolean update(Gamification_goal_definition et) ;
    void updateBatch(List<Gamification_goal_definition> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Gamification_goal_definition get(Long key) ;
    Gamification_goal_definition getDraft(Gamification_goal_definition et) ;
    boolean checkKey(Gamification_goal_definition et) ;
    boolean save(Gamification_goal_definition et) ;
    void saveBatch(List<Gamification_goal_definition> list) ;
    Page<Gamification_goal_definition> searchDefault(Gamification_goal_definitionSearchContext context) ;
    List<Gamification_goal_definition> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Gamification_goal_definition> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Gamification_goal_definition> getGamificationGoalDefinitionByIds(List<Long> ids) ;
    List<Gamification_goal_definition> getGamificationGoalDefinitionByEntities(List<Gamification_goal_definition> entities) ;
}



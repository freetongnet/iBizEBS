package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_installer;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_config_installerSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_config_installer] 服务对象接口
 */
public interface IRes_config_installerService extends IService<Res_config_installer>{

    boolean create(Res_config_installer et) ;
    void createBatch(List<Res_config_installer> list) ;
    boolean update(Res_config_installer et) ;
    void updateBatch(List<Res_config_installer> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_config_installer get(Long key) ;
    Res_config_installer getDraft(Res_config_installer et) ;
    boolean checkKey(Res_config_installer et) ;
    boolean save(Res_config_installer et) ;
    void saveBatch(List<Res_config_installer> list) ;
    Page<Res_config_installer> searchDefault(Res_config_installerSearchContext context) ;
    List<Res_config_installer> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_config_installer> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_config_installer> getResConfigInstallerByIds(List<Long> ids) ;
    List<Res_config_installer> getResConfigInstallerByEntities(List<Res_config_installer> entities) ;
}



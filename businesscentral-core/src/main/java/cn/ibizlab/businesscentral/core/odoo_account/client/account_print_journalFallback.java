package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_print_journal;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_print_journalSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_print_journal] 服务对象接口
 */
@Component
public class account_print_journalFallback implements account_print_journalFeignClient{

    public Account_print_journal create(Account_print_journal account_print_journal){
            return null;
     }
    public Boolean createBatch(List<Account_print_journal> account_print_journals){
            return false;
     }


    public Account_print_journal update(Long id, Account_print_journal account_print_journal){
            return null;
     }
    public Boolean updateBatch(List<Account_print_journal> account_print_journals){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Account_print_journal> search(Account_print_journalSearchContext context){
            return null;
     }


    public Account_print_journal get(Long id){
            return null;
     }



    public Page<Account_print_journal> select(){
            return null;
     }

    public Account_print_journal getDraft(){
            return null;
    }



    public Boolean checkKey(Account_print_journal account_print_journal){
            return false;
     }


    public Boolean save(Account_print_journal account_print_journal){
            return false;
     }
    public Boolean saveBatch(List<Account_print_journal> account_print_journals){
            return false;
     }

    public Page<Account_print_journal> searchDefault(Account_print_journalSearchContext context){
            return null;
     }


}

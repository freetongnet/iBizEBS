package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_unbuildSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_unbuild] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-unbuild", fallback = mrp_unbuildFallback.class)
public interface mrp_unbuildFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds")
    Mrp_unbuild create(@RequestBody Mrp_unbuild mrp_unbuild);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/batch")
    Boolean createBatch(@RequestBody List<Mrp_unbuild> mrp_unbuilds);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/search")
    Page<Mrp_unbuild> search(@RequestBody Mrp_unbuildSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_unbuilds/{id}")
    Mrp_unbuild update(@PathVariable("id") Long id,@RequestBody Mrp_unbuild mrp_unbuild);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_unbuilds/batch")
    Boolean updateBatch(@RequestBody List<Mrp_unbuild> mrp_unbuilds);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_unbuilds/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_unbuilds/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_unbuilds/{id}")
    Mrp_unbuild get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_unbuilds/select")
    Page<Mrp_unbuild> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_unbuilds/getdraft")
    Mrp_unbuild getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/checkkey")
    Boolean checkKey(@RequestBody Mrp_unbuild mrp_unbuild);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/save")
    Boolean save(@RequestBody Mrp_unbuild mrp_unbuild);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_unbuild> mrp_unbuilds);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/searchdefault")
    Page<Mrp_unbuild> searchDefault(@RequestBody Mrp_unbuildSearchContext context);


}

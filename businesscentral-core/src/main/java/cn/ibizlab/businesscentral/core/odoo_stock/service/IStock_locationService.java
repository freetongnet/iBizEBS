package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_locationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_location] 服务对象接口
 */
public interface IStock_locationService extends IService<Stock_location>{

    boolean create(Stock_location et) ;
    void createBatch(List<Stock_location> list) ;
    boolean update(Stock_location et) ;
    void updateBatch(List<Stock_location> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_location get(Long key) ;
    Stock_location getDraft(Stock_location et) ;
    boolean checkKey(Stock_location et) ;
    boolean save(Stock_location et) ;
    void saveBatch(List<Stock_location> list) ;
    Page<Stock_location> searchDefault(Stock_locationSearchContext context) ;
    List<Stock_location> selectByValuationInAccountId(Long id);
    void resetByValuationInAccountId(Long id);
    void resetByValuationInAccountId(Collection<Long> ids);
    void removeByValuationInAccountId(Long id);
    List<Stock_location> selectByValuationOutAccountId(Long id);
    void resetByValuationOutAccountId(Long id);
    void resetByValuationOutAccountId(Collection<Long> ids);
    void removeByValuationOutAccountId(Long id);
    List<Stock_location> selectByRemovalStrategyId(Long id);
    void resetByRemovalStrategyId(Long id);
    void resetByRemovalStrategyId(Collection<Long> ids);
    void removeByRemovalStrategyId(Long id);
    List<Stock_location> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_location> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_location> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_location> getStockLocationByIds(List<Long> ids) ;
    List<Stock_location> getStockLocationByEntities(List<Stock_location> entities) ;
}



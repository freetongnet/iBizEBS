package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_o2m;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_o2mSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[base_import_tests_models_o2m] 服务对象接口
 */
@Component
public class base_import_tests_models_o2mFallback implements base_import_tests_models_o2mFeignClient{

    public Base_import_tests_models_o2m update(Long id, Base_import_tests_models_o2m base_import_tests_models_o2m){
            return null;
     }
    public Boolean updateBatch(List<Base_import_tests_models_o2m> base_import_tests_models_o2ms){
            return false;
     }


    public Base_import_tests_models_o2m get(Long id){
            return null;
     }


    public Base_import_tests_models_o2m create(Base_import_tests_models_o2m base_import_tests_models_o2m){
            return null;
     }
    public Boolean createBatch(List<Base_import_tests_models_o2m> base_import_tests_models_o2ms){
            return false;
     }




    public Page<Base_import_tests_models_o2m> search(Base_import_tests_models_o2mSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Base_import_tests_models_o2m> select(){
            return null;
     }

    public Base_import_tests_models_o2m getDraft(){
            return null;
    }



    public Boolean checkKey(Base_import_tests_models_o2m base_import_tests_models_o2m){
            return false;
     }


    public Boolean save(Base_import_tests_models_o2m base_import_tests_models_o2m){
            return false;
     }
    public Boolean saveBatch(List<Base_import_tests_models_o2m> base_import_tests_models_o2ms){
            return false;
     }

    public Page<Base_import_tests_models_o2m> searchDefault(Base_import_tests_models_o2mSearchContext context){
            return null;
     }


}

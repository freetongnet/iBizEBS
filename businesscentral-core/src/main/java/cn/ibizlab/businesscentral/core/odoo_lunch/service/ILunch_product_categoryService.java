package cn.ibizlab.businesscentral.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_product_categorySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Lunch_product_category] 服务对象接口
 */
public interface ILunch_product_categoryService extends IService<Lunch_product_category>{

    boolean create(Lunch_product_category et) ;
    void createBatch(List<Lunch_product_category> list) ;
    boolean update(Lunch_product_category et) ;
    void updateBatch(List<Lunch_product_category> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Lunch_product_category get(Long key) ;
    Lunch_product_category getDraft(Lunch_product_category et) ;
    boolean checkKey(Lunch_product_category et) ;
    boolean save(Lunch_product_category et) ;
    void saveBatch(List<Lunch_product_category> list) ;
    Page<Lunch_product_category> searchDefault(Lunch_product_categorySearchContext context) ;
    List<Lunch_product_category> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Lunch_product_category> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Lunch_product_category> getLunchProductCategoryByIds(List<Long> ids) ;
    List<Lunch_product_category> getLunchProductCategoryByEntities(List<Lunch_product_category> entities) ;
}



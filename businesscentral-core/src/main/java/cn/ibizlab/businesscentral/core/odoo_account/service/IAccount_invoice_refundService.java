package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_refund;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_refundSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_invoice_refund] 服务对象接口
 */
public interface IAccount_invoice_refundService extends IService<Account_invoice_refund>{

    boolean create(Account_invoice_refund et) ;
    void createBatch(List<Account_invoice_refund> list) ;
    boolean update(Account_invoice_refund et) ;
    void updateBatch(List<Account_invoice_refund> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_invoice_refund get(Long key) ;
    Account_invoice_refund getDraft(Account_invoice_refund et) ;
    boolean checkKey(Account_invoice_refund et) ;
    boolean save(Account_invoice_refund et) ;
    void saveBatch(List<Account_invoice_refund> list) ;
    Page<Account_invoice_refund> searchDefault(Account_invoice_refundSearchContext context) ;
    List<Account_invoice_refund> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_invoice_refund> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_invoice_refund> getAccountInvoiceRefundByIds(List<Long> ids) ;
    List<Account_invoice_refund> getAccountInvoiceRefundByEntities(List<Account_invoice_refund> entities) ;
}



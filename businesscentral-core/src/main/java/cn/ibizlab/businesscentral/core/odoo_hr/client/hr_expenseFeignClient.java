package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expenseSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_expense] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-expense", fallback = hr_expenseFallback.class)
public interface hr_expenseFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/hr_expenses")
    Hr_expense create(@RequestBody Hr_expense hr_expense);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/batch")
    Boolean createBatch(@RequestBody List<Hr_expense> hr_expenses);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expenses/{id}")
    Hr_expense get(@PathVariable("id") Long id);





    @RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/search")
    Page<Hr_expense> search(@RequestBody Hr_expenseSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_expenses/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_expenses/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_expenses/{id}")
    Hr_expense update(@PathVariable("id") Long id,@RequestBody Hr_expense hr_expense);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_expenses/batch")
    Boolean updateBatch(@RequestBody List<Hr_expense> hr_expenses);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expenses/select")
    Page<Hr_expense> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_expenses/getdraft")
    Hr_expense getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/checkkey")
    Boolean checkKey(@RequestBody Hr_expense hr_expense);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/save")
    Boolean save(@RequestBody Hr_expense hr_expense);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_expense> hr_expenses);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/searchdefault")
    Page<Hr_expense> searchDefault(@RequestBody Hr_expenseSearchContext context);


}

package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence;
import cn.ibizlab.businesscentral.core.odoo_ir.service.impl.Ir_sequence_date_rangeServiceImpl;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence_date_range;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[序列日期范围] 自定义服务对象
 */
@Slf4j
@Primary
@Service("Ir_sequence_date_rangeExService")
public class Ir_sequence_date_rangeExService extends Ir_sequence_date_rangeServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[Create_sequence]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Ir_sequence_date_range create_sequence(Ir_sequence_date_range et) {
        return super.create_sequence(et);
    }
    /**
     * 自定义行为[Select_nextval]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Ir_sequence_date_range select_nextval(Ir_sequence_date_range et) {
        List<JSONObject> data = this.select(String.format("SELECT nextval('ir_sequence_%03d_%03d')",et.getSequenceId(), et.getId()), null);
        int nextval = data.get(0).getInteger("nextval");
        et.setNextVal(nextval);
        return super.select_nextval(et);
    }
    /**
     * 自定义行为[Update_nogap]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Ir_sequence_date_range update_nogap(Ir_sequence_date_range et) {
        Ir_sequence sequence = irSequenceService.get(et.getSequenceId());
        et.setNumberNext(et.getNumberNext() + sequence.getNumberIncrement());
        this.update(et);
        return super.update_nogap(et);
    }
}


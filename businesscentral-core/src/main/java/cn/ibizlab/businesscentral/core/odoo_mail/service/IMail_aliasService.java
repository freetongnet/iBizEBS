package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_aliasSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_alias] 服务对象接口
 */
public interface IMail_aliasService extends IService<Mail_alias>{

    boolean create(Mail_alias et) ;
    void createBatch(List<Mail_alias> list) ;
    boolean update(Mail_alias et) ;
    void updateBatch(List<Mail_alias> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_alias get(Long key) ;
    Mail_alias getDraft(Mail_alias et) ;
    boolean checkKey(Mail_alias et) ;
    boolean save(Mail_alias et) ;
    void saveBatch(List<Mail_alias> list) ;
    Page<Mail_alias> searchDefault(Mail_aliasSearchContext context) ;
    List<Mail_alias> selectByAliasUserId(Long id);
    void resetByAliasUserId(Long id);
    void resetByAliasUserId(Collection<Long> ids);
    void removeByAliasUserId(Long id);
    List<Mail_alias> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_alias> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_alias> getMailAliasByIds(List<Long> ids) ;
    List<Mail_alias> getMailAliasByEntities(List<Mail_alias> entities) ;
}



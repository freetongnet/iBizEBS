package cn.ibizlab.businesscentral.core.odoo_lunch.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_alertSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Lunch_alertMapper extends BaseMapper<Lunch_alert>{

    Page<Lunch_alert> searchDefault(IPage page, @Param("srf") Lunch_alertSearchContext context, @Param("ew") Wrapper<Lunch_alert> wrapper) ;
    @Override
    Lunch_alert selectById(Serializable id);
    @Override
    int insert(Lunch_alert entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Lunch_alert entity);
    @Override
    int update(@Param(Constants.ENTITY) Lunch_alert entity, @Param("ew") Wrapper<Lunch_alert> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Lunch_alert> selectByPartnerId(@Param("id") Serializable id) ;

    List<Lunch_alert> selectByCreateUid(@Param("id") Serializable id) ;

    List<Lunch_alert> selectByWriteUid(@Param("id") Serializable id) ;


}

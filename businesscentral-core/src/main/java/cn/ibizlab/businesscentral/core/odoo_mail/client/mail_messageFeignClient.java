package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_messageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_message] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-message", fallback = mail_messageFallback.class)
public interface mail_messageFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_messages/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages")
    Mail_message create(@RequestBody Mail_message mail_message);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages/batch")
    Boolean createBatch(@RequestBody List<Mail_message> mail_messages);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_messages/{id}")
    Mail_message get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages/search")
    Page<Mail_message> search(@RequestBody Mail_messageSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/{id}")
    Mail_message update(@PathVariable("id") Long id,@RequestBody Mail_message mail_message);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_messages/batch")
    Boolean updateBatch(@RequestBody List<Mail_message> mail_messages);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_messages/select")
    Page<Mail_message> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_messages/getdraft")
    Mail_message getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages/checkkey")
    Boolean checkKey(@RequestBody Mail_message mail_message);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{id}/generate_tracking_message_id")
    Mail_message generate_tracking_message_id(@PathVariable("id") Long id,@RequestBody Mail_message mail_message);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages/{id}/post_message")
    Mail_message post_message(@PathVariable("id") Long id,@RequestBody Mail_message mail_message);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages/save")
    Boolean save(@RequestBody Mail_message mail_message);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_message> mail_messages);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages/searchbyres")
    Page<Mail_message> searchByRes(@RequestBody Mail_messageSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_messages/searchdefault")
    Page<Mail_message> searchDefault(@RequestBody Mail_messageSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_resend_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_resend_messageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_resend_message] 服务对象接口
 */
public interface IMail_resend_messageService extends IService<Mail_resend_message>{

    boolean create(Mail_resend_message et) ;
    void createBatch(List<Mail_resend_message> list) ;
    boolean update(Mail_resend_message et) ;
    void updateBatch(List<Mail_resend_message> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_resend_message get(Long key) ;
    Mail_resend_message getDraft(Mail_resend_message et) ;
    boolean checkKey(Mail_resend_message et) ;
    boolean save(Mail_resend_message et) ;
    void saveBatch(List<Mail_resend_message> list) ;
    Page<Mail_resend_message> searchDefault(Mail_resend_messageSearchContext context) ;
    List<Mail_resend_message> selectByMailMessageId(Long id);
    void resetByMailMessageId(Long id);
    void resetByMailMessageId(Collection<Long> ids);
    void removeByMailMessageId(Long id);
    List<Mail_resend_message> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_resend_message> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_resend_message> getMailResendMessageByIds(List<Long> ids) ;
    List<Mail_resend_message> getMailResendMessageByEntities(List<Mail_resend_message> entities) ;
}



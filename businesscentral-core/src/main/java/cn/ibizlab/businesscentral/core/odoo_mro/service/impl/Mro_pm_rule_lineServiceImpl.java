package cn.ibizlab.businesscentral.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_rule_line;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_rule_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_rule_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mro.mapper.Mro_pm_rule_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[Rule for Task] 服务对象接口实现
 */
@Slf4j
@Service("Mro_pm_rule_lineServiceImpl")
public class Mro_pm_rule_lineServiceImpl extends EBSServiceImpl<Mro_pm_rule_lineMapper, Mro_pm_rule_line> implements IMro_pm_rule_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_meter_intervalService mroPmMeterIntervalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_pm_ruleService mroPmRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mro.service.IMro_taskService mroTaskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mro.pm.rule.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mro_pm_rule_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMro_pm_rule_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mro_pm_rule_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mro_pm_rule_line et) {
        Mro_pm_rule_line old = new Mro_pm_rule_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMro_pm_rule_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMro_pm_rule_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mro_pm_rule_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mro_pm_rule_line get(Long key) {
        Mro_pm_rule_line et = getById(key);
        if(et==null){
            et=new Mro_pm_rule_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mro_pm_rule_line getDraft(Mro_pm_rule_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mro_pm_rule_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mro_pm_rule_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mro_pm_rule_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mro_pm_rule_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mro_pm_rule_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mro_pm_rule_line> selectByMeterIntervalId(Long id) {
        return baseMapper.selectByMeterIntervalId(id);
    }
    @Override
    public List<Mro_pm_rule_line> selectByMeterIntervalId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mro_pm_rule_line>().in("id",ids));
    }

    @Override
    public void removeByMeterIntervalId(Long id) {
        this.remove(new QueryWrapper<Mro_pm_rule_line>().eq("meter_interval_id",id));
    }

	@Override
    public List<Mro_pm_rule_line> selectByPmRuleId(Long id) {
        return baseMapper.selectByPmRuleId(id);
    }
    @Override
    public List<Mro_pm_rule_line> selectByPmRuleId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mro_pm_rule_line>().in("id",ids));
    }

    @Override
    public void removeByPmRuleId(Long id) {
        this.remove(new QueryWrapper<Mro_pm_rule_line>().eq("pm_rule_id",id));
    }

	@Override
    public List<Mro_pm_rule_line> selectByTaskId(Long id) {
        return baseMapper.selectByTaskId(id);
    }
    @Override
    public List<Mro_pm_rule_line> selectByTaskId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Mro_pm_rule_line>().in("id",ids));
    }

    @Override
    public void removeByTaskId(Long id) {
        this.remove(new QueryWrapper<Mro_pm_rule_line>().eq("task_id",id));
    }

	@Override
    public List<Mro_pm_rule_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mro_pm_rule_line>().eq("create_uid",id));
    }

	@Override
    public List<Mro_pm_rule_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mro_pm_rule_line>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mro_pm_rule_line> searchDefault(Mro_pm_rule_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mro_pm_rule_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mro_pm_rule_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mro_pm_rule_line et){
        //实体关系[DER1N_MRO_PM_RULE_LINE__MRO_PM_METER_INTERVAL__METER_INTERVAL_ID]
        if(!ObjectUtils.isEmpty(et.getMeterIntervalId())){
            cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_interval odooMeterInterval=et.getOdooMeterInterval();
            if(ObjectUtils.isEmpty(odooMeterInterval)){
                cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_interval majorEntity=mroPmMeterIntervalService.get(et.getMeterIntervalId());
                et.setOdooMeterInterval(majorEntity);
                odooMeterInterval=majorEntity;
            }
            et.setMeterIntervalIdText(odooMeterInterval.getName());
        }
        //实体关系[DER1N_MRO_PM_RULE_LINE__MRO_PM_RULE__PM_RULE_ID]
        if(!ObjectUtils.isEmpty(et.getPmRuleId())){
            cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_rule odooPmRule=et.getOdooPmRule();
            if(ObjectUtils.isEmpty(odooPmRule)){
                cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_rule majorEntity=mroPmRuleService.get(et.getPmRuleId());
                et.setOdooPmRule(majorEntity);
                odooPmRule=majorEntity;
            }
            et.setPmRuleIdText(odooPmRule.getName());
        }
        //实体关系[DER1N_MRO_PM_RULE_LINE__MRO_TASK__TASK_ID]
        if(!ObjectUtils.isEmpty(et.getTaskId())){
            cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task odooTask=et.getOdooTask();
            if(ObjectUtils.isEmpty(odooTask)){
                cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task majorEntity=mroTaskService.get(et.getTaskId());
                et.setOdooTask(majorEntity);
                odooTask=majorEntity;
            }
            et.setTaskIdText(odooTask.getName());
        }
        //实体关系[DER1N_MRO_PM_RULE_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MRO_PM_RULE_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mro_pm_rule_line> getMroPmRuleLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mro_pm_rule_line> getMroPmRuleLineByEntities(List<Mro_pm_rule_line> entities) {
        List ids =new ArrayList();
        for(Mro_pm_rule_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




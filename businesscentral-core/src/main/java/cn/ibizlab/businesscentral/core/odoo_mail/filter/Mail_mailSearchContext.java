package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mail;
/**
 * 关系型数据实体[Mail_mail] 查询条件对象
 */
@Slf4j
@Data
public class Mail_mailSearchContext extends QueryWrapperContext<Mail_mail> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_mailing_id_text_eq;//[群发邮件]
	public void setN_mailing_id_text_eq(String n_mailing_id_text_eq) {
        this.n_mailing_id_text_eq = n_mailing_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_mailing_id_text_eq)){
            this.getSearchCond().eq("mailing_id_text", n_mailing_id_text_eq);
        }
    }
	private String n_mailing_id_text_like;//[群发邮件]
	public void setN_mailing_id_text_like(String n_mailing_id_text_like) {
        this.n_mailing_id_text_like = n_mailing_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_mailing_id_text_like)){
            this.getSearchCond().like("mailing_id_text", n_mailing_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_fetchmail_server_id_text_eq;//[收件服务器]
	public void setN_fetchmail_server_id_text_eq(String n_fetchmail_server_id_text_eq) {
        this.n_fetchmail_server_id_text_eq = n_fetchmail_server_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_fetchmail_server_id_text_eq)){
            this.getSearchCond().eq("fetchmail_server_id_text", n_fetchmail_server_id_text_eq);
        }
    }
	private String n_fetchmail_server_id_text_like;//[收件服务器]
	public void setN_fetchmail_server_id_text_like(String n_fetchmail_server_id_text_like) {
        this.n_fetchmail_server_id_text_like = n_fetchmail_server_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_fetchmail_server_id_text_like)){
            this.getSearchCond().like("fetchmail_server_id_text", n_fetchmail_server_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_mailing_id_eq;//[群发邮件]
	public void setN_mailing_id_eq(Long n_mailing_id_eq) {
        this.n_mailing_id_eq = n_mailing_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mailing_id_eq)){
            this.getSearchCond().eq("mailing_id", n_mailing_id_eq);
        }
    }
	private Long n_fetchmail_server_id_eq;//[收件服务器]
	public void setN_fetchmail_server_id_eq(Long n_fetchmail_server_id_eq) {
        this.n_fetchmail_server_id_eq = n_fetchmail_server_id_eq;
        if(!ObjectUtils.isEmpty(this.n_fetchmail_server_id_eq)){
            this.getSearchCond().eq("fetchmail_server_id", n_fetchmail_server_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_mail_message_id_eq;//[消息]
	public void setN_mail_message_id_eq(Long n_mail_message_id_eq) {
        this.n_mail_message_id_eq = n_mail_message_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mail_message_id_eq)){
            this.getSearchCond().eq("mail_message_id", n_mail_message_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




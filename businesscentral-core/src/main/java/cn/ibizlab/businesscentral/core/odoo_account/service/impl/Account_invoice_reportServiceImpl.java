package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_reportSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_reportService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_invoice_reportMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[发票统计] 服务对象接口实现
 */
@Slf4j
@Service("Account_invoice_reportServiceImpl")
public class Account_invoice_reportServiceImpl extends EBSServiceImpl<Account_invoice_reportMapper, Account_invoice_report> implements IAccount_invoice_reportService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService accountFiscalPositionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService accountJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService accountPaymentTermService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService crmTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_categoryService productCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService resCountryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_bankService resPartnerBankService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.invoice.report" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_invoice_report et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_reportService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_invoice_report> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_invoice_report et) {
        Account_invoice_report old = new Account_invoice_report() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_reportService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_reportService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_invoice_report> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_invoice_report get(Long key) {
        Account_invoice_report et = getById(key);
        if(et==null){
            et=new Account_invoice_report();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_invoice_report getDraft(Account_invoice_report et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_invoice_report et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_invoice_report et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_invoice_report et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_invoice_report> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_invoice_report> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_invoice_report> selectByAccountId(Long id) {
        return baseMapper.selectByAccountId(id);
    }
    @Override
    public void removeByAccountId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("account_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByAccountLineId(Long id) {
        return baseMapper.selectByAccountLineId(id);
    }
    @Override
    public void removeByAccountLineId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("account_line_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByAccountAnalyticId(Long id) {
        return baseMapper.selectByAccountAnalyticId(id);
    }
    @Override
    public void removeByAccountAnalyticId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("account_analytic_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByFiscalPositionId(Long id) {
        return baseMapper.selectByFiscalPositionId(id);
    }
    @Override
    public void removeByFiscalPositionId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("fiscal_position_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByInvoiceId(Long id) {
        return baseMapper.selectByInvoiceId(id);
    }
    @Override
    public void removeByInvoiceId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("invoice_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByJournalId(Long id) {
        return baseMapper.selectByJournalId(id);
    }
    @Override
    public void removeByJournalId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("journal_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByPaymentTermId(Long id) {
        return baseMapper.selectByPaymentTermId(id);
    }
    @Override
    public void removeByPaymentTermId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("payment_term_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByTeamId(Long id) {
        return baseMapper.selectByTeamId(id);
    }
    @Override
    public void removeByTeamId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("team_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByCategId(Long id) {
        return baseMapper.selectByCategId(id);
    }
    @Override
    public void removeByCategId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("categ_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("product_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("company_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByCountryId(Long id) {
        return baseMapper.selectByCountryId(id);
    }
    @Override
    public void removeByCountryId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("country_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("currency_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByPartnerBankId(Long id) {
        return baseMapper.selectByPartnerBankId(id);
    }
    @Override
    public void removeByPartnerBankId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("partner_bank_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByCommercialPartnerId(Long id) {
        return baseMapper.selectByCommercialPartnerId(id);
    }
    @Override
    public void removeByCommercialPartnerId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("commercial_partner_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("partner_id",id));
    }

	@Override
    public List<Account_invoice_report> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_report>().eq("user_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_invoice_report> searchDefault(Account_invoice_reportSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_invoice_report> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_invoice_report>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_invoice_report et){
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__ACCOUNT_ACCOUNT__ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount=et.getOdooAccount();
            if(ObjectUtils.isEmpty(odooAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountId());
                et.setOdooAccount(majorEntity);
                odooAccount=majorEntity;
            }
            et.setAccountIdText(odooAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__ACCOUNT_ACCOUNT__ACCOUNT_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getAccountLineId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccountLine=et.getOdooAccountLine();
            if(ObjectUtils.isEmpty(odooAccountLine)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountLineId());
                et.setOdooAccountLine(majorEntity);
                odooAccountLine=majorEntity;
            }
            et.setAccountLineIdText(odooAccountLine.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__ACCOUNT_ANALYTIC_ACCOUNT__ACCOUNT_ANALYTIC_ID]
        if(!ObjectUtils.isEmpty(et.getAccountAnalyticId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic=et.getOdooAccountAnalytic();
            if(ObjectUtils.isEmpty(odooAccountAnalytic)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAccountAnalyticId());
                et.setOdooAccountAnalytic(majorEntity);
                odooAccountAnalytic=majorEntity;
            }
            et.setAccountAnalyticIdText(odooAccountAnalytic.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__ACCOUNT_FISCAL_POSITION__FISCAL_POSITION_ID]
        if(!ObjectUtils.isEmpty(et.getFiscalPositionId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition=et.getOdooFiscalPosition();
            if(ObjectUtils.isEmpty(odooFiscalPosition)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position majorEntity=accountFiscalPositionService.get(et.getFiscalPositionId());
                et.setOdooFiscalPosition(majorEntity);
                odooFiscalPosition=majorEntity;
            }
            et.setFiscalPositionIdText(odooFiscalPosition.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__ACCOUNT_INVOICE__INVOICE_ID]
        if(!ObjectUtils.isEmpty(et.getInvoiceId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooInvoice=et.getOdooInvoice();
            if(ObjectUtils.isEmpty(odooInvoice)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice majorEntity=accountInvoiceService.get(et.getInvoiceId());
                et.setOdooInvoice(majorEntity);
                odooInvoice=majorEntity;
            }
            et.setInvoiceIdText(odooInvoice.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__ACCOUNT_JOURNAL__JOURNAL_ID]
        if(!ObjectUtils.isEmpty(et.getJournalId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal=et.getOdooJournal();
            if(ObjectUtils.isEmpty(odooJournal)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal majorEntity=accountJournalService.get(et.getJournalId());
                et.setOdooJournal(majorEntity);
                odooJournal=majorEntity;
            }
            et.setJournalIdText(odooJournal.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__ACCOUNT_PAYMENT_TERM__PAYMENT_TERM_ID]
        if(!ObjectUtils.isEmpty(et.getPaymentTermId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooPaymentTerm=et.getOdooPaymentTerm();
            if(ObjectUtils.isEmpty(odooPaymentTerm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term majorEntity=accountPaymentTermService.get(et.getPaymentTermId());
                et.setOdooPaymentTerm(majorEntity);
                odooPaymentTerm=majorEntity;
            }
            et.setPaymentTermIdText(odooPaymentTerm.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__CRM_TEAM__TEAM_ID]
        if(!ObjectUtils.isEmpty(et.getTeamId())){
            cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam=et.getOdooTeam();
            if(ObjectUtils.isEmpty(odooTeam)){
                cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team majorEntity=crmTeamService.get(et.getTeamId());
                et.setOdooTeam(majorEntity);
                odooTeam=majorEntity;
            }
            et.setTeamIdText(odooTeam.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__PRODUCT_CATEGORY__CATEG_ID]
        if(!ObjectUtils.isEmpty(et.getCategId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category odooCateg=et.getOdooCateg();
            if(ObjectUtils.isEmpty(odooCateg)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category majorEntity=productCategoryService.get(et.getCategId());
                et.setOdooCateg(majorEntity);
                odooCateg=majorEntity;
            }
            et.setCategIdText(odooCateg.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__RES_COUNTRY__COUNTRY_ID]
        if(!ObjectUtils.isEmpty(et.getCountryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry=et.getOdooCountry();
            if(ObjectUtils.isEmpty(odooCountry)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country majorEntity=resCountryService.get(et.getCountryId());
                et.setOdooCountry(majorEntity);
                odooCountry=majorEntity;
            }
            et.setCountryIdText(odooCountry.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__RES_PARTNER__COMMERCIAL_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getCommercialPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooCommercialPartner=et.getOdooCommercialPartner();
            if(ObjectUtils.isEmpty(odooCommercialPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getCommercialPartnerId());
                et.setOdooCommercialPartner(majorEntity);
                odooCommercialPartner=majorEntity;
            }
            et.setCommercialPartnerIdText(odooCommercialPartner.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_REPORT__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}




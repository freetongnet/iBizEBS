package cn.ibizlab.businesscentral.core.odoo_purchase.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line;
/**
 * 关系型数据实体[Purchase_order_line] 查询条件对象
 */
@Slf4j
@Data
public class Purchase_order_lineSearchContext extends QueryWrapperContext<Purchase_order_line> {

	private String n_name_like;//[说明]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_partner_id_text_eq;//[业务伙伴]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[业务伙伴]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_orderpoint_id_text_eq;//[订货点]
	public void setN_orderpoint_id_text_eq(String n_orderpoint_id_text_eq) {
        this.n_orderpoint_id_text_eq = n_orderpoint_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_orderpoint_id_text_eq)){
            this.getSearchCond().eq("orderpoint_id_text", n_orderpoint_id_text_eq);
        }
    }
	private String n_orderpoint_id_text_like;//[订货点]
	public void setN_orderpoint_id_text_like(String n_orderpoint_id_text_like) {
        this.n_orderpoint_id_text_like = n_orderpoint_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_orderpoint_id_text_like)){
            this.getSearchCond().like("orderpoint_id_text", n_orderpoint_id_text_like);
        }
    }
	private String n_sale_order_id_text_eq;//[销售订单]
	public void setN_sale_order_id_text_eq(String n_sale_order_id_text_eq) {
        this.n_sale_order_id_text_eq = n_sale_order_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_id_text_eq)){
            this.getSearchCond().eq("sale_order_id_text", n_sale_order_id_text_eq);
        }
    }
	private String n_sale_order_id_text_like;//[销售订单]
	public void setN_sale_order_id_text_like(String n_sale_order_id_text_like) {
        this.n_sale_order_id_text_like = n_sale_order_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sale_order_id_text_like)){
            this.getSearchCond().like("sale_order_id_text", n_sale_order_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_account_analytic_id_text_eq;//[分析账户]
	public void setN_account_analytic_id_text_eq(String n_account_analytic_id_text_eq) {
        this.n_account_analytic_id_text_eq = n_account_analytic_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_text_eq)){
            this.getSearchCond().eq("account_analytic_id_text", n_account_analytic_id_text_eq);
        }
    }
	private String n_account_analytic_id_text_like;//[分析账户]
	public void setN_account_analytic_id_text_like(String n_account_analytic_id_text_like) {
        this.n_account_analytic_id_text_like = n_account_analytic_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_text_like)){
            this.getSearchCond().like("account_analytic_id_text", n_account_analytic_id_text_like);
        }
    }
	private String n_product_uom_text_eq;//[计量单位]
	public void setN_product_uom_text_eq(String n_product_uom_text_eq) {
        this.n_product_uom_text_eq = n_product_uom_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_eq)){
            this.getSearchCond().eq("product_uom_text", n_product_uom_text_eq);
        }
    }
	private String n_product_uom_text_like;//[计量单位]
	public void setN_product_uom_text_like(String n_product_uom_text_like) {
        this.n_product_uom_text_like = n_product_uom_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_like)){
            this.getSearchCond().like("product_uom_text", n_product_uom_text_like);
        }
    }
	private String n_order_id_text_eq;//[订单关联]
	public void setN_order_id_text_eq(String n_order_id_text_eq) {
        this.n_order_id_text_eq = n_order_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_order_id_text_eq)){
            this.getSearchCond().eq("order_id_text", n_order_id_text_eq);
        }
    }
	private String n_order_id_text_like;//[订单关联]
	public void setN_order_id_text_like(String n_order_id_text_like) {
        this.n_order_id_text_like = n_order_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_order_id_text_like)){
            this.getSearchCond().like("order_id_text", n_order_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_sale_line_id_text_eq;//[原销售项]
	public void setN_sale_line_id_text_eq(String n_sale_line_id_text_eq) {
        this.n_sale_line_id_text_eq = n_sale_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_line_id_text_eq)){
            this.getSearchCond().eq("sale_line_id_text", n_sale_line_id_text_eq);
        }
    }
	private String n_sale_line_id_text_like;//[原销售项]
	public void setN_sale_line_id_text_like(String n_sale_line_id_text_like) {
        this.n_sale_line_id_text_like = n_sale_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sale_line_id_text_like)){
            this.getSearchCond().like("sale_line_id_text", n_sale_line_id_text_like);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_account_analytic_id_eq;//[分析账户]
	public void setN_account_analytic_id_eq(Long n_account_analytic_id_eq) {
        this.n_account_analytic_id_eq = n_account_analytic_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_analytic_id_eq)){
            this.getSearchCond().eq("account_analytic_id", n_account_analytic_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_sale_order_id_eq;//[销售订单]
	public void setN_sale_order_id_eq(Long n_sale_order_id_eq) {
        this.n_sale_order_id_eq = n_sale_order_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_order_id_eq)){
            this.getSearchCond().eq("sale_order_id", n_sale_order_id_eq);
        }
    }
	private Long n_product_uom_eq;//[计量单位]
	public void setN_product_uom_eq(Long n_product_uom_eq) {
        this.n_product_uom_eq = n_product_uom_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_eq)){
            this.getSearchCond().eq("product_uom", n_product_uom_eq);
        }
    }
	private Long n_sale_line_id_eq;//[原销售项]
	public void setN_sale_line_id_eq(Long n_sale_line_id_eq) {
        this.n_sale_line_id_eq = n_sale_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_line_id_eq)){
            this.getSearchCond().eq("sale_line_id", n_sale_line_id_eq);
        }
    }
	private Long n_order_id_eq;//[订单关联]
	public void setN_order_id_eq(Long n_order_id_eq) {
        this.n_order_id_eq = n_order_id_eq;
        if(!ObjectUtils.isEmpty(this.n_order_id_eq)){
            this.getSearchCond().eq("order_id", n_order_id_eq);
        }
    }
	private Long n_orderpoint_id_eq;//[订货点]
	public void setN_orderpoint_id_eq(Long n_orderpoint_id_eq) {
        this.n_orderpoint_id_eq = n_orderpoint_id_eq;
        if(!ObjectUtils.isEmpty(this.n_orderpoint_id_eq)){
            this.getSearchCond().eq("orderpoint_id", n_orderpoint_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_partner_id_eq;//[业务伙伴]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




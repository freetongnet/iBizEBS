package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_workcenter_productivity_loss] 服务对象接口
 */
public interface IMrp_workcenter_productivity_lossService extends IService<Mrp_workcenter_productivity_loss>{

    boolean create(Mrp_workcenter_productivity_loss et) ;
    void createBatch(List<Mrp_workcenter_productivity_loss> list) ;
    boolean update(Mrp_workcenter_productivity_loss et) ;
    void updateBatch(List<Mrp_workcenter_productivity_loss> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_workcenter_productivity_loss get(Long key) ;
    Mrp_workcenter_productivity_loss getDraft(Mrp_workcenter_productivity_loss et) ;
    boolean checkKey(Mrp_workcenter_productivity_loss et) ;
    boolean save(Mrp_workcenter_productivity_loss et) ;
    void saveBatch(List<Mrp_workcenter_productivity_loss> list) ;
    Page<Mrp_workcenter_productivity_loss> searchDefault(Mrp_workcenter_productivity_lossSearchContext context) ;
    List<Mrp_workcenter_productivity_loss> selectByLossId(Long id);
    void resetByLossId(Long id);
    void resetByLossId(Collection<Long> ids);
    void removeByLossId(Long id);
    List<Mrp_workcenter_productivity_loss> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_workcenter_productivity_loss> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_workcenter_productivity_loss> getMrpWorkcenterProductivityLossByIds(List<Long> ids) ;
    List<Mrp_workcenter_productivity_loss> getMrpWorkcenterProductivityLossByEntities(List<Mrp_workcenter_productivity_loss> entities) ;
}



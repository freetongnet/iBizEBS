package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.odoo_base.service.impl.Res_config_settingsServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_settings;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[配置设定] 自定义服务对象
 */
@Slf4j
@Primary
@Service("Res_config_settingsExService")
public class Res_config_settingsExService extends Res_config_settingsServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[GetLatestSettings]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Res_config_settings getLatestSettings(Res_config_settings et) {
        return super.getLatestSettings(et);
    }
    /**
     * 自定义行为[Get_default]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Res_config_settings get_default(Res_config_settings et) {
        return super.get_default(et);
    }
}


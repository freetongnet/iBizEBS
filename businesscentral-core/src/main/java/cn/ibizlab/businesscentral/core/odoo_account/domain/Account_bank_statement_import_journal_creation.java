package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[在银行对账单导入创建日记账]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_BANK_STATEMENT_IMPORT_JOURNAL_CREATION",resultMap = "Account_bank_statement_import_journal_creationResultMap")
public class Account_bank_statement_import_journal_creation extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 为付款
     */
    @TableField(exist = false)
    @JSONField(name = "outbound_payment_method_ids")
    @JsonProperty("outbound_payment_method_ids")
    private String outboundPaymentMethodIds;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 允许的科目类型
     */
    @TableField(exist = false)
    @JSONField(name = "type_control_ids")
    @JsonProperty("type_control_ids")
    private String typeControlIds;
    /**
     * 为收款
     */
    @TableField(exist = false)
    @JSONField(name = "inbound_payment_method_ids")
    @JsonProperty("inbound_payment_method_ids")
    private String inboundPaymentMethodIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 允许的科目
     */
    @TableField(exist = false)
    @JSONField(name = "account_control_ids")
    @JsonProperty("account_control_ids")
    private String accountControlIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 别名域
     */
    @TableField(exist = false)
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;
    /**
     * 信用票分录序列
     */
    @TableField(exist = false)
    @JSONField(name = "refund_sequence_id")
    @JsonProperty("refund_sequence_id")
    private Integer refundSequenceId;
    /**
     * 序号
     */
    @TableField(exist = false)
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 银行
     */
    @TableField(exist = false)
    @JSONField(name = "bank_id")
    @JsonProperty("bank_id")
    private Long bankId;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 损失科目
     */
    @TableField(exist = false)
    @JSONField(name = "loss_account_id")
    @JsonProperty("loss_account_id")
    private Long lossAccountId;
    /**
     * 默认贷方科目
     */
    @TableField(exist = false)
    @JSONField(name = "default_credit_account_id")
    @JsonProperty("default_credit_account_id")
    private Long defaultCreditAccountId;
    /**
     * 专用的信用票序列
     */
    @TableField(exist = false)
    @JSONField(name = "refund_sequence")
    @JsonProperty("refund_sequence")
    private Boolean refundSequence;
    /**
     * 至少一个转出
     */
    @TableField(exist = false)
    @JSONField(name = "at_least_one_outbound")
    @JsonProperty("at_least_one_outbound")
    private Boolean atLeastOneOutbound;
    /**
     * 利润科目
     */
    @TableField(exist = false)
    @JSONField(name = "profit_account_id")
    @JsonProperty("profit_account_id")
    private Long profitAccountId;
    /**
     * 信用票：下一号码
     */
    @TableField(exist = false)
    @JSONField(name = "refund_sequence_number_next")
    @JsonProperty("refund_sequence_number_next")
    private Integer refundSequenceNumberNext;
    /**
     * 别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Long aliasId;
    /**
     * 简码
     */
    @TableField(exist = false)
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;
    /**
     * 供应商账单的别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;
    /**
     * 银行账户
     */
    @TableField(exist = false)
    @JSONField(name = "bank_account_id")
    @JsonProperty("bank_account_id")
    private Long bankAccountId;
    /**
     * 颜色索引
     */
    @TableField(exist = false)
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 至少一个转入
     */
    @TableField(exist = false)
    @JSONField(name = "at_least_one_inbound")
    @JsonProperty("at_least_one_inbound")
    private Boolean atLeastOneInbound;
    /**
     * 账户号码
     */
    @TableField(exist = false)
    @JSONField(name = "bank_acc_number")
    @JsonProperty("bank_acc_number")
    private String bankAccNumber;
    /**
     * 下一号码
     */
    @TableField(exist = false)
    @JSONField(name = "sequence_number_next")
    @JsonProperty("sequence_number_next")
    private Integer sequenceNumberNext;
    /**
     * 账户持有人
     */
    @TableField(exist = false)
    @JSONField(name = "company_partner_id")
    @JsonProperty("company_partner_id")
    private Long companyPartnerId;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 默认借方科目
     */
    @TableField(exist = false)
    @JSONField(name = "default_debit_account_id")
    @JsonProperty("default_debit_account_id")
    private Long defaultDebitAccountId;
    /**
     * 有效
     */
    @TableField(exist = false)
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 银行费用
     */
    @TableField(exist = false)
    @JSONField(name = "bank_statements_source")
    @JsonProperty("bank_statements_source")
    private String bankStatementsSource;
    /**
     * 看板仪表板
     */
    @TableField(exist = false)
    @JSONField(name = "kanban_dashboard")
    @JsonProperty("kanban_dashboard")
    private String kanbanDashboard;
    /**
     * 看板仪表板图表
     */
    @TableField(exist = false)
    @JSONField(name = "kanban_dashboard_graph")
    @JsonProperty("kanban_dashboard_graph")
    private String kanbanDashboardGraph;
    /**
     * 在仪表板显示日记账
     */
    @TableField(exist = false)
    @JSONField(name = "show_on_dashboard")
    @JsonProperty("show_on_dashboard")
    private Boolean showOnDashboard;
    /**
     * 属于用户的当前公司
     */
    @TableField(exist = false)
    @JSONField(name = "belongs_to_company")
    @JsonProperty("belongs_to_company")
    private Boolean belongsToCompany;
    /**
     * 日记账名称
     */
    @TableField(exist = false)
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 分录序列
     */
    @TableField(exist = false)
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    private Integer sequenceId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @TableField(value = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Long journalId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [日记账]
     */
    public void setJournalId(Long journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



package cn.ibizlab.businesscentral.core.odoo_uom.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_category;
import cn.ibizlab.businesscentral.core.odoo_uom.filter.Uom_categorySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Uom_category] 服务对象接口
 */
public interface IUom_categoryService extends IService<Uom_category>{

    boolean create(Uom_category et) ;
    void createBatch(List<Uom_category> list) ;
    boolean update(Uom_category et) ;
    void updateBatch(List<Uom_category> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Uom_category get(Long key) ;
    Uom_category getDraft(Uom_category et) ;
    boolean checkKey(Uom_category et) ;
    boolean save(Uom_category et) ;
    void saveBatch(List<Uom_category> list) ;
    Page<Uom_category> searchDefault(Uom_categorySearchContext context) ;
    List<Uom_category> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Uom_category> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Uom_category> getUomCategoryByIds(List<Long> ids) ;
    List<Uom_category> getUomCategoryByEntities(List<Uom_category> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_templateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_template] 服务对象接口
 */
public interface IProduct_templateService extends IService<Product_template>{

    boolean create(Product_template et) ;
    void createBatch(List<Product_template> list) ;
    boolean update(Product_template et) ;
    void updateBatch(List<Product_template> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_template get(Long key) ;
    Product_template getDraft(Product_template et) ;
    boolean checkKey(Product_template et) ;
    boolean save(Product_template et) ;
    void saveBatch(List<Product_template> list) ;
    Page<Product_template> searchDefault(Product_templateSearchContext context) ;
    Page<Product_template> searchMaster(Product_templateSearchContext context) ;
    List<Product_template> selectByCategId(Long id);
    void resetByCategId(Long id);
    void resetByCategId(Collection<Long> ids);
    void removeByCategId(Long id);
    List<Product_template> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Product_template> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_template> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Product_template> selectByUomId(Long id);
    void resetByUomId(Long id);
    void resetByUomId(Collection<Long> ids);
    void removeByUomId(Long id);
    List<Product_template> selectByUomPoId(Long id);
    void resetByUomPoId(Long id);
    void resetByUomPoId(Collection<Long> ids);
    void removeByUomPoId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_template> getProductTemplateByIds(List<Long> ids) ;
    List<Product_template> getProductTemplateByEntities(List<Product_template> entities) ;
}



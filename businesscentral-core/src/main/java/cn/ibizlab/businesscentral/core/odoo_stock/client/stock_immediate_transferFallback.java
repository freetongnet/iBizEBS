package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_immediate_transfer;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_immediate_transferSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_immediate_transfer] 服务对象接口
 */
@Component
public class stock_immediate_transferFallback implements stock_immediate_transferFeignClient{


    public Stock_immediate_transfer create(Stock_immediate_transfer stock_immediate_transfer){
            return null;
     }
    public Boolean createBatch(List<Stock_immediate_transfer> stock_immediate_transfers){
            return false;
     }

    public Stock_immediate_transfer update(Long id, Stock_immediate_transfer stock_immediate_transfer){
            return null;
     }
    public Boolean updateBatch(List<Stock_immediate_transfer> stock_immediate_transfers){
            return false;
     }


    public Stock_immediate_transfer get(Long id){
            return null;
     }



    public Page<Stock_immediate_transfer> search(Stock_immediate_transferSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Stock_immediate_transfer> select(){
            return null;
     }

    public Stock_immediate_transfer getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_immediate_transfer stock_immediate_transfer){
            return false;
     }


    public Boolean save(Stock_immediate_transfer stock_immediate_transfer){
            return false;
     }
    public Boolean saveBatch(List<Stock_immediate_transfer> stock_immediate_transfers){
            return false;
     }

    public Page<Stock_immediate_transfer> searchDefault(Stock_immediate_transferSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_challengeSearchContext;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challengeService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_gamification.mapper.Gamification_challengeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[游戏化挑战] 服务对象接口实现
 */
@Slf4j
@Service("Gamification_challengeServiceImpl")
public class Gamification_challengeServiceImpl extends EBSServiceImpl<Gamification_challengeMapper, Gamification_challenge> implements IGamification_challengeService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badge_userService gamificationBadgeUserService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challenge_lineService gamificationChallengeLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goalService gamificationGoalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_badgeService gamificationBadgeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channelService mailChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_templateService mailTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "gamification.challenge" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Gamification_challenge et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_challengeService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Gamification_challenge> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Gamification_challenge et) {
        Gamification_challenge old = new Gamification_challenge() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_challengeService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_challengeService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Gamification_challenge> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        gamificationBadgeUserService.resetByChallengeId(key);
        gamificationChallengeLineService.removeByChallengeId(key);
        gamificationGoalService.resetByChallengeId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        gamificationBadgeUserService.resetByChallengeId(idList);
        gamificationChallengeLineService.removeByChallengeId(idList);
        gamificationGoalService.resetByChallengeId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Gamification_challenge get(Long key) {
        Gamification_challenge et = getById(key);
        if(et==null){
            et=new Gamification_challenge();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Gamification_challenge getDraft(Gamification_challenge et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Gamification_challenge et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Gamification_challenge et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Gamification_challenge et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Gamification_challenge> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Gamification_challenge> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Gamification_challenge> selectByRewardFirstId(Long id) {
        return baseMapper.selectByRewardFirstId(id);
    }
    @Override
    public void resetByRewardFirstId(Long id) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("reward_first_id",null).eq("reward_first_id",id));
    }

    @Override
    public void resetByRewardFirstId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("reward_first_id",null).in("reward_first_id",ids));
    }

    @Override
    public void removeByRewardFirstId(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge>().eq("reward_first_id",id));
    }

	@Override
    public List<Gamification_challenge> selectByRewardId(Long id) {
        return baseMapper.selectByRewardId(id);
    }
    @Override
    public void resetByRewardId(Long id) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("reward_id",null).eq("reward_id",id));
    }

    @Override
    public void resetByRewardId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("reward_id",null).in("reward_id",ids));
    }

    @Override
    public void removeByRewardId(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge>().eq("reward_id",id));
    }

	@Override
    public List<Gamification_challenge> selectByRewardSecondId(Long id) {
        return baseMapper.selectByRewardSecondId(id);
    }
    @Override
    public void resetByRewardSecondId(Long id) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("reward_second_id",null).eq("reward_second_id",id));
    }

    @Override
    public void resetByRewardSecondId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("reward_second_id",null).in("reward_second_id",ids));
    }

    @Override
    public void removeByRewardSecondId(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge>().eq("reward_second_id",id));
    }

	@Override
    public List<Gamification_challenge> selectByRewardThirdId(Long id) {
        return baseMapper.selectByRewardThirdId(id);
    }
    @Override
    public void resetByRewardThirdId(Long id) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("reward_third_id",null).eq("reward_third_id",id));
    }

    @Override
    public void resetByRewardThirdId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("reward_third_id",null).in("reward_third_id",ids));
    }

    @Override
    public void removeByRewardThirdId(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge>().eq("reward_third_id",id));
    }

	@Override
    public List<Gamification_challenge> selectByReportMessageGroupId(Long id) {
        return baseMapper.selectByReportMessageGroupId(id);
    }
    @Override
    public void resetByReportMessageGroupId(Long id) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("report_message_group_id",null).eq("report_message_group_id",id));
    }

    @Override
    public void resetByReportMessageGroupId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("report_message_group_id",null).in("report_message_group_id",ids));
    }

    @Override
    public void removeByReportMessageGroupId(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge>().eq("report_message_group_id",id));
    }

	@Override
    public List<Gamification_challenge> selectByReportTemplateId(Long id) {
        return baseMapper.selectByReportTemplateId(id);
    }
    @Override
    public void resetByReportTemplateId(Long id) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("report_template_id",null).eq("report_template_id",id));
    }

    @Override
    public void resetByReportTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("report_template_id",null).in("report_template_id",ids));
    }

    @Override
    public void removeByReportTemplateId(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge>().eq("report_template_id",id));
    }

	@Override
    public List<Gamification_challenge> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge>().eq("create_uid",id));
    }

	@Override
    public List<Gamification_challenge> selectByManagerId(Long id) {
        return baseMapper.selectByManagerId(id);
    }
    @Override
    public void resetByManagerId(Long id) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("manager_id",null).eq("manager_id",id));
    }

    @Override
    public void resetByManagerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Gamification_challenge>().set("manager_id",null).in("manager_id",ids));
    }

    @Override
    public void removeByManagerId(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge>().eq("manager_id",id));
    }

	@Override
    public List<Gamification_challenge> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Gamification_challenge> searchDefault(Gamification_challengeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Gamification_challenge> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Gamification_challenge>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Gamification_challenge et){
        //实体关系[DER1N_GAMIFICATION_CHALLENGE__GAMIFICATION_BADGE__REWARD_FIRST_ID]
        if(!ObjectUtils.isEmpty(et.getRewardFirstId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge odooRewardFirst=et.getOdooRewardFirst();
            if(ObjectUtils.isEmpty(odooRewardFirst)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge majorEntity=gamificationBadgeService.get(et.getRewardFirstId());
                et.setOdooRewardFirst(majorEntity);
                odooRewardFirst=majorEntity;
            }
            et.setRewardFirstIdText(odooRewardFirst.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE__GAMIFICATION_BADGE__REWARD_ID]
        if(!ObjectUtils.isEmpty(et.getRewardId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge odooReward=et.getOdooReward();
            if(ObjectUtils.isEmpty(odooReward)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge majorEntity=gamificationBadgeService.get(et.getRewardId());
                et.setOdooReward(majorEntity);
                odooReward=majorEntity;
            }
            et.setRewardIdText(odooReward.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE__GAMIFICATION_BADGE__REWARD_SECOND_ID]
        if(!ObjectUtils.isEmpty(et.getRewardSecondId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge odooRewardSecond=et.getOdooRewardSecond();
            if(ObjectUtils.isEmpty(odooRewardSecond)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge majorEntity=gamificationBadgeService.get(et.getRewardSecondId());
                et.setOdooRewardSecond(majorEntity);
                odooRewardSecond=majorEntity;
            }
            et.setRewardSecondIdText(odooRewardSecond.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE__GAMIFICATION_BADGE__REWARD_THIRD_ID]
        if(!ObjectUtils.isEmpty(et.getRewardThirdId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge odooRewardThird=et.getOdooRewardThird();
            if(ObjectUtils.isEmpty(odooRewardThird)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge majorEntity=gamificationBadgeService.get(et.getRewardThirdId());
                et.setOdooRewardThird(majorEntity);
                odooRewardThird=majorEntity;
            }
            et.setRewardThirdIdText(odooRewardThird.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE__MAIL_CHANNEL__REPORT_MESSAGE_GROUP_ID]
        if(!ObjectUtils.isEmpty(et.getReportMessageGroupId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel odooReportMessageGroup=et.getOdooReportMessageGroup();
            if(ObjectUtils.isEmpty(odooReportMessageGroup)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel majorEntity=mailChannelService.get(et.getReportMessageGroupId());
                et.setOdooReportMessageGroup(majorEntity);
                odooReportMessageGroup=majorEntity;
            }
            et.setReportMessageGroupIdText(odooReportMessageGroup.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE__MAIL_TEMPLATE__REPORT_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getReportTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooReportTemplate=et.getOdooReportTemplate();
            if(ObjectUtils.isEmpty(odooReportTemplate)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template majorEntity=mailTemplateService.get(et.getReportTemplateId());
                et.setOdooReportTemplate(majorEntity);
                odooReportTemplate=majorEntity;
            }
            et.setReportTemplateIdText(odooReportTemplate.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE__RES_USERS__MANAGER_ID]
        if(!ObjectUtils.isEmpty(et.getManagerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooManager=et.getOdooManager();
            if(ObjectUtils.isEmpty(odooManager)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getManagerId());
                et.setOdooManager(majorEntity);
                odooManager=majorEntity;
            }
            et.setManagerIdText(odooManager.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Gamification_challenge> getGamificationChallengeByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Gamification_challenge> getGamificationChallengeByEntities(List<Gamification_challenge> entities) {
        List ids =new ArrayList();
        for(Gamification_challenge entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory_line;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_inventory_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_inventory_line] 服务对象接口
 */
public interface IStock_inventory_lineService extends IService<Stock_inventory_line>{

    boolean create(Stock_inventory_line et) ;
    void createBatch(List<Stock_inventory_line> list) ;
    boolean update(Stock_inventory_line et) ;
    void updateBatch(List<Stock_inventory_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_inventory_line get(Long key) ;
    Stock_inventory_line getDraft(Stock_inventory_line et) ;
    boolean checkKey(Stock_inventory_line et) ;
    boolean save(Stock_inventory_line et) ;
    void saveBatch(List<Stock_inventory_line> list) ;
    Page<Stock_inventory_line> searchDefault(Stock_inventory_lineSearchContext context) ;
    List<Stock_inventory_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_inventory_line> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_inventory_line> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Stock_inventory_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_inventory_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_inventory_line> selectByInventoryId(Long id);
    void removeByInventoryId(Collection<Long> ids);
    void removeByInventoryId(Long id);
    List<Stock_inventory_line> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_inventory_line> selectByProdLotId(Long id);
    void resetByProdLotId(Long id);
    void resetByProdLotId(Collection<Long> ids);
    void removeByProdLotId(Long id);
    List<Stock_inventory_line> selectByPackageId(Long id);
    void resetByPackageId(Long id);
    void resetByPackageId(Collection<Long> ids);
    void removeByPackageId(Long id);
    List<Stock_inventory_line> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_inventory_line> getStockInventoryLineByIds(List<Long> ids) ;
    List<Stock_inventory_line> getStockInventoryLineByEntities(List<Stock_inventory_line> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_repair.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_fee;
/**
 * 关系型数据实体[Repair_fee] 查询条件对象
 */
@Slf4j
@Data
public class Repair_feeSearchContext extends QueryWrapperContext<Repair_fee> {

	private String n_name_like;//[描述]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_product_uom_text_eq;//[产品量度单位]
	public void setN_product_uom_text_eq(String n_product_uom_text_eq) {
        this.n_product_uom_text_eq = n_product_uom_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_eq)){
            this.getSearchCond().eq("product_uom_text", n_product_uom_text_eq);
        }
    }
	private String n_product_uom_text_like;//[产品量度单位]
	public void setN_product_uom_text_like(String n_product_uom_text_like) {
        this.n_product_uom_text_like = n_product_uom_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_like)){
            this.getSearchCond().like("product_uom_text", n_product_uom_text_like);
        }
    }
	private String n_invoice_line_id_text_eq;//[发票明细]
	public void setN_invoice_line_id_text_eq(String n_invoice_line_id_text_eq) {
        this.n_invoice_line_id_text_eq = n_invoice_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_line_id_text_eq)){
            this.getSearchCond().eq("invoice_line_id_text", n_invoice_line_id_text_eq);
        }
    }
	private String n_invoice_line_id_text_like;//[发票明细]
	public void setN_invoice_line_id_text_like(String n_invoice_line_id_text_like) {
        this.n_invoice_line_id_text_like = n_invoice_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_invoice_line_id_text_like)){
            this.getSearchCond().like("invoice_line_id_text", n_invoice_line_id_text_like);
        }
    }
	private String n_repair_id_text_eq;//[维修单编＃]
	public void setN_repair_id_text_eq(String n_repair_id_text_eq) {
        this.n_repair_id_text_eq = n_repair_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_repair_id_text_eq)){
            this.getSearchCond().eq("repair_id_text", n_repair_id_text_eq);
        }
    }
	private String n_repair_id_text_like;//[维修单编＃]
	public void setN_repair_id_text_like(String n_repair_id_text_like) {
        this.n_repair_id_text_like = n_repair_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_repair_id_text_like)){
            this.getSearchCond().like("repair_id_text", n_repair_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建者]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建者]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private Long n_invoice_line_id_eq;//[发票明细]
	public void setN_invoice_line_id_eq(Long n_invoice_line_id_eq) {
        this.n_invoice_line_id_eq = n_invoice_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_line_id_eq)){
            this.getSearchCond().eq("invoice_line_id", n_invoice_line_id_eq);
        }
    }
	private Long n_product_uom_eq;//[产品量度单位]
	public void setN_product_uom_eq(Long n_product_uom_eq) {
        this.n_product_uom_eq = n_product_uom_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_eq)){
            this.getSearchCond().eq("product_uom", n_product_uom_eq);
        }
    }
	private Long n_repair_id_eq;//[维修单编＃]
	public void setN_repair_id_eq(Long n_repair_id_eq) {
        this.n_repair_id_eq = n_repair_id_eq;
        if(!ObjectUtils.isEmpty(this.n_repair_id_eq)){
            this.getSearchCond().eq("repair_id", n_repair_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建者]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




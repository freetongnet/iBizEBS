package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicleSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-fleet:odoo-fleet}", contextId = "fleet-vehicle", fallback = fleet_vehicleFallback.class)
public interface fleet_vehicleFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicles/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicles/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicles/{id}")
    Fleet_vehicle update(@PathVariable("id") Long id,@RequestBody Fleet_vehicle fleet_vehicle);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicles/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle> fleet_vehicles);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicles/{id}")
    Fleet_vehicle get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles")
    Fleet_vehicle create(@RequestBody Fleet_vehicle fleet_vehicle);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle> fleet_vehicles);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/search")
    Page<Fleet_vehicle> search(@RequestBody Fleet_vehicleSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicles/select")
    Page<Fleet_vehicle> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicles/getdraft")
    Fleet_vehicle getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/checkkey")
    Boolean checkKey(@RequestBody Fleet_vehicle fleet_vehicle);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/save")
    Boolean save(@RequestBody Fleet_vehicle fleet_vehicle);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/savebatch")
    Boolean saveBatch(@RequestBody List<Fleet_vehicle> fleet_vehicles);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicles/searchdefault")
    Page<Fleet_vehicle> searchDefault(@RequestBody Fleet_vehicleSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_portal.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_share;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_shareSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Portal_share] 服务对象接口
 */
public interface IPortal_shareService extends IService<Portal_share>{

    boolean create(Portal_share et) ;
    void createBatch(List<Portal_share> list) ;
    boolean update(Portal_share et) ;
    void updateBatch(List<Portal_share> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Portal_share get(Long key) ;
    Portal_share getDraft(Portal_share et) ;
    boolean checkKey(Portal_share et) ;
    boolean save(Portal_share et) ;
    void saveBatch(List<Portal_share> list) ;
    Page<Portal_share> searchDefault(Portal_shareSearchContext context) ;
    List<Portal_share> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Portal_share> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Portal_share> getPortalShareByIds(List<Long> ids) ;
    List<Portal_share> getPortalShareByEntities(List<Portal_share> entities) ;
}



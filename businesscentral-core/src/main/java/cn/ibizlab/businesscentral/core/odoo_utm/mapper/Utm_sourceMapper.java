package cn.ibizlab.businesscentral.core.odoo_utm.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.businesscentral.core.odoo_utm.filter.Utm_sourceSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Utm_sourceMapper extends BaseMapper<Utm_source>{

    Page<Utm_source> searchDefault(IPage page, @Param("srf") Utm_sourceSearchContext context, @Param("ew") Wrapper<Utm_source> wrapper) ;
    @Override
    Utm_source selectById(Serializable id);
    @Override
    int insert(Utm_source entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Utm_source entity);
    @Override
    int update(@Param(Constants.ENTITY) Utm_source entity, @Param("ew") Wrapper<Utm_source> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Utm_source> selectByCreateUid(@Param("id") Serializable id) ;

    List<Utm_source> selectByWriteUid(@Param("id") Serializable id) ;


}

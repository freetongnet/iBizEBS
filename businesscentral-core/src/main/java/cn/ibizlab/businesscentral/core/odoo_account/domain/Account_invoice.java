package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[发票]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_INVOICE",resultMap = "Account_invoiceResultMap")
public class Account_invoice extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 未到期贷项
     */
    @TableField(exist = false)
    @JSONField(name = "outstanding_credits_debits_widget")
    @JsonProperty("outstanding_credits_debits_widget")
    private String outstandingCreditsDebitsWidget;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 日记账分录名称
     */
    @DEField(name = "move_name")
    @TableField(value = "move_name")
    @JSONField(name = "move_name")
    @JsonProperty("move_name")
    private String moveName;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 门户访问网址
     */
    @TableField(exist = false)
    @JSONField(name = "access_url")
    @JsonProperty("access_url")
    private String accessUrl;
    /**
     * 付款凭证明细
     */
    @TableField(exist = false)
    @JSONField(name = "payment_move_line_ids")
    @JsonProperty("payment_move_line_ids")
    private String paymentMoveLineIds;
    /**
     * 源文档
     */
    @TableField(value = "origin")
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;
    /**
     * 会计日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 公司货币的合计
     */
    @DEField(name = "amount_total_company_signed")
    @TableField(value = "amount_total_company_signed")
    @JSONField(name = "amount_total_company_signed")
    @JsonProperty("amount_total_company_signed")
    private BigDecimal amountTotalCompanySigned;
    /**
     * 操作编号
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 按公司本位币计的不含税金额
     */
    @DEField(name = "amount_untaxed_signed")
    @TableField(value = "amount_untaxed_signed")
    @JSONField(name = "amount_untaxed_signed")
    @JsonProperty("amount_untaxed_signed")
    private BigDecimal amountUntaxedSigned;
    /**
     * 到期金额
     */
    @TableField(value = "residual")
    @JSONField(name = "residual")
    @JsonProperty("residual")
    private BigDecimal residual;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 有未清项
     */
    @TableField(exist = false)
    @JSONField(name = "has_outstanding")
    @JsonProperty("has_outstanding")
    private Boolean hasOutstanding;
    /**
     * 额外的信息
     */
    @TableField(value = "comment")
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 按组分配税额
     */
    @TableField(exist = false)
    @JSONField(name = "amount_by_group")
    @JsonProperty("amount_by_group")
    private byte[] amountByGroup;
    /**
     * 支付挂件
     */
    @TableField(exist = false)
    @JSONField(name = "payments_widget")
    @JsonProperty("payments_widget")
    private String paymentsWidget;
    /**
     * 已付／已核销
     */
    @TableField(value = "reconciled")
    @JSONField(name = "reconciled")
    @JsonProperty("reconciled")
    private Boolean reconciled;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 已汇
     */
    @TableField(value = "sent")
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private Boolean sent;
    /**
     * 以发票币种总计
     */
    @DEField(name = "amount_total_signed")
    @TableField(value = "amount_total_signed")
    @JSONField(name = "amount_total_signed")
    @JsonProperty("amount_total_signed")
    private BigDecimal amountTotalSigned;
    /**
     * 发票行
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_line_ids")
    @JsonProperty("invoice_line_ids")
    private String invoiceLineIds;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 访问警告
     */
    @TableField(exist = false)
    @JSONField(name = "access_warning")
    @JsonProperty("access_warning")
    private String accessWarning;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 发票使用币种的逾期金额
     */
    @DEField(name = "residual_signed")
    @TableField(value = "residual_signed")
    @JSONField(name = "residual_signed")
    @JsonProperty("residual_signed")
    private BigDecimal residualSigned;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 安全令牌
     */
    @DEField(name = "access_token")
    @TableField(value = "access_token")
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;
    /**
     * 供应商名称
     */
    @DEField(name = "vendor_display_name")
    @TableField(value = "vendor_display_name")
    @JSONField(name = "vendor_display_name")
    @JsonProperty("vendor_display_name")
    private String vendorDisplayName;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 未税金额
     */
    @DEField(name = "amount_untaxed")
    @TableField(value = "amount_untaxed")
    @JSONField(name = "amount_untaxed")
    @JsonProperty("amount_untaxed")
    private BigDecimal amountUntaxed;
    /**
     * 交易
     */
    @TableField(exist = false)
    @JSONField(name = "transaction_ids")
    @JsonProperty("transaction_ids")
    private String transactionIds;
    /**
     * 到期日期
     */
    @DEField(name = "date_due")
    @TableField(value = "date_due")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_due" , format="yyyy-MM-dd")
    @JsonProperty("date_due")
    private Timestamp dateDue;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 已授权的交易
     */
    @TableField(exist = false)
    @JSONField(name = "authorized_transaction_ids")
    @JsonProperty("authorized_transaction_ids")
    private String authorizedTransactionIds;
    /**
     * 税率明细行
     */
    @TableField(exist = false)
    @JSONField(name = "tax_line_ids")
    @JsonProperty("tax_line_ids")
    private String taxLineIds;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 源邮箱
     */
    @DEField(name = "source_email")
    @TableField(value = "source_email")
    @JSONField(name = "source_email")
    @JsonProperty("source_email")
    private String sourceEmail;
    /**
     * 税率
     */
    @DEField(name = "amount_tax")
    @TableField(value = "amount_tax")
    @JSONField(name = "amount_tax")
    @JsonProperty("amount_tax")
    private BigDecimal amountTax;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * Untaxed Amount in Invoice Currency
     */
    @TableField(exist = false)
    @JSONField(name = "amount_untaxed_invoice_signed")
    @JsonProperty("amount_untaxed_invoice_signed")
    private BigDecimal amountUntaxedInvoiceSigned;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 下一个号码前缀
     */
    @TableField(exist = false)
    @JSONField(name = "sequence_number_next_prefix")
    @JsonProperty("sequence_number_next_prefix")
    private String sequenceNumberNextPrefix;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 退款发票
     */
    @TableField(exist = false)
    @JSONField(name = "refund_invoice_ids")
    @JsonProperty("refund_invoice_ids")
    private String refundInvoiceIds;
    /**
     * 公司使用币种的逾期金额
     */
    @DEField(name = "residual_company_signed")
    @TableField(value = "residual_company_signed")
    @JSONField(name = "residual_company_signed")
    @JsonProperty("residual_company_signed")
    private BigDecimal residualCompanySigned;
    /**
     * 开票日期
     */
    @DEField(name = "date_invoice")
    @TableField(value = "date_invoice")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_invoice" , format="yyyy-MM-dd")
    @JsonProperty("date_invoice")
    private Timestamp dateInvoice;
    /**
     * 参考/说明
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 付款参考:
     */
    @TableField(value = "reference")
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;
    /**
     * 网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 下一号码
     */
    @TableField(exist = false)
    @JSONField(name = "sequence_number_next")
    @JsonProperty("sequence_number_next")
    private String sequenceNumberNext;
    /**
     * 网站信息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * Tax in Invoice Currency
     */
    @TableField(exist = false)
    @JSONField(name = "amount_tax_signed")
    @JsonProperty("amount_tax_signed")
    private BigDecimal amountTaxSigned;
    /**
     * 发票标示
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_icon")
    @JsonProperty("invoice_icon")
    private String invoiceIcon;
    /**
     * 付款
     */
    @TableField(exist = false)
    @JSONField(name = "payment_ids")
    @JsonProperty("payment_ids")
    private String paymentIds;
    /**
     * 需要一个动作消息的编码
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 总计
     */
    @DEField(name = "amount_total")
    @TableField(value = "amount_total")
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private BigDecimal amountTotal;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 日记账
     */
    @TableField(exist = false)
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;
    /**
     * 添加采购订单
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_id_text")
    @JsonProperty("purchase_id_text")
    private String purchaseIdText;
    /**
     * 自动完成
     */
    @TableField(exist = false)
    @JSONField(name = "vendor_bill_purchase_id_text")
    @JsonProperty("vendor_bill_purchase_id_text")
    private String vendorBillPurchaseIdText;
    /**
     * 来源
     */
    @TableField(exist = false)
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;
    /**
     * 营销
     */
    @TableField(exist = false)
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;
    /**
     * 送货地址
     */
    @TableField(exist = false)
    @JSONField(name = "partner_shipping_id_text")
    @JsonProperty("partner_shipping_id_text")
    private String partnerShippingIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 贸易条款
     */
    @TableField(exist = false)
    @JSONField(name = "incoterms_id_text")
    @JsonProperty("incoterms_id_text")
    private String incotermsIdText;
    /**
     * 公司货币
     */
    @TableField(exist = false)
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Long companyCurrencyId;
    /**
     * 科目
     */
    @TableField(exist = false)
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;
    /**
     * 国际贸易术语
     */
    @TableField(exist = false)
    @JSONField(name = "incoterm_id_text")
    @JsonProperty("incoterm_id_text")
    private String incotermIdText;
    /**
     * 业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 媒体
     */
    @TableField(exist = false)
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 销售员
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 现金舍入方式
     */
    @TableField(exist = false)
    @JSONField(name = "cash_rounding_id_text")
    @JsonProperty("cash_rounding_id_text")
    private String cashRoundingIdText;
    /**
     * 供应商账单
     */
    @TableField(exist = false)
    @JSONField(name = "vendor_bill_id_text")
    @JsonProperty("vendor_bill_id_text")
    private String vendorBillIdText;
    /**
     * 此发票为信用票的发票
     */
    @TableField(exist = false)
    @JSONField(name = "refund_invoice_id_text")
    @JsonProperty("refund_invoice_id_text")
    private String refundInvoiceIdText;
    /**
     * 税科目调整
     */
    @TableField(exist = false)
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    private String fiscalPositionIdText;
    /**
     * 号码
     */
    @TableField(exist = false)
    @JSONField(name = "number")
    @JsonProperty("number")
    private String number;
    /**
     * 商业实体
     */
    @TableField(exist = false)
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    private String commercialPartnerIdText;
    /**
     * 销售团队
     */
    @TableField(exist = false)
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;
    /**
     * 付款条款
     */
    @TableField(exist = false)
    @JSONField(name = "payment_term_id_text")
    @JsonProperty("payment_term_id_text")
    private String paymentTermIdText;
    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @TableField(value = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Long teamId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 科目
     */
    @DEField(name = "account_id")
    @TableField(value = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Long accountId;
    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @TableField(value = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Long mediumId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 日记账分录
     */
    @DEField(name = "move_id")
    @TableField(value = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Long moveId;
    /**
     * 付款条款
     */
    @DEField(name = "payment_term_id")
    @TableField(value = "payment_term_id")
    @JSONField(name = "payment_term_id")
    @JsonProperty("payment_term_id")
    private Long paymentTermId;
    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @TableField(value = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Long journalId;
    /**
     * 国际贸易术语
     */
    @DEField(name = "incoterm_id")
    @TableField(value = "incoterm_id")
    @JSONField(name = "incoterm_id")
    @JsonProperty("incoterm_id")
    private Long incotermId;
    /**
     * 添加采购订单
     */
    @DEField(name = "purchase_id")
    @TableField(value = "purchase_id")
    @JSONField(name = "purchase_id")
    @JsonProperty("purchase_id")
    private Long purchaseId;
    /**
     * 税科目调整
     */
    @DEField(name = "fiscal_position_id")
    @TableField(value = "fiscal_position_id")
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    private Long fiscalPositionId;
    /**
     * 此发票为信用票的发票
     */
    @DEField(name = "refund_invoice_id")
    @TableField(value = "refund_invoice_id")
    @JSONField(name = "refund_invoice_id")
    @JsonProperty("refund_invoice_id")
    private Long refundInvoiceId;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 银行账户
     */
    @DEField(name = "partner_bank_id")
    @TableField(value = "partner_bank_id")
    @JSONField(name = "partner_bank_id")
    @JsonProperty("partner_bank_id")
    private Long partnerBankId;
    /**
     * 现金舍入方式
     */
    @DEField(name = "cash_rounding_id")
    @TableField(value = "cash_rounding_id")
    @JSONField(name = "cash_rounding_id")
    @JsonProperty("cash_rounding_id")
    private Long cashRoundingId;
    /**
     * 商业实体
     */
    @DEField(name = "commercial_partner_id")
    @TableField(value = "commercial_partner_id")
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Long commercialPartnerId;
    /**
     * 送货地址
     */
    @DEField(name = "partner_shipping_id")
    @TableField(value = "partner_shipping_id")
    @JSONField(name = "partner_shipping_id")
    @JsonProperty("partner_shipping_id")
    private Long partnerShippingId;
    /**
     * 贸易条款
     */
    @DEField(name = "incoterms_id")
    @TableField(value = "incoterms_id")
    @JSONField(name = "incoterms_id")
    @JsonProperty("incoterms_id")
    private Long incotermsId;
    /**
     * 来源
     */
    @DEField(name = "source_id")
    @TableField(value = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Long sourceId;
    /**
     * 供应商账单
     */
    @DEField(name = "vendor_bill_id")
    @TableField(value = "vendor_bill_id")
    @JSONField(name = "vendor_bill_id")
    @JsonProperty("vendor_bill_id")
    private Long vendorBillId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 自动完成
     */
    @DEField(name = "vendor_bill_purchase_id")
    @TableField(value = "vendor_bill_purchase_id")
    @JSONField(name = "vendor_bill_purchase_id")
    @JsonProperty("vendor_bill_purchase_id")
    private Long vendorBillPurchaseId;
    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @TableField(value = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Long campaignId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_cash_rounding odooCashRounding;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms odooIncoterms;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms odooIncoterm;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooRefundInvoice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooVendorBill;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move odooMove;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooPaymentTerm;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_bill_union odooVendorBillPurchase;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order odooPurchase;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank odooPartnerBank;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooCommercialPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartnerShipping;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource;



    /**
     * 设置 [日记账分录名称]
     */
    public void setMoveName(String moveName){
        this.moveName = moveName ;
        this.modify("move_name",moveName);
    }

    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [会计日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [会计日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
    /**
     * 设置 [公司货币的合计]
     */
    public void setAmountTotalCompanySigned(BigDecimal amountTotalCompanySigned){
        this.amountTotalCompanySigned = amountTotalCompanySigned ;
        this.modify("amount_total_company_signed",amountTotalCompanySigned);
    }

    /**
     * 设置 [按公司本位币计的不含税金额]
     */
    public void setAmountUntaxedSigned(BigDecimal amountUntaxedSigned){
        this.amountUntaxedSigned = amountUntaxedSigned ;
        this.modify("amount_untaxed_signed",amountUntaxedSigned);
    }

    /**
     * 设置 [到期金额]
     */
    public void setResidual(BigDecimal residual){
        this.residual = residual ;
        this.modify("residual",residual);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [额外的信息]
     */
    public void setComment(String comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }

    /**
     * 设置 [已付／已核销]
     */
    public void setReconciled(Boolean reconciled){
        this.reconciled = reconciled ;
        this.modify("reconciled",reconciled);
    }

    /**
     * 设置 [已汇]
     */
    public void setSent(Boolean sent){
        this.sent = sent ;
        this.modify("sent",sent);
    }

    /**
     * 设置 [以发票币种总计]
     */
    public void setAmountTotalSigned(BigDecimal amountTotalSigned){
        this.amountTotalSigned = amountTotalSigned ;
        this.modify("amount_total_signed",amountTotalSigned);
    }

    /**
     * 设置 [发票使用币种的逾期金额]
     */
    public void setResidualSigned(BigDecimal residualSigned){
        this.residualSigned = residualSigned ;
        this.modify("residual_signed",residualSigned);
    }

    /**
     * 设置 [安全令牌]
     */
    public void setAccessToken(String accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [供应商名称]
     */
    public void setVendorDisplayName(String vendorDisplayName){
        this.vendorDisplayName = vendorDisplayName ;
        this.modify("vendor_display_name",vendorDisplayName);
    }

    /**
     * 设置 [未税金额]
     */
    public void setAmountUntaxed(BigDecimal amountUntaxed){
        this.amountUntaxed = amountUntaxed ;
        this.modify("amount_untaxed",amountUntaxed);
    }

    /**
     * 设置 [到期日期]
     */
    public void setDateDue(Timestamp dateDue){
        this.dateDue = dateDue ;
        this.modify("date_due",dateDue);
    }

    /**
     * 格式化日期 [到期日期]
     */
    public String formatDateDue(){
        if (this.dateDue == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateDue);
    }
    /**
     * 设置 [源邮箱]
     */
    public void setSourceEmail(String sourceEmail){
        this.sourceEmail = sourceEmail ;
        this.modify("source_email",sourceEmail);
    }

    /**
     * 设置 [税率]
     */
    public void setAmountTax(BigDecimal amountTax){
        this.amountTax = amountTax ;
        this.modify("amount_tax",amountTax);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [公司使用币种的逾期金额]
     */
    public void setResidualCompanySigned(BigDecimal residualCompanySigned){
        this.residualCompanySigned = residualCompanySigned ;
        this.modify("residual_company_signed",residualCompanySigned);
    }

    /**
     * 设置 [开票日期]
     */
    public void setDateInvoice(Timestamp dateInvoice){
        this.dateInvoice = dateInvoice ;
        this.modify("date_invoice",dateInvoice);
    }

    /**
     * 格式化日期 [开票日期]
     */
    public String formatDateInvoice(){
        if (this.dateInvoice == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateInvoice);
    }
    /**
     * 设置 [参考/说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [付款参考:]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [总计]
     */
    public void setAmountTotal(BigDecimal amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }

    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [销售员]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Long teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [科目]
     */
    public void setAccountId(Long accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [媒体]
     */
    public void setMediumId(Long mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [日记账分录]
     */
    public void setMoveId(Long moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [付款条款]
     */
    public void setPaymentTermId(Long paymentTermId){
        this.paymentTermId = paymentTermId ;
        this.modify("payment_term_id",paymentTermId);
    }

    /**
     * 设置 [日记账]
     */
    public void setJournalId(Long journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [国际贸易术语]
     */
    public void setIncotermId(Long incotermId){
        this.incotermId = incotermId ;
        this.modify("incoterm_id",incotermId);
    }

    /**
     * 设置 [添加采购订单]
     */
    public void setPurchaseId(Long purchaseId){
        this.purchaseId = purchaseId ;
        this.modify("purchase_id",purchaseId);
    }

    /**
     * 设置 [税科目调整]
     */
    public void setFiscalPositionId(Long fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }

    /**
     * 设置 [此发票为信用票的发票]
     */
    public void setRefundInvoiceId(Long refundInvoiceId){
        this.refundInvoiceId = refundInvoiceId ;
        this.modify("refund_invoice_id",refundInvoiceId);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [银行账户]
     */
    public void setPartnerBankId(Long partnerBankId){
        this.partnerBankId = partnerBankId ;
        this.modify("partner_bank_id",partnerBankId);
    }

    /**
     * 设置 [现金舍入方式]
     */
    public void setCashRoundingId(Long cashRoundingId){
        this.cashRoundingId = cashRoundingId ;
        this.modify("cash_rounding_id",cashRoundingId);
    }

    /**
     * 设置 [商业实体]
     */
    public void setCommercialPartnerId(Long commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }

    /**
     * 设置 [送货地址]
     */
    public void setPartnerShippingId(Long partnerShippingId){
        this.partnerShippingId = partnerShippingId ;
        this.modify("partner_shipping_id",partnerShippingId);
    }

    /**
     * 设置 [贸易条款]
     */
    public void setIncotermsId(Long incotermsId){
        this.incotermsId = incotermsId ;
        this.modify("incoterms_id",incotermsId);
    }

    /**
     * 设置 [来源]
     */
    public void setSourceId(Long sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [供应商账单]
     */
    public void setVendorBillId(Long vendorBillId){
        this.vendorBillId = vendorBillId ;
        this.modify("vendor_bill_id",vendorBillId);
    }

    /**
     * 设置 [自动完成]
     */
    public void setVendorBillPurchaseId(Long vendorBillPurchaseId){
        this.vendorBillPurchaseId = vendorBillPurchaseId ;
        this.modify("vendor_bill_purchase_id",vendorBillPurchaseId);
    }

    /**
     * 设置 [营销]
     */
    public void setCampaignId(Long campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



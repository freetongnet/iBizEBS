package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_skill;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_skillSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_skill] 服务对象接口
 */
public interface IHr_skillService extends IService<Hr_skill>{

    boolean create(Hr_skill et) ;
    void createBatch(List<Hr_skill> list) ;
    boolean update(Hr_skill et) ;
    void updateBatch(List<Hr_skill> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_skill get(Long key) ;
    Hr_skill getDraft(Hr_skill et) ;
    boolean checkKey(Hr_skill et) ;
    boolean save(Hr_skill et) ;
    void saveBatch(List<Hr_skill> list) ;
    Page<Hr_skill> searchDefault(Hr_skillSearchContext context) ;
    List<Hr_skill> selectBySkillTypeId(Long id);
    void removeBySkillTypeId(Long id);
    List<Hr_skill> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_skill> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



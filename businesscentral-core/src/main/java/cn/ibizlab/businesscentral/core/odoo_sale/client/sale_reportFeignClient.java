package cn.ibizlab.businesscentral.core.odoo_sale.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_reportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[sale_report] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-sale:odoo-sale}", contextId = "sale-report", fallback = sale_reportFallback.class)
public interface sale_reportFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/sale_reports/search")
    Page<Sale_report> search(@RequestBody Sale_reportSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/sale_reports/{id}")
    Sale_report get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.PUT, value = "/sale_reports/{id}")
    Sale_report update(@PathVariable("id") Long id,@RequestBody Sale_report sale_report);

    @RequestMapping(method = RequestMethod.PUT, value = "/sale_reports/batch")
    Boolean updateBatch(@RequestBody List<Sale_report> sale_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_reports/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/sale_reports/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/sale_reports")
    Sale_report create(@RequestBody Sale_report sale_report);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_reports/batch")
    Boolean createBatch(@RequestBody List<Sale_report> sale_reports);




    @RequestMapping(method = RequestMethod.GET, value = "/sale_reports/select")
    Page<Sale_report> select();


    @RequestMapping(method = RequestMethod.GET, value = "/sale_reports/getdraft")
    Sale_report getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/sale_reports/checkkey")
    Boolean checkKey(@RequestBody Sale_report sale_report);


    @RequestMapping(method = RequestMethod.POST, value = "/sale_reports/save")
    Boolean save(@RequestBody Sale_report sale_report);

    @RequestMapping(method = RequestMethod.POST, value = "/sale_reports/savebatch")
    Boolean saveBatch(@RequestBody List<Sale_report> sale_reports);



    @RequestMapping(method = RequestMethod.POST, value = "/sale_reports/searchdefault")
    Page<Sale_report> searchDefault(@RequestBody Sale_reportSearchContext context);


}

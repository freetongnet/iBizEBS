package cn.ibizlab.businesscentral.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[crm_merge_opportunity] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-crm:odoo-crm}", contextId = "crm-merge-opportunity", fallback = crm_merge_opportunityFallback.class)
public interface crm_merge_opportunityFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/crm_merge_opportunities/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/{id}")
    Crm_merge_opportunity update(@PathVariable("id") Long id,@RequestBody Crm_merge_opportunity crm_merge_opportunity);

    @RequestMapping(method = RequestMethod.PUT, value = "/crm_merge_opportunities/batch")
    Boolean updateBatch(@RequestBody List<Crm_merge_opportunity> crm_merge_opportunities);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities")
    Crm_merge_opportunity create(@RequestBody Crm_merge_opportunity crm_merge_opportunity);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/batch")
    Boolean createBatch(@RequestBody List<Crm_merge_opportunity> crm_merge_opportunities);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/{id}")
    Crm_merge_opportunity get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/search")
    Page<Crm_merge_opportunity> search(@RequestBody Crm_merge_opportunitySearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/select")
    Page<Crm_merge_opportunity> select();


    @RequestMapping(method = RequestMethod.GET, value = "/crm_merge_opportunities/getdraft")
    Crm_merge_opportunity getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/checkkey")
    Boolean checkKey(@RequestBody Crm_merge_opportunity crm_merge_opportunity);


    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/save")
    Boolean save(@RequestBody Crm_merge_opportunity crm_merge_opportunity);

    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/savebatch")
    Boolean saveBatch(@RequestBody List<Crm_merge_opportunity> crm_merge_opportunities);



    @RequestMapping(method = RequestMethod.POST, value = "/crm_merge_opportunities/searchdefault")
    Page<Crm_merge_opportunity> searchDefault(@RequestBody Crm_merge_opportunitySearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_supplierSearchContext;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplierService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_base.mapper.Res_supplierMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[供应商] 服务对象接口实现
 */
@Slf4j
@Service("Res_supplierServiceImpl")
public class Res_supplierServiceImpl extends EBSServiceImpl<Res_supplierMapper, Res_supplier> implements IRes_supplierService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_supplierinfoService productSupplierinfoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchaseOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_requisitionService purchaseRequisitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplier_res_partner_category_relService resSupplierResPartnerCategoryRelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService accountFiscalPositionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService accountPaymentTermService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IDelivery_carrierService deliveryCarrierService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService productPricelistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService resCountryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_country_stateService resCountryStateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_titleService resPartnerTitleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "res.supplier" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Res_supplier et) {
        String category_id = et.getCategoryId() ;

        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_supplierService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        if(org.apache.commons.lang3.StringUtils.isNotBlank(et.getCategoryId())){
            this.baseMapper.saveRelByCategoryId(et.getId() , com.alibaba.fastjson.JSONArray.parseArray(category_id, cn.ibizlab.businesscentral.util.domain.MultiSelectItem.class));
        }

        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Res_supplier> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Res_supplier et) {
        if(et.getFocusNull().contains("category_id") || et.getCategoryId()!=null){
            et.getFocusNull().remove("category_id");
            resSupplierResPartnerCategoryRelService.removeByPartnerId(et.getId());
            if(org.apache.commons.lang3.StringUtils.isNotBlank(et.getCategoryId())){
                this.baseMapper.saveRelByCategoryId(et.getId() , com.alibaba.fastjson.JSONArray.parseArray(et.getCategoryId(), cn.ibizlab.businesscentral.util.domain.MultiSelectItem.class));
            }
        }

        Res_supplier old = new Res_supplier() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_supplierService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_supplierService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Res_supplier> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        resSupplierResPartnerCategoryRelService.removeByPartnerId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        resSupplierResPartnerCategoryRelService.removeByPartnerId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Res_supplier get(Long key) {
        Res_supplier et = getById(key);
        if(et==null){
            et=new Res_supplier();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Res_supplier getDraft(Res_supplier et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Res_supplier et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public Res_supplier masterTabCount(Res_supplier et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean save(Res_supplier et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Res_supplier et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Res_supplier> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Res_supplier> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Res_supplier> selectByPropertyAccountPositionId(Long id) {
        return baseMapper.selectByPropertyAccountPositionId(id);
    }
    @Override
    public void removeByPropertyAccountPositionId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("property_account_position_id",id));
    }

	@Override
    public List<Res_supplier> selectByPropertyPaymentTermId(Long id) {
        return baseMapper.selectByPropertyPaymentTermId(id);
    }
    @Override
    public void removeByPropertyPaymentTermId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("property_payment_term_id",id));
    }

	@Override
    public List<Res_supplier> selectByPropertySupplierPaymentTermId(Long id) {
        return baseMapper.selectByPropertySupplierPaymentTermId(id);
    }
    @Override
    public void removeByPropertySupplierPaymentTermId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("property_supplier_payment_term_id",id));
    }

	@Override
    public List<Res_supplier> selectByPropertyDeliveryCarrierId(Long id) {
        return baseMapper.selectByPropertyDeliveryCarrierId(id);
    }
    @Override
    public void removeByPropertyDeliveryCarrierId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("property_delivery_carrier_id",id));
    }

	@Override
    public List<Res_supplier> selectByPropertyProductPricelist(Long id) {
        return baseMapper.selectByPropertyProductPricelist(id);
    }
    @Override
    public void removeByPropertyProductPricelist(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("property_product_pricelist",id));
    }

	@Override
    public List<Res_supplier> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("company_id",id));
    }

	@Override
    public List<Res_supplier> selectByCountryId(Long id) {
        return baseMapper.selectByCountryId(id);
    }
    @Override
    public void removeByCountryId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("country_id",id));
    }

	@Override
    public List<Res_supplier> selectByStateId(Long id) {
        return baseMapper.selectByStateId(id);
    }
    @Override
    public void removeByStateId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("state_id",id));
    }

	@Override
    public List<Res_supplier> selectByPropertyPurchaseCurrencyId(Long id) {
        return baseMapper.selectByPropertyPurchaseCurrencyId(id);
    }
    @Override
    public void removeByPropertyPurchaseCurrencyId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("property_purchase_currency_id",id));
    }

	@Override
    public List<Res_supplier> selectByParentId(Long id) {
        return baseMapper.selectByParentId(id);
    }
    @Override
    public void removeByParentId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("parent_id",id));
    }

	@Override
    public List<Res_supplier> selectByTitle(Long id) {
        return baseMapper.selectByTitle(id);
    }
    @Override
    public void removeByTitle(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("title",id));
    }

	@Override
    public List<Res_supplier> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("user_id",id));
    }

	@Override
    public List<Res_supplier> selectByPropertyStockCustomer(Long id) {
        return baseMapper.selectByPropertyStockCustomer(id);
    }
    @Override
    public void removeByPropertyStockCustomer(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("property_stock_customer",id));
    }

	@Override
    public List<Res_supplier> selectByPropertyStockSubcontractor(Long id) {
        return baseMapper.selectByPropertyStockSubcontractor(id);
    }
    @Override
    public void removeByPropertyStockSubcontractor(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("property_stock_subcontractor",id));
    }

	@Override
    public List<Res_supplier> selectByPropertyStockSupplier(Long id) {
        return baseMapper.selectByPropertyStockSupplier(id);
    }
    @Override
    public void removeByPropertyStockSupplier(Long id) {
        this.remove(new QueryWrapper<Res_supplier>().eq("property_stock_supplier",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Res_supplier> searchDefault(Res_supplierSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Res_supplier> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Res_supplier>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 首选表格
     */
    @Override
    public Page<Res_supplier> searchMaster(Res_supplierSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Res_supplier> pages=baseMapper.searchMaster(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Res_supplier>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Res_supplier et){
        //实体关系[DER1N_RES_SUPPLIER_ACCOUNT_FISCAL_POSITION_PROPERTY_ACCOUNT_POSITION_ID]
        if(!ObjectUtils.isEmpty(et.getPropertyAccountPositionId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooAccountPosition=et.getOdooAccountPosition();
            if(ObjectUtils.isEmpty(odooAccountPosition)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position majorEntity=accountFiscalPositionService.get(et.getPropertyAccountPositionId());
                et.setOdooAccountPosition(majorEntity);
                odooAccountPosition=majorEntity;
            }
            et.setPropertyAccountPositionName(odooAccountPosition.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_ACCOUNT_PAYMENT_TERM_PROPERTY_PAYMENT_TERM_ID]
        if(!ObjectUtils.isEmpty(et.getPropertyPaymentTermId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooAccountPaymentTerm=et.getOdooAccountPaymentTerm();
            if(ObjectUtils.isEmpty(odooAccountPaymentTerm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term majorEntity=accountPaymentTermService.get(et.getPropertyPaymentTermId());
                et.setOdooAccountPaymentTerm(majorEntity);
                odooAccountPaymentTerm=majorEntity;
            }
            et.setPropertyPaymentTermName(odooAccountPaymentTerm.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_ACCOUNT_PAYMENT_TERM_PROPERTY_SUPPLIER_PAYMENT_TERM_ID]
        if(!ObjectUtils.isEmpty(et.getPropertySupplierPaymentTermId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooPurchasePaymentTerm=et.getOdooPurchasePaymentTerm();
            if(ObjectUtils.isEmpty(odooPurchasePaymentTerm)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term majorEntity=accountPaymentTermService.get(et.getPropertySupplierPaymentTermId());
                et.setOdooPurchasePaymentTerm(majorEntity);
                odooPurchasePaymentTerm=majorEntity;
            }
            et.setPropertySupplierPaymentTermName(odooPurchasePaymentTerm.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_DELIVERY_CARRIER_PROPERTY_DELIVERY_CARRIER_ID]
        if(!ObjectUtils.isEmpty(et.getPropertyDeliveryCarrierId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Delivery_carrier odooDeliveryCarrier=et.getOdooDeliveryCarrier();
            if(ObjectUtils.isEmpty(odooDeliveryCarrier)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Delivery_carrier majorEntity=deliveryCarrierService.get(et.getPropertyDeliveryCarrierId());
                et.setOdooDeliveryCarrier(majorEntity);
                odooDeliveryCarrier=majorEntity;
            }
            et.setPropertyDeliveryCarrierName(odooDeliveryCarrier.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_PRODUCT_PRICELIST_PROPERTY_PRODUCT_PRICELIST]
        if(!ObjectUtils.isEmpty(et.getPropertyProductPricelist())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist=et.getOdooPricelist();
            if(ObjectUtils.isEmpty(odooPricelist)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist majorEntity=productPricelistService.get(et.getPropertyProductPricelist());
                et.setOdooPricelist(majorEntity);
                odooPricelist=majorEntity;
            }
            et.setPropertyProductPricelistName(odooPricelist.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_RES_COMPANY_COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_RES_COUNTRY_COUNTRY_ID]
        if(!ObjectUtils.isEmpty(et.getCountryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry=et.getOdooCountry();
            if(ObjectUtils.isEmpty(odooCountry)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country majorEntity=resCountryService.get(et.getCountryId());
                et.setOdooCountry(majorEntity);
                odooCountry=majorEntity;
            }
            et.setCountryIdText(odooCountry.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_RES_COUNTRY_STATE_STATE_ID]
        if(!ObjectUtils.isEmpty(et.getStateId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state odooState=et.getOdooState();
            if(ObjectUtils.isEmpty(odooState)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state majorEntity=resCountryStateService.get(et.getStateId());
                et.setOdooState(majorEntity);
                odooState=majorEntity;
            }
            et.setStateIdText(odooState.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_RES_CURRENCY_PROPERTY_PURCHASE_CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getPropertyPurchaseCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getPropertyPurchaseCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setPropertyPurchaseCurrencyName(odooCurrency.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_RES_PARTNER_PARENT_ID]
        if(!ObjectUtils.isEmpty(et.getParentId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooParent=et.getOdooParent();
            if(ObjectUtils.isEmpty(odooParent)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getParentId());
                et.setOdooParent(majorEntity);
                odooParent=majorEntity;
            }
            et.setParentName(odooParent.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_RES_PARTNER_TITLE_TITLE]
        if(!ObjectUtils.isEmpty(et.getTitle())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_title odooTitle=et.getOdooTitle();
            if(ObjectUtils.isEmpty(odooTitle)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_title majorEntity=resPartnerTitleService.get(et.getTitle());
                et.setOdooTitle(majorEntity);
                odooTitle=majorEntity;
            }
            et.setTitleText(odooTitle.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_RES_USERS_USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserName(odooUser.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_STOCK_LOCATION_PROPERTY_STOCK_CUSTOMER]
        if(!ObjectUtils.isEmpty(et.getPropertyStockCustomer())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooCustomerLocation=et.getOdooCustomerLocation();
            if(ObjectUtils.isEmpty(odooCustomerLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getPropertyStockCustomer());
                et.setOdooCustomerLocation(majorEntity);
                odooCustomerLocation=majorEntity;
            }
            et.setPropertyStockCustomerName(odooCustomerLocation.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_STOCK_LOCATION_PROPERTY_STOCK_SUBCONTRACTOR]
        if(!ObjectUtils.isEmpty(et.getPropertyStockSubcontractor())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooSubcontractorLocation=et.getOdooSubcontractorLocation();
            if(ObjectUtils.isEmpty(odooSubcontractorLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getPropertyStockSubcontractor());
                et.setOdooSubcontractorLocation(majorEntity);
                odooSubcontractorLocation=majorEntity;
            }
            et.setPropertyStockSubcontractorName(odooSubcontractorLocation.getName());
        }
        //实体关系[DER1N_RES_SUPPLIER_STOCK_LOCATION_PROPERTY_STOCK_SUPPLIER]
        if(!ObjectUtils.isEmpty(et.getPropertyStockSupplier())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooVendorLocation=et.getOdooVendorLocation();
            if(ObjectUtils.isEmpty(odooVendorLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getPropertyStockSupplier());
                et.setOdooVendorLocation(majorEntity);
                odooVendorLocation=majorEntity;
            }
            et.setPropertyStockSupplierName(odooVendorLocation.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Res_supplier> getResSupplierByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Res_supplier> getResSupplierByEntities(List<Res_supplier> entities) {
        List ids =new ArrayList();
        for(Res_supplier entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




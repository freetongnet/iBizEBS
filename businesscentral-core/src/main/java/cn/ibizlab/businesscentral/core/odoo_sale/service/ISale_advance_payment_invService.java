package cn.ibizlab.businesscentral.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sale_advance_payment_inv] 服务对象接口
 */
public interface ISale_advance_payment_invService extends IService<Sale_advance_payment_inv>{

    boolean create(Sale_advance_payment_inv et) ;
    void createBatch(List<Sale_advance_payment_inv> list) ;
    boolean update(Sale_advance_payment_inv et) ;
    void updateBatch(List<Sale_advance_payment_inv> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sale_advance_payment_inv get(Long key) ;
    Sale_advance_payment_inv getDraft(Sale_advance_payment_inv et) ;
    boolean checkKey(Sale_advance_payment_inv et) ;
    boolean save(Sale_advance_payment_inv et) ;
    void saveBatch(List<Sale_advance_payment_inv> list) ;
    Page<Sale_advance_payment_inv> searchDefault(Sale_advance_payment_invSearchContext context) ;
    List<Sale_advance_payment_inv> selectByDepositAccountId(Long id);
    void resetByDepositAccountId(Long id);
    void resetByDepositAccountId(Collection<Long> ids);
    void removeByDepositAccountId(Long id);
    List<Sale_advance_payment_inv> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Sale_advance_payment_inv> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Sale_advance_payment_inv> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sale_advance_payment_inv> getSaleAdvancePaymentInvByIds(List<Long> ids) ;
    List<Sale_advance_payment_inv> getSaleAdvancePaymentInvByEntities(List<Sale_advance_payment_inv> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_project.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_project;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_projectSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Project_projectMapper extends BaseMapper<Project_project>{

    Page<Project_project> searchDefault(IPage page, @Param("srf") Project_projectSearchContext context, @Param("ew") Wrapper<Project_project> wrapper) ;
    @Override
    Project_project selectById(Serializable id);
    @Override
    int insert(Project_project entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Project_project entity);
    @Override
    int update(@Param(Constants.ENTITY) Project_project entity, @Param("ew") Wrapper<Project_project> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Project_project> selectByAliasId(@Param("id") Serializable id) ;

    List<Project_project> selectBySubtaskProjectId(@Param("id") Serializable id) ;

    List<Project_project> selectByResourceCalendarId(@Param("id") Serializable id) ;

    List<Project_project> selectByCompanyId(@Param("id") Serializable id) ;

    List<Project_project> selectByPartnerId(@Param("id") Serializable id) ;

    List<Project_project> selectByCreateUid(@Param("id") Serializable id) ;

    List<Project_project> selectByUserId(@Param("id") Serializable id) ;

    List<Project_project> selectByWriteUid(@Param("id") Serializable id) ;


}

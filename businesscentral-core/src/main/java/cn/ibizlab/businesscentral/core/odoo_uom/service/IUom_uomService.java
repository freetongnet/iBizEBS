package cn.ibizlab.businesscentral.core.odoo_uom.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.businesscentral.core.odoo_uom.filter.Uom_uomSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Uom_uom] 服务对象接口
 */
public interface IUom_uomService extends IService<Uom_uom>{

    boolean create(Uom_uom et) ;
    void createBatch(List<Uom_uom> list) ;
    boolean update(Uom_uom et) ;
    void updateBatch(List<Uom_uom> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Uom_uom get(Long key) ;
    Uom_uom getDraft(Uom_uom et) ;
    boolean checkKey(Uom_uom et) ;
    boolean save(Uom_uom et) ;
    void saveBatch(List<Uom_uom> list) ;
    Page<Uom_uom> searchDefault(Uom_uomSearchContext context) ;
    List<Uom_uom> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Uom_uom> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Uom_uom> selectByCategoryId(Long id);
    void removeByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Uom_uom> getUomUomByIds(List<Long> ids) ;
    List<Uom_uom> getUomUomByEntities(List<Uom_uom> entities) ;
}



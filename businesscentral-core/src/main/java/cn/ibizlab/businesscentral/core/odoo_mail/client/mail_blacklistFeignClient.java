package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_blacklist;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_blacklistSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_blacklist] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-blacklist", fallback = mail_blacklistFallback.class)
public interface mail_blacklistFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/mail_blacklists/{id}")
    Mail_blacklist get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklists/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_blacklists/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklists/{id}")
    Mail_blacklist update(@PathVariable("id") Long id,@RequestBody Mail_blacklist mail_blacklist);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_blacklists/batch")
    Boolean updateBatch(@RequestBody List<Mail_blacklist> mail_blacklists);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists")
    Mail_blacklist create(@RequestBody Mail_blacklist mail_blacklist);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/batch")
    Boolean createBatch(@RequestBody List<Mail_blacklist> mail_blacklists);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/search")
    Page<Mail_blacklist> search(@RequestBody Mail_blacklistSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_blacklists/select")
    Page<Mail_blacklist> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_blacklists/getdraft")
    Mail_blacklist getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/checkkey")
    Boolean checkKey(@RequestBody Mail_blacklist mail_blacklist);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/save")
    Boolean save(@RequestBody Mail_blacklist mail_blacklist);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_blacklist> mail_blacklists);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_blacklists/searchdefault")
    Page<Mail_blacklist> searchDefault(@RequestBody Mail_blacklistSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoiceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_invoice] 服务对象接口
 */
public interface IAccount_invoiceService extends IService<Account_invoice>{

    boolean create(Account_invoice et) ;
    void createBatch(List<Account_invoice> list) ;
    boolean update(Account_invoice et) ;
    void updateBatch(List<Account_invoice> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_invoice get(Long key) ;
    Account_invoice getDraft(Account_invoice et) ;
    boolean checkKey(Account_invoice et) ;
    boolean save(Account_invoice et) ;
    void saveBatch(List<Account_invoice> list) ;
    Page<Account_invoice> searchDefault(Account_invoiceSearchContext context) ;
    List<Account_invoice> selectByAccountId(Long id);
    void resetByAccountId(Long id);
    void resetByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_invoice> selectByCashRoundingId(Long id);
    void resetByCashRoundingId(Long id);
    void resetByCashRoundingId(Collection<Long> ids);
    void removeByCashRoundingId(Long id);
    List<Account_invoice> selectByFiscalPositionId(Long id);
    void resetByFiscalPositionId(Long id);
    void resetByFiscalPositionId(Collection<Long> ids);
    void removeByFiscalPositionId(Long id);
    List<Account_invoice> selectByIncotermsId(Long id);
    void resetByIncotermsId(Long id);
    void resetByIncotermsId(Collection<Long> ids);
    void removeByIncotermsId(Long id);
    List<Account_invoice> selectByIncotermId(Long id);
    void resetByIncotermId(Long id);
    void resetByIncotermId(Collection<Long> ids);
    void removeByIncotermId(Long id);
    List<Account_invoice> selectByRefundInvoiceId(Long id);
    void resetByRefundInvoiceId(Long id);
    void resetByRefundInvoiceId(Collection<Long> ids);
    void removeByRefundInvoiceId(Long id);
    List<Account_invoice> selectByVendorBillId(Long id);
    void resetByVendorBillId(Long id);
    void resetByVendorBillId(Collection<Long> ids);
    void removeByVendorBillId(Long id);
    List<Account_invoice> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Account_invoice> selectByMoveId(Long id);
    List<Account_invoice> selectByMoveId(Collection<Long> ids);
    void removeByMoveId(Long id);
    List<Account_invoice> selectByPaymentTermId(Long id);
    void resetByPaymentTermId(Long id);
    void resetByPaymentTermId(Collection<Long> ids);
    void removeByPaymentTermId(Long id);
    List<Account_invoice> selectByTeamId(Long id);
    void resetByTeamId(Long id);
    void resetByTeamId(Collection<Long> ids);
    void removeByTeamId(Long id);
    List<Account_invoice> selectByVendorBillPurchaseId(Long id);
    void resetByVendorBillPurchaseId(Long id);
    void resetByVendorBillPurchaseId(Collection<Long> ids);
    void removeByVendorBillPurchaseId(Long id);
    List<Account_invoice> selectByPurchaseId(Long id);
    void resetByPurchaseId(Long id);
    void resetByPurchaseId(Collection<Long> ids);
    void removeByPurchaseId(Long id);
    List<Account_invoice> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_invoice> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_invoice> selectByPartnerBankId(Long id);
    void resetByPartnerBankId(Long id);
    void resetByPartnerBankId(Collection<Long> ids);
    void removeByPartnerBankId(Long id);
    List<Account_invoice> selectByCommercialPartnerId(Long id);
    void resetByCommercialPartnerId(Long id);
    void resetByCommercialPartnerId(Collection<Long> ids);
    void removeByCommercialPartnerId(Long id);
    List<Account_invoice> selectByPartnerId(Long id);
    List<Account_invoice> selectByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Account_invoice> selectByPartnerShippingId(Long id);
    void resetByPartnerShippingId(Long id);
    void resetByPartnerShippingId(Collection<Long> ids);
    void removeByPartnerShippingId(Long id);
    List<Account_invoice> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_invoice> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Account_invoice> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Account_invoice> selectByCampaignId(Long id);
    void resetByCampaignId(Long id);
    void resetByCampaignId(Collection<Long> ids);
    void removeByCampaignId(Long id);
    List<Account_invoice> selectByMediumId(Long id);
    void resetByMediumId(Long id);
    void resetByMediumId(Collection<Long> ids);
    void removeByMediumId(Long id);
    List<Account_invoice> selectBySourceId(Long id);
    void resetBySourceId(Long id);
    void resetBySourceId(Collection<Long> ids);
    void removeBySourceId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_invoice> getAccountInvoiceByIds(List<Long> ids) ;
    List<Account_invoice> getAccountInvoiceByEntities(List<Account_invoice> entities) ;
}



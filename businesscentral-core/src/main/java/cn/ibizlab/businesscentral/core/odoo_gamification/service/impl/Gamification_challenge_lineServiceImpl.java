package cn.ibizlab.businesscentral.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge_line;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challenge_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_gamification.mapper.Gamification_challenge_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[游戏化挑战的一般目标] 服务对象接口实现
 */
@Slf4j
@Service("Gamification_challenge_lineServiceImpl")
public class Gamification_challenge_lineServiceImpl extends EBSServiceImpl<Gamification_challenge_lineMapper, Gamification_challenge_line> implements IGamification_challenge_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goalService gamificationGoalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_challengeService gamificationChallengeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_gamification.service.IGamification_goal_definitionService gamificationGoalDefinitionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "gamification.challenge.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Gamification_challenge_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_challenge_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Gamification_challenge_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Gamification_challenge_line et) {
        Gamification_challenge_line old = new Gamification_challenge_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_challenge_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IGamification_challenge_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Gamification_challenge_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        gamificationGoalService.removeByLineId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        gamificationGoalService.removeByLineId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Gamification_challenge_line get(Long key) {
        Gamification_challenge_line et = getById(key);
        if(et==null){
            et=new Gamification_challenge_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Gamification_challenge_line getDraft(Gamification_challenge_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Gamification_challenge_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Gamification_challenge_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Gamification_challenge_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Gamification_challenge_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Gamification_challenge_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Gamification_challenge_line> selectByChallengeId(Long id) {
        return baseMapper.selectByChallengeId(id);
    }
    @Override
    public void removeByChallengeId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Gamification_challenge_line>().in("challenge_id",ids));
    }

    @Override
    public void removeByChallengeId(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge_line>().eq("challenge_id",id));
    }

	@Override
    public List<Gamification_challenge_line> selectByDefinitionId(Long id) {
        return baseMapper.selectByDefinitionId(id);
    }
    @Override
    public void removeByDefinitionId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Gamification_challenge_line>().in("definition_id",ids));
    }

    @Override
    public void removeByDefinitionId(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge_line>().eq("definition_id",id));
    }

	@Override
    public List<Gamification_challenge_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge_line>().eq("create_uid",id));
    }

	@Override
    public List<Gamification_challenge_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Gamification_challenge_line>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Gamification_challenge_line> searchDefault(Gamification_challenge_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Gamification_challenge_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Gamification_challenge_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Gamification_challenge_line et){
        //实体关系[DER1N_GAMIFICATION_CHALLENGE_LINE__GAMIFICATION_CHALLENGE__CHALLENGE_ID]
        if(!ObjectUtils.isEmpty(et.getChallengeId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge odooChallenge=et.getOdooChallenge();
            if(ObjectUtils.isEmpty(odooChallenge)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge majorEntity=gamificationChallengeService.get(et.getChallengeId());
                et.setOdooChallenge(majorEntity);
                odooChallenge=majorEntity;
            }
            et.setChallengeIdText(odooChallenge.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE_LINE__GAMIFICATION_GOAL_DEFINITION__DEFINITION_ID]
        if(!ObjectUtils.isEmpty(et.getDefinitionId())){
            cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_definition odooDefinition=et.getOdooDefinition();
            if(ObjectUtils.isEmpty(odooDefinition)){
                cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_goal_definition majorEntity=gamificationGoalDefinitionService.get(et.getDefinitionId());
                et.setOdooDefinition(majorEntity);
                odooDefinition=majorEntity;
            }
            et.setDefinitionMonetary(odooDefinition.getMonetary());
            et.setCondition(odooDefinition.getCondition());
            et.setDefinitionFullSuffix(odooDefinition.getFullSuffix());
            et.setName(odooDefinition.getName());
            et.setDefinitionSuffix(odooDefinition.getSuffix());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_GAMIFICATION_CHALLENGE_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Gamification_challenge_line> getGamificationChallengeLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Gamification_challenge_line> getGamificationChallengeLineByEntities(List<Gamification_challenge_line> entities) {
        List ids =new ArrayList();
        for(Gamification_challenge_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




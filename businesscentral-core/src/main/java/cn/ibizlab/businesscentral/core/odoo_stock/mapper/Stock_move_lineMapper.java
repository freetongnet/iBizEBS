package cn.ibizlab.businesscentral.core.odoo_stock.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_move_lineSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Stock_move_lineMapper extends BaseMapper<Stock_move_line>{

    Page<Stock_move_line> searchDefault(IPage page, @Param("srf") Stock_move_lineSearchContext context, @Param("ew") Wrapper<Stock_move_line> wrapper) ;
    @Override
    Stock_move_line selectById(Serializable id);
    @Override
    int insert(Stock_move_line entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Stock_move_line entity);
    @Override
    int update(@Param(Constants.ENTITY) Stock_move_line entity, @Param("ew") Wrapper<Stock_move_line> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Stock_move_line> selectByProductionId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByWorkorderId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByProductId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByOwnerId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByCreateUid(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByWriteUid(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByLocationDestId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByLocationId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByMoveId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByPackageLevelId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByPickingId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByLotId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByLotProducedId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByPackageId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByResultPackageId(@Param("id") Serializable id) ;

    List<Stock_move_line> selectByProductUomId(@Param("id") Serializable id) ;


}

package cn.ibizlab.businesscentral.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Purchase_bill_union] 服务对象接口
 */
public interface IPurchase_bill_unionService extends IService<Purchase_bill_union>{

    boolean create(Purchase_bill_union et) ;
    void createBatch(List<Purchase_bill_union> list) ;
    boolean update(Purchase_bill_union et) ;
    void updateBatch(List<Purchase_bill_union> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Purchase_bill_union get(Long key) ;
    Purchase_bill_union getDraft(Purchase_bill_union et) ;
    boolean checkKey(Purchase_bill_union et) ;
    boolean save(Purchase_bill_union et) ;
    void saveBatch(List<Purchase_bill_union> list) ;
    Page<Purchase_bill_union> searchDefault(Purchase_bill_unionSearchContext context) ;
    List<Purchase_bill_union> selectByVendorBillId(Long id);
    void resetByVendorBillId(Long id);
    void resetByVendorBillId(Collection<Long> ids);
    void removeByVendorBillId(Long id);
    List<Purchase_bill_union> selectByPurchaseOrderId(Long id);
    void resetByPurchaseOrderId(Long id);
    void resetByPurchaseOrderId(Collection<Long> ids);
    void removeByPurchaseOrderId(Long id);
    List<Purchase_bill_union> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Purchase_bill_union> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Purchase_bill_union> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



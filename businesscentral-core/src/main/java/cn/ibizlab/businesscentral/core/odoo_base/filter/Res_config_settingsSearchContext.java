package cn.ibizlab.businesscentral.core.odoo_base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_settings;
/**
 * 关系型数据实体[Res_config_settings] 查询条件对象
 */
@Slf4j
@Data
public class Res_config_settingsSearchContext extends QueryWrapperContext<Res_config_settings> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_default_picking_policy_eq;//[拣货策略]
	public void setN_default_picking_policy_eq(String n_default_picking_policy_eq) {
        this.n_default_picking_policy_eq = n_default_picking_policy_eq;
        if(!ObjectUtils.isEmpty(this.n_default_picking_policy_eq)){
            this.getSearchCond().eq("default_picking_policy", n_default_picking_policy_eq);
        }
    }
	private String n_module_procurement_jit_eq;//[保留]
	public void setN_module_procurement_jit_eq(String n_module_procurement_jit_eq) {
        this.n_module_procurement_jit_eq = n_module_procurement_jit_eq;
        if(!ObjectUtils.isEmpty(this.n_module_procurement_jit_eq)){
            this.getSearchCond().eq("module_procurement_jit", n_module_procurement_jit_eq);
        }
    }
	private String n_show_line_subtotals_tax_selection_eq;//[税目汇总表]
	public void setN_show_line_subtotals_tax_selection_eq(String n_show_line_subtotals_tax_selection_eq) {
        this.n_show_line_subtotals_tax_selection_eq = n_show_line_subtotals_tax_selection_eq;
        if(!ObjectUtils.isEmpty(this.n_show_line_subtotals_tax_selection_eq)){
            this.getSearchCond().eq("show_line_subtotals_tax_selection", n_show_line_subtotals_tax_selection_eq);
        }
    }
	private String n_auth_signup_uninvited_eq;//[顾客账号]
	public void setN_auth_signup_uninvited_eq(String n_auth_signup_uninvited_eq) {
        this.n_auth_signup_uninvited_eq = n_auth_signup_uninvited_eq;
        if(!ObjectUtils.isEmpty(this.n_auth_signup_uninvited_eq)){
            this.getSearchCond().eq("auth_signup_uninvited", n_auth_signup_uninvited_eq);
        }
    }
	private String n_default_purchase_method_eq;//[账单控制]
	public void setN_default_purchase_method_eq(String n_default_purchase_method_eq) {
        this.n_default_purchase_method_eq = n_default_purchase_method_eq;
        if(!ObjectUtils.isEmpty(this.n_default_purchase_method_eq)){
            this.getSearchCond().eq("default_purchase_method", n_default_purchase_method_eq);
        }
    }
	private String n_sale_delivery_settings_eq;//[送货管理]
	public void setN_sale_delivery_settings_eq(String n_sale_delivery_settings_eq) {
        this.n_sale_delivery_settings_eq = n_sale_delivery_settings_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_delivery_settings_eq)){
            this.getSearchCond().eq("sale_delivery_settings", n_sale_delivery_settings_eq);
        }
    }
	private String n_default_invoice_policy_eq;//[开票策略]
	public void setN_default_invoice_policy_eq(String n_default_invoice_policy_eq) {
        this.n_default_invoice_policy_eq = n_default_invoice_policy_eq;
        if(!ObjectUtils.isEmpty(this.n_default_invoice_policy_eq)){
            this.getSearchCond().eq("default_invoice_policy", n_default_invoice_policy_eq);
        }
    }
	private String n_product_weight_in_lbs_eq;//[重量单位]
	public void setN_product_weight_in_lbs_eq(String n_product_weight_in_lbs_eq) {
        this.n_product_weight_in_lbs_eq = n_product_weight_in_lbs_eq;
        if(!ObjectUtils.isEmpty(this.n_product_weight_in_lbs_eq)){
            this.getSearchCond().eq("product_weight_in_lbs", n_product_weight_in_lbs_eq);
        }
    }
	private String n_inventory_availability_eq;//[库存可用性]
	public void setN_inventory_availability_eq(String n_inventory_availability_eq) {
        this.n_inventory_availability_eq = n_inventory_availability_eq;
        if(!ObjectUtils.isEmpty(this.n_inventory_availability_eq)){
            this.getSearchCond().eq("inventory_availability", n_inventory_availability_eq);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_digest_id_text_eq;//[摘要邮件]
	public void setN_digest_id_text_eq(String n_digest_id_text_eq) {
        this.n_digest_id_text_eq = n_digest_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_digest_id_text_eq)){
            this.getSearchCond().eq("digest_id_text", n_digest_id_text_eq);
        }
    }
	private String n_digest_id_text_like;//[摘要邮件]
	public void setN_digest_id_text_like(String n_digest_id_text_like) {
        this.n_digest_id_text_like = n_digest_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_digest_id_text_like)){
            this.getSearchCond().like("digest_id_text", n_digest_id_text_like);
        }
    }
	private String n_auth_signup_template_user_id_text_eq;//[用作通过注册创建的新用户的模版]
	public void setN_auth_signup_template_user_id_text_eq(String n_auth_signup_template_user_id_text_eq) {
        this.n_auth_signup_template_user_id_text_eq = n_auth_signup_template_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_auth_signup_template_user_id_text_eq)){
            this.getSearchCond().eq("auth_signup_template_user_id_text", n_auth_signup_template_user_id_text_eq);
        }
    }
	private String n_auth_signup_template_user_id_text_like;//[用作通过注册创建的新用户的模版]
	public void setN_auth_signup_template_user_id_text_like(String n_auth_signup_template_user_id_text_like) {
        this.n_auth_signup_template_user_id_text_like = n_auth_signup_template_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_auth_signup_template_user_id_text_like)){
            this.getSearchCond().like("auth_signup_template_user_id_text", n_auth_signup_template_user_id_text_like);
        }
    }
	private String n_deposit_default_product_id_text_eq;//[押金产品]
	public void setN_deposit_default_product_id_text_eq(String n_deposit_default_product_id_text_eq) {
        this.n_deposit_default_product_id_text_eq = n_deposit_default_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_deposit_default_product_id_text_eq)){
            this.getSearchCond().eq("deposit_default_product_id_text", n_deposit_default_product_id_text_eq);
        }
    }
	private String n_deposit_default_product_id_text_like;//[押金产品]
	public void setN_deposit_default_product_id_text_like(String n_deposit_default_product_id_text_like) {
        this.n_deposit_default_product_id_text_like = n_deposit_default_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_deposit_default_product_id_text_like)){
            this.getSearchCond().like("deposit_default_product_id_text", n_deposit_default_product_id_text_like);
        }
    }
	private String n_template_id_text_eq;//[EMail模板]
	public void setN_template_id_text_eq(String n_template_id_text_eq) {
        this.n_template_id_text_eq = n_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_template_id_text_eq)){
            this.getSearchCond().eq("template_id_text", n_template_id_text_eq);
        }
    }
	private String n_template_id_text_like;//[EMail模板]
	public void setN_template_id_text_like(String n_template_id_text_like) {
        this.n_template_id_text_like = n_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_template_id_text_like)){
            this.getSearchCond().like("template_id_text", n_template_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_chart_template_id_text_eq;//[模板]
	public void setN_chart_template_id_text_eq(String n_chart_template_id_text_eq) {
        this.n_chart_template_id_text_eq = n_chart_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_chart_template_id_text_eq)){
            this.getSearchCond().eq("chart_template_id_text", n_chart_template_id_text_eq);
        }
    }
	private String n_chart_template_id_text_like;//[模板]
	public void setN_chart_template_id_text_like(String n_chart_template_id_text_like) {
        this.n_chart_template_id_text_like = n_chart_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_chart_template_id_text_like)){
            this.getSearchCond().like("chart_template_id_text", n_chart_template_id_text_like);
        }
    }
	private String n_default_sale_order_template_id_text_eq;//[默认模板]
	public void setN_default_sale_order_template_id_text_eq(String n_default_sale_order_template_id_text_eq) {
        this.n_default_sale_order_template_id_text_eq = n_default_sale_order_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_default_sale_order_template_id_text_eq)){
            this.getSearchCond().eq("default_sale_order_template_id_text", n_default_sale_order_template_id_text_eq);
        }
    }
	private String n_default_sale_order_template_id_text_like;//[默认模板]
	public void setN_default_sale_order_template_id_text_like(String n_default_sale_order_template_id_text_like) {
        this.n_default_sale_order_template_id_text_like = n_default_sale_order_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_default_sale_order_template_id_text_like)){
            this.getSearchCond().like("default_sale_order_template_id_text", n_default_sale_order_template_id_text_like);
        }
    }
	private Long n_default_sale_order_template_id_eq;//[默认模板]
	public void setN_default_sale_order_template_id_eq(Long n_default_sale_order_template_id_eq) {
        this.n_default_sale_order_template_id_eq = n_default_sale_order_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_default_sale_order_template_id_eq)){
            this.getSearchCond().eq("default_sale_order_template_id", n_default_sale_order_template_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_digest_id_eq;//[摘要邮件]
	public void setN_digest_id_eq(Long n_digest_id_eq) {
        this.n_digest_id_eq = n_digest_id_eq;
        if(!ObjectUtils.isEmpty(this.n_digest_id_eq)){
            this.getSearchCond().eq("digest_id", n_digest_id_eq);
        }
    }
	private Long n_template_id_eq;//[EMail模板]
	public void setN_template_id_eq(Long n_template_id_eq) {
        this.n_template_id_eq = n_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_template_id_eq)){
            this.getSearchCond().eq("template_id", n_template_id_eq);
        }
    }
	private Long n_chart_template_id_eq;//[模板]
	public void setN_chart_template_id_eq(Long n_chart_template_id_eq) {
        this.n_chart_template_id_eq = n_chart_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_chart_template_id_eq)){
            this.getSearchCond().eq("chart_template_id", n_chart_template_id_eq);
        }
    }
	private Long n_deposit_default_product_id_eq;//[押金产品]
	public void setN_deposit_default_product_id_eq(Long n_deposit_default_product_id_eq) {
        this.n_deposit_default_product_id_eq = n_deposit_default_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_deposit_default_product_id_eq)){
            this.getSearchCond().eq("deposit_default_product_id", n_deposit_default_product_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_auth_signup_template_user_id_eq;//[用作通过注册创建的新用户的模版]
	public void setN_auth_signup_template_user_id_eq(Long n_auth_signup_template_user_id_eq) {
        this.n_auth_signup_template_user_id_eq = n_auth_signup_template_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_auth_signup_template_user_id_eq)){
            this.getSearchCond().eq("auth_signup_template_user_id", n_auth_signup_template_user_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




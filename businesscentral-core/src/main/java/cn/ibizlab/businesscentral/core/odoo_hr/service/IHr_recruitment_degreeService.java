package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_recruitment_degree] 服务对象接口
 */
public interface IHr_recruitment_degreeService extends IService<Hr_recruitment_degree>{

    boolean create(Hr_recruitment_degree et) ;
    void createBatch(List<Hr_recruitment_degree> list) ;
    boolean update(Hr_recruitment_degree et) ;
    void updateBatch(List<Hr_recruitment_degree> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_recruitment_degree get(Long key) ;
    Hr_recruitment_degree getDraft(Hr_recruitment_degree et) ;
    boolean checkKey(Hr_recruitment_degree et) ;
    boolean save(Hr_recruitment_degree et) ;
    void saveBatch(List<Hr_recruitment_degree> list) ;
    Page<Hr_recruitment_degree> searchDefault(Hr_recruitment_degreeSearchContext context) ;
    List<Hr_recruitment_degree> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_recruitment_degree> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_recruitment_degree> getHrRecruitmentDegreeByIds(List<Long> ids) ;
    List<Hr_recruitment_degree> getHrRecruitmentDegreeByEntities(List<Hr_recruitment_degree> entities) ;
}



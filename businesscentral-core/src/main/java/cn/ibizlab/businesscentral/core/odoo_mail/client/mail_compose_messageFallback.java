package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_compose_message;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_compose_messageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_compose_message] 服务对象接口
 */
@Component
public class mail_compose_messageFallback implements mail_compose_messageFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Mail_compose_message> search(Mail_compose_messageSearchContext context){
            return null;
     }


    public Mail_compose_message update(Long id, Mail_compose_message mail_compose_message){
            return null;
     }
    public Boolean updateBatch(List<Mail_compose_message> mail_compose_messages){
            return false;
     }




    public Mail_compose_message create(Mail_compose_message mail_compose_message){
            return null;
     }
    public Boolean createBatch(List<Mail_compose_message> mail_compose_messages){
            return false;
     }


    public Mail_compose_message get(Long id){
            return null;
     }


    public Page<Mail_compose_message> select(){
            return null;
     }

    public Mail_compose_message getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_compose_message mail_compose_message){
            return false;
     }


    public Boolean save(Mail_compose_message mail_compose_message){
            return false;
     }
    public Boolean saveBatch(List<Mail_compose_message> mail_compose_messages){
            return false;
     }

    public Page<Mail_compose_message> searchDefault(Mail_compose_messageSearchContext context){
            return null;
     }


}

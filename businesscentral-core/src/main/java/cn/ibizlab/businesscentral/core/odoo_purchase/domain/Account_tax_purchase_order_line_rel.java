package cn.ibizlab.businesscentral.core.odoo_purchase.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[采购订单税率]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_TAX_PURCHASE_ORDER_LINE_REL",resultMap = "Account_tax_purchase_order_line_relResultMap")
public class Account_tax_purchase_order_line_rel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @DEField(isKeyField=true)
    @TableField(exist = false)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * ID
     */
    @DEField(name = "account_tax_id")
    @TableField(value = "account_tax_id")
    @JSONField(name = "account_tax_id")
    @JsonProperty("account_tax_id")
    private Long accountTaxId;
    /**
     * ID
     */
    @DEField(name = "purchase_order_line_id")
    @TableField(value = "purchase_order_line_id")
    @JSONField(name = "purchase_order_line_id")
    @JsonProperty("purchase_order_line_id")
    private Long purchaseOrderLineId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooAccountTax;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line odooPurchaseOrderLine;



    /**
     * 设置 [ID]
     */
    public void setAccountTaxId(Long accountTaxId){
        this.accountTaxId = accountTaxId ;
        this.modify("account_tax_id",accountTaxId);
    }

    /**
     * 设置 [ID]
     */
    public void setPurchaseOrderLineId(Long purchaseOrderLineId){
        this.purchaseOrderLineId = purchaseOrderLineId ;
        this.modify("purchase_order_line_id",purchaseOrderLineId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



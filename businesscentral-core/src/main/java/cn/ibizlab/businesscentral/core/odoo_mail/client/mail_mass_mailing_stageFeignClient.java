package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_stage;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_stageSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_mass_mailing_stage] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-mass-mailing-stage", fallback = mail_mass_mailing_stageFallback.class)
public interface mail_mass_mailing_stageFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages")
    Mail_mass_mailing_stage create(@RequestBody Mail_mass_mailing_stage mail_mass_mailing_stage);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/batch")
    Boolean createBatch(@RequestBody List<Mail_mass_mailing_stage> mail_mass_mailing_stages);





    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_stages/{id}")
    Mail_mass_mailing_stage update(@PathVariable("id") Long id,@RequestBody Mail_mass_mailing_stage mail_mass_mailing_stage);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_stages/batch")
    Boolean updateBatch(@RequestBody List<Mail_mass_mailing_stage> mail_mass_mailing_stages);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_stages/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_stages/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_stages/{id}")
    Mail_mass_mailing_stage get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/search")
    Page<Mail_mass_mailing_stage> search(@RequestBody Mail_mass_mailing_stageSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_stages/select")
    Page<Mail_mass_mailing_stage> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_stages/getdraft")
    Mail_mass_mailing_stage getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/checkkey")
    Boolean checkKey(@RequestBody Mail_mass_mailing_stage mail_mass_mailing_stage);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/save")
    Boolean save(@RequestBody Mail_mass_mailing_stage mail_mass_mailing_stage);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_mass_mailing_stage> mail_mass_mailing_stages);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_stages/searchdefault")
    Page<Mail_mass_mailing_stage> searchDefault(@RequestBody Mail_mass_mailing_stageSearchContext context);


}

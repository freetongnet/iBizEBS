package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model;
/**
 * 关系型数据实体[Account_reconcile_model] 查询条件对象
 */
@Slf4j
@Data
public class Account_reconcile_modelSearchContext extends QueryWrapperContext<Account_reconcile_model> {

	private String n_amount_type_eq;//[金额类型]
	public void setN_amount_type_eq(String n_amount_type_eq) {
        this.n_amount_type_eq = n_amount_type_eq;
        if(!ObjectUtils.isEmpty(this.n_amount_type_eq)){
            this.getSearchCond().eq("amount_type", n_amount_type_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_match_nature_eq;//[数量性质]
	public void setN_match_nature_eq(String n_match_nature_eq) {
        this.n_match_nature_eq = n_match_nature_eq;
        if(!ObjectUtils.isEmpty(this.n_match_nature_eq)){
            this.getSearchCond().eq("match_nature", n_match_nature_eq);
        }
    }
	private String n_second_amount_type_eq;//[第二金额类型]
	public void setN_second_amount_type_eq(String n_second_amount_type_eq) {
        this.n_second_amount_type_eq = n_second_amount_type_eq;
        if(!ObjectUtils.isEmpty(this.n_second_amount_type_eq)){
            this.getSearchCond().eq("second_amount_type", n_second_amount_type_eq);
        }
    }
	private String n_match_label_eq;//[标签]
	public void setN_match_label_eq(String n_match_label_eq) {
        this.n_match_label_eq = n_match_label_eq;
        if(!ObjectUtils.isEmpty(this.n_match_label_eq)){
            this.getSearchCond().eq("match_label", n_match_label_eq);
        }
    }
	private String n_match_amount_eq;//[金额]
	public void setN_match_amount_eq(String n_match_amount_eq) {
        this.n_match_amount_eq = n_match_amount_eq;
        if(!ObjectUtils.isEmpty(this.n_match_amount_eq)){
            this.getSearchCond().eq("match_amount", n_match_amount_eq);
        }
    }
	private String n_rule_type_eq;//[类型]
	public void setN_rule_type_eq(String n_rule_type_eq) {
        this.n_rule_type_eq = n_rule_type_eq;
        if(!ObjectUtils.isEmpty(this.n_rule_type_eq)){
            this.getSearchCond().eq("rule_type", n_rule_type_eq);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_second_analytic_account_id_text_eq;//[第二分析帐户]
	public void setN_second_analytic_account_id_text_eq(String n_second_analytic_account_id_text_eq) {
        this.n_second_analytic_account_id_text_eq = n_second_analytic_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_second_analytic_account_id_text_eq)){
            this.getSearchCond().eq("second_analytic_account_id_text", n_second_analytic_account_id_text_eq);
        }
    }
	private String n_second_analytic_account_id_text_like;//[第二分析帐户]
	public void setN_second_analytic_account_id_text_like(String n_second_analytic_account_id_text_like) {
        this.n_second_analytic_account_id_text_like = n_second_analytic_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_second_analytic_account_id_text_like)){
            this.getSearchCond().like("second_analytic_account_id_text", n_second_analytic_account_id_text_like);
        }
    }
	private String n_second_tax_id_text_eq;//[第二个税]
	public void setN_second_tax_id_text_eq(String n_second_tax_id_text_eq) {
        this.n_second_tax_id_text_eq = n_second_tax_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_second_tax_id_text_eq)){
            this.getSearchCond().eq("second_tax_id_text", n_second_tax_id_text_eq);
        }
    }
	private String n_second_tax_id_text_like;//[第二个税]
	public void setN_second_tax_id_text_like(String n_second_tax_id_text_like) {
        this.n_second_tax_id_text_like = n_second_tax_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_second_tax_id_text_like)){
            this.getSearchCond().like("second_tax_id_text", n_second_tax_id_text_like);
        }
    }
	private String n_journal_id_text_eq;//[日记账]
	public void setN_journal_id_text_eq(String n_journal_id_text_eq) {
        this.n_journal_id_text_eq = n_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_eq)){
            this.getSearchCond().eq("journal_id_text", n_journal_id_text_eq);
        }
    }
	private String n_journal_id_text_like;//[日记账]
	public void setN_journal_id_text_like(String n_journal_id_text_like) {
        this.n_journal_id_text_like = n_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_journal_id_text_like)){
            this.getSearchCond().like("journal_id_text", n_journal_id_text_like);
        }
    }
	private String n_second_journal_id_text_eq;//[第二个分录]
	public void setN_second_journal_id_text_eq(String n_second_journal_id_text_eq) {
        this.n_second_journal_id_text_eq = n_second_journal_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_second_journal_id_text_eq)){
            this.getSearchCond().eq("second_journal_id_text", n_second_journal_id_text_eq);
        }
    }
	private String n_second_journal_id_text_like;//[第二个分录]
	public void setN_second_journal_id_text_like(String n_second_journal_id_text_like) {
        this.n_second_journal_id_text_like = n_second_journal_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_second_journal_id_text_like)){
            this.getSearchCond().like("second_journal_id_text", n_second_journal_id_text_like);
        }
    }
	private String n_second_account_id_text_eq;//[第二科目]
	public void setN_second_account_id_text_eq(String n_second_account_id_text_eq) {
        this.n_second_account_id_text_eq = n_second_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_second_account_id_text_eq)){
            this.getSearchCond().eq("second_account_id_text", n_second_account_id_text_eq);
        }
    }
	private String n_second_account_id_text_like;//[第二科目]
	public void setN_second_account_id_text_like(String n_second_account_id_text_like) {
        this.n_second_account_id_text_like = n_second_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_second_account_id_text_like)){
            this.getSearchCond().like("second_account_id_text", n_second_account_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_analytic_account_id_text_eq;//[分析账户]
	public void setN_analytic_account_id_text_eq(String n_analytic_account_id_text_eq) {
        this.n_analytic_account_id_text_eq = n_analytic_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_eq)){
            this.getSearchCond().eq("analytic_account_id_text", n_analytic_account_id_text_eq);
        }
    }
	private String n_analytic_account_id_text_like;//[分析账户]
	public void setN_analytic_account_id_text_like(String n_analytic_account_id_text_like) {
        this.n_analytic_account_id_text_like = n_analytic_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_text_like)){
            this.getSearchCond().like("analytic_account_id_text", n_analytic_account_id_text_like);
        }
    }
	private String n_account_id_text_eq;//[科目]
	public void setN_account_id_text_eq(String n_account_id_text_eq) {
        this.n_account_id_text_eq = n_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_eq)){
            this.getSearchCond().eq("account_id_text", n_account_id_text_eq);
        }
    }
	private String n_account_id_text_like;//[科目]
	public void setN_account_id_text_like(String n_account_id_text_like) {
        this.n_account_id_text_like = n_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_account_id_text_like)){
            this.getSearchCond().like("account_id_text", n_account_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_tax_id_text_eq;//[税率]
	public void setN_tax_id_text_eq(String n_tax_id_text_eq) {
        this.n_tax_id_text_eq = n_tax_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_id_text_eq)){
            this.getSearchCond().eq("tax_id_text", n_tax_id_text_eq);
        }
    }
	private String n_tax_id_text_like;//[税率]
	public void setN_tax_id_text_like(String n_tax_id_text_like) {
        this.n_tax_id_text_like = n_tax_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_tax_id_text_like)){
            this.getSearchCond().like("tax_id_text", n_tax_id_text_like);
        }
    }
	private Long n_second_analytic_account_id_eq;//[第二分析帐户]
	public void setN_second_analytic_account_id_eq(Long n_second_analytic_account_id_eq) {
        this.n_second_analytic_account_id_eq = n_second_analytic_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_second_analytic_account_id_eq)){
            this.getSearchCond().eq("second_analytic_account_id", n_second_analytic_account_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_tax_id_eq;//[税率]
	public void setN_tax_id_eq(Long n_tax_id_eq) {
        this.n_tax_id_eq = n_tax_id_eq;
        if(!ObjectUtils.isEmpty(this.n_tax_id_eq)){
            this.getSearchCond().eq("tax_id", n_tax_id_eq);
        }
    }
	private Long n_second_account_id_eq;//[第二科目]
	public void setN_second_account_id_eq(Long n_second_account_id_eq) {
        this.n_second_account_id_eq = n_second_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_second_account_id_eq)){
            this.getSearchCond().eq("second_account_id", n_second_account_id_eq);
        }
    }
	private Long n_second_journal_id_eq;//[第二个分录]
	public void setN_second_journal_id_eq(Long n_second_journal_id_eq) {
        this.n_second_journal_id_eq = n_second_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_second_journal_id_eq)){
            this.getSearchCond().eq("second_journal_id", n_second_journal_id_eq);
        }
    }
	private Long n_analytic_account_id_eq;//[分析账户]
	public void setN_analytic_account_id_eq(Long n_analytic_account_id_eq) {
        this.n_analytic_account_id_eq = n_analytic_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_analytic_account_id_eq)){
            this.getSearchCond().eq("analytic_account_id", n_analytic_account_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_second_tax_id_eq;//[第二个税]
	public void setN_second_tax_id_eq(Long n_second_tax_id_eq) {
        this.n_second_tax_id_eq = n_second_tax_id_eq;
        if(!ObjectUtils.isEmpty(this.n_second_tax_id_eq)){
            this.getSearchCond().eq("second_tax_id", n_second_tax_id_eq);
        }
    }
	private Long n_account_id_eq;//[科目]
	public void setN_account_id_eq(Long n_account_id_eq) {
        this.n_account_id_eq = n_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_account_id_eq)){
            this.getSearchCond().eq("account_id", n_account_id_eq);
        }
    }
	private Long n_journal_id_eq;//[日记账]
	public void setN_journal_id_eq(Long n_journal_id_eq) {
        this.n_journal_id_eq = n_journal_id_eq;
        if(!ObjectUtils.isEmpty(this.n_journal_id_eq)){
            this.getSearchCond().eq("journal_id", n_journal_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




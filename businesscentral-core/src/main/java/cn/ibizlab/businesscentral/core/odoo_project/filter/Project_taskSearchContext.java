package cn.ibizlab.businesscentral.core.odoo_project.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task;
/**
 * 关系型数据实体[Project_task] 查询条件对象
 */
@Slf4j
@Data
public class Project_taskSearchContext extends QueryWrapperContext<Project_task> {

	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_kanban_state_eq;//[看板状态]
	public void setN_kanban_state_eq(String n_kanban_state_eq) {
        this.n_kanban_state_eq = n_kanban_state_eq;
        if(!ObjectUtils.isEmpty(this.n_kanban_state_eq)){
            this.getSearchCond().eq("kanban_state", n_kanban_state_eq);
        }
    }
	private String n_priority_eq;//[优先级]
	public void setN_priority_eq(String n_priority_eq) {
        this.n_priority_eq = n_priority_eq;
        if(!ObjectUtils.isEmpty(this.n_priority_eq)){
            this.getSearchCond().eq("priority", n_priority_eq);
        }
    }
	private String n_name_like;//[称谓]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[客户]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[客户]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_project_id_text_eq;//[项目]
	public void setN_project_id_text_eq(String n_project_id_text_eq) {
        this.n_project_id_text_eq = n_project_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_project_id_text_eq)){
            this.getSearchCond().eq("project_id_text", n_project_id_text_eq);
        }
    }
	private String n_project_id_text_like;//[项目]
	public void setN_project_id_text_like(String n_project_id_text_like) {
        this.n_project_id_text_like = n_project_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_project_id_text_like)){
            this.getSearchCond().like("project_id_text", n_project_id_text_like);
        }
    }
	private String n_parent_id_text_eq;//[上级任务]
	public void setN_parent_id_text_eq(String n_parent_id_text_eq) {
        this.n_parent_id_text_eq = n_parent_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_eq)){
            this.getSearchCond().eq("parent_id_text", n_parent_id_text_eq);
        }
    }
	private String n_parent_id_text_like;//[上级任务]
	public void setN_parent_id_text_like(String n_parent_id_text_like) {
        this.n_parent_id_text_like = n_parent_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_like)){
            this.getSearchCond().like("parent_id_text", n_parent_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[分派给]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[分派给]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_stage_id_text_eq;//[阶段]
	public void setN_stage_id_text_eq(String n_stage_id_text_eq) {
        this.n_stage_id_text_eq = n_stage_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_stage_id_text_eq)){
            this.getSearchCond().eq("stage_id_text", n_stage_id_text_eq);
        }
    }
	private String n_stage_id_text_like;//[阶段]
	public void setN_stage_id_text_like(String n_stage_id_text_like) {
        this.n_stage_id_text_like = n_stage_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_stage_id_text_like)){
            this.getSearchCond().like("stage_id_text", n_stage_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_user_id_eq;//[分派给]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_project_id_eq;//[项目]
	public void setN_project_id_eq(Long n_project_id_eq) {
        this.n_project_id_eq = n_project_id_eq;
        if(!ObjectUtils.isEmpty(this.n_project_id_eq)){
            this.getSearchCond().eq("project_id", n_project_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_partner_id_eq;//[客户]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_parent_id_eq;//[上级任务]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_stage_id_eq;//[阶段]
	public void setN_stage_id_eq(Long n_stage_id_eq) {
        this.n_stage_id_eq = n_stage_id_eq;
        if(!ObjectUtils.isEmpty(this.n_stage_id_eq)){
            this.getSearchCond().eq("stage_id", n_stage_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




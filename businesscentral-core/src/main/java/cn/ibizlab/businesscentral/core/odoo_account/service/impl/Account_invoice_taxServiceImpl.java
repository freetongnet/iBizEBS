package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_taxSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_taxService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_invoice_taxMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[发票税率] 服务对象接口实现
 */
@Slf4j
@Service("Account_invoice_taxServiceImpl")
public class Account_invoice_taxServiceImpl extends EBSServiceImpl<Account_invoice_taxMapper, Account_invoice_tax> implements IAccount_invoice_taxService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_taxService accountTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.invoice.tax" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_invoice_tax et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_taxService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_invoice_tax> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_invoice_tax et) {
        Account_invoice_tax old = new Account_invoice_tax() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_taxService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_taxService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_invoice_tax> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_invoice_tax get(Long key) {
        Account_invoice_tax et = getById(key);
        if(et==null){
            et=new Account_invoice_tax();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_invoice_tax getDraft(Account_invoice_tax et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_invoice_tax et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_invoice_tax et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_invoice_tax et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_invoice_tax> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_invoice_tax> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_invoice_tax> selectByAccountId(Long id) {
        return baseMapper.selectByAccountId(id);
    }
    @Override
    public void resetByAccountId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_tax>().set("account_id",null).eq("account_id",id));
    }

    @Override
    public void resetByAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_tax>().set("account_id",null).in("account_id",ids));
    }

    @Override
    public void removeByAccountId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_tax>().eq("account_id",id));
    }

	@Override
    public List<Account_invoice_tax> selectByAccountAnalyticId(Long id) {
        return baseMapper.selectByAccountAnalyticId(id);
    }
    @Override
    public void resetByAccountAnalyticId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_tax>().set("account_analytic_id",null).eq("account_analytic_id",id));
    }

    @Override
    public void resetByAccountAnalyticId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_tax>().set("account_analytic_id",null).in("account_analytic_id",ids));
    }

    @Override
    public void removeByAccountAnalyticId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_tax>().eq("account_analytic_id",id));
    }

	@Override
    public List<Account_invoice_tax> selectByInvoiceId(Long id) {
        return baseMapper.selectByInvoiceId(id);
    }
    @Override
    public void removeByInvoiceId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_invoice_tax>().in("invoice_id",ids));
    }

    @Override
    public void removeByInvoiceId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_tax>().eq("invoice_id",id));
    }

	@Override
    public List<Account_invoice_tax> selectByTaxId(Long id) {
        return baseMapper.selectByTaxId(id);
    }
    @Override
    public List<Account_invoice_tax> selectByTaxId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_invoice_tax>().in("id",ids));
    }

    @Override
    public void removeByTaxId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_tax>().eq("tax_id",id));
    }

	@Override
    public List<Account_invoice_tax> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_tax>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_tax>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_tax>().eq("company_id",id));
    }

	@Override
    public List<Account_invoice_tax> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_tax>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_tax>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_tax>().eq("currency_id",id));
    }

	@Override
    public List<Account_invoice_tax> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_invoice_tax>().eq("create_uid",id));
    }

	@Override
    public List<Account_invoice_tax> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_invoice_tax>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_invoice_tax> searchDefault(Account_invoice_taxSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_invoice_tax> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_invoice_tax>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_invoice_tax et){
        //实体关系[DER1N_ACCOUNT_INVOICE_TAX__ACCOUNT_ACCOUNT__ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount=et.getOdooAccount();
            if(ObjectUtils.isEmpty(odooAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountId());
                et.setOdooAccount(majorEntity);
                odooAccount=majorEntity;
            }
            et.setAccountIdText(odooAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_TAX__ACCOUNT_ANALYTIC_ACCOUNT__ACCOUNT_ANALYTIC_ID]
        if(!ObjectUtils.isEmpty(et.getAccountAnalyticId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic=et.getOdooAccountAnalytic();
            if(ObjectUtils.isEmpty(odooAccountAnalytic)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAccountAnalyticId());
                et.setOdooAccountAnalytic(majorEntity);
                odooAccountAnalytic=majorEntity;
            }
            et.setAccountAnalyticIdText(odooAccountAnalytic.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_TAX__ACCOUNT_INVOICE__INVOICE_ID]
        if(!ObjectUtils.isEmpty(et.getInvoiceId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooInvoice=et.getOdooInvoice();
            if(ObjectUtils.isEmpty(odooInvoice)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice majorEntity=accountInvoiceService.get(et.getInvoiceId());
                et.setOdooInvoice(majorEntity);
                odooInvoice=majorEntity;
            }
            et.setInvoiceIdText(odooInvoice.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_TAX__ACCOUNT_TAX__TAX_ID]
        if(!ObjectUtils.isEmpty(et.getTaxId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooTax=et.getOdooTax();
            if(ObjectUtils.isEmpty(odooTax)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax majorEntity=accountTaxService.get(et.getTaxId());
                et.setOdooTax(majorEntity);
                odooTax=majorEntity;
            }
            et.setTaxIdText(odooTax.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_TAX__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_TAX__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_TAX__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_TAX__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_invoice_tax> getAccountInvoiceTaxByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_invoice_tax> getAccountInvoiceTaxByEntities(List<Account_invoice_tax> entities) {
        List ids =new ArrayList();
        for(Account_invoice_tax entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




package cn.ibizlab.businesscentral.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_eventSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Calendar_event] 服务对象接口
 */
public interface ICalendar_eventService extends IService<Calendar_event>{

    boolean create(Calendar_event et) ;
    void createBatch(List<Calendar_event> list) ;
    boolean update(Calendar_event et) ;
    void updateBatch(List<Calendar_event> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Calendar_event get(Long key) ;
    Calendar_event getDraft(Calendar_event et) ;
    boolean checkKey(Calendar_event et) ;
    boolean save(Calendar_event et) ;
    void saveBatch(List<Calendar_event> list) ;
    Page<Calendar_event> searchDefault(Calendar_eventSearchContext context) ;
    List<Calendar_event> selectByOpportunityId(Long id);
    void resetByOpportunityId(Long id);
    void resetByOpportunityId(Collection<Long> ids);
    void removeByOpportunityId(Long id);
    List<Calendar_event> selectByApplicantId(Long id);
    void resetByApplicantId(Long id);
    void resetByApplicantId(Collection<Long> ids);
    void removeByApplicantId(Long id);
    List<Calendar_event> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Calendar_event> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Calendar_event> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Calendar_event> getCalendarEventByIds(List<Long> ids) ;
    List<Calendar_event> getCalendarEventByEntities(List<Calendar_event> entities) ;
}



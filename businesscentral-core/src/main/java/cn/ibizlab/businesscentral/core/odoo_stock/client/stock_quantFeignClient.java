package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quantSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_quant] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-quant", fallback = stock_quantFallback.class)
public interface stock_quantFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_quants/{id}")
    Stock_quant update(@PathVariable("id") Long id,@RequestBody Stock_quant stock_quant);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_quants/batch")
    Boolean updateBatch(@RequestBody List<Stock_quant> stock_quants);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_quants/search")
    Page<Stock_quant> search(@RequestBody Stock_quantSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_quants/{id}")
    Stock_quant get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_quants")
    Stock_quant create(@RequestBody Stock_quant stock_quant);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_quants/batch")
    Boolean createBatch(@RequestBody List<Stock_quant> stock_quants);



    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_quants/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_quants/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_quants/select")
    Page<Stock_quant> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_quants/getdraft")
    Stock_quant getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_quants/checkkey")
    Boolean checkKey(@RequestBody Stock_quant stock_quant);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_quants/save")
    Boolean save(@RequestBody Stock_quant stock_quant);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_quants/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_quant> stock_quants);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_quants/searchdefault")
    Page<Stock_quant> searchDefault(@RequestBody Stock_quantSearchContext context);


}

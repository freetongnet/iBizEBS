package cn.ibizlab.businesscentral.core.odoo_sms.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.businesscentral.core.odoo_sms.filter.Sms_send_smsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sms_send_sms] 服务对象接口
 */
public interface ISms_send_smsService extends IService<Sms_send_sms>{

    boolean create(Sms_send_sms et) ;
    void createBatch(List<Sms_send_sms> list) ;
    boolean update(Sms_send_sms et) ;
    void updateBatch(List<Sms_send_sms> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sms_send_sms get(Long key) ;
    Sms_send_sms getDraft(Sms_send_sms et) ;
    boolean checkKey(Sms_send_sms et) ;
    boolean save(Sms_send_sms et) ;
    void saveBatch(List<Sms_send_sms> list) ;
    Page<Sms_send_sms> searchDefault(Sms_send_smsSearchContext context) ;
    List<Sms_send_sms> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Sms_send_sms> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sms_send_sms> getSmsSendSmsByIds(List<Long> ids) ;
    List<Sms_send_sms> getSmsSendSmsByEntities(List<Sms_send_sms> entities) ;
}



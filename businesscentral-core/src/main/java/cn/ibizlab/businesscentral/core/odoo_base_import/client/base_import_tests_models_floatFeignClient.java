package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_float;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_floatSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_float] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base-import:odoo-base-import}", contextId = "base-import-tests-models-float", fallback = base_import_tests_models_floatFallback.class)
public interface base_import_tests_models_floatFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats")
    Base_import_tests_models_float create(@RequestBody Base_import_tests_models_float base_import_tests_models_float);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_float> base_import_tests_models_floats);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_floats/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_floats/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_floats/{id}")
    Base_import_tests_models_float get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_floats/{id}")
    Base_import_tests_models_float update(@PathVariable("id") Long id,@RequestBody Base_import_tests_models_float base_import_tests_models_float);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_floats/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_float> base_import_tests_models_floats);




    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/search")
    Page<Base_import_tests_models_float> search(@RequestBody Base_import_tests_models_floatSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_floats/select")
    Page<Base_import_tests_models_float> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_floats/getdraft")
    Base_import_tests_models_float getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/checkkey")
    Boolean checkKey(@RequestBody Base_import_tests_models_float base_import_tests_models_float);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/save")
    Boolean save(@RequestBody Base_import_tests_models_float base_import_tests_models_float);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/savebatch")
    Boolean saveBatch(@RequestBody List<Base_import_tests_models_float> base_import_tests_models_floats);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/searchdefault")
    Page<Base_import_tests_models_float> searchDefault(@RequestBody Base_import_tests_models_floatSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_usersSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_users] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-users", fallback = res_usersFallback.class)
public interface res_usersFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/res_users/{id}")
    Res_users get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/res_users/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_users/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/res_users")
    Res_users create(@RequestBody Res_users res_users);

    @RequestMapping(method = RequestMethod.POST, value = "/res_users/batch")
    Boolean createBatch(@RequestBody List<Res_users> res_users);




    @RequestMapping(method = RequestMethod.PUT, value = "/res_users/{id}")
    Res_users update(@PathVariable("id") Long id,@RequestBody Res_users res_users);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_users/batch")
    Boolean updateBatch(@RequestBody List<Res_users> res_users);



    @RequestMapping(method = RequestMethod.POST, value = "/res_users/search")
    Page<Res_users> search(@RequestBody Res_usersSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/res_users/select")
    Page<Res_users> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_users/getdraft")
    Res_users getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_users/{id}/a")
    Res_users a(@PathVariable("id") Long id,@RequestBody Res_users res_users);


    @RequestMapping(method = RequestMethod.POST, value = "/res_users/checkkey")
    Boolean checkKey(@RequestBody Res_users res_users);


    @RequestMapping(method = RequestMethod.POST, value = "/res_users/save")
    Boolean save(@RequestBody Res_users res_users);

    @RequestMapping(method = RequestMethod.POST, value = "/res_users/savebatch")
    Boolean saveBatch(@RequestBody List<Res_users> res_users);



    @RequestMapping(method = RequestMethod.POST, value = "/res_users/searchactive")
    Page<Res_users> searchActive(@RequestBody Res_usersSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/res_users/searchdefault")
    Page<Res_users> searchDefault(@RequestBody Res_usersSearchContext context);


}

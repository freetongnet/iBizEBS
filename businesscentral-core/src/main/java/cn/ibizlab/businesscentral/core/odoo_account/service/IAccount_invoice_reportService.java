package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_reportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_invoice_report] 服务对象接口
 */
public interface IAccount_invoice_reportService extends IService<Account_invoice_report>{

    boolean create(Account_invoice_report et) ;
    void createBatch(List<Account_invoice_report> list) ;
    boolean update(Account_invoice_report et) ;
    void updateBatch(List<Account_invoice_report> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_invoice_report get(Long key) ;
    Account_invoice_report getDraft(Account_invoice_report et) ;
    boolean checkKey(Account_invoice_report et) ;
    boolean save(Account_invoice_report et) ;
    void saveBatch(List<Account_invoice_report> list) ;
    Page<Account_invoice_report> searchDefault(Account_invoice_reportSearchContext context) ;
    List<Account_invoice_report> selectByAccountId(Long id);
    void removeByAccountId(Long id);
    List<Account_invoice_report> selectByAccountLineId(Long id);
    void removeByAccountLineId(Long id);
    List<Account_invoice_report> selectByAccountAnalyticId(Long id);
    void removeByAccountAnalyticId(Long id);
    List<Account_invoice_report> selectByFiscalPositionId(Long id);
    void removeByFiscalPositionId(Long id);
    List<Account_invoice_report> selectByInvoiceId(Long id);
    void removeByInvoiceId(Long id);
    List<Account_invoice_report> selectByJournalId(Long id);
    void removeByJournalId(Long id);
    List<Account_invoice_report> selectByPaymentTermId(Long id);
    void removeByPaymentTermId(Long id);
    List<Account_invoice_report> selectByTeamId(Long id);
    void removeByTeamId(Long id);
    List<Account_invoice_report> selectByCategId(Long id);
    void removeByCategId(Long id);
    List<Account_invoice_report> selectByProductId(Long id);
    void removeByProductId(Long id);
    List<Account_invoice_report> selectByCompanyId(Long id);
    void removeByCompanyId(Long id);
    List<Account_invoice_report> selectByCountryId(Long id);
    void removeByCountryId(Long id);
    List<Account_invoice_report> selectByCurrencyId(Long id);
    void removeByCurrencyId(Long id);
    List<Account_invoice_report> selectByPartnerBankId(Long id);
    void removeByPartnerBankId(Long id);
    List<Account_invoice_report> selectByCommercialPartnerId(Long id);
    void removeByCommercialPartnerId(Long id);
    List<Account_invoice_report> selectByPartnerId(Long id);
    void removeByPartnerId(Long id);
    List<Account_invoice_report> selectByUserId(Long id);
    void removeByUserId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



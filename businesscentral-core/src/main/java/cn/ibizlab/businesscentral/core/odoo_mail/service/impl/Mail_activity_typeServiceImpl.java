package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_activity_typeSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activity_typeService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_activity_typeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[活动类型] 服务对象接口实现
 */
@Slf4j
@Service("Mail_activity_typeServiceImpl")
public class Mail_activity_typeServiceImpl extends EBSServiceImpl<Mail_activity_typeMapper, Mail_activity_type> implements IMail_activity_typeService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_activity_reportService crmActivityReportService;

    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activity_typeService mailActivityTypeService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_activityService mailActivityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_compose_messageService mailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService mailMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_mail_compose_messageService surveyMailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.activity.type" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_activity_type et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_activity_typeService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_activity_type> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_activity_type et) {
        Mail_activity_type old = new Mail_activity_type() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_activity_typeService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_activity_typeService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_activity_type> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        mailActivityTypeService.resetByDefaultNextTypeId(key);
        if(!ObjectUtils.isEmpty(mailActivityService.selectByActivityTypeId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mail_activity]数据，无法删除!","","");
        mailActivityService.resetByPreviousActivityTypeId(key);
        mailActivityService.resetByRecommendedActivityTypeId(key);
        mailComposeMessageService.resetByMailActivityTypeId(key);
        mailMessageService.resetByMailActivityTypeId(key);
        surveyMailComposeMessageService.resetByMailActivityTypeId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        mailActivityTypeService.resetByDefaultNextTypeId(idList);
        if(!ObjectUtils.isEmpty(mailActivityService.selectByActivityTypeId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mail_activity]数据，无法删除!","","");
        mailActivityService.resetByPreviousActivityTypeId(idList);
        mailActivityService.resetByRecommendedActivityTypeId(idList);
        mailComposeMessageService.resetByMailActivityTypeId(idList);
        mailMessageService.resetByMailActivityTypeId(idList);
        surveyMailComposeMessageService.resetByMailActivityTypeId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_activity_type get(Long key) {
        Mail_activity_type et = getById(key);
        if(et==null){
            et=new Mail_activity_type();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_activity_type getDraft(Mail_activity_type et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mail_activity_type et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_activity_type et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_activity_type et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_activity_type> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_activity_type> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_activity_type> selectByDefaultNextTypeId(Long id) {
        return baseMapper.selectByDefaultNextTypeId(id);
    }
    @Override
    public void resetByDefaultNextTypeId(Long id) {
        this.update(new UpdateWrapper<Mail_activity_type>().set("default_next_type_id",null).eq("default_next_type_id",id));
    }

    @Override
    public void resetByDefaultNextTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_activity_type>().set("default_next_type_id",null).in("default_next_type_id",ids));
    }

    @Override
    public void removeByDefaultNextTypeId(Long id) {
        this.remove(new QueryWrapper<Mail_activity_type>().eq("default_next_type_id",id));
    }

	@Override
    public List<Mail_activity_type> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mail_activity_type>().eq("create_uid",id));
    }

	@Override
    public List<Mail_activity_type> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mail_activity_type>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_activity_type> searchDefault(Mail_activity_typeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_activity_type> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_activity_type>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mail_activity_type et){
        //实体关系[DER1N_MAIL_ACTIVITY_TYPE__MAIL_ACTIVITY_TYPE__DEFAULT_NEXT_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getDefaultNextTypeId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type odooDefaultNextType=et.getOdooDefaultNextType();
            if(ObjectUtils.isEmpty(odooDefaultNextType)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity_type majorEntity=mailActivityTypeService.get(et.getDefaultNextTypeId());
                et.setOdooDefaultNextType(majorEntity);
                odooDefaultNextType=majorEntity;
            }
            et.setDefaultNextTypeIdText(odooDefaultNextType.getName());
        }
        //实体关系[DER1N_MAIL_ACTIVITY_TYPE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAIL_ACTIVITY_TYPE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mail_activity_type> getMailActivityTypeByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mail_activity_type> getMailActivityTypeByEntities(List<Mail_activity_type> entities) {
        List ids =new ArrayList();
        for(Mail_activity_type entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




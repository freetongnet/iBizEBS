package cn.ibizlab.businesscentral.core.odoo_resource.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_mixin;
/**
 * 关系型数据实体[Resource_mixin] 查询条件对象
 */
@Slf4j
@Data
public class Resource_mixinSearchContext extends QueryWrapperContext<Resource_mixin> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_resource_id_text_eq;//[资源]
	public void setN_resource_id_text_eq(String n_resource_id_text_eq) {
        this.n_resource_id_text_eq = n_resource_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_id_text_eq)){
            this.getSearchCond().eq("resource_id_text", n_resource_id_text_eq);
        }
    }
	private String n_resource_id_text_like;//[资源]
	public void setN_resource_id_text_like(String n_resource_id_text_like) {
        this.n_resource_id_text_like = n_resource_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_resource_id_text_like)){
            this.getSearchCond().like("resource_id_text", n_resource_id_text_like);
        }
    }
	private String n_resource_calendar_id_text_eq;//[工作时间]
	public void setN_resource_calendar_id_text_eq(String n_resource_calendar_id_text_eq) {
        this.n_resource_calendar_id_text_eq = n_resource_calendar_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_text_eq)){
            this.getSearchCond().eq("resource_calendar_id_text", n_resource_calendar_id_text_eq);
        }
    }
	private String n_resource_calendar_id_text_like;//[工作时间]
	public void setN_resource_calendar_id_text_like(String n_resource_calendar_id_text_like) {
        this.n_resource_calendar_id_text_like = n_resource_calendar_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_text_like)){
            this.getSearchCond().like("resource_calendar_id_text", n_resource_calendar_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private Long n_resource_calendar_id_eq;//[工作时间]
	public void setN_resource_calendar_id_eq(Long n_resource_calendar_id_eq) {
        this.n_resource_calendar_id_eq = n_resource_calendar_id_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_calendar_id_eq)){
            this.getSearchCond().eq("resource_calendar_id", n_resource_calendar_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_resource_id_eq;//[资源]
	public void setN_resource_id_eq(Long n_resource_id_eq) {
        this.n_resource_id_eq = n_resource_id_eq;
        if(!ObjectUtils.isEmpty(this.n_resource_id_eq)){
            this.getSearchCond().eq("resource_id", n_resource_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




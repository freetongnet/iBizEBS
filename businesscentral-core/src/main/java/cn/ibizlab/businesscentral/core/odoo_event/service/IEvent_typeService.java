package cn.ibizlab.businesscentral.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_typeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Event_type] 服务对象接口
 */
public interface IEvent_typeService extends IService<Event_type>{

    boolean create(Event_type et) ;
    void createBatch(List<Event_type> list) ;
    boolean update(Event_type et) ;
    void updateBatch(List<Event_type> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Event_type get(Long key) ;
    Event_type getDraft(Event_type et) ;
    boolean checkKey(Event_type et) ;
    boolean save(Event_type et) ;
    void saveBatch(List<Event_type> list) ;
    Page<Event_type> searchDefault(Event_typeSearchContext context) ;
    List<Event_type> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Event_type> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Event_type> getEventTypeByIds(List<Long> ids) ;
    List<Event_type> getEventTypeByEntities(List<Event_type> entities) ;
}



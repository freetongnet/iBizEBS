package cn.ibizlab.businesscentral.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_requisitionSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Purchase_requisition] 服务对象接口
 */
public interface IPurchase_requisitionService extends IService<Purchase_requisition>{

    boolean create(Purchase_requisition et) ;
    void createBatch(List<Purchase_requisition> list) ;
    boolean update(Purchase_requisition et) ;
    void updateBatch(List<Purchase_requisition> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Purchase_requisition get(Long key) ;
    Purchase_requisition getDraft(Purchase_requisition et) ;
    Purchase_requisition action_cancel(Purchase_requisition et) ;
    Purchase_requisition action_done(Purchase_requisition et) ;
    Purchase_requisition action_draft(Purchase_requisition et) ;
    Purchase_requisition action_in_progress(Purchase_requisition et) ;
    Purchase_requisition action_open(Purchase_requisition et) ;
    boolean checkKey(Purchase_requisition et) ;
    Purchase_requisition masterTabCount(Purchase_requisition et) ;
    boolean save(Purchase_requisition et) ;
    void saveBatch(List<Purchase_requisition> list) ;
    Page<Purchase_requisition> searchDefault(Purchase_requisitionSearchContext context) ;
    Page<Purchase_requisition> searchMaster(Purchase_requisitionSearchContext context) ;
    List<Purchase_requisition> selectByTypeId(Long id);
    void removeByTypeId(Long id);
    List<Purchase_requisition> selectByCompanyId(Long id);
    void removeByCompanyId(Long id);
    List<Purchase_requisition> selectByCurrencyId(Long id);
    void removeByCurrencyId(Long id);
    List<Purchase_requisition> selectByVendorId(Long id);
    void removeByVendorId(Long id);
    List<Purchase_requisition> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Purchase_requisition> selectByUserId(Long id);
    void removeByUserId(Long id);
    List<Purchase_requisition> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Purchase_requisition> selectByPickingTypeId(Long id);
    void removeByPickingTypeId(Long id);
    List<Purchase_requisition> selectByWarehouseId(Long id);
    void removeByWarehouseId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Purchase_requisition> getPurchaseRequisitionByIds(List<Long> ids) ;
    List<Purchase_requisition> getPurchaseRequisitionByEntities(List<Purchase_requisition> entities) ;
}



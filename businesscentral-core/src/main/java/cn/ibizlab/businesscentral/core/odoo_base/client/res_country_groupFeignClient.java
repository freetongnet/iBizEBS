package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_group;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_country_groupSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_country_group] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-country-group", fallback = res_country_groupFallback.class)
public interface res_country_groupFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/res_country_groups/{id}")
    Res_country_group get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_country_groups/{id}")
    Res_country_group update(@PathVariable("id") Long id,@RequestBody Res_country_group res_country_group);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_country_groups/batch")
    Boolean updateBatch(@RequestBody List<Res_country_group> res_country_groups);




    @RequestMapping(method = RequestMethod.DELETE, value = "/res_country_groups/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_country_groups/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/search")
    Page<Res_country_group> search(@RequestBody Res_country_groupSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/res_country_groups")
    Res_country_group create(@RequestBody Res_country_group res_country_group);

    @RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/batch")
    Boolean createBatch(@RequestBody List<Res_country_group> res_country_groups);


    @RequestMapping(method = RequestMethod.GET, value = "/res_country_groups/select")
    Page<Res_country_group> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_country_groups/getdraft")
    Res_country_group getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/checkkey")
    Boolean checkKey(@RequestBody Res_country_group res_country_group);


    @RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/save")
    Boolean save(@RequestBody Res_country_group res_country_group);

    @RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/savebatch")
    Boolean saveBatch(@RequestBody List<Res_country_group> res_country_groups);



    @RequestMapping(method = RequestMethod.POST, value = "/res_country_groups/searchdefault")
    Page<Res_country_group> searchDefault(@RequestBody Res_country_groupSearchContext context);


}

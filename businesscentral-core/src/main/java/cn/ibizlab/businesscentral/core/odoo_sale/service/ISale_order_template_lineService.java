package cn.ibizlab.businesscentral.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template_line;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_order_template_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sale_order_template_line] 服务对象接口
 */
public interface ISale_order_template_lineService extends IService<Sale_order_template_line>{

    boolean create(Sale_order_template_line et) ;
    void createBatch(List<Sale_order_template_line> list) ;
    boolean update(Sale_order_template_line et) ;
    void updateBatch(List<Sale_order_template_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sale_order_template_line get(Long key) ;
    Sale_order_template_line getDraft(Sale_order_template_line et) ;
    boolean checkKey(Sale_order_template_line et) ;
    boolean save(Sale_order_template_line et) ;
    void saveBatch(List<Sale_order_template_line> list) ;
    Page<Sale_order_template_line> searchDefault(Sale_order_template_lineSearchContext context) ;
    List<Sale_order_template_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Sale_order_template_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Sale_order_template_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Sale_order_template_line> selectBySaleOrderTemplateId(Long id);
    void removeBySaleOrderTemplateId(Collection<Long> ids);
    void removeBySaleOrderTemplateId(Long id);
    List<Sale_order_template_line> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sale_order_template_line> getSaleOrderTemplateLineByIds(List<Long> ids) ;
    List<Sale_order_template_line> getSaleOrderTemplateLineByEntities(List<Sale_order_template_line> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendarSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Resource_calendar] 服务对象接口
 */
public interface IResource_calendarService extends IService<Resource_calendar>{

    boolean create(Resource_calendar et) ;
    void createBatch(List<Resource_calendar> list) ;
    boolean update(Resource_calendar et) ;
    void updateBatch(List<Resource_calendar> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Resource_calendar get(Long key) ;
    Resource_calendar getDraft(Resource_calendar et) ;
    boolean checkKey(Resource_calendar et) ;
    boolean save(Resource_calendar et) ;
    void saveBatch(List<Resource_calendar> list) ;
    Page<Resource_calendar> searchDefault(Resource_calendarSearchContext context) ;
    List<Resource_calendar> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Resource_calendar> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Resource_calendar> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Resource_calendar> getResourceCalendarByIds(List<Long> ids) ;
    List<Resource_calendar> getResourceCalendarByEntities(List<Resource_calendar> entities) ;
}



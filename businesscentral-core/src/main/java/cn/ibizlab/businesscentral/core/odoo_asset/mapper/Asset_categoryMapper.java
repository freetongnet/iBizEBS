package cn.ibizlab.businesscentral.core.odoo_asset.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.businesscentral.core.odoo_asset.filter.Asset_categorySearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Asset_categoryMapper extends BaseMapper<Asset_category>{

    Page<Asset_category> searchDefault(IPage page, @Param("srf") Asset_categorySearchContext context, @Param("ew") Wrapper<Asset_category> wrapper) ;
    @Override
    Asset_category selectById(Serializable id);
    @Override
    int insert(Asset_category entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Asset_category entity);
    @Override
    int update(@Param(Constants.ENTITY) Asset_category entity, @Param("ew") Wrapper<Asset_category> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Asset_category> selectByCreateUid(@Param("id") Serializable id) ;

    List<Asset_category> selectByWriteUid(@Param("id") Serializable id) ;


}

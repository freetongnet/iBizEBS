package cn.ibizlab.businesscentral.core.odoo_portal.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_mixinSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Portal_mixin] 服务对象接口
 */
public interface IPortal_mixinService extends IService<Portal_mixin>{

    boolean create(Portal_mixin et) ;
    void createBatch(List<Portal_mixin> list) ;
    boolean update(Portal_mixin et) ;
    void updateBatch(List<Portal_mixin> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Portal_mixin get(Long key) ;
    Portal_mixin getDraft(Portal_mixin et) ;
    boolean checkKey(Portal_mixin et) ;
    boolean save(Portal_mixin et) ;
    void saveBatch(List<Portal_mixin> list) ;
    Page<Portal_mixin> searchDefault(Portal_mixinSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



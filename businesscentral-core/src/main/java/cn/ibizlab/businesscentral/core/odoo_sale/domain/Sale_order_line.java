package cn.ibizlab.businesscentral.core.odoo_sale.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[销售订单行]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SALE_ORDER_LINE",resultMap = "Sale_order_lineResultMap")
public class Sale_order_line extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 可报销
     */
    @DEField(name = "is_expense")
    @TableField(value = "is_expense")
    @JSONField(name = "is_expense")
    @JsonProperty("is_expense")
    private Boolean isExpense;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 发票数量
     */
    @DEField(name = "qty_to_invoice")
    @TableField(value = "qty_to_invoice")
    @JSONField(name = "qty_to_invoice")
    @JsonProperty("qty_to_invoice")
    private Double qtyToInvoice;
    /**
     * 分析标签
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;
    /**
     * 警告
     */
    @DEField(name = "warning_stock")
    @TableField(value = "warning_stock")
    @JSONField(name = "warning_stock")
    @JsonProperty("warning_stock")
    private String warningStock;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 不含税价
     */
    @DEField(name = "price_reduce_taxexcl")
    @TableField(value = "price_reduce_taxexcl")
    @JSONField(name = "price_reduce_taxexcl")
    @JsonProperty("price_reduce_taxexcl")
    private BigDecimal priceReduceTaxexcl;
    /**
     * 用户输入自定义产品属性值
     */
    @TableField(exist = false)
    @JSONField(name = "product_custom_attribute_value_ids")
    @JsonProperty("product_custom_attribute_value_ids")
    private String productCustomAttributeValueIds;
    /**
     * 手动发货
     */
    @DEField(name = "qty_delivered_manual")
    @TableField(value = "qty_delivered_manual")
    @JSONField(name = "qty_delivered_manual")
    @JsonProperty("qty_delivered_manual")
    private Double qtyDeliveredManual;
    /**
     * 税额总计
     */
    @DEField(name = "price_tax")
    @TableField(value = "price_tax")
    @JSONField(name = "price_tax")
    @JsonProperty("price_tax")
    private Double priceTax;
    /**
     * 更新数量的方法
     */
    @DEField(name = "qty_delivered_method")
    @TableField(value = "qty_delivered_method")
    @JSONField(name = "qty_delivered_method")
    @JsonProperty("qty_delivered_method")
    private String qtyDeliveredMethod;
    /**
     * 发票状态
     */
    @DEField(name = "invoice_status")
    @TableField(value = "invoice_status")
    @JSONField(name = "invoice_status")
    @JsonProperty("invoice_status")
    private String invoiceStatus;
    /**
     * 显示类型
     */
    @DEField(name = "display_type")
    @TableField(value = "display_type")
    @JSONField(name = "display_type")
    @JsonProperty("display_type")
    private String displayType;
    /**
     * 库存移动
     */
    @TableField(exist = false)
    @JSONField(name = "move_ids")
    @JsonProperty("move_ids")
    private String moveIds;
    /**
     * 未含税的发票金额
     */
    @DEField(name = "untaxed_amount_invoiced")
    @TableField(value = "untaxed_amount_invoiced")
    @JSONField(name = "untaxed_amount_invoiced")
    @JsonProperty("untaxed_amount_invoiced")
    private BigDecimal untaxedAmountInvoiced;
    /**
     * 没有创建变量的产品属性值
     */
    @TableField(exist = false)
    @JSONField(name = "product_no_variant_attribute_value_ids")
    @JsonProperty("product_no_variant_attribute_value_ids")
    private String productNoVariantAttributeValueIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 是首付款吗？
     */
    @DEField(name = "is_downpayment")
    @TableField(value = "is_downpayment")
    @JSONField(name = "is_downpayment")
    @JsonProperty("is_downpayment")
    private Boolean isDownpayment;
    /**
     * 分析明细行
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_line_ids")
    @JsonProperty("analytic_line_ids")
    private String analyticLineIds;
    /**
     * 减税后价格
     */
    @DEField(name = "price_reduce_taxinc")
    @TableField(value = "price_reduce_taxinc")
    @JSONField(name = "price_reduce_taxinc")
    @JsonProperty("price_reduce_taxinc")
    private BigDecimal priceReduceTaxinc;
    /**
     * 已交货数量
     */
    @DEField(name = "qty_delivered")
    @TableField(value = "qty_delivered")
    @JSONField(name = "qty_delivered")
    @JsonProperty("qty_delivered")
    private Double qtyDelivered;
    /**
     * 折扣(%)
     */
    @TableField(value = "discount")
    @JSONField(name = "discount")
    @JsonProperty("discount")
    private Double discount;
    /**
     * 订购数量
     */
    @DEField(name = "product_uom_qty")
    @TableField(value = "product_uom_qty")
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;
    /**
     * 降价
     */
    @DEField(name = "price_reduce")
    @TableField(value = "price_reduce")
    @JSONField(name = "price_reduce")
    @JsonProperty("price_reduce")
    private Double priceReduce;
    /**
     * 交货提前时间
     */
    @DEField(name = "customer_lead")
    @TableField(value = "customer_lead")
    @JSONField(name = "customer_lead")
    @JsonProperty("customer_lead")
    private Double customerLead;
    /**
     * 允许编辑
     */
    @TableField(exist = false)
    @JSONField(name = "product_updatable")
    @JsonProperty("product_updatable")
    private Boolean productUpdatable;
    /**
     * 已开发票数量
     */
    @DEField(name = "qty_invoiced")
    @TableField(value = "qty_invoiced")
    @JSONField(name = "qty_invoiced")
    @JsonProperty("qty_invoiced")
    private Double qtyInvoiced;
    /**
     * 不含税待开票金额
     */
    @DEField(name = "untaxed_amount_to_invoice")
    @TableField(value = "untaxed_amount_to_invoice")
    @JSONField(name = "untaxed_amount_to_invoice")
    @JsonProperty("untaxed_amount_to_invoice")
    private BigDecimal untaxedAmountToInvoice;
    /**
     * 说明
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 单价
     */
    @DEField(name = "price_unit")
    @TableField(value = "price_unit")
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 生成采购订单号
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_line_count")
    @JsonProperty("purchase_line_count")
    private Integer purchaseLineCount;
    /**
     * 姓名简称
     */
    @TableField(exist = false)
    @JSONField(name = "name_short")
    @JsonProperty("name_short")
    private String nameShort;
    /**
     * 链接选项
     */
    @TableField(exist = false)
    @JSONField(name = "option_line_ids")
    @JsonProperty("option_line_ids")
    private String optionLineIds;
    /**
     * 税率
     */
    @TableField(exist = false)
    @JSONField(name = "tax_id")
    @JsonProperty("tax_id")
    private String taxId;
    /**
     * 可选产品行
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_option_ids")
    @JsonProperty("sale_order_option_ids")
    private String saleOrderOptionIds;
    /**
     * 小计
     */
    @DEField(name = "price_subtotal")
    @TableField(value = "price_subtotal")
    @JSONField(name = "price_subtotal")
    @JsonProperty("price_subtotal")
    private BigDecimal priceSubtotal;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 总计
     */
    @DEField(name = "price_total")
    @TableField(value = "price_total")
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private BigDecimal priceTotal;
    /**
     * 发票行
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_lines")
    @JsonProperty("invoice_lines")
    private String invoiceLines;
    /**
     * 生成采购订单明细行
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_line_ids")
    @JsonProperty("purchase_line_ids")
    private String purchaseLineIds;
    /**
     * 是一张活动票吗？
     */
    @TableField(exist = false)
    @JSONField(name = "event_ok")
    @JsonProperty("event_ok")
    private Boolean eventOk;
    /**
     * 活动入场券
     */
    @TableField(exist = false)
    @JSONField(name = "event_ticket_id_text")
    @JsonProperty("event_ticket_id_text")
    private String eventTicketIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 链接的订单明细
     */
    @TableField(exist = false)
    @JSONField(name = "linked_line_id_text")
    @JsonProperty("linked_line_id_text")
    private String linkedLineIdText;
    /**
     * 客户
     */
    @TableField(exist = false)
    @JSONField(name = "order_partner_id_text")
    @JsonProperty("order_partner_id_text")
    private String orderPartnerIdText;
    /**
     * 产品图片
     */
    @TableField(exist = false)
    @JSONField(name = "product_image")
    @JsonProperty("product_image")
    private byte[] productImage;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;
    /**
     * 包裹
     */
    @TableField(exist = false)
    @JSONField(name = "product_packaging_text")
    @JsonProperty("product_packaging_text")
    private String productPackagingText;
    /**
     * 订单关联
     */
    @TableField(exist = false)
    @JSONField(name = "order_id_text")
    @JsonProperty("order_id_text")
    private String orderIdText;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "event_id_text")
    @JsonProperty("event_id_text")
    private String eventIdText;
    /**
     * 销售员
     */
    @TableField(exist = false)
    @JSONField(name = "salesman_id_text")
    @JsonProperty("salesman_id_text")
    private String salesmanIdText;
    /**
     * 计量单位
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 订单状态
     */
    @TableField(exist = false)
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 路线
     */
    @TableField(exist = false)
    @JSONField(name = "route_id_text")
    @JsonProperty("route_id_text")
    private String routeIdText;
    /**
     * 销售员
     */
    @DEField(name = "salesman_id")
    @TableField(value = "salesman_id")
    @JSONField(name = "salesman_id")
    @JsonProperty("salesman_id")
    private Long salesmanId;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 订单关联
     */
    @DEField(name = "order_id")
    @TableField(value = "order_id")
    @JSONField(name = "order_id")
    @JsonProperty("order_id")
    private Long orderId;
    /**
     * 活动
     */
    @DEField(name = "event_id")
    @TableField(value = "event_id")
    @JSONField(name = "event_id")
    @JsonProperty("event_id")
    private Long eventId;
    /**
     * 链接的订单明细
     */
    @DEField(name = "linked_line_id")
    @TableField(value = "linked_line_id")
    @JSONField(name = "linked_line_id")
    @JsonProperty("linked_line_id")
    private Long linkedLineId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 路线
     */
    @DEField(name = "route_id")
    @TableField(value = "route_id")
    @JSONField(name = "route_id")
    @JsonProperty("route_id")
    private Long routeId;
    /**
     * 客户
     */
    @DEField(name = "order_partner_id")
    @TableField(value = "order_partner_id")
    @JSONField(name = "order_partner_id")
    @JsonProperty("order_partner_id")
    private Long orderPartnerId;
    /**
     * 计量单位
     */
    @DEField(name = "product_uom")
    @TableField(value = "product_uom")
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Long productUom;
    /**
     * 产品
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 包裹
     */
    @DEField(name = "product_packaging")
    @TableField(value = "product_packaging")
    @JSONField(name = "product_packaging")
    @JsonProperty("product_packaging")
    private Long productPackaging;
    /**
     * 活动入场券
     */
    @DEField(name = "event_ticket_id")
    @TableField(value = "event_ticket_id")
    @JSONField(name = "event_ticket_id")
    @JsonProperty("event_ticket_id")
    private Long eventTicketId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket odooEventTicket;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event odooEvent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_packaging odooProductPackaging;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOrderPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooSalesman;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line odooLinkedLine;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooOrder;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooRoute;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom;



    /**
     * 设置 [可报销]
     */
    public void setIsExpense(Boolean isExpense){
        this.isExpense = isExpense ;
        this.modify("is_expense",isExpense);
    }

    /**
     * 设置 [发票数量]
     */
    public void setQtyToInvoice(Double qtyToInvoice){
        this.qtyToInvoice = qtyToInvoice ;
        this.modify("qty_to_invoice",qtyToInvoice);
    }

    /**
     * 设置 [警告]
     */
    public void setWarningStock(String warningStock){
        this.warningStock = warningStock ;
        this.modify("warning_stock",warningStock);
    }

    /**
     * 设置 [不含税价]
     */
    public void setPriceReduceTaxexcl(BigDecimal priceReduceTaxexcl){
        this.priceReduceTaxexcl = priceReduceTaxexcl ;
        this.modify("price_reduce_taxexcl",priceReduceTaxexcl);
    }

    /**
     * 设置 [手动发货]
     */
    public void setQtyDeliveredManual(Double qtyDeliveredManual){
        this.qtyDeliveredManual = qtyDeliveredManual ;
        this.modify("qty_delivered_manual",qtyDeliveredManual);
    }

    /**
     * 设置 [税额总计]
     */
    public void setPriceTax(Double priceTax){
        this.priceTax = priceTax ;
        this.modify("price_tax",priceTax);
    }

    /**
     * 设置 [更新数量的方法]
     */
    public void setQtyDeliveredMethod(String qtyDeliveredMethod){
        this.qtyDeliveredMethod = qtyDeliveredMethod ;
        this.modify("qty_delivered_method",qtyDeliveredMethod);
    }

    /**
     * 设置 [发票状态]
     */
    public void setInvoiceStatus(String invoiceStatus){
        this.invoiceStatus = invoiceStatus ;
        this.modify("invoice_status",invoiceStatus);
    }

    /**
     * 设置 [显示类型]
     */
    public void setDisplayType(String displayType){
        this.displayType = displayType ;
        this.modify("display_type",displayType);
    }

    /**
     * 设置 [未含税的发票金额]
     */
    public void setUntaxedAmountInvoiced(BigDecimal untaxedAmountInvoiced){
        this.untaxedAmountInvoiced = untaxedAmountInvoiced ;
        this.modify("untaxed_amount_invoiced",untaxedAmountInvoiced);
    }

    /**
     * 设置 [是首付款吗？]
     */
    public void setIsDownpayment(Boolean isDownpayment){
        this.isDownpayment = isDownpayment ;
        this.modify("is_downpayment",isDownpayment);
    }

    /**
     * 设置 [减税后价格]
     */
    public void setPriceReduceTaxinc(BigDecimal priceReduceTaxinc){
        this.priceReduceTaxinc = priceReduceTaxinc ;
        this.modify("price_reduce_taxinc",priceReduceTaxinc);
    }

    /**
     * 设置 [已交货数量]
     */
    public void setQtyDelivered(Double qtyDelivered){
        this.qtyDelivered = qtyDelivered ;
        this.modify("qty_delivered",qtyDelivered);
    }

    /**
     * 设置 [折扣(%)]
     */
    public void setDiscount(Double discount){
        this.discount = discount ;
        this.modify("discount",discount);
    }

    /**
     * 设置 [订购数量]
     */
    public void setProductUomQty(Double productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [降价]
     */
    public void setPriceReduce(Double priceReduce){
        this.priceReduce = priceReduce ;
        this.modify("price_reduce",priceReduce);
    }

    /**
     * 设置 [交货提前时间]
     */
    public void setCustomerLead(Double customerLead){
        this.customerLead = customerLead ;
        this.modify("customer_lead",customerLead);
    }

    /**
     * 设置 [已开发票数量]
     */
    public void setQtyInvoiced(Double qtyInvoiced){
        this.qtyInvoiced = qtyInvoiced ;
        this.modify("qty_invoiced",qtyInvoiced);
    }

    /**
     * 设置 [不含税待开票金额]
     */
    public void setUntaxedAmountToInvoice(BigDecimal untaxedAmountToInvoice){
        this.untaxedAmountToInvoice = untaxedAmountToInvoice ;
        this.modify("untaxed_amount_to_invoice",untaxedAmountToInvoice);
    }

    /**
     * 设置 [说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [单价]
     */
    public void setPriceUnit(Double priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [小计]
     */
    public void setPriceSubtotal(BigDecimal priceSubtotal){
        this.priceSubtotal = priceSubtotal ;
        this.modify("price_subtotal",priceSubtotal);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [总计]
     */
    public void setPriceTotal(BigDecimal priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }

    /**
     * 设置 [销售员]
     */
    public void setSalesmanId(Long salesmanId){
        this.salesmanId = salesmanId ;
        this.modify("salesman_id",salesmanId);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [订单关联]
     */
    public void setOrderId(Long orderId){
        this.orderId = orderId ;
        this.modify("order_id",orderId);
    }

    /**
     * 设置 [活动]
     */
    public void setEventId(Long eventId){
        this.eventId = eventId ;
        this.modify("event_id",eventId);
    }

    /**
     * 设置 [链接的订单明细]
     */
    public void setLinkedLineId(Long linkedLineId){
        this.linkedLineId = linkedLineId ;
        this.modify("linked_line_id",linkedLineId);
    }

    /**
     * 设置 [路线]
     */
    public void setRouteId(Long routeId){
        this.routeId = routeId ;
        this.modify("route_id",routeId);
    }

    /**
     * 设置 [客户]
     */
    public void setOrderPartnerId(Long orderPartnerId){
        this.orderPartnerId = orderPartnerId ;
        this.modify("order_partner_id",orderPartnerId);
    }

    /**
     * 设置 [计量单位]
     */
    public void setProductUom(Long productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [产品]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [包裹]
     */
    public void setProductPackaging(Long productPackaging){
        this.productPackaging = productPackaging ;
        this.modify("product_packaging",productPackaging);
    }

    /**
     * 设置 [活动入场券]
     */
    public void setEventTicketId(Long eventTicketId){
        this.eventTicketId = eventTicketId ;
        this.modify("event_ticket_id",eventTicketId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



package cn.ibizlab.businesscentral.core.odoo_crm.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_activity_reportSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Crm_activity_reportMapper extends BaseMapper<Crm_activity_report>{

    Page<Crm_activity_report> searchDefault(IPage page, @Param("srf") Crm_activity_reportSearchContext context, @Param("ew") Wrapper<Crm_activity_report> wrapper) ;
    @Override
    Crm_activity_report selectById(Serializable id);
    @Override
    int insert(Crm_activity_report entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Crm_activity_report entity);
    @Override
    int update(@Param(Constants.ENTITY) Crm_activity_report entity, @Param("ew") Wrapper<Crm_activity_report> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Crm_activity_report> selectByLeadId(@Param("id") Serializable id) ;

    List<Crm_activity_report> selectByStageId(@Param("id") Serializable id) ;

    List<Crm_activity_report> selectByTeamId(@Param("id") Serializable id) ;

    List<Crm_activity_report> selectByMailActivityTypeId(@Param("id") Serializable id) ;

    List<Crm_activity_report> selectBySubtypeId(@Param("id") Serializable id) ;

    List<Crm_activity_report> selectByCompanyId(@Param("id") Serializable id) ;

    List<Crm_activity_report> selectByCountryId(@Param("id") Serializable id) ;

    List<Crm_activity_report> selectByAuthorId(@Param("id") Serializable id) ;

    List<Crm_activity_report> selectByPartnerId(@Param("id") Serializable id) ;

    List<Crm_activity_report> selectByUserId(@Param("id") Serializable id) ;


}

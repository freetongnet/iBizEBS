package cn.ibizlab.businesscentral.core.odoo_survey.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_stageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[survey_stage] 服务对象接口
 */
@Component
public class survey_stageFallback implements survey_stageFeignClient{

    public Page<Survey_stage> search(Survey_stageSearchContext context){
            return null;
     }




    public Survey_stage create(Survey_stage survey_stage){
            return null;
     }
    public Boolean createBatch(List<Survey_stage> survey_stages){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Survey_stage update(Long id, Survey_stage survey_stage){
            return null;
     }
    public Boolean updateBatch(List<Survey_stage> survey_stages){
            return false;
     }


    public Survey_stage get(Long id){
            return null;
     }


    public Page<Survey_stage> select(){
            return null;
     }

    public Survey_stage getDraft(){
            return null;
    }



    public Boolean checkKey(Survey_stage survey_stage){
            return false;
     }


    public Boolean save(Survey_stage survey_stage){
            return false;
     }
    public Boolean saveBatch(List<Survey_stage> survey_stages){
            return false;
     }

    public Page<Survey_stage> searchDefault(Survey_stageSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template;
/**
 * 关系型数据实体[Product_template] 查询条件对象
 */
@Slf4j
@Data
public class Product_templateSearchContext extends QueryWrapperContext<Product_template> {

	private String n_purchase_line_warn_eq;//[采购订单行]
	public void setN_purchase_line_warn_eq(String n_purchase_line_warn_eq) {
        this.n_purchase_line_warn_eq = n_purchase_line_warn_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_line_warn_eq)){
            this.getSearchCond().eq("purchase_line_warn", n_purchase_line_warn_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_property_cost_method_eq;//[成本方法]
	public void setN_property_cost_method_eq(String n_property_cost_method_eq) {
        this.n_property_cost_method_eq = n_property_cost_method_eq;
        if(!ObjectUtils.isEmpty(this.n_property_cost_method_eq)){
            this.getSearchCond().eq("property_cost_method", n_property_cost_method_eq);
        }
    }
	private String n_invoice_policy_eq;//[开票策略]
	public void setN_invoice_policy_eq(String n_invoice_policy_eq) {
        this.n_invoice_policy_eq = n_invoice_policy_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_policy_eq)){
            this.getSearchCond().eq("invoice_policy", n_invoice_policy_eq);
        }
    }
	private String n_type_eq;//[产品类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!ObjectUtils.isEmpty(this.n_type_eq)){
            this.getSearchCond().eq("type", n_type_eq);
        }
    }
	private String n_expense_policy_eq;//[重开收据规则]
	public void setN_expense_policy_eq(String n_expense_policy_eq) {
        this.n_expense_policy_eq = n_expense_policy_eq;
        if(!ObjectUtils.isEmpty(this.n_expense_policy_eq)){
            this.getSearchCond().eq("expense_policy", n_expense_policy_eq);
        }
    }
	private String n_sale_line_warn_eq;//[销售订单行]
	public void setN_sale_line_warn_eq(String n_sale_line_warn_eq) {
        this.n_sale_line_warn_eq = n_sale_line_warn_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_line_warn_eq)){
            this.getSearchCond().eq("sale_line_warn", n_sale_line_warn_eq);
        }
    }
	private String n_purchase_method_eq;//[控制策略]
	public void setN_purchase_method_eq(String n_purchase_method_eq) {
        this.n_purchase_method_eq = n_purchase_method_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_method_eq)){
            this.getSearchCond().eq("purchase_method", n_purchase_method_eq);
        }
    }
	private String n_service_type_eq;//[跟踪服务]
	public void setN_service_type_eq(String n_service_type_eq) {
        this.n_service_type_eq = n_service_type_eq;
        if(!ObjectUtils.isEmpty(this.n_service_type_eq)){
            this.getSearchCond().eq("service_type", n_service_type_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_tracking_eq;//[追踪]
	public void setN_tracking_eq(String n_tracking_eq) {
        this.n_tracking_eq = n_tracking_eq;
        if(!ObjectUtils.isEmpty(this.n_tracking_eq)){
            this.getSearchCond().eq("tracking", n_tracking_eq);
        }
    }
	private String n_inventory_availability_eq;//[库存可用性]
	public void setN_inventory_availability_eq(String n_inventory_availability_eq) {
        this.n_inventory_availability_eq = n_inventory_availability_eq;
        if(!ObjectUtils.isEmpty(this.n_inventory_availability_eq)){
            this.getSearchCond().eq("inventory_availability", n_inventory_availability_eq);
        }
    }
	private String n_property_valuation_eq;//[库存计价]
	public void setN_property_valuation_eq(String n_property_valuation_eq) {
        this.n_property_valuation_eq = n_property_valuation_eq;
        if(!ObjectUtils.isEmpty(this.n_property_valuation_eq)){
            this.getSearchCond().eq("property_valuation", n_property_valuation_eq);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_uom_po_id_text_eq;//[采购计量单位]
	public void setN_uom_po_id_text_eq(String n_uom_po_id_text_eq) {
        this.n_uom_po_id_text_eq = n_uom_po_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_uom_po_id_text_eq)){
            this.getSearchCond().eq("uom_po_id_text", n_uom_po_id_text_eq);
        }
    }
	private String n_uom_po_id_text_like;//[采购计量单位]
	public void setN_uom_po_id_text_like(String n_uom_po_id_text_like) {
        this.n_uom_po_id_text_like = n_uom_po_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_uom_po_id_text_like)){
            this.getSearchCond().like("uom_po_id_text", n_uom_po_id_text_like);
        }
    }
	private String n_uom_name_eq;//[单位名称]
	public void setN_uom_name_eq(String n_uom_name_eq) {
        this.n_uom_name_eq = n_uom_name_eq;
        if(!ObjectUtils.isEmpty(this.n_uom_name_eq)){
            this.getSearchCond().eq("uom_name", n_uom_name_eq);
        }
    }
	private String n_uom_name_like;//[单位名称]
	public void setN_uom_name_like(String n_uom_name_like) {
        this.n_uom_name_like = n_uom_name_like;
        if(!ObjectUtils.isEmpty(this.n_uom_name_like)){
            this.getSearchCond().like("uom_name", n_uom_name_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_categ_id_text_eq;//[产品种类]
	public void setN_categ_id_text_eq(String n_categ_id_text_eq) {
        this.n_categ_id_text_eq = n_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_categ_id_text_eq)){
            this.getSearchCond().eq("categ_id_text", n_categ_id_text_eq);
        }
    }
	private String n_categ_id_text_like;//[产品种类]
	public void setN_categ_id_text_like(String n_categ_id_text_like) {
        this.n_categ_id_text_like = n_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_categ_id_text_like)){
            this.getSearchCond().like("categ_id_text", n_categ_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_categ_id_eq;//[产品种类]
	public void setN_categ_id_eq(Long n_categ_id_eq) {
        this.n_categ_id_eq = n_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_categ_id_eq)){
            this.getSearchCond().eq("categ_id", n_categ_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_uom_id_eq;//[计量单位]
	public void setN_uom_id_eq(Long n_uom_id_eq) {
        this.n_uom_id_eq = n_uom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_uom_id_eq)){
            this.getSearchCond().eq("uom_id", n_uom_id_eq);
        }
    }
	private Long n_uom_po_id_eq;//[采购计量单位]
	public void setN_uom_po_id_eq(Long n_uom_po_id_eq) {
        this.n_uom_po_id_eq = n_uom_po_id_eq;
        if(!ObjectUtils.isEmpty(this.n_uom_po_id_eq)){
            this.getSearchCond().eq("uom_po_id", n_uom_po_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




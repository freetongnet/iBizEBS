package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_recruitment_stage] 服务对象接口
 */
public interface IHr_recruitment_stageService extends IService<Hr_recruitment_stage>{

    boolean create(Hr_recruitment_stage et) ;
    void createBatch(List<Hr_recruitment_stage> list) ;
    boolean update(Hr_recruitment_stage et) ;
    void updateBatch(List<Hr_recruitment_stage> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_recruitment_stage get(Long key) ;
    Hr_recruitment_stage getDraft(Hr_recruitment_stage et) ;
    boolean checkKey(Hr_recruitment_stage et) ;
    boolean save(Hr_recruitment_stage et) ;
    void saveBatch(List<Hr_recruitment_stage> list) ;
    Page<Hr_recruitment_stage> searchDefault(Hr_recruitment_stageSearchContext context) ;
    List<Hr_recruitment_stage> selectByJobId(Long id);
    void removeByJobId(Collection<Long> ids);
    void removeByJobId(Long id);
    List<Hr_recruitment_stage> selectByTemplateId(Long id);
    void resetByTemplateId(Long id);
    void resetByTemplateId(Collection<Long> ids);
    void removeByTemplateId(Long id);
    List<Hr_recruitment_stage> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_recruitment_stage> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_recruitment_stage> getHrRecruitmentStageByIds(List<Long> ids) ;
    List<Hr_recruitment_stage> getHrRecruitmentStageByEntities(List<Hr_recruitment_stage> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_ruleSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_ruleMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[库存规则] 服务对象接口实现
 */
@Slf4j
@Service("Stock_ruleServiceImpl")
public class Stock_ruleServiceImpl extends EBSServiceImpl<Stock_ruleMapper, Stock_rule> implements IStock_ruleService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_location_routeService stockLocationRouteService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.rule" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_rule et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_ruleService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_rule> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_rule et) {
        Stock_rule old = new Stock_rule() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_ruleService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_ruleService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_rule> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        if(!ObjectUtils.isEmpty(stockMoveService.selectByRuleId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_move]数据，无法删除!","","");
        stockWarehouseService.resetByBuyPullId(key);
        stockWarehouseService.resetByManufacturePullId(key);
        stockWarehouseService.resetByMtoPullId(key);
        stockWarehouseService.resetByPbmMtoPullId(key);
        stockWarehouseService.resetBySamRuleId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        if(!ObjectUtils.isEmpty(stockMoveService.selectByRuleId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_move]数据，无法删除!","","");
        stockWarehouseService.resetByBuyPullId(idList);
        stockWarehouseService.resetByManufacturePullId(idList);
        stockWarehouseService.resetByMtoPullId(idList);
        stockWarehouseService.resetByPbmMtoPullId(idList);
        stockWarehouseService.resetBySamRuleId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_rule get(Long key) {
        Stock_rule et = getById(key);
        if(et==null){
            et=new Stock_rule();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_rule getDraft(Stock_rule et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_rule et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_rule et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_rule et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_rule> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_rule> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_rule> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Stock_rule>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_rule>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("company_id",id));
    }

	@Override
    public List<Stock_rule> selectByPartnerAddressId(Long id) {
        return baseMapper.selectByPartnerAddressId(id);
    }
    @Override
    public void resetByPartnerAddressId(Long id) {
        this.update(new UpdateWrapper<Stock_rule>().set("partner_address_id",null).eq("partner_address_id",id));
    }

    @Override
    public void resetByPartnerAddressId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_rule>().set("partner_address_id",null).in("partner_address_id",ids));
    }

    @Override
    public void removeByPartnerAddressId(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("partner_address_id",id));
    }

	@Override
    public List<Stock_rule> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("create_uid",id));
    }

	@Override
    public List<Stock_rule> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("write_uid",id));
    }

	@Override
    public List<Stock_rule> selectByRouteId(Long id) {
        return baseMapper.selectByRouteId(id);
    }
    @Override
    public void removeByRouteId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Stock_rule>().in("route_id",ids));
    }

    @Override
    public void removeByRouteId(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("route_id",id));
    }

	@Override
    public List<Stock_rule> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_rule>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_rule>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("location_id",id));
    }

	@Override
    public List<Stock_rule> selectByLocationSrcId(Long id) {
        return baseMapper.selectByLocationSrcId(id);
    }
    @Override
    public void resetByLocationSrcId(Long id) {
        this.update(new UpdateWrapper<Stock_rule>().set("location_src_id",null).eq("location_src_id",id));
    }

    @Override
    public void resetByLocationSrcId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_rule>().set("location_src_id",null).in("location_src_id",ids));
    }

    @Override
    public void removeByLocationSrcId(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("location_src_id",id));
    }

	@Override
    public List<Stock_rule> selectByPickingTypeId(Long id) {
        return baseMapper.selectByPickingTypeId(id);
    }
    @Override
    public void resetByPickingTypeId(Long id) {
        this.update(new UpdateWrapper<Stock_rule>().set("picking_type_id",null).eq("picking_type_id",id));
    }

    @Override
    public void resetByPickingTypeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_rule>().set("picking_type_id",null).in("picking_type_id",ids));
    }

    @Override
    public void removeByPickingTypeId(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("picking_type_id",id));
    }

	@Override
    public List<Stock_rule> selectByPropagateWarehouseId(Long id) {
        return baseMapper.selectByPropagateWarehouseId(id);
    }
    @Override
    public void resetByPropagateWarehouseId(Long id) {
        this.update(new UpdateWrapper<Stock_rule>().set("propagate_warehouse_id",null).eq("propagate_warehouse_id",id));
    }

    @Override
    public void resetByPropagateWarehouseId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_rule>().set("propagate_warehouse_id",null).in("propagate_warehouse_id",ids));
    }

    @Override
    public void removeByPropagateWarehouseId(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("propagate_warehouse_id",id));
    }

	@Override
    public List<Stock_rule> selectByWarehouseId(Long id) {
        return baseMapper.selectByWarehouseId(id);
    }
    @Override
    public void resetByWarehouseId(Long id) {
        this.update(new UpdateWrapper<Stock_rule>().set("warehouse_id",null).eq("warehouse_id",id));
    }

    @Override
    public void resetByWarehouseId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_rule>().set("warehouse_id",null).in("warehouse_id",ids));
    }

    @Override
    public void removeByWarehouseId(Long id) {
        this.remove(new QueryWrapper<Stock_rule>().eq("warehouse_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_rule> searchDefault(Stock_ruleSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_rule> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_rule>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_rule et){
        //实体关系[DER1N_STOCK_RULE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_STOCK_RULE__RES_PARTNER__PARTNER_ADDRESS_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerAddressId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartnerAddress=et.getOdooPartnerAddress();
            if(ObjectUtils.isEmpty(odooPartnerAddress)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerAddressId());
                et.setOdooPartnerAddress(majorEntity);
                odooPartnerAddress=majorEntity;
            }
            et.setPartnerAddressIdText(odooPartnerAddress.getName());
        }
        //实体关系[DER1N_STOCK_RULE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_RULE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_RULE__STOCK_LOCATION_ROUTE__ROUTE_ID]
        if(!ObjectUtils.isEmpty(et.getRouteId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route odooRoute=et.getOdooRoute();
            if(ObjectUtils.isEmpty(odooRoute)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route majorEntity=stockLocationRouteService.get(et.getRouteId());
                et.setOdooRoute(majorEntity);
                odooRoute=majorEntity;
            }
            et.setRouteIdText(odooRoute.getName());
            et.setRouteSequence(odooRoute.getSequence());
        }
        //实体关系[DER1N_STOCK_RULE__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_STOCK_RULE__STOCK_LOCATION__LOCATION_SRC_ID]
        if(!ObjectUtils.isEmpty(et.getLocationSrcId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationSrc=et.getOdooLocationSrc();
            if(ObjectUtils.isEmpty(odooLocationSrc)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationSrcId());
                et.setOdooLocationSrc(majorEntity);
                odooLocationSrc=majorEntity;
            }
            et.setLocationSrcIdText(odooLocationSrc.getName());
        }
        //实体关系[DER1N_STOCK_RULE__STOCK_PICKING_TYPE__PICKING_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getPickingTypeId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type odooPickingType=et.getOdooPickingType();
            if(ObjectUtils.isEmpty(odooPickingType)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type majorEntity=stockPickingTypeService.get(et.getPickingTypeId());
                et.setOdooPickingType(majorEntity);
                odooPickingType=majorEntity;
            }
            et.setPickingTypeIdText(odooPickingType.getName());
        }
        //实体关系[DER1N_STOCK_RULE__STOCK_WAREHOUSE__PROPAGATE_WAREHOUSE_ID]
        if(!ObjectUtils.isEmpty(et.getPropagateWarehouseId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooPropagateWarehouse=et.getOdooPropagateWarehouse();
            if(ObjectUtils.isEmpty(odooPropagateWarehouse)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse majorEntity=stockWarehouseService.get(et.getPropagateWarehouseId());
                et.setOdooPropagateWarehouse(majorEntity);
                odooPropagateWarehouse=majorEntity;
            }
            et.setPropagateWarehouseIdText(odooPropagateWarehouse.getName());
        }
        //实体关系[DER1N_STOCK_RULE__STOCK_WAREHOUSE__WAREHOUSE_ID]
        if(!ObjectUtils.isEmpty(et.getWarehouseId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooWarehouse=et.getOdooWarehouse();
            if(ObjectUtils.isEmpty(odooWarehouse)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse majorEntity=stockWarehouseService.get(et.getWarehouseId());
                et.setOdooWarehouse(majorEntity);
                odooWarehouse=majorEntity;
            }
            et.setWarehouseIdText(odooWarehouse.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_rule> getStockRuleByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_rule> getStockRuleByEntities(List<Stock_rule> entities) {
        List ids =new ArrayList();
        for(Stock_rule entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




package cn.ibizlab.businesscentral.core.odoo_portal.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[portal_wizard] 服务对象接口
 */
@Component
public class portal_wizardFallback implements portal_wizardFeignClient{




    public Portal_wizard create(Portal_wizard portal_wizard){
            return null;
     }
    public Boolean createBatch(List<Portal_wizard> portal_wizards){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Portal_wizard get(Long id){
            return null;
     }


    public Page<Portal_wizard> search(Portal_wizardSearchContext context){
            return null;
     }


    public Portal_wizard update(Long id, Portal_wizard portal_wizard){
            return null;
     }
    public Boolean updateBatch(List<Portal_wizard> portal_wizards){
            return false;
     }


    public Page<Portal_wizard> select(){
            return null;
     }

    public Portal_wizard getDraft(){
            return null;
    }



    public Boolean checkKey(Portal_wizard portal_wizard){
            return false;
     }


    public Boolean save(Portal_wizard portal_wizard){
            return false;
     }
    public Boolean saveBatch(List<Portal_wizard> portal_wizards){
            return false;
     }

    public Page<Portal_wizard> searchDefault(Portal_wizardSearchContext context){
            return null;
     }


}

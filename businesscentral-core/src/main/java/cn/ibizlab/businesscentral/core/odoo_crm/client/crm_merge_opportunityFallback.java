package cn.ibizlab.businesscentral.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_merge_opportunity] 服务对象接口
 */
@Component
public class crm_merge_opportunityFallback implements crm_merge_opportunityFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Crm_merge_opportunity update(Long id, Crm_merge_opportunity crm_merge_opportunity){
            return null;
     }
    public Boolean updateBatch(List<Crm_merge_opportunity> crm_merge_opportunities){
            return false;
     }



    public Crm_merge_opportunity create(Crm_merge_opportunity crm_merge_opportunity){
            return null;
     }
    public Boolean createBatch(List<Crm_merge_opportunity> crm_merge_opportunities){
            return false;
     }

    public Crm_merge_opportunity get(Long id){
            return null;
     }


    public Page<Crm_merge_opportunity> search(Crm_merge_opportunitySearchContext context){
            return null;
     }


    public Page<Crm_merge_opportunity> select(){
            return null;
     }

    public Crm_merge_opportunity getDraft(){
            return null;
    }



    public Boolean checkKey(Crm_merge_opportunity crm_merge_opportunity){
            return false;
     }


    public Boolean save(Crm_merge_opportunity crm_merge_opportunity){
            return false;
     }
    public Boolean saveBatch(List<Crm_merge_opportunity> crm_merge_opportunities){
            return false;
     }

    public Page<Crm_merge_opportunity> searchDefault(Crm_merge_opportunitySearchContext context){
            return null;
     }


}

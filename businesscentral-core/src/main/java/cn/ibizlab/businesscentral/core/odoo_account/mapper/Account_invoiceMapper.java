package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoiceSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_invoiceMapper extends BaseMapper<Account_invoice>{

    Page<Account_invoice> searchDefault(IPage page, @Param("srf") Account_invoiceSearchContext context, @Param("ew") Wrapper<Account_invoice> wrapper) ;
    @Override
    Account_invoice selectById(Serializable id);
    @Override
    int insert(Account_invoice entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_invoice entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_invoice entity, @Param("ew") Wrapper<Account_invoice> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_invoice> selectByAccountId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByCashRoundingId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByFiscalPositionId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByIncotermsId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByIncotermId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByRefundInvoiceId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByVendorBillId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByJournalId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByMoveId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByPaymentTermId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByTeamId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByVendorBillPurchaseId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByPurchaseId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByCompanyId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByPartnerBankId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByCommercialPartnerId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByPartnerId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByPartnerShippingId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByCreateUid(@Param("id") Serializable id) ;

    List<Account_invoice> selectByUserId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByWriteUid(@Param("id") Serializable id) ;

    List<Account_invoice> selectByCampaignId(@Param("id") Serializable id) ;

    List<Account_invoice> selectByMediumId(@Param("id") Serializable id) ;

    List<Account_invoice> selectBySourceId(@Param("id") Serializable id) ;


}

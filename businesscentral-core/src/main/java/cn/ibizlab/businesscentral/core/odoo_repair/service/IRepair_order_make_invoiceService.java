package cn.ibizlab.businesscentral.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Repair_order_make_invoice] 服务对象接口
 */
public interface IRepair_order_make_invoiceService extends IService<Repair_order_make_invoice>{

    boolean create(Repair_order_make_invoice et) ;
    void createBatch(List<Repair_order_make_invoice> list) ;
    boolean update(Repair_order_make_invoice et) ;
    void updateBatch(List<Repair_order_make_invoice> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Repair_order_make_invoice get(Long key) ;
    Repair_order_make_invoice getDraft(Repair_order_make_invoice et) ;
    boolean checkKey(Repair_order_make_invoice et) ;
    boolean save(Repair_order_make_invoice et) ;
    void saveBatch(List<Repair_order_make_invoice> list) ;
    Page<Repair_order_make_invoice> searchDefault(Repair_order_make_invoiceSearchContext context) ;
    List<Repair_order_make_invoice> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Repair_order_make_invoice> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Repair_order_make_invoice> getRepairOrderMakeInvoiceByIds(List<Long> ids) ;
    List<Repair_order_make_invoice> getRepairOrderMakeInvoiceByEntities(List<Repair_order_make_invoice> entities) ;
}



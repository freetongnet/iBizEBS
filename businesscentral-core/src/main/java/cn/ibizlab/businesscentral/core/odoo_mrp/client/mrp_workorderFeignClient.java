package cn.ibizlab.businesscentral.core.odoo_mrp.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workorderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mrp_workorder] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mrp:odoo-mrp}", contextId = "mrp-workorder", fallback = mrp_workorderFallback.class)
public interface mrp_workorderFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/search")
    Page<Mrp_workorder> search(@RequestBody Mrp_workorderSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders")
    Mrp_workorder create(@RequestBody Mrp_workorder mrp_workorder);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/batch")
    Boolean createBatch(@RequestBody List<Mrp_workorder> mrp_workorders);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workorders/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workorders/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workorders/{id}")
    Mrp_workorder update(@PathVariable("id") Long id,@RequestBody Mrp_workorder mrp_workorder);

    @RequestMapping(method = RequestMethod.PUT, value = "/mrp_workorders/batch")
    Boolean updateBatch(@RequestBody List<Mrp_workorder> mrp_workorders);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workorders/{id}")
    Mrp_workorder get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workorders/select")
    Page<Mrp_workorder> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mrp_workorders/getdraft")
    Mrp_workorder getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/checkkey")
    Boolean checkKey(@RequestBody Mrp_workorder mrp_workorder);


    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/save")
    Boolean save(@RequestBody Mrp_workorder mrp_workorder);

    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/savebatch")
    Boolean saveBatch(@RequestBody List<Mrp_workorder> mrp_workorders);



    @RequestMapping(method = RequestMethod.POST, value = "/mrp_workorders/searchdefault")
    Page<Mrp_workorder> searchDefault(@RequestBody Mrp_workorderSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_model_templateService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_reconcile_model_templateMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[核销模型模板] 服务对象接口实现
 */
@Slf4j
@Service("Account_reconcile_model_templateServiceImpl")
public class Account_reconcile_model_templateServiceImpl extends EBSServiceImpl<Account_reconcile_model_templateMapper, Account_reconcile_model_template> implements IAccount_reconcile_model_templateService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_account_templateService accountAccountTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_chart_templateService accountChartTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_tax_templateService accountTaxTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.reconcile.model.template" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_reconcile_model_template et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_reconcile_model_templateService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_reconcile_model_template> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_reconcile_model_template et) {
        Account_reconcile_model_template old = new Account_reconcile_model_template() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_reconcile_model_templateService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_reconcile_model_templateService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_reconcile_model_template> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_reconcile_model_template get(Long key) {
        Account_reconcile_model_template et = getById(key);
        if(et==null){
            et=new Account_reconcile_model_template();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_reconcile_model_template getDraft(Account_reconcile_model_template et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_reconcile_model_template et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_reconcile_model_template et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_reconcile_model_template et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_reconcile_model_template> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_reconcile_model_template> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_reconcile_model_template> selectByAccountId(Long id) {
        return baseMapper.selectByAccountId(id);
    }
    @Override
    public void removeByAccountId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_reconcile_model_template>().in("account_id",ids));
    }

    @Override
    public void removeByAccountId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model_template>().eq("account_id",id));
    }

	@Override
    public List<Account_reconcile_model_template> selectBySecondAccountId(Long id) {
        return baseMapper.selectBySecondAccountId(id);
    }
    @Override
    public void removeBySecondAccountId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_reconcile_model_template>().in("second_account_id",ids));
    }

    @Override
    public void removeBySecondAccountId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model_template>().eq("second_account_id",id));
    }

	@Override
    public List<Account_reconcile_model_template> selectByChartTemplateId(Long id) {
        return baseMapper.selectByChartTemplateId(id);
    }
    @Override
    public void resetByChartTemplateId(Long id) {
        this.update(new UpdateWrapper<Account_reconcile_model_template>().set("chart_template_id",null).eq("chart_template_id",id));
    }

    @Override
    public void resetByChartTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_reconcile_model_template>().set("chart_template_id",null).in("chart_template_id",ids));
    }

    @Override
    public void removeByChartTemplateId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model_template>().eq("chart_template_id",id));
    }

	@Override
    public List<Account_reconcile_model_template> selectBySecondTaxId(Long id) {
        return baseMapper.selectBySecondTaxId(id);
    }
    @Override
    public List<Account_reconcile_model_template> selectBySecondTaxId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_reconcile_model_template>().in("id",ids));
    }

    @Override
    public void removeBySecondTaxId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model_template>().eq("second_tax_id",id));
    }

	@Override
    public List<Account_reconcile_model_template> selectByTaxId(Long id) {
        return baseMapper.selectByTaxId(id);
    }
    @Override
    public List<Account_reconcile_model_template> selectByTaxId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_reconcile_model_template>().in("id",ids));
    }

    @Override
    public void removeByTaxId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model_template>().eq("tax_id",id));
    }

	@Override
    public List<Account_reconcile_model_template> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model_template>().eq("create_uid",id));
    }

	@Override
    public List<Account_reconcile_model_template> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model_template>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_reconcile_model_template> searchDefault(Account_reconcile_model_templateSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_reconcile_model_template> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_reconcile_model_template>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_reconcile_model_template et){
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL_TEMPLATE__ACCOUNT_ACCOUNT_TEMPLATE__ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooAccount=et.getOdooAccount();
            if(ObjectUtils.isEmpty(odooAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template majorEntity=accountAccountTemplateService.get(et.getAccountId());
                et.setOdooAccount(majorEntity);
                odooAccount=majorEntity;
            }
            et.setAccountIdText(odooAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL_TEMPLATE__ACCOUNT_ACCOUNT_TEMPLATE__SECOND_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getSecondAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooSecondAccount=et.getOdooSecondAccount();
            if(ObjectUtils.isEmpty(odooSecondAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template majorEntity=accountAccountTemplateService.get(et.getSecondAccountId());
                et.setOdooSecondAccount(majorEntity);
                odooSecondAccount=majorEntity;
            }
            et.setSecondAccountIdText(odooSecondAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL_TEMPLATE__ACCOUNT_CHART_TEMPLATE__CHART_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getChartTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooChartTemplate=et.getOdooChartTemplate();
            if(ObjectUtils.isEmpty(odooChartTemplate)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template majorEntity=accountChartTemplateService.get(et.getChartTemplateId());
                et.setOdooChartTemplate(majorEntity);
                odooChartTemplate=majorEntity;
            }
            et.setChartTemplateIdText(odooChartTemplate.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL_TEMPLATE__ACCOUNT_TAX_TEMPLATE__SECOND_TAX_ID]
        if(!ObjectUtils.isEmpty(et.getSecondTaxId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_template odooSecondTax=et.getOdooSecondTax();
            if(ObjectUtils.isEmpty(odooSecondTax)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_template majorEntity=accountTaxTemplateService.get(et.getSecondTaxId());
                et.setOdooSecondTax(majorEntity);
                odooSecondTax=majorEntity;
            }
            et.setSecondTaxIdText(odooSecondTax.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL_TEMPLATE__ACCOUNT_TAX_TEMPLATE__TAX_ID]
        if(!ObjectUtils.isEmpty(et.getTaxId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_template odooTax=et.getOdooTax();
            if(ObjectUtils.isEmpty(odooTax)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_template majorEntity=accountTaxTemplateService.get(et.getTaxId());
                et.setOdooTax(majorEntity);
                odooTax=majorEntity;
            }
            et.setTaxIdText(odooTax.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL_TEMPLATE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL_TEMPLATE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_reconcile_model_template> getAccountReconcileModelTemplateByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_reconcile_model_template> getAccountReconcileModelTemplateByEntities(List<Account_reconcile_model_template> entities) {
        List ids =new ArrayList();
        for(Account_reconcile_model_template entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




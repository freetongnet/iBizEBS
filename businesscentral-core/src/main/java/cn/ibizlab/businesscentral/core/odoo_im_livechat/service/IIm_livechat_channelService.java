package cn.ibizlab.businesscentral.core.odoo_im_livechat.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_channelSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Im_livechat_channel] 服务对象接口
 */
public interface IIm_livechat_channelService extends IService<Im_livechat_channel>{

    boolean create(Im_livechat_channel et) ;
    void createBatch(List<Im_livechat_channel> list) ;
    boolean update(Im_livechat_channel et) ;
    void updateBatch(List<Im_livechat_channel> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Im_livechat_channel get(Long key) ;
    Im_livechat_channel getDraft(Im_livechat_channel et) ;
    boolean checkKey(Im_livechat_channel et) ;
    boolean save(Im_livechat_channel et) ;
    void saveBatch(List<Im_livechat_channel> list) ;
    Page<Im_livechat_channel> searchDefault(Im_livechat_channelSearchContext context) ;
    List<Im_livechat_channel> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Im_livechat_channel> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Im_livechat_channel> getImLivechatChannelByIds(List<Long> ids) ;
    List<Im_livechat_channel> getImLivechatChannelByEntities(List<Im_livechat_channel> entities) ;
}



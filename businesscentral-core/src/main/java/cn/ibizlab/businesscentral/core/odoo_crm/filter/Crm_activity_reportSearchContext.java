package cn.ibizlab.businesscentral.core.odoo_crm.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_activity_report;
/**
 * 关系型数据实体[Crm_activity_report] 查询条件对象
 */
@Slf4j
@Data
public class Crm_activity_reportSearchContext extends QueryWrapperContext<Crm_activity_report> {

	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_team_id_text_eq;//[销售团队]
	public void setN_team_id_text_eq(String n_team_id_text_eq) {
        this.n_team_id_text_eq = n_team_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_eq)){
            this.getSearchCond().eq("team_id_text", n_team_id_text_eq);
        }
    }
	private String n_team_id_text_like;//[销售团队]
	public void setN_team_id_text_like(String n_team_id_text_like) {
        this.n_team_id_text_like = n_team_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_like)){
            this.getSearchCond().like("team_id_text", n_team_id_text_like);
        }
    }
	private String n_mail_activity_type_id_text_eq;//[活动类型]
	public void setN_mail_activity_type_id_text_eq(String n_mail_activity_type_id_text_eq) {
        this.n_mail_activity_type_id_text_eq = n_mail_activity_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_mail_activity_type_id_text_eq)){
            this.getSearchCond().eq("mail_activity_type_id_text", n_mail_activity_type_id_text_eq);
        }
    }
	private String n_mail_activity_type_id_text_like;//[活动类型]
	public void setN_mail_activity_type_id_text_like(String n_mail_activity_type_id_text_like) {
        this.n_mail_activity_type_id_text_like = n_mail_activity_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_mail_activity_type_id_text_like)){
            this.getSearchCond().like("mail_activity_type_id_text", n_mail_activity_type_id_text_like);
        }
    }
	private String n_lead_id_text_eq;//[线索]
	public void setN_lead_id_text_eq(String n_lead_id_text_eq) {
        this.n_lead_id_text_eq = n_lead_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_lead_id_text_eq)){
            this.getSearchCond().eq("lead_id_text", n_lead_id_text_eq);
        }
    }
	private String n_lead_id_text_like;//[线索]
	public void setN_lead_id_text_like(String n_lead_id_text_like) {
        this.n_lead_id_text_like = n_lead_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_lead_id_text_like)){
            this.getSearchCond().like("lead_id_text", n_lead_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[销售员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[销售员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_subtype_id_text_eq;//[子类型]
	public void setN_subtype_id_text_eq(String n_subtype_id_text_eq) {
        this.n_subtype_id_text_eq = n_subtype_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_subtype_id_text_eq)){
            this.getSearchCond().eq("subtype_id_text", n_subtype_id_text_eq);
        }
    }
	private String n_subtype_id_text_like;//[子类型]
	public void setN_subtype_id_text_like(String n_subtype_id_text_like) {
        this.n_subtype_id_text_like = n_subtype_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_subtype_id_text_like)){
            this.getSearchCond().like("subtype_id_text", n_subtype_id_text_like);
        }
    }
	private String n_country_id_text_eq;//[国家]
	public void setN_country_id_text_eq(String n_country_id_text_eq) {
        this.n_country_id_text_eq = n_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_eq)){
            this.getSearchCond().eq("country_id_text", n_country_id_text_eq);
        }
    }
	private String n_country_id_text_like;//[国家]
	public void setN_country_id_text_like(String n_country_id_text_like) {
        this.n_country_id_text_like = n_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_like)){
            this.getSearchCond().like("country_id_text", n_country_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[业务合作伙伴/客户]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[业务合作伙伴/客户]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_stage_id_text_eq;//[阶段]
	public void setN_stage_id_text_eq(String n_stage_id_text_eq) {
        this.n_stage_id_text_eq = n_stage_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_stage_id_text_eq)){
            this.getSearchCond().eq("stage_id_text", n_stage_id_text_eq);
        }
    }
	private String n_stage_id_text_like;//[阶段]
	public void setN_stage_id_text_like(String n_stage_id_text_like) {
        this.n_stage_id_text_like = n_stage_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_stage_id_text_like)){
            this.getSearchCond().like("stage_id_text", n_stage_id_text_like);
        }
    }
	private String n_author_id_text_eq;//[创建人]
	public void setN_author_id_text_eq(String n_author_id_text_eq) {
        this.n_author_id_text_eq = n_author_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_author_id_text_eq)){
            this.getSearchCond().eq("author_id_text", n_author_id_text_eq);
        }
    }
	private String n_author_id_text_like;//[创建人]
	public void setN_author_id_text_like(String n_author_id_text_like) {
        this.n_author_id_text_like = n_author_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_author_id_text_like)){
            this.getSearchCond().like("author_id_text", n_author_id_text_like);
        }
    }
	private Long n_author_id_eq;//[创建人]
	public void setN_author_id_eq(Long n_author_id_eq) {
        this.n_author_id_eq = n_author_id_eq;
        if(!ObjectUtils.isEmpty(this.n_author_id_eq)){
            this.getSearchCond().eq("author_id", n_author_id_eq);
        }
    }
	private Long n_country_id_eq;//[国家]
	public void setN_country_id_eq(Long n_country_id_eq) {
        this.n_country_id_eq = n_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_eq)){
            this.getSearchCond().eq("country_id", n_country_id_eq);
        }
    }
	private Long n_stage_id_eq;//[阶段]
	public void setN_stage_id_eq(Long n_stage_id_eq) {
        this.n_stage_id_eq = n_stage_id_eq;
        if(!ObjectUtils.isEmpty(this.n_stage_id_eq)){
            this.getSearchCond().eq("stage_id", n_stage_id_eq);
        }
    }
	private Long n_subtype_id_eq;//[子类型]
	public void setN_subtype_id_eq(Long n_subtype_id_eq) {
        this.n_subtype_id_eq = n_subtype_id_eq;
        if(!ObjectUtils.isEmpty(this.n_subtype_id_eq)){
            this.getSearchCond().eq("subtype_id", n_subtype_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_lead_id_eq;//[线索]
	public void setN_lead_id_eq(Long n_lead_id_eq) {
        this.n_lead_id_eq = n_lead_id_eq;
        if(!ObjectUtils.isEmpty(this.n_lead_id_eq)){
            this.getSearchCond().eq("lead_id", n_lead_id_eq);
        }
    }
	private Long n_mail_activity_type_id_eq;//[活动类型]
	public void setN_mail_activity_type_id_eq(Long n_mail_activity_type_id_eq) {
        this.n_mail_activity_type_id_eq = n_mail_activity_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mail_activity_type_id_eq)){
            this.getSearchCond().eq("mail_activity_type_id", n_mail_activity_type_id_eq);
        }
    }
	private Long n_user_id_eq;//[销售员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_partner_id_eq;//[业务合作伙伴/客户]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_team_id_eq;//[销售团队]
	public void setN_team_id_eq(Long n_team_id_eq) {
        this.n_team_id_eq = n_team_id_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_eq)){
            this.getSearchCond().eq("team_id", n_team_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




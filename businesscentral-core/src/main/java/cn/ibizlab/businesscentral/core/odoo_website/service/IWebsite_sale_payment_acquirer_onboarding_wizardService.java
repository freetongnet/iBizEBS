package cn.ibizlab.businesscentral.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_sale_payment_acquirer_onboarding_wizardSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Website_sale_payment_acquirer_onboarding_wizard] 服务对象接口
 */
public interface IWebsite_sale_payment_acquirer_onboarding_wizardService extends IService<Website_sale_payment_acquirer_onboarding_wizard>{

    boolean create(Website_sale_payment_acquirer_onboarding_wizard et) ;
    void createBatch(List<Website_sale_payment_acquirer_onboarding_wizard> list) ;
    boolean update(Website_sale_payment_acquirer_onboarding_wizard et) ;
    void updateBatch(List<Website_sale_payment_acquirer_onboarding_wizard> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Website_sale_payment_acquirer_onboarding_wizard get(Long key) ;
    Website_sale_payment_acquirer_onboarding_wizard getDraft(Website_sale_payment_acquirer_onboarding_wizard et) ;
    boolean checkKey(Website_sale_payment_acquirer_onboarding_wizard et) ;
    boolean save(Website_sale_payment_acquirer_onboarding_wizard et) ;
    void saveBatch(List<Website_sale_payment_acquirer_onboarding_wizard> list) ;
    Page<Website_sale_payment_acquirer_onboarding_wizard> searchDefault(Website_sale_payment_acquirer_onboarding_wizardSearchContext context) ;
    List<Website_sale_payment_acquirer_onboarding_wizard> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Website_sale_payment_acquirer_onboarding_wizard> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Website_sale_payment_acquirer_onboarding_wizard> getWebsiteSalePaymentAcquirerOnboardingWizardByIds(List<Long> ids) ;
    List<Website_sale_payment_acquirer_onboarding_wizard> getWebsiteSalePaymentAcquirerOnboardingWizardByEntities(List<Website_sale_payment_acquirer_onboarding_wizard> entities) ;
}



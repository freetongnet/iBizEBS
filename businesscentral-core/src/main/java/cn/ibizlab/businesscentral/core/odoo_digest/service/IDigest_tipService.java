package cn.ibizlab.businesscentral.core.odoo_digest.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.businesscentral.core.odoo_digest.filter.Digest_tipSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Digest_tip] 服务对象接口
 */
public interface IDigest_tipService extends IService<Digest_tip>{

    boolean create(Digest_tip et) ;
    void createBatch(List<Digest_tip> list) ;
    boolean update(Digest_tip et) ;
    void updateBatch(List<Digest_tip> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Digest_tip get(Long key) ;
    Digest_tip getDraft(Digest_tip et) ;
    boolean checkKey(Digest_tip et) ;
    boolean save(Digest_tip et) ;
    void saveBatch(List<Digest_tip> list) ;
    Page<Digest_tip> searchDefault(Digest_tipSearchContext context) ;
    List<Digest_tip> selectByGroupId(Long id);
    void resetByGroupId(Long id);
    void resetByGroupId(Collection<Long> ids);
    void removeByGroupId(Long id);
    List<Digest_tip> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Digest_tip> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Digest_tip> getDigestTipByIds(List<Long> ids) ;
    List<Digest_tip> getDigestTipByEntities(List<Digest_tip> entities) ;
}



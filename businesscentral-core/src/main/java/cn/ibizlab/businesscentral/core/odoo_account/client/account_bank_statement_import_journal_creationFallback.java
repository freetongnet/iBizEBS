package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_import_journal_creation;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_import_journal_creationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_bank_statement_import_journal_creation] 服务对象接口
 */
@Component
public class account_bank_statement_import_journal_creationFallback implements account_bank_statement_import_journal_creationFeignClient{

    public Account_bank_statement_import_journal_creation create(Account_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
            return null;
     }
    public Boolean createBatch(List<Account_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations){
            return false;
     }

    public Account_bank_statement_import_journal_creation get(Long id){
            return null;
     }





    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_bank_statement_import_journal_creation update(Long id, Account_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
            return null;
     }
    public Boolean updateBatch(List<Account_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations){
            return false;
     }


    public Page<Account_bank_statement_import_journal_creation> search(Account_bank_statement_import_journal_creationSearchContext context){
            return null;
     }


    public Page<Account_bank_statement_import_journal_creation> select(){
            return null;
     }

    public Account_bank_statement_import_journal_creation getDraft(){
            return null;
    }



    public Boolean checkKey(Account_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
            return false;
     }


    public Boolean save(Account_bank_statement_import_journal_creation account_bank_statement_import_journal_creation){
            return false;
     }
    public Boolean saveBatch(List<Account_bank_statement_import_journal_creation> account_bank_statement_import_journal_creations){
            return false;
     }

    public Page<Account_bank_statement_import_journal_creation> searchDefault(Account_bank_statement_import_journal_creationSearchContext context){
            return null;
     }


}

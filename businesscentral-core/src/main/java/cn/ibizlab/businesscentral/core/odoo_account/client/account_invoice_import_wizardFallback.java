package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_import_wizard;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_import_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_invoice_import_wizard] 服务对象接口
 */
@Component
public class account_invoice_import_wizardFallback implements account_invoice_import_wizardFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_invoice_import_wizard create(Account_invoice_import_wizard account_invoice_import_wizard){
            return null;
     }
    public Boolean createBatch(List<Account_invoice_import_wizard> account_invoice_import_wizards){
            return false;
     }



    public Account_invoice_import_wizard get(Long id){
            return null;
     }


    public Account_invoice_import_wizard update(Long id, Account_invoice_import_wizard account_invoice_import_wizard){
            return null;
     }
    public Boolean updateBatch(List<Account_invoice_import_wizard> account_invoice_import_wizards){
            return false;
     }


    public Page<Account_invoice_import_wizard> search(Account_invoice_import_wizardSearchContext context){
            return null;
     }


    public Page<Account_invoice_import_wizard> select(){
            return null;
     }

    public Account_invoice_import_wizard getDraft(){
            return null;
    }



    public Boolean checkKey(Account_invoice_import_wizard account_invoice_import_wizard){
            return false;
     }


    public Boolean save(Account_invoice_import_wizard account_invoice_import_wizard){
            return false;
     }
    public Boolean saveBatch(List<Account_invoice_import_wizard> account_invoice_import_wizards){
            return false;
     }

    public Page<Account_invoice_import_wizard> searchDefault(Account_invoice_import_wizardSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_production_lotSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_production_lotMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[批次/序列号] 服务对象接口实现
 */
@Slf4j
@Service("Stock_production_lotServiceImpl")
public class Stock_production_lotServiceImpl extends EBSServiceImpl<Stock_production_lotMapper, Stock_production_lot> implements IStock_production_lotService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produce_lineService mrpProductProduceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_product_produceService mrpProductProduceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_unbuildService mrpUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workorderService mrpWorkorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_lineService repairLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventory_lineService stockInventoryLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventoryService stockInventoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService stockMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantService stockQuantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService stockScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.production.lot" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_production_lot et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_production_lotService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_production_lot> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_production_lot et) {
        Stock_production_lot old = new Stock_production_lot() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_production_lotService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_production_lotService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_production_lot> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        mrpProductProduceLineService.resetByLotId(key);
        mrpProductProduceService.resetByLotId(key);
        mrpUnbuildService.resetByLotId(key);
        mrpWorkorderService.resetByFinalLotId(key);
        repairLineService.resetByLotId(key);
        repairOrderService.resetByLotId(key);
        stockInventoryLineService.resetByProdLotId(key);
        stockInventoryService.resetByLotId(key);
        stockMoveLineService.resetByLotId(key);
        stockMoveLineService.resetByLotProducedId(key);
        if(!ObjectUtils.isEmpty(stockQuantService.selectByLotId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_quant]数据，无法删除!","","");
        stockScrapService.resetByLotId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        mrpProductProduceLineService.resetByLotId(idList);
        mrpProductProduceService.resetByLotId(idList);
        mrpUnbuildService.resetByLotId(idList);
        mrpWorkorderService.resetByFinalLotId(idList);
        repairLineService.resetByLotId(idList);
        repairOrderService.resetByLotId(idList);
        stockInventoryLineService.resetByProdLotId(idList);
        stockInventoryService.resetByLotId(idList);
        stockMoveLineService.resetByLotId(idList);
        stockMoveLineService.resetByLotProducedId(idList);
        if(!ObjectUtils.isEmpty(stockQuantService.selectByLotId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_quant]数据，无法删除!","","");
        stockScrapService.resetByLotId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_production_lot get(Long key) {
        Stock_production_lot et = getById(key);
        if(et==null){
            et=new Stock_production_lot();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_production_lot getDraft(Stock_production_lot et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_production_lot et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_production_lot et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_production_lot et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_production_lot> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_production_lot> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_production_lot> selectByUseNextOnWorkOrderId(Long id) {
        return baseMapper.selectByUseNextOnWorkOrderId(id);
    }
    @Override
    public void resetByUseNextOnWorkOrderId(Long id) {
        this.update(new UpdateWrapper<Stock_production_lot>().set("use_next_on_work_order_id",null).eq("use_next_on_work_order_id",id));
    }

    @Override
    public void resetByUseNextOnWorkOrderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_production_lot>().set("use_next_on_work_order_id",null).in("use_next_on_work_order_id",ids));
    }

    @Override
    public void removeByUseNextOnWorkOrderId(Long id) {
        this.remove(new QueryWrapper<Stock_production_lot>().eq("use_next_on_work_order_id",id));
    }

	@Override
    public List<Stock_production_lot> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Stock_production_lot>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_production_lot>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Stock_production_lot>().eq("product_id",id));
    }

	@Override
    public List<Stock_production_lot> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_production_lot>().eq("create_uid",id));
    }

	@Override
    public List<Stock_production_lot> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_production_lot>().eq("write_uid",id));
    }

	@Override
    public List<Stock_production_lot> selectByProductUomId(Long id) {
        return baseMapper.selectByProductUomId(id);
    }
    @Override
    public void resetByProductUomId(Long id) {
        this.update(new UpdateWrapper<Stock_production_lot>().set("product_uom_id",null).eq("product_uom_id",id));
    }

    @Override
    public void resetByProductUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_production_lot>().set("product_uom_id",null).in("product_uom_id",ids));
    }

    @Override
    public void removeByProductUomId(Long id) {
        this.remove(new QueryWrapper<Stock_production_lot>().eq("product_uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_production_lot> searchDefault(Stock_production_lotSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_production_lot> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_production_lot>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_production_lot et){
        //实体关系[DER1N_STOCK_PRODUCTION_LOT__MRP_WORKORDER__USE_NEXT_ON_WORK_ORDER_ID]
        if(!ObjectUtils.isEmpty(et.getUseNextOnWorkOrderId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder odooUseNextOnWorkOrder=et.getOdooUseNextOnWorkOrder();
            if(ObjectUtils.isEmpty(odooUseNextOnWorkOrder)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder majorEntity=mrpWorkorderService.get(et.getUseNextOnWorkOrderId());
                et.setOdooUseNextOnWorkOrder(majorEntity);
                odooUseNextOnWorkOrder=majorEntity;
            }
            et.setUseNextOnWorkOrderIdText(odooUseNextOnWorkOrder.getName());
        }
        //实体关系[DER1N_STOCK_PRODUCTION_LOT__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_STOCK_PRODUCTION_LOT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_PRODUCTION_LOT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_PRODUCTION_LOT__UOM_UOM__PRODUCT_UOM_ID]
        if(!ObjectUtils.isEmpty(et.getProductUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUomId());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomIdText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_production_lot> getStockProductionLotByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_production_lot> getStockProductionLotByEntities(List<Stock_production_lot> entities) {
        List ids =new ArrayList();
        for(Stock_production_lot entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




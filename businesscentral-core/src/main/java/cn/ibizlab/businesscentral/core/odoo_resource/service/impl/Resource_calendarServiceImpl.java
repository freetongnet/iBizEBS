package cn.ibizlab.businesscentral.core.odoo_resource.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_calendarSearchContext;
import cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendarService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_resource.mapper.Resource_calendarMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[资源工作时间] 服务对象接口实现
 */
@Slf4j
@Service("Resource_calendarServiceImpl")
public class Resource_calendarServiceImpl extends EBSServiceImpl<Resource_calendarMapper, Resource_calendar> implements IResource_calendarService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_automationService baseAutomationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contractService hrContractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workcenterService mrpWorkcenterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_projectService projectProjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendar_attendanceService resourceCalendarAttendanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_calendar_leavesService resourceCalendarLeavesService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_mixinService resourceMixinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_resourceService resourceResourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_resource.service.IResource_testService resourceTestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "resource.calendar" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Resource_calendar et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IResource_calendarService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Resource_calendar> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Resource_calendar et) {
        Resource_calendar old = new Resource_calendar() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IResource_calendarService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IResource_calendarService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Resource_calendar> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        baseAutomationService.resetByTrgDateCalendarId(key);
        hrContractService.resetByResourceCalendarId(key);
        hrEmployeeService.resetByResourceCalendarId(key);
        mrpWorkcenterService.resetByResourceCalendarId(key);
        projectProjectService.resetByResourceCalendarId(key);
        resourceCalendarAttendanceService.removeByCalendarId(key);
        resourceCalendarLeavesService.resetByCalendarId(key);
        resourceMixinService.resetByResourceCalendarId(key);
        resourceResourceService.resetByCalendarId(key);
        resourceTestService.resetByResourceCalendarId(key);
        if(!ObjectUtils.isEmpty(resCompanyService.selectByResourceCalendarId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Res_company]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        baseAutomationService.resetByTrgDateCalendarId(idList);
        hrContractService.resetByResourceCalendarId(idList);
        hrEmployeeService.resetByResourceCalendarId(idList);
        mrpWorkcenterService.resetByResourceCalendarId(idList);
        projectProjectService.resetByResourceCalendarId(idList);
        resourceCalendarAttendanceService.removeByCalendarId(idList);
        resourceCalendarLeavesService.resetByCalendarId(idList);
        resourceMixinService.resetByResourceCalendarId(idList);
        resourceResourceService.resetByCalendarId(idList);
        resourceTestService.resetByResourceCalendarId(idList);
        if(!ObjectUtils.isEmpty(resCompanyService.selectByResourceCalendarId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Res_company]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Resource_calendar get(Long key) {
        Resource_calendar et = getById(key);
        if(et==null){
            et=new Resource_calendar();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Resource_calendar getDraft(Resource_calendar et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Resource_calendar et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Resource_calendar et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Resource_calendar et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Resource_calendar> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Resource_calendar> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Resource_calendar> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Resource_calendar>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Resource_calendar>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Resource_calendar>().eq("company_id",id));
    }

	@Override
    public List<Resource_calendar> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Resource_calendar>().eq("create_uid",id));
    }

	@Override
    public List<Resource_calendar> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Resource_calendar>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Resource_calendar> searchDefault(Resource_calendarSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Resource_calendar> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Resource_calendar>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Resource_calendar et){
        //实体关系[DER1N_RESOURCE_CALENDAR__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_RESOURCE_CALENDAR__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_RESOURCE_CALENDAR__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Resource_calendar> getResourceCalendarByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Resource_calendar> getResourceCalendarByEntities(List<Resource_calendar> entities) {
        List ids =new ArrayList();
        for(Resource_calendar entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




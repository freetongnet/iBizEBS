package cn.ibizlab.businesscentral.core.odoo_base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_automation;
/**
 * 关系型数据实体[Base_automation] 查询条件对象
 */
@Slf4j
@Data
public class Base_automationSearchContext extends QueryWrapperContext<Base_automation> {

	private String n_usage_eq;//[用途]
	public void setN_usage_eq(String n_usage_eq) {
        this.n_usage_eq = n_usage_eq;
        if(!ObjectUtils.isEmpty(this.n_usage_eq)){
            this.getSearchCond().eq("usage", n_usage_eq);
        }
    }
	private String n_trg_date_range_type_eq;//[延迟类型]
	public void setN_trg_date_range_type_eq(String n_trg_date_range_type_eq) {
        this.n_trg_date_range_type_eq = n_trg_date_range_type_eq;
        if(!ObjectUtils.isEmpty(this.n_trg_date_range_type_eq)){
            this.getSearchCond().eq("trg_date_range_type", n_trg_date_range_type_eq);
        }
    }
	private String n_activity_user_type_eq;//[活动用户类型]
	public void setN_activity_user_type_eq(String n_activity_user_type_eq) {
        this.n_activity_user_type_eq = n_activity_user_type_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_user_type_eq)){
            this.getSearchCond().eq("activity_user_type", n_activity_user_type_eq);
        }
    }
	private String n_binding_type_eq;//[绑定类型]
	public void setN_binding_type_eq(String n_binding_type_eq) {
        this.n_binding_type_eq = n_binding_type_eq;
        if(!ObjectUtils.isEmpty(this.n_binding_type_eq)){
            this.getSearchCond().eq("binding_type", n_binding_type_eq);
        }
    }
	private String n_name_like;//[动作名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_state_eq;//[待办的行动]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_activity_date_deadline_range_type_eq;//[到期类型]
	public void setN_activity_date_deadline_range_type_eq(String n_activity_date_deadline_range_type_eq) {
        this.n_activity_date_deadline_range_type_eq = n_activity_date_deadline_range_type_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_date_deadline_range_type_eq)){
            this.getSearchCond().eq("activity_date_deadline_range_type", n_activity_date_deadline_range_type_eq);
        }
    }
	private String n_trigger_eq;//[触发条件]
	public void setN_trigger_eq(String n_trigger_eq) {
        this.n_trigger_eq = n_trigger_eq;
        if(!ObjectUtils.isEmpty(this.n_trigger_eq)){
            this.getSearchCond().eq("trigger", n_trigger_eq);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_trg_date_calendar_id_text_eq;//[使用日历]
	public void setN_trg_date_calendar_id_text_eq(String n_trg_date_calendar_id_text_eq) {
        this.n_trg_date_calendar_id_text_eq = n_trg_date_calendar_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_trg_date_calendar_id_text_eq)){
            this.getSearchCond().eq("trg_date_calendar_id_text", n_trg_date_calendar_id_text_eq);
        }
    }
	private String n_trg_date_calendar_id_text_like;//[使用日历]
	public void setN_trg_date_calendar_id_text_like(String n_trg_date_calendar_id_text_like) {
        this.n_trg_date_calendar_id_text_like = n_trg_date_calendar_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_trg_date_calendar_id_text_like)){
            this.getSearchCond().like("trg_date_calendar_id_text", n_trg_date_calendar_id_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_trg_date_calendar_id_eq;//[使用日历]
	public void setN_trg_date_calendar_id_eq(Long n_trg_date_calendar_id_eq) {
        this.n_trg_date_calendar_id_eq = n_trg_date_calendar_id_eq;
        if(!ObjectUtils.isEmpty(this.n_trg_date_calendar_id_eq)){
            this.getSearchCond().eq("trg_date_calendar_id", n_trg_date_calendar_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




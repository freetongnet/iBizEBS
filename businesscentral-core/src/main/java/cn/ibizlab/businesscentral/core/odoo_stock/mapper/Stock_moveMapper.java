package cn.ibizlab.businesscentral.core.odoo_stock.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_moveSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Stock_moveMapper extends BaseMapper<Stock_move>{

    Page<Stock_move> searchDefault(IPage page, @Param("srf") Stock_moveSearchContext context, @Param("ew") Wrapper<Stock_move> wrapper) ;
    @Override
    Stock_move selectById(Serializable id);
    @Override
    int insert(Stock_move entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Stock_move entity);
    @Override
    int update(@Param(Constants.ENTITY) Stock_move entity, @Param("ew") Wrapper<Stock_move> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Stock_move> selectByBomLineId(@Param("id") Serializable id) ;

    List<Stock_move> selectByCreatedProductionId(@Param("id") Serializable id) ;

    List<Stock_move> selectByProductionId(@Param("id") Serializable id) ;

    List<Stock_move> selectByRawMaterialProductionId(@Param("id") Serializable id) ;

    List<Stock_move> selectByOperationId(@Param("id") Serializable id) ;

    List<Stock_move> selectByConsumeUnbuildId(@Param("id") Serializable id) ;

    List<Stock_move> selectByUnbuildId(@Param("id") Serializable id) ;

    List<Stock_move> selectByWorkorderId(@Param("id") Serializable id) ;

    List<Stock_move> selectByProductPackaging(@Param("id") Serializable id) ;

    List<Stock_move> selectByProductId(@Param("id") Serializable id) ;

    List<Stock_move> selectByCreatedPurchaseLineId(@Param("id") Serializable id) ;

    List<Stock_move> selectByPurchaseLineId(@Param("id") Serializable id) ;

    List<Stock_move> selectByRepairId(@Param("id") Serializable id) ;

    List<Stock_move> selectByCompanyId(@Param("id") Serializable id) ;

    List<Stock_move> selectByPartnerId(@Param("id") Serializable id) ;

    List<Stock_move> selectByRestrictPartnerId(@Param("id") Serializable id) ;

    List<Stock_move> selectByCreateUid(@Param("id") Serializable id) ;

    List<Stock_move> selectByWriteUid(@Param("id") Serializable id) ;

    List<Stock_move> selectBySaleLineId(@Param("id") Serializable id) ;

    List<Stock_move> selectByInventoryId(@Param("id") Serializable id) ;

    List<Stock_move> selectByLocationDestId(@Param("id") Serializable id) ;

    List<Stock_move> selectByLocationId(@Param("id") Serializable id) ;

    List<Stock_move> selectByOriginReturnedMoveId(@Param("id") Serializable id) ;

    List<Stock_move> selectByPackageLevelId(@Param("id") Serializable id) ;

    List<Stock_move> selectByPickingTypeId(@Param("id") Serializable id) ;

    List<Stock_move> selectByPickingId(@Param("id") Serializable id) ;

    List<Stock_move> selectByRuleId(@Param("id") Serializable id) ;

    List<Stock_move> selectByWarehouseId(@Param("id") Serializable id) ;

    List<Stock_move> selectByProductUom(@Param("id") Serializable id) ;


}

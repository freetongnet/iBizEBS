package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.odoo_base.service.impl.Res_usersServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[用户] 自定义服务对象
 */
@Slf4j
@Primary
@Service("Res_usersExService")
public class Res_usersExService extends Res_usersServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[A]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Res_users a(Res_users et) {
        return super.a(et);
    }
}


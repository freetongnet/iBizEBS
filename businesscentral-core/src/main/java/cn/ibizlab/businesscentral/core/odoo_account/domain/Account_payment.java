package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[付款]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_PAYMENT",resultMap = "Account_paymentResultMap")
public class Account_payment extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 操作编号
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 付款类型
     */
    @DEField(name = "payment_type")
    @TableField(value = "payment_type")
    @JSONField(name = "payment_type")
    @JsonProperty("payment_type")
    private String paymentType;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 有发票
     */
    @TableField(exist = false)
    @JSONField(name = "has_invoices")
    @JsonProperty("has_invoices")
    private Boolean hasInvoices;
    /**
     * 付款日期
     */
    @DEField(name = "payment_date")
    @TableField(value = "payment_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "payment_date" , format="yyyy-MM-dd")
    @JsonProperty("payment_date")
    private Timestamp paymentDate;
    /**
     * 多
     */
    @TableField(value = "multi")
    @JSONField(name = "multi")
    @JsonProperty("multi")
    private Boolean multi;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 凭证已核销
     */
    @TableField(exist = false)
    @JSONField(name = "move_reconciled")
    @JsonProperty("move_reconciled")
    private Boolean moveReconciled;
    /**
     * 目标账户
     */
    @TableField(exist = false)
    @JSONField(name = "destination_account_id")
    @JsonProperty("destination_account_id")
    private Integer destinationAccountId;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 付款差异
     */
    @TableField(exist = false)
    @JSONField(name = "payment_difference")
    @JsonProperty("payment_difference")
    private BigDecimal paymentDifference;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 需要一个动作消息的编码
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 付款差异处理
     */
    @DEField(name = "payment_difference_handling")
    @TableField(value = "payment_difference_handling")
    @JSONField(name = "payment_difference_handling")
    @JsonProperty("payment_difference_handling")
    private String paymentDifferenceHandling;
    /**
     * 已核销的发票
     */
    @TableField(exist = false)
    @JSONField(name = "reconciled_invoice_ids")
    @JsonProperty("reconciled_invoice_ids")
    private String reconciledInvoiceIds;
    /**
     * 付款参考
     */
    @DEField(name = "payment_reference")
    @TableField(value = "payment_reference")
    @JSONField(name = "payment_reference")
    @JsonProperty("payment_reference")
    private String paymentReference;
    /**
     * 日记账项目标签
     */
    @DEField(name = "writeoff_label")
    @TableField(value = "writeoff_label")
    @JSONField(name = "writeoff_label")
    @JsonProperty("writeoff_label")
    private String writeoffLabel;
    /**
     * 分录行
     */
    @TableField(exist = false)
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;
    /**
     * 网站信息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 付款金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private BigDecimal amount;
    /**
     * 显示合作伙伴银行账户
     */
    @TableField(exist = false)
    @JSONField(name = "show_partner_bank_account")
    @JsonProperty("show_partner_bank_account")
    private Boolean showPartnerBankAccount;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 业务伙伴类型
     */
    @DEField(name = "partner_type")
    @TableField(value = "partner_type")
    @JSONField(name = "partner_type")
    @JsonProperty("partner_type")
    private String partnerType;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 备忘
     */
    @TableField(value = "communication")
    @JSONField(name = "communication")
    @JsonProperty("communication")
    private String communication;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 隐藏付款方式
     */
    @TableField(exist = false)
    @JSONField(name = "hide_payment_method")
    @JsonProperty("hide_payment_method")
    private Boolean hidePaymentMethod;
    /**
     * 日记账分录名称
     */
    @DEField(name = "move_name")
    @TableField(value = "move_name")
    @JSONField(name = "move_name")
    @JsonProperty("move_name")
    private String moveName;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 转账到
     */
    @TableField(exist = false)
    @JSONField(name = "destination_journal_id_text")
    @JsonProperty("destination_journal_id_text")
    private String destinationJournalIdText;
    /**
     * 代码
     */
    @TableField(exist = false)
    @JSONField(name = "payment_method_code")
    @JsonProperty("payment_method_code")
    private String paymentMethodCode;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 付款日记账
     */
    @TableField(exist = false)
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 差异科目
     */
    @TableField(exist = false)
    @JSONField(name = "writeoff_account_id_text")
    @JsonProperty("writeoff_account_id_text")
    private String writeoffAccountIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 付款方法类型
     */
    @TableField(exist = false)
    @JSONField(name = "payment_method_id_text")
    @JsonProperty("payment_method_id_text")
    private String paymentMethodIdText;
    /**
     * 保存的付款令牌
     */
    @TableField(exist = false)
    @JSONField(name = "payment_token_id_text")
    @JsonProperty("payment_token_id_text")
    private String paymentTokenIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 保存的付款令牌
     */
    @DEField(name = "payment_token_id")
    @TableField(value = "payment_token_id")
    @JSONField(name = "payment_token_id")
    @JsonProperty("payment_token_id")
    private Long paymentTokenId;
    /**
     * 付款交易
     */
    @DEField(name = "payment_transaction_id")
    @TableField(value = "payment_transaction_id")
    @JSONField(name = "payment_transaction_id")
    @JsonProperty("payment_transaction_id")
    private Long paymentTransactionId;
    /**
     * 差异科目
     */
    @DEField(name = "writeoff_account_id")
    @TableField(value = "writeoff_account_id")
    @JSONField(name = "writeoff_account_id")
    @JsonProperty("writeoff_account_id")
    private Long writeoffAccountId;
    /**
     * 收款银行账号
     */
    @DEField(name = "partner_bank_account_id")
    @TableField(value = "partner_bank_account_id")
    @JSONField(name = "partner_bank_account_id")
    @JsonProperty("partner_bank_account_id")
    private Long partnerBankAccountId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 付款方法类型
     */
    @DEField(name = "payment_method_id")
    @TableField(value = "payment_method_id")
    @JSONField(name = "payment_method_id")
    @JsonProperty("payment_method_id")
    private Long paymentMethodId;
    /**
     * 转账到
     */
    @DEField(name = "destination_journal_id")
    @TableField(value = "destination_journal_id")
    @JSONField(name = "destination_journal_id")
    @JsonProperty("destination_journal_id")
    private Long destinationJournalId;
    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 付款日记账
     */
    @DEField(name = "journal_id")
    @TableField(value = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Long journalId;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooWriteoffAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooDestinationJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_method odooPaymentMethod;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_token odooPaymentToken;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_payment.domain.Payment_transaction odooPaymentTransaction;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank odooPartnerBankAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [付款类型]
     */
    public void setPaymentType(String paymentType){
        this.paymentType = paymentType ;
        this.modify("payment_type",paymentType);
    }

    /**
     * 设置 [付款日期]
     */
    public void setPaymentDate(Timestamp paymentDate){
        this.paymentDate = paymentDate ;
        this.modify("payment_date",paymentDate);
    }

    /**
     * 格式化日期 [付款日期]
     */
    public String formatPaymentDate(){
        if (this.paymentDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(paymentDate);
    }
    /**
     * 设置 [多]
     */
    public void setMulti(Boolean multi){
        this.multi = multi ;
        this.modify("multi",multi);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [付款差异处理]
     */
    public void setPaymentDifferenceHandling(String paymentDifferenceHandling){
        this.paymentDifferenceHandling = paymentDifferenceHandling ;
        this.modify("payment_difference_handling",paymentDifferenceHandling);
    }

    /**
     * 设置 [付款参考]
     */
    public void setPaymentReference(String paymentReference){
        this.paymentReference = paymentReference ;
        this.modify("payment_reference",paymentReference);
    }

    /**
     * 设置 [日记账项目标签]
     */
    public void setWriteoffLabel(String writeoffLabel){
        this.writeoffLabel = writeoffLabel ;
        this.modify("writeoff_label",writeoffLabel);
    }

    /**
     * 设置 [付款金额]
     */
    public void setAmount(BigDecimal amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [业务伙伴类型]
     */
    public void setPartnerType(String partnerType){
        this.partnerType = partnerType ;
        this.modify("partner_type",partnerType);
    }

    /**
     * 设置 [备忘]
     */
    public void setCommunication(String communication){
        this.communication = communication ;
        this.modify("communication",communication);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [日记账分录名称]
     */
    public void setMoveName(String moveName){
        this.moveName = moveName ;
        this.modify("move_name",moveName);
    }

    /**
     * 设置 [保存的付款令牌]
     */
    public void setPaymentTokenId(Long paymentTokenId){
        this.paymentTokenId = paymentTokenId ;
        this.modify("payment_token_id",paymentTokenId);
    }

    /**
     * 设置 [付款交易]
     */
    public void setPaymentTransactionId(Long paymentTransactionId){
        this.paymentTransactionId = paymentTransactionId ;
        this.modify("payment_transaction_id",paymentTransactionId);
    }

    /**
     * 设置 [差异科目]
     */
    public void setWriteoffAccountId(Long writeoffAccountId){
        this.writeoffAccountId = writeoffAccountId ;
        this.modify("writeoff_account_id",writeoffAccountId);
    }

    /**
     * 设置 [收款银行账号]
     */
    public void setPartnerBankAccountId(Long partnerBankAccountId){
        this.partnerBankAccountId = partnerBankAccountId ;
        this.modify("partner_bank_account_id",partnerBankAccountId);
    }

    /**
     * 设置 [付款方法类型]
     */
    public void setPaymentMethodId(Long paymentMethodId){
        this.paymentMethodId = paymentMethodId ;
        this.modify("payment_method_id",paymentMethodId);
    }

    /**
     * 设置 [转账到]
     */
    public void setDestinationJournalId(Long destinationJournalId){
        this.destinationJournalId = destinationJournalId ;
        this.modify("destination_journal_id",destinationJournalId);
    }

    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [付款日记账]
     */
    public void setJournalId(Long journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



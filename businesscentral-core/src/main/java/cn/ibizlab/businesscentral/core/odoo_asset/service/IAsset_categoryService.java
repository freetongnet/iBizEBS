package cn.ibizlab.businesscentral.core.odoo_asset.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.businesscentral.core.odoo_asset.filter.Asset_categorySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Asset_category] 服务对象接口
 */
public interface IAsset_categoryService extends IService<Asset_category>{

    boolean create(Asset_category et) ;
    void createBatch(List<Asset_category> list) ;
    boolean update(Asset_category et) ;
    void updateBatch(List<Asset_category> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Asset_category get(Long key) ;
    Asset_category getDraft(Asset_category et) ;
    boolean checkKey(Asset_category et) ;
    boolean save(Asset_category et) ;
    void saveBatch(List<Asset_category> list) ;
    Page<Asset_category> searchDefault(Asset_categorySearchContext context) ;
    List<Asset_category> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Asset_category> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Asset_category> getAssetCategoryByIds(List<Long> ids) ;
    List<Asset_category> getAssetCategoryByEntities(List<Asset_category> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_botSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_bot] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-bot", fallback = mail_botFallback.class)
public interface mail_botFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_bots/{id}")
    Mail_bot update(@PathVariable("id") Long id,@RequestBody Mail_bot mail_bot);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_bots/batch")
    Boolean updateBatch(@RequestBody List<Mail_bot> mail_bots);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_bots")
    Mail_bot create(@RequestBody Mail_bot mail_bot);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_bots/batch")
    Boolean createBatch(@RequestBody List<Mail_bot> mail_bots);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_bots/{id}")
    Mail_bot get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_bots/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_bots/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/mail_bots/search")
    Page<Mail_bot> search(@RequestBody Mail_botSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_bots/select")
    Page<Mail_bot> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_bots/getdraft")
    Mail_bot getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_bots/checkkey")
    Boolean checkKey(@RequestBody Mail_bot mail_bot);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_bots/save")
    Boolean save(@RequestBody Mail_bot mail_bot);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_bots/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_bot> mail_bots);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_bots/searchdefault")
    Page<Mail_bot> searchDefault(@RequestBody Mail_botSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_currencySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_currency] 服务对象接口
 */
public interface IRes_currencyService extends IService<Res_currency>{

    boolean create(Res_currency et) ;
    void createBatch(List<Res_currency> list) ;
    boolean update(Res_currency et) ;
    void updateBatch(List<Res_currency> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_currency get(Long key) ;
    Res_currency getDraft(Res_currency et) ;
    boolean checkKey(Res_currency et) ;
    boolean save(Res_currency et) ;
    void saveBatch(List<Res_currency> list) ;
    Page<Res_currency> searchDEFAULT(Res_currencySearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_currency> getResCurrencyByIds(List<Long> ids) ;
    List<Res_currency> getResCurrencyByEntities(List<Res_currency> entities) ;
}



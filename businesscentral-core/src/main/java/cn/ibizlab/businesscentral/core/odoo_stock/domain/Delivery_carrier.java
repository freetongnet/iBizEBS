package cn.ibizlab.businesscentral.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[送货方式]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "DELIVERY_CARRIER",resultMap = "Delivery_carrierResultMap")
public class Delivery_carrier extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 是否公开
     */
    @DEField(name = "is_published")
    @TableField(value = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;
    /**
     * 固定价格
     */
    @DEField(name = "fixed_price")
    @TableField(value = "fixed_price")
    @JSONField(name = "fixed_price")
    @JsonProperty("fixed_price")
    private Double fixedPrice;
    /**
     * 供应商
     */
    @DEField(name = "delivery_type")
    @TableField(value = "delivery_type")
    @JSONField(name = "delivery_type")
    @JsonProperty("delivery_type")
    private String deliveryType;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date")
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 交易退货标签
     */
    @DEField(name = "return_label_on_delivery")
    @TableField(value = "return_label_on_delivery")
    @JSONField(name = "return_label_on_delivery")
    @JsonProperty("return_label_on_delivery")
    private Boolean returnLabelOnDelivery;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 发票原则
     */
    @DEField(name = "invoice_policy")
    @TableField(value = "invoice_policy")
    @JSONField(name = "invoice_policy")
    @JsonProperty("invoice_policy")
    private String invoicePolicy;
    /**
     * 创建时间
     */
    @DEField(name = "create_date")
    @TableField(value = "create_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 利润率
     */
    @TableField(value = "margin")
    @JSONField(name = "margin")
    @JsonProperty("margin")
    private Double margin;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 调试记录
     */
    @DEField(name = "debug_logging")
    @TableField(value = "debug_logging")
    @JSONField(name = "debug_logging")
    @JsonProperty("debug_logging")
    private Boolean debugLogging;
    /**
     * 邮编到
     */
    @DEField(name = "zip_to")
    @TableField(value = "zip_to")
    @JSONField(name = "zip_to")
    @JsonProperty("zip_to")
    private String zipTo;
    /**
     * 邮编从
     */
    @DEField(name = "zip_from")
    @TableField(value = "zip_from")
    @JSONField(name = "zip_from")
    @JsonProperty("zip_from")
    private String zipFrom;
    /**
     * 如果订货量大则免费
     */
    @DEField(name = "free_over")
    @TableField(value = "free_over")
    @JSONField(name = "free_over")
    @JsonProperty("free_over")
    private Boolean freeOver;
    /**
     * 集成级别
     */
    @DEField(name = "integration_level")
    @TableField(value = "integration_level")
    @JSONField(name = "integration_level")
    @JsonProperty("integration_level")
    private String integrationLevel;
    /**
     * 生产环境
     */
    @DEField(name = "prod_environment")
    @TableField(value = "prod_environment")
    @JSONField(name = "prod_environment")
    @JsonProperty("prod_environment")
    private Boolean prodEnvironment;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 从门户获取返回标签
     */
    @DEField(name = "get_return_label_from_portal")
    @TableField(value = "get_return_label_from_portal")
    @JSONField(name = "get_return_label_from_portal")
    @JsonProperty("get_return_label_from_portal")
    private Boolean getReturnLabelFromPortal;
    /**
     * 名称
     */
    @TableField(exist = false)
    @JSONField(name = "product_name")
    @JsonProperty("product_name")
    private String productName;
    /**
     * 公司名称
     */
    @TableField(exist = false)
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    private String companyName;
    /**
     * 名称
     */
    @TableField(exist = false)
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    private String writeUname;
    /**
     * 名称
     */
    @TableField(exist = false)
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    private String createUname;
    /**
     * ID
     */
    @DEField(name = "write_uid")
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * ID
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * ID
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * ID
     */
    @DEField(name = "create_uid")
    @TableField(value = "create_uid")
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [是否公开]
     */
    public void setIsPublished(Boolean isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [固定价格]
     */
    public void setFixedPrice(Double fixedPrice){
        this.fixedPrice = fixedPrice ;
        this.modify("fixed_price",fixedPrice);
    }

    /**
     * 设置 [供应商]
     */
    public void setDeliveryType(String deliveryType){
        this.deliveryType = deliveryType ;
        this.modify("delivery_type",deliveryType);
    }

    /**
     * 设置 [最后更新时间]
     */
    public void setWriteDate(Timestamp writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 格式化日期 [最后更新时间]
     */
    public String formatWriteDate(){
        if (this.writeDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(writeDate);
    }
    /**
     * 设置 [交易退货标签]
     */
    public void setReturnLabelOnDelivery(Boolean returnLabelOnDelivery){
        this.returnLabelOnDelivery = returnLabelOnDelivery ;
        this.modify("return_label_on_delivery",returnLabelOnDelivery);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [发票原则]
     */
    public void setInvoicePolicy(String invoicePolicy){
        this.invoicePolicy = invoicePolicy ;
        this.modify("invoice_policy",invoicePolicy);
    }

    /**
     * 设置 [创建时间]
     */
    public void setCreateDate(Timestamp createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 格式化日期 [创建时间]
     */
    public String formatCreateDate(){
        if (this.createDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(createDate);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [利润率]
     */
    public void setMargin(Double margin){
        this.margin = margin ;
        this.modify("margin",margin);
    }

    /**
     * 设置 [金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [调试记录]
     */
    public void setDebugLogging(Boolean debugLogging){
        this.debugLogging = debugLogging ;
        this.modify("debug_logging",debugLogging);
    }

    /**
     * 设置 [邮编到]
     */
    public void setZipTo(String zipTo){
        this.zipTo = zipTo ;
        this.modify("zip_to",zipTo);
    }

    /**
     * 设置 [邮编从]
     */
    public void setZipFrom(String zipFrom){
        this.zipFrom = zipFrom ;
        this.modify("zip_from",zipFrom);
    }

    /**
     * 设置 [如果订货量大则免费]
     */
    public void setFreeOver(Boolean freeOver){
        this.freeOver = freeOver ;
        this.modify("free_over",freeOver);
    }

    /**
     * 设置 [集成级别]
     */
    public void setIntegrationLevel(String integrationLevel){
        this.integrationLevel = integrationLevel ;
        this.modify("integration_level",integrationLevel);
    }

    /**
     * 设置 [生产环境]
     */
    public void setProdEnvironment(Boolean prodEnvironment){
        this.prodEnvironment = prodEnvironment ;
        this.modify("prod_environment",prodEnvironment);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [从门户获取返回标签]
     */
    public void setGetReturnLabelFromPortal(Boolean getReturnLabelFromPortal){
        this.getReturnLabelFromPortal = getReturnLabelFromPortal ;
        this.modify("get_return_label_from_portal",getReturnLabelFromPortal);
    }

    /**
     * 设置 [ID]
     */
    public void setWriteUid(Long writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [ID]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [ID]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [ID]
     */
    public void setCreateUid(Long createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



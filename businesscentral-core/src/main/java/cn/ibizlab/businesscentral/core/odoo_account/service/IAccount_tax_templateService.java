package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_template;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_tax_templateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_tax_template] 服务对象接口
 */
public interface IAccount_tax_templateService extends IService<Account_tax_template>{

    boolean create(Account_tax_template et) ;
    void createBatch(List<Account_tax_template> list) ;
    boolean update(Account_tax_template et) ;
    void updateBatch(List<Account_tax_template> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_tax_template get(Long key) ;
    Account_tax_template getDraft(Account_tax_template et) ;
    boolean checkKey(Account_tax_template et) ;
    boolean save(Account_tax_template et) ;
    void saveBatch(List<Account_tax_template> list) ;
    Page<Account_tax_template> searchDefault(Account_tax_templateSearchContext context) ;
    List<Account_tax_template> selectByAccountId(Long id);
    List<Account_tax_template> selectByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_tax_template> selectByCashBasisAccountId(Long id);
    void resetByCashBasisAccountId(Long id);
    void resetByCashBasisAccountId(Collection<Long> ids);
    void removeByCashBasisAccountId(Long id);
    List<Account_tax_template> selectByCashBasisBaseAccountId(Long id);
    void resetByCashBasisBaseAccountId(Long id);
    void resetByCashBasisBaseAccountId(Collection<Long> ids);
    void removeByCashBasisBaseAccountId(Long id);
    List<Account_tax_template> selectByRefundAccountId(Long id);
    List<Account_tax_template> selectByRefundAccountId(Collection<Long> ids);
    void removeByRefundAccountId(Long id);
    List<Account_tax_template> selectByChartTemplateId(Long id);
    void resetByChartTemplateId(Long id);
    void resetByChartTemplateId(Collection<Long> ids);
    void removeByChartTemplateId(Long id);
    List<Account_tax_template> selectByTaxGroupId(Long id);
    void resetByTaxGroupId(Long id);
    void resetByTaxGroupId(Collection<Long> ids);
    void removeByTaxGroupId(Long id);
    List<Account_tax_template> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_tax_template> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_tax_template> getAccountTaxTemplateByIds(List<Long> ids) ;
    List<Account_tax_template> getAccountTaxTemplateByEntities(List<Account_tax_template> entities) ;
}



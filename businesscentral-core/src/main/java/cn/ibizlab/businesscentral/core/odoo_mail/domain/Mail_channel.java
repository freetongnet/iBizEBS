package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[讨论频道]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_CHANNEL",resultMap = "Mail_channelResultMap")
public class Mail_channel extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 中等尺寸照片
     */
    @TableField(exist = false)
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;
    /**
     * 管理EMail账户
     */
    @TableField(exist = false)
    @JSONField(name = "moderation_count")
    @JsonProperty("moderation_count")
    private Integer moderationCount;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 自动通知
     */
    @DEField(name = "moderation_notify")
    @TableField(value = "moderation_notify")
    @JSONField(name = "moderation_notify")
    @JsonProperty("moderation_notify")
    private Boolean moderationNotify;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 管理频道
     */
    @TableField(value = "moderation")
    @JSONField(name = "moderation")
    @JsonProperty("moderation")
    private Boolean moderation;
    /**
     * 管理员
     */
    @TableField(exist = false)
    @JSONField(name = "is_moderator")
    @JsonProperty("is_moderator")
    private Boolean isModerator;
    /**
     * 通知消息
     */
    @DEField(name = "moderation_notify_msg")
    @TableField(value = "moderation_notify_msg")
    @JSONField(name = "moderation_notify_msg")
    @JsonProperty("moderation_notify_msg")
    private String moderationNotifyMsg;
    /**
     * 管理EMail
     */
    @TableField(exist = false)
    @JSONField(name = "moderation_ids")
    @JsonProperty("moderation_ids")
    private String moderationIds;
    /**
     * 最新图像评级
     */
    @TableField(exist = false)
    @JSONField(name = "rating_last_image")
    @JsonProperty("rating_last_image")
    private byte[] ratingLastImage;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 向新用户发送订阅指南
     */
    @DEField(name = "moderation_guidelines")
    @TableField(value = "moderation_guidelines")
    @JSONField(name = "moderation_guidelines")
    @JsonProperty("moderation_guidelines")
    private Boolean moderationGuidelines;
    /**
     * 最新反馈评级
     */
    @TableField(exist = false)
    @JSONField(name = "rating_last_feedback")
    @JsonProperty("rating_last_feedback")
    private String ratingLastFeedback;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 人力资源部门
     */
    @TableField(exist = false)
    @JSONField(name = "subscription_department_ids")
    @JsonProperty("subscription_department_ids")
    private String subscriptionDepartmentIds;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 隐私
     */
    @TableField(value = "ibizpublic")
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    private String ibizpublic;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 评级
     */
    @TableField(exist = false)
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;
    /**
     * 匿名用户姓名
     */
    @DEField(name = "anonymous_name")
    @TableField(value = "anonymous_name")
    @JSONField(name = "anonymous_name")
    @JsonProperty("anonymous_name")
    private String anonymousName;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 成员
     */
    @TableField(exist = false)
    @JSONField(name = "is_member")
    @JsonProperty("is_member")
    private Boolean isMember;
    /**
     * 已订阅
     */
    @TableField(exist = false)
    @JSONField(name = "is_subscribed")
    @JsonProperty("is_subscribed")
    private Boolean isSubscribed;
    /**
     * 方针
     */
    @DEField(name = "moderation_guidelines_msg")
    @TableField(value = "moderation_guidelines_msg")
    @JSONField(name = "moderation_guidelines_msg")
    @JsonProperty("moderation_guidelines_msg")
    private String moderationGuidelinesMsg;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 照片
     */
    @TableField(exist = false)
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 小尺寸照片
     */
    @TableField(exist = false)
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 错误个数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 监听器
     */
    @TableField(exist = false)
    @JSONField(name = "channel_partner_ids")
    @JsonProperty("channel_partner_ids")
    private String channelPartnerIds;
    /**
     * 管理员
     */
    @TableField(exist = false)
    @JSONField(name = "moderator_ids")
    @JsonProperty("moderator_ids")
    private String moderatorIds;
    /**
     * 自动订阅
     */
    @TableField(exist = false)
    @JSONField(name = "group_ids")
    @JsonProperty("group_ids")
    private String groupIds;
    /**
     * 以邮件形式发送
     */
    @DEField(name = "email_send")
    @TableField(value = "email_send")
    @JSONField(name = "email_send")
    @JsonProperty("email_send")
    private Boolean emailSend;
    /**
     * 最近一次查阅
     */
    @TableField(exist = false)
    @JSONField(name = "channel_last_seen_partner_ids")
    @JsonProperty("channel_last_seen_partner_ids")
    private String channelLastSeenPartnerIds;
    /**
     * 前置操作
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * UUID
     */
    @TableField(value = "uuid")
    @JSONField(name = "uuid")
    @JsonProperty("uuid")
    private String uuid;
    /**
     * 评级数
     */
    @TableField(exist = false)
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 操作次数
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 渠道消息
     */
    @TableField(exist = false)
    @JSONField(name = "channel_message_ids")
    @JsonProperty("channel_message_ids")
    private String channelMessageIds;
    /**
     * 渠道类型
     */
    @DEField(name = "channel_type")
    @TableField(value = "channel_type")
    @JSONField(name = "channel_type")
    @JsonProperty("channel_type")
    private String channelType;
    /**
     * 是聊天
     */
    @TableField(exist = false)
    @JSONField(name = "is_chat")
    @JsonProperty("is_chat")
    private Boolean isChat;
    /**
     * 最新值评级
     */
    @DEField(name = "rating_last_value")
    @TableField(value = "rating_last_value")
    @JSONField(name = "rating_last_value")
    @JsonProperty("rating_last_value")
    private Double ratingLastValue;
    /**
     * 记录线索ID
     */
    @TableField(exist = false)
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;
    /**
     * 默认值
     */
    @TableField(exist = false)
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;
    /**
     * 所有者
     */
    @TableField(exist = false)
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Long aliasUserId;
    /**
     * 上级模型
     */
    @TableField(exist = false)
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;
    /**
     * 经授权的群组
     */
    @TableField(exist = false)
    @JSONField(name = "group_public_id_text")
    @JsonProperty("group_public_id_text")
    private String groupPublicIdText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 上级记录ID
     */
    @TableField(exist = false)
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;
    /**
     * 别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;
    /**
     * 模型别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;
    /**
     * 渠道
     */
    @TableField(exist = false)
    @JSONField(name = "livechat_channel_id_text")
    @JsonProperty("livechat_channel_id_text")
    private String livechatChannelIdText;
    /**
     * 网域别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 安全联系人别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;
    /**
     * 经授权的群组
     */
    @DEField(name = "group_public_id")
    @TableField(value = "group_public_id")
    @JSONField(name = "group_public_id")
    @JsonProperty("group_public_id")
    private Long groupPublicId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @TableField(value = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Long aliasId;
    /**
     * 渠道
     */
    @DEField(name = "livechat_channel_id")
    @TableField(value = "livechat_channel_id")
    @JSONField(name = "livechat_channel_id")
    @JsonProperty("livechat_channel_id")
    private Long livechatChannelId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel odooLivechatChannel;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_groups odooGroupPublic;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [自动通知]
     */
    public void setModerationNotify(Boolean moderationNotify){
        this.moderationNotify = moderationNotify ;
        this.modify("moderation_notify",moderationNotify);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [管理频道]
     */
    public void setModeration(Boolean moderation){
        this.moderation = moderation ;
        this.modify("moderation",moderation);
    }

    /**
     * 设置 [通知消息]
     */
    public void setModerationNotifyMsg(String moderationNotifyMsg){
        this.moderationNotifyMsg = moderationNotifyMsg ;
        this.modify("moderation_notify_msg",moderationNotifyMsg);
    }

    /**
     * 设置 [向新用户发送订阅指南]
     */
    public void setModerationGuidelines(Boolean moderationGuidelines){
        this.moderationGuidelines = moderationGuidelines ;
        this.modify("moderation_guidelines",moderationGuidelines);
    }

    /**
     * 设置 [隐私]
     */
    public void setIbizpublic(String ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.modify("ibizpublic",ibizpublic);
    }

    /**
     * 设置 [匿名用户姓名]
     */
    public void setAnonymousName(String anonymousName){
        this.anonymousName = anonymousName ;
        this.modify("anonymous_name",anonymousName);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [方针]
     */
    public void setModerationGuidelinesMsg(String moderationGuidelinesMsg){
        this.moderationGuidelinesMsg = moderationGuidelinesMsg ;
        this.modify("moderation_guidelines_msg",moderationGuidelinesMsg);
    }

    /**
     * 设置 [以邮件形式发送]
     */
    public void setEmailSend(Boolean emailSend){
        this.emailSend = emailSend ;
        this.modify("email_send",emailSend);
    }

    /**
     * 设置 [UUID]
     */
    public void setUuid(String uuid){
        this.uuid = uuid ;
        this.modify("uuid",uuid);
    }

    /**
     * 设置 [渠道类型]
     */
    public void setChannelType(String channelType){
        this.channelType = channelType ;
        this.modify("channel_type",channelType);
    }

    /**
     * 设置 [最新值评级]
     */
    public void setRatingLastValue(Double ratingLastValue){
        this.ratingLastValue = ratingLastValue ;
        this.modify("rating_last_value",ratingLastValue);
    }

    /**
     * 设置 [经授权的群组]
     */
    public void setGroupPublicId(Long groupPublicId){
        this.groupPublicId = groupPublicId ;
        this.modify("group_public_id",groupPublicId);
    }

    /**
     * 设置 [别名]
     */
    public void setAliasId(Long aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }

    /**
     * 设置 [渠道]
     */
    public void setLivechatChannelId(Long livechatChannelId){
        this.livechatChannelId = livechatChannelId ;
        this.modify("livechat_channel_id",livechatChannelId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_import;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_importSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_import] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base-import:odoo-base-import}", contextId = "base-import-import", fallback = base_import_importFallback.class)
public interface base_import_importFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_imports/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_imports/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_imports/{id}")
    Base_import_import update(@PathVariable("id") Long id,@RequestBody Base_import_import base_import_import);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_imports/batch")
    Boolean updateBatch(@RequestBody List<Base_import_import> base_import_imports);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/search")
    Page<Base_import_import> search(@RequestBody Base_import_importSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_imports")
    Base_import_import create(@RequestBody Base_import_import base_import_import);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/batch")
    Boolean createBatch(@RequestBody List<Base_import_import> base_import_imports);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_imports/{id}")
    Base_import_import get(@PathVariable("id") Long id);





    @RequestMapping(method = RequestMethod.GET, value = "/base_import_imports/select")
    Page<Base_import_import> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_imports/getdraft")
    Base_import_import getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/checkkey")
    Boolean checkKey(@RequestBody Base_import_import base_import_import);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/save")
    Boolean save(@RequestBody Base_import_import base_import_import);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/savebatch")
    Boolean saveBatch(@RequestBody List<Base_import_import> base_import_imports);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/searchdefault")
    Page<Base_import_import> searchDefault(@RequestBody Base_import_importSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Repair_line] 服务对象接口
 */
public interface IRepair_lineService extends IService<Repair_line>{

    boolean create(Repair_line et) ;
    void createBatch(List<Repair_line> list) ;
    boolean update(Repair_line et) ;
    void updateBatch(List<Repair_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Repair_line get(Long key) ;
    Repair_line getDraft(Repair_line et) ;
    boolean checkKey(Repair_line et) ;
    boolean save(Repair_line et) ;
    void saveBatch(List<Repair_line> list) ;
    Page<Repair_line> searchDefault(Repair_lineSearchContext context) ;
    List<Repair_line> selectByInvoiceLineId(Long id);
    void resetByInvoiceLineId(Long id);
    void resetByInvoiceLineId(Collection<Long> ids);
    void removeByInvoiceLineId(Long id);
    List<Repair_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Repair_line> selectByRepairId(Long id);
    void removeByRepairId(Collection<Long> ids);
    void removeByRepairId(Long id);
    List<Repair_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Repair_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Repair_line> selectByLocationDestId(Long id);
    void resetByLocationDestId(Long id);
    void resetByLocationDestId(Collection<Long> ids);
    void removeByLocationDestId(Long id);
    List<Repair_line> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Repair_line> selectByMoveId(Long id);
    void resetByMoveId(Long id);
    void resetByMoveId(Collection<Long> ids);
    void removeByMoveId(Long id);
    List<Repair_line> selectByLotId(Long id);
    void resetByLotId(Long id);
    void resetByLotId(Collection<Long> ids);
    void removeByLotId(Long id);
    List<Repair_line> selectByProductUom(Long id);
    void resetByProductUom(Long id);
    void resetByProductUom(Collection<Long> ids);
    void removeByProductUom(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Repair_line> getRepairLineByIds(List<Long> ids) ;
    List<Repair_line> getRepairLineByEntities(List<Repair_line> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_calendar.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_alarmSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Calendar_alarm] 服务对象接口
 */
public interface ICalendar_alarmService extends IService<Calendar_alarm>{

    boolean create(Calendar_alarm et) ;
    void createBatch(List<Calendar_alarm> list) ;
    boolean update(Calendar_alarm et) ;
    void updateBatch(List<Calendar_alarm> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Calendar_alarm get(Long key) ;
    Calendar_alarm getDraft(Calendar_alarm et) ;
    boolean checkKey(Calendar_alarm et) ;
    boolean save(Calendar_alarm et) ;
    void saveBatch(List<Calendar_alarm> list) ;
    Page<Calendar_alarm> searchDefault(Calendar_alarmSearchContext context) ;
    List<Calendar_alarm> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Calendar_alarm> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Calendar_alarm> getCalendarAlarmByIds(List<Long> ids) ;
    List<Calendar_alarm> getCalendarAlarmByEntities(List<Calendar_alarm> entities) ;
}



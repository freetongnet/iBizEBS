package cn.ibizlab.businesscentral.core.odoo_lunch.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[午餐提醒]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "LUNCH_ALERT",resultMap = "Lunch_alertResultMap")
public class Lunch_alert extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日
     */
    @DEField(name = "specific_day")
    @TableField(value = "specific_day")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "specific_day" , format="yyyy-MM-dd")
    @JsonProperty("specific_day")
    private Timestamp specificDay;
    /**
     * 显示
     */
    @TableField(exist = false)
    @JSONField(name = "display")
    @JsonProperty("display")
    private Boolean display;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 周四
     */
    @TableField(value = "thursday")
    @JSONField(name = "thursday")
    @JsonProperty("thursday")
    private Boolean thursday;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 周三
     */
    @TableField(value = "wednesday")
    @JSONField(name = "wednesday")
    @JsonProperty("wednesday")
    private Boolean wednesday;
    /**
     * 周二
     */
    @TableField(value = "tuesday")
    @JSONField(name = "tuesday")
    @JsonProperty("tuesday")
    private Boolean tuesday;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 消息
     */
    @TableField(value = "message")
    @JSONField(name = "message")
    @JsonProperty("message")
    private String message;
    /**
     * 重新提起
     */
    @DEField(name = "alert_type")
    @TableField(value = "alert_type")
    @JSONField(name = "alert_type")
    @JsonProperty("alert_type")
    private String alertType;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 周六
     */
    @TableField(value = "saturday")
    @JSONField(name = "saturday")
    @JsonProperty("saturday")
    private Boolean saturday;
    /**
     * 介于
     */
    @DEField(name = "start_hour")
    @TableField(value = "start_hour")
    @JSONField(name = "start_hour")
    @JsonProperty("start_hour")
    private Double startHour;
    /**
     * 周一
     */
    @TableField(value = "monday")
    @JSONField(name = "monday")
    @JsonProperty("monday")
    private Boolean monday;
    /**
     * 周五
     */
    @TableField(value = "friday")
    @JSONField(name = "friday")
    @JsonProperty("friday")
    private Boolean friday;
    /**
     * 和
     */
    @DEField(name = "end_hour")
    @TableField(value = "end_hour")
    @JSONField(name = "end_hour")
    @JsonProperty("end_hour")
    private Double endHour;
    /**
     * 周日
     */
    @TableField(value = "sunday")
    @JSONField(name = "sunday")
    @JsonProperty("sunday")
    private Boolean sunday;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 供应商
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [日]
     */
    public void setSpecificDay(Timestamp specificDay){
        this.specificDay = specificDay ;
        this.modify("specific_day",specificDay);
    }

    /**
     * 格式化日期 [日]
     */
    public String formatSpecificDay(){
        if (this.specificDay == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(specificDay);
    }
    /**
     * 设置 [周四]
     */
    public void setThursday(Boolean thursday){
        this.thursday = thursday ;
        this.modify("thursday",thursday);
    }

    /**
     * 设置 [周三]
     */
    public void setWednesday(Boolean wednesday){
        this.wednesday = wednesday ;
        this.modify("wednesday",wednesday);
    }

    /**
     * 设置 [周二]
     */
    public void setTuesday(Boolean tuesday){
        this.tuesday = tuesday ;
        this.modify("tuesday",tuesday);
    }

    /**
     * 设置 [消息]
     */
    public void setMessage(String message){
        this.message = message ;
        this.modify("message",message);
    }

    /**
     * 设置 [重新提起]
     */
    public void setAlertType(String alertType){
        this.alertType = alertType ;
        this.modify("alert_type",alertType);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [周六]
     */
    public void setSaturday(Boolean saturday){
        this.saturday = saturday ;
        this.modify("saturday",saturday);
    }

    /**
     * 设置 [介于]
     */
    public void setStartHour(Double startHour){
        this.startHour = startHour ;
        this.modify("start_hour",startHour);
    }

    /**
     * 设置 [周一]
     */
    public void setMonday(Boolean monday){
        this.monday = monday ;
        this.modify("monday",monday);
    }

    /**
     * 设置 [周五]
     */
    public void setFriday(Boolean friday){
        this.friday = friday ;
        this.modify("friday",friday);
    }

    /**
     * 设置 [和]
     */
    public void setEndHour(Double endHour){
        this.endHour = endHour ;
        this.modify("end_hour",endHour);
    }

    /**
     * 设置 [周日]
     */
    public void setSunday(Boolean sunday){
        this.sunday = sunday ;
        this.modify("sunday",sunday);
    }

    /**
     * 设置 [供应商]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



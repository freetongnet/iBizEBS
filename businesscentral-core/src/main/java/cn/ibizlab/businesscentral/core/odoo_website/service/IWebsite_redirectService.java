package cn.ibizlab.businesscentral.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_redirectSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Website_redirect] 服务对象接口
 */
public interface IWebsite_redirectService extends IService<Website_redirect>{

    boolean create(Website_redirect et) ;
    void createBatch(List<Website_redirect> list) ;
    boolean update(Website_redirect et) ;
    void updateBatch(List<Website_redirect> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Website_redirect get(Long key) ;
    Website_redirect getDraft(Website_redirect et) ;
    boolean checkKey(Website_redirect et) ;
    boolean save(Website_redirect et) ;
    void saveBatch(List<Website_redirect> list) ;
    Page<Website_redirect> searchDefault(Website_redirectSearchContext context) ;
    List<Website_redirect> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Website_redirect> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Website_redirect> getWebsiteRedirectByIds(List<Long> ids) ;
    List<Website_redirect> getWebsiteRedirectByEntities(List<Website_redirect> entities) ;
}



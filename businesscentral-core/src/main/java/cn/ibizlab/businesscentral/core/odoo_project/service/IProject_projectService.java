package cn.ibizlab.businesscentral.core.odoo_project.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_project;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_projectSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Project_project] 服务对象接口
 */
public interface IProject_projectService extends IService<Project_project>{

    boolean create(Project_project et) ;
    void createBatch(List<Project_project> list) ;
    boolean update(Project_project et) ;
    void updateBatch(List<Project_project> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Project_project get(Long key) ;
    Project_project getDraft(Project_project et) ;
    boolean checkKey(Project_project et) ;
    boolean save(Project_project et) ;
    void saveBatch(List<Project_project> list) ;
    Page<Project_project> searchDefault(Project_projectSearchContext context) ;
    List<Project_project> selectByAliasId(Long id);
    List<Project_project> selectByAliasId(Collection<Long> ids);
    void removeByAliasId(Long id);
    List<Project_project> selectBySubtaskProjectId(Long id);
    List<Project_project> selectBySubtaskProjectId(Collection<Long> ids);
    void removeBySubtaskProjectId(Long id);
    List<Project_project> selectByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Collection<Long> ids);
    void removeByResourceCalendarId(Long id);
    List<Project_project> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Project_project> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Project_project> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Project_project> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Project_project> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Project_project> getProjectProjectByIds(List<Long> ids) ;
    List<Project_project> getProjectProjectByEntities(List<Project_project> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_sale.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[销售订单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SALE_ORDER",resultMap = "Sale_orderResultMap")
public class Sale_order extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 单据日期
     */
    @DEField(name = "date_order")
    @TableField(value = "date_order")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_order" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_order")
    private Timestamp dateOrder;
    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 未税金额
     */
    @DEField(name = "amount_untaxed")
    @TableField(value = "amount_untaxed")
    @JSONField(name = "amount_untaxed")
    @JsonProperty("amount_untaxed")
    private BigDecimal amountUntaxed;
    /**
     * 验证
     */
    @DEField(name = "validity_date")
    @TableField(value = "validity_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "validity_date" , format="yyyy-MM-dd")
    @JsonProperty("validity_date")
    private Timestamp validityDate;
    /**
     * 折扣前金额
     */
    @TableField(exist = false)
    @JSONField(name = "amount_undiscounted")
    @JsonProperty("amount_undiscounted")
    private Double amountUndiscounted;
    /**
     * 发票数
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_count")
    @JsonProperty("invoice_count")
    private Integer invoiceCount;
    /**
     * 访问警告
     */
    @TableField(exist = false)
    @JSONField(name = "access_warning")
    @JsonProperty("access_warning")
    private String accessWarning;
    /**
     * 警告
     */
    @DEField(name = "warning_stock")
    @TableField(value = "warning_stock")
    @JSONField(name = "warning_stock")
    @JsonProperty("warning_stock")
    private String warningStock;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 实际日期
     */
    @DEField(name = "effective_date")
    @TableField(value = "effective_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effective_date" , format="yyyy-MM-dd")
    @JsonProperty("effective_date")
    private Timestamp effectiveDate;
    /**
     * 网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 在线签名
     */
    @DEField(name = "require_signature")
    @TableField(value = "require_signature")
    @JSONField(name = "require_signature")
    @JsonProperty("require_signature")
    private Boolean requireSignature;
    /**
     * 汇率
     */
    @DEField(name = "currency_rate")
    @TableField(value = "currency_rate")
    @JSONField(name = "currency_rate")
    @JsonProperty("currency_rate")
    private Double currencyRate;
    /**
     * 送货策略
     */
    @DEField(name = "picking_policy")
    @TableField(value = "picking_policy")
    @JSONField(name = "picking_policy")
    @JsonProperty("picking_policy")
    private String pickingPolicy;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 签名
     */
    @TableField(exist = false)
    @JSONField(name = "signature")
    @JsonProperty("signature")
    private byte[] signature;
    /**
     * 补货组
     */
    @DEField(name = "procurement_group_id")
    @TableField(value = "procurement_group_id")
    @JSONField(name = "procurement_group_id")
    @JsonProperty("procurement_group_id")
    private Integer procurementGroupId;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 购物车数量
     */
    @TableField(exist = false)
    @JSONField(name = "cart_quantity")
    @JsonProperty("cart_quantity")
    private Integer cartQuantity;
    /**
     * 类型名称
     */
    @TableField(exist = false)
    @JSONField(name = "type_name")
    @JsonProperty("type_name")
    private String typeName;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 出库单
     */
    @TableField(exist = false)
    @JSONField(name = "delivery_count")
    @JsonProperty("delivery_count")
    private Integer deliveryCount;
    /**
     * 已签核
     */
    @DEField(name = "signed_by")
    @TableField(value = "signed_by")
    @JSONField(name = "signed_by")
    @JsonProperty("signed_by")
    private String signedBy;
    /**
     * 订单行
     */
    @TableField(exist = false)
    @JSONField(name = "order_line")
    @JsonProperty("order_line")
    private String orderLine;
    /**
     * 创建日期
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 在线支付
     */
    @DEField(name = "require_payment")
    @TableField(value = "require_payment")
    @JSONField(name = "require_payment")
    @JsonProperty("require_payment")
    private Boolean requirePayment;
    /**
     * 只是服务
     */
    @TableField(exist = false)
    @JSONField(name = "only_services")
    @JsonProperty("only_services")
    private Boolean onlyServices;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 网页显示订单明细
     */
    @TableField(exist = false)
    @JSONField(name = "website_order_line")
    @JsonProperty("website_order_line")
    private String websiteOrderLine;
    /**
     * 付款参考:
     */
    @TableField(value = "reference")
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 发票状态
     */
    @DEField(name = "invoice_status")
    @TableField(value = "invoice_status")
    @JSONField(name = "invoice_status")
    @JsonProperty("invoice_status")
    private String invoiceStatus;
    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 交易
     */
    @TableField(exist = false)
    @JSONField(name = "transaction_ids")
    @JsonProperty("transaction_ids")
    private String transactionIds;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 遗弃的购物车
     */
    @TableField(exist = false)
    @JSONField(name = "is_abandoned_cart")
    @JsonProperty("is_abandoned_cart")
    private Boolean isAbandonedCart;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * # 费用
     */
    @TableField(exist = false)
    @JSONField(name = "expense_count")
    @JsonProperty("expense_count")
    private Integer expenseCount;
    /**
     * 已授权的交易
     */
    @TableField(exist = false)
    @JSONField(name = "authorized_transaction_ids")
    @JsonProperty("authorized_transaction_ids")
    private String authorizedTransactionIds;
    /**
     * 承诺日期
     */
    @DEField(name = "commitment_date")
    @TableField(value = "commitment_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "commitment_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("commitment_date")
    private Timestamp commitmentDate;
    /**
     * 过期了
     */
    @TableField(exist = false)
    @JSONField(name = "is_expired")
    @JsonProperty("is_expired")
    private Boolean isExpired;
    /**
     * 网站信息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 剩余确认天数
     */
    @TableField(exist = false)
    @JSONField(name = "remaining_validity_days")
    @JsonProperty("remaining_validity_days")
    private Integer remainingValidityDays;
    /**
     * 预计日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expected_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expected_date")
    private Timestamp expectedDate;
    /**
     * 采购订单号
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_order_count")
    @JsonProperty("purchase_order_count")
    private Integer purchaseOrderCount;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 门户访问网址
     */
    @TableField(exist = false)
    @JSONField(name = "access_url")
    @JsonProperty("access_url")
    private String accessUrl;
    /**
     * 订单关联
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 可选产品行
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_option_ids")
    @JsonProperty("sale_order_option_ids")
    private String saleOrderOptionIds;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 源文档
     */
    @TableField(value = "origin")
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;
    /**
     * 确认日期
     */
    @DEField(name = "confirmation_date")
    @TableField(value = "confirmation_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "confirmation_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("confirmation_date")
    private Timestamp confirmationDate;
    /**
     * 条款和条件
     */
    @TableField(value = "note")
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;
    /**
     * 按组分配税额
     */
    @TableField(exist = false)
    @JSONField(name = "amount_by_group")
    @JsonProperty("amount_by_group")
    private byte[] amountByGroup;
    /**
     * 拣货
     */
    @TableField(exist = false)
    @JSONField(name = "picking_ids")
    @JsonProperty("picking_ids")
    private String pickingIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 安全令牌
     */
    @DEField(name = "access_token")
    @TableField(value = "access_token")
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;
    /**
     * 总计
     */
    @DEField(name = "amount_total")
    @TableField(value = "amount_total")
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private BigDecimal amountTotal;
    /**
     * 客户订单号
     */
    @DEField(name = "client_order_ref")
    @TableField(value = "client_order_ref")
    @JSONField(name = "client_order_ref")
    @JsonProperty("client_order_ref")
    private String clientOrderRef;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 购物车恢复EMail已发送
     */
    @DEField(name = "cart_recovery_email_sent")
    @TableField(value = "cart_recovery_email_sent")
    @JSONField(name = "cart_recovery_email_sent")
    @JsonProperty("cart_recovery_email_sent")
    private Boolean cartRecoveryEmailSent;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 税率
     */
    @DEField(name = "amount_tax")
    @TableField(value = "amount_tax")
    @JSONField(name = "amount_tax")
    @JsonProperty("amount_tax")
    private BigDecimal amountTax;
    /**
     * 费用
     */
    @TableField(exist = false)
    @JSONField(name = "expense_ids")
    @JsonProperty("expense_ids")
    private String expenseIds;
    /**
     * 发票地址
     */
    @TableField(exist = false)
    @JSONField(name = "partner_invoice_id_text")
    @JsonProperty("partner_invoice_id_text")
    private String partnerInvoiceIdText;
    /**
     * 贸易条款
     */
    @TableField(exist = false)
    @JSONField(name = "incoterm_text")
    @JsonProperty("incoterm_text")
    private String incotermText;
    /**
     * 销售团队
     */
    @TableField(exist = false)
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;
    /**
     * 分析账户
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    private String analyticAccountIdText;
    /**
     * 客户
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 营销
     */
    @TableField(exist = false)
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 送货地址
     */
    @TableField(exist = false)
    @JSONField(name = "partner_shipping_id_text")
    @JsonProperty("partner_shipping_id_text")
    private String partnerShippingIdText;
    /**
     * 仓库
     */
    @TableField(exist = false)
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;
    /**
     * 销售员
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 商机
     */
    @TableField(exist = false)
    @JSONField(name = "opportunity_id_text")
    @JsonProperty("opportunity_id_text")
    private String opportunityIdText;
    /**
     * 来源
     */
    @TableField(exist = false)
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;
    /**
     * 媒体
     */
    @TableField(exist = false)
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;
    /**
     * 税科目调整
     */
    @TableField(exist = false)
    @JSONField(name = "fiscal_position_id_text")
    @JsonProperty("fiscal_position_id_text")
    private String fiscalPositionIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 价格表
     */
    @TableField(exist = false)
    @JSONField(name = "pricelist_id_text")
    @JsonProperty("pricelist_id_text")
    private String pricelistIdText;
    /**
     * 付款条款
     */
    @TableField(exist = false)
    @JSONField(name = "payment_term_id_text")
    @JsonProperty("payment_term_id_text")
    private String paymentTermIdText;
    /**
     * 报价单模板
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_template_id_text")
    @JsonProperty("sale_order_template_id_text")
    private String saleOrderTemplateIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @TableField(value = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Long teamId;
    /**
     * 发票地址
     */
    @DEField(name = "partner_invoice_id")
    @TableField(value = "partner_invoice_id")
    @JSONField(name = "partner_invoice_id")
    @JsonProperty("partner_invoice_id")
    private Long partnerInvoiceId;
    /**
     * 税科目调整
     */
    @DEField(name = "fiscal_position_id")
    @TableField(value = "fiscal_position_id")
    @JSONField(name = "fiscal_position_id")
    @JsonProperty("fiscal_position_id")
    private Long fiscalPositionId;
    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @TableField(value = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Long campaignId;
    /**
     * 来源
     */
    @DEField(name = "source_id")
    @TableField(value = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Long sourceId;
    /**
     * 报价单模板
     */
    @DEField(name = "sale_order_template_id")
    @TableField(value = "sale_order_template_id")
    @JSONField(name = "sale_order_template_id")
    @JsonProperty("sale_order_template_id")
    private Long saleOrderTemplateId;
    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @TableField(value = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Long mediumId;
    /**
     * 分析账户
     */
    @DEField(name = "analytic_account_id")
    @TableField(value = "analytic_account_id")
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    private Long analyticAccountId;
    /**
     * 付款条款
     */
    @DEField(name = "payment_term_id")
    @TableField(value = "payment_term_id")
    @JSONField(name = "payment_term_id")
    @JsonProperty("payment_term_id")
    private Long paymentTermId;
    /**
     * 送货地址
     */
    @DEField(name = "partner_shipping_id")
    @TableField(value = "partner_shipping_id")
    @JSONField(name = "partner_shipping_id")
    @JsonProperty("partner_shipping_id")
    private Long partnerShippingId;
    /**
     * 商机
     */
    @DEField(name = "opportunity_id")
    @TableField(value = "opportunity_id")
    @JSONField(name = "opportunity_id")
    @JsonProperty("opportunity_id")
    private Long opportunityId;
    /**
     * 贸易条款
     */
    @TableField(value = "incoterm")
    @JSONField(name = "incoterm")
    @JsonProperty("incoterm")
    private Long incoterm;
    /**
     * 价格表
     */
    @DEField(name = "pricelist_id")
    @TableField(value = "pricelist_id")
    @JSONField(name = "pricelist_id")
    @JsonProperty("pricelist_id")
    private Long pricelistId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 仓库
     */
    @DEField(name = "warehouse_id")
    @TableField(value = "warehouse_id")
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Long warehouseId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooFiscalPosition;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_incoterms odooIncoterm;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooPaymentTerm;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_lead odooOpportunity;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartnerInvoice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartnerShipping;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template odooSaleOrderTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooWarehouse;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource;



    /**
     * 设置 [单据日期]
     */
    public void setDateOrder(Timestamp dateOrder){
        this.dateOrder = dateOrder ;
        this.modify("date_order",dateOrder);
    }

    /**
     * 格式化日期 [单据日期]
     */
    public String formatDateOrder(){
        if (this.dateOrder == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateOrder);
    }
    /**
     * 设置 [未税金额]
     */
    public void setAmountUntaxed(BigDecimal amountUntaxed){
        this.amountUntaxed = amountUntaxed ;
        this.modify("amount_untaxed",amountUntaxed);
    }

    /**
     * 设置 [验证]
     */
    public void setValidityDate(Timestamp validityDate){
        this.validityDate = validityDate ;
        this.modify("validity_date",validityDate);
    }

    /**
     * 格式化日期 [验证]
     */
    public String formatValidityDate(){
        if (this.validityDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(validityDate);
    }
    /**
     * 设置 [警告]
     */
    public void setWarningStock(String warningStock){
        this.warningStock = warningStock ;
        this.modify("warning_stock",warningStock);
    }

    /**
     * 设置 [实际日期]
     */
    public void setEffectiveDate(Timestamp effectiveDate){
        this.effectiveDate = effectiveDate ;
        this.modify("effective_date",effectiveDate);
    }

    /**
     * 格式化日期 [实际日期]
     */
    public String formatEffectiveDate(){
        if (this.effectiveDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(effectiveDate);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [在线签名]
     */
    public void setRequireSignature(Boolean requireSignature){
        this.requireSignature = requireSignature ;
        this.modify("require_signature",requireSignature);
    }

    /**
     * 设置 [汇率]
     */
    public void setCurrencyRate(Double currencyRate){
        this.currencyRate = currencyRate ;
        this.modify("currency_rate",currencyRate);
    }

    /**
     * 设置 [送货策略]
     */
    public void setPickingPolicy(String pickingPolicy){
        this.pickingPolicy = pickingPolicy ;
        this.modify("picking_policy",pickingPolicy);
    }

    /**
     * 设置 [补货组]
     */
    public void setProcurementGroupId(Integer procurementGroupId){
        this.procurementGroupId = procurementGroupId ;
        this.modify("procurement_group_id",procurementGroupId);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [已签核]
     */
    public void setSignedBy(String signedBy){
        this.signedBy = signedBy ;
        this.modify("signed_by",signedBy);
    }

    /**
     * 设置 [在线支付]
     */
    public void setRequirePayment(Boolean requirePayment){
        this.requirePayment = requirePayment ;
        this.modify("require_payment",requirePayment);
    }

    /**
     * 设置 [付款参考:]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [发票状态]
     */
    public void setInvoiceStatus(String invoiceStatus){
        this.invoiceStatus = invoiceStatus ;
        this.modify("invoice_status",invoiceStatus);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [承诺日期]
     */
    public void setCommitmentDate(Timestamp commitmentDate){
        this.commitmentDate = commitmentDate ;
        this.modify("commitment_date",commitmentDate);
    }

    /**
     * 格式化日期 [承诺日期]
     */
    public String formatCommitmentDate(){
        if (this.commitmentDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(commitmentDate);
    }
    /**
     * 设置 [订单关联]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [确认日期]
     */
    public void setConfirmationDate(Timestamp confirmationDate){
        this.confirmationDate = confirmationDate ;
        this.modify("confirmation_date",confirmationDate);
    }

    /**
     * 格式化日期 [确认日期]
     */
    public String formatConfirmationDate(){
        if (this.confirmationDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(confirmationDate);
    }
    /**
     * 设置 [条款和条件]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [安全令牌]
     */
    public void setAccessToken(String accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [总计]
     */
    public void setAmountTotal(BigDecimal amountTotal){
        this.amountTotal = amountTotal ;
        this.modify("amount_total",amountTotal);
    }

    /**
     * 设置 [客户订单号]
     */
    public void setClientOrderRef(String clientOrderRef){
        this.clientOrderRef = clientOrderRef ;
        this.modify("client_order_ref",clientOrderRef);
    }

    /**
     * 设置 [购物车恢复EMail已发送]
     */
    public void setCartRecoveryEmailSent(Boolean cartRecoveryEmailSent){
        this.cartRecoveryEmailSent = cartRecoveryEmailSent ;
        this.modify("cart_recovery_email_sent",cartRecoveryEmailSent);
    }

    /**
     * 设置 [税率]
     */
    public void setAmountTax(BigDecimal amountTax){
        this.amountTax = amountTax ;
        this.modify("amount_tax",amountTax);
    }

    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Long teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

    /**
     * 设置 [发票地址]
     */
    public void setPartnerInvoiceId(Long partnerInvoiceId){
        this.partnerInvoiceId = partnerInvoiceId ;
        this.modify("partner_invoice_id",partnerInvoiceId);
    }

    /**
     * 设置 [税科目调整]
     */
    public void setFiscalPositionId(Long fiscalPositionId){
        this.fiscalPositionId = fiscalPositionId ;
        this.modify("fiscal_position_id",fiscalPositionId);
    }

    /**
     * 设置 [客户]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [营销]
     */
    public void setCampaignId(Long campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [来源]
     */
    public void setSourceId(Long sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [报价单模板]
     */
    public void setSaleOrderTemplateId(Long saleOrderTemplateId){
        this.saleOrderTemplateId = saleOrderTemplateId ;
        this.modify("sale_order_template_id",saleOrderTemplateId);
    }

    /**
     * 设置 [销售员]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [媒体]
     */
    public void setMediumId(Long mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [分析账户]
     */
    public void setAnalyticAccountId(Long analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }

    /**
     * 设置 [付款条款]
     */
    public void setPaymentTermId(Long paymentTermId){
        this.paymentTermId = paymentTermId ;
        this.modify("payment_term_id",paymentTermId);
    }

    /**
     * 设置 [送货地址]
     */
    public void setPartnerShippingId(Long partnerShippingId){
        this.partnerShippingId = partnerShippingId ;
        this.modify("partner_shipping_id",partnerShippingId);
    }

    /**
     * 设置 [商机]
     */
    public void setOpportunityId(Long opportunityId){
        this.opportunityId = opportunityId ;
        this.modify("opportunity_id",opportunityId);
    }

    /**
     * 设置 [贸易条款]
     */
    public void setIncoterm(Long incoterm){
        this.incoterm = incoterm ;
        this.modify("incoterm",incoterm);
    }

    /**
     * 设置 [价格表]
     */
    public void setPricelistId(Long pricelistId){
        this.pricelistId = pricelistId ;
        this.modify("pricelist_id",pricelistId);
    }

    /**
     * 设置 [仓库]
     */
    public void setWarehouseId(Long warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



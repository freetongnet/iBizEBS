package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_holidays_summary_employee;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_holidays_summary_employeeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_holidays_summary_employee] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-holidays-summary-employee", fallback = hr_holidays_summary_employeeFallback.class)
public interface hr_holidays_summary_employeeFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_employees/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_holidays_summary_employees/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);





    @RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_employees/{id}")
    Hr_holidays_summary_employee update(@PathVariable("id") Long id,@RequestBody Hr_holidays_summary_employee hr_holidays_summary_employee);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_holidays_summary_employees/batch")
    Boolean updateBatch(@RequestBody List<Hr_holidays_summary_employee> hr_holidays_summary_employees);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_employees/{id}")
    Hr_holidays_summary_employee get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/search")
    Page<Hr_holidays_summary_employee> search(@RequestBody Hr_holidays_summary_employeeSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees")
    Hr_holidays_summary_employee create(@RequestBody Hr_holidays_summary_employee hr_holidays_summary_employee);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/batch")
    Boolean createBatch(@RequestBody List<Hr_holidays_summary_employee> hr_holidays_summary_employees);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_employees/select")
    Page<Hr_holidays_summary_employee> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_holidays_summary_employees/getdraft")
    Hr_holidays_summary_employee getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/checkkey")
    Boolean checkKey(@RequestBody Hr_holidays_summary_employee hr_holidays_summary_employee);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/save")
    Boolean save(@RequestBody Hr_holidays_summary_employee hr_holidays_summary_employee);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_holidays_summary_employee> hr_holidays_summary_employees);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_holidays_summary_employees/searchdefault")
    Page<Hr_holidays_summary_employee> searchDefault(@RequestBody Hr_holidays_summary_employeeSearchContext context);


}

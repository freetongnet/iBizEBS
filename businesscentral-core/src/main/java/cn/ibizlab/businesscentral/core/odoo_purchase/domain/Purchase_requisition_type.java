package cn.ibizlab.businesscentral.core.odoo_purchase.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[采购申请类型]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PURCHASE_REQUISITION_TYPE",resultMap = "Purchase_requisition_typeResultMap")
public class Purchase_requisition_type extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 申请类型
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 供应商选择
     */
    @TableField(value = "exclusive")
    @JSONField(name = "exclusive")
    @JsonProperty("exclusive")
    private String exclusive;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 设置数量方式
     */
    @DEField(name = "quantity_copy")
    @TableField(value = "quantity_copy")
    @JSONField(name = "quantity_copy")
    @JsonProperty("quantity_copy")
    private String quantityCopy;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date")
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 创建时间
     */
    @DEField(name = "create_date")
    @TableField(value = "create_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 明细行
     */
    @DEField(name = "line_copy")
    @TableField(value = "line_copy")
    @JSONField(name = "line_copy")
    @JsonProperty("line_copy")
    private String lineCopy;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    private String createUname;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    private String writeUname;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid")
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid")
    @TableField(value = "create_uid")
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [申请类型]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [供应商选择]
     */
    public void setExclusive(String exclusive){
        this.exclusive = exclusive ;
        this.modify("exclusive",exclusive);
    }

    /**
     * 设置 [设置数量方式]
     */
    public void setQuantityCopy(String quantityCopy){
        this.quantityCopy = quantityCopy ;
        this.modify("quantity_copy",quantityCopy);
    }

    /**
     * 设置 [最后更新时间]
     */
    public void setWriteDate(Timestamp writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 格式化日期 [最后更新时间]
     */
    public String formatWriteDate(){
        if (this.writeDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(writeDate);
    }
    /**
     * 设置 [创建时间]
     */
    public void setCreateDate(Timestamp createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 格式化日期 [创建时间]
     */
    public String formatCreateDate(){
        if (this.createDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(createDate);
    }
    /**
     * 设置 [明细行]
     */
    public void setLineCopy(String lineCopy){
        this.lineCopy = lineCopy ;
        this.modify("line_copy",lineCopy);
    }

    /**
     * 设置 [最后更新人]
     */
    public void setWriteUid(Long writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [创建人]
     */
    public void setCreateUid(Long createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



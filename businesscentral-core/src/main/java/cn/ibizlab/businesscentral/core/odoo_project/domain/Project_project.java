package cn.ibizlab.businesscentral.core.odoo_project.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[项目]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PROJECT_PROJECT",resultMap = "Project_projectResultMap")
public class Project_project extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 访问警告
     */
    @TableField(exist = false)
    @JSONField(name = "access_warning")
    @JsonProperty("access_warning")
    private String accessWarning;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 过期日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 任务活动
     */
    @TableField(exist = false)
    @JSONField(name = "tasks")
    @JsonProperty("tasks")
    private String tasks;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 好 % 任务
     */
    @DEField(name = "percentage_satisfaction_task")
    @TableField(value = "percentage_satisfaction_task")
    @JSONField(name = "percentage_satisfaction_task")
    @JsonProperty("percentage_satisfaction_task")
    private Integer percentageSatisfactionTask;
    /**
     * 门户访问网址
     */
    @TableField(exist = false)
    @JSONField(name = "access_url")
    @JsonProperty("access_url")
    private String accessUrl;
    /**
     * 任务阶段
     */
    @TableField(exist = false)
    @JSONField(name = "type_ids")
    @JsonProperty("type_ids")
    private String typeIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 安全令牌
     */
    @DEField(name = "access_token")
    @TableField(value = "access_token")
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 隐私
     */
    @DEField(name = "privacy_visibility")
    @TableField(value = "privacy_visibility")
    @JSONField(name = "privacy_visibility")
    @JsonProperty("privacy_visibility")
    private String privacyVisibility;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 点评频率
     */
    @DEField(name = "rating_status_period")
    @TableField(value = "rating_status_period")
    @JSONField(name = "rating_status_period")
    @JsonProperty("rating_status_period")
    private String ratingStatusPeriod;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 好 % 项目
     */
    @DEField(name = "percentage_satisfaction_project")
    @TableField(value = "percentage_satisfaction_project")
    @JSONField(name = "percentage_satisfaction_project")
    @JsonProperty("percentage_satisfaction_project")
    private Integer percentageSatisfactionProject;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "doc_count")
    @JsonProperty("doc_count")
    private Integer docCount;
    /**
     * 会员
     */
    @TableField(exist = false)
    @JSONField(name = "favorite_user_ids")
    @JsonProperty("favorite_user_ids")
    private String favoriteUserIds;
    /**
     * 颜色索引
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 网站信息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 任务统计
     */
    @TableField(exist = false)
    @JSONField(name = "task_count")
    @JsonProperty("task_count")
    private Integer taskCount;
    /**
     * 在仪表板显示项目
     */
    @TableField(exist = false)
    @JSONField(name = "is_favorite")
    @JsonProperty("is_favorite")
    private Boolean isFavorite;
    /**
     * 公开评级
     */
    @DEField(name = "portal_show_rating")
    @TableField(value = "portal_show_rating")
    @JSONField(name = "portal_show_rating")
    @JsonProperty("portal_show_rating")
    private Boolean portalShowRating;
    /**
     * 评级请求截止日期
     */
    @DEField(name = "rating_request_deadline")
    @TableField(value = "rating_request_deadline")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "rating_request_deadline" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("rating_request_deadline")
    private Timestamp ratingRequestDeadline;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 客户点评
     */
    @DEField(name = "rating_status")
    @TableField(value = "rating_status")
    @JSONField(name = "rating_status")
    @JsonProperty("rating_status")
    private String ratingStatus;
    /**
     * 用任务来
     */
    @DEField(name = "label_tasks")
    @TableField(value = "label_tasks")
    @JSONField(name = "label_tasks")
    @JsonProperty("label_tasks")
    private String labelTasks;
    /**
     * 任务
     */
    @TableField(exist = false)
    @JSONField(name = "task_ids")
    @JsonProperty("task_ids")
    private String taskIds;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 开始日期
     */
    @DEField(name = "date_start")
    @TableField(value = "date_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd")
    @JsonProperty("date_start")
    private Timestamp dateStart;
    /**
     * 客户
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 上级记录线程ID
     */
    @TableField(exist = false)
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;
    /**
     * 记录线索ID
     */
    @TableField(exist = false)
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 别名网域
     */
    @TableField(exist = false)
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;
    /**
     * 别名联系人安全
     */
    @TableField(exist = false)
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;
    /**
     * 上级模型
     */
    @TableField(exist = false)
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;
    /**
     * 别名的模型
     */
    @TableField(exist = false)
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 所有者
     */
    @TableField(exist = false)
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Long aliasUserId;
    /**
     * 默认值
     */
    @TableField(exist = false)
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;
    /**
     * 工作时间
     */
    @TableField(exist = false)
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;
    /**
     * 子任务项目
     */
    @TableField(exist = false)
    @JSONField(name = "subtask_project_id_text")
    @JsonProperty("subtask_project_id_text")
    private String subtaskProjectIdText;
    /**
     * 别名
     */
    @TableField(exist = false)
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;
    /**
     * 项目管理员
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 项目管理员
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 子任务项目
     */
    @DEField(name = "subtask_project_id")
    @TableField(value = "subtask_project_id")
    @JSONField(name = "subtask_project_id")
    @JsonProperty("subtask_project_id")
    private Long subtaskProjectId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 工作时间
     */
    @DEField(name = "resource_calendar_id")
    @TableField(value = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Long resourceCalendarId;
    /**
     * 别名
     */
    @DEField(name = "alias_id")
    @TableField(value = "alias_id")
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Long aliasId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_project.domain.Project_project odooSubtaskProject;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [过期日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [过期日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
    /**
     * 设置 [好 % 任务]
     */
    public void setPercentageSatisfactionTask(Integer percentageSatisfactionTask){
        this.percentageSatisfactionTask = percentageSatisfactionTask ;
        this.modify("percentage_satisfaction_task",percentageSatisfactionTask);
    }

    /**
     * 设置 [安全令牌]
     */
    public void setAccessToken(String accessToken){
        this.accessToken = accessToken ;
        this.modify("access_token",accessToken);
    }

    /**
     * 设置 [隐私]
     */
    public void setPrivacyVisibility(String privacyVisibility){
        this.privacyVisibility = privacyVisibility ;
        this.modify("privacy_visibility",privacyVisibility);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [点评频率]
     */
    public void setRatingStatusPeriod(String ratingStatusPeriod){
        this.ratingStatusPeriod = ratingStatusPeriod ;
        this.modify("rating_status_period",ratingStatusPeriod);
    }

    /**
     * 设置 [好 % 项目]
     */
    public void setPercentageSatisfactionProject(Integer percentageSatisfactionProject){
        this.percentageSatisfactionProject = percentageSatisfactionProject ;
        this.modify("percentage_satisfaction_project",percentageSatisfactionProject);
    }

    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [公开评级]
     */
    public void setPortalShowRating(Boolean portalShowRating){
        this.portalShowRating = portalShowRating ;
        this.modify("portal_show_rating",portalShowRating);
    }

    /**
     * 设置 [评级请求截止日期]
     */
    public void setRatingRequestDeadline(Timestamp ratingRequestDeadline){
        this.ratingRequestDeadline = ratingRequestDeadline ;
        this.modify("rating_request_deadline",ratingRequestDeadline);
    }

    /**
     * 格式化日期 [评级请求截止日期]
     */
    public String formatRatingRequestDeadline(){
        if (this.ratingRequestDeadline == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(ratingRequestDeadline);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [客户点评]
     */
    public void setRatingStatus(String ratingStatus){
        this.ratingStatus = ratingStatus ;
        this.modify("rating_status",ratingStatus);
    }

    /**
     * 设置 [用任务来]
     */
    public void setLabelTasks(String labelTasks){
        this.labelTasks = labelTasks ;
        this.modify("label_tasks",labelTasks);
    }

    /**
     * 设置 [开始日期]
     */
    public void setDateStart(Timestamp dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatDateStart(){
        if (this.dateStart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateStart);
    }
    /**
     * 设置 [项目管理员]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [客户]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [子任务项目]
     */
    public void setSubtaskProjectId(Long subtaskProjectId){
        this.subtaskProjectId = subtaskProjectId ;
        this.modify("subtask_project_id",subtaskProjectId);
    }

    /**
     * 设置 [工作时间]
     */
    public void setResourceCalendarId(Long resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }

    /**
     * 设置 [别名]
     */
    public void setAliasId(Long aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



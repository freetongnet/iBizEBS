package cn.ibizlab.businesscentral.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_productSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[lunch_product] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-lunch:odoo-lunch}", contextId = "lunch-product", fallback = lunch_productFallback.class)
public interface lunch_productFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products")
    Lunch_product create(@RequestBody Lunch_product lunch_product);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products/batch")
    Boolean createBatch(@RequestBody List<Lunch_product> lunch_products);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_products/{id}")
    Lunch_product get(@PathVariable("id") Long id);





    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products/search")
    Page<Lunch_product> search(@RequestBody Lunch_productSearchContext context);



    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_products/{id}")
    Lunch_product update(@PathVariable("id") Long id,@RequestBody Lunch_product lunch_product);

    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_products/batch")
    Boolean updateBatch(@RequestBody List<Lunch_product> lunch_products);


    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_products/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_products/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_products/select")
    Page<Lunch_product> select();


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_products/getdraft")
    Lunch_product getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products/checkkey")
    Boolean checkKey(@RequestBody Lunch_product lunch_product);


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products/save")
    Boolean save(@RequestBody Lunch_product lunch_product);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products/savebatch")
    Boolean saveBatch(@RequestBody List<Lunch_product> lunch_products);



    @RequestMapping(method = RequestMethod.POST, value = "/lunch_products/searchdefault")
    Page<Lunch_product> searchDefault(@RequestBody Lunch_productSearchContext context);


}

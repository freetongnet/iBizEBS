package cn.ibizlab.businesscentral.core.odoo_web_editor.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.businesscentral.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[web_editor_converter_test_sub] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-web-editor:odoo-web-editor}", contextId = "web-editor-converter-test-sub", fallback = web_editor_converter_test_subFallback.class)
public interface web_editor_converter_test_subFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_test_subs/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_test_subs/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs")
    Web_editor_converter_test_sub create(@RequestBody Web_editor_converter_test_sub web_editor_converter_test_sub);

    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/batch")
    Boolean createBatch(@RequestBody List<Web_editor_converter_test_sub> web_editor_converter_test_subs);



    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/search")
    Page<Web_editor_converter_test_sub> search(@RequestBody Web_editor_converter_test_subSearchContext context);




    @RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_test_subs/{id}")
    Web_editor_converter_test_sub update(@PathVariable("id") Long id,@RequestBody Web_editor_converter_test_sub web_editor_converter_test_sub);

    @RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_test_subs/batch")
    Boolean updateBatch(@RequestBody List<Web_editor_converter_test_sub> web_editor_converter_test_subs);



    @RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_test_subs/{id}")
    Web_editor_converter_test_sub get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_test_subs/select")
    Page<Web_editor_converter_test_sub> select();


    @RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_test_subs/getdraft")
    Web_editor_converter_test_sub getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/checkkey")
    Boolean checkKey(@RequestBody Web_editor_converter_test_sub web_editor_converter_test_sub);


    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/save")
    Boolean save(@RequestBody Web_editor_converter_test_sub web_editor_converter_test_sub);

    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/savebatch")
    Boolean saveBatch(@RequestBody List<Web_editor_converter_test_sub> web_editor_converter_test_subs);



    @RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/searchdefault")
    Page<Web_editor_converter_test_sub> searchDefault(@RequestBody Web_editor_converter_test_subSearchContext context);


}

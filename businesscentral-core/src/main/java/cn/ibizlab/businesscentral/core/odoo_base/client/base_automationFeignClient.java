package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_automation;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_automationSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_automation] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "base-automation", fallback = base_automationFallback.class)
public interface base_automationFeignClient {





    @RequestMapping(method = RequestMethod.POST, value = "/base_automations/search")
    Page<Base_automation> search(@RequestBody Base_automationSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/base_automations")
    Base_automation create(@RequestBody Base_automation base_automation);

    @RequestMapping(method = RequestMethod.POST, value = "/base_automations/batch")
    Boolean createBatch(@RequestBody List<Base_automation> base_automations);


    @RequestMapping(method = RequestMethod.GET, value = "/base_automations/{id}")
    Base_automation get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/base_automations/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_automations/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/base_automations/{id}")
    Base_automation update(@PathVariable("id") Long id,@RequestBody Base_automation base_automation);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_automations/batch")
    Boolean updateBatch(@RequestBody List<Base_automation> base_automations);


    @RequestMapping(method = RequestMethod.GET, value = "/base_automations/select")
    Page<Base_automation> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_automations/getdraft")
    Base_automation getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_automations/checkkey")
    Boolean checkKey(@RequestBody Base_automation base_automation);


    @RequestMapping(method = RequestMethod.POST, value = "/base_automations/save")
    Boolean save(@RequestBody Base_automation base_automation);

    @RequestMapping(method = RequestMethod.POST, value = "/base_automations/savebatch")
    Boolean saveBatch(@RequestBody List<Base_automation> base_automations);



    @RequestMapping(method = RequestMethod.POST, value = "/base_automations/searchdefault")
    Page<Base_automation> searchDefault(@RequestBody Base_automationSearchContext context);


}

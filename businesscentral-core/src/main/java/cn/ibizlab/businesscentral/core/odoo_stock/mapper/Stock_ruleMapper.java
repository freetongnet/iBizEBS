package cn.ibizlab.businesscentral.core.odoo_stock.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_ruleSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Stock_ruleMapper extends BaseMapper<Stock_rule>{

    Page<Stock_rule> searchDefault(IPage page, @Param("srf") Stock_ruleSearchContext context, @Param("ew") Wrapper<Stock_rule> wrapper) ;
    @Override
    Stock_rule selectById(Serializable id);
    @Override
    int insert(Stock_rule entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Stock_rule entity);
    @Override
    int update(@Param(Constants.ENTITY) Stock_rule entity, @Param("ew") Wrapper<Stock_rule> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Stock_rule> selectByCompanyId(@Param("id") Serializable id) ;

    List<Stock_rule> selectByPartnerAddressId(@Param("id") Serializable id) ;

    List<Stock_rule> selectByCreateUid(@Param("id") Serializable id) ;

    List<Stock_rule> selectByWriteUid(@Param("id") Serializable id) ;

    List<Stock_rule> selectByRouteId(@Param("id") Serializable id) ;

    List<Stock_rule> selectByLocationId(@Param("id") Serializable id) ;

    List<Stock_rule> selectByLocationSrcId(@Param("id") Serializable id) ;

    List<Stock_rule> selectByPickingTypeId(@Param("id") Serializable id) ;

    List<Stock_rule> selectByPropagateWarehouseId(@Param("id") Serializable id) ;

    List<Stock_rule> selectByWarehouseId(@Param("id") Serializable id) ;


}

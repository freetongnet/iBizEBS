package cn.ibizlab.businesscentral.core.odoo_ir.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_property;
/**
 * 关系型数据实体[Ir_property] 查询条件对象
 */
@Slf4j
@Data
public class Ir_propertySearchContext extends QueryWrapperContext<Ir_property> {

	private String n_name_eq;//[名称]
	public void setN_name_eq(String n_name_eq) {
        this.n_name_eq = n_name_eq;
        if(!ObjectUtils.isEmpty(this.n_name_eq)){
            this.getSearchCond().eq("name", n_name_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_res_id_eq;//[res_id]
	public void setN_res_id_eq(String n_res_id_eq) {
        this.n_res_id_eq = n_res_id_eq;
        if(!ObjectUtils.isEmpty(this.n_res_id_eq)){
            this.getSearchCond().eq("res_id", n_res_id_eq);
        }
    }
	private Long n_fields_id_eq;//[ID]
	public void setN_fields_id_eq(Long n_fields_id_eq) {
        this.n_fields_id_eq = n_fields_id_eq;
        if(!ObjectUtils.isEmpty(this.n_fields_id_eq)){
            this.getSearchCond().eq("fields_id", n_fields_id_eq);
        }
    }
	private Long n_company_id_eq;//[ID]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




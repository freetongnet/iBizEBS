package cn.ibizlab.businesscentral.core.odoo_hr.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_applicantSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Hr_applicantMapper extends BaseMapper<Hr_applicant>{

    Page<Hr_applicant> searchDefault(IPage page, @Param("srf") Hr_applicantSearchContext context, @Param("ew") Wrapper<Hr_applicant> wrapper) ;
    @Override
    Hr_applicant selectById(Serializable id);
    @Override
    int insert(Hr_applicant entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Hr_applicant entity);
    @Override
    int update(@Param(Constants.ENTITY) Hr_applicant entity, @Param("ew") Wrapper<Hr_applicant> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Hr_applicant> selectByDepartmentId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByEmpId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByJobId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByTypeId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByLastStageId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByStageId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByCompanyId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByPartnerId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByCreateUid(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByUserId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByWriteUid(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByCampaignId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectByMediumId(@Param("id") Serializable id) ;

    List<Hr_applicant> selectBySourceId(@Param("id") Serializable id) ;


}

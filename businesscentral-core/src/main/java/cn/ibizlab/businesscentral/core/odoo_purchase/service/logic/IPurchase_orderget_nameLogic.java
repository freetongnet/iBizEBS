package cn.ibizlab.businesscentral.core.odoo_purchase.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;

/**
 * 关系型数据实体[get_name] 对象
 */
public interface IPurchase_orderget_nameLogic {

    void execute(Purchase_order et) ;

}

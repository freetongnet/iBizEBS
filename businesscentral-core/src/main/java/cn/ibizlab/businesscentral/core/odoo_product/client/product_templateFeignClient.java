package cn.ibizlab.businesscentral.core.odoo_product.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_templateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[product_template] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-product:odoo-product}", contextId = "product-template", fallback = product_templateFallback.class)
public interface product_templateFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/product_templates/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/product_templates")
    Product_template create(@RequestBody Product_template product_template);

    @RequestMapping(method = RequestMethod.POST, value = "/product_templates/batch")
    Boolean createBatch(@RequestBody List<Product_template> product_templates);


    @RequestMapping(method = RequestMethod.PUT, value = "/product_templates/{id}")
    Product_template update(@PathVariable("id") Long id,@RequestBody Product_template product_template);

    @RequestMapping(method = RequestMethod.PUT, value = "/product_templates/batch")
    Boolean updateBatch(@RequestBody List<Product_template> product_templates);




    @RequestMapping(method = RequestMethod.GET, value = "/product_templates/{id}")
    Product_template get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/product_templates/search")
    Page<Product_template> search(@RequestBody Product_templateSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/product_templates/select")
    Page<Product_template> select();


    @RequestMapping(method = RequestMethod.GET, value = "/product_templates/getdraft")
    Product_template getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/product_templates/checkkey")
    Boolean checkKey(@RequestBody Product_template product_template);


    @RequestMapping(method = RequestMethod.POST, value = "/product_templates/save")
    Boolean save(@RequestBody Product_template product_template);

    @RequestMapping(method = RequestMethod.POST, value = "/product_templates/savebatch")
    Boolean saveBatch(@RequestBody List<Product_template> product_templates);



    @RequestMapping(method = RequestMethod.POST, value = "/product_templates/searchdefault")
    Page<Product_template> searchDefault(@RequestBody Product_templateSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/product_templates/searchmaster")
    Page<Product_template> searchMaster(@RequestBody Product_templateSearchContext context);


}

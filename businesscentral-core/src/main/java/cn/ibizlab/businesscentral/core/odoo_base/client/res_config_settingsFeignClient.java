package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_config_settingsSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_config_settings] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-config-settings", fallback = res_config_settingsFallback.class)
public interface res_config_settingsFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/search")
    Page<Res_config_settings> search(@RequestBody Res_config_settingsSearchContext context);




    @RequestMapping(method = RequestMethod.PUT, value = "/res_config_settings/{id}")
    Res_config_settings update(@PathVariable("id") Long id,@RequestBody Res_config_settings res_config_settings);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_config_settings/batch")
    Boolean updateBatch(@RequestBody List<Res_config_settings> res_config_settings);


    @RequestMapping(method = RequestMethod.DELETE, value = "/res_config_settings/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_config_settings/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/res_config_settings")
    Res_config_settings create(@RequestBody Res_config_settings res_config_settings);

    @RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/batch")
    Boolean createBatch(@RequestBody List<Res_config_settings> res_config_settings);


    @RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/{id}")
    Res_config_settings get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/select")
    Page<Res_config_settings> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/getdraft")
    Res_config_settings getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/checkkey")
    Boolean checkKey(@RequestBody Res_config_settings res_config_settings);


    @RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/{id}/getlatestsettings")
    Res_config_settings getLatestSettings(@PathVariable("id") Long id,@RequestBody Res_config_settings res_config_settings);


    @RequestMapping(method = RequestMethod.GET, value = "/res_config_settings/{id}/get_default")
    Res_config_settings get_default(@PathVariable("id") Long id,@RequestBody Res_config_settings res_config_settings);


    @RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/save")
    Boolean save(@RequestBody Res_config_settings res_config_settings);

    @RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/savebatch")
    Boolean saveBatch(@RequestBody List<Res_config_settings> res_config_settings);



    @RequestMapping(method = RequestMethod.POST, value = "/res_config_settings/searchdefault")
    Page<Res_config_settings> searchDefault(@RequestBody Res_config_settingsSearchContext context);


}

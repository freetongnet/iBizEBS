package cn.ibizlab.businesscentral.core.odoo_fleet.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Fleet_vehicle_log_contract] 服务对象接口
 */
public interface IFleet_vehicle_log_contractService extends IService<Fleet_vehicle_log_contract>{

    boolean create(Fleet_vehicle_log_contract et) ;
    void createBatch(List<Fleet_vehicle_log_contract> list) ;
    boolean update(Fleet_vehicle_log_contract et) ;
    void updateBatch(List<Fleet_vehicle_log_contract> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Fleet_vehicle_log_contract get(Long key) ;
    Fleet_vehicle_log_contract getDraft(Fleet_vehicle_log_contract et) ;
    boolean checkKey(Fleet_vehicle_log_contract et) ;
    boolean save(Fleet_vehicle_log_contract et) ;
    void saveBatch(List<Fleet_vehicle_log_contract> list) ;
    Page<Fleet_vehicle_log_contract> searchDefault(Fleet_vehicle_log_contractSearchContext context) ;
    List<Fleet_vehicle_log_contract> selectByCostId(Long id);
    void removeByCostId(Collection<Long> ids);
    void removeByCostId(Long id);
    List<Fleet_vehicle_log_contract> selectByInsurerId(Long id);
    void resetByInsurerId(Long id);
    void resetByInsurerId(Collection<Long> ids);
    void removeByInsurerId(Long id);
    List<Fleet_vehicle_log_contract> selectByPurchaserId(Long id);
    void resetByPurchaserId(Long id);
    void resetByPurchaserId(Collection<Long> ids);
    void removeByPurchaserId(Long id);
    List<Fleet_vehicle_log_contract> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Fleet_vehicle_log_contract> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Fleet_vehicle_log_contract> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Fleet_vehicle_log_contract> getFleetVehicleLogContractByIds(List<Long> ids) ;
    List<Fleet_vehicle_log_contract> getFleetVehicleLogContractByEntities(List<Fleet_vehicle_log_contract> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_sale.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_product_configuratorSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sale_product_configurator] 服务对象接口
 */
public interface ISale_product_configuratorService extends IService<Sale_product_configurator>{

    boolean create(Sale_product_configurator et) ;
    void createBatch(List<Sale_product_configurator> list) ;
    boolean update(Sale_product_configurator et) ;
    void updateBatch(List<Sale_product_configurator> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sale_product_configurator get(Long key) ;
    Sale_product_configurator getDraft(Sale_product_configurator et) ;
    boolean checkKey(Sale_product_configurator et) ;
    boolean save(Sale_product_configurator et) ;
    void saveBatch(List<Sale_product_configurator> list) ;
    Page<Sale_product_configurator> searchDefault(Sale_product_configuratorSearchContext context) ;
    List<Sale_product_configurator> selectByPricelistId(Long id);
    void resetByPricelistId(Long id);
    void resetByPricelistId(Collection<Long> ids);
    void removeByPricelistId(Long id);
    List<Sale_product_configurator> selectByProductTemplateId(Long id);
    void resetByProductTemplateId(Long id);
    void resetByProductTemplateId(Collection<Long> ids);
    void removeByProductTemplateId(Long id);
    List<Sale_product_configurator> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Sale_product_configurator> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Sale_product_configurator> getSaleProductConfiguratorByIds(List<Long> ids) ;
    List<Sale_product_configurator> getSaleProductConfiguratorByEntities(List<Sale_product_configurator> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_survey.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_page;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_pageSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Survey_pageMapper extends BaseMapper<Survey_page>{

    Page<Survey_page> searchDefault(IPage page, @Param("srf") Survey_pageSearchContext context, @Param("ew") Wrapper<Survey_page> wrapper) ;
    @Override
    Survey_page selectById(Serializable id);
    @Override
    int insert(Survey_page entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Survey_page entity);
    @Override
    int update(@Param(Constants.ENTITY) Survey_page entity, @Param("ew") Wrapper<Survey_page> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Survey_page> selectByCreateUid(@Param("id") Serializable id) ;

    List<Survey_page> selectByWriteUid(@Param("id") Serializable id) ;

    List<Survey_page> selectBySurveyId(@Param("id") Serializable id) ;


}

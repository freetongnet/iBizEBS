package cn.ibizlab.businesscentral.core.odoo_im_livechat.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_channel;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_report_channelSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[im_livechat_report_channel] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-im-livechat:odoo-im-livechat}", contextId = "im-livechat-report-channel", fallback = im_livechat_report_channelFallback.class)
public interface im_livechat_report_channelFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/search")
    Page<Im_livechat_report_channel> search(@RequestBody Im_livechat_report_channelSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_channels/{id}")
    Im_livechat_report_channel get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels")
    Im_livechat_report_channel create(@RequestBody Im_livechat_report_channel im_livechat_report_channel);

    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/batch")
    Boolean createBatch(@RequestBody List<Im_livechat_report_channel> im_livechat_report_channels);


    @RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_channels/{id}")
    Im_livechat_report_channel update(@PathVariable("id") Long id,@RequestBody Im_livechat_report_channel im_livechat_report_channel);

    @RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_report_channels/batch")
    Boolean updateBatch(@RequestBody List<Im_livechat_report_channel> im_livechat_report_channels);


    @RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_channels/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_report_channels/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_channels/select")
    Page<Im_livechat_report_channel> select();


    @RequestMapping(method = RequestMethod.GET, value = "/im_livechat_report_channels/getdraft")
    Im_livechat_report_channel getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/checkkey")
    Boolean checkKey(@RequestBody Im_livechat_report_channel im_livechat_report_channel);


    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/save")
    Boolean save(@RequestBody Im_livechat_report_channel im_livechat_report_channel);

    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/savebatch")
    Boolean saveBatch(@RequestBody List<Im_livechat_report_channel> im_livechat_report_channels);



    @RequestMapping(method = RequestMethod.POST, value = "/im_livechat_report_channels/searchdefault")
    Page<Im_livechat_report_channel> searchDefault(@RequestBody Im_livechat_report_channelSearchContext context);


}

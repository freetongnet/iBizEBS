package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_fixed_putaway_strat] 服务对象接口
 */
@Component
public class stock_fixed_putaway_stratFallback implements stock_fixed_putaway_stratFeignClient{



    public Stock_fixed_putaway_strat update(Long id, Stock_fixed_putaway_strat stock_fixed_putaway_strat){
            return null;
     }
    public Boolean updateBatch(List<Stock_fixed_putaway_strat> stock_fixed_putaway_strats){
            return false;
     }


    public Stock_fixed_putaway_strat create(Stock_fixed_putaway_strat stock_fixed_putaway_strat){
            return null;
     }
    public Boolean createBatch(List<Stock_fixed_putaway_strat> stock_fixed_putaway_strats){
            return false;
     }

    public Stock_fixed_putaway_strat get(Long id){
            return null;
     }



    public Page<Stock_fixed_putaway_strat> search(Stock_fixed_putaway_stratSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Stock_fixed_putaway_strat> select(){
            return null;
     }

    public Stock_fixed_putaway_strat getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_fixed_putaway_strat stock_fixed_putaway_strat){
            return false;
     }


    public Boolean save(Stock_fixed_putaway_strat stock_fixed_putaway_strat){
            return false;
     }
    public Boolean saveBatch(List<Stock_fixed_putaway_strat> stock_fixed_putaway_strats){
            return false;
     }

    public Page<Stock_fixed_putaway_strat> searchDefault(Stock_fixed_putaway_stratSearchContext context){
            return null;
     }


}

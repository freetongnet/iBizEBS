package cn.ibizlab.businesscentral.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_mixin;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_mixinSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Resource_mixin] 服务对象接口
 */
public interface IResource_mixinService extends IService<Resource_mixin>{

    boolean create(Resource_mixin et) ;
    void createBatch(List<Resource_mixin> list) ;
    boolean update(Resource_mixin et) ;
    void updateBatch(List<Resource_mixin> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Resource_mixin get(Long key) ;
    Resource_mixin getDraft(Resource_mixin et) ;
    boolean checkKey(Resource_mixin et) ;
    boolean save(Resource_mixin et) ;
    void saveBatch(List<Resource_mixin> list) ;
    Page<Resource_mixin> searchDefault(Resource_mixinSearchContext context) ;
    List<Resource_mixin> selectByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Collection<Long> ids);
    void removeByResourceCalendarId(Long id);
    List<Resource_mixin> selectByResourceId(Long id);
    List<Resource_mixin> selectByResourceId(Collection<Long> ids);
    void removeByResourceId(Long id);
    List<Resource_mixin> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



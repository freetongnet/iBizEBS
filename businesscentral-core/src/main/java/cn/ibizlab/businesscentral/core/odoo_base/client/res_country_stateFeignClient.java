package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_country_stateSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_country_state] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-country-state", fallback = res_country_stateFallback.class)
public interface res_country_stateFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/res_country_states/{id}")
    Res_country_state update(@PathVariable("id") Long id,@RequestBody Res_country_state res_country_state);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_country_states/batch")
    Boolean updateBatch(@RequestBody List<Res_country_state> res_country_states);




    @RequestMapping(method = RequestMethod.DELETE, value = "/res_country_states/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_country_states/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/res_country_states/{id}")
    Res_country_state get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/res_country_states/search")
    Page<Res_country_state> search(@RequestBody Res_country_stateSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/res_country_states")
    Res_country_state create(@RequestBody Res_country_state res_country_state);

    @RequestMapping(method = RequestMethod.POST, value = "/res_country_states/batch")
    Boolean createBatch(@RequestBody List<Res_country_state> res_country_states);



    @RequestMapping(method = RequestMethod.GET, value = "/res_country_states/select")
    Page<Res_country_state> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_country_states/getdraft")
    Res_country_state getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_country_states/checkkey")
    Boolean checkKey(@RequestBody Res_country_state res_country_state);


    @RequestMapping(method = RequestMethod.POST, value = "/res_country_states/save")
    Boolean save(@RequestBody Res_country_state res_country_state);

    @RequestMapping(method = RequestMethod.POST, value = "/res_country_states/savebatch")
    Boolean saveBatch(@RequestBody List<Res_country_state> res_country_states);



    @RequestMapping(method = RequestMethod.POST, value = "/res_country_states/searchdefault")
    Page<Res_country_state> searchDefault(@RequestBody Res_country_stateSearchContext context);


}

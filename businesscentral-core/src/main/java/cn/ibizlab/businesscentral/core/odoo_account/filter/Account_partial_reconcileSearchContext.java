package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_partial_reconcile;
/**
 * 关系型数据实体[Account_partial_reconcile] 查询条件对象
 */
@Slf4j
@Data
public class Account_partial_reconcileSearchContext extends QueryWrapperContext<Account_partial_reconcile> {

	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_credit_move_id_text_eq;//[贷方凭证]
	public void setN_credit_move_id_text_eq(String n_credit_move_id_text_eq) {
        this.n_credit_move_id_text_eq = n_credit_move_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_credit_move_id_text_eq)){
            this.getSearchCond().eq("credit_move_id_text", n_credit_move_id_text_eq);
        }
    }
	private String n_credit_move_id_text_like;//[贷方凭证]
	public void setN_credit_move_id_text_like(String n_credit_move_id_text_like) {
        this.n_credit_move_id_text_like = n_credit_move_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_credit_move_id_text_like)){
            this.getSearchCond().like("credit_move_id_text", n_credit_move_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_debit_move_id_text_eq;//[借方移动]
	public void setN_debit_move_id_text_eq(String n_debit_move_id_text_eq) {
        this.n_debit_move_id_text_eq = n_debit_move_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_debit_move_id_text_eq)){
            this.getSearchCond().eq("debit_move_id_text", n_debit_move_id_text_eq);
        }
    }
	private String n_debit_move_id_text_like;//[借方移动]
	public void setN_debit_move_id_text_like(String n_debit_move_id_text_like) {
        this.n_debit_move_id_text_like = n_debit_move_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_debit_move_id_text_like)){
            this.getSearchCond().like("debit_move_id_text", n_debit_move_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_full_reconcile_id_text_eq;//[完全核销]
	public void setN_full_reconcile_id_text_eq(String n_full_reconcile_id_text_eq) {
        this.n_full_reconcile_id_text_eq = n_full_reconcile_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_full_reconcile_id_text_eq)){
            this.getSearchCond().eq("full_reconcile_id_text", n_full_reconcile_id_text_eq);
        }
    }
	private String n_full_reconcile_id_text_like;//[完全核销]
	public void setN_full_reconcile_id_text_like(String n_full_reconcile_id_text_like) {
        this.n_full_reconcile_id_text_like = n_full_reconcile_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_full_reconcile_id_text_like)){
            this.getSearchCond().like("full_reconcile_id_text", n_full_reconcile_id_text_like);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_full_reconcile_id_eq;//[完全核销]
	public void setN_full_reconcile_id_eq(Long n_full_reconcile_id_eq) {
        this.n_full_reconcile_id_eq = n_full_reconcile_id_eq;
        if(!ObjectUtils.isEmpty(this.n_full_reconcile_id_eq)){
            this.getSearchCond().eq("full_reconcile_id", n_full_reconcile_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_debit_move_id_eq;//[借方移动]
	public void setN_debit_move_id_eq(Long n_debit_move_id_eq) {
        this.n_debit_move_id_eq = n_debit_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_debit_move_id_eq)){
            this.getSearchCond().eq("debit_move_id", n_debit_move_id_eq);
        }
    }
	private Long n_credit_move_id_eq;//[贷方凭证]
	public void setN_credit_move_id_eq(Long n_credit_move_id_eq) {
        this.n_credit_move_id_eq = n_credit_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_credit_move_id_eq)){
            this.getSearchCond().eq("credit_move_id", n_credit_move_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_send;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_sendSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_invoice_send] 服务对象接口
 */
@Component
public class account_invoice_sendFallback implements account_invoice_sendFeignClient{

    public Account_invoice_send create(Account_invoice_send account_invoice_send){
            return null;
     }
    public Boolean createBatch(List<Account_invoice_send> account_invoice_sends){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Account_invoice_send> search(Account_invoice_sendSearchContext context){
            return null;
     }



    public Account_invoice_send get(Long id){
            return null;
     }



    public Account_invoice_send update(Long id, Account_invoice_send account_invoice_send){
            return null;
     }
    public Boolean updateBatch(List<Account_invoice_send> account_invoice_sends){
            return false;
     }


    public Page<Account_invoice_send> select(){
            return null;
     }

    public Account_invoice_send getDraft(){
            return null;
    }



    public Boolean checkKey(Account_invoice_send account_invoice_send){
            return false;
     }


    public Boolean save(Account_invoice_send account_invoice_send){
            return false;
     }
    public Boolean saveBatch(List<Account_invoice_send> account_invoice_sends){
            return false;
     }

    public Page<Account_invoice_send> searchDefault(Account_invoice_sendSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Maintenance_equipment] 服务对象接口
 */
public interface IMaintenance_equipmentService extends IService<Maintenance_equipment>{

    boolean create(Maintenance_equipment et) ;
    void createBatch(List<Maintenance_equipment> list) ;
    boolean update(Maintenance_equipment et) ;
    void updateBatch(List<Maintenance_equipment> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Maintenance_equipment get(Long key) ;
    Maintenance_equipment getDraft(Maintenance_equipment et) ;
    boolean checkKey(Maintenance_equipment et) ;
    boolean save(Maintenance_equipment et) ;
    void saveBatch(List<Maintenance_equipment> list) ;
    Page<Maintenance_equipment> searchDefault(Maintenance_equipmentSearchContext context) ;
    List<Maintenance_equipment> selectByDepartmentId(Long id);
    void resetByDepartmentId(Long id);
    void resetByDepartmentId(Collection<Long> ids);
    void removeByDepartmentId(Long id);
    List<Maintenance_equipment> selectByEmployeeId(Long id);
    void resetByEmployeeId(Long id);
    void resetByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Maintenance_equipment> selectByCategoryId(Long id);
    void resetByCategoryId(Long id);
    void resetByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Maintenance_equipment> selectByMaintenanceTeamId(Long id);
    void resetByMaintenanceTeamId(Long id);
    void resetByMaintenanceTeamId(Collection<Long> ids);
    void removeByMaintenanceTeamId(Long id);
    List<Maintenance_equipment> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Maintenance_equipment> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Maintenance_equipment> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Maintenance_equipment> selectByOwnerUserId(Long id);
    void resetByOwnerUserId(Long id);
    void resetByOwnerUserId(Collection<Long> ids);
    void removeByOwnerUserId(Long id);
    List<Maintenance_equipment> selectByTechnicianUserId(Long id);
    void resetByTechnicianUserId(Long id);
    void resetByTechnicianUserId(Collection<Long> ids);
    void removeByTechnicianUserId(Long id);
    List<Maintenance_equipment> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Maintenance_equipment> getMaintenanceEquipmentByIds(List<Long> ids) ;
    List<Maintenance_equipment> getMaintenanceEquipmentByEntities(List<Maintenance_equipment> entities) ;
}



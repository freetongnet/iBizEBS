package cn.ibizlab.businesscentral.core.odoo_maintenance.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.businesscentral.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Maintenance_equipmentMapper extends BaseMapper<Maintenance_equipment>{

    Page<Maintenance_equipment> searchDefault(IPage page, @Param("srf") Maintenance_equipmentSearchContext context, @Param("ew") Wrapper<Maintenance_equipment> wrapper) ;
    @Override
    Maintenance_equipment selectById(Serializable id);
    @Override
    int insert(Maintenance_equipment entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Maintenance_equipment entity);
    @Override
    int update(@Param(Constants.ENTITY) Maintenance_equipment entity, @Param("ew") Wrapper<Maintenance_equipment> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Maintenance_equipment> selectByDepartmentId(@Param("id") Serializable id) ;

    List<Maintenance_equipment> selectByEmployeeId(@Param("id") Serializable id) ;

    List<Maintenance_equipment> selectByCategoryId(@Param("id") Serializable id) ;

    List<Maintenance_equipment> selectByMaintenanceTeamId(@Param("id") Serializable id) ;

    List<Maintenance_equipment> selectByCompanyId(@Param("id") Serializable id) ;

    List<Maintenance_equipment> selectByPartnerId(@Param("id") Serializable id) ;

    List<Maintenance_equipment> selectByCreateUid(@Param("id") Serializable id) ;

    List<Maintenance_equipment> selectByOwnerUserId(@Param("id") Serializable id) ;

    List<Maintenance_equipment> selectByTechnicianUserId(@Param("id") Serializable id) ;

    List<Maintenance_equipment> selectByWriteUid(@Param("id") Serializable id) ;


}

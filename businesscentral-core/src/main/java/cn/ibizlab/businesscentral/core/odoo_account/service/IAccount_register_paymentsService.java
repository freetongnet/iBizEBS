package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_register_payments;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_register_paymentsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_register_payments] 服务对象接口
 */
public interface IAccount_register_paymentsService extends IService<Account_register_payments>{

    boolean create(Account_register_payments et) ;
    void createBatch(List<Account_register_payments> list) ;
    boolean update(Account_register_payments et) ;
    void updateBatch(List<Account_register_payments> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_register_payments get(Long key) ;
    Account_register_payments getDraft(Account_register_payments et) ;
    boolean checkKey(Account_register_payments et) ;
    boolean save(Account_register_payments et) ;
    void saveBatch(List<Account_register_payments> list) ;
    Page<Account_register_payments> searchDefault(Account_register_paymentsSearchContext context) ;
    List<Account_register_payments> selectByWriteoffAccountId(Long id);
    void resetByWriteoffAccountId(Long id);
    void resetByWriteoffAccountId(Collection<Long> ids);
    void removeByWriteoffAccountId(Long id);
    List<Account_register_payments> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Account_register_payments> selectByPaymentMethodId(Long id);
    void resetByPaymentMethodId(Long id);
    void resetByPaymentMethodId(Collection<Long> ids);
    void removeByPaymentMethodId(Long id);
    List<Account_register_payments> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_register_payments> selectByPartnerBankAccountId(Long id);
    void resetByPartnerBankAccountId(Long id);
    void resetByPartnerBankAccountId(Collection<Long> ids);
    void removeByPartnerBankAccountId(Long id);
    List<Account_register_payments> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Account_register_payments> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_register_payments> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_register_payments> getAccountRegisterPaymentsByIds(List<Long> ids) ;
    List<Account_register_payments> getAccountRegisterPaymentsByEntities(List<Account_register_payments> entities) ;
}



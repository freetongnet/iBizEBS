package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_templateSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_template] 服务对象接口
 */
@Component
public class mail_templateFallback implements mail_templateFeignClient{


    public Mail_template create(Mail_template mail_template){
            return null;
     }
    public Boolean createBatch(List<Mail_template> mail_templates){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Mail_template update(Long id, Mail_template mail_template){
            return null;
     }
    public Boolean updateBatch(List<Mail_template> mail_templates){
            return false;
     }


    public Mail_template get(Long id){
            return null;
     }


    public Page<Mail_template> search(Mail_templateSearchContext context){
            return null;
     }



    public Page<Mail_template> select(){
            return null;
     }

    public Mail_template getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_template mail_template){
            return false;
     }


    public Boolean save(Mail_template mail_template){
            return false;
     }
    public Boolean saveBatch(List<Mail_template> mail_templates){
            return false;
     }

    public Page<Mail_template> searchDefault(Mail_templateSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_location_routeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_location_route] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-location-route", fallback = stock_location_routeFallback.class)
public interface stock_location_routeFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes")
    Stock_location_route create(@RequestBody Stock_location_route stock_location_route);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/batch")
    Boolean createBatch(@RequestBody List<Stock_location_route> stock_location_routes);





    @RequestMapping(method = RequestMethod.GET, value = "/stock_location_routes/{id}")
    Stock_location_route get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/search")
    Page<Stock_location_route> search(@RequestBody Stock_location_routeSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_location_routes/{id}")
    Stock_location_route update(@PathVariable("id") Long id,@RequestBody Stock_location_route stock_location_route);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_location_routes/batch")
    Boolean updateBatch(@RequestBody List<Stock_location_route> stock_location_routes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_location_routes/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_location_routes/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_location_routes/select")
    Page<Stock_location_route> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_location_routes/getdraft")
    Stock_location_route getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/checkkey")
    Boolean checkKey(@RequestBody Stock_location_route stock_location_route);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/save")
    Boolean save(@RequestBody Stock_location_route stock_location_route);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_location_route> stock_location_routes);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_location_routes/searchdefault")
    Page<Stock_location_route> searchDefault(@RequestBody Stock_location_routeSearchContext context);


}

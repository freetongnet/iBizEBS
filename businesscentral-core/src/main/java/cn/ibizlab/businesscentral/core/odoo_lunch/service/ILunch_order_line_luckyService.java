package cn.ibizlab.businesscentral.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_order_line_luckySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Lunch_order_line_lucky] 服务对象接口
 */
public interface ILunch_order_line_luckyService extends IService<Lunch_order_line_lucky>{

    boolean create(Lunch_order_line_lucky et) ;
    void createBatch(List<Lunch_order_line_lucky> list) ;
    boolean update(Lunch_order_line_lucky et) ;
    void updateBatch(List<Lunch_order_line_lucky> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Lunch_order_line_lucky get(Long key) ;
    Lunch_order_line_lucky getDraft(Lunch_order_line_lucky et) ;
    boolean checkKey(Lunch_order_line_lucky et) ;
    boolean save(Lunch_order_line_lucky et) ;
    void saveBatch(List<Lunch_order_line_lucky> list) ;
    Page<Lunch_order_line_lucky> searchDefault(Lunch_order_line_luckySearchContext context) ;
    List<Lunch_order_line_lucky> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Lunch_order_line_lucky> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Lunch_order_line_lucky> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Lunch_order_line_lucky> getLunchOrderLineLuckyByIds(List<Long> ids) ;
    List<Lunch_order_line_lucky> getLunchOrderLineLuckyByEntities(List<Lunch_order_line_lucky> entities) ;
}



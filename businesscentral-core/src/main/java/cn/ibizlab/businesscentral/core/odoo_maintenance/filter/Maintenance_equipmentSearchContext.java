package cn.ibizlab.businesscentral.core.odoo_maintenance.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment;
/**
 * 关系型数据实体[Maintenance_equipment] 查询条件对象
 */
@Slf4j
@Data
public class Maintenance_equipmentSearchContext extends QueryWrapperContext<Maintenance_equipment> {

	private String n_equipment_assign_to_eq;//[用于]
	public void setN_equipment_assign_to_eq(String n_equipment_assign_to_eq) {
        this.n_equipment_assign_to_eq = n_equipment_assign_to_eq;
        if(!ObjectUtils.isEmpty(this.n_equipment_assign_to_eq)){
            this.getSearchCond().eq("equipment_assign_to", n_equipment_assign_to_eq);
        }
    }
	private String n_name_like;//[设备名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_department_id_text_eq;//[分配到部门]
	public void setN_department_id_text_eq(String n_department_id_text_eq) {
        this.n_department_id_text_eq = n_department_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_department_id_text_eq)){
            this.getSearchCond().eq("department_id_text", n_department_id_text_eq);
        }
    }
	private String n_department_id_text_like;//[分配到部门]
	public void setN_department_id_text_like(String n_department_id_text_like) {
        this.n_department_id_text_like = n_department_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_department_id_text_like)){
            this.getSearchCond().like("department_id_text", n_department_id_text_like);
        }
    }
	private String n_employee_id_text_eq;//[分配到员工]
	public void setN_employee_id_text_eq(String n_employee_id_text_eq) {
        this.n_employee_id_text_eq = n_employee_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_employee_id_text_eq)){
            this.getSearchCond().eq("employee_id_text", n_employee_id_text_eq);
        }
    }
	private String n_employee_id_text_like;//[分配到员工]
	public void setN_employee_id_text_like(String n_employee_id_text_like) {
        this.n_employee_id_text_like = n_employee_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_employee_id_text_like)){
            this.getSearchCond().like("employee_id_text", n_employee_id_text_like);
        }
    }
	private String n_technician_user_id_text_eq;//[技术员]
	public void setN_technician_user_id_text_eq(String n_technician_user_id_text_eq) {
        this.n_technician_user_id_text_eq = n_technician_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_technician_user_id_text_eq)){
            this.getSearchCond().eq("technician_user_id_text", n_technician_user_id_text_eq);
        }
    }
	private String n_technician_user_id_text_like;//[技术员]
	public void setN_technician_user_id_text_like(String n_technician_user_id_text_like) {
        this.n_technician_user_id_text_like = n_technician_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_technician_user_id_text_like)){
            this.getSearchCond().like("technician_user_id_text", n_technician_user_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_category_id_text_eq;//[设备类别]
	public void setN_category_id_text_eq(String n_category_id_text_eq) {
        this.n_category_id_text_eq = n_category_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_category_id_text_eq)){
            this.getSearchCond().eq("category_id_text", n_category_id_text_eq);
        }
    }
	private String n_category_id_text_like;//[设备类别]
	public void setN_category_id_text_like(String n_category_id_text_like) {
        this.n_category_id_text_like = n_category_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_category_id_text_like)){
            this.getSearchCond().like("category_id_text", n_category_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_partner_id_text_eq;//[供应商]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[供应商]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_owner_user_id_text_eq;//[所有者]
	public void setN_owner_user_id_text_eq(String n_owner_user_id_text_eq) {
        this.n_owner_user_id_text_eq = n_owner_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_owner_user_id_text_eq)){
            this.getSearchCond().eq("owner_user_id_text", n_owner_user_id_text_eq);
        }
    }
	private String n_owner_user_id_text_like;//[所有者]
	public void setN_owner_user_id_text_like(String n_owner_user_id_text_like) {
        this.n_owner_user_id_text_like = n_owner_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_owner_user_id_text_like)){
            this.getSearchCond().like("owner_user_id_text", n_owner_user_id_text_like);
        }
    }
	private String n_maintenance_team_id_text_eq;//[保养团队]
	public void setN_maintenance_team_id_text_eq(String n_maintenance_team_id_text_eq) {
        this.n_maintenance_team_id_text_eq = n_maintenance_team_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_maintenance_team_id_text_eq)){
            this.getSearchCond().eq("maintenance_team_id_text", n_maintenance_team_id_text_eq);
        }
    }
	private String n_maintenance_team_id_text_like;//[保养团队]
	public void setN_maintenance_team_id_text_like(String n_maintenance_team_id_text_like) {
        this.n_maintenance_team_id_text_like = n_maintenance_team_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_maintenance_team_id_text_like)){
            this.getSearchCond().like("maintenance_team_id_text", n_maintenance_team_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_technician_user_id_eq;//[技术员]
	public void setN_technician_user_id_eq(Long n_technician_user_id_eq) {
        this.n_technician_user_id_eq = n_technician_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_technician_user_id_eq)){
            this.getSearchCond().eq("technician_user_id", n_technician_user_id_eq);
        }
    }
	private Long n_partner_id_eq;//[供应商]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_category_id_eq;//[设备类别]
	public void setN_category_id_eq(Long n_category_id_eq) {
        this.n_category_id_eq = n_category_id_eq;
        if(!ObjectUtils.isEmpty(this.n_category_id_eq)){
            this.getSearchCond().eq("category_id", n_category_id_eq);
        }
    }
	private Long n_employee_id_eq;//[分配到员工]
	public void setN_employee_id_eq(Long n_employee_id_eq) {
        this.n_employee_id_eq = n_employee_id_eq;
        if(!ObjectUtils.isEmpty(this.n_employee_id_eq)){
            this.getSearchCond().eq("employee_id", n_employee_id_eq);
        }
    }
	private Long n_maintenance_team_id_eq;//[保养团队]
	public void setN_maintenance_team_id_eq(Long n_maintenance_team_id_eq) {
        this.n_maintenance_team_id_eq = n_maintenance_team_id_eq;
        if(!ObjectUtils.isEmpty(this.n_maintenance_team_id_eq)){
            this.getSearchCond().eq("maintenance_team_id", n_maintenance_team_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_owner_user_id_eq;//[所有者]
	public void setN_owner_user_id_eq(Long n_owner_user_id_eq) {
        this.n_owner_user_id_eq = n_owner_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_owner_user_id_eq)){
            this.getSearchCond().eq("owner_user_id", n_owner_user_id_eq);
        }
    }
	private Long n_department_id_eq;//[分配到部门]
	public void setN_department_id_eq(Long n_department_id_eq) {
        this.n_department_id_eq = n_department_id_eq;
        if(!ObjectUtils.isEmpty(this.n_department_id_eq)){
            this.getSearchCond().eq("department_id", n_department_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




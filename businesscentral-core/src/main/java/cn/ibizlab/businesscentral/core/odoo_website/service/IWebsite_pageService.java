package cn.ibizlab.businesscentral.core.odoo_website.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_page;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_pageSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Website_page] 服务对象接口
 */
public interface IWebsite_pageService extends IService<Website_page>{

    boolean create(Website_page et) ;
    void createBatch(List<Website_page> list) ;
    boolean update(Website_page et) ;
    void updateBatch(List<Website_page> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Website_page get(Long key) ;
    Website_page getDraft(Website_page et) ;
    boolean checkKey(Website_page et) ;
    boolean save(Website_page et) ;
    void saveBatch(List<Website_page> list) ;
    Page<Website_page> searchDefault(Website_pageSearchContext context) ;
    List<Website_page> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Website_page> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Website_page> getWebsitePageByIds(List<Long> ids) ;
    List<Website_page> getWebsitePageByEntities(List<Website_page> entities) ;
}



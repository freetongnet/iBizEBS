package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_routingSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_routing] 服务对象接口
 */
public interface IMrp_routingService extends IService<Mrp_routing>{

    boolean create(Mrp_routing et) ;
    void createBatch(List<Mrp_routing> list) ;
    boolean update(Mrp_routing et) ;
    void updateBatch(List<Mrp_routing> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_routing get(Long key) ;
    Mrp_routing getDraft(Mrp_routing et) ;
    boolean checkKey(Mrp_routing et) ;
    boolean save(Mrp_routing et) ;
    void saveBatch(List<Mrp_routing> list) ;
    Page<Mrp_routing> searchDefault(Mrp_routingSearchContext context) ;
    List<Mrp_routing> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Mrp_routing> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_routing> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mrp_routing> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_routing> getMrpRoutingByIds(List<Long> ids) ;
    List<Mrp_routing> getMrpRoutingByEntities(List<Mrp_routing> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_ir.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence_date_range;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_sequence_date_rangeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Ir_sequence_date_range] 服务对象接口
 */
public interface IIr_sequence_date_rangeService extends IService<Ir_sequence_date_range>{

    boolean create(Ir_sequence_date_range et) ;
    void createBatch(List<Ir_sequence_date_range> list) ;
    boolean update(Ir_sequence_date_range et) ;
    void updateBatch(List<Ir_sequence_date_range> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Ir_sequence_date_range get(Long key) ;
    Ir_sequence_date_range getDraft(Ir_sequence_date_range et) ;
    boolean checkKey(Ir_sequence_date_range et) ;
    Ir_sequence_date_range create_sequence(Ir_sequence_date_range et) ;
    boolean save(Ir_sequence_date_range et) ;
    void saveBatch(List<Ir_sequence_date_range> list) ;
    Ir_sequence_date_range select_nextval(Ir_sequence_date_range et) ;
    Ir_sequence_date_range update_nogap(Ir_sequence_date_range et) ;
    Page<Ir_sequence_date_range> searchDefault(Ir_sequence_date_rangeSearchContext context) ;
    List<Ir_sequence_date_range> selectBySequenceId(Long id);
    void removeBySequenceId(Long id);
    List<Ir_sequence_date_range> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Ir_sequence_date_range> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



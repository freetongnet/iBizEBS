package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[银行设置通用配置]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_SETUP_BANK_MANUAL_CONFIG",resultMap = "Account_setup_bank_manual_configResultMap")
public class Account_setup_bank_manual_config extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 会计日记账
     */
    @TableField(exist = false)
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private String journalId;
    /**
     * 日记账
     */
    @TableField(exist = false)
    @JSONField(name = "linked_journal_id")
    @JsonProperty("linked_journal_id")
    private Integer linkedJournalId;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 新科目名
     */
    @TableField(exist = false)
    @JSONField(name = "new_journal_name")
    @JsonProperty("new_journal_name")
    private String newJournalName;
    /**
     * 代码
     */
    @DEField(name = "new_journal_code")
    @TableField(value = "new_journal_code")
    @JSONField(name = "new_journal_code")
    @JsonProperty("new_journal_code")
    private String newJournalCode;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 创建或链接选项
     */
    @DEField(name = "create_or_link_option")
    @TableField(value = "create_or_link_option")
    @JSONField(name = "create_or_link_option")
    @JsonProperty("create_or_link_option")
    private String createOrLinkOption;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 科目类型
     */
    @TableField(exist = false)
    @JSONField(name = "related_acc_type")
    @JsonProperty("related_acc_type")
    private String relatedAccType;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 账户持有人
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 名称
     */
    @TableField(exist = false)
    @JSONField(name = "bank_name")
    @JsonProperty("bank_name")
    private String bankName;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 银行
     */
    @TableField(exist = false)
    @JSONField(name = "bank_id")
    @JsonProperty("bank_id")
    private Long bankId;
    /**
     * ‎有所有必需的参数‎
     */
    @TableField(exist = false)
    @JSONField(name = "qr_code_valid")
    @JsonProperty("qr_code_valid")
    private Boolean qrCodeValid;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 核对银行账号
     */
    @TableField(exist = false)
    @JSONField(name = "sanitized_acc_number")
    @JsonProperty("sanitized_acc_number")
    private String sanitizedAccNumber;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "acc_type")
    @JsonProperty("acc_type")
    private String accType;
    /**
     * 序号
     */
    @TableField(exist = false)
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 账户号码
     */
    @TableField(exist = false)
    @JSONField(name = "acc_number")
    @JsonProperty("acc_number")
    private String accNumber;
    /**
     * 银行识别代码
     */
    @TableField(exist = false)
    @JSONField(name = "bank_bic")
    @JsonProperty("bank_bic")
    private String bankBic;
    /**
     * 账户持有人名称
     */
    @TableField(exist = false)
    @JSONField(name = "acc_holder_name")
    @JsonProperty("acc_holder_name")
    private String accHolderName;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 业务伙伴银行账户
     */
    @DEField(name = "res_partner_bank_id")
    @TableField(value = "res_partner_bank_id")
    @JSONField(name = "res_partner_bank_id")
    @JsonProperty("res_partner_bank_id")
    private Long resPartnerBankId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank odooResPartnerBank;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [代码]
     */
    public void setNewJournalCode(String newJournalCode){
        this.newJournalCode = newJournalCode ;
        this.modify("new_journal_code",newJournalCode);
    }

    /**
     * 设置 [创建或链接选项]
     */
    public void setCreateOrLinkOption(String createOrLinkOption){
        this.createOrLinkOption = createOrLinkOption ;
        this.modify("create_or_link_option",createOrLinkOption);
    }

    /**
     * 设置 [业务伙伴银行账户]
     */
    public void setResPartnerBankId(Long resPartnerBankId){
        this.resPartnerBankId = resPartnerBankId ;
        this.modify("res_partner_bank_id",resPartnerBankId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



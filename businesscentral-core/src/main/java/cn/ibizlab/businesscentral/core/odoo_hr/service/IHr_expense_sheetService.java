package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expense_sheetSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_expense_sheet] 服务对象接口
 */
public interface IHr_expense_sheetService extends IService<Hr_expense_sheet>{

    boolean create(Hr_expense_sheet et) ;
    void createBatch(List<Hr_expense_sheet> list) ;
    boolean update(Hr_expense_sheet et) ;
    void updateBatch(List<Hr_expense_sheet> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_expense_sheet get(Long key) ;
    Hr_expense_sheet getDraft(Hr_expense_sheet et) ;
    boolean checkKey(Hr_expense_sheet et) ;
    boolean save(Hr_expense_sheet et) ;
    void saveBatch(List<Hr_expense_sheet> list) ;
    Page<Hr_expense_sheet> searchDefault(Hr_expense_sheetSearchContext context) ;
    List<Hr_expense_sheet> selectByBankJournalId(Long id);
    void resetByBankJournalId(Long id);
    void resetByBankJournalId(Collection<Long> ids);
    void removeByBankJournalId(Long id);
    List<Hr_expense_sheet> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Hr_expense_sheet> selectByAccountMoveId(Long id);
    List<Hr_expense_sheet> selectByAccountMoveId(Collection<Long> ids);
    void removeByAccountMoveId(Long id);
    List<Hr_expense_sheet> selectByDepartmentId(Long id);
    void resetByDepartmentId(Long id);
    void resetByDepartmentId(Collection<Long> ids);
    void removeByDepartmentId(Long id);
    List<Hr_expense_sheet> selectByEmployeeId(Long id);
    void resetByEmployeeId(Long id);
    void resetByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Hr_expense_sheet> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Hr_expense_sheet> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Hr_expense_sheet> selectByAddressId(Long id);
    void resetByAddressId(Long id);
    void resetByAddressId(Collection<Long> ids);
    void removeByAddressId(Long id);
    List<Hr_expense_sheet> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_expense_sheet> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Hr_expense_sheet> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_expense_sheet> getHrExpenseSheetByIds(List<Long> ids) ;
    List<Hr_expense_sheet> getHrExpenseSheetByEntities(List<Hr_expense_sheet> entities) ;
}



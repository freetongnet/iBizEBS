package cn.ibizlab.businesscentral.core.odoo_product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category;
/**
 * 关系型数据实体[Product_category] 查询条件对象
 */
@Slf4j
@Data
public class Product_categorySearchContext extends QueryWrapperContext<Product_category> {

	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_property_valuation_eq;//[库存计价]
	public void setN_property_valuation_eq(String n_property_valuation_eq) {
        this.n_property_valuation_eq = n_property_valuation_eq;
        if(!ObjectUtils.isEmpty(this.n_property_valuation_eq)){
            this.getSearchCond().eq("property_valuation", n_property_valuation_eq);
        }
    }
	private String n_property_cost_method_eq;//[成本方法]
	public void setN_property_cost_method_eq(String n_property_cost_method_eq) {
        this.n_property_cost_method_eq = n_property_cost_method_eq;
        if(!ObjectUtils.isEmpty(this.n_property_cost_method_eq)){
            this.getSearchCond().eq("property_cost_method", n_property_cost_method_eq);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_parent_id_text_eq;//[上级类别]
	public void setN_parent_id_text_eq(String n_parent_id_text_eq) {
        this.n_parent_id_text_eq = n_parent_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_eq)){
            this.getSearchCond().eq("parent_id_text", n_parent_id_text_eq);
        }
    }
	private String n_parent_id_text_like;//[上级类别]
	public void setN_parent_id_text_like(String n_parent_id_text_like) {
        this.n_parent_id_text_like = n_parent_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_like)){
            this.getSearchCond().like("parent_id_text", n_parent_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_removal_strategy_id_text_eq;//[强制下架策略]
	public void setN_removal_strategy_id_text_eq(String n_removal_strategy_id_text_eq) {
        this.n_removal_strategy_id_text_eq = n_removal_strategy_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_removal_strategy_id_text_eq)){
            this.getSearchCond().eq("removal_strategy_id_text", n_removal_strategy_id_text_eq);
        }
    }
	private String n_removal_strategy_id_text_like;//[强制下架策略]
	public void setN_removal_strategy_id_text_like(String n_removal_strategy_id_text_like) {
        this.n_removal_strategy_id_text_like = n_removal_strategy_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_removal_strategy_id_text_like)){
            this.getSearchCond().like("removal_strategy_id_text", n_removal_strategy_id_text_like);
        }
    }
	private Long n_parent_id_eq;//[上级类别]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_removal_strategy_id_eq;//[强制下架策略]
	public void setN_removal_strategy_id_eq(Long n_removal_strategy_id_eq) {
        this.n_removal_strategy_id_eq = n_removal_strategy_id_eq;
        if(!ObjectUtils.isEmpty(this.n_removal_strategy_id_eq)){
            this.getSearchCond().eq("removal_strategy_id", n_removal_strategy_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message;
/**
 * 关系型数据实体[Mail_message] 查询条件对象
 */
@Slf4j
@Data
public class Mail_messageSearchContext extends QueryWrapperContext<Mail_message> {

	private String n_model_eq;//[相关的文档模型]
	public void setN_model_eq(String n_model_eq) {
        this.n_model_eq = n_model_eq;
        if(!ObjectUtils.isEmpty(this.n_model_eq)){
            this.getSearchCond().eq("model", n_model_eq);
        }
    }
	private String n_moderation_status_eq;//[管理状态]
	public void setN_moderation_status_eq(String n_moderation_status_eq) {
        this.n_moderation_status_eq = n_moderation_status_eq;
        if(!ObjectUtils.isEmpty(this.n_moderation_status_eq)){
            this.getSearchCond().eq("moderation_status", n_moderation_status_eq);
        }
    }
	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private Integer n_res_id_eq;//[相关文档编号]
	public void setN_res_id_eq(Integer n_res_id_eq) {
        this.n_res_id_eq = n_res_id_eq;
        if(!ObjectUtils.isEmpty(this.n_res_id_eq)){
            this.getSearchCond().eq("res_id", n_res_id_eq);
        }
    }
	private String n_message_type_eq;//[类型]
	public void setN_message_type_eq(String n_message_type_eq) {
        this.n_message_type_eq = n_message_type_eq;
        if(!ObjectUtils.isEmpty(this.n_message_type_eq)){
            this.getSearchCond().eq("message_type", n_message_type_eq);
        }
    }
	private String n_author_id_text_eq;//[作者]
	public void setN_author_id_text_eq(String n_author_id_text_eq) {
        this.n_author_id_text_eq = n_author_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_author_id_text_eq)){
            this.getSearchCond().eq("author_id_text", n_author_id_text_eq);
        }
    }
	private String n_author_id_text_like;//[作者]
	public void setN_author_id_text_like(String n_author_id_text_like) {
        this.n_author_id_text_like = n_author_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_author_id_text_like)){
            this.getSearchCond().like("author_id_text", n_author_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_mail_activity_type_id_text_eq;//[邮件活动类型]
	public void setN_mail_activity_type_id_text_eq(String n_mail_activity_type_id_text_eq) {
        this.n_mail_activity_type_id_text_eq = n_mail_activity_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_mail_activity_type_id_text_eq)){
            this.getSearchCond().eq("mail_activity_type_id_text", n_mail_activity_type_id_text_eq);
        }
    }
	private String n_mail_activity_type_id_text_like;//[邮件活动类型]
	public void setN_mail_activity_type_id_text_like(String n_mail_activity_type_id_text_like) {
        this.n_mail_activity_type_id_text_like = n_mail_activity_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_mail_activity_type_id_text_like)){
            this.getSearchCond().like("mail_activity_type_id_text", n_mail_activity_type_id_text_like);
        }
    }
	private String n_moderator_id_text_eq;//[管理员]
	public void setN_moderator_id_text_eq(String n_moderator_id_text_eq) {
        this.n_moderator_id_text_eq = n_moderator_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_moderator_id_text_eq)){
            this.getSearchCond().eq("moderator_id_text", n_moderator_id_text_eq);
        }
    }
	private String n_moderator_id_text_like;//[管理员]
	public void setN_moderator_id_text_like(String n_moderator_id_text_like) {
        this.n_moderator_id_text_like = n_moderator_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_moderator_id_text_like)){
            this.getSearchCond().like("moderator_id_text", n_moderator_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_subtype_id_text_eq;//[子类型]
	public void setN_subtype_id_text_eq(String n_subtype_id_text_eq) {
        this.n_subtype_id_text_eq = n_subtype_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_subtype_id_text_eq)){
            this.getSearchCond().eq("subtype_id_text", n_subtype_id_text_eq);
        }
    }
	private String n_subtype_id_text_like;//[子类型]
	public void setN_subtype_id_text_like(String n_subtype_id_text_like) {
        this.n_subtype_id_text_like = n_subtype_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_subtype_id_text_like)){
            this.getSearchCond().like("subtype_id_text", n_subtype_id_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_mail_activity_type_id_eq;//[邮件活动类型]
	public void setN_mail_activity_type_id_eq(Long n_mail_activity_type_id_eq) {
        this.n_mail_activity_type_id_eq = n_mail_activity_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mail_activity_type_id_eq)){
            this.getSearchCond().eq("mail_activity_type_id", n_mail_activity_type_id_eq);
        }
    }
	private Long n_moderator_id_eq;//[管理员]
	public void setN_moderator_id_eq(Long n_moderator_id_eq) {
        this.n_moderator_id_eq = n_moderator_id_eq;
        if(!ObjectUtils.isEmpty(this.n_moderator_id_eq)){
            this.getSearchCond().eq("moderator_id", n_moderator_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_author_id_eq;//[作者]
	public void setN_author_id_eq(Long n_author_id_eq) {
        this.n_author_id_eq = n_author_id_eq;
        if(!ObjectUtils.isEmpty(this.n_author_id_eq)){
            this.getSearchCond().eq("author_id", n_author_id_eq);
        }
    }
	private Long n_parent_id_eq;//[上级消息]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_subtype_id_eq;//[子类型]
	public void setN_subtype_id_eq(Long n_subtype_id_eq) {
        this.n_subtype_id_eq = n_subtype_id_eq;
        if(!ObjectUtils.isEmpty(this.n_subtype_id_eq)){
            this.getSearchCond().eq("subtype_id", n_subtype_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




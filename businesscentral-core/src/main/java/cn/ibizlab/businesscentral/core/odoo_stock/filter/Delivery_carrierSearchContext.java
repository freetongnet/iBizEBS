package cn.ibizlab.businesscentral.core.odoo_stock.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Delivery_carrier;
/**
 * 关系型数据实体[Delivery_carrier] 查询条件对象
 */
@Slf4j
@Data
public class Delivery_carrierSearchContext extends QueryWrapperContext<Delivery_carrier> {

	private String n_delivery_type_eq;//[供应商]
	public void setN_delivery_type_eq(String n_delivery_type_eq) {
        this.n_delivery_type_eq = n_delivery_type_eq;
        if(!ObjectUtils.isEmpty(this.n_delivery_type_eq)){
            this.getSearchCond().eq("delivery_type", n_delivery_type_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_invoice_policy_eq;//[发票原则]
	public void setN_invoice_policy_eq(String n_invoice_policy_eq) {
        this.n_invoice_policy_eq = n_invoice_policy_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_policy_eq)){
            this.getSearchCond().eq("invoice_policy", n_invoice_policy_eq);
        }
    }
	private String n_integration_level_eq;//[集成级别]
	public void setN_integration_level_eq(String n_integration_level_eq) {
        this.n_integration_level_eq = n_integration_level_eq;
        if(!ObjectUtils.isEmpty(this.n_integration_level_eq)){
            this.getSearchCond().eq("integration_level", n_integration_level_eq);
        }
    }
	private String n_product_name_eq;//[名称]
	public void setN_product_name_eq(String n_product_name_eq) {
        this.n_product_name_eq = n_product_name_eq;
        if(!ObjectUtils.isEmpty(this.n_product_name_eq)){
            this.getSearchCond().eq("product_name", n_product_name_eq);
        }
    }
	private String n_product_name_like;//[名称]
	public void setN_product_name_like(String n_product_name_like) {
        this.n_product_name_like = n_product_name_like;
        if(!ObjectUtils.isEmpty(this.n_product_name_like)){
            this.getSearchCond().like("product_name", n_product_name_like);
        }
    }
	private String n_company_name_eq;//[公司名称]
	public void setN_company_name_eq(String n_company_name_eq) {
        this.n_company_name_eq = n_company_name_eq;
        if(!ObjectUtils.isEmpty(this.n_company_name_eq)){
            this.getSearchCond().eq("company_name", n_company_name_eq);
        }
    }
	private String n_company_name_like;//[公司名称]
	public void setN_company_name_like(String n_company_name_like) {
        this.n_company_name_like = n_company_name_like;
        if(!ObjectUtils.isEmpty(this.n_company_name_like)){
            this.getSearchCond().like("company_name", n_company_name_like);
        }
    }
	private String n_write_uname_eq;//[名称]
	public void setN_write_uname_eq(String n_write_uname_eq) {
        this.n_write_uname_eq = n_write_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uname_eq)){
            this.getSearchCond().eq("write_uname", n_write_uname_eq);
        }
    }
	private String n_write_uname_like;//[名称]
	public void setN_write_uname_like(String n_write_uname_like) {
        this.n_write_uname_like = n_write_uname_like;
        if(!ObjectUtils.isEmpty(this.n_write_uname_like)){
            this.getSearchCond().like("write_uname", n_write_uname_like);
        }
    }
	private String n_create_uname_eq;//[名称]
	public void setN_create_uname_eq(String n_create_uname_eq) {
        this.n_create_uname_eq = n_create_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uname_eq)){
            this.getSearchCond().eq("create_uname", n_create_uname_eq);
        }
    }
	private String n_create_uname_like;//[名称]
	public void setN_create_uname_like(String n_create_uname_like) {
        this.n_create_uname_like = n_create_uname_like;
        if(!ObjectUtils.isEmpty(this.n_create_uname_like)){
            this.getSearchCond().like("create_uname", n_create_uname_like);
        }
    }
	private Long n_write_uid_eq;//[ID]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_company_id_eq;//[ID]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_product_id_eq;//[ID]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_create_uid_eq;//[ID]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




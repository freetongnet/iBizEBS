package cn.ibizlab.businesscentral.core.odoo_board.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_board.domain.Board_board;
import cn.ibizlab.businesscentral.core.odoo_board.filter.Board_boardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[board_board] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-board:odoo-board}", contextId = "board-board", fallback = board_boardFallback.class)
public interface board_boardFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/board_boards/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/board_boards/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/board_boards")
    Board_board create(@RequestBody Board_board board_board);

    @RequestMapping(method = RequestMethod.POST, value = "/board_boards/batch")
    Boolean createBatch(@RequestBody List<Board_board> board_boards);


    @RequestMapping(method = RequestMethod.PUT, value = "/board_boards/{id}")
    Board_board update(@PathVariable("id") Long id,@RequestBody Board_board board_board);

    @RequestMapping(method = RequestMethod.PUT, value = "/board_boards/batch")
    Boolean updateBatch(@RequestBody List<Board_board> board_boards);



    @RequestMapping(method = RequestMethod.POST, value = "/board_boards/search")
    Page<Board_board> search(@RequestBody Board_boardSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/board_boards/{id}")
    Board_board get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.GET, value = "/board_boards/select")
    Page<Board_board> select();


    @RequestMapping(method = RequestMethod.GET, value = "/board_boards/getdraft")
    Board_board getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/board_boards/checkkey")
    Boolean checkKey(@RequestBody Board_board board_board);


    @RequestMapping(method = RequestMethod.POST, value = "/board_boards/save")
    Boolean save(@RequestBody Board_board board_board);

    @RequestMapping(method = RequestMethod.POST, value = "/board_boards/savebatch")
    Boolean saveBatch(@RequestBody List<Board_board> board_boards);



    @RequestMapping(method = RequestMethod.POST, value = "/board_boards/searchdefault")
    Page<Board_board> searchDefault(@RequestBody Board_boardSearchContext context);


}

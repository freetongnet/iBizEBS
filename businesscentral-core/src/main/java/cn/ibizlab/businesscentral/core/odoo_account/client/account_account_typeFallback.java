package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_type;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_account_typeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_account_type] 服务对象接口
 */
@Component
public class account_account_typeFallback implements account_account_typeFeignClient{



    public Account_account_type create(Account_account_type account_account_type){
            return null;
     }
    public Boolean createBatch(List<Account_account_type> account_account_types){
            return false;
     }

    public Page<Account_account_type> search(Account_account_typeSearchContext context){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Account_account_type update(Long id, Account_account_type account_account_type){
            return null;
     }
    public Boolean updateBatch(List<Account_account_type> account_account_types){
            return false;
     }


    public Account_account_type get(Long id){
            return null;
     }


    public Page<Account_account_type> select(){
            return null;
     }

    public Account_account_type getDraft(){
            return null;
    }



    public Boolean checkKey(Account_account_type account_account_type){
            return false;
     }


    public Boolean save(Account_account_type account_account_type){
            return false;
     }
    public Boolean saveBatch(List<Account_account_type> account_account_types){
            return false;
     }

    public Page<Account_account_type> searchDefault(Account_account_typeSearchContext context){
            return null;
     }


}

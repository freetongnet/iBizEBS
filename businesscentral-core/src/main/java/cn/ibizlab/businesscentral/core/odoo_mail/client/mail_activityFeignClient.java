package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_activitySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_activity] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-activity", fallback = mail_activityFallback.class)
public interface mail_activityFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/mail_activities/search")
    Page<Mail_activity> search(@RequestBody Mail_activitySearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_activities/{id}")
    Mail_activity get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_activities/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_activities/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/mail_activities/{id}")
    Mail_activity update(@PathVariable("id") Long id,@RequestBody Mail_activity mail_activity);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_activities/batch")
    Boolean updateBatch(@RequestBody List<Mail_activity> mail_activities);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_activities")
    Mail_activity create(@RequestBody Mail_activity mail_activity);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_activities/batch")
    Boolean createBatch(@RequestBody List<Mail_activity> mail_activities);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_activities/select")
    Page<Mail_activity> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_activities/getdraft")
    Mail_activity getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_activities/{id}/action_done")
    Mail_activity action_done(@PathVariable("id") Long id,@RequestBody Mail_activity mail_activity);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_activities/checkkey")
    Boolean checkKey(@RequestBody Mail_activity mail_activity);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_activities/save")
    Boolean save(@RequestBody Mail_activity mail_activity);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_activities/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_activity> mail_activities);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_activities/searchdefault")
    Page<Mail_activity> searchDefault(@RequestBody Mail_activitySearchContext context);


}

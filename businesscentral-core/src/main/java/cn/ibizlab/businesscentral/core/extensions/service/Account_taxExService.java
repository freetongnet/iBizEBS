package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.odoo_account.service.impl.Account_taxServiceImpl;
import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;
import java.util.*;

/**
 * 实体[税率] 自定义服务对象
 */
@Slf4j
@Primary
@Service("Account_taxExService")
public class Account_taxExService extends Account_taxServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 自定义行为[Calc_tax]用户扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public Account_tax calc_tax(Account_tax et) {
        return super.calc_tax(et);
    }
}


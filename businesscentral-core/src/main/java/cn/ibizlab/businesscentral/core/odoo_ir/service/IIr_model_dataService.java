package cn.ibizlab.businesscentral.core.odoo_ir.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_model_data;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_model_dataSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Ir_model_data] 服务对象接口
 */
public interface IIr_model_dataService extends IService<Ir_model_data>{

    boolean create(Ir_model_data et) ;
    void createBatch(List<Ir_model_data> list) ;
    boolean update(Ir_model_data et) ;
    void updateBatch(List<Ir_model_data> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Ir_model_data get(Long key) ;
    Ir_model_data getDraft(Ir_model_data et) ;
    boolean checkKey(Ir_model_data et) ;
    boolean save(Ir_model_data et) ;
    void saveBatch(List<Ir_model_data> list) ;
    Page<Ir_model_data> searchDefault(Ir_model_dataSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



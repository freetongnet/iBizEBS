package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_scheduler_compute;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_scheduler_computeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_scheduler_compute] 服务对象接口
 */
public interface IStock_scheduler_computeService extends IService<Stock_scheduler_compute>{

    boolean create(Stock_scheduler_compute et) ;
    void createBatch(List<Stock_scheduler_compute> list) ;
    boolean update(Stock_scheduler_compute et) ;
    void updateBatch(List<Stock_scheduler_compute> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_scheduler_compute get(Long key) ;
    Stock_scheduler_compute getDraft(Stock_scheduler_compute et) ;
    boolean checkKey(Stock_scheduler_compute et) ;
    boolean save(Stock_scheduler_compute et) ;
    void saveBatch(List<Stock_scheduler_compute> list) ;
    Page<Stock_scheduler_compute> searchDefault(Stock_scheduler_computeSearchContext context) ;
    List<Stock_scheduler_compute> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_scheduler_compute> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_scheduler_compute> getStockSchedulerComputeByIds(List<Long> ids) ;
    List<Stock_scheduler_compute> getStockSchedulerComputeByEntities(List<Stock_scheduler_compute> entities) ;
}



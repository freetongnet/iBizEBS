package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_return_picking;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_return_pickingSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_return_pickingService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_return_pickingMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[退回拣货] 服务对象接口实现
 */
@Slf4j
@Service("Stock_return_pickingServiceImpl")
public class Stock_return_pickingServiceImpl extends EBSServiceImpl<Stock_return_pickingMapper, Stock_return_picking> implements IStock_return_pickingService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_return_picking_lineService stockReturnPickingLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.return.picking" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_return_picking et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_return_pickingService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_return_picking> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_return_picking et) {
        Stock_return_picking old = new Stock_return_picking() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_return_pickingService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_return_pickingService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_return_picking> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        stockReturnPickingLineService.resetByWizardId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        stockReturnPickingLineService.resetByWizardId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_return_picking get(Long key) {
        Stock_return_picking et = getById(key);
        if(et==null){
            et=new Stock_return_picking();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_return_picking getDraft(Stock_return_picking et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_return_picking et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_return_picking et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_return_picking et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_return_picking> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_return_picking> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_return_picking> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_return_picking>().eq("create_uid",id));
    }

	@Override
    public List<Stock_return_picking> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_return_picking>().eq("write_uid",id));
    }

	@Override
    public List<Stock_return_picking> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_return_picking>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_return_picking>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_return_picking>().eq("location_id",id));
    }

	@Override
    public List<Stock_return_picking> selectByOriginalLocationId(Long id) {
        return baseMapper.selectByOriginalLocationId(id);
    }
    @Override
    public void resetByOriginalLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_return_picking>().set("original_location_id",null).eq("original_location_id",id));
    }

    @Override
    public void resetByOriginalLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_return_picking>().set("original_location_id",null).in("original_location_id",ids));
    }

    @Override
    public void removeByOriginalLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_return_picking>().eq("original_location_id",id));
    }

	@Override
    public List<Stock_return_picking> selectByParentLocationId(Long id) {
        return baseMapper.selectByParentLocationId(id);
    }
    @Override
    public void resetByParentLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_return_picking>().set("parent_location_id",null).eq("parent_location_id",id));
    }

    @Override
    public void resetByParentLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_return_picking>().set("parent_location_id",null).in("parent_location_id",ids));
    }

    @Override
    public void removeByParentLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_return_picking>().eq("parent_location_id",id));
    }

	@Override
    public List<Stock_return_picking> selectByPickingId(Long id) {
        return baseMapper.selectByPickingId(id);
    }
    @Override
    public void resetByPickingId(Long id) {
        this.update(new UpdateWrapper<Stock_return_picking>().set("picking_id",null).eq("picking_id",id));
    }

    @Override
    public void resetByPickingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_return_picking>().set("picking_id",null).in("picking_id",ids));
    }

    @Override
    public void removeByPickingId(Long id) {
        this.remove(new QueryWrapper<Stock_return_picking>().eq("picking_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_return_picking> searchDefault(Stock_return_pickingSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_return_picking> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_return_picking>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_return_picking et){
        //实体关系[DER1N_STOCK_RETURN_PICKING__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_RETURN_PICKING__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_RETURN_PICKING__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_STOCK_RETURN_PICKING__STOCK_LOCATION__ORIGINAL_LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getOriginalLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooOriginalLocation=et.getOdooOriginalLocation();
            if(ObjectUtils.isEmpty(odooOriginalLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getOriginalLocationId());
                et.setOdooOriginalLocation(majorEntity);
                odooOriginalLocation=majorEntity;
            }
            et.setOriginalLocationIdText(odooOriginalLocation.getName());
        }
        //实体关系[DER1N_STOCK_RETURN_PICKING__STOCK_LOCATION__PARENT_LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getParentLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooParentLocation=et.getOdooParentLocation();
            if(ObjectUtils.isEmpty(odooParentLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getParentLocationId());
                et.setOdooParentLocation(majorEntity);
                odooParentLocation=majorEntity;
            }
            et.setParentLocationIdText(odooParentLocation.getName());
        }
        //实体关系[DER1N_STOCK_RETURN_PICKING__STOCK_PICKING__PICKING_ID]
        if(!ObjectUtils.isEmpty(et.getPickingId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking odooPicking=et.getOdooPicking();
            if(ObjectUtils.isEmpty(odooPicking)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking majorEntity=stockPickingService.get(et.getPickingId());
                et.setOdooPicking(majorEntity);
                odooPicking=majorEntity;
            }
            et.setPickingIdText(odooPicking.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_return_picking> getStockReturnPickingByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_return_picking> getStockReturnPickingByEntities(List<Stock_return_picking> entities) {
        List ids =new ArrayList();
        for(Stock_return_picking entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




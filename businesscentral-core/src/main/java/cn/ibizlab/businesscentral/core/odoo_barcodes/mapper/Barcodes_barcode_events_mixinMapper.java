package cn.ibizlab.businesscentral.core.odoo_barcodes.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.businesscentral.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Barcodes_barcode_events_mixinMapper extends BaseMapper<Barcodes_barcode_events_mixin>{

    Page<Barcodes_barcode_events_mixin> searchDefault(IPage page, @Param("srf") Barcodes_barcode_events_mixinSearchContext context, @Param("ew") Wrapper<Barcodes_barcode_events_mixin> wrapper) ;
    @Override
    Barcodes_barcode_events_mixin selectById(Serializable id);
    @Override
    int insert(Barcodes_barcode_events_mixin entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Barcodes_barcode_events_mixin entity);
    @Override
    int update(@Param(Constants.ENTITY) Barcodes_barcode_events_mixin entity, @Param("ew") Wrapper<Barcodes_barcode_events_mixin> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);


}

package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_abstract_paymentSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_abstract_payment] 服务对象接口
 */
@Component
public class account_abstract_paymentFallback implements account_abstract_paymentFeignClient{

    public Account_abstract_payment create(Account_abstract_payment account_abstract_payment){
            return null;
     }
    public Boolean createBatch(List<Account_abstract_payment> account_abstract_payments){
            return false;
     }


    public Account_abstract_payment get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Account_abstract_payment> search(Account_abstract_paymentSearchContext context){
            return null;
     }



    public Account_abstract_payment update(Long id, Account_abstract_payment account_abstract_payment){
            return null;
     }
    public Boolean updateBatch(List<Account_abstract_payment> account_abstract_payments){
            return false;
     }



    public Page<Account_abstract_payment> select(){
            return null;
     }

    public Account_abstract_payment getDraft(){
            return null;
    }



    public Boolean checkKey(Account_abstract_payment account_abstract_payment){
            return false;
     }


    public Boolean save(Account_abstract_payment account_abstract_payment){
            return false;
     }
    public Boolean saveBatch(List<Account_abstract_payment> account_abstract_payments){
            return false;
     }

    public Page<Account_abstract_payment> searchDefault(Account_abstract_paymentSearchContext context){
            return null;
     }


}

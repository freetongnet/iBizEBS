package cn.ibizlab.businesscentral.core.odoo_board.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_board.domain.Board_board;
import cn.ibizlab.businesscentral.core.odoo_board.filter.Board_boardSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Board_board] 服务对象接口
 */
public interface IBoard_boardService extends IService<Board_board>{

    boolean create(Board_board et) ;
    void createBatch(List<Board_board> list) ;
    boolean update(Board_board et) ;
    void updateBatch(List<Board_board> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Board_board get(Long key) ;
    Board_board getDraft(Board_board et) ;
    boolean checkKey(Board_board et) ;
    boolean save(Board_board et) ;
    void saveBatch(List<Board_board> list) ;
    Page<Board_board> searchDefault(Board_boardSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



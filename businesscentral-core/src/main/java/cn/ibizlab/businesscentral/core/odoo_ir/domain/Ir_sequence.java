package cn.ibizlab.businesscentral.core.odoo_ir.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[序列]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "IR_SEQUENCE",resultMap = "Ir_sequenceResultMap")
public class Ir_sequence extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 每个日期范围使用不同序号
     */
    @DEField(name = "use_date_range")
    @TableField(value = "use_date_range")
    @JSONField(name = "use_date_range")
    @JsonProperty("use_date_range")
    private Boolean useDateRange;
    /**
     * 增量
     */
    @DEField(defaultValue = "1")
    @TableField(value = "number_increment")
    @JSONField(name = "number_increment")
    @JsonProperty("number_increment")
    private Integer numberIncrement;
    /**
     * 后缀
     */
    @TableField(value = "suffix")
    @JSONField(name = "suffix")
    @JsonProperty("suffix")
    private String suffix;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * next_char
     */
    @TableField(exist = false)
    @JSONField(name = "next_char")
    @JsonProperty("next_char")
    private String nextChar;
    /**
     * next_val
     */
    @TableField(exist = false)
    @JSONField(name = "next_val")
    @JsonProperty("next_val")
    private Integer nextVal;
    /**
     * Padding
     */
    @DEField(defaultValue = "1")
    @TableField(value = "padding")
    @JSONField(name = "padding")
    @JsonProperty("padding")
    private Integer padding;
    /**
     * 创建时间
     */
    @DEField(name = "create_date")
    @TableField(value = "create_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 活动
     */
    @DEField(defaultValue = "true")
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * Next
     */
    @DEField(defaultValue = "1")
    @TableField(value = "number_next")
    @JSONField(name = "number_next")
    @JsonProperty("number_next")
    private Integer numberNext;
    /**
     * 序列名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 代码
     */
    @TableField(value = "code")
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date")
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 实施
     */
    @DEField(defaultValue = "standard")
    @TableField(value = "implementation")
    @JSONField(name = "implementation")
    @JsonProperty("implementation")
    private String implementation;
    /**
     * 前缀
     */
    @TableField(value = "prefix")
    @JSONField(name = "prefix")
    @JsonProperty("prefix")
    private String prefix;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    private String createUname;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    private String writeUname;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid")
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid")
    @TableField(value = "create_uid")
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 公司
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [每个日期范围使用不同序号]
     */
    public void setUseDateRange(Boolean useDateRange){
        this.useDateRange = useDateRange ;
        this.modify("use_date_range",useDateRange);
    }

    /**
     * 设置 [增量]
     */
    public void setNumberIncrement(Integer numberIncrement){
        this.numberIncrement = numberIncrement ;
        this.modify("number_increment",numberIncrement);
    }

    /**
     * 设置 [后缀]
     */
    public void setSuffix(String suffix){
        this.suffix = suffix ;
        this.modify("suffix",suffix);
    }

    /**
     * 设置 [Padding]
     */
    public void setPadding(Integer padding){
        this.padding = padding ;
        this.modify("padding",padding);
    }

    /**
     * 设置 [创建时间]
     */
    public void setCreateDate(Timestamp createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 格式化日期 [创建时间]
     */
    public String formatCreateDate(){
        if (this.createDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(createDate);
    }
    /**
     * 设置 [活动]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [Next]
     */
    public void setNumberNext(Integer numberNext){
        this.numberNext = numberNext ;
        this.modify("number_next",numberNext);
    }

    /**
     * 设置 [序列名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [代码]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }

    /**
     * 设置 [最后更新时间]
     */
    public void setWriteDate(Timestamp writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 格式化日期 [最后更新时间]
     */
    public String formatWriteDate(){
        if (this.writeDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(writeDate);
    }
    /**
     * 设置 [实施]
     */
    public void setImplementation(String implementation){
        this.implementation = implementation ;
        this.modify("implementation",implementation);
    }

    /**
     * 设置 [前缀]
     */
    public void setPrefix(String prefix){
        this.prefix = prefix ;
        this.modify("prefix",prefix);
    }

    /**
     * 设置 [最后更新人]
     */
    public void setWriteUid(Long writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [创建人]
     */
    public void setCreateUid(Long createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



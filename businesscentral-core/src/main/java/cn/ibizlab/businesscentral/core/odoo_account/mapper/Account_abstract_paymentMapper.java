package cn.ibizlab.businesscentral.core.odoo_account.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_abstract_paymentSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Account_abstract_paymentMapper extends BaseMapper<Account_abstract_payment>{

    Page<Account_abstract_payment> searchDefault(IPage page, @Param("srf") Account_abstract_paymentSearchContext context, @Param("ew") Wrapper<Account_abstract_payment> wrapper) ;
    @Override
    Account_abstract_payment selectById(Serializable id);
    @Override
    int insert(Account_abstract_payment entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Account_abstract_payment entity);
    @Override
    int update(@Param(Constants.ENTITY) Account_abstract_payment entity, @Param("ew") Wrapper<Account_abstract_payment> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Account_abstract_payment> selectByWriteoffAccountId(@Param("id") Serializable id) ;

    List<Account_abstract_payment> selectByJournalId(@Param("id") Serializable id) ;

    List<Account_abstract_payment> selectByPaymentMethodId(@Param("id") Serializable id) ;

    List<Account_abstract_payment> selectByCurrencyId(@Param("id") Serializable id) ;

    List<Account_abstract_payment> selectByPartnerBankAccountId(@Param("id") Serializable id) ;

    List<Account_abstract_payment> selectByPartnerId(@Param("id") Serializable id) ;


}

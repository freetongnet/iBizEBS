package cn.ibizlab.businesscentral.core.odoo_project.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_task_typeSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Project_task_typeMapper extends BaseMapper<Project_task_type>{

    Page<Project_task_type> searchDefault(IPage page, @Param("srf") Project_task_typeSearchContext context, @Param("ew") Wrapper<Project_task_type> wrapper) ;
    @Override
    Project_task_type selectById(Serializable id);
    @Override
    int insert(Project_task_type entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Project_task_type entity);
    @Override
    int update(@Param(Constants.ENTITY) Project_task_type entity, @Param("ew") Wrapper<Project_task_type> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Project_task_type> selectByMailTemplateId(@Param("id") Serializable id) ;

    List<Project_task_type> selectByRatingTemplateId(@Param("id") Serializable id) ;

    List<Project_task_type> selectByCreateUid(@Param("id") Serializable id) ;

    List<Project_task_type> selectByWriteUid(@Param("id") Serializable id) ;


}

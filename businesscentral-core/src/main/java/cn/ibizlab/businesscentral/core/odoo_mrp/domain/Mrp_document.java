package cn.ibizlab.businesscentral.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[生产文档]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MRP_DOCUMENT",resultMap = "Mrp_documentResultMap")
public class Mrp_document extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 资源字段
     */
    @TableField(exist = false)
    @JSONField(name = "res_field")
    @JsonProperty("res_field")
    private String resField;
    /**
     * 资源名称
     */
    @TableField(exist = false)
    @JSONField(name = "res_name")
    @JsonProperty("res_name")
    private String resName;
    /**
     * RES型号名称
     */
    @TableField(exist = false)
    @JSONField(name = "res_model_name")
    @JsonProperty("res_model_name")
    private String resModelName;
    /**
     * 文件名
     */
    @TableField(exist = false)
    @JSONField(name = "datas_fname")
    @JsonProperty("datas_fname")
    private String datasFname;
    /**
     * 主题模板
     */
    @TableField(exist = false)
    @JSONField(name = "theme_template_id")
    @JsonProperty("theme_template_id")
    private Integer themeTemplateId;
    /**
     * MIME 类型
     */
    @TableField(exist = false)
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;
    /**
     * 资源ID
     */
    @TableField(exist = false)
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;
    /**
     * 存储的文件名
     */
    @TableField(exist = false)
    @JSONField(name = "store_fname")
    @JsonProperty("store_fname")
    private String storeFname;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 附件网址
     */
    @TableField(exist = false)
    @JSONField(name = "local_url")
    @JsonProperty("local_url")
    private String localUrl;
    /**
     * 键
     */
    @TableField(exist = false)
    @JSONField(name = "key")
    @JsonProperty("key")
    private String key;
    /**
     * 是公开文档
     */
    @TableField(exist = false)
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    private Boolean ibizpublic;
    /**
     * 资源模型
     */
    @TableField(exist = false)
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 略所图
     */
    @TableField(exist = false)
    @JSONField(name = "thumbnail")
    @JsonProperty("thumbnail")
    private byte[] thumbnail;
    /**
     * Url网址
     */
    @TableField(exist = false)
    @JSONField(name = "url")
    @JsonProperty("url")
    private String url;
    /**
     * 文件大小
     */
    @TableField(exist = false)
    @JSONField(name = "file_size")
    @JsonProperty("file_size")
    private Integer fileSize;
    /**
     * 访问令牌
     */
    @TableField(exist = false)
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;
    /**
     * 相关的附件
     */
    @DEField(name = "ir_attachment_id")
    @TableField(value = "ir_attachment_id")
    @JSONField(name = "ir_attachment_id")
    @JsonProperty("ir_attachment_id")
    private Integer irAttachmentId;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 校验和/SHA1
     */
    @TableField(exist = false)
    @JSONField(name = "checksum")
    @JsonProperty("checksum")
    private String checksum;
    /**
     * 数据库数据
     */
    @TableField(exist = false)
    @JSONField(name = "db_datas")
    @JsonProperty("db_datas")
    private byte[] dbDatas;
    /**
     * 索引的内容
     */
    @TableField(exist = false)
    @JSONField(name = "index_content")
    @JsonProperty("index_content")
    private String indexContent;
    /**
     * 说明
     */
    @TableField(exist = false)
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 名称
     */
    @TableField(exist = false)
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 网站网址
     */
    @TableField(exist = false)
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;
    /**
     * 网站
     */
    @TableField(exist = false)
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 文件内容
     */
    @TableField(exist = false)
    @JSONField(name = "datas")
    @JsonProperty("datas")
    private byte[] datas;
    /**
     * 优先级
     */
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [相关的附件]
     */
    public void setIrAttachmentId(Integer irAttachmentId){
        this.irAttachmentId = irAttachmentId ;
        this.modify("ir_attachment_id",irAttachmentId);
    }

    /**
     * 设置 [优先级]
     */
    public void setPriority(String priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



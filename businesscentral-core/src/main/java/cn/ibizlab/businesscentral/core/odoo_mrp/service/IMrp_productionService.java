package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_productionSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_production] 服务对象接口
 */
public interface IMrp_productionService extends IService<Mrp_production>{

    boolean create(Mrp_production et) ;
    void createBatch(List<Mrp_production> list) ;
    boolean update(Mrp_production et) ;
    void updateBatch(List<Mrp_production> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_production get(Long key) ;
    Mrp_production getDraft(Mrp_production et) ;
    boolean checkKey(Mrp_production et) ;
    boolean save(Mrp_production et) ;
    void saveBatch(List<Mrp_production> list) ;
    Page<Mrp_production> searchDefault(Mrp_productionSearchContext context) ;
    List<Mrp_production> selectByBomId(Long id);
    void resetByBomId(Long id);
    void resetByBomId(Collection<Long> ids);
    void removeByBomId(Long id);
    List<Mrp_production> selectByRoutingId(Long id);
    void resetByRoutingId(Long id);
    void resetByRoutingId(Collection<Long> ids);
    void removeByRoutingId(Long id);
    List<Mrp_production> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Mrp_production> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Mrp_production> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_production> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Mrp_production> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mrp_production> selectByLocationDestId(Long id);
    void resetByLocationDestId(Long id);
    void resetByLocationDestId(Collection<Long> ids);
    void removeByLocationDestId(Long id);
    List<Mrp_production> selectByLocationSrcId(Long id);
    void resetByLocationSrcId(Long id);
    void resetByLocationSrcId(Collection<Long> ids);
    void removeByLocationSrcId(Long id);
    List<Mrp_production> selectByPickingTypeId(Long id);
    void resetByPickingTypeId(Long id);
    void resetByPickingTypeId(Collection<Long> ids);
    void removeByPickingTypeId(Long id);
    List<Mrp_production> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_production> getMrpProductionByIds(List<Long> ids) ;
    List<Mrp_production> getMrpProductionByEntities(List<Mrp_production> entities) ;
}



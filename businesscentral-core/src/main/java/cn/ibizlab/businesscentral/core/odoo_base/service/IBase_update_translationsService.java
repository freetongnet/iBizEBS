package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_update_translations;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_update_translationsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Base_update_translations] 服务对象接口
 */
public interface IBase_update_translationsService extends IService<Base_update_translations>{

    boolean create(Base_update_translations et) ;
    void createBatch(List<Base_update_translations> list) ;
    boolean update(Base_update_translations et) ;
    void updateBatch(List<Base_update_translations> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Base_update_translations get(Long key) ;
    Base_update_translations getDraft(Base_update_translations et) ;
    boolean checkKey(Base_update_translations et) ;
    boolean save(Base_update_translations et) ;
    void saveBatch(List<Base_update_translations> list) ;
    Page<Base_update_translations> searchDefault(Base_update_translationsSearchContext context) ;
    List<Base_update_translations> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Base_update_translations> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Base_update_translations> getBaseUpdateTranslationsByIds(List<Long> ids) ;
    List<Base_update_translations> getBaseUpdateTranslationsByEntities(List<Base_update_translations> entities) ;
}



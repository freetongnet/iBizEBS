package cn.ibizlab.businesscentral.core.odoo_gamification.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge;
/**
 * 关系型数据实体[Gamification_challenge] 查询条件对象
 */
@Slf4j
@Data
public class Gamification_challengeSearchContext extends QueryWrapperContext<Gamification_challenge> {

	private String n_name_like;//[挑战名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_category_eq;//[出现在]
	public void setN_category_eq(String n_category_eq) {
        this.n_category_eq = n_category_eq;
        if(!ObjectUtils.isEmpty(this.n_category_eq)){
            this.getSearchCond().eq("category", n_category_eq);
        }
    }
	private String n_report_message_frequency_eq;//[报告的频率]
	public void setN_report_message_frequency_eq(String n_report_message_frequency_eq) {
        this.n_report_message_frequency_eq = n_report_message_frequency_eq;
        if(!ObjectUtils.isEmpty(this.n_report_message_frequency_eq)){
            this.getSearchCond().eq("report_message_frequency", n_report_message_frequency_eq);
        }
    }
	private String n_period_eq;//[周期]
	public void setN_period_eq(String n_period_eq) {
        this.n_period_eq = n_period_eq;
        if(!ObjectUtils.isEmpty(this.n_period_eq)){
            this.getSearchCond().eq("period", n_period_eq);
        }
    }
	private String n_visibility_mode_eq;//[显示模式]
	public void setN_visibility_mode_eq(String n_visibility_mode_eq) {
        this.n_visibility_mode_eq = n_visibility_mode_eq;
        if(!ObjectUtils.isEmpty(this.n_visibility_mode_eq)){
            this.getSearchCond().eq("visibility_mode", n_visibility_mode_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_manager_id_text_eq;//[负责人]
	public void setN_manager_id_text_eq(String n_manager_id_text_eq) {
        this.n_manager_id_text_eq = n_manager_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_manager_id_text_eq)){
            this.getSearchCond().eq("manager_id_text", n_manager_id_text_eq);
        }
    }
	private String n_manager_id_text_like;//[负责人]
	public void setN_manager_id_text_like(String n_manager_id_text_like) {
        this.n_manager_id_text_like = n_manager_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_manager_id_text_like)){
            this.getSearchCond().like("manager_id_text", n_manager_id_text_like);
        }
    }
	private String n_report_message_group_id_text_eq;//[抄送]
	public void setN_report_message_group_id_text_eq(String n_report_message_group_id_text_eq) {
        this.n_report_message_group_id_text_eq = n_report_message_group_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_report_message_group_id_text_eq)){
            this.getSearchCond().eq("report_message_group_id_text", n_report_message_group_id_text_eq);
        }
    }
	private String n_report_message_group_id_text_like;//[抄送]
	public void setN_report_message_group_id_text_like(String n_report_message_group_id_text_like) {
        this.n_report_message_group_id_text_like = n_report_message_group_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_report_message_group_id_text_like)){
            this.getSearchCond().like("report_message_group_id_text", n_report_message_group_id_text_like);
        }
    }
	private String n_report_template_id_text_eq;//[报告模板]
	public void setN_report_template_id_text_eq(String n_report_template_id_text_eq) {
        this.n_report_template_id_text_eq = n_report_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_report_template_id_text_eq)){
            this.getSearchCond().eq("report_template_id_text", n_report_template_id_text_eq);
        }
    }
	private String n_report_template_id_text_like;//[报告模板]
	public void setN_report_template_id_text_like(String n_report_template_id_text_like) {
        this.n_report_template_id_text_like = n_report_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_report_template_id_text_like)){
            this.getSearchCond().like("report_template_id_text", n_report_template_id_text_like);
        }
    }
	private String n_reward_id_text_eq;//[每位获得成功的用户]
	public void setN_reward_id_text_eq(String n_reward_id_text_eq) {
        this.n_reward_id_text_eq = n_reward_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_reward_id_text_eq)){
            this.getSearchCond().eq("reward_id_text", n_reward_id_text_eq);
        }
    }
	private String n_reward_id_text_like;//[每位获得成功的用户]
	public void setN_reward_id_text_like(String n_reward_id_text_like) {
        this.n_reward_id_text_like = n_reward_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_reward_id_text_like)){
            this.getSearchCond().like("reward_id_text", n_reward_id_text_like);
        }
    }
	private String n_reward_first_id_text_eq;//[第一位用户]
	public void setN_reward_first_id_text_eq(String n_reward_first_id_text_eq) {
        this.n_reward_first_id_text_eq = n_reward_first_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_reward_first_id_text_eq)){
            this.getSearchCond().eq("reward_first_id_text", n_reward_first_id_text_eq);
        }
    }
	private String n_reward_first_id_text_like;//[第一位用户]
	public void setN_reward_first_id_text_like(String n_reward_first_id_text_like) {
        this.n_reward_first_id_text_like = n_reward_first_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_reward_first_id_text_like)){
            this.getSearchCond().like("reward_first_id_text", n_reward_first_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_reward_second_id_text_eq;//[第二位用户]
	public void setN_reward_second_id_text_eq(String n_reward_second_id_text_eq) {
        this.n_reward_second_id_text_eq = n_reward_second_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_reward_second_id_text_eq)){
            this.getSearchCond().eq("reward_second_id_text", n_reward_second_id_text_eq);
        }
    }
	private String n_reward_second_id_text_like;//[第二位用户]
	public void setN_reward_second_id_text_like(String n_reward_second_id_text_like) {
        this.n_reward_second_id_text_like = n_reward_second_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_reward_second_id_text_like)){
            this.getSearchCond().like("reward_second_id_text", n_reward_second_id_text_like);
        }
    }
	private String n_reward_third_id_text_eq;//[第三位用户]
	public void setN_reward_third_id_text_eq(String n_reward_third_id_text_eq) {
        this.n_reward_third_id_text_eq = n_reward_third_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_reward_third_id_text_eq)){
            this.getSearchCond().eq("reward_third_id_text", n_reward_third_id_text_eq);
        }
    }
	private String n_reward_third_id_text_like;//[第三位用户]
	public void setN_reward_third_id_text_like(String n_reward_third_id_text_like) {
        this.n_reward_third_id_text_like = n_reward_third_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_reward_third_id_text_like)){
            this.getSearchCond().like("reward_third_id_text", n_reward_third_id_text_like);
        }
    }
	private Long n_reward_first_id_eq;//[第一位用户]
	public void setN_reward_first_id_eq(Long n_reward_first_id_eq) {
        this.n_reward_first_id_eq = n_reward_first_id_eq;
        if(!ObjectUtils.isEmpty(this.n_reward_first_id_eq)){
            this.getSearchCond().eq("reward_first_id", n_reward_first_id_eq);
        }
    }
	private Long n_report_template_id_eq;//[报告模板]
	public void setN_report_template_id_eq(Long n_report_template_id_eq) {
        this.n_report_template_id_eq = n_report_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_report_template_id_eq)){
            this.getSearchCond().eq("report_template_id", n_report_template_id_eq);
        }
    }
	private Long n_reward_second_id_eq;//[第二位用户]
	public void setN_reward_second_id_eq(Long n_reward_second_id_eq) {
        this.n_reward_second_id_eq = n_reward_second_id_eq;
        if(!ObjectUtils.isEmpty(this.n_reward_second_id_eq)){
            this.getSearchCond().eq("reward_second_id", n_reward_second_id_eq);
        }
    }
	private Long n_reward_third_id_eq;//[第三位用户]
	public void setN_reward_third_id_eq(Long n_reward_third_id_eq) {
        this.n_reward_third_id_eq = n_reward_third_id_eq;
        if(!ObjectUtils.isEmpty(this.n_reward_third_id_eq)){
            this.getSearchCond().eq("reward_third_id", n_reward_third_id_eq);
        }
    }
	private Long n_report_message_group_id_eq;//[抄送]
	public void setN_report_message_group_id_eq(Long n_report_message_group_id_eq) {
        this.n_report_message_group_id_eq = n_report_message_group_id_eq;
        if(!ObjectUtils.isEmpty(this.n_report_message_group_id_eq)){
            this.getSearchCond().eq("report_message_group_id", n_report_message_group_id_eq);
        }
    }
	private Long n_reward_id_eq;//[每位获得成功的用户]
	public void setN_reward_id_eq(Long n_reward_id_eq) {
        this.n_reward_id_eq = n_reward_id_eq;
        if(!ObjectUtils.isEmpty(this.n_reward_id_eq)){
            this.getSearchCond().eq("reward_id", n_reward_id_eq);
        }
    }
	private Long n_manager_id_eq;//[负责人]
	public void setN_manager_id_eq(Long n_manager_id_eq) {
        this.n_manager_id_eq = n_manager_id_eq;
        if(!ObjectUtils.isEmpty(this.n_manager_id_eq)){
            this.getSearchCond().eq("manager_id", n_manager_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




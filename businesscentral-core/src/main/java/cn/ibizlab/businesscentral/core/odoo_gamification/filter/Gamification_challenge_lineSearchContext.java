package cn.ibizlab.businesscentral.core.odoo_gamification.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge_line;
/**
 * 关系型数据实体[Gamification_challenge_line] 查询条件对象
 */
@Slf4j
@Data
public class Gamification_challenge_lineSearchContext extends QueryWrapperContext<Gamification_challenge_line> {

	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_challenge_id_text_eq;//[挑战]
	public void setN_challenge_id_text_eq(String n_challenge_id_text_eq) {
        this.n_challenge_id_text_eq = n_challenge_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_challenge_id_text_eq)){
            this.getSearchCond().eq("challenge_id_text", n_challenge_id_text_eq);
        }
    }
	private String n_challenge_id_text_like;//[挑战]
	public void setN_challenge_id_text_like(String n_challenge_id_text_like) {
        this.n_challenge_id_text_like = n_challenge_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_challenge_id_text_like)){
            this.getSearchCond().like("challenge_id_text", n_challenge_id_text_like);
        }
    }
	private String n_name_eq;//[名称]
	public void setN_name_eq(String n_name_eq) {
        this.n_name_eq = n_name_eq;
        if(!ObjectUtils.isEmpty(this.n_name_eq)){
            this.getSearchCond().eq("name", n_name_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_challenge_id_eq;//[挑战]
	public void setN_challenge_id_eq(Long n_challenge_id_eq) {
        this.n_challenge_id_eq = n_challenge_id_eq;
        if(!ObjectUtils.isEmpty(this.n_challenge_id_eq)){
            this.getSearchCond().eq("challenge_id", n_challenge_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_definition_id_eq;//[目标定义]
	public void setN_definition_id_eq(Long n_definition_id_eq) {
        this.n_definition_id_eq = n_definition_id_eq;
        if(!ObjectUtils.isEmpty(this.n_definition_id_eq)){
            this.getSearchCond().eq("definition_id", n_definition_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




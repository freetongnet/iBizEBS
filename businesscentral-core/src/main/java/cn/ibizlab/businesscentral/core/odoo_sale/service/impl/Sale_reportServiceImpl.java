package cn.ibizlab.businesscentral.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_reportSearchContext;
import cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_sale.mapper.Sale_reportMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[销售分析报告] 服务对象接口实现
 */
@Slf4j
@Service("Sale_reportServiceImpl")
public class Sale_reportServiceImpl extends EBSServiceImpl<Sale_reportMapper, Sale_report> implements ISale_reportService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService crmTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_categoryService productCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService productPricelistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_templateService productTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService resCountryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_campaignService utmCampaignService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_mediumService utmMediumService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_utm.service.IUtm_sourceService utmSourceService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "sale.report" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Sale_report et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_reportService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Sale_report> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Sale_report et) {
        Sale_report old = new Sale_report() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_reportService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISale_reportService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Sale_report> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Sale_report get(Long key) {
        Sale_report et = getById(key);
        if(et==null){
            et=new Sale_report();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Sale_report getDraft(Sale_report et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Sale_report et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Sale_report et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Sale_report et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Sale_report> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Sale_report> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Sale_report> selectByAnalyticAccountId(Long id) {
        return baseMapper.selectByAnalyticAccountId(id);
    }
    @Override
    public void resetByAnalyticAccountId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("analytic_account_id",null).eq("analytic_account_id",id));
    }

    @Override
    public void resetByAnalyticAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("analytic_account_id",null).in("analytic_account_id",ids));
    }

    @Override
    public void removeByAnalyticAccountId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("analytic_account_id",id));
    }

	@Override
    public List<Sale_report> selectByTeamId(Long id) {
        return baseMapper.selectByTeamId(id);
    }
    @Override
    public void resetByTeamId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("team_id",null).eq("team_id",id));
    }

    @Override
    public void resetByTeamId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("team_id",null).in("team_id",ids));
    }

    @Override
    public void removeByTeamId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("team_id",id));
    }

	@Override
    public List<Sale_report> selectByCategId(Long id) {
        return baseMapper.selectByCategId(id);
    }
    @Override
    public void resetByCategId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("categ_id",null).eq("categ_id",id));
    }

    @Override
    public void resetByCategId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("categ_id",null).in("categ_id",ids));
    }

    @Override
    public void removeByCategId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("categ_id",id));
    }

	@Override
    public List<Sale_report> selectByPricelistId(Long id) {
        return baseMapper.selectByPricelistId(id);
    }
    @Override
    public void resetByPricelistId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("pricelist_id",null).eq("pricelist_id",id));
    }

    @Override
    public void resetByPricelistId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("pricelist_id",null).in("pricelist_id",ids));
    }

    @Override
    public void removeByPricelistId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("pricelist_id",id));
    }

	@Override
    public List<Sale_report> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("product_id",id));
    }

	@Override
    public List<Sale_report> selectByProductTmplId(Long id) {
        return baseMapper.selectByProductTmplId(id);
    }
    @Override
    public void resetByProductTmplId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("product_tmpl_id",null).eq("product_tmpl_id",id));
    }

    @Override
    public void resetByProductTmplId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("product_tmpl_id",null).in("product_tmpl_id",ids));
    }

    @Override
    public void removeByProductTmplId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("product_tmpl_id",id));
    }

	@Override
    public List<Sale_report> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("company_id",id));
    }

	@Override
    public List<Sale_report> selectByCountryId(Long id) {
        return baseMapper.selectByCountryId(id);
    }
    @Override
    public void resetByCountryId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("country_id",null).eq("country_id",id));
    }

    @Override
    public void resetByCountryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("country_id",null).in("country_id",ids));
    }

    @Override
    public void removeByCountryId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("country_id",id));
    }

	@Override
    public List<Sale_report> selectByCommercialPartnerId(Long id) {
        return baseMapper.selectByCommercialPartnerId(id);
    }
    @Override
    public void resetByCommercialPartnerId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("commercial_partner_id",null).eq("commercial_partner_id",id));
    }

    @Override
    public void resetByCommercialPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("commercial_partner_id",null).in("commercial_partner_id",ids));
    }

    @Override
    public void removeByCommercialPartnerId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("commercial_partner_id",id));
    }

	@Override
    public List<Sale_report> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("partner_id",id));
    }

	@Override
    public List<Sale_report> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("user_id",id));
    }

	@Override
    public List<Sale_report> selectByOrderId(Long id) {
        return baseMapper.selectByOrderId(id);
    }
    @Override
    public void resetByOrderId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("order_id",null).eq("order_id",id));
    }

    @Override
    public void resetByOrderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("order_id",null).in("order_id",ids));
    }

    @Override
    public void removeByOrderId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("order_id",id));
    }

	@Override
    public List<Sale_report> selectByWarehouseId(Long id) {
        return baseMapper.selectByWarehouseId(id);
    }
    @Override
    public void resetByWarehouseId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("warehouse_id",null).eq("warehouse_id",id));
    }

    @Override
    public void resetByWarehouseId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("warehouse_id",null).in("warehouse_id",ids));
    }

    @Override
    public void removeByWarehouseId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("warehouse_id",id));
    }

	@Override
    public List<Sale_report> selectByProductUom(Long id) {
        return baseMapper.selectByProductUom(id);
    }
    @Override
    public void resetByProductUom(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("product_uom",null).eq("product_uom",id));
    }

    @Override
    public void resetByProductUom(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("product_uom",null).in("product_uom",ids));
    }

    @Override
    public void removeByProductUom(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("product_uom",id));
    }

	@Override
    public List<Sale_report> selectByCampaignId(Long id) {
        return baseMapper.selectByCampaignId(id);
    }
    @Override
    public void resetByCampaignId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("campaign_id",null).eq("campaign_id",id));
    }

    @Override
    public void resetByCampaignId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("campaign_id",null).in("campaign_id",ids));
    }

    @Override
    public void removeByCampaignId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("campaign_id",id));
    }

	@Override
    public List<Sale_report> selectByMediumId(Long id) {
        return baseMapper.selectByMediumId(id);
    }
    @Override
    public void resetByMediumId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("medium_id",null).eq("medium_id",id));
    }

    @Override
    public void resetByMediumId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("medium_id",null).in("medium_id",ids));
    }

    @Override
    public void removeByMediumId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("medium_id",id));
    }

	@Override
    public List<Sale_report> selectBySourceId(Long id) {
        return baseMapper.selectBySourceId(id);
    }
    @Override
    public void resetBySourceId(Long id) {
        this.update(new UpdateWrapper<Sale_report>().set("source_id",null).eq("source_id",id));
    }

    @Override
    public void resetBySourceId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Sale_report>().set("source_id",null).in("source_id",ids));
    }

    @Override
    public void removeBySourceId(Long id) {
        this.remove(new QueryWrapper<Sale_report>().eq("source_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Sale_report> searchDefault(Sale_reportSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Sale_report> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Sale_report>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Sale_report et){
        //实体关系[DER1N_SALE_REPORT__ACCOUNT_ANALYTIC_ACCOUNT__ANALYTIC_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAnalyticAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount=et.getOdooAnalyticAccount();
            if(ObjectUtils.isEmpty(odooAnalyticAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAnalyticAccountId());
                et.setOdooAnalyticAccount(majorEntity);
                odooAnalyticAccount=majorEntity;
            }
            et.setAnalyticAccountIdText(odooAnalyticAccount.getName());
        }
        //实体关系[DER1N_SALE_REPORT__CRM_TEAM__TEAM_ID]
        if(!ObjectUtils.isEmpty(et.getTeamId())){
            cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam=et.getOdooTeam();
            if(ObjectUtils.isEmpty(odooTeam)){
                cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team majorEntity=crmTeamService.get(et.getTeamId());
                et.setOdooTeam(majorEntity);
                odooTeam=majorEntity;
            }
            et.setTeamIdText(odooTeam.getName());
        }
        //实体关系[DER1N_SALE_REPORT__PRODUCT_CATEGORY__CATEG_ID]
        if(!ObjectUtils.isEmpty(et.getCategId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category odooCateg=et.getOdooCateg();
            if(ObjectUtils.isEmpty(odooCateg)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_category majorEntity=productCategoryService.get(et.getCategId());
                et.setOdooCateg(majorEntity);
                odooCateg=majorEntity;
            }
            et.setCategIdText(odooCateg.getName());
        }
        //实体关系[DER1N_SALE_REPORT__PRODUCT_PRICELIST__PRICELIST_ID]
        if(!ObjectUtils.isEmpty(et.getPricelistId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist=et.getOdooPricelist();
            if(ObjectUtils.isEmpty(odooPricelist)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist majorEntity=productPricelistService.get(et.getPricelistId());
                et.setOdooPricelist(majorEntity);
                odooPricelist=majorEntity;
            }
            et.setPricelistIdText(odooPricelist.getName());
        }
        //实体关系[DER1N_SALE_REPORT__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_SALE_REPORT__PRODUCT_TEMPLATE__PRODUCT_TMPL_ID]
        if(!ObjectUtils.isEmpty(et.getProductTmplId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template odooProductTmpl=et.getOdooProductTmpl();
            if(ObjectUtils.isEmpty(odooProductTmpl)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template majorEntity=productTemplateService.get(et.getProductTmplId());
                et.setOdooProductTmpl(majorEntity);
                odooProductTmpl=majorEntity;
            }
            et.setProductTmplIdText(odooProductTmpl.getName());
        }
        //实体关系[DER1N_SALE_REPORT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_SALE_REPORT__RES_COUNTRY__COUNTRY_ID]
        if(!ObjectUtils.isEmpty(et.getCountryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry=et.getOdooCountry();
            if(ObjectUtils.isEmpty(odooCountry)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country majorEntity=resCountryService.get(et.getCountryId());
                et.setOdooCountry(majorEntity);
                odooCountry=majorEntity;
            }
            et.setCountryIdText(odooCountry.getName());
        }
        //实体关系[DER1N_SALE_REPORT__RES_PARTNER__COMMERCIAL_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getCommercialPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooCommercialPartner=et.getOdooCommercialPartner();
            if(ObjectUtils.isEmpty(odooCommercialPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getCommercialPartnerId());
                et.setOdooCommercialPartner(majorEntity);
                odooCommercialPartner=majorEntity;
            }
            et.setCommercialPartnerIdText(odooCommercialPartner.getName());
        }
        //实体关系[DER1N_SALE_REPORT__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_SALE_REPORT__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_SALE_REPORT__SALE_ORDER__ORDER_ID]
        if(!ObjectUtils.isEmpty(et.getOrderId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooOrder=et.getOdooOrder();
            if(ObjectUtils.isEmpty(odooOrder)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order majorEntity=saleOrderService.get(et.getOrderId());
                et.setOdooOrder(majorEntity);
                odooOrder=majorEntity;
            }
            et.setOrderIdText(odooOrder.getName());
        }
        //实体关系[DER1N_SALE_REPORT__STOCK_WAREHOUSE__WAREHOUSE_ID]
        if(!ObjectUtils.isEmpty(et.getWarehouseId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooWarehouse=et.getOdooWarehouse();
            if(ObjectUtils.isEmpty(odooWarehouse)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse majorEntity=stockWarehouseService.get(et.getWarehouseId());
                et.setOdooWarehouse(majorEntity);
                odooWarehouse=majorEntity;
            }
            et.setWarehouseIdText(odooWarehouse.getName());
        }
        //实体关系[DER1N_SALE_REPORT__UOM_UOM__PRODUCT_UOM]
        if(!ObjectUtils.isEmpty(et.getProductUom())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUom());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomText(odooProductUom.getName());
        }
        //实体关系[DER1N_SALE_REPORT__UTM_CAMPAIGN__CAMPAIGN_ID]
        if(!ObjectUtils.isEmpty(et.getCampaignId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign=et.getOdooCampaign();
            if(ObjectUtils.isEmpty(odooCampaign)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign majorEntity=utmCampaignService.get(et.getCampaignId());
                et.setOdooCampaign(majorEntity);
                odooCampaign=majorEntity;
            }
            et.setCampaignIdText(odooCampaign.getName());
        }
        //实体关系[DER1N_SALE_REPORT__UTM_MEDIUM__MEDIUM_ID]
        if(!ObjectUtils.isEmpty(et.getMediumId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium=et.getOdooMedium();
            if(ObjectUtils.isEmpty(odooMedium)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium majorEntity=utmMediumService.get(et.getMediumId());
                et.setOdooMedium(majorEntity);
                odooMedium=majorEntity;
            }
            et.setMediumIdText(odooMedium.getName());
        }
        //实体关系[DER1N_SALE_REPORT__UTM_SOURCE__SOURCE_ID]
        if(!ObjectUtils.isEmpty(et.getSourceId())){
            cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource=et.getOdooSource();
            if(ObjectUtils.isEmpty(odooSource)){
                cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source majorEntity=utmSourceService.get(et.getSourceId());
                et.setOdooSource(majorEntity);
                odooSource=majorEntity;
            }
            et.setSourceIdText(odooSource.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}




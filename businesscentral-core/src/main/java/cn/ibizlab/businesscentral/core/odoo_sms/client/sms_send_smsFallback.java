package cn.ibizlab.businesscentral.core.odoo_sms.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.businesscentral.core.odoo_sms.filter.Sms_send_smsSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[sms_send_sms] 服务对象接口
 */
@Component
public class sms_send_smsFallback implements sms_send_smsFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Sms_send_sms get(Long id){
            return null;
     }


    public Sms_send_sms create(Sms_send_sms sms_send_sms){
            return null;
     }
    public Boolean createBatch(List<Sms_send_sms> sms_send_sms){
            return false;
     }

    public Page<Sms_send_sms> search(Sms_send_smsSearchContext context){
            return null;
     }



    public Sms_send_sms update(Long id, Sms_send_sms sms_send_sms){
            return null;
     }
    public Boolean updateBatch(List<Sms_send_sms> sms_send_sms){
            return false;
     }



    public Page<Sms_send_sms> select(){
            return null;
     }

    public Sms_send_sms getDraft(){
            return null;
    }



    public Boolean checkKey(Sms_send_sms sms_send_sms){
            return false;
     }


    public Boolean save(Sms_send_sms sms_send_sms){
            return false;
     }
    public Boolean saveBatch(List<Sms_send_sms> sms_send_sms){
            return false;
     }

    public Page<Sms_send_sms> searchDefault(Sms_send_smsSearchContext context){
            return null;
     }


}

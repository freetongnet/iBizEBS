package cn.ibizlab.businesscentral.core.odoo_purchase.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Purchase_orderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[purchase_order] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-purchase:odoo-purchase}", contextId = "purchase-order", fallback = purchase_orderFallback.class)
public interface purchase_orderFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/purchase_orders/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/search")
    Page<Purchase_order> search(@RequestBody Purchase_orderSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/{id}")
    Purchase_order update(@PathVariable("id") Long id,@RequestBody Purchase_order purchase_order);

    @RequestMapping(method = RequestMethod.PUT, value = "/purchase_orders/batch")
    Boolean updateBatch(@RequestBody List<Purchase_order> purchase_orders);





    @RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/{id}")
    Purchase_order get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders")
    Purchase_order create(@RequestBody Purchase_order purchase_order);

    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/batch")
    Boolean createBatch(@RequestBody List<Purchase_order> purchase_orders);


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/select")
    Page<Purchase_order> select();


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/getdraft")
    Purchase_order getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{id}/button_approve")
    Purchase_order button_approve(@PathVariable("id") Long id,@RequestBody Purchase_order purchase_order);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{id}/button_cancel")
    Purchase_order button_cancel(@PathVariable("id") Long id,@RequestBody Purchase_order purchase_order);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{id}/button_confirm")
    Purchase_order button_confirm(@PathVariable("id") Long id,@RequestBody Purchase_order purchase_order);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{id}/button_done")
    Purchase_order button_done(@PathVariable("id") Long id,@RequestBody Purchase_order purchase_order);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{id}/button_unlock")
    Purchase_order button_unlock(@PathVariable("id") Long id,@RequestBody Purchase_order purchase_order);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/{id}/calc_amount")
    Purchase_order calc_amount(@PathVariable("id") Long id,@RequestBody Purchase_order purchase_order);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/checkkey")
    Boolean checkKey(@RequestBody Purchase_order purchase_order);


    @RequestMapping(method = RequestMethod.GET, value = "/purchase_orders/{id}/get_name")
    Purchase_order get_name(@PathVariable("id") Long id,@RequestBody Purchase_order purchase_order);


    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/save")
    Boolean save(@RequestBody Purchase_order purchase_order);

    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/savebatch")
    Boolean saveBatch(@RequestBody List<Purchase_order> purchase_orders);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/searchdefault")
    Page<Purchase_order> searchDefault(@RequestBody Purchase_orderSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/searchmaster")
    Page<Purchase_order> searchMaster(@RequestBody Purchase_orderSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/purchase_orders/searchorder")
    Page<Purchase_order> searchOrder(@RequestBody Purchase_orderSearchContext context);


}

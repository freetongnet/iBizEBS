package cn.ibizlab.businesscentral.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_teamSearchContext;
import cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_crm.mapper.Crm_teamMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[销售渠道] 服务对象接口实现
 */
@Slf4j
@Service("Crm_teamServiceImpl")
public class Crm_teamServiceImpl extends EBSServiceImpl<Crm_teamMapper, Crm_team> implements ICrm_teamService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_reportService accountInvoiceReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_activity_reportService crmActivityReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead2opportunity_partner_massService crmLead2opportunityPartnerMassService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead2opportunity_partnerService crmLead2opportunityPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_leadService crmLeadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_merge_opportunityService crmMergeOpportunityService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_stageService crmStageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService saleReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_aliasService mailAliasService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "crm.team" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Crm_team et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ICrm_teamService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Crm_team> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Crm_team et) {
        Crm_team old = new Crm_team() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ICrm_teamService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ICrm_teamService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Crm_team> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountInvoiceService.resetByTeamId(key);
        crmLead2opportunityPartnerMassService.resetByTeamId(key);
        crmLead2opportunityPartnerService.resetByTeamId(key);
        crmLeadService.resetByTeamId(key);
        crmMergeOpportunityService.resetByTeamId(key);
        crmStageService.resetByTeamId(key);
        resPartnerService.resetByTeamId(key);
        resUsersService.resetBySaleTeamId(key);
        saleOrderService.resetByTeamId(key);
        saleReportService.resetByTeamId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountInvoiceService.resetByTeamId(idList);
        crmLead2opportunityPartnerMassService.resetByTeamId(idList);
        crmLead2opportunityPartnerService.resetByTeamId(idList);
        crmLeadService.resetByTeamId(idList);
        crmMergeOpportunityService.resetByTeamId(idList);
        crmStageService.resetByTeamId(idList);
        resPartnerService.resetByTeamId(idList);
        resUsersService.resetBySaleTeamId(idList);
        saleOrderService.resetByTeamId(idList);
        saleReportService.resetByTeamId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Crm_team get(Long key) {
        Crm_team et = getById(key);
        if(et==null){
            et=new Crm_team();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Crm_team getDraft(Crm_team et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Crm_team et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Crm_team et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Crm_team et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Crm_team> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Crm_team> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Crm_team> selectByAliasId(Long id) {
        return baseMapper.selectByAliasId(id);
    }
    @Override
    public List<Crm_team> selectByAliasId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Crm_team>().in("id",ids));
    }

    @Override
    public void removeByAliasId(Long id) {
        this.remove(new QueryWrapper<Crm_team>().eq("alias_id",id));
    }

	@Override
    public List<Crm_team> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Crm_team>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Crm_team>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Crm_team>().eq("company_id",id));
    }

	@Override
    public List<Crm_team> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Crm_team>().eq("create_uid",id));
    }

	@Override
    public List<Crm_team> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Crm_team>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Crm_team>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Crm_team>().eq("user_id",id));
    }

	@Override
    public List<Crm_team> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Crm_team>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Crm_team> searchDefault(Crm_teamSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Crm_team> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Crm_team>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Crm_team et){
        //实体关系[DER1N_CRM_TEAM__MAIL_ALIAS__ALIAS_ID]
        if(!ObjectUtils.isEmpty(et.getAliasId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias odooAlias=et.getOdooAlias();
            if(ObjectUtils.isEmpty(odooAlias)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias majorEntity=mailAliasService.get(et.getAliasId());
                et.setOdooAlias(majorEntity);
                odooAlias=majorEntity;
            }
            et.setAliasDomain(odooAlias.getAliasDomain());
            et.setAliasModelId(odooAlias.getAliasModelId());
            et.setAliasParentModelId(odooAlias.getAliasParentModelId());
            et.setAliasContact(odooAlias.getAliasContact());
            et.setAliasUserId(odooAlias.getAliasUserId());
            et.setAliasDefaults(odooAlias.getAliasDefaults());
            et.setAliasParentThreadId(odooAlias.getAliasParentThreadId());
            et.setAliasForceThreadId(odooAlias.getAliasForceThreadId());
            et.setAliasName(odooAlias.getAliasName());
        }
        //实体关系[DER1N_CRM_TEAM__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
            et.setCurrencyId(odooCompany.getCurrencyId());
        }
        //实体关系[DER1N_CRM_TEAM__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_CRM_TEAM__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_CRM_TEAM__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Crm_team> getCrmTeamByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Crm_team> getCrmTeamByEntities(List<Crm_team> entities) {
        List ids =new ArrayList();
        for(Crm_team entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




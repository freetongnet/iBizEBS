package cn.ibizlab.businesscentral.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_questionSearchContext;
import cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_questionService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_survey.mapper.Survey_questionMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[调查问题] 服务对象接口实现
 */
@Slf4j
@Service("Survey_questionServiceImpl")
public class Survey_questionServiceImpl extends EBSServiceImpl<Survey_questionMapper, Survey_question> implements ISurvey_questionService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_labelService surveyLabelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_input_lineService surveyUserInputLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_pageService surveyPageService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "survey.question" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Survey_question et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISurvey_questionService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Survey_question> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Survey_question et) {
        Survey_question old = new Survey_question() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISurvey_questionService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (ISurvey_questionService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Survey_question> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        surveyLabelService.removeByQuestionId(key);
        surveyLabelService.removeByQuestionId2(key);
        if(!ObjectUtils.isEmpty(surveyUserInputLineService.selectByQuestionId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Survey_user_input_line]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        surveyLabelService.removeByQuestionId(idList);
        surveyLabelService.removeByQuestionId2(idList);
        if(!ObjectUtils.isEmpty(surveyUserInputLineService.selectByQuestionId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Survey_user_input_line]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Survey_question get(Long key) {
        Survey_question et = getById(key);
        if(et==null){
            et=new Survey_question();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Survey_question getDraft(Survey_question et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Survey_question et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Survey_question et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Survey_question et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Survey_question> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Survey_question> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Survey_question> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Survey_question>().eq("create_uid",id));
    }

	@Override
    public List<Survey_question> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Survey_question>().eq("write_uid",id));
    }

	@Override
    public List<Survey_question> selectByPageId(Long id) {
        return baseMapper.selectByPageId(id);
    }
    @Override
    public void removeByPageId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Survey_question>().in("page_id",ids));
    }

    @Override
    public void removeByPageId(Long id) {
        this.remove(new QueryWrapper<Survey_question>().eq("page_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Survey_question> searchDefault(Survey_questionSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Survey_question> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Survey_question>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Survey_question et){
        //实体关系[DER1N_SURVEY_QUESTION__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_SURVEY_QUESTION__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_SURVEY_QUESTION__SURVEY_PAGE__PAGE_ID]
        if(!ObjectUtils.isEmpty(et.getPageId())){
            cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_page odooPage=et.getOdooPage();
            if(ObjectUtils.isEmpty(odooPage)){
                cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_page majorEntity=surveyPageService.get(et.getPageId());
                et.setOdooPage(majorEntity);
                odooPage=majorEntity;
            }
            et.setSurveyId(odooPage.getSurveyId());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Survey_question> getSurveyQuestionByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Survey_question> getSurveyQuestionByEntities(List<Survey_question> entities) {
        List ids =new ArrayList();
        for(Survey_question entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




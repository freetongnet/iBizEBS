package cn.ibizlab.businesscentral.core.odoo_website.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_page;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_pageSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Website_pageMapper extends BaseMapper<Website_page>{

    Page<Website_page> searchDefault(IPage page, @Param("srf") Website_pageSearchContext context, @Param("ew") Wrapper<Website_page> wrapper) ;
    @Override
    Website_page selectById(Serializable id);
    @Override
    int insert(Website_page entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Website_page entity);
    @Override
    int update(@Param(Constants.ENTITY) Website_page entity, @Param("ew") Wrapper<Website_page> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Website_page> selectByCreateUid(@Param("id") Serializable id) ;

    List<Website_page> selectByWriteUid(@Param("id") Serializable id) ;


}

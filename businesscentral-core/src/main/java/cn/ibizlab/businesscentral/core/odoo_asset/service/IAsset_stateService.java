package cn.ibizlab.businesscentral.core.odoo_asset.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_state;
import cn.ibizlab.businesscentral.core.odoo_asset.filter.Asset_stateSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Asset_state] 服务对象接口
 */
public interface IAsset_stateService extends IService<Asset_state>{

    boolean create(Asset_state et) ;
    void createBatch(List<Asset_state> list) ;
    boolean update(Asset_state et) ;
    void updateBatch(List<Asset_state> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Asset_state get(Long key) ;
    Asset_state getDraft(Asset_state et) ;
    boolean checkKey(Asset_state et) ;
    boolean save(Asset_state et) ;
    void saveBatch(List<Asset_state> list) ;
    Page<Asset_state> searchDefault(Asset_stateSearchContext context) ;
    List<Asset_state> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Asset_state> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Asset_state> getAssetStateByIds(List<Long> ids) ;
    List<Asset_state> getAssetStateByEntities(List<Asset_state> entities) ;
}



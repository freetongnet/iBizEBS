package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rule;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_ruleSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_rule] 服务对象接口
 */
@Component
public class stock_ruleFallback implements stock_ruleFeignClient{

    public Stock_rule update(Long id, Stock_rule stock_rule){
            return null;
     }
    public Boolean updateBatch(List<Stock_rule> stock_rules){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Stock_rule create(Stock_rule stock_rule){
            return null;
     }
    public Boolean createBatch(List<Stock_rule> stock_rules){
            return false;
     }

    public Page<Stock_rule> search(Stock_ruleSearchContext context){
            return null;
     }




    public Stock_rule get(Long id){
            return null;
     }


    public Page<Stock_rule> select(){
            return null;
     }

    public Stock_rule getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_rule stock_rule){
            return false;
     }


    public Boolean save(Stock_rule stock_rule){
            return false;
     }
    public Boolean saveBatch(List<Stock_rule> stock_rules){
            return false;
     }

    public Page<Stock_rule> searchDefault(Stock_ruleSearchContext context){
            return null;
     }


}

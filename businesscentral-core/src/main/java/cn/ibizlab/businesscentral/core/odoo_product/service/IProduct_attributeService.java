package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_attributeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_attribute] 服务对象接口
 */
public interface IProduct_attributeService extends IService<Product_attribute>{

    boolean create(Product_attribute et) ;
    void createBatch(List<Product_attribute> list) ;
    boolean update(Product_attribute et) ;
    void updateBatch(List<Product_attribute> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_attribute get(Long key) ;
    Product_attribute getDraft(Product_attribute et) ;
    boolean checkKey(Product_attribute et) ;
    boolean save(Product_attribute et) ;
    void saveBatch(List<Product_attribute> list) ;
    Page<Product_attribute> searchDefault(Product_attributeSearchContext context) ;
    List<Product_attribute> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_attribute> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_attribute> getProductAttributeByIds(List<Long> ids) ;
    List<Product_attribute> getProductAttributeByEntities(List<Product_attribute> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[配置设定]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RES_CONFIG_SETTINGS",resultMap = "Res_config_settingsResultMap")
public class Res_config_settings extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 导入.qif 文件
     */
    @DEField(name = "module_account_bank_statement_import_qif")
    @TableField(value = "module_account_bank_statement_import_qif")
    @JSONField(name = "module_account_bank_statement_import_qif")
    @JsonProperty("module_account_bank_statement_import_qif")
    private Boolean moduleAccountBankStatementImportQif;
    /**
     * 导入.ofx格式
     */
    @DEField(name = "module_account_bank_statement_import_ofx")
    @TableField(value = "module_account_bank_statement_import_ofx")
    @JSONField(name = "module_account_bank_statement_import_ofx")
    @JsonProperty("module_account_bank_statement_import_ofx")
    private Boolean moduleAccountBankStatementImportOfx;
    /**
     * Google 地图
     */
    @TableField(exist = false)
    @JSONField(name = "has_google_maps")
    @JsonProperty("has_google_maps")
    private Boolean hasGoogleMaps;
    /**
     * 欧盟数字商品增值税
     */
    @DEField(name = "module_l10n_eu_service")
    @TableField(value = "module_l10n_eu_service")
    @JSONField(name = "module_l10n_eu_service")
    @JsonProperty("module_l10n_eu_service")
    private Boolean moduleL10nEuService;
    /**
     * 内容发布网络 (CDN)
     */
    @TableField(exist = false)
    @JSONField(name = "cdn_activated")
    @JsonProperty("cdn_activated")
    private Boolean cdnActivated;
    /**
     * 默认语言代码
     */
    @TableField(exist = false)
    @JSONField(name = "website_default_lang_code")
    @JsonProperty("website_default_lang_code")
    private String websiteDefaultLangCode;
    /**
     * 允许在登录页开启密码重置功能
     */
    @DEField(name = "auth_signup_reset_password")
    @TableField(value = "auth_signup_reset_password")
    @JSONField(name = "auth_signup_reset_password")
    @JsonProperty("auth_signup_reset_password")
    private Boolean authSignupResetPassword;
    /**
     * 国家/地区分组
     */
    @TableField(exist = false)
    @JSONField(name = "website_country_group_ids")
    @JsonProperty("website_country_group_ids")
    private String websiteCountryGroupIds;
    /**
     * Instagram 账号
     */
    @TableField(exist = false)
    @JSONField(name = "social_instagram")
    @JsonProperty("social_instagram")
    private String socialInstagram;
    /**
     * 采购招标
     */
    @DEField(name = "module_purchase_requisition")
    @TableField(value = "module_purchase_requisition")
    @JSONField(name = "module_purchase_requisition")
    @JsonProperty("module_purchase_requisition")
    private Boolean modulePurchaseRequisition;
    /**
     * Easypost 接口
     */
    @DEField(name = "module_delivery_easypost")
    @TableField(value = "module_delivery_easypost")
    @JSONField(name = "module_delivery_easypost")
    @JsonProperty("module_delivery_easypost")
    private Boolean moduleDeliveryEasypost;
    /**
     * 折扣
     */
    @DEField(name = "group_discount_per_so_line")
    @TableField(value = "group_discount_per_so_line")
    @JSONField(name = "group_discount_per_so_line")
    @JsonProperty("group_discount_per_so_line")
    private Boolean groupDiscountPerSoLine;
    /**
     * 自动开票
     */
    @DEField(name = "automatic_invoice")
    @TableField(value = "automatic_invoice")
    @JSONField(name = "automatic_invoice")
    @JsonProperty("automatic_invoice")
    private Boolean automaticInvoice;
    /**
     * Plaid 接口
     */
    @DEField(name = "module_account_plaid")
    @TableField(value = "module_account_plaid")
    @JSONField(name = "module_account_plaid")
    @JsonProperty("module_account_plaid")
    private Boolean moduleAccountPlaid;
    /**
     * Google 客户 ID
     */
    @TableField(exist = false)
    @JSONField(name = "google_management_client_id")
    @JsonProperty("google_management_client_id")
    private String googleManagementClientId;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 允许支票打印和存款
     */
    @DEField(name = "module_account_check_printing")
    @TableField(value = "module_account_check_printing")
    @JSONField(name = "module_account_check_printing")
    @JsonProperty("module_account_check_printing")
    private Boolean moduleAccountCheckPrinting;
    /**
     * 自动填充公司数据
     */
    @DEField(name = "module_partner_autocomplete")
    @TableField(value = "module_partner_autocomplete")
    @JSONField(name = "module_partner_autocomplete")
    @JsonProperty("module_partner_autocomplete")
    private Boolean modulePartnerAutocomplete;
    /**
     * 语言数量
     */
    @TableField(exist = false)
    @JSONField(name = "language_count")
    @JsonProperty("language_count")
    private Integer languageCount;
    /**
     * 链接跟踪器
     */
    @DEField(name = "module_website_links")
    @TableField(value = "module_website_links")
    @JSONField(name = "module_website_links")
    @JsonProperty("module_website_links")
    private Boolean moduleWebsiteLinks;
    /**
     * 联系表单上的技术数据
     */
    @TableField(exist = false)
    @JSONField(name = "website_form_enable_metadata")
    @JsonProperty("website_form_enable_metadata")
    private Boolean websiteFormEnableMetadata;
    /**
     * 设置社交平台
     */
    @TableField(exist = false)
    @JSONField(name = "has_social_network")
    @JsonProperty("has_social_network")
    private Boolean hasSocialNetwork;
    /**
     * 客户地址
     */
    @DEField(name = "group_sale_delivery_address")
    @TableField(value = "group_sale_delivery_address")
    @JSONField(name = "group_sale_delivery_address")
    @JsonProperty("group_sale_delivery_address")
    private Boolean groupSaleDeliveryAddress;
    /**
     * 谷歌分析密钥
     */
    @TableField(exist = false)
    @JSONField(name = "google_analytics_key")
    @JsonProperty("google_analytics_key")
    private String googleAnalyticsKey;
    /**
     * LDAP认证
     */
    @DEField(name = "module_auth_ldap")
    @TableField(value = "module_auth_ldap")
    @JSONField(name = "module_auth_ldap")
    @JsonProperty("module_auth_ldap")
    private Boolean moduleAuthLdap;
    /**
     * 具体用户账号
     */
    @TableField(exist = false)
    @JSONField(name = "specific_user_account")
    @JsonProperty("specific_user_account")
    private Boolean specificUserAccount;
    /**
     * 在线发布
     */
    @DEField(name = "module_website_hr_recruitment")
    @TableField(value = "module_website_hr_recruitment")
    @JSONField(name = "module_website_hr_recruitment")
    @JsonProperty("module_website_hr_recruitment")
    private Boolean moduleWebsiteHrRecruitment;
    /**
     * 预测
     */
    @DEField(name = "module_project_forecast")
    @TableField(value = "module_project_forecast")
    @JSONField(name = "module_project_forecast")
    @JsonProperty("module_project_forecast")
    private Boolean moduleProjectForecast;
    /**
     * 寄售
     */
    @DEField(name = "group_stock_tracking_owner")
    @TableField(value = "group_stock_tracking_owner")
    @JSONField(name = "group_stock_tracking_owner")
    @JsonProperty("group_stock_tracking_owner")
    private Boolean groupStockTrackingOwner;
    /**
     * 允许用户同步Google日历
     */
    @DEField(name = "module_google_calendar")
    @TableField(value = "module_google_calendar")
    @JSONField(name = "module_google_calendar")
    @JsonProperty("module_google_calendar")
    private Boolean moduleGoogleCalendar;
    /**
     * 开票
     */
    @DEField(name = "module_account")
    @TableField(value = "module_account")
    @JSONField(name = "module_account")
    @JsonProperty("module_account")
    private Boolean moduleAccount;
    /**
     * 附加Google文档到记录
     */
    @DEField(name = "module_google_drive")
    @TableField(value = "module_google_drive")
    @JSONField(name = "module_google_drive")
    @JsonProperty("module_google_drive")
    private Boolean moduleGoogleDrive;
    /**
     * 自动汇率
     */
    @DEField(name = "module_currency_rate_live")
    @TableField(value = "module_currency_rate_live")
    @JSONField(name = "module_currency_rate_live")
    @JsonProperty("module_currency_rate_live")
    private Boolean moduleCurrencyRateLive;
    /**
     * 形式发票
     */
    @DEField(name = "group_proforma_sales")
    @TableField(value = "group_proforma_sales")
    @JSONField(name = "group_proforma_sales")
    @JsonProperty("group_proforma_sales")
    private Boolean groupProformaSales;
    /**
     * FedEx 接口
     */
    @DEField(name = "module_delivery_fedex")
    @TableField(value = "module_delivery_fedex")
    @JSONField(name = "module_delivery_fedex")
    @JsonProperty("module_delivery_fedex")
    private Boolean moduleDeliveryFedex;
    /**
     * 特定的EMail
     */
    @DEField(name = "module_product_email_template")
    @TableField(value = "module_product_email_template")
    @JSONField(name = "module_product_email_template")
    @JsonProperty("module_product_email_template")
    private Boolean moduleProductEmailTemplate;
    /**
     * 显示效果
     */
    @DEField(name = "show_effect")
    @TableField(value = "show_effect")
    @JSONField(name = "show_effect")
    @JsonProperty("show_effect")
    private Boolean showEffect;
    /**
     * 拣货策略
     */
    @DEField(name = "default_picking_policy")
    @TableField(value = "default_picking_policy")
    @JSONField(name = "default_picking_policy")
    @JsonProperty("default_picking_policy")
    private String defaultPickingPolicy;
    /**
     * Youtube账号
     */
    @TableField(exist = false)
    @JSONField(name = "social_youtube")
    @JsonProperty("social_youtube")
    private String socialYoutube;
    /**
     * 网站公司
     */
    @TableField(exist = false)
    @JSONField(name = "website_company_id")
    @JsonProperty("website_company_id")
    private Integer websiteCompanyId;
    /**
     * USPS 接口
     */
    @DEField(name = "module_delivery_usps")
    @TableField(value = "module_delivery_usps")
    @JSONField(name = "module_delivery_usps")
    @JsonProperty("module_delivery_usps")
    private Boolean moduleDeliveryUsps;
    /**
     * DHL 接口
     */
    @DEField(name = "module_delivery_dhl")
    @TableField(value = "module_delivery_dhl")
    @JSONField(name = "module_delivery_dhl")
    @JsonProperty("module_delivery_dhl")
    private Boolean moduleDeliveryDhl;
    /**
     * 使用项目评级
     */
    @DEField(name = "group_project_rating")
    @TableField(value = "group_project_rating")
    @JSONField(name = "group_project_rating")
    @JsonProperty("group_project_rating")
    private Boolean groupProjectRating;
    /**
     * Google 地图 API 密钥
     */
    @TableField(exist = false)
    @JSONField(name = "google_maps_api_key")
    @JsonProperty("google_maps_api_key")
    private String googleMapsApiKey;
    /**
     * 线索
     */
    @DEField(name = "group_use_lead")
    @TableField(value = "group_use_lead")
    @JSONField(name = "group_use_lead")
    @JsonProperty("group_use_lead")
    private Boolean groupUseLead;
    /**
     * 交货包裹
     */
    @DEField(name = "group_stock_tracking_lot")
    @TableField(value = "group_stock_tracking_lot")
    @JSONField(name = "group_stock_tracking_lot")
    @JsonProperty("group_stock_tracking_lot")
    private Boolean groupStockTrackingLot;
    /**
     * 多步路由
     */
    @DEField(name = "group_stock_adv_location")
    @TableField(value = "group_stock_adv_location")
    @JSONField(name = "group_stock_adv_location")
    @JsonProperty("group_stock_adv_location")
    private Boolean groupStockAdvLocation;
    /**
     * 公司有科目表
     */
    @TableField(exist = false)
    @JSONField(name = "has_chart_of_accounts")
    @JsonProperty("has_chart_of_accounts")
    private Boolean hasChartOfAccounts;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 保留
     */
    @DEField(name = "module_procurement_jit")
    @TableField(value = "module_procurement_jit")
    @JSONField(name = "module_procurement_jit")
    @JsonProperty("module_procurement_jit")
    private String moduleProcurementJit;
    /**
     * 允许员工通过EMail记录费用
     */
    @DEField(name = "use_mailgateway")
    @TableField(value = "use_mailgateway")
    @JSONField(name = "use_mailgateway")
    @JsonProperty("use_mailgateway")
    private Boolean useMailgateway;
    /**
     * 为每个客户使用价格表来适配您的价格
     */
    @DEField(name = "group_sale_pricelist")
    @TableField(value = "group_sale_pricelist")
    @JSONField(name = "group_sale_pricelist")
    @JsonProperty("group_sale_pricelist")
    private Boolean groupSalePricelist;
    /**
     * 默认销售团队
     */
    @TableField(exist = false)
    @JSONField(name = "crm_default_team_id")
    @JsonProperty("crm_default_team_id")
    private Integer crmDefaultTeamId;
    /**
     * 储存位置
     */
    @DEField(name = "group_stock_multi_locations")
    @TableField(value = "group_stock_multi_locations")
    @JSONField(name = "group_stock_multi_locations")
    @JsonProperty("group_stock_multi_locations")
    private Boolean groupStockMultiLocations;
    /**
     * 默认制造提前期
     */
    @DEField(name = "use_manufacturing_lead")
    @TableField(value = "use_manufacturing_lead")
    @JSONField(name = "use_manufacturing_lead")
    @JsonProperty("use_manufacturing_lead")
    private Boolean useManufacturingLead;
    /**
     * Google 电子表格
     */
    @DEField(name = "module_google_spreadsheet")
    @TableField(value = "module_google_spreadsheet")
    @JSONField(name = "module_google_spreadsheet")
    @JsonProperty("module_google_spreadsheet")
    private Boolean moduleGoogleSpreadsheet;
    /**
     * 税目汇总表
     */
    @DEField(name = "show_line_subtotals_tax_selection")
    @TableField(value = "show_line_subtotals_tax_selection")
    @JSONField(name = "show_line_subtotals_tax_selection")
    @JsonProperty("show_line_subtotals_tax_selection")
    private String showLineSubtotalsTaxSelection;
    /**
     * 语言
     */
    @TableField(exist = false)
    @JSONField(name = "language_ids")
    @JsonProperty("language_ids")
    private String languageIds;
    /**
     * 电商物流成本
     */
    @DEField(name = "module_website_sale_delivery")
    @TableField(value = "module_website_sale_delivery")
    @JSONField(name = "module_website_sale_delivery")
    @JsonProperty("module_website_sale_delivery")
    private Boolean moduleWebsiteSaleDelivery;
    /**
     * 主生产排程
     */
    @DEField(name = "module_mrp_mps")
    @TableField(value = "module_mrp_mps")
    @JSONField(name = "module_mrp_mps")
    @JsonProperty("module_mrp_mps")
    private Boolean moduleMrpMps;
    /**
     * 信用不足
     */
    @TableField(exist = false)
    @JSONField(name = "partner_autocomplete_insufficient_credit")
    @JsonProperty("partner_autocomplete_insufficient_credit")
    private Boolean partnerAutocompleteInsufficientCredit;
    /**
     * 显示组织架构图
     */
    @DEField(name = "module_hr_org_chart")
    @TableField(value = "module_hr_org_chart")
    @JSONField(name = "module_hr_org_chart")
    @JsonProperty("module_hr_org_chart")
    private Boolean moduleHrOrgChart;
    /**
     * 到期日
     */
    @DEField(name = "module_product_expiry")
    @TableField(value = "module_product_expiry")
    @JSONField(name = "module_product_expiry")
    @JsonProperty("module_product_expiry")
    private Boolean moduleProductExpiry;
    /**
     * bpost 接口
     */
    @DEField(name = "module_delivery_bpost")
    @TableField(value = "module_delivery_bpost")
    @JSONField(name = "module_delivery_bpost")
    @JsonProperty("module_delivery_bpost")
    private Boolean moduleDeliveryBpost;
    /**
     * 条码扫描器
     */
    @DEField(name = "module_stock_barcode")
    @TableField(value = "module_stock_barcode")
    @JSONField(name = "module_stock_barcode")
    @JsonProperty("module_stock_barcode")
    private Boolean moduleStockBarcode;
    /**
     * GitHub账户
     */
    @TableField(exist = false)
    @JSONField(name = "social_github")
    @JsonProperty("social_github")
    private String socialGithub;
    /**
     * 国际贸易统计组织
     */
    @DEField(name = "module_account_intrastat")
    @TableField(value = "module_account_intrastat")
    @JSONField(name = "module_account_intrastat")
    @JsonProperty("module_account_intrastat")
    private Boolean moduleAccountIntrastat;
    /**
     * 顾客账号
     */
    @TableField(exist = false)
    @JSONField(name = "auth_signup_uninvited")
    @JsonProperty("auth_signup_uninvited")
    private String authSignupUninvited;
    /**
     * 报价单模板
     */
    @DEField(name = "group_sale_order_template")
    @TableField(value = "group_sale_order_template")
    @JSONField(name = "group_sale_order_template")
    @JsonProperty("group_sale_order_template")
    private Boolean groupSaleOrderTemplate;
    /**
     * 使用SEPA直接计入借方
     */
    @DEField(name = "module_account_sepa_direct_debit")
    @TableField(value = "module_account_sepa_direct_debit")
    @JSONField(name = "module_account_sepa_direct_debit")
    @JsonProperty("module_account_sepa_direct_debit")
    private Boolean moduleAccountSepaDirectDebit;
    /**
     * 默认报价有效期
     */
    @DEField(name = "use_quotation_validity_days")
    @TableField(value = "use_quotation_validity_days")
    @JSONField(name = "use_quotation_validity_days")
    @JsonProperty("use_quotation_validity_days")
    private Boolean useQuotationValidityDays;
    /**
     * 使用批量付款
     */
    @DEField(name = "module_account_batch_payment")
    @TableField(value = "module_account_batch_payment")
    @JSONField(name = "module_account_batch_payment")
    @JsonProperty("module_account_batch_payment")
    private Boolean moduleAccountBatchPayment;
    /**
     * Twitter账号
     */
    @TableField(exist = false)
    @JSONField(name = "social_twitter")
    @JsonProperty("social_twitter")
    private String socialTwitter;
    /**
     * 预算管理
     */
    @DEField(name = "module_account_budget")
    @TableField(value = "module_account_budget")
    @JSONField(name = "module_account_budget")
    @JsonProperty("module_account_budget")
    private Boolean moduleAccountBudget;
    /**
     * MRP 工单
     */
    @DEField(name = "group_mrp_routings")
    @TableField(value = "group_mrp_routings")
    @JSONField(name = "group_mrp_routings")
    @JsonProperty("group_mrp_routings")
    private Boolean groupMrpRoutings;
    /**
     * 现金舍入
     */
    @DEField(name = "group_cash_rounding")
    @TableField(value = "group_cash_rounding")
    @JSONField(name = "group_cash_rounding")
    @JsonProperty("group_cash_rounding")
    private Boolean groupCashRounding;
    /**
     * 到岸成本
     */
    @DEField(name = "module_stock_landed_costs")
    @TableField(value = "module_stock_landed_costs")
    @JSONField(name = "module_stock_landed_costs")
    @JsonProperty("module_stock_landed_costs")
    private Boolean moduleStockLandedCosts;
    /**
     * 网站名称
     */
    @TableField(exist = false)
    @JSONField(name = "website_name")
    @JsonProperty("website_name")
    private String websiteName;
    /**
     * 库存
     */
    @DEField(name = "module_website_sale_stock")
    @TableField(value = "module_website_sale_stock")
    @JSONField(name = "module_website_sale_stock")
    @JsonProperty("module_website_sale_stock")
    private Boolean moduleWebsiteSaleStock;
    /**
     * 耿宗并计划
     */
    @DEField(name = "module_website_event_track")
    @TableField(value = "module_website_event_track")
    @JSONField(name = "module_website_event_track")
    @JsonProperty("module_website_event_track")
    private Boolean moduleWebsiteEventTrack;
    /**
     * 运输成本
     */
    @DEField(name = "module_delivery")
    @TableField(value = "module_delivery")
    @JSONField(name = "module_delivery")
    @JsonProperty("module_delivery")
    private Boolean moduleDelivery;
    /**
     * 自动票据处理
     */
    @DEField(name = "module_account_invoice_extract")
    @TableField(value = "module_account_invoice_extract")
    @JSONField(name = "module_account_invoice_extract")
    @JsonProperty("module_account_invoice_extract")
    private Boolean moduleAccountInvoiceExtract;
    /**
     * 网站直播频道
     */
    @TableField(exist = false)
    @JSONField(name = "channel_id")
    @JsonProperty("channel_id")
    private Integer channelId;
    /**
     * 销售订单警告
     */
    @DEField(name = "group_warning_sale")
    @TableField(value = "group_warning_sale")
    @JSONField(name = "group_warning_sale")
    @JsonProperty("group_warning_sale")
    private Boolean groupWarningSale;
    /**
     * CDN基本网址
     */
    @TableField(exist = false)
    @JSONField(name = "cdn_url")
    @JsonProperty("cdn_url")
    private String cdnUrl;
    /**
     * 条码
     */
    @DEField(name = "module_event_barcode")
    @TableField(value = "module_event_barcode")
    @JSONField(name = "module_event_barcode")
    @JsonProperty("module_event_barcode")
    private Boolean moduleEventBarcode;
    /**
     * 别名域
     */
    @DEField(name = "alias_domain")
    @TableField(value = "alias_domain")
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;
    /**
     * 领英账号
     */
    @TableField(exist = false)
    @JSONField(name = "social_linkedin")
    @JsonProperty("social_linkedin")
    private String socialLinkedin;
    /**
     * 多仓库
     */
    @DEField(name = "group_stock_multi_warehouses")
    @TableField(value = "group_stock_multi_warehouses")
    @JSONField(name = "group_stock_multi_warehouses")
    @JsonProperty("group_stock_multi_warehouses")
    private Boolean groupStockMultiWarehouses;
    /**
     * 销售员
     */
    @TableField(exist = false)
    @JSONField(name = "salesperson_id")
    @JsonProperty("salesperson_id")
    private Integer salespersonId;
    /**
     * 动态报告
     */
    @DEField(name = "module_account_reports")
    @TableField(value = "module_account_reports")
    @JSONField(name = "module_account_reports")
    @JsonProperty("module_account_reports")
    private Boolean moduleAccountReports;
    /**
     * 显示产品的价目表
     */
    @DEField(name = "group_product_pricelist")
    @TableField(value = "group_product_pricelist")
    @JSONField(name = "group_product_pricelist")
    @JsonProperty("group_product_pricelist")
    private Boolean groupProductPricelist;
    /**
     * 技能管理
     */
    @DEField(name = "module_hr_skills")
    @TableField(value = "module_hr_skills")
    @JSONField(name = "module_hr_skills")
    @JsonProperty("module_hr_skills")
    private Boolean moduleHrSkills;
    /**
     * A / B测试
     */
    @DEField(name = "module_website_version")
    @TableField(value = "module_website_version")
    @JSONField(name = "module_website_version")
    @JsonProperty("module_website_version")
    private Boolean moduleWebsiteVersion;
    /**
     * 允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据
     */
    @DEField(name = "module_base_import")
    @TableField(value = "module_base_import")
    @JSONField(name = "module_base_import")
    @JsonProperty("module_base_import")
    private Boolean moduleBaseImport;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 以 CSV 格式导入
     */
    @DEField(name = "module_account_bank_statement_import_csv")
    @TableField(value = "module_account_bank_statement_import_csv")
    @JSONField(name = "module_account_bank_statement_import_csv")
    @JsonProperty("module_account_bank_statement_import_csv")
    private Boolean moduleAccountBankStatementImportCsv;
    /**
     * 科目税
     */
    @DEField(name = "module_account_taxcloud")
    @TableField(value = "module_account_taxcloud")
    @JSONField(name = "module_account_taxcloud")
    @JsonProperty("module_account_taxcloud")
    private Boolean moduleAccountTaxcloud;
    /**
     * 放弃时长
     */
    @TableField(exist = false)
    @JSONField(name = "cart_abandoned_delay")
    @JsonProperty("cart_abandoned_delay")
    private Double cartAbandonedDelay;
    /**
     * 网站域名
     */
    @TableField(exist = false)
    @JSONField(name = "website_domain")
    @JsonProperty("website_domain")
    private String websiteDomain;
    /**
     * Accounting
     */
    @DEField(name = "module_account_accountant")
    @TableField(value = "module_account_accountant")
    @JSONField(name = "module_account_accountant")
    @JsonProperty("module_account_accountant")
    private Boolean moduleAccountAccountant;
    /**
     * 毛利
     */
    @DEField(name = "module_sale_margin")
    @TableField(value = "module_sale_margin")
    @JSONField(name = "module_sale_margin")
    @JsonProperty("module_sale_margin")
    private Boolean moduleSaleMargin;
    /**
     * 摘要邮件
     */
    @DEField(name = "digest_emails")
    @TableField(value = "digest_emails")
    @JSONField(name = "digest_emails")
    @JsonProperty("digest_emails")
    private Boolean digestEmails;
    /**
     * 协作pad
     */
    @DEField(name = "module_pad")
    @TableField(value = "module_pad")
    @JSONField(name = "module_pad")
    @JsonProperty("module_pad")
    private Boolean modulePad;
    /**
     * 发票警告
     */
    @DEField(name = "group_warning_account")
    @TableField(value = "group_warning_account")
    @JSONField(name = "group_warning_account")
    @JsonProperty("group_warning_account")
    private Boolean groupWarningAccount;
    /**
     * 贸易条款
     */
    @DEField(name = "group_display_incoterm")
    @TableField(value = "group_display_incoterm")
    @JSONField(name = "group_display_incoterm")
    @JsonProperty("group_display_incoterm")
    private Boolean groupDisplayIncoterm;
    /**
     * 心愿单
     */
    @DEField(name = "module_website_sale_wishlist")
    @TableField(value = "module_website_sale_wishlist")
    @JSONField(name = "module_website_sale_wishlist")
    @JsonProperty("module_website_sale_wishlist")
    private Boolean moduleWebsiteSaleWishlist;
    /**
     * 默认访问权限
     */
    @DEField(name = "user_default_rights")
    @TableField(value = "user_default_rights")
    @JSONField(name = "user_default_rights")
    @JsonProperty("user_default_rights")
    private Boolean userDefaultRights;
    /**
     * 账单控制
     */
    @DEField(name = "default_purchase_method")
    @TableField(value = "default_purchase_method")
    @JSONField(name = "default_purchase_method")
    @JsonProperty("default_purchase_method")
    private String defaultPurchaseMethod;
    /**
     * 送货地址
     */
    @DEField(name = "group_delivery_invoice_address")
    @TableField(value = "group_delivery_invoice_address")
    @JSONField(name = "group_delivery_invoice_address")
    @JsonProperty("group_delivery_invoice_address")
    private Boolean groupDeliveryInvoiceAddress;
    /**
     * 显示批次 / 序列号
     */
    @DEField(name = "group_lot_on_delivery_slip")
    @TableField(value = "group_lot_on_delivery_slip")
    @JSONField(name = "group_lot_on_delivery_slip")
    @JsonProperty("group_lot_on_delivery_slip")
    private Boolean groupLotOnDeliverySlip;
    /**
     * 入场券
     */
    @DEField(name = "module_event_sale")
    @TableField(value = "module_event_sale")
    @JSONField(name = "module_event_sale")
    @JsonProperty("module_event_sale")
    private Boolean moduleEventSale;
    /**
     * 显示含税明细行在汇总表(B2B).
     */
    @DEField(name = "group_show_line_subtotals_tax_included")
    @TableField(value = "group_show_line_subtotals_tax_included")
    @JSONField(name = "group_show_line_subtotals_tax_included")
    @JsonProperty("group_show_line_subtotals_tax_included")
    private Boolean groupShowLineSubtotalsTaxIncluded;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 变体和选项
     */
    @DEField(name = "group_product_variant")
    @TableField(value = "group_product_variant")
    @JSONField(name = "group_product_variant")
    @JsonProperty("group_product_variant")
    private Boolean groupProductVariant;
    /**
     * SEPA贷记交易
     */
    @DEField(name = "module_account_sepa")
    @TableField(value = "module_account_sepa")
    @JSONField(name = "module_account_sepa")
    @JsonProperty("module_account_sepa")
    private Boolean moduleAccountSepa;
    /**
     * 多币种
     */
    @DEField(name = "group_multi_currency")
    @TableField(value = "group_multi_currency")
    @JSONField(name = "group_multi_currency")
    @JsonProperty("group_multi_currency")
    private Boolean groupMultiCurrency;
    /**
     * 分析会计
     */
    @DEField(name = "group_analytic_accounting")
    @TableField(value = "group_analytic_accounting")
    @JSONField(name = "group_analytic_accounting")
    @JsonProperty("group_analytic_accounting")
    private Boolean groupAnalyticAccounting;
    /**
     * 产品包装
     */
    @DEField(name = "group_stock_packaging")
    @TableField(value = "group_stock_packaging")
    @JSONField(name = "group_stock_packaging")
    @JsonProperty("group_stock_packaging")
    private Boolean groupStockPackaging;
    /**
     * 谷歌文档密钥
     */
    @TableField(exist = false)
    @JSONField(name = "website_slide_google_app_key")
    @JsonProperty("website_slide_google_app_key")
    private String websiteSlideGoogleAppKey;
    /**
     * 采购订单批准
     */
    @DEField(name = "po_order_approval")
    @TableField(value = "po_order_approval")
    @JSONField(name = "po_order_approval")
    @JsonProperty("po_order_approval")
    private Boolean poOrderApproval;
    /**
     * 销售模块是否已安装
     */
    @DEField(name = "is_installed_sale")
    @TableField(value = "is_installed_sale")
    @JSONField(name = "is_installed_sale")
    @JsonProperty("is_installed_sale")
    private Boolean isInstalledSale;
    /**
     * 发票在线付款
     */
    @DEField(name = "module_account_payment")
    @TableField(value = "module_account_payment")
    @JSONField(name = "module_account_payment")
    @JsonProperty("module_account_payment")
    private Boolean moduleAccountPayment;
    /**
     * 分析标签
     */
    @DEField(name = "group_analytic_tags")
    @TableField(value = "group_analytic_tags")
    @JSONField(name = "group_analytic_tags")
    @JsonProperty("group_analytic_tags")
    private Boolean groupAnalyticTags;
    /**
     * Asterisk (开源VoIP平台)
     */
    @DEField(name = "module_voip")
    @TableField(value = "module_voip")
    @JSONField(name = "module_voip")
    @JsonProperty("module_voip")
    private Boolean moduleVoip;
    /**
     * 购物车恢复EMail
     */
    @TableField(exist = false)
    @JSONField(name = "cart_recovery_mail_template")
    @JsonProperty("cart_recovery_mail_template")
    private Integer cartRecoveryMailTemplate;
    /**
     * 多网站
     */
    @DEField(name = "group_multi_website")
    @TableField(value = "group_multi_website")
    @JSONField(name = "group_multi_website")
    @JsonProperty("group_multi_website")
    private Boolean groupMultiWebsite;
    /**
     * 使用外部验证提供者 (OAuth)
     */
    @DEField(name = "module_auth_oauth")
    @TableField(value = "module_auth_oauth")
    @JSONField(name = "module_auth_oauth")
    @JsonProperty("module_auth_oauth")
    private Boolean moduleAuthOauth;
    /**
     * 送货管理
     */
    @DEField(name = "sale_delivery_settings")
    @TableField(value = "sale_delivery_settings")
    @JSONField(name = "sale_delivery_settings")
    @JsonProperty("sale_delivery_settings")
    private String saleDeliverySettings;
    /**
     * 报价单生成器
     */
    @DEField(name = "module_sale_quotation_builder")
    @TableField(value = "module_sale_quotation_builder")
    @JSONField(name = "module_sale_quotation_builder")
    @JsonProperty("module_sale_quotation_builder")
    private Boolean moduleSaleQuotationBuilder;
    /**
     * 管理公司间交易
     */
    @DEField(name = "module_inter_company_rules")
    @TableField(value = "module_inter_company_rules")
    @JSONField(name = "module_inter_company_rules")
    @JsonProperty("module_inter_company_rules")
    private Boolean moduleInterCompanyRules;
    /**
     * 销售的安全提前期
     */
    @DEField(name = "use_security_lead")
    @TableField(value = "use_security_lead")
    @JSONField(name = "use_security_lead")
    @JsonProperty("use_security_lead")
    private Boolean useSecurityLead;
    /**
     * 开票策略
     */
    @DEField(name = "default_invoice_policy")
    @TableField(value = "default_invoice_policy")
    @JSONField(name = "default_invoice_policy")
    @JsonProperty("default_invoice_policy")
    private String defaultInvoicePolicy;
    /**
     * 锁定确认订单
     */
    @DEField(name = "lock_confirmed_po")
    @TableField(value = "lock_confirmed_po")
    @JSONField(name = "lock_confirmed_po")
    @JsonProperty("lock_confirmed_po")
    private Boolean lockConfirmedPo;
    /**
     * 重量单位
     */
    @DEField(name = "product_weight_in_lbs")
    @TableField(value = "product_weight_in_lbs")
    @JSONField(name = "product_weight_in_lbs")
    @JsonProperty("product_weight_in_lbs")
    private String productWeightInLbs;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 谷歌分析仪表板
     */
    @TableField(exist = false)
    @JSONField(name = "has_google_analytics_dashboard")
    @JsonProperty("has_google_analytics_dashboard")
    private Boolean hasGoogleAnalyticsDashboard;
    /**
     * 批量拣货
     */
    @DEField(name = "module_stock_picking_batch")
    @TableField(value = "module_stock_picking_batch")
    @JSONField(name = "module_stock_picking_batch")
    @JsonProperty("module_stock_picking_batch")
    private Boolean moduleStockPickingBatch;
    /**
     * 网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 默认的费用别名
     */
    @DEField(name = "expense_alias_prefix")
    @TableField(value = "expense_alias_prefix")
    @JSONField(name = "expense_alias_prefix")
    @JsonProperty("expense_alias_prefix")
    private String expenseAliasPrefix;
    /**
     * 脸书账号
     */
    @TableField(exist = false)
    @JSONField(name = "social_facebook")
    @JsonProperty("social_facebook")
    private String socialFacebook;
    /**
     * Unsplash图像库
     */
    @DEField(name = "module_web_unsplash")
    @TableField(value = "module_web_unsplash")
    @JSONField(name = "module_web_unsplash")
    @JsonProperty("module_web_unsplash")
    private Boolean moduleWebUnsplash;
    /**
     * 群发邮件营销
     */
    @DEField(name = "group_mass_mailing_campaign")
    @TableField(value = "group_mass_mailing_campaign")
    @JSONField(name = "group_mass_mailing_campaign")
    @JsonProperty("group_mass_mailing_campaign")
    private Boolean groupMassMailingCampaign;
    /**
     * 用 CAMT.053 格式导入
     */
    @DEField(name = "module_account_bank_statement_import_camt")
    @TableField(value = "module_account_bank_statement_import_camt")
    @JSONField(name = "module_account_bank_statement_import_camt")
    @JsonProperty("module_account_bank_statement_import_camt")
    private Boolean moduleAccountBankStatementImportCamt;
    /**
     * 允许产品毛利
     */
    @DEField(name = "module_product_margin")
    @TableField(value = "module_product_margin")
    @JSONField(name = "module_product_margin")
    @JsonProperty("module_product_margin")
    private Boolean moduleProductMargin;
    /**
     * 子任务
     */
    @DEField(name = "group_subtask_project")
    @TableField(value = "group_subtask_project")
    @JSONField(name = "group_subtask_project")
    @JsonProperty("group_subtask_project")
    private Boolean groupSubtaskProject;
    /**
     * 三方匹配:采购，收货和发票
     */
    @DEField(name = "module_account_3way_match")
    @TableField(value = "module_account_3way_match")
    @JSONField(name = "module_account_3way_match")
    @JsonProperty("module_account_3way_match")
    private Boolean moduleAccount3wayMatch;
    /**
     * 数字内容
     */
    @DEField(name = "module_website_sale_digital")
    @TableField(value = "module_website_sale_digital")
    @JSONField(name = "module_website_sale_digital")
    @JsonProperty("module_website_sale_digital")
    private Boolean moduleWebsiteSaleDigital;
    /**
     * 优惠券和促销
     */
    @DEField(name = "module_sale_coupon")
    @TableField(value = "module_sale_coupon")
    @JSONField(name = "module_sale_coupon")
    @JsonProperty("module_sale_coupon")
    private Boolean moduleSaleCoupon;
    /**
     * 在取消订阅页面上显示黑名单按钮
     */
    @DEField(name = "show_blacklist_buttons")
    @TableField(value = "show_blacklist_buttons")
    @JSONField(name = "show_blacklist_buttons")
    @JsonProperty("show_blacklist_buttons")
    private Boolean showBlacklistButtons;
    /**
     * 库存警报
     */
    @DEField(name = "group_warning_stock")
    @TableField(value = "group_warning_stock")
    @JSONField(name = "group_warning_stock")
    @JsonProperty("group_warning_stock")
    private Boolean groupWarningStock;
    /**
     * 员工 PIN
     */
    @DEField(name = "group_attendance_use_pin")
    @TableField(value = "group_attendance_use_pin")
    @JSONField(name = "group_attendance_use_pin")
    @JsonProperty("group_attendance_use_pin")
    private Boolean groupAttendanceUsePin;
    /**
     * 销售团队
     */
    @TableField(exist = false)
    @JSONField(name = "salesteam_id")
    @JsonProperty("salesteam_id")
    private Integer salesteamId;
    /**
     * 图标
     */
    @TableField(exist = false)
    @JSONField(name = "favicon")
    @JsonProperty("favicon")
    private byte[] favicon;
    /**
     * 产品比较工具
     */
    @DEField(name = "module_website_sale_comparison")
    @TableField(value = "module_website_sale_comparison")
    @JSONField(name = "module_website_sale_comparison")
    @JsonProperty("module_website_sale_comparison")
    private Boolean moduleWebsiteSaleComparison;
    /**
     * 可用阈值
     */
    @DEField(name = "available_threshold")
    @TableField(value = "available_threshold")
    @JSONField(name = "available_threshold")
    @JsonProperty("available_threshold")
    private Double availableThreshold;
    /**
     * 安全交货时间
     */
    @DEField(name = "use_po_lead")
    @TableField(value = "use_po_lead")
    @JSONField(name = "use_po_lead")
    @JsonProperty("use_po_lead")
    private Boolean usePoLead;
    /**
     * 财年
     */
    @DEField(name = "group_fiscal_year")
    @TableField(value = "group_fiscal_year")
    @JSONField(name = "group_fiscal_year")
    @JsonProperty("group_fiscal_year")
    private Boolean groupFiscalYear;
    /**
     * 任务日志
     */
    @DEField(name = "module_hr_timesheet")
    @TableField(value = "module_hr_timesheet")
    @JSONField(name = "module_hr_timesheet")
    @JsonProperty("module_hr_timesheet")
    private Boolean moduleHrTimesheet;
    /**
     * 产品生命周期管理 (PLM)
     */
    @DEField(name = "module_mrp_plm")
    @TableField(value = "module_mrp_plm")
    @JSONField(name = "module_mrp_plm")
    @JsonProperty("module_mrp_plm")
    private Boolean moduleMrpPlm;
    /**
     * 银行接口－自动同步银行费用
     */
    @DEField(name = "module_account_yodlee")
    @TableField(value = "module_account_yodlee")
    @JSONField(name = "module_account_yodlee")
    @JsonProperty("module_account_yodlee")
    private Boolean moduleAccountYodlee;
    /**
     * CDN筛选
     */
    @TableField(exist = false)
    @JSONField(name = "cdn_filters")
    @JsonProperty("cdn_filters")
    private String cdnFilters;
    /**
     * UPS 接口
     */
    @DEField(name = "module_delivery_ups")
    @TableField(value = "module_delivery_ups")
    @JSONField(name = "module_delivery_ups")
    @JsonProperty("module_delivery_ups")
    private Boolean moduleDeliveryUps;
    /**
     * 失败的邮件
     */
    @DEField(name = "fail_counter")
    @TableField(value = "fail_counter")
    @JSONField(name = "fail_counter")
    @JsonProperty("fail_counter")
    private Integer failCounter;
    /**
     * 批次和序列号
     */
    @DEField(name = "group_stock_production_lot")
    @TableField(value = "group_stock_production_lot")
    @JSONField(name = "group_stock_production_lot")
    @JsonProperty("group_stock_production_lot")
    private Boolean groupStockProductionLot;
    /**
     * 有会计分录
     */
    @TableField(exist = false)
    @JSONField(name = "has_accounting_entries")
    @JsonProperty("has_accounting_entries")
    private Boolean hasAccountingEntries;
    /**
     * 计量单位
     */
    @DEField(name = "group_uom")
    @TableField(value = "group_uom")
    @JSONField(name = "group_uom")
    @JsonProperty("group_uom")
    private Boolean groupUom;
    /**
     * 指定邮件服务器
     */
    @DEField(name = "mass_mailing_outgoing_mail_server")
    @TableField(value = "mass_mailing_outgoing_mail_server")
    @JSONField(name = "mass_mailing_outgoing_mail_server")
    @JsonProperty("mass_mailing_outgoing_mail_server")
    private Boolean massMailingOutgoingMailServer;
    /**
     * 默认线索别名
     */
    @DEField(name = "crm_alias_prefix")
    @TableField(value = "crm_alias_prefix")
    @JSONField(name = "crm_alias_prefix")
    @JsonProperty("crm_alias_prefix")
    private String crmAliasPrefix;
    /**
     * Google+账户
     */
    @TableField(exist = false)
    @JSONField(name = "social_googleplus")
    @JsonProperty("social_googleplus")
    private String socialGoogleplus;
    /**
     * Google 客户端密钥
     */
    @TableField(exist = false)
    @JSONField(name = "google_management_client_secret")
    @JsonProperty("google_management_client_secret")
    private String googleManagementClientSecret;
    /**
     * 默认社交分享图片
     */
    @TableField(exist = false)
    @JSONField(name = "social_default_image")
    @JsonProperty("social_default_image")
    private byte[] socialDefaultImage;
    /**
     * Google 分析
     */
    @TableField(exist = false)
    @JSONField(name = "has_google_analytics")
    @JsonProperty("has_google_analytics")
    private Boolean hasGoogleAnalytics;
    /**
     * 调查登记
     */
    @DEField(name = "module_website_event_questions")
    @TableField(value = "module_website_event_questions")
    @JSONField(name = "module_website_event_questions")
    @JsonProperty("module_website_event_questions")
    private Boolean moduleWebsiteEventQuestions;
    /**
     * 默认语言
     */
    @TableField(exist = false)
    @JSONField(name = "website_default_lang_id")
    @JsonProperty("website_default_lang_id")
    private Integer websiteDefaultLangId;
    /**
     * 库存可用性
     */
    @DEField(name = "inventory_availability")
    @TableField(value = "inventory_availability")
    @JSONField(name = "inventory_availability")
    @JsonProperty("inventory_availability")
    private String inventoryAvailability;
    /**
     * 采购警告
     */
    @DEField(name = "group_warning_purchase")
    @TableField(value = "group_warning_purchase")
    @JSONField(name = "group_warning_purchase")
    @JsonProperty("group_warning_purchase")
    private Boolean groupWarningPurchase;
    /**
     * 质量
     */
    @DEField(name = "module_quality_control")
    @TableField(value = "module_quality_control")
    @JSONField(name = "module_quality_control")
    @JsonProperty("module_quality_control")
    private Boolean moduleQualityControl;
    /**
     * 手动分配EMail
     */
    @DEField(name = "generate_lead_from_alias")
    @TableField(value = "generate_lead_from_alias")
    @JSONField(name = "generate_lead_from_alias")
    @JsonProperty("generate_lead_from_alias")
    private Boolean generateLeadFromAlias;
    /**
     * 外部邮件服务器
     */
    @DEField(name = "external_email_server_default")
    @TableField(value = "external_email_server_default")
    @JSONField(name = "external_email_server_default")
    @JsonProperty("external_email_server_default")
    private Boolean externalEmailServerDefault;
    /**
     * 访问秘钥
     */
    @DEField(name = "unsplash_access_key")
    @TableField(value = "unsplash_access_key")
    @JSONField(name = "unsplash_access_key")
    @JsonProperty("unsplash_access_key")
    private String unsplashAccessKey;
    /**
     * 用Gengo翻译您的网站
     */
    @DEField(name = "module_base_gengo")
    @TableField(value = "module_base_gengo")
    @JSONField(name = "module_base_gengo")
    @JsonProperty("module_base_gengo")
    private Boolean moduleBaseGengo;
    /**
     * 在线票务
     */
    @DEField(name = "module_website_event_sale")
    @TableField(value = "module_website_event_sale")
    @JSONField(name = "module_website_event_sale")
    @JsonProperty("module_website_event_sale")
    private Boolean moduleWebsiteEventSale;
    /**
     * 默认销售人员
     */
    @TableField(exist = false)
    @JSONField(name = "crm_default_user_id")
    @JsonProperty("crm_default_user_id")
    private Integer crmDefaultUserId;
    /**
     * 代发货
     */
    @DEField(name = "module_stock_dropshipping")
    @TableField(value = "module_stock_dropshipping")
    @JSONField(name = "module_stock_dropshipping")
    @JsonProperty("module_stock_dropshipping")
    private Boolean moduleStockDropshipping;
    /**
     * 邮件服务器
     */
    @DEField(name = "mass_mailing_mail_server_id")
    @TableField(value = "mass_mailing_mail_server_id")
    @JSONField(name = "mass_mailing_mail_server_id")
    @JsonProperty("mass_mailing_mail_server_id")
    private Integer massMailingMailServerId;
    /**
     * 明细行汇总含税(B2B).
     */
    @DEField(name = "group_show_line_subtotals_tax_excluded")
    @TableField(value = "group_show_line_subtotals_tax_excluded")
    @JSONField(name = "group_show_line_subtotals_tax_excluded")
    @JsonProperty("group_show_line_subtotals_tax_excluded")
    private Boolean groupShowLineSubtotalsTaxExcluded;
    /**
     * 面试表单
     */
    @DEField(name = "module_hr_recruitment_survey")
    @TableField(value = "module_hr_recruitment_survey")
    @JSONField(name = "module_hr_recruitment_survey")
    @JsonProperty("module_hr_recruitment_survey")
    private Boolean moduleHrRecruitmentSurvey;
    /**
     * 工单
     */
    @DEField(name = "module_mrp_workorder")
    @TableField(value = "module_mrp_workorder")
    @JSONField(name = "module_mrp_workorder")
    @JsonProperty("module_mrp_workorder")
    private Boolean moduleMrpWorkorder;
    /**
     * 集成卡支付
     */
    @DEField(name = "module_pos_mercury")
    @TableField(value = "module_pos_mercury")
    @JSONField(name = "module_pos_mercury")
    @JsonProperty("module_pos_mercury")
    private Boolean modulePosMercury;
    /**
     * 公司货币
     */
    @TableField(exist = false)
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="currency_id")
    private Long companyCurrencyId;
    /**
     * 现金收付制
     */
    @TableField(exist = false)
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="tax_exigibility")
    private Boolean taxExigibility;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService.class,referenceKey="id",referenceName="write_uid",backFillName="name")
    private String writeUidText;
    /**
     * 银行核销阈值
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "account_bank_reconciliation_start" , format="yyyy-MM-dd")
    @JsonProperty("account_bank_reconciliation_start")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="account_bank_reconciliation_start")
    private Timestamp accountBankReconciliationStart;
    /**
     * 税率现金收付制日记账
     */
    @TableField(exist = false)
    @JSONField(name = "tax_cash_basis_journal_id")
    @JsonProperty("tax_cash_basis_journal_id")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="tax_cash_basis_journal_id")
    private Long taxCashBasisJournalId;
    /**
     * 采购提前时间
     */
    @TableField(exist = false)
    @JSONField(name = "po_lead")
    @JsonProperty("po_lead")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="po_lead")
    private Double poLead;
    /**
     * 彩色打印
     */
    @TableField(exist = false)
    @JSONField(name = "snailmail_color")
    @JsonProperty("snailmail_color")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="snailmail_color")
    private Boolean snailmailColor;
    /**
     * 纸张格式
     */
    @TableField(exist = false)
    @JSONField(name = "paperformat_id")
    @JsonProperty("paperformat_id")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="paperformat_id")
    private Integer paperformatId;
    /**
     * 在线签名
     */
    @TableField(exist = false)
    @JSONField(name = "portal_confirmation_sign")
    @JsonProperty("portal_confirmation_sign")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="portal_confirmation_sign")
    private Boolean portalConfirmationSign;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="currency_id")
    private Long currencyId;
    /**
     * 摘要邮件
     */
    @TableField(exist = false)
    @JSONField(name = "digest_id_text")
    @JsonProperty("digest_id_text")
    private String digestIdText;
    /**
     * 用作通过注册创建的新用户的模版
     */
    @TableField(exist = false)
    @JSONField(name = "auth_signup_template_user_id_text")
    @JsonProperty("auth_signup_template_user_id_text")
    private String authSignupTemplateUserIdText;
    /**
     * 打印
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_is_print")
    @JsonProperty("invoice_is_print")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="invoice_is_print")
    private Boolean invoiceIsPrint;
    /**
     * 押金产品
     */
    @TableField(exist = false)
    @JSONField(name = "deposit_default_product_id_text")
    @JsonProperty("deposit_default_product_id_text")
    private String depositDefaultProductIdText;
    /**
     * 公司的上班时间
     */
    @TableField(exist = false)
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="resource_calendar_id")
    private Long resourceCalendarId;
    /**
     * 采购订单修改 *
     */
    @TableField(exist = false)
    @JSONField(name = "po_lock")
    @JsonProperty("po_lock")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="po_lock")
    private String poLock;
    /**
     * 透过邮递
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_is_snailmail")
    @JsonProperty("invoice_is_snailmail")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="invoice_is_snailmail")
    private Boolean invoiceIsSnailmail;
    /**
     * 制造提前期(日)
     */
    @TableField(exist = false)
    @JSONField(name = "manufacturing_lead")
    @JsonProperty("manufacturing_lead")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="manufacturing_lead")
    private Double manufacturingLead;
    /**
     * 审批层级 *
     */
    @TableField(exist = false)
    @JSONField(name = "po_double_validation")
    @JsonProperty("po_double_validation")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="po_double_validation")
    private String poDoubleValidation;
    /**
     * 自定义报表页脚
     */
    @TableField(exist = false)
    @JSONField(name = "report_footer")
    @JsonProperty("report_footer")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="report_footer")
    private String reportFooter;
    /**
     * EMail模板
     */
    @TableField(exist = false)
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;
    /**
     * 发送EMail
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_is_email")
    @JsonProperty("invoice_is_email")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="invoice_is_email")
    private Boolean invoiceIsEmail;
    /**
     * 在线支付
     */
    @TableField(exist = false)
    @JSONField(name = "portal_confirmation_pay")
    @JsonProperty("portal_confirmation_pay")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="portal_confirmation_pay")
    private Boolean portalConfirmationPay;
    /**
     * 显示SEPA QR码
     */
    @TableField(exist = false)
    @JSONField(name = "qr_code")
    @JsonProperty("qr_code")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="qr_code")
    private Boolean qrCode;
    /**
     * 双面打印
     */
    @TableField(exist = false)
    @JSONField(name = "snailmail_duplex")
    @JsonProperty("snailmail_duplex")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="snailmail_duplex")
    private Boolean snailmailDuplex;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="name")
    private String companyIdText;
    /**
     * 安全时间
     */
    @TableField(exist = false)
    @JSONField(name = "security_lead")
    @JsonProperty("security_lead")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="security_lead")
    private Double securityLead;
    /**
     * 文档模板
     */
    @TableField(exist = false)
    @JSONField(name = "external_report_layout_id")
    @JsonProperty("external_report_layout_id")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="external_report_layout_id")
    private Integer externalReportLayoutId;
    /**
     * 默认进项税
     */
    @TableField(exist = false)
    @JSONField(name = "purchase_tax_id")
    @JsonProperty("purchase_tax_id")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="account_purchase_tax_id")
    private Long purchaseTaxId;
    /**
     * 默认报价有效期（日）
     */
    @TableField(exist = false)
    @JSONField(name = "quotation_validity_days")
    @JsonProperty("quotation_validity_days")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="quotation_validity_days")
    private Integer quotationValidityDays;
    /**
     * 税率计算的舍入方法
     */
    @TableField(exist = false)
    @JSONField(name = "tax_calculation_rounding_method")
    @JsonProperty("tax_calculation_rounding_method")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="tax_calculation_rounding_method")
    private String taxCalculationRoundingMethod;
    /**
     * 汇兑损益
     */
    @TableField(exist = false)
    @JSONField(name = "currency_exchange_journal_id")
    @JsonProperty("currency_exchange_journal_id")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="currency_exchange_journal_id")
    private Long currencyExchangeJournalId;
    /**
     * 最小金额
     */
    @TableField(exist = false)
    @JSONField(name = "po_double_validation_amount")
    @JsonProperty("po_double_validation_amount")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="po_double_validation_amount")
    private BigDecimal poDoubleValidationAmount;
    /**
     * 模板
     */
    @TableField(exist = false)
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;
    /**
     * 默认销售税
     */
    @TableField(exist = false)
    @JSONField(name = "sale_tax_id")
    @JsonProperty("sale_tax_id")
    @BackFillProperty(reference=cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company.class,referenceService=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService.class,referenceKey="id",referenceName="company_id",backFillName="account_sale_tax_id")
    private Long saleTaxId;
    /**
     * 默认模板
     */
    @TableField(exist = false)
    @JSONField(name = "default_sale_order_template_id_text")
    @JsonProperty("default_sale_order_template_id_text")
    private String defaultSaleOrderTemplateIdText;
    /**
     * 默认模板
     */
    @DEField(name = "default_sale_order_template_id")
    @TableField(value = "default_sale_order_template_id")
    @JSONField(name = "default_sale_order_template_id")
    @JsonProperty("default_sale_order_template_id")
    private Long defaultSaleOrderTemplateId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 摘要邮件
     */
    @DEField(name = "digest_id")
    @TableField(value = "digest_id")
    @JSONField(name = "digest_id")
    @JsonProperty("digest_id")
    private Long digestId;
    /**
     * EMail模板
     */
    @DEField(name = "template_id")
    @TableField(value = "template_id")
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Long templateId;
    /**
     * 模板
     */
    @DEField(name = "chart_template_id")
    @TableField(value = "chart_template_id")
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Long chartTemplateId;
    /**
     * 押金产品
     */
    @DEField(name = "deposit_default_product_id")
    @TableField(value = "deposit_default_product_id")
    @JSONField(name = "deposit_default_product_id")
    @JsonProperty("deposit_default_product_id")
    private Long depositDefaultProductId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 用作通过注册创建的新用户的模版
     */
    @DEField(name = "auth_signup_template_user_id")
    @TableField(value = "auth_signup_template_user_id")
    @JSONField(name = "auth_signup_template_user_id")
    @JsonProperty("auth_signup_template_user_id")
    private Long authSignupTemplateUserId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooChartTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_digest.domain.Digest_digest odooDigest;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooDepositDefaultProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooAuthSignupTemplateUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_template odooDefaultSaleOrderTemplate;



    /**
     * 设置 [导入.qif 文件]
     */
    public void setModuleAccountBankStatementImportQif(Boolean moduleAccountBankStatementImportQif){
        this.moduleAccountBankStatementImportQif = moduleAccountBankStatementImportQif ;
        this.modify("module_account_bank_statement_import_qif",moduleAccountBankStatementImportQif);
    }

    /**
     * 设置 [导入.ofx格式]
     */
    public void setModuleAccountBankStatementImportOfx(Boolean moduleAccountBankStatementImportOfx){
        this.moduleAccountBankStatementImportOfx = moduleAccountBankStatementImportOfx ;
        this.modify("module_account_bank_statement_import_ofx",moduleAccountBankStatementImportOfx);
    }

    /**
     * 设置 [欧盟数字商品增值税]
     */
    public void setModuleL10nEuService(Boolean moduleL10nEuService){
        this.moduleL10nEuService = moduleL10nEuService ;
        this.modify("module_l10n_eu_service",moduleL10nEuService);
    }

    /**
     * 设置 [允许在登录页开启密码重置功能]
     */
    public void setAuthSignupResetPassword(Boolean authSignupResetPassword){
        this.authSignupResetPassword = authSignupResetPassword ;
        this.modify("auth_signup_reset_password",authSignupResetPassword);
    }

    /**
     * 设置 [采购招标]
     */
    public void setModulePurchaseRequisition(Boolean modulePurchaseRequisition){
        this.modulePurchaseRequisition = modulePurchaseRequisition ;
        this.modify("module_purchase_requisition",modulePurchaseRequisition);
    }

    /**
     * 设置 [Easypost 接口]
     */
    public void setModuleDeliveryEasypost(Boolean moduleDeliveryEasypost){
        this.moduleDeliveryEasypost = moduleDeliveryEasypost ;
        this.modify("module_delivery_easypost",moduleDeliveryEasypost);
    }

    /**
     * 设置 [折扣]
     */
    public void setGroupDiscountPerSoLine(Boolean groupDiscountPerSoLine){
        this.groupDiscountPerSoLine = groupDiscountPerSoLine ;
        this.modify("group_discount_per_so_line",groupDiscountPerSoLine);
    }

    /**
     * 设置 [自动开票]
     */
    public void setAutomaticInvoice(Boolean automaticInvoice){
        this.automaticInvoice = automaticInvoice ;
        this.modify("automatic_invoice",automaticInvoice);
    }

    /**
     * 设置 [Plaid 接口]
     */
    public void setModuleAccountPlaid(Boolean moduleAccountPlaid){
        this.moduleAccountPlaid = moduleAccountPlaid ;
        this.modify("module_account_plaid",moduleAccountPlaid);
    }

    /**
     * 设置 [允许支票打印和存款]
     */
    public void setModuleAccountCheckPrinting(Boolean moduleAccountCheckPrinting){
        this.moduleAccountCheckPrinting = moduleAccountCheckPrinting ;
        this.modify("module_account_check_printing",moduleAccountCheckPrinting);
    }

    /**
     * 设置 [自动填充公司数据]
     */
    public void setModulePartnerAutocomplete(Boolean modulePartnerAutocomplete){
        this.modulePartnerAutocomplete = modulePartnerAutocomplete ;
        this.modify("module_partner_autocomplete",modulePartnerAutocomplete);
    }

    /**
     * 设置 [链接跟踪器]
     */
    public void setModuleWebsiteLinks(Boolean moduleWebsiteLinks){
        this.moduleWebsiteLinks = moduleWebsiteLinks ;
        this.modify("module_website_links",moduleWebsiteLinks);
    }

    /**
     * 设置 [客户地址]
     */
    public void setGroupSaleDeliveryAddress(Boolean groupSaleDeliveryAddress){
        this.groupSaleDeliveryAddress = groupSaleDeliveryAddress ;
        this.modify("group_sale_delivery_address",groupSaleDeliveryAddress);
    }

    /**
     * 设置 [LDAP认证]
     */
    public void setModuleAuthLdap(Boolean moduleAuthLdap){
        this.moduleAuthLdap = moduleAuthLdap ;
        this.modify("module_auth_ldap",moduleAuthLdap);
    }

    /**
     * 设置 [在线发布]
     */
    public void setModuleWebsiteHrRecruitment(Boolean moduleWebsiteHrRecruitment){
        this.moduleWebsiteHrRecruitment = moduleWebsiteHrRecruitment ;
        this.modify("module_website_hr_recruitment",moduleWebsiteHrRecruitment);
    }

    /**
     * 设置 [预测]
     */
    public void setModuleProjectForecast(Boolean moduleProjectForecast){
        this.moduleProjectForecast = moduleProjectForecast ;
        this.modify("module_project_forecast",moduleProjectForecast);
    }

    /**
     * 设置 [寄售]
     */
    public void setGroupStockTrackingOwner(Boolean groupStockTrackingOwner){
        this.groupStockTrackingOwner = groupStockTrackingOwner ;
        this.modify("group_stock_tracking_owner",groupStockTrackingOwner);
    }

    /**
     * 设置 [允许用户同步Google日历]
     */
    public void setModuleGoogleCalendar(Boolean moduleGoogleCalendar){
        this.moduleGoogleCalendar = moduleGoogleCalendar ;
        this.modify("module_google_calendar",moduleGoogleCalendar);
    }

    /**
     * 设置 [开票]
     */
    public void setModuleAccount(Boolean moduleAccount){
        this.moduleAccount = moduleAccount ;
        this.modify("module_account",moduleAccount);
    }

    /**
     * 设置 [附加Google文档到记录]
     */
    public void setModuleGoogleDrive(Boolean moduleGoogleDrive){
        this.moduleGoogleDrive = moduleGoogleDrive ;
        this.modify("module_google_drive",moduleGoogleDrive);
    }

    /**
     * 设置 [自动汇率]
     */
    public void setModuleCurrencyRateLive(Boolean moduleCurrencyRateLive){
        this.moduleCurrencyRateLive = moduleCurrencyRateLive ;
        this.modify("module_currency_rate_live",moduleCurrencyRateLive);
    }

    /**
     * 设置 [形式发票]
     */
    public void setGroupProformaSales(Boolean groupProformaSales){
        this.groupProformaSales = groupProformaSales ;
        this.modify("group_proforma_sales",groupProformaSales);
    }

    /**
     * 设置 [FedEx 接口]
     */
    public void setModuleDeliveryFedex(Boolean moduleDeliveryFedex){
        this.moduleDeliveryFedex = moduleDeliveryFedex ;
        this.modify("module_delivery_fedex",moduleDeliveryFedex);
    }

    /**
     * 设置 [特定的EMail]
     */
    public void setModuleProductEmailTemplate(Boolean moduleProductEmailTemplate){
        this.moduleProductEmailTemplate = moduleProductEmailTemplate ;
        this.modify("module_product_email_template",moduleProductEmailTemplate);
    }

    /**
     * 设置 [显示效果]
     */
    public void setShowEffect(Boolean showEffect){
        this.showEffect = showEffect ;
        this.modify("show_effect",showEffect);
    }

    /**
     * 设置 [拣货策略]
     */
    public void setDefaultPickingPolicy(String defaultPickingPolicy){
        this.defaultPickingPolicy = defaultPickingPolicy ;
        this.modify("default_picking_policy",defaultPickingPolicy);
    }

    /**
     * 设置 [USPS 接口]
     */
    public void setModuleDeliveryUsps(Boolean moduleDeliveryUsps){
        this.moduleDeliveryUsps = moduleDeliveryUsps ;
        this.modify("module_delivery_usps",moduleDeliveryUsps);
    }

    /**
     * 设置 [DHL 接口]
     */
    public void setModuleDeliveryDhl(Boolean moduleDeliveryDhl){
        this.moduleDeliveryDhl = moduleDeliveryDhl ;
        this.modify("module_delivery_dhl",moduleDeliveryDhl);
    }

    /**
     * 设置 [使用项目评级]
     */
    public void setGroupProjectRating(Boolean groupProjectRating){
        this.groupProjectRating = groupProjectRating ;
        this.modify("group_project_rating",groupProjectRating);
    }

    /**
     * 设置 [线索]
     */
    public void setGroupUseLead(Boolean groupUseLead){
        this.groupUseLead = groupUseLead ;
        this.modify("group_use_lead",groupUseLead);
    }

    /**
     * 设置 [交货包裹]
     */
    public void setGroupStockTrackingLot(Boolean groupStockTrackingLot){
        this.groupStockTrackingLot = groupStockTrackingLot ;
        this.modify("group_stock_tracking_lot",groupStockTrackingLot);
    }

    /**
     * 设置 [多步路由]
     */
    public void setGroupStockAdvLocation(Boolean groupStockAdvLocation){
        this.groupStockAdvLocation = groupStockAdvLocation ;
        this.modify("group_stock_adv_location",groupStockAdvLocation);
    }

    /**
     * 设置 [保留]
     */
    public void setModuleProcurementJit(String moduleProcurementJit){
        this.moduleProcurementJit = moduleProcurementJit ;
        this.modify("module_procurement_jit",moduleProcurementJit);
    }

    /**
     * 设置 [允许员工通过EMail记录费用]
     */
    public void setUseMailgateway(Boolean useMailgateway){
        this.useMailgateway = useMailgateway ;
        this.modify("use_mailgateway",useMailgateway);
    }

    /**
     * 设置 [为每个客户使用价格表来适配您的价格]
     */
    public void setGroupSalePricelist(Boolean groupSalePricelist){
        this.groupSalePricelist = groupSalePricelist ;
        this.modify("group_sale_pricelist",groupSalePricelist);
    }

    /**
     * 设置 [储存位置]
     */
    public void setGroupStockMultiLocations(Boolean groupStockMultiLocations){
        this.groupStockMultiLocations = groupStockMultiLocations ;
        this.modify("group_stock_multi_locations",groupStockMultiLocations);
    }

    /**
     * 设置 [默认制造提前期]
     */
    public void setUseManufacturingLead(Boolean useManufacturingLead){
        this.useManufacturingLead = useManufacturingLead ;
        this.modify("use_manufacturing_lead",useManufacturingLead);
    }

    /**
     * 设置 [Google 电子表格]
     */
    public void setModuleGoogleSpreadsheet(Boolean moduleGoogleSpreadsheet){
        this.moduleGoogleSpreadsheet = moduleGoogleSpreadsheet ;
        this.modify("module_google_spreadsheet",moduleGoogleSpreadsheet);
    }

    /**
     * 设置 [税目汇总表]
     */
    public void setShowLineSubtotalsTaxSelection(String showLineSubtotalsTaxSelection){
        this.showLineSubtotalsTaxSelection = showLineSubtotalsTaxSelection ;
        this.modify("show_line_subtotals_tax_selection",showLineSubtotalsTaxSelection);
    }

    /**
     * 设置 [电商物流成本]
     */
    public void setModuleWebsiteSaleDelivery(Boolean moduleWebsiteSaleDelivery){
        this.moduleWebsiteSaleDelivery = moduleWebsiteSaleDelivery ;
        this.modify("module_website_sale_delivery",moduleWebsiteSaleDelivery);
    }

    /**
     * 设置 [主生产排程]
     */
    public void setModuleMrpMps(Boolean moduleMrpMps){
        this.moduleMrpMps = moduleMrpMps ;
        this.modify("module_mrp_mps",moduleMrpMps);
    }

    /**
     * 设置 [显示组织架构图]
     */
    public void setModuleHrOrgChart(Boolean moduleHrOrgChart){
        this.moduleHrOrgChart = moduleHrOrgChart ;
        this.modify("module_hr_org_chart",moduleHrOrgChart);
    }

    /**
     * 设置 [到期日]
     */
    public void setModuleProductExpiry(Boolean moduleProductExpiry){
        this.moduleProductExpiry = moduleProductExpiry ;
        this.modify("module_product_expiry",moduleProductExpiry);
    }

    /**
     * 设置 [bpost 接口]
     */
    public void setModuleDeliveryBpost(Boolean moduleDeliveryBpost){
        this.moduleDeliveryBpost = moduleDeliveryBpost ;
        this.modify("module_delivery_bpost",moduleDeliveryBpost);
    }

    /**
     * 设置 [条码扫描器]
     */
    public void setModuleStockBarcode(Boolean moduleStockBarcode){
        this.moduleStockBarcode = moduleStockBarcode ;
        this.modify("module_stock_barcode",moduleStockBarcode);
    }

    /**
     * 设置 [国际贸易统计组织]
     */
    public void setModuleAccountIntrastat(Boolean moduleAccountIntrastat){
        this.moduleAccountIntrastat = moduleAccountIntrastat ;
        this.modify("module_account_intrastat",moduleAccountIntrastat);
    }

    /**
     * 设置 [报价单模板]
     */
    public void setGroupSaleOrderTemplate(Boolean groupSaleOrderTemplate){
        this.groupSaleOrderTemplate = groupSaleOrderTemplate ;
        this.modify("group_sale_order_template",groupSaleOrderTemplate);
    }

    /**
     * 设置 [使用SEPA直接计入借方]
     */
    public void setModuleAccountSepaDirectDebit(Boolean moduleAccountSepaDirectDebit){
        this.moduleAccountSepaDirectDebit = moduleAccountSepaDirectDebit ;
        this.modify("module_account_sepa_direct_debit",moduleAccountSepaDirectDebit);
    }

    /**
     * 设置 [默认报价有效期]
     */
    public void setUseQuotationValidityDays(Boolean useQuotationValidityDays){
        this.useQuotationValidityDays = useQuotationValidityDays ;
        this.modify("use_quotation_validity_days",useQuotationValidityDays);
    }

    /**
     * 设置 [使用批量付款]
     */
    public void setModuleAccountBatchPayment(Boolean moduleAccountBatchPayment){
        this.moduleAccountBatchPayment = moduleAccountBatchPayment ;
        this.modify("module_account_batch_payment",moduleAccountBatchPayment);
    }

    /**
     * 设置 [预算管理]
     */
    public void setModuleAccountBudget(Boolean moduleAccountBudget){
        this.moduleAccountBudget = moduleAccountBudget ;
        this.modify("module_account_budget",moduleAccountBudget);
    }

    /**
     * 设置 [MRP 工单]
     */
    public void setGroupMrpRoutings(Boolean groupMrpRoutings){
        this.groupMrpRoutings = groupMrpRoutings ;
        this.modify("group_mrp_routings",groupMrpRoutings);
    }

    /**
     * 设置 [现金舍入]
     */
    public void setGroupCashRounding(Boolean groupCashRounding){
        this.groupCashRounding = groupCashRounding ;
        this.modify("group_cash_rounding",groupCashRounding);
    }

    /**
     * 设置 [到岸成本]
     */
    public void setModuleStockLandedCosts(Boolean moduleStockLandedCosts){
        this.moduleStockLandedCosts = moduleStockLandedCosts ;
        this.modify("module_stock_landed_costs",moduleStockLandedCosts);
    }

    /**
     * 设置 [库存]
     */
    public void setModuleWebsiteSaleStock(Boolean moduleWebsiteSaleStock){
        this.moduleWebsiteSaleStock = moduleWebsiteSaleStock ;
        this.modify("module_website_sale_stock",moduleWebsiteSaleStock);
    }

    /**
     * 设置 [耿宗并计划]
     */
    public void setModuleWebsiteEventTrack(Boolean moduleWebsiteEventTrack){
        this.moduleWebsiteEventTrack = moduleWebsiteEventTrack ;
        this.modify("module_website_event_track",moduleWebsiteEventTrack);
    }

    /**
     * 设置 [运输成本]
     */
    public void setModuleDelivery(Boolean moduleDelivery){
        this.moduleDelivery = moduleDelivery ;
        this.modify("module_delivery",moduleDelivery);
    }

    /**
     * 设置 [自动票据处理]
     */
    public void setModuleAccountInvoiceExtract(Boolean moduleAccountInvoiceExtract){
        this.moduleAccountInvoiceExtract = moduleAccountInvoiceExtract ;
        this.modify("module_account_invoice_extract",moduleAccountInvoiceExtract);
    }

    /**
     * 设置 [销售订单警告]
     */
    public void setGroupWarningSale(Boolean groupWarningSale){
        this.groupWarningSale = groupWarningSale ;
        this.modify("group_warning_sale",groupWarningSale);
    }

    /**
     * 设置 [条码]
     */
    public void setModuleEventBarcode(Boolean moduleEventBarcode){
        this.moduleEventBarcode = moduleEventBarcode ;
        this.modify("module_event_barcode",moduleEventBarcode);
    }

    /**
     * 设置 [别名域]
     */
    public void setAliasDomain(String aliasDomain){
        this.aliasDomain = aliasDomain ;
        this.modify("alias_domain",aliasDomain);
    }

    /**
     * 设置 [多仓库]
     */
    public void setGroupStockMultiWarehouses(Boolean groupStockMultiWarehouses){
        this.groupStockMultiWarehouses = groupStockMultiWarehouses ;
        this.modify("group_stock_multi_warehouses",groupStockMultiWarehouses);
    }

    /**
     * 设置 [动态报告]
     */
    public void setModuleAccountReports(Boolean moduleAccountReports){
        this.moduleAccountReports = moduleAccountReports ;
        this.modify("module_account_reports",moduleAccountReports);
    }

    /**
     * 设置 [显示产品的价目表]
     */
    public void setGroupProductPricelist(Boolean groupProductPricelist){
        this.groupProductPricelist = groupProductPricelist ;
        this.modify("group_product_pricelist",groupProductPricelist);
    }

    /**
     * 设置 [技能管理]
     */
    public void setModuleHrSkills(Boolean moduleHrSkills){
        this.moduleHrSkills = moduleHrSkills ;
        this.modify("module_hr_skills",moduleHrSkills);
    }

    /**
     * 设置 [A / B测试]
     */
    public void setModuleWebsiteVersion(Boolean moduleWebsiteVersion){
        this.moduleWebsiteVersion = moduleWebsiteVersion ;
        this.modify("module_website_version",moduleWebsiteVersion);
    }

    /**
     * 设置 [允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据]
     */
    public void setModuleBaseImport(Boolean moduleBaseImport){
        this.moduleBaseImport = moduleBaseImport ;
        this.modify("module_base_import",moduleBaseImport);
    }

    /**
     * 设置 [以 CSV 格式导入]
     */
    public void setModuleAccountBankStatementImportCsv(Boolean moduleAccountBankStatementImportCsv){
        this.moduleAccountBankStatementImportCsv = moduleAccountBankStatementImportCsv ;
        this.modify("module_account_bank_statement_import_csv",moduleAccountBankStatementImportCsv);
    }

    /**
     * 设置 [科目税]
     */
    public void setModuleAccountTaxcloud(Boolean moduleAccountTaxcloud){
        this.moduleAccountTaxcloud = moduleAccountTaxcloud ;
        this.modify("module_account_taxcloud",moduleAccountTaxcloud);
    }

    /**
     * 设置 [Accounting]
     */
    public void setModuleAccountAccountant(Boolean moduleAccountAccountant){
        this.moduleAccountAccountant = moduleAccountAccountant ;
        this.modify("module_account_accountant",moduleAccountAccountant);
    }

    /**
     * 设置 [毛利]
     */
    public void setModuleSaleMargin(Boolean moduleSaleMargin){
        this.moduleSaleMargin = moduleSaleMargin ;
        this.modify("module_sale_margin",moduleSaleMargin);
    }

    /**
     * 设置 [摘要邮件]
     */
    public void setDigestEmails(Boolean digestEmails){
        this.digestEmails = digestEmails ;
        this.modify("digest_emails",digestEmails);
    }

    /**
     * 设置 [协作pad]
     */
    public void setModulePad(Boolean modulePad){
        this.modulePad = modulePad ;
        this.modify("module_pad",modulePad);
    }

    /**
     * 设置 [发票警告]
     */
    public void setGroupWarningAccount(Boolean groupWarningAccount){
        this.groupWarningAccount = groupWarningAccount ;
        this.modify("group_warning_account",groupWarningAccount);
    }

    /**
     * 设置 [贸易条款]
     */
    public void setGroupDisplayIncoterm(Boolean groupDisplayIncoterm){
        this.groupDisplayIncoterm = groupDisplayIncoterm ;
        this.modify("group_display_incoterm",groupDisplayIncoterm);
    }

    /**
     * 设置 [心愿单]
     */
    public void setModuleWebsiteSaleWishlist(Boolean moduleWebsiteSaleWishlist){
        this.moduleWebsiteSaleWishlist = moduleWebsiteSaleWishlist ;
        this.modify("module_website_sale_wishlist",moduleWebsiteSaleWishlist);
    }

    /**
     * 设置 [默认访问权限]
     */
    public void setUserDefaultRights(Boolean userDefaultRights){
        this.userDefaultRights = userDefaultRights ;
        this.modify("user_default_rights",userDefaultRights);
    }

    /**
     * 设置 [账单控制]
     */
    public void setDefaultPurchaseMethod(String defaultPurchaseMethod){
        this.defaultPurchaseMethod = defaultPurchaseMethod ;
        this.modify("default_purchase_method",defaultPurchaseMethod);
    }

    /**
     * 设置 [送货地址]
     */
    public void setGroupDeliveryInvoiceAddress(Boolean groupDeliveryInvoiceAddress){
        this.groupDeliveryInvoiceAddress = groupDeliveryInvoiceAddress ;
        this.modify("group_delivery_invoice_address",groupDeliveryInvoiceAddress);
    }

    /**
     * 设置 [显示批次 / 序列号]
     */
    public void setGroupLotOnDeliverySlip(Boolean groupLotOnDeliverySlip){
        this.groupLotOnDeliverySlip = groupLotOnDeliverySlip ;
        this.modify("group_lot_on_delivery_slip",groupLotOnDeliverySlip);
    }

    /**
     * 设置 [入场券]
     */
    public void setModuleEventSale(Boolean moduleEventSale){
        this.moduleEventSale = moduleEventSale ;
        this.modify("module_event_sale",moduleEventSale);
    }

    /**
     * 设置 [显示含税明细行在汇总表(B2B).]
     */
    public void setGroupShowLineSubtotalsTaxIncluded(Boolean groupShowLineSubtotalsTaxIncluded){
        this.groupShowLineSubtotalsTaxIncluded = groupShowLineSubtotalsTaxIncluded ;
        this.modify("group_show_line_subtotals_tax_included",groupShowLineSubtotalsTaxIncluded);
    }

    /**
     * 设置 [变体和选项]
     */
    public void setGroupProductVariant(Boolean groupProductVariant){
        this.groupProductVariant = groupProductVariant ;
        this.modify("group_product_variant",groupProductVariant);
    }

    /**
     * 设置 [SEPA贷记交易]
     */
    public void setModuleAccountSepa(Boolean moduleAccountSepa){
        this.moduleAccountSepa = moduleAccountSepa ;
        this.modify("module_account_sepa",moduleAccountSepa);
    }

    /**
     * 设置 [多币种]
     */
    public void setGroupMultiCurrency(Boolean groupMultiCurrency){
        this.groupMultiCurrency = groupMultiCurrency ;
        this.modify("group_multi_currency",groupMultiCurrency);
    }

    /**
     * 设置 [分析会计]
     */
    public void setGroupAnalyticAccounting(Boolean groupAnalyticAccounting){
        this.groupAnalyticAccounting = groupAnalyticAccounting ;
        this.modify("group_analytic_accounting",groupAnalyticAccounting);
    }

    /**
     * 设置 [产品包装]
     */
    public void setGroupStockPackaging(Boolean groupStockPackaging){
        this.groupStockPackaging = groupStockPackaging ;
        this.modify("group_stock_packaging",groupStockPackaging);
    }

    /**
     * 设置 [采购订单批准]
     */
    public void setPoOrderApproval(Boolean poOrderApproval){
        this.poOrderApproval = poOrderApproval ;
        this.modify("po_order_approval",poOrderApproval);
    }

    /**
     * 设置 [销售模块是否已安装]
     */
    public void setIsInstalledSale(Boolean isInstalledSale){
        this.isInstalledSale = isInstalledSale ;
        this.modify("is_installed_sale",isInstalledSale);
    }

    /**
     * 设置 [发票在线付款]
     */
    public void setModuleAccountPayment(Boolean moduleAccountPayment){
        this.moduleAccountPayment = moduleAccountPayment ;
        this.modify("module_account_payment",moduleAccountPayment);
    }

    /**
     * 设置 [分析标签]
     */
    public void setGroupAnalyticTags(Boolean groupAnalyticTags){
        this.groupAnalyticTags = groupAnalyticTags ;
        this.modify("group_analytic_tags",groupAnalyticTags);
    }

    /**
     * 设置 [Asterisk (开源VoIP平台)]
     */
    public void setModuleVoip(Boolean moduleVoip){
        this.moduleVoip = moduleVoip ;
        this.modify("module_voip",moduleVoip);
    }

    /**
     * 设置 [多网站]
     */
    public void setGroupMultiWebsite(Boolean groupMultiWebsite){
        this.groupMultiWebsite = groupMultiWebsite ;
        this.modify("group_multi_website",groupMultiWebsite);
    }

    /**
     * 设置 [使用外部验证提供者 (OAuth)]
     */
    public void setModuleAuthOauth(Boolean moduleAuthOauth){
        this.moduleAuthOauth = moduleAuthOauth ;
        this.modify("module_auth_oauth",moduleAuthOauth);
    }

    /**
     * 设置 [送货管理]
     */
    public void setSaleDeliverySettings(String saleDeliverySettings){
        this.saleDeliverySettings = saleDeliverySettings ;
        this.modify("sale_delivery_settings",saleDeliverySettings);
    }

    /**
     * 设置 [报价单生成器]
     */
    public void setModuleSaleQuotationBuilder(Boolean moduleSaleQuotationBuilder){
        this.moduleSaleQuotationBuilder = moduleSaleQuotationBuilder ;
        this.modify("module_sale_quotation_builder",moduleSaleQuotationBuilder);
    }

    /**
     * 设置 [管理公司间交易]
     */
    public void setModuleInterCompanyRules(Boolean moduleInterCompanyRules){
        this.moduleInterCompanyRules = moduleInterCompanyRules ;
        this.modify("module_inter_company_rules",moduleInterCompanyRules);
    }

    /**
     * 设置 [销售的安全提前期]
     */
    public void setUseSecurityLead(Boolean useSecurityLead){
        this.useSecurityLead = useSecurityLead ;
        this.modify("use_security_lead",useSecurityLead);
    }

    /**
     * 设置 [开票策略]
     */
    public void setDefaultInvoicePolicy(String defaultInvoicePolicy){
        this.defaultInvoicePolicy = defaultInvoicePolicy ;
        this.modify("default_invoice_policy",defaultInvoicePolicy);
    }

    /**
     * 设置 [锁定确认订单]
     */
    public void setLockConfirmedPo(Boolean lockConfirmedPo){
        this.lockConfirmedPo = lockConfirmedPo ;
        this.modify("lock_confirmed_po",lockConfirmedPo);
    }

    /**
     * 设置 [重量单位]
     */
    public void setProductWeightInLbs(String productWeightInLbs){
        this.productWeightInLbs = productWeightInLbs ;
        this.modify("product_weight_in_lbs",productWeightInLbs);
    }

    /**
     * 设置 [批量拣货]
     */
    public void setModuleStockPickingBatch(Boolean moduleStockPickingBatch){
        this.moduleStockPickingBatch = moduleStockPickingBatch ;
        this.modify("module_stock_picking_batch",moduleStockPickingBatch);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [默认的费用别名]
     */
    public void setExpenseAliasPrefix(String expenseAliasPrefix){
        this.expenseAliasPrefix = expenseAliasPrefix ;
        this.modify("expense_alias_prefix",expenseAliasPrefix);
    }

    /**
     * 设置 [Unsplash图像库]
     */
    public void setModuleWebUnsplash(Boolean moduleWebUnsplash){
        this.moduleWebUnsplash = moduleWebUnsplash ;
        this.modify("module_web_unsplash",moduleWebUnsplash);
    }

    /**
     * 设置 [群发邮件营销]
     */
    public void setGroupMassMailingCampaign(Boolean groupMassMailingCampaign){
        this.groupMassMailingCampaign = groupMassMailingCampaign ;
        this.modify("group_mass_mailing_campaign",groupMassMailingCampaign);
    }

    /**
     * 设置 [用 CAMT.053 格式导入]
     */
    public void setModuleAccountBankStatementImportCamt(Boolean moduleAccountBankStatementImportCamt){
        this.moduleAccountBankStatementImportCamt = moduleAccountBankStatementImportCamt ;
        this.modify("module_account_bank_statement_import_camt",moduleAccountBankStatementImportCamt);
    }

    /**
     * 设置 [允许产品毛利]
     */
    public void setModuleProductMargin(Boolean moduleProductMargin){
        this.moduleProductMargin = moduleProductMargin ;
        this.modify("module_product_margin",moduleProductMargin);
    }

    /**
     * 设置 [子任务]
     */
    public void setGroupSubtaskProject(Boolean groupSubtaskProject){
        this.groupSubtaskProject = groupSubtaskProject ;
        this.modify("group_subtask_project",groupSubtaskProject);
    }

    /**
     * 设置 [三方匹配:采购，收货和发票]
     */
    public void setModuleAccount3wayMatch(Boolean moduleAccount3wayMatch){
        this.moduleAccount3wayMatch = moduleAccount3wayMatch ;
        this.modify("module_account_3way_match",moduleAccount3wayMatch);
    }

    /**
     * 设置 [数字内容]
     */
    public void setModuleWebsiteSaleDigital(Boolean moduleWebsiteSaleDigital){
        this.moduleWebsiteSaleDigital = moduleWebsiteSaleDigital ;
        this.modify("module_website_sale_digital",moduleWebsiteSaleDigital);
    }

    /**
     * 设置 [优惠券和促销]
     */
    public void setModuleSaleCoupon(Boolean moduleSaleCoupon){
        this.moduleSaleCoupon = moduleSaleCoupon ;
        this.modify("module_sale_coupon",moduleSaleCoupon);
    }

    /**
     * 设置 [在取消订阅页面上显示黑名单按钮]
     */
    public void setShowBlacklistButtons(Boolean showBlacklistButtons){
        this.showBlacklistButtons = showBlacklistButtons ;
        this.modify("show_blacklist_buttons",showBlacklistButtons);
    }

    /**
     * 设置 [库存警报]
     */
    public void setGroupWarningStock(Boolean groupWarningStock){
        this.groupWarningStock = groupWarningStock ;
        this.modify("group_warning_stock",groupWarningStock);
    }

    /**
     * 设置 [员工 PIN]
     */
    public void setGroupAttendanceUsePin(Boolean groupAttendanceUsePin){
        this.groupAttendanceUsePin = groupAttendanceUsePin ;
        this.modify("group_attendance_use_pin",groupAttendanceUsePin);
    }

    /**
     * 设置 [产品比较工具]
     */
    public void setModuleWebsiteSaleComparison(Boolean moduleWebsiteSaleComparison){
        this.moduleWebsiteSaleComparison = moduleWebsiteSaleComparison ;
        this.modify("module_website_sale_comparison",moduleWebsiteSaleComparison);
    }

    /**
     * 设置 [可用阈值]
     */
    public void setAvailableThreshold(Double availableThreshold){
        this.availableThreshold = availableThreshold ;
        this.modify("available_threshold",availableThreshold);
    }

    /**
     * 设置 [安全交货时间]
     */
    public void setUsePoLead(Boolean usePoLead){
        this.usePoLead = usePoLead ;
        this.modify("use_po_lead",usePoLead);
    }

    /**
     * 设置 [财年]
     */
    public void setGroupFiscalYear(Boolean groupFiscalYear){
        this.groupFiscalYear = groupFiscalYear ;
        this.modify("group_fiscal_year",groupFiscalYear);
    }

    /**
     * 设置 [任务日志]
     */
    public void setModuleHrTimesheet(Boolean moduleHrTimesheet){
        this.moduleHrTimesheet = moduleHrTimesheet ;
        this.modify("module_hr_timesheet",moduleHrTimesheet);
    }

    /**
     * 设置 [产品生命周期管理 (PLM)]
     */
    public void setModuleMrpPlm(Boolean moduleMrpPlm){
        this.moduleMrpPlm = moduleMrpPlm ;
        this.modify("module_mrp_plm",moduleMrpPlm);
    }

    /**
     * 设置 [银行接口－自动同步银行费用]
     */
    public void setModuleAccountYodlee(Boolean moduleAccountYodlee){
        this.moduleAccountYodlee = moduleAccountYodlee ;
        this.modify("module_account_yodlee",moduleAccountYodlee);
    }

    /**
     * 设置 [UPS 接口]
     */
    public void setModuleDeliveryUps(Boolean moduleDeliveryUps){
        this.moduleDeliveryUps = moduleDeliveryUps ;
        this.modify("module_delivery_ups",moduleDeliveryUps);
    }

    /**
     * 设置 [失败的邮件]
     */
    public void setFailCounter(Integer failCounter){
        this.failCounter = failCounter ;
        this.modify("fail_counter",failCounter);
    }

    /**
     * 设置 [批次和序列号]
     */
    public void setGroupStockProductionLot(Boolean groupStockProductionLot){
        this.groupStockProductionLot = groupStockProductionLot ;
        this.modify("group_stock_production_lot",groupStockProductionLot);
    }

    /**
     * 设置 [计量单位]
     */
    public void setGroupUom(Boolean groupUom){
        this.groupUom = groupUom ;
        this.modify("group_uom",groupUom);
    }

    /**
     * 设置 [指定邮件服务器]
     */
    public void setMassMailingOutgoingMailServer(Boolean massMailingOutgoingMailServer){
        this.massMailingOutgoingMailServer = massMailingOutgoingMailServer ;
        this.modify("mass_mailing_outgoing_mail_server",massMailingOutgoingMailServer);
    }

    /**
     * 设置 [默认线索别名]
     */
    public void setCrmAliasPrefix(String crmAliasPrefix){
        this.crmAliasPrefix = crmAliasPrefix ;
        this.modify("crm_alias_prefix",crmAliasPrefix);
    }

    /**
     * 设置 [调查登记]
     */
    public void setModuleWebsiteEventQuestions(Boolean moduleWebsiteEventQuestions){
        this.moduleWebsiteEventQuestions = moduleWebsiteEventQuestions ;
        this.modify("module_website_event_questions",moduleWebsiteEventQuestions);
    }

    /**
     * 设置 [库存可用性]
     */
    public void setInventoryAvailability(String inventoryAvailability){
        this.inventoryAvailability = inventoryAvailability ;
        this.modify("inventory_availability",inventoryAvailability);
    }

    /**
     * 设置 [采购警告]
     */
    public void setGroupWarningPurchase(Boolean groupWarningPurchase){
        this.groupWarningPurchase = groupWarningPurchase ;
        this.modify("group_warning_purchase",groupWarningPurchase);
    }

    /**
     * 设置 [质量]
     */
    public void setModuleQualityControl(Boolean moduleQualityControl){
        this.moduleQualityControl = moduleQualityControl ;
        this.modify("module_quality_control",moduleQualityControl);
    }

    /**
     * 设置 [手动分配EMail]
     */
    public void setGenerateLeadFromAlias(Boolean generateLeadFromAlias){
        this.generateLeadFromAlias = generateLeadFromAlias ;
        this.modify("generate_lead_from_alias",generateLeadFromAlias);
    }

    /**
     * 设置 [外部邮件服务器]
     */
    public void setExternalEmailServerDefault(Boolean externalEmailServerDefault){
        this.externalEmailServerDefault = externalEmailServerDefault ;
        this.modify("external_email_server_default",externalEmailServerDefault);
    }

    /**
     * 设置 [访问秘钥]
     */
    public void setUnsplashAccessKey(String unsplashAccessKey){
        this.unsplashAccessKey = unsplashAccessKey ;
        this.modify("unsplash_access_key",unsplashAccessKey);
    }

    /**
     * 设置 [用Gengo翻译您的网站]
     */
    public void setModuleBaseGengo(Boolean moduleBaseGengo){
        this.moduleBaseGengo = moduleBaseGengo ;
        this.modify("module_base_gengo",moduleBaseGengo);
    }

    /**
     * 设置 [在线票务]
     */
    public void setModuleWebsiteEventSale(Boolean moduleWebsiteEventSale){
        this.moduleWebsiteEventSale = moduleWebsiteEventSale ;
        this.modify("module_website_event_sale",moduleWebsiteEventSale);
    }

    /**
     * 设置 [代发货]
     */
    public void setModuleStockDropshipping(Boolean moduleStockDropshipping){
        this.moduleStockDropshipping = moduleStockDropshipping ;
        this.modify("module_stock_dropshipping",moduleStockDropshipping);
    }

    /**
     * 设置 [邮件服务器]
     */
    public void setMassMailingMailServerId(Integer massMailingMailServerId){
        this.massMailingMailServerId = massMailingMailServerId ;
        this.modify("mass_mailing_mail_server_id",massMailingMailServerId);
    }

    /**
     * 设置 [明细行汇总含税(B2B).]
     */
    public void setGroupShowLineSubtotalsTaxExcluded(Boolean groupShowLineSubtotalsTaxExcluded){
        this.groupShowLineSubtotalsTaxExcluded = groupShowLineSubtotalsTaxExcluded ;
        this.modify("group_show_line_subtotals_tax_excluded",groupShowLineSubtotalsTaxExcluded);
    }

    /**
     * 设置 [面试表单]
     */
    public void setModuleHrRecruitmentSurvey(Boolean moduleHrRecruitmentSurvey){
        this.moduleHrRecruitmentSurvey = moduleHrRecruitmentSurvey ;
        this.modify("module_hr_recruitment_survey",moduleHrRecruitmentSurvey);
    }

    /**
     * 设置 [工单]
     */
    public void setModuleMrpWorkorder(Boolean moduleMrpWorkorder){
        this.moduleMrpWorkorder = moduleMrpWorkorder ;
        this.modify("module_mrp_workorder",moduleMrpWorkorder);
    }

    /**
     * 设置 [集成卡支付]
     */
    public void setModulePosMercury(Boolean modulePosMercury){
        this.modulePosMercury = modulePosMercury ;
        this.modify("module_pos_mercury",modulePosMercury);
    }

    /**
     * 设置 [默认模板]
     */
    public void setDefaultSaleOrderTemplateId(Long defaultSaleOrderTemplateId){
        this.defaultSaleOrderTemplateId = defaultSaleOrderTemplateId ;
        this.modify("default_sale_order_template_id",defaultSaleOrderTemplateId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [摘要邮件]
     */
    public void setDigestId(Long digestId){
        this.digestId = digestId ;
        this.modify("digest_id",digestId);
    }

    /**
     * 设置 [EMail模板]
     */
    public void setTemplateId(Long templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }

    /**
     * 设置 [模板]
     */
    public void setChartTemplateId(Long chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }

    /**
     * 设置 [押金产品]
     */
    public void setDepositDefaultProductId(Long depositDefaultProductId){
        this.depositDefaultProductId = depositDefaultProductId ;
        this.modify("deposit_default_product_id",depositDefaultProductId);
    }

    /**
     * 设置 [用作通过注册创建的新用户的模版]
     */
    public void setAuthSignupTemplateUserId(Long authSignupTemplateUserId){
        this.authSignupTemplateUserId = authSignupTemplateUserId ;
        this.modify("auth_signup_template_user_id",authSignupTemplateUserId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



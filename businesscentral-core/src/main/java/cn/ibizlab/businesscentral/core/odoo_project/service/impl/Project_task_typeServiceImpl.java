package cn.ibizlab.businesscentral.core.odoo_project.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.businesscentral.core.odoo_project.filter.Project_task_typeSearchContext;
import cn.ibizlab.businesscentral.core.odoo_project.service.IProject_task_typeService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_project.mapper.Project_task_typeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[任务阶段] 服务对象接口实现
 */
@Slf4j
@Service("Project_task_typeServiceImpl")
public class Project_task_typeServiceImpl extends EBSServiceImpl<Project_task_typeMapper, Project_task_type> implements IProject_task_typeService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_taskService projectTaskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_templateService mailTemplateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "project.task.type" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Project_task_type et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProject_task_typeService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Project_task_type> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Project_task_type et) {
        Project_task_type old = new Project_task_type() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProject_task_typeService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IProject_task_typeService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Project_task_type> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        if(!ObjectUtils.isEmpty(projectTaskService.selectByStageId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Project_task]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        if(!ObjectUtils.isEmpty(projectTaskService.selectByStageId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Project_task]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Project_task_type get(Long key) {
        Project_task_type et = getById(key);
        if(et==null){
            et=new Project_task_type();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Project_task_type getDraft(Project_task_type et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Project_task_type et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Project_task_type et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Project_task_type et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Project_task_type> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Project_task_type> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Project_task_type> selectByMailTemplateId(Long id) {
        return baseMapper.selectByMailTemplateId(id);
    }
    @Override
    public void resetByMailTemplateId(Long id) {
        this.update(new UpdateWrapper<Project_task_type>().set("mail_template_id",null).eq("mail_template_id",id));
    }

    @Override
    public void resetByMailTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Project_task_type>().set("mail_template_id",null).in("mail_template_id",ids));
    }

    @Override
    public void removeByMailTemplateId(Long id) {
        this.remove(new QueryWrapper<Project_task_type>().eq("mail_template_id",id));
    }

	@Override
    public List<Project_task_type> selectByRatingTemplateId(Long id) {
        return baseMapper.selectByRatingTemplateId(id);
    }
    @Override
    public void resetByRatingTemplateId(Long id) {
        this.update(new UpdateWrapper<Project_task_type>().set("rating_template_id",null).eq("rating_template_id",id));
    }

    @Override
    public void resetByRatingTemplateId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Project_task_type>().set("rating_template_id",null).in("rating_template_id",ids));
    }

    @Override
    public void removeByRatingTemplateId(Long id) {
        this.remove(new QueryWrapper<Project_task_type>().eq("rating_template_id",id));
    }

	@Override
    public List<Project_task_type> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Project_task_type>().eq("create_uid",id));
    }

	@Override
    public List<Project_task_type> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Project_task_type>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Project_task_type> searchDefault(Project_task_typeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Project_task_type> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Project_task_type>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Project_task_type et){
        //实体关系[DER1N_PROJECT_TASK_TYPE__MAIL_TEMPLATE__MAIL_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getMailTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooMailTemplate=et.getOdooMailTemplate();
            if(ObjectUtils.isEmpty(odooMailTemplate)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template majorEntity=mailTemplateService.get(et.getMailTemplateId());
                et.setOdooMailTemplate(majorEntity);
                odooMailTemplate=majorEntity;
            }
            et.setMailTemplateIdText(odooMailTemplate.getName());
        }
        //实体关系[DER1N_PROJECT_TASK_TYPE__MAIL_TEMPLATE__RATING_TEMPLATE_ID]
        if(!ObjectUtils.isEmpty(et.getRatingTemplateId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooRatingTemplate=et.getOdooRatingTemplate();
            if(ObjectUtils.isEmpty(odooRatingTemplate)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template majorEntity=mailTemplateService.get(et.getRatingTemplateId());
                et.setOdooRatingTemplate(majorEntity);
                odooRatingTemplate=majorEntity;
            }
            et.setRatingTemplateIdText(odooRatingTemplate.getName());
        }
        //实体关系[DER1N_PROJECT_TASK_TYPE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_PROJECT_TASK_TYPE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Project_task_type> getProjectTaskTypeByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Project_task_type> getProjectTaskTypeByEntities(List<Project_task_type> entities) {
        List ids =new ArrayList();
        for(Project_task_type entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




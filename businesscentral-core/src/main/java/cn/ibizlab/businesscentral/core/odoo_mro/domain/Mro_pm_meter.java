package cn.ibizlab.businesscentral.core.odoo_mro.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[Asset Meters]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MRO_PM_METER",resultMap = "Mro_pm_meterResultMap")
public class Mro_pm_meter extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * Min Utilization (per day)
     */
    @DEField(name = "min_utilization")
    @TableField(value = "min_utilization")
    @JSONField(name = "min_utilization")
    @JsonProperty("min_utilization")
    private Double minUtilization;
    /**
     * 日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * Reading Type
     */
    @DEField(name = "reading_type")
    @TableField(value = "reading_type")
    @JSONField(name = "reading_type")
    @JsonProperty("reading_type")
    private String readingType;
    /**
     * 值
     */
    @TableField(exist = false)
    @JSONField(name = "value")
    @JsonProperty("value")
    private Double value;
    /**
     * Meters
     */
    @TableField(exist = false)
    @JSONField(name = "meter_line_ids")
    @JsonProperty("meter_line_ids")
    private String meterLineIds;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * View Line
     */
    @TableField(exist = false)
    @JSONField(name = "view_line_ids")
    @JsonProperty("view_line_ids")
    private String viewLineIds;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * Utilization (per day)
     */
    @TableField(exist = false)
    @JSONField(name = "utilization")
    @JsonProperty("utilization")
    private Double utilization;
    /**
     * New value
     */
    @DEField(name = "new_value")
    @TableField(value = "new_value")
    @JSONField(name = "new_value")
    @JsonProperty("new_value")
    private Double newValue;
    /**
     * Averaging time (days)
     */
    @DEField(name = "av_time")
    @TableField(value = "av_time")
    @JSONField(name = "av_time")
    @JsonProperty("av_time")
    private Double avTime;
    /**
     * Total Value
     */
    @TableField(exist = false)
    @JSONField(name = "total_value")
    @JsonProperty("total_value")
    private Double totalValue;
    /**
     * Meter
     */
    @TableField(exist = false)
    @JSONField(name = "name_text")
    @JsonProperty("name_text")
    private String nameText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 单位
     */
    @TableField(exist = false)
    @JSONField(name = "meter_uom")
    @JsonProperty("meter_uom")
    private Long meterUom;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * Asset
     */
    @TableField(exist = false)
    @JSONField(name = "asset_id_text")
    @JsonProperty("asset_id_text")
    private String assetIdText;
    /**
     * Ratio to Source
     */
    @TableField(exist = false)
    @JSONField(name = "parent_ratio_id_text")
    @JsonProperty("parent_ratio_id_text")
    private String parentRatioIdText;
    /**
     * Source Meter
     */
    @DEField(name = "parent_meter_id")
    @TableField(value = "parent_meter_id")
    @JSONField(name = "parent_meter_id")
    @JsonProperty("parent_meter_id")
    private Long parentMeterId;
    /**
     * Ratio to Source
     */
    @DEField(name = "parent_ratio_id")
    @TableField(value = "parent_ratio_id")
    @JSONField(name = "parent_ratio_id")
    @JsonProperty("parent_ratio_id")
    private Long parentRatioId;
    /**
     * Meter
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private Long name;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * Asset
     */
    @DEField(name = "asset_id")
    @TableField(value = "asset_id")
    @JSONField(name = "asset_id")
    @JsonProperty("asset_id")
    private Long assetId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_asset.domain.Asset_asset odooAsset;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_ratio odooParentRatio;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter odooParentMeter;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_parameter odooName;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [Min Utilization (per day)]
     */
    public void setMinUtilization(Double minUtilization){
        this.minUtilization = minUtilization ;
        this.modify("min_utilization",minUtilization);
    }

    /**
     * 设置 [Reading Type]
     */
    public void setReadingType(String readingType){
        this.readingType = readingType ;
        this.modify("reading_type",readingType);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [New value]
     */
    public void setNewValue(Double newValue){
        this.newValue = newValue ;
        this.modify("new_value",newValue);
    }

    /**
     * 设置 [Averaging time (days)]
     */
    public void setAvTime(Double avTime){
        this.avTime = avTime ;
        this.modify("av_time",avTime);
    }

    /**
     * 设置 [Source Meter]
     */
    public void setParentMeterId(Long parentMeterId){
        this.parentMeterId = parentMeterId ;
        this.modify("parent_meter_id",parentMeterId);
    }

    /**
     * 设置 [Ratio to Source]
     */
    public void setParentRatioId(Long parentRatioId){
        this.parentRatioId = parentRatioId ;
        this.modify("parent_ratio_id",parentRatioId);
    }

    /**
     * 设置 [Meter]
     */
    public void setName(Long name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [Asset]
     */
    public void setAssetId(Long assetId){
        this.assetId = assetId ;
        this.modify("asset_id",assetId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



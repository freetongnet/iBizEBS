package cn.ibizlab.businesscentral.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[calendar_alarm_manager] 服务对象接口
 */
@Component
public class calendar_alarm_managerFallback implements calendar_alarm_managerFeignClient{


    public Calendar_alarm_manager update(Long id, Calendar_alarm_manager calendar_alarm_manager){
            return null;
     }
    public Boolean updateBatch(List<Calendar_alarm_manager> calendar_alarm_managers){
            return false;
     }



    public Calendar_alarm_manager get(Long id){
            return null;
     }


    public Page<Calendar_alarm_manager> search(Calendar_alarm_managerSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Calendar_alarm_manager create(Calendar_alarm_manager calendar_alarm_manager){
            return null;
     }
    public Boolean createBatch(List<Calendar_alarm_manager> calendar_alarm_managers){
            return false;
     }


    public Page<Calendar_alarm_manager> select(){
            return null;
     }

    public Calendar_alarm_manager getDraft(){
            return null;
    }



    public Boolean checkKey(Calendar_alarm_manager calendar_alarm_manager){
            return false;
     }


    public Boolean save(Calendar_alarm_manager calendar_alarm_manager){
            return false;
     }
    public Boolean saveBatch(List<Calendar_alarm_manager> calendar_alarm_managers){
            return false;
     }

    public Page<Calendar_alarm_manager> searchDefault(Calendar_alarm_managerSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_web_tour.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.businesscentral.core.odoo_web_tour.filter.Web_tour_tourSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Web_tour_tourMapper extends BaseMapper<Web_tour_tour>{

    Page<Web_tour_tour> searchDefault(IPage page, @Param("srf") Web_tour_tourSearchContext context, @Param("ew") Wrapper<Web_tour_tour> wrapper) ;
    @Override
    Web_tour_tour selectById(Serializable id);
    @Override
    int insert(Web_tour_tour entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Web_tour_tour entity);
    @Override
    int update(@Param(Constants.ENTITY) Web_tour_tour entity, @Param("ew") Wrapper<Web_tour_tour> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Web_tour_tour> selectByUserId(@Param("id") Serializable id) ;


}

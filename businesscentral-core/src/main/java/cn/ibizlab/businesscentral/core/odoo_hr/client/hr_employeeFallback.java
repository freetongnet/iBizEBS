package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_employeeSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_employee] 服务对象接口
 */
@Component
public class hr_employeeFallback implements hr_employeeFeignClient{


    public Hr_employee update(Long id, Hr_employee hr_employee){
            return null;
     }
    public Boolean updateBatch(List<Hr_employee> hr_employees){
            return false;
     }



    public Page<Hr_employee> search(Hr_employeeSearchContext context){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Hr_employee create(Hr_employee hr_employee){
            return null;
     }
    public Boolean createBatch(List<Hr_employee> hr_employees){
            return false;
     }

    public Hr_employee get(Long id){
            return null;
     }


    public Page<Hr_employee> select(){
            return null;
     }

    public Hr_employee getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_employee hr_employee){
            return false;
     }


    public Boolean save(Hr_employee hr_employee){
            return false;
     }
    public Boolean saveBatch(List<Hr_employee> hr_employees){
            return false;
     }

    public Page<Hr_employee> searchDefault(Hr_employeeSearchContext context){
            return null;
     }


    public Page<Hr_employee> searchMaster(Hr_employeeSearchContext context){
            return null;
     }


}

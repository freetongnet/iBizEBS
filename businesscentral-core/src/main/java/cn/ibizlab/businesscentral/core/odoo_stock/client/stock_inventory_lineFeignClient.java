package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory_line;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_inventory_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_inventory_line] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-inventory-line", fallback = stock_inventory_lineFallback.class)
public interface stock_inventory_lineFeignClient {




    @RequestMapping(method = RequestMethod.PUT, value = "/stock_inventory_lines/{id}")
    Stock_inventory_line update(@PathVariable("id") Long id,@RequestBody Stock_inventory_line stock_inventory_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_inventory_lines/batch")
    Boolean updateBatch(@RequestBody List<Stock_inventory_line> stock_inventory_lines);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines")
    Stock_inventory_line create(@RequestBody Stock_inventory_line stock_inventory_line);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/batch")
    Boolean createBatch(@RequestBody List<Stock_inventory_line> stock_inventory_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventory_lines/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventory_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/search")
    Page<Stock_inventory_line> search(@RequestBody Stock_inventory_lineSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_inventory_lines/{id}")
    Stock_inventory_line get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_inventory_lines/select")
    Page<Stock_inventory_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_inventory_lines/getdraft")
    Stock_inventory_line getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/checkkey")
    Boolean checkKey(@RequestBody Stock_inventory_line stock_inventory_line);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/save")
    Boolean save(@RequestBody Stock_inventory_line stock_inventory_line);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_inventory_line> stock_inventory_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/searchdefault")
    Page<Stock_inventory_line> searchDefault(@RequestBody Stock_inventory_lineSearchContext context);


}

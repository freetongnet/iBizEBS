package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_cost] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-fleet:odoo-fleet}", contextId = "fleet-vehicle-cost", fallback = fleet_vehicle_costFallback.class)
public interface fleet_vehicle_costFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/search")
    Page<Fleet_vehicle_cost> search(@RequestBody Fleet_vehicle_costSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs")
    Fleet_vehicle_cost create(@RequestBody Fleet_vehicle_cost fleet_vehicle_cost);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_cost> fleet_vehicle_costs);


    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_costs/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_costs/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_costs/{id}")
    Fleet_vehicle_cost update(@PathVariable("id") Long id,@RequestBody Fleet_vehicle_cost fleet_vehicle_cost);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_costs/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_cost> fleet_vehicle_costs);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_costs/{id}")
    Fleet_vehicle_cost get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_costs/select")
    Page<Fleet_vehicle_cost> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_costs/getdraft")
    Fleet_vehicle_cost getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/checkkey")
    Boolean checkKey(@RequestBody Fleet_vehicle_cost fleet_vehicle_cost);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/save")
    Boolean save(@RequestBody Fleet_vehicle_cost fleet_vehicle_cost);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/savebatch")
    Boolean saveBatch(@RequestBody List<Fleet_vehicle_cost> fleet_vehicle_costs);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/searchdefault")
    Page<Fleet_vehicle_cost> searchDefault(@RequestBody Fleet_vehicle_costSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leaveSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_leave] 服务对象接口
 */
@Component
public class hr_leaveFallback implements hr_leaveFeignClient{


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Hr_leave create(Hr_leave hr_leave){
            return null;
     }
    public Boolean createBatch(List<Hr_leave> hr_leaves){
            return false;
     }

    public Page<Hr_leave> search(Hr_leaveSearchContext context){
            return null;
     }


    public Hr_leave get(Long id){
            return null;
     }


    public Hr_leave update(Long id, Hr_leave hr_leave){
            return null;
     }
    public Boolean updateBatch(List<Hr_leave> hr_leaves){
            return false;
     }


    public Page<Hr_leave> select(){
            return null;
     }

    public Hr_leave getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_leave hr_leave){
            return false;
     }


    public Boolean save(Hr_leave hr_leave){
            return false;
     }
    public Boolean saveBatch(List<Hr_leave> hr_leaves){
            return false;
     }

    public Page<Hr_leave> searchDefault(Hr_leaveSearchContext context){
            return null;
     }


}

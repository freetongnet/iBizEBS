package cn.ibizlab.businesscentral.core.odoo_survey.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[调查用户输入明细]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "SURVEY_USER_INPUT_LINE",resultMap = "Survey_user_input_lineResultMap")
public class Survey_user_input_line extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 回复日期
     */
    @DEField(name = "value_date")
    @TableField(value = "value_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "value_date" , format="yyyy-MM-dd")
    @JsonProperty("value_date")
    private Timestamp valueDate;
    /**
     * 自由文本答案
     */
    @DEField(name = "value_free_text")
    @TableField(value = "value_free_text")
    @JSONField(name = "value_free_text")
    @JsonProperty("value_free_text")
    private String valueFreeText;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 创建日期
     */
    @DEField(name = "date_create")
    @TableField(value = "date_create")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_create" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_create")
    private Timestamp dateCreate;
    /**
     * 忽略
     */
    @TableField(value = "skipped")
    @JSONField(name = "skipped")
    @JsonProperty("skipped")
    private Boolean skipped;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 这个选项分配的分数
     */
    @DEField(name = "quizz_mark")
    @TableField(value = "quizz_mark")
    @JSONField(name = "quizz_mark")
    @JsonProperty("quizz_mark")
    private Double quizzMark;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 回复类型
     */
    @DEField(name = "answer_type")
    @TableField(value = "answer_type")
    @JSONField(name = "answer_type")
    @JsonProperty("answer_type")
    private String answerType;
    /**
     * 文本答案
     */
    @DEField(name = "value_text")
    @TableField(value = "value_text")
    @JSONField(name = "value_text")
    @JsonProperty("value_text")
    private String valueText;
    /**
     * 数字答案
     */
    @DEField(name = "value_number")
    @TableField(value = "value_number")
    @JSONField(name = "value_number")
    @JsonProperty("value_number")
    private Double valueNumber;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 页
     */
    @TableField(exist = false)
    @JSONField(name = "page_id")
    @JsonProperty("page_id")
    private Long pageId;
    /**
     * 用户输入
     */
    @DEField(name = "user_input_id")
    @TableField(value = "user_input_id")
    @JSONField(name = "user_input_id")
    @JsonProperty("user_input_id")
    private Long userInputId;
    /**
     * 建议答案
     */
    @DEField(name = "value_suggested")
    @TableField(value = "value_suggested")
    @JSONField(name = "value_suggested")
    @JsonProperty("value_suggested")
    private Long valueSuggested;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 答案行
     */
    @DEField(name = "value_suggested_row")
    @TableField(value = "value_suggested_row")
    @JSONField(name = "value_suggested_row")
    @JsonProperty("value_suggested_row")
    private Long valueSuggestedRow;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 疑问
     */
    @DEField(name = "question_id")
    @TableField(value = "question_id")
    @JSONField(name = "question_id")
    @JsonProperty("question_id")
    private Long questionId;
    /**
     * 问卷
     */
    @DEField(name = "survey_id")
    @TableField(value = "survey_id")
    @JSONField(name = "survey_id")
    @JsonProperty("survey_id")
    private Long surveyId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_label odooValue;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_label odooValueSuggested;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_question odooQuestion;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_survey odooSurvey;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_user_input odooUserInput;



    /**
     * 设置 [回复日期]
     */
    public void setValueDate(Timestamp valueDate){
        this.valueDate = valueDate ;
        this.modify("value_date",valueDate);
    }

    /**
     * 格式化日期 [回复日期]
     */
    public String formatValueDate(){
        if (this.valueDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(valueDate);
    }
    /**
     * 设置 [自由文本答案]
     */
    public void setValueFreeText(String valueFreeText){
        this.valueFreeText = valueFreeText ;
        this.modify("value_free_text",valueFreeText);
    }

    /**
     * 设置 [创建日期]
     */
    public void setDateCreate(Timestamp dateCreate){
        this.dateCreate = dateCreate ;
        this.modify("date_create",dateCreate);
    }

    /**
     * 格式化日期 [创建日期]
     */
    public String formatDateCreate(){
        if (this.dateCreate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateCreate);
    }
    /**
     * 设置 [忽略]
     */
    public void setSkipped(Boolean skipped){
        this.skipped = skipped ;
        this.modify("skipped",skipped);
    }

    /**
     * 设置 [这个选项分配的分数]
     */
    public void setQuizzMark(Double quizzMark){
        this.quizzMark = quizzMark ;
        this.modify("quizz_mark",quizzMark);
    }

    /**
     * 设置 [回复类型]
     */
    public void setAnswerType(String answerType){
        this.answerType = answerType ;
        this.modify("answer_type",answerType);
    }

    /**
     * 设置 [文本答案]
     */
    public void setValueText(String valueText){
        this.valueText = valueText ;
        this.modify("value_text",valueText);
    }

    /**
     * 设置 [数字答案]
     */
    public void setValueNumber(Double valueNumber){
        this.valueNumber = valueNumber ;
        this.modify("value_number",valueNumber);
    }

    /**
     * 设置 [用户输入]
     */
    public void setUserInputId(Long userInputId){
        this.userInputId = userInputId ;
        this.modify("user_input_id",userInputId);
    }

    /**
     * 设置 [建议答案]
     */
    public void setValueSuggested(Long valueSuggested){
        this.valueSuggested = valueSuggested ;
        this.modify("value_suggested",valueSuggested);
    }

    /**
     * 设置 [答案行]
     */
    public void setValueSuggestedRow(Long valueSuggestedRow){
        this.valueSuggestedRow = valueSuggestedRow ;
        this.modify("value_suggested_row",valueSuggestedRow);
    }

    /**
     * 设置 [疑问]
     */
    public void setQuestionId(Long questionId){
        this.questionId = questionId ;
        this.modify("question_id",questionId);
    }

    /**
     * 设置 [问卷]
     */
    public void setSurveyId(Long surveyId){
        this.surveyId = surveyId ;
        this.modify("survey_id",surveyId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



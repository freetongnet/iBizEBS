package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_moderationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_moderation] 服务对象接口
 */
public interface IMail_moderationService extends IService<Mail_moderation>{

    boolean create(Mail_moderation et) ;
    void createBatch(List<Mail_moderation> list) ;
    boolean update(Mail_moderation et) ;
    void updateBatch(List<Mail_moderation> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_moderation get(Long key) ;
    Mail_moderation getDraft(Mail_moderation et) ;
    boolean checkKey(Mail_moderation et) ;
    boolean save(Mail_moderation et) ;
    void saveBatch(List<Mail_moderation> list) ;
    Page<Mail_moderation> searchDefault(Mail_moderationSearchContext context) ;
    List<Mail_moderation> selectByChannelId(Long id);
    void resetByChannelId(Long id);
    void resetByChannelId(Collection<Long> ids);
    void removeByChannelId(Long id);
    List<Mail_moderation> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_moderation> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_moderation> getMailModerationByIds(List<Long> ids) ;
    List<Mail_moderation> getMailModerationByEntities(List<Mail_moderation> entities) ;
}



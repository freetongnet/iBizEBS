package cn.ibizlab.businesscentral.core.odoo_sms.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.businesscentral.core.odoo_sms.filter.Sms_apiSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Sms_api] 服务对象接口
 */
public interface ISms_apiService extends IService<Sms_api>{

    boolean create(Sms_api et) ;
    void createBatch(List<Sms_api> list) ;
    boolean update(Sms_api et) ;
    void updateBatch(List<Sms_api> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Sms_api get(Long key) ;
    Sms_api getDraft(Sms_api et) ;
    boolean checkKey(Sms_api et) ;
    boolean save(Sms_api et) ;
    void saveBatch(List<Sms_api> list) ;
    Page<Sms_api> searchDefault(Sms_apiSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



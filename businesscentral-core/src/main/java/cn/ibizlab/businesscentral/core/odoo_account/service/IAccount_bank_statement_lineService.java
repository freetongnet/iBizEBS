package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_bank_statement_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_bank_statement_line] 服务对象接口
 */
public interface IAccount_bank_statement_lineService extends IService<Account_bank_statement_line>{

    boolean create(Account_bank_statement_line et) ;
    void createBatch(List<Account_bank_statement_line> list) ;
    boolean update(Account_bank_statement_line et) ;
    void updateBatch(List<Account_bank_statement_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_bank_statement_line get(Long key) ;
    Account_bank_statement_line getDraft(Account_bank_statement_line et) ;
    boolean checkKey(Account_bank_statement_line et) ;
    boolean save(Account_bank_statement_line et) ;
    void saveBatch(List<Account_bank_statement_line> list) ;
    Page<Account_bank_statement_line> searchDefault(Account_bank_statement_lineSearchContext context) ;
    List<Account_bank_statement_line> selectByAccountId(Long id);
    void resetByAccountId(Long id);
    void resetByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_bank_statement_line> selectByStatementId(Long id);
    void removeByStatementId(Collection<Long> ids);
    void removeByStatementId(Long id);
    List<Account_bank_statement_line> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Account_bank_statement_line> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_bank_statement_line> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_bank_statement_line> selectByBankAccountId(Long id);
    void resetByBankAccountId(Long id);
    void resetByBankAccountId(Collection<Long> ids);
    void removeByBankAccountId(Long id);
    List<Account_bank_statement_line> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Account_bank_statement_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_bank_statement_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_bank_statement_line> getAccountBankStatementLineByIds(List<Long> ids) ;
    List<Account_bank_statement_line> getAccountBankStatementLineByEntities(List<Account_bank_statement_line> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_event.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_type_mailSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Event_type_mail] 服务对象接口
 */
public interface IEvent_type_mailService extends IService<Event_type_mail>{

    boolean create(Event_type_mail et) ;
    void createBatch(List<Event_type_mail> list) ;
    boolean update(Event_type_mail et) ;
    void updateBatch(List<Event_type_mail> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Event_type_mail get(Long key) ;
    Event_type_mail getDraft(Event_type_mail et) ;
    boolean checkKey(Event_type_mail et) ;
    boolean save(Event_type_mail et) ;
    void saveBatch(List<Event_type_mail> list) ;
    Page<Event_type_mail> searchDefault(Event_type_mailSearchContext context) ;
    List<Event_type_mail> selectByEventTypeId(Long id);
    void removeByEventTypeId(Collection<Long> ids);
    void removeByEventTypeId(Long id);
    List<Event_type_mail> selectByTemplateId(Long id);
    List<Event_type_mail> selectByTemplateId(Collection<Long> ids);
    void removeByTemplateId(Long id);
    List<Event_type_mail> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Event_type_mail> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Event_type_mail> getEventTypeMailByIds(List<Long> ids) ;
    List<Event_type_mail> getEventTypeMailByEntities(List<Event_type_mail> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_ir.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_property;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_propertySearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Ir_propertyMapper extends BaseMapper<Ir_property>{

    Page<Ir_property> searchDefault(IPage page, @Param("srf") Ir_propertySearchContext context, @Param("ew") Wrapper<Ir_property> wrapper) ;
    @Override
    Ir_property selectById(Serializable id);
    @Override
    int insert(Ir_property entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Ir_property entity);
    @Override
    int update(@Param(Constants.ENTITY) Ir_property entity, @Param("ew") Wrapper<Ir_property> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Ir_property> selectByFieldsId(@Param("id") Serializable id) ;

    List<Ir_property> selectByCompanyId(@Param("id") Serializable id) ;


}

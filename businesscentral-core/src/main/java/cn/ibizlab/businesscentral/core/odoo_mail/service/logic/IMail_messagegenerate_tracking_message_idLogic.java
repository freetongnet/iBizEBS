package cn.ibizlab.businesscentral.core.odoo_mail.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message;

/**
 * 关系型数据实体[generate_tracking_message_id] 对象
 */
public interface IMail_messagegenerate_tracking_message_idLogic {

    void execute(Mail_message et) ;

}

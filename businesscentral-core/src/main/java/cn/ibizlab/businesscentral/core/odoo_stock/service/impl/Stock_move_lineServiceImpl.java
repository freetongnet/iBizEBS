package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_move_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_move_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[产品移动(移库明细)] 服务对象接口实现
 */
@Slf4j
@Service("Stock_move_lineServiceImpl")
public class Stock_move_lineServiceImpl extends EBSServiceImpl<Stock_move_lineMapper, Stock_move_line> implements IStock_move_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService mrpProductionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_workorderService mrpWorkorderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService stockLocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_levelService stockPackageLevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_production_lotService stockProductionLotService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quant_packageService stockQuantPackageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.move.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_move_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_move_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_move_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_move_line et) {
        Stock_move_line old = new Stock_move_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_move_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_move_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_move_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_move_line get(Long key) {
        Stock_move_line et = getById(key);
        if(et==null){
            et=new Stock_move_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_move_line getDraft(Stock_move_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_move_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_move_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_move_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_move_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_move_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_move_line> selectByProductionId(Long id) {
        return baseMapper.selectByProductionId(id);
    }
    @Override
    public void resetByProductionId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("production_id",null).eq("production_id",id));
    }

    @Override
    public void resetByProductionId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("production_id",null).in("production_id",ids));
    }

    @Override
    public void removeByProductionId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("production_id",id));
    }

	@Override
    public List<Stock_move_line> selectByWorkorderId(Long id) {
        return baseMapper.selectByWorkorderId(id);
    }
    @Override
    public void resetByWorkorderId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("workorder_id",null).eq("workorder_id",id));
    }

    @Override
    public void resetByWorkorderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("workorder_id",null).in("workorder_id",ids));
    }

    @Override
    public void removeByWorkorderId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("workorder_id",id));
    }

	@Override
    public List<Stock_move_line> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void removeByProductId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Stock_move_line>().in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("product_id",id));
    }

	@Override
    public List<Stock_move_line> selectByOwnerId(Long id) {
        return baseMapper.selectByOwnerId(id);
    }
    @Override
    public void resetByOwnerId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("owner_id",null).eq("owner_id",id));
    }

    @Override
    public void resetByOwnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("owner_id",null).in("owner_id",ids));
    }

    @Override
    public void removeByOwnerId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("owner_id",id));
    }

	@Override
    public List<Stock_move_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("create_uid",id));
    }

	@Override
    public List<Stock_move_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("write_uid",id));
    }

	@Override
    public List<Stock_move_line> selectByLocationDestId(Long id) {
        return baseMapper.selectByLocationDestId(id);
    }
    @Override
    public void resetByLocationDestId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("location_dest_id",null).eq("location_dest_id",id));
    }

    @Override
    public void resetByLocationDestId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("location_dest_id",null).in("location_dest_id",ids));
    }

    @Override
    public void removeByLocationDestId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("location_dest_id",id));
    }

	@Override
    public List<Stock_move_line> selectByLocationId(Long id) {
        return baseMapper.selectByLocationId(id);
    }
    @Override
    public void resetByLocationId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("location_id",null).eq("location_id",id));
    }

    @Override
    public void resetByLocationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("location_id",null).in("location_id",ids));
    }

    @Override
    public void removeByLocationId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("location_id",id));
    }

	@Override
    public List<Stock_move_line> selectByMoveId(Long id) {
        return baseMapper.selectByMoveId(id);
    }
    @Override
    public void resetByMoveId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("move_id",null).eq("move_id",id));
    }

    @Override
    public void resetByMoveId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("move_id",null).in("move_id",ids));
    }

    @Override
    public void removeByMoveId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("move_id",id));
    }

	@Override
    public List<Stock_move_line> selectByPackageLevelId(Long id) {
        return baseMapper.selectByPackageLevelId(id);
    }
    @Override
    public void resetByPackageLevelId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("package_level_id",null).eq("package_level_id",id));
    }

    @Override
    public void resetByPackageLevelId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("package_level_id",null).in("package_level_id",ids));
    }

    @Override
    public void removeByPackageLevelId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("package_level_id",id));
    }

	@Override
    public List<Stock_move_line> selectByPickingId(Long id) {
        return baseMapper.selectByPickingId(id);
    }
    @Override
    public void resetByPickingId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("picking_id",null).eq("picking_id",id));
    }

    @Override
    public void resetByPickingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("picking_id",null).in("picking_id",ids));
    }

    @Override
    public void removeByPickingId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("picking_id",id));
    }

	@Override
    public List<Stock_move_line> selectByLotId(Long id) {
        return baseMapper.selectByLotId(id);
    }
    @Override
    public void resetByLotId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("lot_id",null).eq("lot_id",id));
    }

    @Override
    public void resetByLotId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("lot_id",null).in("lot_id",ids));
    }

    @Override
    public void removeByLotId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("lot_id",id));
    }

	@Override
    public List<Stock_move_line> selectByLotProducedId(Long id) {
        return baseMapper.selectByLotProducedId(id);
    }
    @Override
    public void resetByLotProducedId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("lot_produced_id",null).eq("lot_produced_id",id));
    }

    @Override
    public void resetByLotProducedId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("lot_produced_id",null).in("lot_produced_id",ids));
    }

    @Override
    public void removeByLotProducedId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("lot_produced_id",id));
    }

	@Override
    public List<Stock_move_line> selectByPackageId(Long id) {
        return baseMapper.selectByPackageId(id);
    }
    @Override
    public List<Stock_move_line> selectByPackageId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_move_line>().in("id",ids));
    }

    @Override
    public void removeByPackageId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("package_id",id));
    }

	@Override
    public List<Stock_move_line> selectByResultPackageId(Long id) {
        return baseMapper.selectByResultPackageId(id);
    }
    @Override
    public List<Stock_move_line> selectByResultPackageId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Stock_move_line>().in("id",ids));
    }

    @Override
    public void removeByResultPackageId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("result_package_id",id));
    }

	@Override
    public List<Stock_move_line> selectByProductUomId(Long id) {
        return baseMapper.selectByProductUomId(id);
    }
    @Override
    public void resetByProductUomId(Long id) {
        this.update(new UpdateWrapper<Stock_move_line>().set("product_uom_id",null).eq("product_uom_id",id));
    }

    @Override
    public void resetByProductUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_move_line>().set("product_uom_id",null).in("product_uom_id",ids));
    }

    @Override
    public void removeByProductUomId(Long id) {
        this.remove(new QueryWrapper<Stock_move_line>().eq("product_uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_move_line> searchDefault(Stock_move_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_move_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_move_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_move_line et){
        //实体关系[DER1N_STOCK_MOVE_LINE__MRP_PRODUCTION__PRODUCTION_ID]
        if(!ObjectUtils.isEmpty(et.getProductionId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production odooProduction=et.getOdooProduction();
            if(ObjectUtils.isEmpty(odooProduction)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_production majorEntity=mrpProductionService.get(et.getProductionId());
                et.setOdooProduction(majorEntity);
                odooProduction=majorEntity;
            }
            et.setProductionIdText(odooProduction.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__MRP_WORKORDER__WORKORDER_ID]
        if(!ObjectUtils.isEmpty(et.getWorkorderId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder odooWorkorder=et.getOdooWorkorder();
            if(ObjectUtils.isEmpty(odooWorkorder)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workorder majorEntity=mrpWorkorderService.get(et.getWorkorderId());
                et.setOdooWorkorder(majorEntity);
                odooWorkorder=majorEntity;
            }
            et.setWorkorderIdText(odooWorkorder.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
            et.setTracking(odooProduct.getTracking());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__RES_PARTNER__OWNER_ID]
        if(!ObjectUtils.isEmpty(et.getOwnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOwner=et.getOdooOwner();
            if(ObjectUtils.isEmpty(odooOwner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getOwnerId());
                et.setOdooOwner(majorEntity);
                odooOwner=majorEntity;
            }
            et.setOwnerIdText(odooOwner.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__STOCK_LOCATION__LOCATION_DEST_ID]
        if(!ObjectUtils.isEmpty(et.getLocationDestId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocationDest=et.getOdooLocationDest();
            if(ObjectUtils.isEmpty(odooLocationDest)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationDestId());
                et.setOdooLocationDest(majorEntity);
                odooLocationDest=majorEntity;
            }
            et.setLocationDestIdText(odooLocationDest.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__STOCK_LOCATION__LOCATION_ID]
        if(!ObjectUtils.isEmpty(et.getLocationId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooLocation=et.getOdooLocation();
            if(ObjectUtils.isEmpty(odooLocation)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location majorEntity=stockLocationService.get(et.getLocationId());
                et.setOdooLocation(majorEntity);
                odooLocation=majorEntity;
            }
            et.setLocationIdText(odooLocation.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__STOCK_MOVE__MOVE_ID]
        if(!ObjectUtils.isEmpty(et.getMoveId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move odooMove=et.getOdooMove();
            if(ObjectUtils.isEmpty(odooMove)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move majorEntity=stockMoveService.get(et.getMoveId());
                et.setOdooMove(majorEntity);
                odooMove=majorEntity;
            }
            et.setMoveIdText(odooMove.getName());
            et.setIsLocked(odooMove.getIsLocked());
            et.setIsInitialDemandEditable(odooMove.getIsInitialDemandEditable());
            et.setState(odooMove.getState());
            et.setReference(odooMove.getReference());
            et.setDoneMove(odooMove.getIsDone());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__STOCK_PICKING__PICKING_ID]
        if(!ObjectUtils.isEmpty(et.getPickingId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking odooPicking=et.getOdooPicking();
            if(ObjectUtils.isEmpty(odooPicking)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking majorEntity=stockPickingService.get(et.getPickingId());
                et.setOdooPicking(majorEntity);
                odooPicking=majorEntity;
            }
            et.setPickingIdText(odooPicking.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__STOCK_PRODUCTION_LOT__LOT_ID]
        if(!ObjectUtils.isEmpty(et.getLotId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLot=et.getOdooLot();
            if(ObjectUtils.isEmpty(odooLot)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot majorEntity=stockProductionLotService.get(et.getLotId());
                et.setOdooLot(majorEntity);
                odooLot=majorEntity;
            }
            et.setLotIdText(odooLot.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__STOCK_PRODUCTION_LOT__LOT_PRODUCED_ID]
        if(!ObjectUtils.isEmpty(et.getLotProducedId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot odooLotProduced=et.getOdooLotProduced();
            if(ObjectUtils.isEmpty(odooLotProduced)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot majorEntity=stockProductionLotService.get(et.getLotProducedId());
                et.setOdooLotProduced(majorEntity);
                odooLotProduced=majorEntity;
            }
            et.setLotProducedIdText(odooLotProduced.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__STOCK_QUANT_PACKAGE__PACKAGE_ID]
        if(!ObjectUtils.isEmpty(et.getPackageId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package odooPackage=et.getOdooPackage();
            if(ObjectUtils.isEmpty(odooPackage)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package majorEntity=stockQuantPackageService.get(et.getPackageId());
                et.setOdooPackage(majorEntity);
                odooPackage=majorEntity;
            }
            et.setPackageIdText(odooPackage.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__STOCK_QUANT_PACKAGE__RESULT_PACKAGE_ID]
        if(!ObjectUtils.isEmpty(et.getResultPackageId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package odooResultPackage=et.getOdooResultPackage();
            if(ObjectUtils.isEmpty(odooResultPackage)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant_package majorEntity=stockQuantPackageService.get(et.getResultPackageId());
                et.setOdooResultPackage(majorEntity);
                odooResultPackage=majorEntity;
            }
            et.setResultPackageIdText(odooResultPackage.getName());
        }
        //实体关系[DER1N_STOCK_MOVE_LINE__UOM_UOM__PRODUCT_UOM_ID]
        if(!ObjectUtils.isEmpty(et.getProductUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUomId());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomIdText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_move_line> getStockMoveLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_move_line> getStockMoveLineByEntities(List<Stock_move_line> entities) {
        List ids =new ArrayList();
        for(Stock_move_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




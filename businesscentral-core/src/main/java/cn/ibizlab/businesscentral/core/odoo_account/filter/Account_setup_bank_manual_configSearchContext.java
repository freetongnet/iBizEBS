package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_setup_bank_manual_config;
/**
 * 关系型数据实体[Account_setup_bank_manual_config] 查询条件对象
 */
@Slf4j
@Data
public class Account_setup_bank_manual_configSearchContext extends QueryWrapperContext<Account_setup_bank_manual_config> {

	private String n_create_or_link_option_eq;//[创建或链接选项]
	public void setN_create_or_link_option_eq(String n_create_or_link_option_eq) {
        this.n_create_or_link_option_eq = n_create_or_link_option_eq;
        if(!ObjectUtils.isEmpty(this.n_create_or_link_option_eq)){
            this.getSearchCond().eq("create_or_link_option", n_create_or_link_option_eq);
        }
    }
	private String n_related_acc_type_eq;//[科目类型]
	public void setN_related_acc_type_eq(String n_related_acc_type_eq) {
        this.n_related_acc_type_eq = n_related_acc_type_eq;
        if(!ObjectUtils.isEmpty(this.n_related_acc_type_eq)){
            this.getSearchCond().eq("related_acc_type", n_related_acc_type_eq);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_res_partner_bank_id_eq;//[业务伙伴银行账户]
	public void setN_res_partner_bank_id_eq(Long n_res_partner_bank_id_eq) {
        this.n_res_partner_bank_id_eq = n_res_partner_bank_id_eq;
        if(!ObjectUtils.isEmpty(this.n_res_partner_bank_id_eq)){
            this.getSearchCond().eq("res_partner_bank_id", n_res_partner_bank_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




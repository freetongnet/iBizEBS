package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_group;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_analytic_groupSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_analytic_group] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-analytic-group", fallback = account_analytic_groupFallback.class)
public interface account_analytic_groupFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_groups/{id}")
    Account_analytic_group update(@PathVariable("id") Long id,@RequestBody Account_analytic_group account_analytic_group);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_groups/batch")
    Boolean updateBatch(@RequestBody List<Account_analytic_group> account_analytic_groups);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_groups/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_groups/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups")
    Account_analytic_group create(@RequestBody Account_analytic_group account_analytic_group);

    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/batch")
    Boolean createBatch(@RequestBody List<Account_analytic_group> account_analytic_groups);


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_groups/{id}")
    Account_analytic_group get(@PathVariable("id") Long id);





    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/search")
    Page<Account_analytic_group> search(@RequestBody Account_analytic_groupSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_groups/select")
    Page<Account_analytic_group> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_analytic_groups/getdraft")
    Account_analytic_group getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/checkkey")
    Boolean checkKey(@RequestBody Account_analytic_group account_analytic_group);


    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/save")
    Boolean save(@RequestBody Account_analytic_group account_analytic_group);

    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/savebatch")
    Boolean saveBatch(@RequestBody List<Account_analytic_group> account_analytic_groups);



    @RequestMapping(method = RequestMethod.POST, value = "/account_analytic_groups/searchdefault")
    Page<Account_analytic_group> searchDefault(@RequestBody Account_analytic_groupSearchContext context);


}

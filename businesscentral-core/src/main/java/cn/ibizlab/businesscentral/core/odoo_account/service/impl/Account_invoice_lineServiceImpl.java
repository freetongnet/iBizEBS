package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_invoice_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[发票行] 服务对象接口实现
 */
@Slf4j
@Service("Account_invoice_lineServiceImpl")
public class Account_invoice_lineServiceImpl extends EBSServiceImpl<Account_invoice_lineMapper, Account_invoice_line> implements IAccount_invoice_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_feeService repairFeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_lineService repairLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchaseOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.invoice.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_invoice_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_invoice_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_invoice_line et) {
        Account_invoice_line old = new Account_invoice_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_invoice_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_invoice_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        repairFeeService.resetByInvoiceLineId(key);
        repairLineService.resetByInvoiceLineId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        repairFeeService.resetByInvoiceLineId(idList);
        repairLineService.resetByInvoiceLineId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_invoice_line get(Long key) {
        Account_invoice_line et = getById(key);
        if(et==null){
            et=new Account_invoice_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_invoice_line getDraft(Account_invoice_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_invoice_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_invoice_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_invoice_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_invoice_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_invoice_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_invoice_line> selectByAccountId(Long id) {
        return baseMapper.selectByAccountId(id);
    }
    @Override
    public void resetByAccountId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("account_id",null).eq("account_id",id));
    }

    @Override
    public void resetByAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("account_id",null).in("account_id",ids));
    }

    @Override
    public void removeByAccountId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("account_id",id));
    }

	@Override
    public List<Account_invoice_line> selectByAccountAnalyticId(Long id) {
        return baseMapper.selectByAccountAnalyticId(id);
    }
    @Override
    public void resetByAccountAnalyticId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("account_analytic_id",null).eq("account_analytic_id",id));
    }

    @Override
    public void resetByAccountAnalyticId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("account_analytic_id",null).in("account_analytic_id",ids));
    }

    @Override
    public void removeByAccountAnalyticId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("account_analytic_id",id));
    }

	@Override
    public List<Account_invoice_line> selectByInvoiceId(Long id) {
        return baseMapper.selectByInvoiceId(id);
    }
    @Override
    public void removeByInvoiceId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_invoice_line>().in("invoice_id",ids));
    }

    @Override
    public void removeByInvoiceId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("invoice_id",id));
    }

	@Override
    public List<Account_invoice_line> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public List<Account_invoice_line> selectByProductId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_invoice_line>().in("id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("product_id",id));
    }

	@Override
    public List<Account_invoice_line> selectByPurchaseLineId(Long id) {
        return baseMapper.selectByPurchaseLineId(id);
    }
    @Override
    public void resetByPurchaseLineId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("purchase_line_id",null).eq("purchase_line_id",id));
    }

    @Override
    public void resetByPurchaseLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("purchase_line_id",null).in("purchase_line_id",ids));
    }

    @Override
    public void removeByPurchaseLineId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("purchase_line_id",id));
    }

	@Override
    public List<Account_invoice_line> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("company_id",id));
    }

	@Override
    public List<Account_invoice_line> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("currency_id",id));
    }

	@Override
    public List<Account_invoice_line> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void resetByPartnerId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("partner_id",null).eq("partner_id",id));
    }

    @Override
    public void resetByPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("partner_id",null).in("partner_id",ids));
    }

    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("partner_id",id));
    }

	@Override
    public List<Account_invoice_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("create_uid",id));
    }

	@Override
    public List<Account_invoice_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("write_uid",id));
    }

	@Override
    public List<Account_invoice_line> selectByUomId(Long id) {
        return baseMapper.selectByUomId(id);
    }
    @Override
    public void resetByUomId(Long id) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("uom_id",null).eq("uom_id",id));
    }

    @Override
    public void resetByUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_invoice_line>().set("uom_id",null).in("uom_id",ids));
    }

    @Override
    public void removeByUomId(Long id) {
        this.remove(new QueryWrapper<Account_invoice_line>().eq("uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_invoice_line> searchDefault(Account_invoice_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_invoice_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_invoice_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_invoice_line et){
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__ACCOUNT_ACCOUNT__ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount=et.getOdooAccount();
            if(ObjectUtils.isEmpty(odooAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountId());
                et.setOdooAccount(majorEntity);
                odooAccount=majorEntity;
            }
            et.setAccountIdText(odooAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__ACCOUNT_ANALYTIC_ACCOUNT__ACCOUNT_ANALYTIC_ID]
        if(!ObjectUtils.isEmpty(et.getAccountAnalyticId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic=et.getOdooAccountAnalytic();
            if(ObjectUtils.isEmpty(odooAccountAnalytic)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAccountAnalyticId());
                et.setOdooAccountAnalytic(majorEntity);
                odooAccountAnalytic=majorEntity;
            }
            et.setAccountAnalyticIdText(odooAccountAnalytic.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__ACCOUNT_INVOICE__INVOICE_ID]
        if(!ObjectUtils.isEmpty(et.getInvoiceId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooInvoice=et.getOdooInvoice();
            if(ObjectUtils.isEmpty(odooInvoice)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice majorEntity=accountInvoiceService.get(et.getInvoiceId());
                et.setOdooInvoice(majorEntity);
                odooInvoice=majorEntity;
            }
            et.setInvoiceIdText(odooInvoice.getName());
            et.setInvoiceType(odooInvoice.getType());
            et.setCompanyCurrencyId(odooInvoice.getCompanyCurrencyId());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
            et.setProductImage(odooProduct.getImage());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__PURCHASE_ORDER_LINE__PURCHASE_LINE_ID]
        if(!ObjectUtils.isEmpty(et.getPurchaseLineId())){
            cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line odooPurchaseLine=et.getOdooPurchaseLine();
            if(ObjectUtils.isEmpty(odooPurchaseLine)){
                cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order_line majorEntity=purchaseOrderLineService.get(et.getPurchaseLineId());
                et.setOdooPurchaseLine(majorEntity);
                odooPurchaseLine=majorEntity;
            }
            et.setPurchaseId(odooPurchaseLine.getOrderId());
            et.setPurchaseLineIdText(odooPurchaseLine.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_ACCOUNT_INVOICE_LINE__UOM_UOM__UOM_ID]
        if(!ObjectUtils.isEmpty(et.getUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooUom=et.getOdooUom();
            if(ObjectUtils.isEmpty(odooUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getUomId());
                et.setOdooUom(majorEntity);
                odooUom=majorEntity;
            }
            et.setUomIdText(odooUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_invoice_line> getAccountInvoiceLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_invoice_line> getAccountInvoiceLineByEntities(List<Account_invoice_line> entities) {
        List ids =new ArrayList();
        for(Account_invoice_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




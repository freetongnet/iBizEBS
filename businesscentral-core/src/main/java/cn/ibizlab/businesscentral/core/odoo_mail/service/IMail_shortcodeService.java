package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_shortcode;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_shortcodeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_shortcode] 服务对象接口
 */
public interface IMail_shortcodeService extends IService<Mail_shortcode>{

    boolean create(Mail_shortcode et) ;
    void createBatch(List<Mail_shortcode> list) ;
    boolean update(Mail_shortcode et) ;
    void updateBatch(List<Mail_shortcode> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_shortcode get(Long key) ;
    Mail_shortcode getDraft(Mail_shortcode et) ;
    boolean checkKey(Mail_shortcode et) ;
    boolean save(Mail_shortcode et) ;
    void saveBatch(List<Mail_shortcode> list) ;
    Page<Mail_shortcode> searchDefault(Mail_shortcodeSearchContext context) ;
    List<Mail_shortcode> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_shortcode> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_shortcode> getMailShortcodeByIds(List<Long> ids) ;
    List<Mail_shortcode> getMailShortcodeByEntities(List<Mail_shortcode> entities) ;
}



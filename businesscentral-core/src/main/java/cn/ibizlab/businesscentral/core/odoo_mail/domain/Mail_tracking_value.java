package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[邮件跟踪值]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_TRACKING_VALUE",resultMap = "Mail_tracking_valueResultMap")
public class Mail_tracking_value extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字段说明
     */
    @DEField(name = "field_desc")
    @TableField(value = "field_desc")
    @JSONField(name = "field_desc")
    @JsonProperty("field_desc")
    private String fieldDesc;
    /**
     * 旧字符值
     */
    @DEField(name = "old_value_char")
    @TableField(value = "old_value_char")
    @JSONField(name = "old_value_char")
    @JsonProperty("old_value_char")
    private String oldValueChar;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 新日期时间值
     */
    @DEField(name = "new_value_datetime")
    @TableField(value = "new_value_datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "new_value_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("new_value_datetime")
    private Timestamp newValueDatetime;
    /**
     * 旧货币值
     */
    @DEField(name = "old_value_monetary")
    @TableField(value = "old_value_monetary")
    @JSONField(name = "old_value_monetary")
    @JsonProperty("old_value_monetary")
    private Double oldValueMonetary;
    /**
     * tracking_sequence
     */
    @DEField(name = "tracking_sequence")
    @TableField(value = "tracking_sequence")
    @JSONField(name = "tracking_sequence")
    @JsonProperty("tracking_sequence")
    private Integer trackingSequence;
    /**
     * 新字符值
     */
    @DEField(name = "new_value_char")
    @TableField(value = "new_value_char")
    @JSONField(name = "new_value_char")
    @JsonProperty("new_value_char")
    private String newValueChar;
    /**
     * 新文本值
     */
    @DEField(name = "new_value_text")
    @TableField(value = "new_value_text")
    @JSONField(name = "new_value_text")
    @JsonProperty("new_value_text")
    private String newValueText;
    /**
     * 新货币值
     */
    @DEField(name = "new_value_monetary")
    @TableField(value = "new_value_monetary")
    @JSONField(name = "new_value_monetary")
    @JsonProperty("new_value_monetary")
    private Double newValueMonetary;
    /**
     * 旧日期时间值
     */
    @DEField(name = "old_value_datetime")
    @TableField(value = "old_value_datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "old_value_datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("old_value_datetime")
    private Timestamp oldValueDatetime;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 旧整数值
     */
    @DEField(name = "old_value_integer")
    @TableField(value = "old_value_integer")
    @JSONField(name = "old_value_integer")
    @JsonProperty("old_value_integer")
    private Integer oldValueInteger;
    /**
     * 旧文本值
     */
    @DEField(name = "old_value_text")
    @TableField(value = "old_value_text")
    @JSONField(name = "old_value_text")
    @JsonProperty("old_value_text")
    private String oldValueText;
    /**
     * 字段类型
     */
    @DEField(name = "field_type")
    @TableField(value = "field_type")
    @JSONField(name = "field_type")
    @JsonProperty("field_type")
    private String fieldType;
    /**
     * 新整数值
     */
    @DEField(name = "new_value_integer")
    @TableField(value = "new_value_integer")
    @JSONField(name = "new_value_integer")
    @JsonProperty("new_value_integer")
    private Integer newValueInteger;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 新浮点值
     */
    @DEField(name = "new_value_float")
    @TableField(value = "new_value_float")
    @JSONField(name = "new_value_float")
    @JsonProperty("new_value_float")
    private Double newValueFloat;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 更改的字段
     */
    @TableField(value = "field")
    @JSONField(name = "field")
    @JsonProperty("field")
    private String field;
    /**
     * 旧浮点值
     */
    @DEField(name = "old_value_float")
    @TableField(value = "old_value_float")
    @JSONField(name = "old_value_float")
    @JsonProperty("old_value_float")
    private Double oldValueFloat;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 邮件消息ID
     */
    @DEField(name = "mail_message_id")
    @TableField(value = "mail_message_id")
    @JSONField(name = "mail_message_id")
    @JsonProperty("mail_message_id")
    private Long mailMessageId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message odooMailMessage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [字段说明]
     */
    public void setFieldDesc(String fieldDesc){
        this.fieldDesc = fieldDesc ;
        this.modify("field_desc",fieldDesc);
    }

    /**
     * 设置 [旧字符值]
     */
    public void setOldValueChar(String oldValueChar){
        this.oldValueChar = oldValueChar ;
        this.modify("old_value_char",oldValueChar);
    }

    /**
     * 设置 [新日期时间值]
     */
    public void setNewValueDatetime(Timestamp newValueDatetime){
        this.newValueDatetime = newValueDatetime ;
        this.modify("new_value_datetime",newValueDatetime);
    }

    /**
     * 格式化日期 [新日期时间值]
     */
    public String formatNewValueDatetime(){
        if (this.newValueDatetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(newValueDatetime);
    }
    /**
     * 设置 [旧货币值]
     */
    public void setOldValueMonetary(Double oldValueMonetary){
        this.oldValueMonetary = oldValueMonetary ;
        this.modify("old_value_monetary",oldValueMonetary);
    }

    /**
     * 设置 [tracking_sequence]
     */
    public void setTrackingSequence(Integer trackingSequence){
        this.trackingSequence = trackingSequence ;
        this.modify("tracking_sequence",trackingSequence);
    }

    /**
     * 设置 [新字符值]
     */
    public void setNewValueChar(String newValueChar){
        this.newValueChar = newValueChar ;
        this.modify("new_value_char",newValueChar);
    }

    /**
     * 设置 [新文本值]
     */
    public void setNewValueText(String newValueText){
        this.newValueText = newValueText ;
        this.modify("new_value_text",newValueText);
    }

    /**
     * 设置 [新货币值]
     */
    public void setNewValueMonetary(Double newValueMonetary){
        this.newValueMonetary = newValueMonetary ;
        this.modify("new_value_monetary",newValueMonetary);
    }

    /**
     * 设置 [旧日期时间值]
     */
    public void setOldValueDatetime(Timestamp oldValueDatetime){
        this.oldValueDatetime = oldValueDatetime ;
        this.modify("old_value_datetime",oldValueDatetime);
    }

    /**
     * 格式化日期 [旧日期时间值]
     */
    public String formatOldValueDatetime(){
        if (this.oldValueDatetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(oldValueDatetime);
    }
    /**
     * 设置 [旧整数值]
     */
    public void setOldValueInteger(Integer oldValueInteger){
        this.oldValueInteger = oldValueInteger ;
        this.modify("old_value_integer",oldValueInteger);
    }

    /**
     * 设置 [旧文本值]
     */
    public void setOldValueText(String oldValueText){
        this.oldValueText = oldValueText ;
        this.modify("old_value_text",oldValueText);
    }

    /**
     * 设置 [字段类型]
     */
    public void setFieldType(String fieldType){
        this.fieldType = fieldType ;
        this.modify("field_type",fieldType);
    }

    /**
     * 设置 [新整数值]
     */
    public void setNewValueInteger(Integer newValueInteger){
        this.newValueInteger = newValueInteger ;
        this.modify("new_value_integer",newValueInteger);
    }

    /**
     * 设置 [新浮点值]
     */
    public void setNewValueFloat(Double newValueFloat){
        this.newValueFloat = newValueFloat ;
        this.modify("new_value_float",newValueFloat);
    }

    /**
     * 设置 [更改的字段]
     */
    public void setField(String field){
        this.field = field ;
        this.modify("field",field);
    }

    /**
     * 设置 [旧浮点值]
     */
    public void setOldValueFloat(Double oldValueFloat){
        this.oldValueFloat = oldValueFloat ;
        this.modify("old_value_float",oldValueFloat);
    }

    /**
     * 设置 [邮件消息ID]
     */
    public void setMailMessageId(Long mailMessageId){
        this.mailMessageId = mailMessageId ;
        this.modify("mail_message_id",mailMessageId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



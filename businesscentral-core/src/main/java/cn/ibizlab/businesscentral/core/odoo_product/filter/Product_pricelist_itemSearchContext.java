package cn.ibizlab.businesscentral.core.odoo_product.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist_item;
/**
 * 关系型数据实体[Product_pricelist_item] 查询条件对象
 */
@Slf4j
@Data
public class Product_pricelist_itemSearchContext extends QueryWrapperContext<Product_pricelist_item> {

	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_base_eq;//[基于]
	public void setN_base_eq(String n_base_eq) {
        this.n_base_eq = n_base_eq;
        if(!ObjectUtils.isEmpty(this.n_base_eq)){
            this.getSearchCond().eq("base", n_base_eq);
        }
    }
	private String n_compute_price_eq;//[计算价格]
	public void setN_compute_price_eq(String n_compute_price_eq) {
        this.n_compute_price_eq = n_compute_price_eq;
        if(!ObjectUtils.isEmpty(this.n_compute_price_eq)){
            this.getSearchCond().eq("compute_price", n_compute_price_eq);
        }
    }
	private String n_applied_on_eq;//[应用于]
	public void setN_applied_on_eq(String n_applied_on_eq) {
        this.n_applied_on_eq = n_applied_on_eq;
        if(!ObjectUtils.isEmpty(this.n_applied_on_eq)){
            this.getSearchCond().eq("applied_on", n_applied_on_eq);
        }
    }
	private String n_pricelist_id_text_eq;//[价格表]
	public void setN_pricelist_id_text_eq(String n_pricelist_id_text_eq) {
        this.n_pricelist_id_text_eq = n_pricelist_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_eq)){
            this.getSearchCond().eq("pricelist_id_text", n_pricelist_id_text_eq);
        }
    }
	private String n_pricelist_id_text_like;//[价格表]
	public void setN_pricelist_id_text_like(String n_pricelist_id_text_like) {
        this.n_pricelist_id_text_like = n_pricelist_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_text_like)){
            this.getSearchCond().like("pricelist_id_text", n_pricelist_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_categ_id_text_eq;//[产品种类]
	public void setN_categ_id_text_eq(String n_categ_id_text_eq) {
        this.n_categ_id_text_eq = n_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_categ_id_text_eq)){
            this.getSearchCond().eq("categ_id_text", n_categ_id_text_eq);
        }
    }
	private String n_categ_id_text_like;//[产品种类]
	public void setN_categ_id_text_like(String n_categ_id_text_like) {
        this.n_categ_id_text_like = n_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_categ_id_text_like)){
            this.getSearchCond().like("categ_id_text", n_categ_id_text_like);
        }
    }
	private String n_base_pricelist_id_text_eq;//[其他价格表]
	public void setN_base_pricelist_id_text_eq(String n_base_pricelist_id_text_eq) {
        this.n_base_pricelist_id_text_eq = n_base_pricelist_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_base_pricelist_id_text_eq)){
            this.getSearchCond().eq("base_pricelist_id_text", n_base_pricelist_id_text_eq);
        }
    }
	private String n_base_pricelist_id_text_like;//[其他价格表]
	public void setN_base_pricelist_id_text_like(String n_base_pricelist_id_text_like) {
        this.n_base_pricelist_id_text_like = n_base_pricelist_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_base_pricelist_id_text_like)){
            this.getSearchCond().like("base_pricelist_id_text", n_base_pricelist_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_product_tmpl_id_text_eq;//[产品模板]
	public void setN_product_tmpl_id_text_eq(String n_product_tmpl_id_text_eq) {
        this.n_product_tmpl_id_text_eq = n_product_tmpl_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_tmpl_id_text_eq)){
            this.getSearchCond().eq("product_tmpl_id_text", n_product_tmpl_id_text_eq);
        }
    }
	private String n_product_tmpl_id_text_like;//[产品模板]
	public void setN_product_tmpl_id_text_like(String n_product_tmpl_id_text_like) {
        this.n_product_tmpl_id_text_like = n_product_tmpl_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_tmpl_id_text_like)){
            this.getSearchCond().like("product_tmpl_id_text", n_product_tmpl_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_pricelist_id_eq;//[价格表]
	public void setN_pricelist_id_eq(Long n_pricelist_id_eq) {
        this.n_pricelist_id_eq = n_pricelist_id_eq;
        if(!ObjectUtils.isEmpty(this.n_pricelist_id_eq)){
            this.getSearchCond().eq("pricelist_id", n_pricelist_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_base_pricelist_id_eq;//[其他价格表]
	public void setN_base_pricelist_id_eq(Long n_base_pricelist_id_eq) {
        this.n_base_pricelist_id_eq = n_base_pricelist_id_eq;
        if(!ObjectUtils.isEmpty(this.n_base_pricelist_id_eq)){
            this.getSearchCond().eq("base_pricelist_id", n_base_pricelist_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_categ_id_eq;//[产品种类]
	public void setN_categ_id_eq(Long n_categ_id_eq) {
        this.n_categ_id_eq = n_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_categ_id_eq)){
            this.getSearchCond().eq("categ_id", n_categ_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_product_tmpl_id_eq;//[产品模板]
	public void setN_product_tmpl_id_eq(Long n_product_tmpl_id_eq) {
        this.n_product_tmpl_id_eq = n_product_tmpl_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_tmpl_id_eq)){
            this.getSearchCond().eq("product_tmpl_id", n_product_tmpl_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




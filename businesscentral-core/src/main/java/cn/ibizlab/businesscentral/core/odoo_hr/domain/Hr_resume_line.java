package cn.ibizlab.businesscentral.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[员工简历行]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "HR_RESUME_LINE",resultMap = "Hr_resume_lineResultMap")
public class Hr_resume_line extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date")
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 创建时间
     */
    @DEField(name = "create_date")
    @TableField(value = "create_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 开始日期
     */
    @DEField(name = "date_start")
    @TableField(value = "date_start")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_start" , format="yyyy-MM-dd")
    @JsonProperty("date_start")
    private Timestamp dateStart;
    /**
     * 结束日期
     */
    @DEField(name = "date_end")
    @TableField(value = "date_end")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd")
    @JsonProperty("date_end")
    private Timestamp dateEnd;
    /**
     * 显示类型
     */
    @DEField(name = "display_type")
    @TableField(value = "display_type")
    @JSONField(name = "display_type")
    @JsonProperty("display_type")
    private String displayType;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "line_type_id_text")
    @JsonProperty("line_type_id_text")
    private String lineTypeIdText;
    /**
     * 员工
     */
    @TableField(exist = false)
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;
    /**
     * 员工
     */
    @DEField(name = "employee_id")
    @TableField(value = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Long employeeId;
    /**
     * ID
     */
    @DEField(name = "create_uid")
    @TableField(value = "create_uid")
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 类型
     */
    @DEField(name = "line_type_id")
    @TableField(value = "line_type_id")
    @JSONField(name = "line_type_id")
    @JsonProperty("line_type_id")
    private Long lineTypeId;
    /**
     * ID
     */
    @DEField(name = "write_uid")
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_resume_line_type odooResumeLineType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [最后更新时间]
     */
    public void setWriteDate(Timestamp writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 格式化日期 [最后更新时间]
     */
    public String formatWriteDate(){
        if (this.writeDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(writeDate);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [创建时间]
     */
    public void setCreateDate(Timestamp createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 格式化日期 [创建时间]
     */
    public String formatCreateDate(){
        if (this.createDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(createDate);
    }
    /**
     * 设置 [开始日期]
     */
    public void setDateStart(Timestamp dateStart){
        this.dateStart = dateStart ;
        this.modify("date_start",dateStart);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatDateStart(){
        if (this.dateStart == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateStart);
    }
    /**
     * 设置 [结束日期]
     */
    public void setDateEnd(Timestamp dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatDateEnd(){
        if (this.dateEnd == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateEnd);
    }
    /**
     * 设置 [显示类型]
     */
    public void setDisplayType(String displayType){
        this.displayType = displayType ;
        this.modify("display_type",displayType);
    }

    /**
     * 设置 [员工]
     */
    public void setEmployeeId(Long employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }

    /**
     * 设置 [ID]
     */
    public void setCreateUid(Long createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }

    /**
     * 设置 [类型]
     */
    public void setLineTypeId(Long lineTypeId){
        this.lineTypeId = lineTypeId ;
        this.modify("line_type_id",lineTypeId);
    }

    /**
     * 设置 [ID]
     */
    public void setWriteUid(Long writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



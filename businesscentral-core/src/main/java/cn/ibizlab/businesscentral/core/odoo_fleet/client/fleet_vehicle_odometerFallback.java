package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_odometer;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_odometerSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[fleet_vehicle_odometer] 服务对象接口
 */
@Component
public class fleet_vehicle_odometerFallback implements fleet_vehicle_odometerFeignClient{

    public Page<Fleet_vehicle_odometer> search(Fleet_vehicle_odometerSearchContext context){
            return null;
     }




    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Fleet_vehicle_odometer create(Fleet_vehicle_odometer fleet_vehicle_odometer){
            return null;
     }
    public Boolean createBatch(List<Fleet_vehicle_odometer> fleet_vehicle_odometers){
            return false;
     }

    public Fleet_vehicle_odometer get(Long id){
            return null;
     }


    public Fleet_vehicle_odometer update(Long id, Fleet_vehicle_odometer fleet_vehicle_odometer){
            return null;
     }
    public Boolean updateBatch(List<Fleet_vehicle_odometer> fleet_vehicle_odometers){
            return false;
     }


    public Page<Fleet_vehicle_odometer> select(){
            return null;
     }

    public Fleet_vehicle_odometer getDraft(){
            return null;
    }



    public Boolean checkKey(Fleet_vehicle_odometer fleet_vehicle_odometer){
            return false;
     }


    public Boolean save(Fleet_vehicle_odometer fleet_vehicle_odometer){
            return false;
     }
    public Boolean saveBatch(List<Fleet_vehicle_odometer> fleet_vehicle_odometers){
            return false;
     }

    public Page<Fleet_vehicle_odometer> searchDefault(Fleet_vehicle_odometerSearchContext context){
            return null;
     }


}

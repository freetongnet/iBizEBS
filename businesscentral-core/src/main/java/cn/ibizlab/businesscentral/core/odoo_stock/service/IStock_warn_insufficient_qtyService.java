package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warn_insufficient_qty;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_warn_insufficient_qtySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_warn_insufficient_qty] 服务对象接口
 */
public interface IStock_warn_insufficient_qtyService extends IService<Stock_warn_insufficient_qty>{

    boolean create(Stock_warn_insufficient_qty et) ;
    void createBatch(List<Stock_warn_insufficient_qty> list) ;
    boolean update(Stock_warn_insufficient_qty et) ;
    void updateBatch(List<Stock_warn_insufficient_qty> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_warn_insufficient_qty get(Long key) ;
    Stock_warn_insufficient_qty getDraft(Stock_warn_insufficient_qty et) ;
    boolean checkKey(Stock_warn_insufficient_qty et) ;
    boolean save(Stock_warn_insufficient_qty et) ;
    void saveBatch(List<Stock_warn_insufficient_qty> list) ;
    Page<Stock_warn_insufficient_qty> searchDefault(Stock_warn_insufficient_qtySearchContext context) ;
    List<Stock_warn_insufficient_qty> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_warn_insufficient_qty> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



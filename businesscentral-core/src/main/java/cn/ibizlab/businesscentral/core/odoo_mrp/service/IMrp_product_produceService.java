package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_product_produceSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_product_produce] 服务对象接口
 */
public interface IMrp_product_produceService extends IService<Mrp_product_produce>{

    boolean create(Mrp_product_produce et) ;
    void createBatch(List<Mrp_product_produce> list) ;
    boolean update(Mrp_product_produce et) ;
    void updateBatch(List<Mrp_product_produce> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_product_produce get(Long key) ;
    Mrp_product_produce getDraft(Mrp_product_produce et) ;
    boolean checkKey(Mrp_product_produce et) ;
    boolean save(Mrp_product_produce et) ;
    void saveBatch(List<Mrp_product_produce> list) ;
    Page<Mrp_product_produce> searchDefault(Mrp_product_produceSearchContext context) ;
    List<Mrp_product_produce> selectByProductionId(Long id);
    void resetByProductionId(Long id);
    void resetByProductionId(Collection<Long> ids);
    void removeByProductionId(Long id);
    List<Mrp_product_produce> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Mrp_product_produce> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_product_produce> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mrp_product_produce> selectByLotId(Long id);
    void resetByLotId(Long id);
    void resetByLotId(Collection<Long> ids);
    void removeByLotId(Long id);
    List<Mrp_product_produce> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_product_produce> getMrpProductProduceByIds(List<Long> ids) ;
    List<Mrp_product_produce> getMrpProductProduceByEntities(List<Mrp_product_produce> entities) ;
}



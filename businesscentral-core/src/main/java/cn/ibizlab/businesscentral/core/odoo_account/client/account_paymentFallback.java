package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_paymentSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_payment] 服务对象接口
 */
@Component
public class account_paymentFallback implements account_paymentFeignClient{

    public Page<Account_payment> search(Account_paymentSearchContext context){
            return null;
     }



    public Account_payment create(Account_payment account_payment){
            return null;
     }
    public Boolean createBatch(List<Account_payment> account_payments){
            return false;
     }

    public Account_payment update(Long id, Account_payment account_payment){
            return null;
     }
    public Boolean updateBatch(List<Account_payment> account_payments){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Account_payment get(Long id){
            return null;
     }


    public Page<Account_payment> select(){
            return null;
     }

    public Account_payment getDraft(){
            return null;
    }



    public Boolean checkKey(Account_payment account_payment){
            return false;
     }


    public Boolean save(Account_payment account_payment){
            return false;
     }
    public Boolean saveBatch(List<Account_payment> account_payments){
            return false;
     }

    public Page<Account_payment> searchDefault(Account_paymentSearchContext context){
            return null;
     }


}

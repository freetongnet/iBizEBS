package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_backorder_confirmation;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_backorder_confirmationSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[stock_backorder_confirmation] 服务对象接口
 */
@Component
public class stock_backorder_confirmationFallback implements stock_backorder_confirmationFeignClient{

    public Stock_backorder_confirmation create(Stock_backorder_confirmation stock_backorder_confirmation){
            return null;
     }
    public Boolean createBatch(List<Stock_backorder_confirmation> stock_backorder_confirmations){
            return false;
     }

    public Stock_backorder_confirmation get(Long id){
            return null;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Stock_backorder_confirmation> search(Stock_backorder_confirmationSearchContext context){
            return null;
     }


    public Stock_backorder_confirmation update(Long id, Stock_backorder_confirmation stock_backorder_confirmation){
            return null;
     }
    public Boolean updateBatch(List<Stock_backorder_confirmation> stock_backorder_confirmations){
            return false;
     }



    public Page<Stock_backorder_confirmation> select(){
            return null;
     }

    public Stock_backorder_confirmation getDraft(){
            return null;
    }



    public Boolean checkKey(Stock_backorder_confirmation stock_backorder_confirmation){
            return false;
     }


    public Boolean save(Stock_backorder_confirmation stock_backorder_confirmation){
            return false;
     }
    public Boolean saveBatch(List<Stock_backorder_confirmation> stock_backorder_confirmations){
            return false;
     }

    public Page<Stock_backorder_confirmation> searchDefault(Stock_backorder_confirmationSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Product_template_attribute_exclusion] 服务对象接口
 */
public interface IProduct_template_attribute_exclusionService extends IService<Product_template_attribute_exclusion>{

    boolean create(Product_template_attribute_exclusion et) ;
    void createBatch(List<Product_template_attribute_exclusion> list) ;
    boolean update(Product_template_attribute_exclusion et) ;
    void updateBatch(List<Product_template_attribute_exclusion> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Product_template_attribute_exclusion get(Long key) ;
    Product_template_attribute_exclusion getDraft(Product_template_attribute_exclusion et) ;
    boolean checkKey(Product_template_attribute_exclusion et) ;
    boolean save(Product_template_attribute_exclusion et) ;
    void saveBatch(List<Product_template_attribute_exclusion> list) ;
    Page<Product_template_attribute_exclusion> searchDefault(Product_template_attribute_exclusionSearchContext context) ;
    List<Product_template_attribute_exclusion> selectByProductTemplateAttributeValueId(Long id);
    void removeByProductTemplateAttributeValueId(Collection<Long> ids);
    void removeByProductTemplateAttributeValueId(Long id);
    List<Product_template_attribute_exclusion> selectByProductTmplId(Long id);
    void removeByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Product_template_attribute_exclusion> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Product_template_attribute_exclusion> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Product_template_attribute_exclusion> getProductTemplateAttributeExclusionByIds(List<Long> ids) ;
    List<Product_template_attribute_exclusion> getProductTemplateAttributeExclusionByEntities(List<Product_template_attribute_exclusion> entities) ;
}



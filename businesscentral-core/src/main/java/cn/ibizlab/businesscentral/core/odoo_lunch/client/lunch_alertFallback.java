package cn.ibizlab.businesscentral.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_alertSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[lunch_alert] 服务对象接口
 */
@Component
public class lunch_alertFallback implements lunch_alertFeignClient{



    public Page<Lunch_alert> search(Lunch_alertSearchContext context){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Lunch_alert get(Long id){
            return null;
     }



    public Lunch_alert update(Long id, Lunch_alert lunch_alert){
            return null;
     }
    public Boolean updateBatch(List<Lunch_alert> lunch_alerts){
            return false;
     }


    public Lunch_alert create(Lunch_alert lunch_alert){
            return null;
     }
    public Boolean createBatch(List<Lunch_alert> lunch_alerts){
            return false;
     }

    public Page<Lunch_alert> select(){
            return null;
     }

    public Lunch_alert getDraft(){
            return null;
    }



    public Boolean checkKey(Lunch_alert lunch_alert){
            return false;
     }


    public Boolean save(Lunch_alert lunch_alert){
            return false;
     }
    public Boolean saveBatch(List<Lunch_alert> lunch_alerts){
            return false;
     }

    public Page<Lunch_alert> searchDefault(Lunch_alertSearchContext context){
            return null;
     }


}

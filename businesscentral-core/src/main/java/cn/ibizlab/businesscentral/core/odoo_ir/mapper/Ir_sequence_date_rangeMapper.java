package cn.ibizlab.businesscentral.core.odoo_ir.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence_date_range;
import cn.ibizlab.businesscentral.core.odoo_ir.filter.Ir_sequence_date_rangeSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Ir_sequence_date_rangeMapper extends BaseMapper<Ir_sequence_date_range>{

    Page<Ir_sequence_date_range> searchDefault(IPage page, @Param("srf") Ir_sequence_date_rangeSearchContext context, @Param("ew") Wrapper<Ir_sequence_date_range> wrapper) ;
    @Override
    Ir_sequence_date_range selectById(Serializable id);
    @Override
    int insert(Ir_sequence_date_range entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Ir_sequence_date_range entity);
    @Override
    int update(@Param(Constants.ENTITY) Ir_sequence_date_range entity, @Param("ew") Wrapper<Ir_sequence_date_range> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Ir_sequence_date_range> selectBySequenceId(@Param("id") Serializable id) ;

    List<Ir_sequence_date_range> selectByCreateUid(@Param("id") Serializable id) ;

    List<Ir_sequence_date_range> selectByWriteUid(@Param("id") Serializable id) ;


}

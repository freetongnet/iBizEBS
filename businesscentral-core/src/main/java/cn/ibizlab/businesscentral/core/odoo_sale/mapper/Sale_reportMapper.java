package cn.ibizlab.businesscentral.core.odoo_sale.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_reportSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Sale_reportMapper extends BaseMapper<Sale_report>{

    Page<Sale_report> searchDefault(IPage page, @Param("srf") Sale_reportSearchContext context, @Param("ew") Wrapper<Sale_report> wrapper) ;
    @Override
    Sale_report selectById(Serializable id);
    @Override
    int insert(Sale_report entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Sale_report entity);
    @Override
    int update(@Param(Constants.ENTITY) Sale_report entity, @Param("ew") Wrapper<Sale_report> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Sale_report> selectByAnalyticAccountId(@Param("id") Serializable id) ;

    List<Sale_report> selectByTeamId(@Param("id") Serializable id) ;

    List<Sale_report> selectByCategId(@Param("id") Serializable id) ;

    List<Sale_report> selectByPricelistId(@Param("id") Serializable id) ;

    List<Sale_report> selectByProductId(@Param("id") Serializable id) ;

    List<Sale_report> selectByProductTmplId(@Param("id") Serializable id) ;

    List<Sale_report> selectByCompanyId(@Param("id") Serializable id) ;

    List<Sale_report> selectByCountryId(@Param("id") Serializable id) ;

    List<Sale_report> selectByCommercialPartnerId(@Param("id") Serializable id) ;

    List<Sale_report> selectByPartnerId(@Param("id") Serializable id) ;

    List<Sale_report> selectByUserId(@Param("id") Serializable id) ;

    List<Sale_report> selectByOrderId(@Param("id") Serializable id) ;

    List<Sale_report> selectByWarehouseId(@Param("id") Serializable id) ;

    List<Sale_report> selectByProductUom(@Param("id") Serializable id) ;

    List<Sale_report> selectByCampaignId(@Param("id") Serializable id) ;

    List<Sale_report> selectByMediumId(@Param("id") Serializable id) ;

    List<Sale_report> selectBySourceId(@Param("id") Serializable id) ;


}

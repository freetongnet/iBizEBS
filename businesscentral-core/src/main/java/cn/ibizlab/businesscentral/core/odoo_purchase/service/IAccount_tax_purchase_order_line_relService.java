package cn.ibizlab.businesscentral.core.odoo_purchase.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Account_tax_purchase_order_line_rel;
import cn.ibizlab.businesscentral.core.odoo_purchase.filter.Account_tax_purchase_order_line_relSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_tax_purchase_order_line_rel] 服务对象接口
 */
public interface IAccount_tax_purchase_order_line_relService extends IService<Account_tax_purchase_order_line_rel>{

    boolean create(Account_tax_purchase_order_line_rel et) ;
    void createBatch(List<Account_tax_purchase_order_line_rel> list) ;
    boolean update(Account_tax_purchase_order_line_rel et) ;
    void updateBatch(List<Account_tax_purchase_order_line_rel> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_tax_purchase_order_line_rel get(Long key) ;
    Account_tax_purchase_order_line_rel getDraft(Account_tax_purchase_order_line_rel et) ;
    boolean checkKey(Account_tax_purchase_order_line_rel et) ;
    boolean save(Account_tax_purchase_order_line_rel et) ;
    void saveBatch(List<Account_tax_purchase_order_line_rel> list) ;
    Page<Account_tax_purchase_order_line_rel> searchDefault(Account_tax_purchase_order_line_relSearchContext context) ;
    List<Account_tax_purchase_order_line_rel> selectByAccountTaxId(Long id);
    void removeByAccountTaxId(Long id);
    List<Account_tax_purchase_order_line_rel> selectByPurchaseOrderLineId(Long id);
    void removeByPurchaseOrderLineId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



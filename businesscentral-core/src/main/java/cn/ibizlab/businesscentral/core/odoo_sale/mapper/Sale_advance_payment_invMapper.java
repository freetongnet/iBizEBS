package cn.ibizlab.businesscentral.core.odoo_sale.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_advance_payment_inv;
import cn.ibizlab.businesscentral.core.odoo_sale.filter.Sale_advance_payment_invSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Sale_advance_payment_invMapper extends BaseMapper<Sale_advance_payment_inv>{

    Page<Sale_advance_payment_inv> searchDefault(IPage page, @Param("srf") Sale_advance_payment_invSearchContext context, @Param("ew") Wrapper<Sale_advance_payment_inv> wrapper) ;
    @Override
    Sale_advance_payment_inv selectById(Serializable id);
    @Override
    int insert(Sale_advance_payment_inv entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Sale_advance_payment_inv entity);
    @Override
    int update(@Param(Constants.ENTITY) Sale_advance_payment_inv entity, @Param("ew") Wrapper<Sale_advance_payment_inv> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Sale_advance_payment_inv> selectByDepositAccountId(@Param("id") Serializable id) ;

    List<Sale_advance_payment_inv> selectByProductId(@Param("id") Serializable id) ;

    List<Sale_advance_payment_inv> selectByCreateUid(@Param("id") Serializable id) ;

    List<Sale_advance_payment_inv> selectByWriteUid(@Param("id") Serializable id) ;


}

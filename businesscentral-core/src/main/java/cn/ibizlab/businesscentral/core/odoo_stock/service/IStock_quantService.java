package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_quant;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_quantSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_quant] 服务对象接口
 */
public interface IStock_quantService extends IService<Stock_quant>{

    boolean create(Stock_quant et) ;
    void createBatch(List<Stock_quant> list) ;
    boolean update(Stock_quant et) ;
    void updateBatch(List<Stock_quant> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_quant get(Long key) ;
    Stock_quant getDraft(Stock_quant et) ;
    boolean checkKey(Stock_quant et) ;
    boolean save(Stock_quant et) ;
    void saveBatch(List<Stock_quant> list) ;
    Page<Stock_quant> searchDefault(Stock_quantSearchContext context) ;
    List<Stock_quant> selectByProductId(Long id);
    List<Stock_quant> selectByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_quant> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_quant> selectByOwnerId(Long id);
    void resetByOwnerId(Long id);
    void resetByOwnerId(Collection<Long> ids);
    void removeByOwnerId(Long id);
    List<Stock_quant> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_quant> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_quant> selectByLocationId(Long id);
    List<Stock_quant> selectByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_quant> selectByLotId(Long id);
    List<Stock_quant> selectByLotId(Collection<Long> ids);
    void removeByLotId(Long id);
    List<Stock_quant> selectByPackageId(Long id);
    List<Stock_quant> selectByPackageId(Collection<Long> ids);
    void removeByPackageId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_quant> getStockQuantByIds(List<Long> ids) ;
    List<Stock_quant> getStockQuantByEntities(List<Stock_quant> entities) ;
}



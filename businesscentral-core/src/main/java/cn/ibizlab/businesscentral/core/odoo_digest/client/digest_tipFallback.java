package cn.ibizlab.businesscentral.core.odoo_digest.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.businesscentral.core.odoo_digest.filter.Digest_tipSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[digest_tip] 服务对象接口
 */
@Component
public class digest_tipFallback implements digest_tipFeignClient{


    public Page<Digest_tip> search(Digest_tipSearchContext context){
            return null;
     }


    public Digest_tip get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Digest_tip update(Long id, Digest_tip digest_tip){
            return null;
     }
    public Boolean updateBatch(List<Digest_tip> digest_tips){
            return false;
     }


    public Digest_tip create(Digest_tip digest_tip){
            return null;
     }
    public Boolean createBatch(List<Digest_tip> digest_tips){
            return false;
     }


    public Page<Digest_tip> select(){
            return null;
     }

    public Digest_tip getDraft(){
            return null;
    }



    public Boolean checkKey(Digest_tip digest_tip){
            return false;
     }


    public Boolean save(Digest_tip digest_tip){
            return false;
     }
    public Boolean saveBatch(List<Digest_tip> digest_tips){
            return false;
     }

    public Page<Digest_tip> searchDefault(Digest_tipSearchContext context){
            return null;
     }


}

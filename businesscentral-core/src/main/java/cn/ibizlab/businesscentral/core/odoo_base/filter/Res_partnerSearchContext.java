package cn.ibizlab.businesscentral.core.odoo_base.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner;
/**
 * 关系型数据实体[Res_partner] 查询条件对象
 */
@Slf4j
@Data
public class Res_partnerSearchContext extends QueryWrapperContext<Res_partner> {

	private String n_type_eq;//[地址类型]
	public void setN_type_eq(String n_type_eq) {
        this.n_type_eq = n_type_eq;
        if(!ObjectUtils.isEmpty(this.n_type_eq)){
            this.getSearchCond().eq("type", n_type_eq);
        }
    }
	private String n_trust_eq;//[对此债务人的信任度]
	public void setN_trust_eq(String n_trust_eq) {
        this.n_trust_eq = n_trust_eq;
        if(!ObjectUtils.isEmpty(this.n_trust_eq)){
            this.getSearchCond().eq("trust", n_trust_eq);
        }
    }
	private String n_invoice_warn_eq;//[发票]
	public void setN_invoice_warn_eq(String n_invoice_warn_eq) {
        this.n_invoice_warn_eq = n_invoice_warn_eq;
        if(!ObjectUtils.isEmpty(this.n_invoice_warn_eq)){
            this.getSearchCond().eq("invoice_warn", n_invoice_warn_eq);
        }
    }
	private String n_tz_eq;//[时区]
	public void setN_tz_eq(String n_tz_eq) {
        this.n_tz_eq = n_tz_eq;
        if(!ObjectUtils.isEmpty(this.n_tz_eq)){
            this.getSearchCond().eq("tz", n_tz_eq);
        }
    }
	private String n_sale_warn_eq;//[销售警告]
	public void setN_sale_warn_eq(String n_sale_warn_eq) {
        this.n_sale_warn_eq = n_sale_warn_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_warn_eq)){
            this.getSearchCond().eq("sale_warn", n_sale_warn_eq);
        }
    }
	private String n_picking_warn_eq;//[库存拣货]
	public void setN_picking_warn_eq(String n_picking_warn_eq) {
        this.n_picking_warn_eq = n_picking_warn_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_warn_eq)){
            this.getSearchCond().eq("picking_warn", n_picking_warn_eq);
        }
    }
	private String n_purchase_warn_eq;//[采购订单]
	public void setN_purchase_warn_eq(String n_purchase_warn_eq) {
        this.n_purchase_warn_eq = n_purchase_warn_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_warn_eq)){
            this.getSearchCond().eq("purchase_warn", n_purchase_warn_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_company_type_eq;//[公司类别]
	public void setN_company_type_eq(String n_company_type_eq) {
        this.n_company_type_eq = n_company_type_eq;
        if(!ObjectUtils.isEmpty(this.n_company_type_eq)){
            this.getSearchCond().eq("company_type", n_company_type_eq);
        }
    }
	private String n_lang_eq;//[语言]
	public void setN_lang_eq(String n_lang_eq) {
        this.n_lang_eq = n_lang_eq;
        if(!ObjectUtils.isEmpty(this.n_lang_eq)){
            this.getSearchCond().eq("lang", n_lang_eq);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_title_text_eq;//[称谓]
	public void setN_title_text_eq(String n_title_text_eq) {
        this.n_title_text_eq = n_title_text_eq;
        if(!ObjectUtils.isEmpty(this.n_title_text_eq)){
            this.getSearchCond().eq("title_text", n_title_text_eq);
        }
    }
	private String n_title_text_like;//[称谓]
	public void setN_title_text_like(String n_title_text_like) {
        this.n_title_text_like = n_title_text_like;
        if(!ObjectUtils.isEmpty(this.n_title_text_like)){
            this.getSearchCond().like("title_text", n_title_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_country_id_text_eq;//[国家/地区]
	public void setN_country_id_text_eq(String n_country_id_text_eq) {
        this.n_country_id_text_eq = n_country_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_eq)){
            this.getSearchCond().eq("country_id_text", n_country_id_text_eq);
        }
    }
	private String n_country_id_text_like;//[国家/地区]
	public void setN_country_id_text_like(String n_country_id_text_like) {
        this.n_country_id_text_like = n_country_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_country_id_text_like)){
            this.getSearchCond().like("country_id_text", n_country_id_text_like);
        }
    }
	private String n_state_id_text_eq;//[省/ 州]
	public void setN_state_id_text_eq(String n_state_id_text_eq) {
        this.n_state_id_text_eq = n_state_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_state_id_text_eq)){
            this.getSearchCond().eq("state_id_text", n_state_id_text_eq);
        }
    }
	private String n_state_id_text_like;//[省/ 州]
	public void setN_state_id_text_like(String n_state_id_text_like) {
        this.n_state_id_text_like = n_state_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_state_id_text_like)){
            this.getSearchCond().like("state_id_text", n_state_id_text_like);
        }
    }
	private String n_commercial_partner_id_text_eq;//[商业实体]
	public void setN_commercial_partner_id_text_eq(String n_commercial_partner_id_text_eq) {
        this.n_commercial_partner_id_text_eq = n_commercial_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_eq)){
            this.getSearchCond().eq("commercial_partner_id_text", n_commercial_partner_id_text_eq);
        }
    }
	private String n_commercial_partner_id_text_like;//[商业实体]
	public void setN_commercial_partner_id_text_like(String n_commercial_partner_id_text_like) {
        this.n_commercial_partner_id_text_like = n_commercial_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_text_like)){
            this.getSearchCond().like("commercial_partner_id_text", n_commercial_partner_id_text_like);
        }
    }
	private String n_parent_name_eq;//[上级名称]
	public void setN_parent_name_eq(String n_parent_name_eq) {
        this.n_parent_name_eq = n_parent_name_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_name_eq)){
            this.getSearchCond().eq("parent_name", n_parent_name_eq);
        }
    }
	private String n_parent_name_like;//[上级名称]
	public void setN_parent_name_like(String n_parent_name_like) {
        this.n_parent_name_like = n_parent_name_like;
        if(!ObjectUtils.isEmpty(this.n_parent_name_like)){
            this.getSearchCond().like("parent_name", n_parent_name_like);
        }
    }
	private String n_user_id_text_eq;//[销售员]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[销售员]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_industry_id_text_eq;//[工业]
	public void setN_industry_id_text_eq(String n_industry_id_text_eq) {
        this.n_industry_id_text_eq = n_industry_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_industry_id_text_eq)){
            this.getSearchCond().eq("industry_id_text", n_industry_id_text_eq);
        }
    }
	private String n_industry_id_text_like;//[工业]
	public void setN_industry_id_text_like(String n_industry_id_text_like) {
        this.n_industry_id_text_like = n_industry_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_industry_id_text_like)){
            this.getSearchCond().like("industry_id_text", n_industry_id_text_like);
        }
    }
	private String n_team_id_text_eq;//[销售团队]
	public void setN_team_id_text_eq(String n_team_id_text_eq) {
        this.n_team_id_text_eq = n_team_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_eq)){
            this.getSearchCond().eq("team_id_text", n_team_id_text_eq);
        }
    }
	private String n_team_id_text_like;//[销售团队]
	public void setN_team_id_text_like(String n_team_id_text_like) {
        this.n_team_id_text_like = n_team_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_team_id_text_like)){
            this.getSearchCond().like("team_id_text", n_team_id_text_like);
        }
    }
	private Long n_team_id_eq;//[销售团队]
	public void setN_team_id_eq(Long n_team_id_eq) {
        this.n_team_id_eq = n_team_id_eq;
        if(!ObjectUtils.isEmpty(this.n_team_id_eq)){
            this.getSearchCond().eq("team_id", n_team_id_eq);
        }
    }
	private Long n_state_id_eq;//[省/ 州]
	public void setN_state_id_eq(Long n_state_id_eq) {
        this.n_state_id_eq = n_state_id_eq;
        if(!ObjectUtils.isEmpty(this.n_state_id_eq)){
            this.getSearchCond().eq("state_id", n_state_id_eq);
        }
    }
	private Long n_user_id_eq;//[销售员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_parent_id_eq;//[关联公司]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_title_eq;//[称谓]
	public void setN_title_eq(Long n_title_eq) {
        this.n_title_eq = n_title_eq;
        if(!ObjectUtils.isEmpty(this.n_title_eq)){
            this.getSearchCond().eq("title", n_title_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_commercial_partner_id_eq;//[商业实体]
	public void setN_commercial_partner_id_eq(Long n_commercial_partner_id_eq) {
        this.n_commercial_partner_id_eq = n_commercial_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_commercial_partner_id_eq)){
            this.getSearchCond().eq("commercial_partner_id", n_commercial_partner_id_eq);
        }
    }
	private Long n_industry_id_eq;//[工业]
	public void setN_industry_id_eq(Long n_industry_id_eq) {
        this.n_industry_id_eq = n_industry_id_eq;
        if(!ObjectUtils.isEmpty(this.n_industry_id_eq)){
            this.getSearchCond().eq("industry_id", n_industry_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_country_id_eq;//[国家/地区]
	public void setN_country_id_eq(Long n_country_id_eq) {
        this.n_country_id_eq = n_country_id_eq;
        if(!ObjectUtils.isEmpty(this.n_country_id_eq)){
            this.getSearchCond().eq("country_id", n_country_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




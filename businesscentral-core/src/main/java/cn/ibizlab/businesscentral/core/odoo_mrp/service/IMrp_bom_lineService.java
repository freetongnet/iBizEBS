package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_bom_line] 服务对象接口
 */
public interface IMrp_bom_lineService extends IService<Mrp_bom_line>{

    boolean create(Mrp_bom_line et) ;
    void createBatch(List<Mrp_bom_line> list) ;
    boolean update(Mrp_bom_line et) ;
    void updateBatch(List<Mrp_bom_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_bom_line get(Long key) ;
    Mrp_bom_line getDraft(Mrp_bom_line et) ;
    boolean checkKey(Mrp_bom_line et) ;
    boolean save(Mrp_bom_line et) ;
    void saveBatch(List<Mrp_bom_line> list) ;
    Page<Mrp_bom_line> searchDefault(Mrp_bom_lineSearchContext context) ;
    List<Mrp_bom_line> selectByBomId(Long id);
    void removeByBomId(Collection<Long> ids);
    void removeByBomId(Long id);
    List<Mrp_bom_line> selectByOperationId(Long id);
    void resetByOperationId(Long id);
    void resetByOperationId(Collection<Long> ids);
    void removeByOperationId(Long id);
    List<Mrp_bom_line> selectByRoutingId(Long id);
    void resetByRoutingId(Long id);
    void resetByRoutingId(Collection<Long> ids);
    void removeByRoutingId(Long id);
    List<Mrp_bom_line> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Mrp_bom_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_bom_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mrp_bom_line> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_bom_line> getMrpBomLineByIds(List<Long> ids) ;
    List<Mrp_bom_line> getMrpBomLineByEntities(List<Mrp_bom_line> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_groups;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_groupsSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_groups] 服务对象接口
 */
public interface IRes_groupsService extends IService<Res_groups>{

    boolean create(Res_groups et) ;
    void createBatch(List<Res_groups> list) ;
    boolean update(Res_groups et) ;
    void updateBatch(List<Res_groups> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_groups get(Long key) ;
    Res_groups getDraft(Res_groups et) ;
    boolean checkKey(Res_groups et) ;
    boolean save(Res_groups et) ;
    void saveBatch(List<Res_groups> list) ;
    Page<Res_groups> searchDefault(Res_groupsSearchContext context) ;
    List<Res_groups> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_groups> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_groups> getResGroupsByIds(List<Long> ids) ;
    List<Res_groups> getResGroupsByEntities(List<Res_groups> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expenseSearchContext;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expenseService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_hr.mapper.Hr_expenseMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[费用] 服务对象接口实现
 */
@Slf4j
@Service("Hr_expenseServiceImpl")
public class Hr_expenseServiceImpl extends EBSServiceImpl<Hr_expenseMapper, Hr_expense> implements IHr_expenseService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService hrExpenseSheetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService resCurrencyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "hr.expense" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Hr_expense et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_expenseService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Hr_expense> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Hr_expense et) {
        Hr_expense old = new Hr_expense() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_expenseService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_expenseService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Hr_expense> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountMoveLineService.resetByExpenseId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountMoveLineService.resetByExpenseId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Hr_expense get(Long key) {
        Hr_expense et = getById(key);
        if(et==null){
            et=new Hr_expense();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Hr_expense getDraft(Hr_expense et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Hr_expense et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Hr_expense et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Hr_expense et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Hr_expense> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Hr_expense> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Hr_expense> selectByAccountId(Long id) {
        return baseMapper.selectByAccountId(id);
    }
    @Override
    public void resetByAccountId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("account_id",null).eq("account_id",id));
    }

    @Override
    public void resetByAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("account_id",null).in("account_id",ids));
    }

    @Override
    public void removeByAccountId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("account_id",id));
    }

	@Override
    public List<Hr_expense> selectByAnalyticAccountId(Long id) {
        return baseMapper.selectByAnalyticAccountId(id);
    }
    @Override
    public void resetByAnalyticAccountId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("analytic_account_id",null).eq("analytic_account_id",id));
    }

    @Override
    public void resetByAnalyticAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("analytic_account_id",null).in("analytic_account_id",ids));
    }

    @Override
    public void removeByAnalyticAccountId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("analytic_account_id",id));
    }

	@Override
    public List<Hr_expense> selectByEmployeeId(Long id) {
        return baseMapper.selectByEmployeeId(id);
    }
    @Override
    public void resetByEmployeeId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("employee_id",null).eq("employee_id",id));
    }

    @Override
    public void resetByEmployeeId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("employee_id",null).in("employee_id",ids));
    }

    @Override
    public void removeByEmployeeId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("employee_id",id));
    }

	@Override
    public List<Hr_expense> selectBySheetId(Long id) {
        return baseMapper.selectBySheetId(id);
    }
    @Override
    public void resetBySheetId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("sheet_id",null).eq("sheet_id",id));
    }

    @Override
    public void resetBySheetId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("sheet_id",null).in("sheet_id",ids));
    }

    @Override
    public void removeBySheetId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("sheet_id",id));
    }

	@Override
    public List<Hr_expense> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("product_id",id));
    }

	@Override
    public List<Hr_expense> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("company_id",id));
    }

	@Override
    public List<Hr_expense> selectByCompanyCurrencyId(Long id) {
        return baseMapper.selectByCompanyCurrencyId(id);
    }
    @Override
    public void resetByCompanyCurrencyId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("company_currency_id",null).eq("company_currency_id",id));
    }

    @Override
    public void resetByCompanyCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("company_currency_id",null).in("company_currency_id",ids));
    }

    @Override
    public void removeByCompanyCurrencyId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("company_currency_id",id));
    }

	@Override
    public List<Hr_expense> selectByCurrencyId(Long id) {
        return baseMapper.selectByCurrencyId(id);
    }
    @Override
    public void resetByCurrencyId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("currency_id",null).eq("currency_id",id));
    }

    @Override
    public void resetByCurrencyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("currency_id",null).in("currency_id",ids));
    }

    @Override
    public void removeByCurrencyId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("currency_id",id));
    }

	@Override
    public List<Hr_expense> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("create_uid",id));
    }

	@Override
    public List<Hr_expense> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("write_uid",id));
    }

	@Override
    public List<Hr_expense> selectBySaleOrderId(Long id) {
        return baseMapper.selectBySaleOrderId(id);
    }
    @Override
    public void resetBySaleOrderId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("sale_order_id",null).eq("sale_order_id",id));
    }

    @Override
    public void resetBySaleOrderId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("sale_order_id",null).in("sale_order_id",ids));
    }

    @Override
    public void removeBySaleOrderId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("sale_order_id",id));
    }

	@Override
    public List<Hr_expense> selectByProductUomId(Long id) {
        return baseMapper.selectByProductUomId(id);
    }
    @Override
    public void resetByProductUomId(Long id) {
        this.update(new UpdateWrapper<Hr_expense>().set("product_uom_id",null).eq("product_uom_id",id));
    }

    @Override
    public void resetByProductUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_expense>().set("product_uom_id",null).in("product_uom_id",ids));
    }

    @Override
    public void removeByProductUomId(Long id) {
        this.remove(new QueryWrapper<Hr_expense>().eq("product_uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Hr_expense> searchDefault(Hr_expenseSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_expense> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_expense>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Hr_expense et){
        //实体关系[DER1N_HR_EXPENSE__ACCOUNT_ACCOUNT__ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount=et.getOdooAccount();
            if(ObjectUtils.isEmpty(odooAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountId());
                et.setOdooAccount(majorEntity);
                odooAccount=majorEntity;
            }
            et.setAccountIdText(odooAccount.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__ACCOUNT_ANALYTIC_ACCOUNT__ANALYTIC_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAnalyticAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount=et.getOdooAnalyticAccount();
            if(ObjectUtils.isEmpty(odooAnalyticAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAnalyticAccountId());
                et.setOdooAnalyticAccount(majorEntity);
                odooAnalyticAccount=majorEntity;
            }
            et.setAnalyticAccountIdText(odooAnalyticAccount.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__HR_EMPLOYEE__EMPLOYEE_ID]
        if(!ObjectUtils.isEmpty(et.getEmployeeId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee=et.getOdooEmployee();
            if(ObjectUtils.isEmpty(odooEmployee)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getEmployeeId());
                et.setOdooEmployee(majorEntity);
                odooEmployee=majorEntity;
            }
            et.setEmployeeIdText(odooEmployee.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__HR_EXPENSE_SHEET__SHEET_ID]
        if(!ObjectUtils.isEmpty(et.getSheetId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet odooSheet=et.getOdooSheet();
            if(ObjectUtils.isEmpty(odooSheet)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet majorEntity=hrExpenseSheetService.get(et.getSheetId());
                et.setOdooSheet(majorEntity);
                odooSheet=majorEntity;
            }
            et.setSheetIdText(odooSheet.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__RES_CURRENCY__COMPANY_CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCompanyCurrency=et.getOdooCompanyCurrency();
            if(ObjectUtils.isEmpty(odooCompanyCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCompanyCurrencyId());
                et.setOdooCompanyCurrency(majorEntity);
                odooCompanyCurrency=majorEntity;
            }
            et.setCompanyCurrencyIdText(odooCompanyCurrency.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__RES_CURRENCY__CURRENCY_ID]
        if(!ObjectUtils.isEmpty(et.getCurrencyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency=et.getOdooCurrency();
            if(ObjectUtils.isEmpty(odooCurrency)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency majorEntity=resCurrencyService.get(et.getCurrencyId());
                et.setOdooCurrency(majorEntity);
                odooCurrency=majorEntity;
            }
            et.setCurrencyIdText(odooCurrency.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__SALE_ORDER__SALE_ORDER_ID]
        if(!ObjectUtils.isEmpty(et.getSaleOrderId())){
            cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooSaleOrder=et.getOdooSaleOrder();
            if(ObjectUtils.isEmpty(odooSaleOrder)){
                cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order majorEntity=saleOrderService.get(et.getSaleOrderId());
                et.setOdooSaleOrder(majorEntity);
                odooSaleOrder=majorEntity;
            }
            et.setSaleOrderIdText(odooSaleOrder.getName());
        }
        //实体关系[DER1N_HR_EXPENSE__UOM_UOM__PRODUCT_UOM_ID]
        if(!ObjectUtils.isEmpty(et.getProductUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUomId());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomIdText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Hr_expense> getHrExpenseByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Hr_expense> getHrExpenseByEntities(List<Hr_expense> entities) {
        List ids =new ArrayList();
        for(Hr_expense entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




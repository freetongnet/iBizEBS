package cn.ibizlab.businesscentral.core.odoo_web_tour.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.businesscentral.core.odoo_web_tour.filter.Web_tour_tourSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[web_tour_tour] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-web-tour:odoo-web-tour}", contextId = "web-tour-tour", fallback = web_tour_tourFallback.class)
public interface web_tour_tourFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/search")
    Page<Web_tour_tour> search(@RequestBody Web_tour_tourSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/web_tour_tours/{id}")
    Web_tour_tour get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.PUT, value = "/web_tour_tours/{id}")
    Web_tour_tour update(@PathVariable("id") Long id,@RequestBody Web_tour_tour web_tour_tour);

    @RequestMapping(method = RequestMethod.PUT, value = "/web_tour_tours/batch")
    Boolean updateBatch(@RequestBody List<Web_tour_tour> web_tour_tours);


    @RequestMapping(method = RequestMethod.DELETE, value = "/web_tour_tours/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/web_tour_tours/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours")
    Web_tour_tour create(@RequestBody Web_tour_tour web_tour_tour);

    @RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/batch")
    Boolean createBatch(@RequestBody List<Web_tour_tour> web_tour_tours);


    @RequestMapping(method = RequestMethod.GET, value = "/web_tour_tours/select")
    Page<Web_tour_tour> select();


    @RequestMapping(method = RequestMethod.GET, value = "/web_tour_tours/getdraft")
    Web_tour_tour getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/checkkey")
    Boolean checkKey(@RequestBody Web_tour_tour web_tour_tour);


    @RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/save")
    Boolean save(@RequestBody Web_tour_tour web_tour_tour);

    @RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/savebatch")
    Boolean saveBatch(@RequestBody List<Web_tour_tour> web_tour_tours);



    @RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/searchdefault")
    Page<Web_tour_tour> searchDefault(@RequestBody Web_tour_tourSearchContext context);


}

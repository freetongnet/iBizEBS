package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_location_routeSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_location_routeService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_location_routeMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[库存路线] 服务对象接口实现
 */
@Slf4j
@Service("Stock_location_routeServiceImpl")
public class Stock_location_routeServiceImpl extends EBSServiceImpl<Stock_location_routeMapper, Stock_location_route> implements IStock_location_routeService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService stockRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.location.route" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_location_route et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_location_routeService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_location_route> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_location_route et) {
        Stock_location_route old = new Stock_location_route() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_location_routeService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_location_routeService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_location_route> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        if(!ObjectUtils.isEmpty(saleOrderLineService.selectByRouteId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Sale_order_line]数据，无法删除!","","");
        stockRuleService.removeByRouteId(key);
        if(!ObjectUtils.isEmpty(stockWarehouseService.selectByCrossdockRouteId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_warehouse]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(stockWarehouseService.selectByDeliveryRouteId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_warehouse]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(stockWarehouseService.selectByPbmRouteId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_warehouse]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(stockWarehouseService.selectByReceptionRouteId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_warehouse]数据，无法删除!","","");
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        if(!ObjectUtils.isEmpty(saleOrderLineService.selectByRouteId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Sale_order_line]数据，无法删除!","","");
        stockRuleService.removeByRouteId(idList);
        if(!ObjectUtils.isEmpty(stockWarehouseService.selectByCrossdockRouteId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_warehouse]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(stockWarehouseService.selectByDeliveryRouteId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_warehouse]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(stockWarehouseService.selectByPbmRouteId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_warehouse]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(stockWarehouseService.selectByReceptionRouteId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_warehouse]数据，无法删除!","","");
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_location_route get(Long key) {
        Stock_location_route et = getById(key);
        if(et==null){
            et=new Stock_location_route();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_location_route getDraft(Stock_location_route et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_location_route et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_location_route et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_location_route et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_location_route> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_location_route> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_location_route> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Stock_location_route>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_location_route>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Stock_location_route>().eq("company_id",id));
    }

	@Override
    public List<Stock_location_route> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_location_route>().eq("create_uid",id));
    }

	@Override
    public List<Stock_location_route> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_location_route>().eq("write_uid",id));
    }

	@Override
    public List<Stock_location_route> selectBySuppliedWhId(Long id) {
        return baseMapper.selectBySuppliedWhId(id);
    }
    @Override
    public void resetBySuppliedWhId(Long id) {
        this.update(new UpdateWrapper<Stock_location_route>().set("supplied_wh_id",null).eq("supplied_wh_id",id));
    }

    @Override
    public void resetBySuppliedWhId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_location_route>().set("supplied_wh_id",null).in("supplied_wh_id",ids));
    }

    @Override
    public void removeBySuppliedWhId(Long id) {
        this.remove(new QueryWrapper<Stock_location_route>().eq("supplied_wh_id",id));
    }

	@Override
    public List<Stock_location_route> selectBySupplierWhId(Long id) {
        return baseMapper.selectBySupplierWhId(id);
    }
    @Override
    public void resetBySupplierWhId(Long id) {
        this.update(new UpdateWrapper<Stock_location_route>().set("supplier_wh_id",null).eq("supplier_wh_id",id));
    }

    @Override
    public void resetBySupplierWhId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_location_route>().set("supplier_wh_id",null).in("supplier_wh_id",ids));
    }

    @Override
    public void removeBySupplierWhId(Long id) {
        this.remove(new QueryWrapper<Stock_location_route>().eq("supplier_wh_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_location_route> searchDefault(Stock_location_routeSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_location_route> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_location_route>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_location_route et){
        //实体关系[DER1N_STOCK_LOCATION_ROUTE__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_STOCK_LOCATION_ROUTE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_LOCATION_ROUTE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_STOCK_LOCATION_ROUTE__STOCK_WAREHOUSE__SUPPLIED_WH_ID]
        if(!ObjectUtils.isEmpty(et.getSuppliedWhId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooSuppliedWh=et.getOdooSuppliedWh();
            if(ObjectUtils.isEmpty(odooSuppliedWh)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse majorEntity=stockWarehouseService.get(et.getSuppliedWhId());
                et.setOdooSuppliedWh(majorEntity);
                odooSuppliedWh=majorEntity;
            }
            et.setSuppliedWhIdText(odooSuppliedWh.getName());
        }
        //实体关系[DER1N_STOCK_LOCATION_ROUTE__STOCK_WAREHOUSE__SUPPLIER_WH_ID]
        if(!ObjectUtils.isEmpty(et.getSupplierWhId())){
            cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse odooSupplierWh=et.getOdooSupplierWh();
            if(ObjectUtils.isEmpty(odooSupplierWh)){
                cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse majorEntity=stockWarehouseService.get(et.getSupplierWhId());
                et.setOdooSupplierWh(majorEntity);
                odooSupplierWh=majorEntity;
            }
            et.setSupplierWhIdText(odooSupplierWh.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_location_route> getStockLocationRouteByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_location_route> getStockLocationRouteByEntities(List<Stock_location_route> entities) {
        List ids =new ArrayList();
        for(Stock_location_route entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




package cn.ibizlab.businesscentral.core.odoo_hr.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[申请人]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "HR_APPLICANT",resultMap = "Hr_applicantResultMap")
public class Hr_applicant extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 评价
     */
    @TableField(value = "priority")
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;
    /**
     * 期望的薪酬
     */
    @DEField(name = "salary_expected")
    @TableField(value = "salary_expected")
    @JSONField(name = "salary_expected")
    @JsonProperty("salary_expected")
    private Double salaryExpected;
    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "categ_ids")
    @JsonProperty("categ_ids")
    private String categIds;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 提议薪酬
     */
    @DEField(name = "salary_proposed")
    @TableField(value = "salary_proposed")
    @JSONField(name = "salary_proposed")
    @JsonProperty("salary_proposed")
    private Double salaryProposed;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "attachment_number")
    @JsonProperty("attachment_number")
    private Integer attachmentNumber;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 信息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 电话
     */
    @DEField(name = "partner_phone")
    @TableField(value = "partner_phone")
    @JSONField(name = "partner_phone")
    @JsonProperty("partner_phone")
    private String partnerPhone;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 看板状态
     */
    @DEField(name = "kanban_state")
    @TableField(value = "kanban_state")
    @JSONField(name = "kanban_state")
    @JsonProperty("kanban_state")
    private String kanbanState;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 已指派
     */
    @DEField(name = "date_open")
    @TableField(value = "date_open")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_open" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_open")
    private Timestamp dateOpen;
    /**
     * 开启天数
     */
    @TableField(exist = false)
    @JSONField(name = "day_open")
    @JsonProperty("day_open")
    private Double dayOpen;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 可用量
     */
    @TableField(value = "availability")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "availability" , format="yyyy-MM-dd")
    @JsonProperty("availability")
    private Timestamp availability;
    /**
     * 期望薪资
     */
    @DEField(name = "salary_expected_extra")
    @TableField(value = "salary_expected_extra")
    @JSONField(name = "salary_expected_extra")
    @JsonProperty("salary_expected_extra")
    private String salaryExpectedExtra;
    /**
     * 已关闭
     */
    @DEField(name = "date_closed")
    @TableField(value = "date_closed")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_closed" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_closed")
    private Timestamp dateClosed;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 手机
     */
    @DEField(name = "partner_mobile")
    @TableField(value = "partner_mobile")
    @JSONField(name = "partner_mobile")
    @JsonProperty("partner_mobile")
    private String partnerMobile;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 薪酬标准
     */
    @DEField(name = "salary_proposed_extra")
    @TableField(value = "salary_proposed_extra")
    @JSONField(name = "salary_proposed_extra")
    @JsonProperty("salary_proposed_extra")
    private String salaryProposedExtra;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * EMail
     */
    @DEField(name = "email_from")
    @TableField(value = "email_from")
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 主题/应用 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 最后阶段更新
     */
    @DEField(name = "date_last_stage_update")
    @TableField(value = "date_last_stage_update")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_last_stage_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_last_stage_update")
    private Timestamp dateLastStageUpdate;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 引荐于
     */
    @TableField(value = "reference")
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;
    /**
     * 附件
     */
    @TableField(exist = false)
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;
    /**
     * 关注者的EMail
     */
    @DEField(name = "email_cc")
    @TableField(value = "email_cc")
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    private String emailCc;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 创建日期
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 申请人姓名
     */
    @DEField(name = "partner_name")
    @TableField(value = "partner_name")
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    private String partnerName;
    /**
     * 关闭日期
     */
    @TableField(exist = false)
    @JSONField(name = "day_close")
    @JsonProperty("day_close")
    private Double dayClose;
    /**
     * 概率
     */
    @TableField(value = "probability")
    @JSONField(name = "probability")
    @JsonProperty("probability")
    private Double probability;
    /**
     * 颜色索引
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 延迟关闭
     */
    @DEField(name = "delay_close")
    @TableField(value = "delay_close")
    @JSONField(name = "delay_close")
    @JsonProperty("delay_close")
    private Double delayClose;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 来源
     */
    @TableField(exist = false)
    @JSONField(name = "source_id_text")
    @JsonProperty("source_id_text")
    private String sourceIdText;
    /**
     * 营销
     */
    @TableField(exist = false)
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;
    /**
     * 部门
     */
    @TableField(exist = false)
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;
    /**
     * 最终阶段
     */
    @TableField(exist = false)
    @JSONField(name = "last_stage_id_text")
    @JsonProperty("last_stage_id_text")
    private String lastStageIdText;
    /**
     * 阶段
     */
    @TableField(exist = false)
    @JSONField(name = "stage_id_text")
    @JsonProperty("stage_id_text")
    private String stageIdText;
    /**
     * 媒体
     */
    @TableField(exist = false)
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;
    /**
     * 看板进展
     */
    @TableField(exist = false)
    @JSONField(name = "legend_normal")
    @JsonProperty("legend_normal")
    private String legendNormal;
    /**
     * 负责人
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 学历
     */
    @TableField(exist = false)
    @JSONField(name = "type_id_text")
    @JsonProperty("type_id_text")
    private String typeIdText;
    /**
     * 申请的职位
     */
    @TableField(exist = false)
    @JSONField(name = "job_id_text")
    @JsonProperty("job_id_text")
    private String jobIdText;
    /**
     * 看板有效
     */
    @TableField(exist = false)
    @JSONField(name = "legend_done")
    @JsonProperty("legend_done")
    private String legendDone;
    /**
     * 看板阻塞
     */
    @TableField(exist = false)
    @JSONField(name = "legend_blocked")
    @JsonProperty("legend_blocked")
    private String legendBlocked;
    /**
     * 员工姓名
     */
    @TableField(exist = false)
    @JSONField(name = "employee_name")
    @JsonProperty("employee_name")
    private String employeeName;
    /**
     * 用户EMail
     */
    @TableField(exist = false)
    @JSONField(name = "user_email")
    @JsonProperty("user_email")
    private String userEmail;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 联系
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 申请的职位
     */
    @DEField(name = "job_id")
    @TableField(value = "job_id")
    @JSONField(name = "job_id")
    @JsonProperty("job_id")
    private Long jobId;
    /**
     * 营销
     */
    @DEField(name = "campaign_id")
    @TableField(value = "campaign_id")
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Long campaignId;
    /**
     * 最终阶段
     */
    @DEField(name = "last_stage_id")
    @TableField(value = "last_stage_id")
    @JSONField(name = "last_stage_id")
    @JsonProperty("last_stage_id")
    private Long lastStageId;
    /**
     * 部门
     */
    @DEField(name = "department_id")
    @TableField(value = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Long departmentId;
    /**
     * 联系
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 媒体
     */
    @DEField(name = "medium_id")
    @TableField(value = "medium_id")
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Long mediumId;
    /**
     * 员工
     */
    @DEField(name = "emp_id")
    @TableField(value = "emp_id")
    @JSONField(name = "emp_id")
    @JsonProperty("emp_id")
    private Long empId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 学历
     */
    @DEField(name = "type_id")
    @TableField(value = "type_id")
    @JSONField(name = "type_id")
    @JsonProperty("type_id")
    private Long typeId;
    /**
     * 来源
     */
    @DEField(name = "source_id")
    @TableField(value = "source_id")
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Long sourceId;
    /**
     * 阶段
     */
    @DEField(name = "stage_id")
    @TableField(value = "stage_id")
    @JSONField(name = "stage_id")
    @JsonProperty("stage_id")
    private Long stageId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 负责人
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmp;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_job odooJob;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_degree odooType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_stage odooLastStage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_recruitment_stage odooStage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_campaign odooCampaign;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_medium odooMedium;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_utm.domain.Utm_source odooSource;



    /**
     * 设置 [评价]
     */
    public void setPriority(String priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [期望的薪酬]
     */
    public void setSalaryExpected(Double salaryExpected){
        this.salaryExpected = salaryExpected ;
        this.modify("salary_expected",salaryExpected);
    }

    /**
     * 设置 [提议薪酬]
     */
    public void setSalaryProposed(Double salaryProposed){
        this.salaryProposed = salaryProposed ;
        this.modify("salary_proposed",salaryProposed);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [电话]
     */
    public void setPartnerPhone(String partnerPhone){
        this.partnerPhone = partnerPhone ;
        this.modify("partner_phone",partnerPhone);
    }

    /**
     * 设置 [看板状态]
     */
    public void setKanbanState(String kanbanState){
        this.kanbanState = kanbanState ;
        this.modify("kanban_state",kanbanState);
    }

    /**
     * 设置 [已指派]
     */
    public void setDateOpen(Timestamp dateOpen){
        this.dateOpen = dateOpen ;
        this.modify("date_open",dateOpen);
    }

    /**
     * 格式化日期 [已指派]
     */
    public String formatDateOpen(){
        if (this.dateOpen == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateOpen);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [可用量]
     */
    public void setAvailability(Timestamp availability){
        this.availability = availability ;
        this.modify("availability",availability);
    }

    /**
     * 格式化日期 [可用量]
     */
    public String formatAvailability(){
        if (this.availability == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(availability);
    }
    /**
     * 设置 [期望薪资]
     */
    public void setSalaryExpectedExtra(String salaryExpectedExtra){
        this.salaryExpectedExtra = salaryExpectedExtra ;
        this.modify("salary_expected_extra",salaryExpectedExtra);
    }

    /**
     * 设置 [已关闭]
     */
    public void setDateClosed(Timestamp dateClosed){
        this.dateClosed = dateClosed ;
        this.modify("date_closed",dateClosed);
    }

    /**
     * 格式化日期 [已关闭]
     */
    public String formatDateClosed(){
        if (this.dateClosed == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateClosed);
    }
    /**
     * 设置 [手机]
     */
    public void setPartnerMobile(String partnerMobile){
        this.partnerMobile = partnerMobile ;
        this.modify("partner_mobile",partnerMobile);
    }

    /**
     * 设置 [薪酬标准]
     */
    public void setSalaryProposedExtra(String salaryProposedExtra){
        this.salaryProposedExtra = salaryProposedExtra ;
        this.modify("salary_proposed_extra",salaryProposedExtra);
    }

    /**
     * 设置 [EMail]
     */
    public void setEmailFrom(String emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [主题/应用 名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [最后阶段更新]
     */
    public void setDateLastStageUpdate(Timestamp dateLastStageUpdate){
        this.dateLastStageUpdate = dateLastStageUpdate ;
        this.modify("date_last_stage_update",dateLastStageUpdate);
    }

    /**
     * 格式化日期 [最后阶段更新]
     */
    public String formatDateLastStageUpdate(){
        if (this.dateLastStageUpdate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateLastStageUpdate);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [引荐于]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [关注者的EMail]
     */
    public void setEmailCc(String emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }

    /**
     * 设置 [申请人姓名]
     */
    public void setPartnerName(String partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }

    /**
     * 设置 [概率]
     */
    public void setProbability(Double probability){
        this.probability = probability ;
        this.modify("probability",probability);
    }

    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [延迟关闭]
     */
    public void setDelayClose(Double delayClose){
        this.delayClose = delayClose ;
        this.modify("delay_close",delayClose);
    }

    /**
     * 设置 [申请的职位]
     */
    public void setJobId(Long jobId){
        this.jobId = jobId ;
        this.modify("job_id",jobId);
    }

    /**
     * 设置 [营销]
     */
    public void setCampaignId(Long campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [最终阶段]
     */
    public void setLastStageId(Long lastStageId){
        this.lastStageId = lastStageId ;
        this.modify("last_stage_id",lastStageId);
    }

    /**
     * 设置 [部门]
     */
    public void setDepartmentId(Long departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }

    /**
     * 设置 [联系]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [媒体]
     */
    public void setMediumId(Long mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [员工]
     */
    public void setEmpId(Long empId){
        this.empId = empId ;
        this.modify("emp_id",empId);
    }

    /**
     * 设置 [学历]
     */
    public void setTypeId(Long typeId){
        this.typeId = typeId ;
        this.modify("type_id",typeId);
    }

    /**
     * 设置 [来源]
     */
    public void setSourceId(Long sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }

    /**
     * 设置 [阶段]
     */
    public void setStageId(Long stageId){
        this.stageId = stageId ;
        this.modify("stage_id",stageId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [负责人]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave_report;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leave_reportSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_leave_report] 服务对象接口
 */
public interface IHr_leave_reportService extends IService<Hr_leave_report>{

    boolean create(Hr_leave_report et) ;
    void createBatch(List<Hr_leave_report> list) ;
    boolean update(Hr_leave_report et) ;
    void updateBatch(List<Hr_leave_report> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_leave_report get(Long key) ;
    Hr_leave_report getDraft(Hr_leave_report et) ;
    boolean checkKey(Hr_leave_report et) ;
    boolean save(Hr_leave_report et) ;
    void saveBatch(List<Hr_leave_report> list) ;
    Page<Hr_leave_report> searchDefault(Hr_leave_reportSearchContext context) ;
    List<Hr_leave_report> selectByDepartmentId(Long id);
    void removeByDepartmentId(Long id);
    List<Hr_leave_report> selectByCategoryId(Long id);
    void removeByCategoryId(Long id);
    List<Hr_leave_report> selectByEmployeeId(Long id);
    void removeByEmployeeId(Long id);
    List<Hr_leave_report> selectByHolidayStatusId(Long id);
    void removeByHolidayStatusId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



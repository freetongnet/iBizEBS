package cn.ibizlab.businesscentral.core.odoo_im_livechat.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.filter.Im_livechat_report_operatorSearchContext;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_report_operatorService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_im_livechat.mapper.Im_livechat_report_operatorMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[实时聊天支持操作员报告] 服务对象接口实现
 */
@Slf4j
@Service("Im_livechat_report_operatorServiceImpl")
public class Im_livechat_report_operatorServiceImpl extends EBSServiceImpl<Im_livechat_report_operatorMapper, Im_livechat_report_operator> implements IIm_livechat_report_operatorService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_channelService imLivechatChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channelService mailChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "im.livechat.report.operator" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Im_livechat_report_operator et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IIm_livechat_report_operatorService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Im_livechat_report_operator> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Im_livechat_report_operator et) {
        Im_livechat_report_operator old = new Im_livechat_report_operator() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IIm_livechat_report_operatorService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IIm_livechat_report_operatorService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Im_livechat_report_operator> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Im_livechat_report_operator get(Long key) {
        Im_livechat_report_operator et = getById(key);
        if(et==null){
            et=new Im_livechat_report_operator();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Im_livechat_report_operator getDraft(Im_livechat_report_operator et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Im_livechat_report_operator et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Im_livechat_report_operator et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Im_livechat_report_operator et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Im_livechat_report_operator> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Im_livechat_report_operator> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Im_livechat_report_operator> selectByLivechatChannelId(Long id) {
        return baseMapper.selectByLivechatChannelId(id);
    }
    @Override
    public void removeByLivechatChannelId(Long id) {
        this.remove(new QueryWrapper<Im_livechat_report_operator>().eq("livechat_channel_id",id));
    }

	@Override
    public List<Im_livechat_report_operator> selectByChannelId(Long id) {
        return baseMapper.selectByChannelId(id);
    }
    @Override
    public void removeByChannelId(Long id) {
        this.remove(new QueryWrapper<Im_livechat_report_operator>().eq("channel_id",id));
    }

	@Override
    public List<Im_livechat_report_operator> selectByPartnerId(Long id) {
        return baseMapper.selectByPartnerId(id);
    }
    @Override
    public void removeByPartnerId(Long id) {
        this.remove(new QueryWrapper<Im_livechat_report_operator>().eq("partner_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Im_livechat_report_operator> searchDefault(Im_livechat_report_operatorSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Im_livechat_report_operator> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Im_livechat_report_operator>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Im_livechat_report_operator et){
        //实体关系[DER1N_IM_LIVECHAT_REPORT_OPERATOR__IM_LIVECHAT_CHANNEL__LIVECHAT_CHANNEL_ID]
        if(!ObjectUtils.isEmpty(et.getLivechatChannelId())){
            cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel odooLivechatChannel=et.getOdooLivechatChannel();
            if(ObjectUtils.isEmpty(odooLivechatChannel)){
                cn.ibizlab.businesscentral.core.odoo_im_livechat.domain.Im_livechat_channel majorEntity=imLivechatChannelService.get(et.getLivechatChannelId());
                et.setOdooLivechatChannel(majorEntity);
                odooLivechatChannel=majorEntity;
            }
            et.setLivechatChannelIdText(odooLivechatChannel.getName());
        }
        //实体关系[DER1N_IM_LIVECHAT_REPORT_OPERATOR__MAIL_CHANNEL__CHANNEL_ID]
        if(!ObjectUtils.isEmpty(et.getChannelId())){
            cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel odooChannel=et.getOdooChannel();
            if(ObjectUtils.isEmpty(odooChannel)){
                cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_channel majorEntity=mailChannelService.get(et.getChannelId());
                et.setOdooChannel(majorEntity);
                odooChannel=majorEntity;
            }
            et.setChannelIdText(odooChannel.getName());
        }
        //实体关系[DER1N_IM_LIVECHAT_REPORT_OPERATOR__RES_PARTNER__PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner=et.getOdooPartner();
            if(ObjectUtils.isEmpty(odooPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getPartnerId());
                et.setOdooPartner(majorEntity);
                odooPartner=majorEntity;
            }
            et.setPartnerIdText(odooPartner.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }




}




package cn.ibizlab.businesscentral.core.odoo_survey.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.businesscentral.core.odoo_survey.filter.Survey_labelSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Survey_label] 服务对象接口
 */
public interface ISurvey_labelService extends IService<Survey_label>{

    boolean create(Survey_label et) ;
    void createBatch(List<Survey_label> list) ;
    boolean update(Survey_label et) ;
    void updateBatch(List<Survey_label> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Survey_label get(Long key) ;
    Survey_label getDraft(Survey_label et) ;
    boolean checkKey(Survey_label et) ;
    boolean save(Survey_label et) ;
    void saveBatch(List<Survey_label> list) ;
    Page<Survey_label> searchDefault(Survey_labelSearchContext context) ;
    List<Survey_label> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Survey_label> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Survey_label> selectByQuestionId(Long id);
    void removeByQuestionId(Collection<Long> ids);
    void removeByQuestionId(Long id);
    List<Survey_label> selectByQuestionId2(Long id);
    void removeByQuestionId2(Collection<Long> ids);
    void removeByQuestionId2(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Survey_label> getSurveyLabelByIds(List<Long> ids) ;
    List<Survey_label> getSurveyLabelByEntities(List<Survey_label> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_crm.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_activity_report;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_activity_reportSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[crm_activity_report] 服务对象接口
 */
@Component
public class crm_activity_reportFallback implements crm_activity_reportFeignClient{


    public Page<Crm_activity_report> search(Crm_activity_reportSearchContext context){
            return null;
     }




    public Crm_activity_report create(Crm_activity_report crm_activity_report){
            return null;
     }
    public Boolean createBatch(List<Crm_activity_report> crm_activity_reports){
            return false;
     }

    public Crm_activity_report get(Long id){
            return null;
     }


    public Crm_activity_report update(Long id, Crm_activity_report crm_activity_report){
            return null;
     }
    public Boolean updateBatch(List<Crm_activity_report> crm_activity_reports){
            return false;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Page<Crm_activity_report> select(){
            return null;
     }

    public Crm_activity_report getDraft(){
            return null;
    }



    public Boolean checkKey(Crm_activity_report crm_activity_report){
            return false;
     }


    public Boolean save(Crm_activity_report crm_activity_report){
            return false;
     }
    public Boolean saveBatch(List<Crm_activity_report> crm_activity_reports){
            return false;
     }

    public Page<Crm_activity_report> searchDefault(Crm_activity_reportSearchContext context){
            return null;
     }


}

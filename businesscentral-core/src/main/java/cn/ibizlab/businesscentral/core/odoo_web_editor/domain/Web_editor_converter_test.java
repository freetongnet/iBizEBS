package cn.ibizlab.businesscentral.core.odoo_web_editor.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[Web编辑器转换器测试]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "WEB_EDITOR_CONVERTER_TEST",resultMap = "Web_editor_converter_testResultMap")
public class Web_editor_converter_test extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Text
     */
    @TableField(value = "text")
    @JSONField(name = "text")
    @JsonProperty("text")
    private String text;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * Lorsqu'un pancake prend l'avion à destination de Toronto et
     */
    @DEField(name = "selection_str")
    @TableField(value = "selection_str")
    @JSONField(name = "selection_str")
    @JsonProperty("selection_str")
    private String selectionStr;
    /**
     * Numeric
     */
    @TableField(value = "numeric")
    @JSONField(name = "numeric")
    @JsonProperty("numeric")
    private Double numeric;
    /**
     * Integer
     */
    @TableField(value = "integer")
    @JSONField(name = "integer")
    @JsonProperty("integer")
    private Integer integer;
    /**
     * Binary
     */
    @TableField(exist = false)
    @JSONField(name = "binary")
    @JsonProperty("binary")
    private byte[] binary;
    /**
     * 日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 浮点数
     */
    @TableField(value = "ibizfloat")
    @JSONField(name = "ibizfloat")
    @JsonProperty("ibizfloat")
    private Double ibizfloat;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * Datetime
     */
    @TableField(value = "datetime")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "datetime" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("datetime")
    private Timestamp datetime;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * Html
     */
    @TableField(value = "html")
    @JSONField(name = "html")
    @JsonProperty("html")
    private String html;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * Char
     */
    @TableField(value = "ibizchar")
    @JSONField(name = "ibizchar")
    @JsonProperty("ibizchar")
    private String ibizchar;
    /**
     * Selection
     */
    @TableField(value = "selection")
    @JSONField(name = "selection")
    @JsonProperty("selection")
    private String selection;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * Many2One
     */
    @TableField(exist = false)
    @JSONField(name = "many2one_text")
    @JsonProperty("many2one_text")
    private String many2oneText;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * Many2One
     */
    @TableField(value = "many2one")
    @JSONField(name = "many2one")
    @JsonProperty("many2one")
    private Long many2one;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test_sub odooMany2one;



    /**
     * 设置 [Text]
     */
    public void setText(String text){
        this.text = text ;
        this.modify("text",text);
    }

    /**
     * 设置 [Lorsqu'un pancake prend l'avion à destination de Toronto et]
     */
    public void setSelectionStr(String selectionStr){
        this.selectionStr = selectionStr ;
        this.modify("selection_str",selectionStr);
    }

    /**
     * 设置 [Numeric]
     */
    public void setNumeric(Double numeric){
        this.numeric = numeric ;
        this.modify("numeric",numeric);
    }

    /**
     * 设置 [Integer]
     */
    public void setInteger(Integer integer){
        this.integer = integer ;
        this.modify("integer",integer);
    }

    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
    /**
     * 设置 [浮点数]
     */
    public void setIbizfloat(Double ibizfloat){
        this.ibizfloat = ibizfloat ;
        this.modify("ibizfloat",ibizfloat);
    }

    /**
     * 设置 [Datetime]
     */
    public void setDatetime(Timestamp datetime){
        this.datetime = datetime ;
        this.modify("datetime",datetime);
    }

    /**
     * 格式化日期 [Datetime]
     */
    public String formatDatetime(){
        if (this.datetime == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(datetime);
    }
    /**
     * 设置 [Html]
     */
    public void setHtml(String html){
        this.html = html ;
        this.modify("html",html);
    }

    /**
     * 设置 [Char]
     */
    public void setIbizchar(String ibizchar){
        this.ibizchar = ibizchar ;
        this.modify("ibizchar",ibizchar);
    }

    /**
     * 设置 [Selection]
     */
    public void setSelection(String selection){
        this.selection = selection ;
        this.modify("selection",selection);
    }

    /**
     * 设置 [Many2One]
     */
    public void setMany2one(Long many2one){
        this.many2one = many2one ;
        this.modify("many2one",many2one);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



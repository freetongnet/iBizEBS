package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_activity;
/**
 * 关系型数据实体[Mail_activity] 查询条件对象
 */
@Slf4j
@Data
public class Mail_activitySearchContext extends QueryWrapperContext<Mail_activity> {

	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private Integer n_res_id_eq;//[相关文档编号]
	public void setN_res_id_eq(Integer n_res_id_eq) {
        this.n_res_id_eq = n_res_id_eq;
        if(!ObjectUtils.isEmpty(this.n_res_id_eq)){
            this.getSearchCond().eq("res_id", n_res_id_eq);
        }
    }
	private String n_res_model_eq;//[相关的文档模型]
	public void setN_res_model_eq(String n_res_model_eq) {
        this.n_res_model_eq = n_res_model_eq;
        if(!ObjectUtils.isEmpty(this.n_res_model_eq)){
            this.getSearchCond().eq("res_model", n_res_model_eq);
        }
    }
	private String n_user_id_text_eq;//[分派给]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[分派给]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_activity_type_id_text_eq;//[活动]
	public void setN_activity_type_id_text_eq(String n_activity_type_id_text_eq) {
        this.n_activity_type_id_text_eq = n_activity_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_type_id_text_eq)){
            this.getSearchCond().eq("activity_type_id_text", n_activity_type_id_text_eq);
        }
    }
	private String n_activity_type_id_text_like;//[活动]
	public void setN_activity_type_id_text_like(String n_activity_type_id_text_like) {
        this.n_activity_type_id_text_like = n_activity_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_activity_type_id_text_like)){
            this.getSearchCond().like("activity_type_id_text", n_activity_type_id_text_like);
        }
    }
	private String n_previous_activity_type_id_text_eq;//[前一活动类型]
	public void setN_previous_activity_type_id_text_eq(String n_previous_activity_type_id_text_eq) {
        this.n_previous_activity_type_id_text_eq = n_previous_activity_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_previous_activity_type_id_text_eq)){
            this.getSearchCond().eq("previous_activity_type_id_text", n_previous_activity_type_id_text_eq);
        }
    }
	private String n_previous_activity_type_id_text_like;//[前一活动类型]
	public void setN_previous_activity_type_id_text_like(String n_previous_activity_type_id_text_like) {
        this.n_previous_activity_type_id_text_like = n_previous_activity_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_previous_activity_type_id_text_like)){
            this.getSearchCond().like("previous_activity_type_id_text", n_previous_activity_type_id_text_like);
        }
    }
	private String n_note_id_text_eq;//[相关便签]
	public void setN_note_id_text_eq(String n_note_id_text_eq) {
        this.n_note_id_text_eq = n_note_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_note_id_text_eq)){
            this.getSearchCond().eq("note_id_text", n_note_id_text_eq);
        }
    }
	private String n_note_id_text_like;//[相关便签]
	public void setN_note_id_text_like(String n_note_id_text_like) {
        this.n_note_id_text_like = n_note_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_note_id_text_like)){
            this.getSearchCond().like("note_id_text", n_note_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_recommended_activity_type_id_text_eq;//[推荐的活动类型]
	public void setN_recommended_activity_type_id_text_eq(String n_recommended_activity_type_id_text_eq) {
        this.n_recommended_activity_type_id_text_eq = n_recommended_activity_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_recommended_activity_type_id_text_eq)){
            this.getSearchCond().eq("recommended_activity_type_id_text", n_recommended_activity_type_id_text_eq);
        }
    }
	private String n_recommended_activity_type_id_text_like;//[推荐的活动类型]
	public void setN_recommended_activity_type_id_text_like(String n_recommended_activity_type_id_text_like) {
        this.n_recommended_activity_type_id_text_like = n_recommended_activity_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_recommended_activity_type_id_text_like)){
            this.getSearchCond().like("recommended_activity_type_id_text", n_recommended_activity_type_id_text_like);
        }
    }
	private String n_calendar_event_id_text_eq;//[日历会议]
	public void setN_calendar_event_id_text_eq(String n_calendar_event_id_text_eq) {
        this.n_calendar_event_id_text_eq = n_calendar_event_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_calendar_event_id_text_eq)){
            this.getSearchCond().eq("calendar_event_id_text", n_calendar_event_id_text_eq);
        }
    }
	private String n_calendar_event_id_text_like;//[日历会议]
	public void setN_calendar_event_id_text_like(String n_calendar_event_id_text_like) {
        this.n_calendar_event_id_text_like = n_calendar_event_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_calendar_event_id_text_like)){
            this.getSearchCond().like("calendar_event_id_text", n_calendar_event_id_text_like);
        }
    }
	private Long n_recommended_activity_type_id_eq;//[推荐的活动类型]
	public void setN_recommended_activity_type_id_eq(Long n_recommended_activity_type_id_eq) {
        this.n_recommended_activity_type_id_eq = n_recommended_activity_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_recommended_activity_type_id_eq)){
            this.getSearchCond().eq("recommended_activity_type_id", n_recommended_activity_type_id_eq);
        }
    }
	private Long n_activity_type_id_eq;//[活动]
	public void setN_activity_type_id_eq(Long n_activity_type_id_eq) {
        this.n_activity_type_id_eq = n_activity_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_type_id_eq)){
            this.getSearchCond().eq("activity_type_id", n_activity_type_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_user_id_eq;//[分派给]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_note_id_eq;//[相关便签]
	public void setN_note_id_eq(Long n_note_id_eq) {
        this.n_note_id_eq = n_note_id_eq;
        if(!ObjectUtils.isEmpty(this.n_note_id_eq)){
            this.getSearchCond().eq("note_id", n_note_id_eq);
        }
    }
	private Long n_previous_activity_type_id_eq;//[前一活动类型]
	public void setN_previous_activity_type_id_eq(Long n_previous_activity_type_id_eq) {
        this.n_previous_activity_type_id_eq = n_previous_activity_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_previous_activity_type_id_eq)){
            this.getSearchCond().eq("previous_activity_type_id", n_previous_activity_type_id_eq);
        }
    }
	private Long n_calendar_event_id_eq;//[日历会议]
	public void setN_calendar_event_id_eq(Long n_calendar_event_id_eq) {
        this.n_calendar_event_id_eq = n_calendar_event_id_eq;
        if(!ObjectUtils.isEmpty(this.n_calendar_event_id_eq)){
            this.getSearchCond().eq("calendar_event_id", n_calendar_event_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




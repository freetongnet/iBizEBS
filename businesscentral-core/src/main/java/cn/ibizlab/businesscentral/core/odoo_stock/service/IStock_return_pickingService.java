package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_return_picking;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_return_pickingSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_return_picking] 服务对象接口
 */
public interface IStock_return_pickingService extends IService<Stock_return_picking>{

    boolean create(Stock_return_picking et) ;
    void createBatch(List<Stock_return_picking> list) ;
    boolean update(Stock_return_picking et) ;
    void updateBatch(List<Stock_return_picking> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_return_picking get(Long key) ;
    Stock_return_picking getDraft(Stock_return_picking et) ;
    boolean checkKey(Stock_return_picking et) ;
    boolean save(Stock_return_picking et) ;
    void saveBatch(List<Stock_return_picking> list) ;
    Page<Stock_return_picking> searchDefault(Stock_return_pickingSearchContext context) ;
    List<Stock_return_picking> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_return_picking> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_return_picking> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_return_picking> selectByOriginalLocationId(Long id);
    void resetByOriginalLocationId(Long id);
    void resetByOriginalLocationId(Collection<Long> ids);
    void removeByOriginalLocationId(Long id);
    List<Stock_return_picking> selectByParentLocationId(Long id);
    void resetByParentLocationId(Long id);
    void resetByParentLocationId(Collection<Long> ids);
    void removeByParentLocationId(Long id);
    List<Stock_return_picking> selectByPickingId(Long id);
    void resetByPickingId(Long id);
    void resetByPickingId(Collection<Long> ids);
    void removeByPickingId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_return_picking> getStockReturnPickingByIds(List<Long> ids) ;
    List<Stock_return_picking> getStockReturnPickingByEntities(List<Stock_return_picking> entities) ;
}



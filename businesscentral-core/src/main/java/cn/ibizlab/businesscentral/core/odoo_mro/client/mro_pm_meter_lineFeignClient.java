package cn.ibizlab.businesscentral.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_pm_meter_line;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_pm_meter_lineSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_pm_meter_line] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mro:odoo-mro}", contextId = "mro-pm-meter-line", fallback = mro_pm_meter_lineFallback.class)
public interface mro_pm_meter_lineFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_lines")
    Mro_pm_meter_line create(@RequestBody Mro_pm_meter_line mro_pm_meter_line);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_lines/batch")
    Boolean createBatch(@RequestBody List<Mro_pm_meter_line> mro_pm_meter_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_lines/{id}")
    Mro_pm_meter_line get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_lines/{id}")
    Mro_pm_meter_line update(@PathVariable("id") Long id,@RequestBody Mro_pm_meter_line mro_pm_meter_line);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meter_lines/batch")
    Boolean updateBatch(@RequestBody List<Mro_pm_meter_line> mro_pm_meter_lines);



    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_lines/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meter_lines/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_lines/search")
    Page<Mro_pm_meter_line> search(@RequestBody Mro_pm_meter_lineSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_lines/select")
    Page<Mro_pm_meter_line> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meter_lines/getdraft")
    Mro_pm_meter_line getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_lines/checkkey")
    Boolean checkKey(@RequestBody Mro_pm_meter_line mro_pm_meter_line);


    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_lines/save")
    Boolean save(@RequestBody Mro_pm_meter_line mro_pm_meter_line);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_lines/savebatch")
    Boolean saveBatch(@RequestBody List<Mro_pm_meter_line> mro_pm_meter_lines);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meter_lines/searchdefault")
    Page<Mro_pm_meter_line> searchDefault(@RequestBody Mro_pm_meter_lineSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_repair.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_cancelSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Repair_cancel] 服务对象接口
 */
public interface IRepair_cancelService extends IService<Repair_cancel>{

    boolean create(Repair_cancel et) ;
    void createBatch(List<Repair_cancel> list) ;
    boolean update(Repair_cancel et) ;
    void updateBatch(List<Repair_cancel> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Repair_cancel get(Long key) ;
    Repair_cancel getDraft(Repair_cancel et) ;
    boolean checkKey(Repair_cancel et) ;
    boolean save(Repair_cancel et) ;
    void saveBatch(List<Repair_cancel> list) ;
    Page<Repair_cancel> searchDefault(Repair_cancelSearchContext context) ;
    List<Repair_cancel> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Repair_cancel> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Repair_cancel> getRepairCancelByIds(List<Long> ids) ;
    List<Repair_cancel> getRepairCancelByEntities(List<Repair_cancel> entities) ;
}



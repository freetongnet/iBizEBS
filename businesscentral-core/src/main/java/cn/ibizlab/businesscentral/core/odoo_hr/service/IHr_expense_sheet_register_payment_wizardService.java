package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet_register_payment_wizard;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expense_sheet_register_payment_wizardSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_expense_sheet_register_payment_wizard] 服务对象接口
 */
public interface IHr_expense_sheet_register_payment_wizardService extends IService<Hr_expense_sheet_register_payment_wizard>{

    boolean create(Hr_expense_sheet_register_payment_wizard et) ;
    void createBatch(List<Hr_expense_sheet_register_payment_wizard> list) ;
    boolean update(Hr_expense_sheet_register_payment_wizard et) ;
    void updateBatch(List<Hr_expense_sheet_register_payment_wizard> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_expense_sheet_register_payment_wizard get(Long key) ;
    Hr_expense_sheet_register_payment_wizard getDraft(Hr_expense_sheet_register_payment_wizard et) ;
    boolean checkKey(Hr_expense_sheet_register_payment_wizard et) ;
    boolean save(Hr_expense_sheet_register_payment_wizard et) ;
    void saveBatch(List<Hr_expense_sheet_register_payment_wizard> list) ;
    Page<Hr_expense_sheet_register_payment_wizard> searchDefault(Hr_expense_sheet_register_payment_wizardSearchContext context) ;
    List<Hr_expense_sheet_register_payment_wizard> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Hr_expense_sheet_register_payment_wizard> selectByPaymentMethodId(Long id);
    void resetByPaymentMethodId(Long id);
    void resetByPaymentMethodId(Collection<Long> ids);
    void removeByPaymentMethodId(Long id);
    List<Hr_expense_sheet_register_payment_wizard> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Hr_expense_sheet_register_payment_wizard> selectByPartnerBankAccountId(Long id);
    void resetByPartnerBankAccountId(Long id);
    void resetByPartnerBankAccountId(Collection<Long> ids);
    void removeByPartnerBankAccountId(Long id);
    List<Hr_expense_sheet_register_payment_wizard> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Hr_expense_sheet_register_payment_wizard> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_expense_sheet_register_payment_wizard> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_expense_sheet_register_payment_wizard> getHrExpenseSheetRegisterPaymentWizardByIds(List<Long> ids) ;
    List<Hr_expense_sheet_register_payment_wizard> getHrExpenseSheetRegisterPaymentWizardByEntities(List<Hr_expense_sheet_register_payment_wizard> entities) ;
}



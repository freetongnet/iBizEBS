package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_task;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_taskSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_task] 服务对象接口
 */
public interface IMro_taskService extends IService<Mro_task>{

    boolean create(Mro_task et) ;
    void createBatch(List<Mro_task> list) ;
    boolean update(Mro_task et) ;
    void updateBatch(List<Mro_task> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_task get(Long key) ;
    Mro_task getDraft(Mro_task et) ;
    boolean checkKey(Mro_task et) ;
    boolean save(Mro_task et) ;
    void saveBatch(List<Mro_task> list) ;
    Page<Mro_task> searchDefault(Mro_taskSearchContext context) ;
    List<Mro_task> selectByCategoryId(Long id);
    List<Mro_task> selectByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Mro_task> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_task> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_task> getMroTaskByIds(List<Long> ids) ;
    List<Mro_task> getMroTaskByEntities(List<Mro_task> entities) ;
}



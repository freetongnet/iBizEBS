package cn.ibizlab.businesscentral.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[gamification_badge_user_wizard] 服务对象接口
 */
@Component
public class gamification_badge_user_wizardFallback implements gamification_badge_user_wizardFeignClient{



    public Gamification_badge_user_wizard get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Gamification_badge_user_wizard create(Gamification_badge_user_wizard gamification_badge_user_wizard){
            return null;
     }
    public Boolean createBatch(List<Gamification_badge_user_wizard> gamification_badge_user_wizards){
            return false;
     }

    public Page<Gamification_badge_user_wizard> search(Gamification_badge_user_wizardSearchContext context){
            return null;
     }


    public Gamification_badge_user_wizard update(Long id, Gamification_badge_user_wizard gamification_badge_user_wizard){
            return null;
     }
    public Boolean updateBatch(List<Gamification_badge_user_wizard> gamification_badge_user_wizards){
            return false;
     }



    public Page<Gamification_badge_user_wizard> select(){
            return null;
     }

    public Gamification_badge_user_wizard getDraft(){
            return null;
    }



    public Boolean checkKey(Gamification_badge_user_wizard gamification_badge_user_wizard){
            return false;
     }


    public Boolean save(Gamification_badge_user_wizard gamification_badge_user_wizard){
            return false;
     }
    public Boolean saveBatch(List<Gamification_badge_user_wizard> gamification_badge_user_wizards){
            return false;
     }

    public Page<Gamification_badge_user_wizard> searchDefault(Gamification_badge_user_wizardSearchContext context){
            return null;
     }


}

package cn.ibizlab.businesscentral.core.odoo_repair.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_repair.domain.Repair_cancel;
import cn.ibizlab.businesscentral.core.odoo_repair.filter.Repair_cancelSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Repair_cancelMapper extends BaseMapper<Repair_cancel>{

    Page<Repair_cancel> searchDefault(IPage page, @Param("srf") Repair_cancelSearchContext context, @Param("ew") Wrapper<Repair_cancel> wrapper) ;
    @Override
    Repair_cancel selectById(Serializable id);
    @Override
    int insert(Repair_cancel entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Repair_cancel entity);
    @Override
    int update(@Param(Constants.ENTITY) Repair_cancel entity, @Param("ew") Wrapper<Repair_cancel> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Repair_cancel> selectByCreateUid(@Param("id") Serializable id) ;

    List<Repair_cancel> selectByWriteUid(@Param("id") Serializable id) ;


}

package cn.ibizlab.businesscentral.core.odoo_calendar.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_alarmSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[calendar_alarm] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-calendar:odoo-calendar}", contextId = "calendar-alarm", fallback = calendar_alarmFallback.class)
public interface calendar_alarmFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms")
    Calendar_alarm create(@RequestBody Calendar_alarm calendar_alarm);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/batch")
    Boolean createBatch(@RequestBody List<Calendar_alarm> calendar_alarms);


    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarms/{id}")
    Calendar_alarm update(@PathVariable("id") Long id,@RequestBody Calendar_alarm calendar_alarm);

    @RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarms/batch")
    Boolean updateBatch(@RequestBody List<Calendar_alarm> calendar_alarms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarms/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarms/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/search")
    Page<Calendar_alarm> search(@RequestBody Calendar_alarmSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/calendar_alarms/{id}")
    Calendar_alarm get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_alarms/select")
    Page<Calendar_alarm> select();


    @RequestMapping(method = RequestMethod.GET, value = "/calendar_alarms/getdraft")
    Calendar_alarm getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/checkkey")
    Boolean checkKey(@RequestBody Calendar_alarm calendar_alarm);


    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/save")
    Boolean save(@RequestBody Calendar_alarm calendar_alarm);

    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/savebatch")
    Boolean saveBatch(@RequestBody List<Calendar_alarm> calendar_alarms);



    @RequestMapping(method = RequestMethod.POST, value = "/calendar_alarms/searchdefault")
    Page<Calendar_alarm> searchDefault(@RequestBody Calendar_alarmSearchContext context);


}

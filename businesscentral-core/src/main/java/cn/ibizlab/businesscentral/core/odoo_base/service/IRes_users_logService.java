package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_users_logSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_users_log] 服务对象接口
 */
public interface IRes_users_logService extends IService<Res_users_log>{

    boolean create(Res_users_log et) ;
    void createBatch(List<Res_users_log> list) ;
    boolean update(Res_users_log et) ;
    void updateBatch(List<Res_users_log> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_users_log get(Long key) ;
    Res_users_log getDraft(Res_users_log et) ;
    boolean checkKey(Res_users_log et) ;
    boolean save(Res_users_log et) ;
    void saveBatch(List<Res_users_log> list) ;
    Page<Res_users_log> searchDefault(Res_users_logSearchContext context) ;
    List<Res_users_log> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Res_users_log> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_users_log> getResUsersLogByIds(List<Long> ids) ;
    List<Res_users_log> getResUsersLogByEntities(List<Res_users_log> entities) ;
}



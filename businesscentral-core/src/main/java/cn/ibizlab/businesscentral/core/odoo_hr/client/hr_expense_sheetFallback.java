package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense_sheet;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_expense_sheetSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[hr_expense_sheet] 服务对象接口
 */
@Component
public class hr_expense_sheetFallback implements hr_expense_sheetFeignClient{

    public Hr_expense_sheet create(Hr_expense_sheet hr_expense_sheet){
            return null;
     }
    public Boolean createBatch(List<Hr_expense_sheet> hr_expense_sheets){
            return false;
     }

    public Page<Hr_expense_sheet> search(Hr_expense_sheetSearchContext context){
            return null;
     }




    public Hr_expense_sheet update(Long id, Hr_expense_sheet hr_expense_sheet){
            return null;
     }
    public Boolean updateBatch(List<Hr_expense_sheet> hr_expense_sheets){
            return false;
     }


    public Hr_expense_sheet get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Hr_expense_sheet> select(){
            return null;
     }

    public Hr_expense_sheet getDraft(){
            return null;
    }



    public Boolean checkKey(Hr_expense_sheet hr_expense_sheet){
            return false;
     }


    public Boolean save(Hr_expense_sheet hr_expense_sheet){
            return false;
     }
    public Boolean saveBatch(List<Hr_expense_sheet> hr_expense_sheets){
            return false;
     }

    public Page<Hr_expense_sheet> searchDefault(Hr_expense_sheetSearchContext context){
            return null;
     }


}

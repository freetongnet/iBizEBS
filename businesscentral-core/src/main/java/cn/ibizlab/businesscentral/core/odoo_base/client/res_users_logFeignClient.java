package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_users_logSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_users_log] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-users-log", fallback = res_users_logFallback.class)
public interface res_users_logFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/search")
    Page<Res_users_log> search(@RequestBody Res_users_logSearchContext context);



    @RequestMapping(method = RequestMethod.DELETE, value = "/res_users_logs/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_users_logs/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/res_users_logs")
    Res_users_log create(@RequestBody Res_users_log res_users_log);

    @RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/batch")
    Boolean createBatch(@RequestBody List<Res_users_log> res_users_logs);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_users_logs/{id}")
    Res_users_log update(@PathVariable("id") Long id,@RequestBody Res_users_log res_users_log);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_users_logs/batch")
    Boolean updateBatch(@RequestBody List<Res_users_log> res_users_logs);


    @RequestMapping(method = RequestMethod.GET, value = "/res_users_logs/{id}")
    Res_users_log get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.GET, value = "/res_users_logs/select")
    Page<Res_users_log> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_users_logs/getdraft")
    Res_users_log getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/checkkey")
    Boolean checkKey(@RequestBody Res_users_log res_users_log);


    @RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/save")
    Boolean save(@RequestBody Res_users_log res_users_log);

    @RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/savebatch")
    Boolean saveBatch(@RequestBody List<Res_users_log> res_users_logs);



    @RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/searchdefault")
    Page<Res_users_log> searchDefault(@RequestBody Res_users_logSearchContext context);


}

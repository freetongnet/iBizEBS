package cn.ibizlab.businesscentral.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_departmentSearchContext;
import cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_hr.mapper.Hr_departmentMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[HR 部门] 服务对象接口实现
 */
@Slf4j
@Service("Hr_departmentServiceImpl")
public class Hr_departmentServiceImpl extends EBSServiceImpl<Hr_departmentMapper, Hr_department> implements IHr_departmentService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicantService hrApplicantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_contractService hrContractService;

    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_departmentService hrDepartmentService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService hrExpenseSheetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_allocationService hrLeaveAllocationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leave_reportService hrLeaveReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_leaveService hrLeaveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipmentService maintenanceEquipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_requestService maintenanceRequestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "hr.department" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Hr_department et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_departmentService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Hr_department> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Hr_department et) {
        Hr_department old = new Hr_department() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_departmentService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IHr_departmentService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Hr_department> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        hrApplicantService.resetByDepartmentId(key);
        hrContractService.resetByDepartmentId(key);
        hrDepartmentService.resetByParentId(key);
        hrEmployeeService.resetByDepartmentId(key);
        hrExpenseSheetService.resetByDepartmentId(key);
        hrJobService.resetByDepartmentId(key);
        hrLeaveAllocationService.resetByDepartmentId(key);
        hrLeaveService.resetByDepartmentId(key);
        maintenanceEquipmentService.resetByDepartmentId(key);
        maintenanceRequestService.resetByDepartmentId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        hrApplicantService.resetByDepartmentId(idList);
        hrContractService.resetByDepartmentId(idList);
        hrDepartmentService.resetByParentId(idList);
        hrEmployeeService.resetByDepartmentId(idList);
        hrExpenseSheetService.resetByDepartmentId(idList);
        hrJobService.resetByDepartmentId(idList);
        hrLeaveAllocationService.resetByDepartmentId(idList);
        hrLeaveService.resetByDepartmentId(idList);
        maintenanceEquipmentService.resetByDepartmentId(idList);
        maintenanceRequestService.resetByDepartmentId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Hr_department get(Long key) {
        Hr_department et = getById(key);
        if(et==null){
            et=new Hr_department();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Hr_department getDraft(Hr_department et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Hr_department et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Hr_department et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Hr_department et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Hr_department> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Hr_department> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Hr_department> selectByParentId(Long id) {
        return baseMapper.selectByParentId(id);
    }
    @Override
    public void resetByParentId(Long id) {
        this.update(new UpdateWrapper<Hr_department>().set("parent_id",null).eq("parent_id",id));
    }

    @Override
    public void resetByParentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_department>().set("parent_id",null).in("parent_id",ids));
    }

    @Override
    public void removeByParentId(Long id) {
        this.remove(new QueryWrapper<Hr_department>().eq("parent_id",id));
    }

	@Override
    public List<Hr_department> selectByManagerId(Long id) {
        return baseMapper.selectByManagerId(id);
    }
    @Override
    public void resetByManagerId(Long id) {
        this.update(new UpdateWrapper<Hr_department>().set("manager_id",null).eq("manager_id",id));
    }

    @Override
    public void resetByManagerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_department>().set("manager_id",null).in("manager_id",ids));
    }

    @Override
    public void removeByManagerId(Long id) {
        this.remove(new QueryWrapper<Hr_department>().eq("manager_id",id));
    }

	@Override
    public List<Hr_department> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Hr_department>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Hr_department>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Hr_department>().eq("company_id",id));
    }

	@Override
    public List<Hr_department> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Hr_department>().eq("create_uid",id));
    }

	@Override
    public List<Hr_department> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Hr_department>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Hr_department> searchDefault(Hr_departmentSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_department> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_department>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 首选表格
     */
    @Override
    public Page<Hr_department> searchMaster(Hr_departmentSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_department> pages=baseMapper.searchMaster(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_department>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 根节点部门查询
     */
    @Override
    public Page<Hr_department> searchROOT(Hr_departmentSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Hr_department> pages=baseMapper.searchROOT(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Hr_department>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Hr_department et){
        //实体关系[DER1N_HR_DEPARTMENT__HR_DEPARTMENT__PARENT_ID]
        if(!ObjectUtils.isEmpty(et.getParentId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooParent=et.getOdooParent();
            if(ObjectUtils.isEmpty(odooParent)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department majorEntity=hrDepartmentService.get(et.getParentId());
                et.setOdooParent(majorEntity);
                odooParent=majorEntity;
            }
            et.setParentIdText(odooParent.getName());
        }
        //实体关系[DER1N_HR_DEPARTMENT__HR_EMPLOYEE__MANAGER_ID]
        if(!ObjectUtils.isEmpty(et.getManagerId())){
            cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooManager=et.getOdooManager();
            if(ObjectUtils.isEmpty(odooManager)){
                cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee majorEntity=hrEmployeeService.get(et.getManagerId());
                et.setOdooManager(majorEntity);
                odooManager=majorEntity;
            }
            et.setManagerIdText(odooManager.getName());
        }
        //实体关系[DER1N_HR_DEPARTMENT__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_HR_DEPARTMENT__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_HR_DEPARTMENT__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Hr_department> getHrDepartmentByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Hr_department> getHrDepartmentByEntities(List<Hr_department> entities) {
        List ids =new ArrayList();
        for(Hr_department entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




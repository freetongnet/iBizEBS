package cn.ibizlab.businesscentral.core.odoo_event.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[活动]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "EVENT_EVENT",resultMap = "Event_eventResultMap")
public class Event_event extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 参与者数目
     */
    @DEField(name = "seats_used")
    @TableField(value = "seats_used")
    @JSONField(name = "seats_used")
    @JsonProperty("seats_used")
    private Integer seatsUsed;
    /**
     * SEO优化
     */
    @TableField(exist = false)
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private Boolean isSeoOptimized;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 徽章字体
     */
    @DEField(name = "badge_front")
    @TableField(value = "badge_front")
    @JSONField(name = "badge_front")
    @JsonProperty("badge_front")
    private String badgeFront;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 邮件排程
     */
    @TableField(exist = false)
    @JSONField(name = "event_mail_ids")
    @JsonProperty("event_mail_ids")
    private String eventMailIds;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 预期的与会人员数量
     */
    @TableField(exist = false)
    @JSONField(name = "seats_expected")
    @JsonProperty("seats_expected")
    private Integer seatsExpected;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 与会者最多人数
     */
    @DEField(name = "seats_availability")
    @TableField(value = "seats_availability")
    @JSONField(name = "seats_availability")
    @JsonProperty("seats_availability")
    private String seatsAvailability;
    /**
     * 活动入场券
     */
    @TableField(exist = false)
    @JSONField(name = "event_ticket_ids")
    @JsonProperty("event_ticket_ids")
    private String eventTicketIds;
    /**
     * 未确认的席位预订
     */
    @DEField(name = "seats_unconfirmed")
    @TableField(value = "seats_unconfirmed")
    @JSONField(name = "seats_unconfirmed")
    @JsonProperty("seats_unconfirmed")
    private Integer seatsUnconfirmed;
    /**
     * 正在参加
     */
    @TableField(exist = false)
    @JSONField(name = "is_participating")
    @JsonProperty("is_participating")
    private Boolean isParticipating;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * Twitter主题标签
     */
    @DEField(name = "twitter_hashtag")
    @TableField(value = "twitter_hashtag")
    @JSONField(name = "twitter_hashtag")
    @JsonProperty("twitter_hashtag")
    private String twitterHashtag;
    /**
     * 与会者最多人数
     */
    @DEField(name = "seats_max")
    @TableField(value = "seats_max")
    @JSONField(name = "seats_max")
    @JsonProperty("seats_max")
    private Integer seatsMax;
    /**
     * 开始日期
     */
    @DEField(name = "date_begin")
    @TableField(value = "date_begin")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_begin" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_begin")
    private Timestamp dateBegin;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 网站opengraph图像
     */
    @DEField(name = "website_meta_og_img")
    @TableField(value = "website_meta_og_img")
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;
    /**
     * 在当前网站显示
     */
    @TableField(exist = false)
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 网站meta标题
     */
    @DEField(name = "website_meta_title")
    @TableField(value = "website_meta_title")
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 与会者
     */
    @TableField(exist = false)
    @JSONField(name = "registration_ids")
    @JsonProperty("registration_ids")
    private String registrationIds;
    /**
     * 说明
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 徽章内部右边
     */
    @DEField(name = "badge_innerright")
    @TableField(value = "badge_innerright")
    @JSONField(name = "badge_innerright")
    @JsonProperty("badge_innerright")
    private String badgeInnerright;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 网站元说明
     */
    @DEField(name = "website_meta_description")
    @TableField(value = "website_meta_description")
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;
    /**
     * 与会者最少数量
     */
    @DEField(name = "seats_min")
    @TableField(value = "seats_min")
    @JSONField(name = "seats_min")
    @JsonProperty("seats_min")
    private Integer seatsMin;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 徽章背面
     */
    @DEField(name = "badge_back")
    @TableField(value = "badge_back")
    @JSONField(name = "badge_back")
    @JsonProperty("badge_back")
    private String badgeBack;
    /**
     * 网站meta关键词
     */
    @DEField(name = "website_meta_keywords")
    @TableField(value = "website_meta_keywords")
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 自动确认注册
     */
    @DEField(name = "auto_confirm")
    @TableField(value = "auto_confirm")
    @JSONField(name = "auto_confirm")
    @JsonProperty("auto_confirm")
    private Boolean autoConfirm;
    /**
     * 结束日期
     */
    @DEField(name = "date_end")
    @TableField(value = "date_end")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_end")
    private Timestamp dateEnd;
    /**
     * 活动
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 徽章内部左边
     */
    @DEField(name = "badge_innerleft")
    @TableField(value = "badge_innerleft")
    @JSONField(name = "badge_innerleft")
    @JsonProperty("badge_innerleft")
    private String badgeInnerleft;
    /**
     * 预订席位
     */
    @DEField(name = "seats_reserved")
    @TableField(value = "seats_reserved")
    @JSONField(name = "seats_reserved")
    @JsonProperty("seats_reserved")
    private Integer seatsReserved;
    /**
     * 专用菜单
     */
    @DEField(name = "website_menu")
    @TableField(value = "website_menu")
    @JSONField(name = "website_menu")
    @JsonProperty("website_menu")
    private Boolean websiteMenu;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 定位最后日期
     */
    @TableField(exist = false)
    @JSONField(name = "date_end_located")
    @JsonProperty("date_end_located")
    private String dateEndLocated;
    /**
     * 可用席位
     */
    @DEField(name = "seats_available")
    @TableField(value = "seats_available")
    @JSONField(name = "seats_available")
    @JsonProperty("seats_available")
    private Integer seatsAvailable;
    /**
     * 已发布
     */
    @DEField(name = "is_published")
    @TableField(value = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 在线活动
     */
    @DEField(name = "is_online")
    @TableField(value = "is_online")
    @JSONField(name = "is_online")
    @JsonProperty("is_online")
    private Boolean isOnline;
    /**
     * 时区
     */
    @DEField(name = "date_tz")
    @TableField(value = "date_tz")
    @JSONField(name = "date_tz")
    @JsonProperty("date_tz")
    private String dateTz;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 定位开始日期
     */
    @TableField(exist = false)
    @JSONField(name = "date_begin_located")
    @JsonProperty("date_begin_located")
    private String dateBeginLocated;
    /**
     * 事件图标
     */
    @DEField(name = "event_logo")
    @TableField(value = "event_logo")
    @JSONField(name = "event_logo")
    @JsonProperty("event_logo")
    private String eventLogo;
    /**
     * 网站网址
     */
    @TableField(exist = false)
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;
    /**
     * 看板颜色索引
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 类别
     */
    @TableField(exist = false)
    @JSONField(name = "event_type_id_text")
    @JsonProperty("event_type_id_text")
    private String eventTypeIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 地点
     */
    @TableField(exist = false)
    @JSONField(name = "address_id_text")
    @JsonProperty("address_id_text")
    private String addressIdText;
    /**
     * 国家
     */
    @TableField(exist = false)
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;
    /**
     * 活动菜单
     */
    @TableField(exist = false)
    @JSONField(name = "menu_id_text")
    @JsonProperty("menu_id_text")
    private String menuIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 负责人
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 组织者
     */
    @TableField(exist = false)
    @JSONField(name = "organizer_id_text")
    @JsonProperty("organizer_id_text")
    private String organizerIdText;
    /**
     * 负责人
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 国家
     */
    @DEField(name = "country_id")
    @TableField(value = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Long countryId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 组织者
     */
    @DEField(name = "organizer_id")
    @TableField(value = "organizer_id")
    @JSONField(name = "organizer_id")
    @JsonProperty("organizer_id")
    private Long organizerId;
    /**
     * 活动菜单
     */
    @DEField(name = "menu_id")
    @TableField(value = "menu_id")
    @JSONField(name = "menu_id")
    @JsonProperty("menu_id")
    private Long menuId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 地点
     */
    @DEField(name = "address_id")
    @TableField(value = "address_id")
    @JSONField(name = "address_id")
    @JsonProperty("address_id")
    private Long addressId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 类别
     */
    @DEField(name = "event_type_id")
    @TableField(value = "event_type_id")
    @JSONField(name = "event_type_id")
    @JsonProperty("event_type_id")
    private Long eventTypeId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type odooEventType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooAddress;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooOrganizer;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_website.domain.Website_menu odooMenu;



    /**
     * 设置 [参与者数目]
     */
    public void setSeatsUsed(Integer seatsUsed){
        this.seatsUsed = seatsUsed ;
        this.modify("seats_used",seatsUsed);
    }

    /**
     * 设置 [徽章字体]
     */
    public void setBadgeFront(String badgeFront){
        this.badgeFront = badgeFront ;
        this.modify("badge_front",badgeFront);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [与会者最多人数]
     */
    public void setSeatsAvailability(String seatsAvailability){
        this.seatsAvailability = seatsAvailability ;
        this.modify("seats_availability",seatsAvailability);
    }

    /**
     * 设置 [未确认的席位预订]
     */
    public void setSeatsUnconfirmed(Integer seatsUnconfirmed){
        this.seatsUnconfirmed = seatsUnconfirmed ;
        this.modify("seats_unconfirmed",seatsUnconfirmed);
    }

    /**
     * 设置 [Twitter主题标签]
     */
    public void setTwitterHashtag(String twitterHashtag){
        this.twitterHashtag = twitterHashtag ;
        this.modify("twitter_hashtag",twitterHashtag);
    }

    /**
     * 设置 [与会者最多人数]
     */
    public void setSeatsMax(Integer seatsMax){
        this.seatsMax = seatsMax ;
        this.modify("seats_max",seatsMax);
    }

    /**
     * 设置 [开始日期]
     */
    public void setDateBegin(Timestamp dateBegin){
        this.dateBegin = dateBegin ;
        this.modify("date_begin",dateBegin);
    }

    /**
     * 格式化日期 [开始日期]
     */
    public String formatDateBegin(){
        if (this.dateBegin == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateBegin);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [网站opengraph图像]
     */
    public void setWebsiteMetaOgImg(String websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }

    /**
     * 设置 [网站meta标题]
     */
    public void setWebsiteMetaTitle(String websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }

    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [徽章内部右边]
     */
    public void setBadgeInnerright(String badgeInnerright){
        this.badgeInnerright = badgeInnerright ;
        this.modify("badge_innerright",badgeInnerright);
    }

    /**
     * 设置 [网站元说明]
     */
    public void setWebsiteMetaDescription(String websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }

    /**
     * 设置 [与会者最少数量]
     */
    public void setSeatsMin(Integer seatsMin){
        this.seatsMin = seatsMin ;
        this.modify("seats_min",seatsMin);
    }

    /**
     * 设置 [徽章背面]
     */
    public void setBadgeBack(String badgeBack){
        this.badgeBack = badgeBack ;
        this.modify("badge_back",badgeBack);
    }

    /**
     * 设置 [网站meta关键词]
     */
    public void setWebsiteMetaKeywords(String websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }

    /**
     * 设置 [自动确认注册]
     */
    public void setAutoConfirm(Boolean autoConfirm){
        this.autoConfirm = autoConfirm ;
        this.modify("auto_confirm",autoConfirm);
    }

    /**
     * 设置 [结束日期]
     */
    public void setDateEnd(Timestamp dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatDateEnd(){
        if (this.dateEnd == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateEnd);
    }
    /**
     * 设置 [活动]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [徽章内部左边]
     */
    public void setBadgeInnerleft(String badgeInnerleft){
        this.badgeInnerleft = badgeInnerleft ;
        this.modify("badge_innerleft",badgeInnerleft);
    }

    /**
     * 设置 [预订席位]
     */
    public void setSeatsReserved(Integer seatsReserved){
        this.seatsReserved = seatsReserved ;
        this.modify("seats_reserved",seatsReserved);
    }

    /**
     * 设置 [专用菜单]
     */
    public void setWebsiteMenu(Boolean websiteMenu){
        this.websiteMenu = websiteMenu ;
        this.modify("website_menu",websiteMenu);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [可用席位]
     */
    public void setSeatsAvailable(Integer seatsAvailable){
        this.seatsAvailable = seatsAvailable ;
        this.modify("seats_available",seatsAvailable);
    }

    /**
     * 设置 [已发布]
     */
    public void setIsPublished(Boolean isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [在线活动]
     */
    public void setIsOnline(Boolean isOnline){
        this.isOnline = isOnline ;
        this.modify("is_online",isOnline);
    }

    /**
     * 设置 [时区]
     */
    public void setDateTz(String dateTz){
        this.dateTz = dateTz ;
        this.modify("date_tz",dateTz);
    }

    /**
     * 设置 [事件图标]
     */
    public void setEventLogo(String eventLogo){
        this.eventLogo = eventLogo ;
        this.modify("event_logo",eventLogo);
    }

    /**
     * 设置 [看板颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [负责人]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [国家]
     */
    public void setCountryId(Long countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [组织者]
     */
    public void setOrganizerId(Long organizerId){
        this.organizerId = organizerId ;
        this.modify("organizer_id",organizerId);
    }

    /**
     * 设置 [活动菜单]
     */
    public void setMenuId(Long menuId){
        this.menuId = menuId ;
        this.modify("menu_id",menuId);
    }

    /**
     * 设置 [地点]
     */
    public void setAddressId(Long addressId){
        this.addressId = addressId ;
        this.modify("address_id",addressId);
    }

    /**
     * 设置 [类别]
     */
    public void setEventTypeId(Long eventTypeId){
        this.eventTypeId = eventTypeId ;
        this.modify("event_type_id",eventTypeId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



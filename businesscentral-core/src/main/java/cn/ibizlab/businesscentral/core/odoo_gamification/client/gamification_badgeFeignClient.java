package cn.ibizlab.businesscentral.core.odoo_gamification.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badgeSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[gamification_badge] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-gamification:odoo-gamification}", contextId = "gamification-badge", fallback = gamification_badgeFallback.class)
public interface gamification_badgeFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badges/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/gamification_badges/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_badges/{id}")
    Gamification_badge get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/search")
    Page<Gamification_badge> search(@RequestBody Gamification_badgeSearchContext context);




    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_badges/{id}")
    Gamification_badge update(@PathVariable("id") Long id,@RequestBody Gamification_badge gamification_badge);

    @RequestMapping(method = RequestMethod.PUT, value = "/gamification_badges/batch")
    Boolean updateBatch(@RequestBody List<Gamification_badge> gamification_badges);


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badges")
    Gamification_badge create(@RequestBody Gamification_badge gamification_badge);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/batch")
    Boolean createBatch(@RequestBody List<Gamification_badge> gamification_badges);


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_badges/select")
    Page<Gamification_badge> select();


    @RequestMapping(method = RequestMethod.GET, value = "/gamification_badges/getdraft")
    Gamification_badge getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/checkkey")
    Boolean checkKey(@RequestBody Gamification_badge gamification_badge);


    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/save")
    Boolean save(@RequestBody Gamification_badge gamification_badge);

    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/savebatch")
    Boolean saveBatch(@RequestBody List<Gamification_badge> gamification_badges);



    @RequestMapping(method = RequestMethod.POST, value = "/gamification_badges/searchdefault")
    Page<Gamification_badge> searchDefault(@RequestBody Gamification_badgeSearchContext context);


}

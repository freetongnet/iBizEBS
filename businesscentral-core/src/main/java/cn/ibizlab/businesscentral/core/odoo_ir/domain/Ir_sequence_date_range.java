package cn.ibizlab.businesscentral.core.odoo_ir.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[序列日期范围]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "IR_SEQUENCE_DATE_RANGE",resultMap = "Ir_sequence_date_rangeResultMap")
public class Ir_sequence_date_range extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    @DEField(name = "create_date")
    @TableField(value = "create_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 起始日期
     */
    @DEField(name = "date_from")
    @TableField(value = "date_from")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_from" , format="yyyy-MM-dd")
    @JsonProperty("date_from")
    private Timestamp dateFrom;
    /**
     * 结束日期
     */
    @DEField(name = "date_to")
    @TableField(value = "date_to")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_to" , format="yyyy-MM-dd")
    @JsonProperty("date_to")
    private Timestamp dateTo;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date")
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * next_char
     */
    @TableField(exist = false)
    @JSONField(name = "next_char")
    @JsonProperty("next_char")
    private String nextChar;
    /**
     * next_val
     */
    @TableField(exist = false)
    @JSONField(name = "next_val")
    @JsonProperty("next_val")
    private Integer nextVal;
    /**
     * Next
     */
    @DEField(name = "number_next")
    @TableField(value = "number_next")
    @JSONField(name = "number_next")
    @JsonProperty("number_next")
    private Integer numberNext;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uname")
    @JsonProperty("create_uname")
    private String createUname;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uname")
    @JsonProperty("write_uname")
    private String writeUname;
    /**
     * ID
     */
    @DEField(name = "sequence_id")
    @TableField(value = "sequence_id")
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    private Long sequenceId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid")
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid")
    @TableField(value = "create_uid")
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence odooSequence;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [创建时间]
     */
    public void setCreateDate(Timestamp createDate){
        this.createDate = createDate ;
        this.modify("create_date",createDate);
    }

    /**
     * 格式化日期 [创建时间]
     */
    public String formatCreateDate(){
        if (this.createDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(createDate);
    }
    /**
     * 设置 [起始日期]
     */
    public void setDateFrom(Timestamp dateFrom){
        this.dateFrom = dateFrom ;
        this.modify("date_from",dateFrom);
    }

    /**
     * 格式化日期 [起始日期]
     */
    public String formatDateFrom(){
        if (this.dateFrom == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateFrom);
    }
    /**
     * 设置 [结束日期]
     */
    public void setDateTo(Timestamp dateTo){
        this.dateTo = dateTo ;
        this.modify("date_to",dateTo);
    }

    /**
     * 格式化日期 [结束日期]
     */
    public String formatDateTo(){
        if (this.dateTo == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateTo);
    }
    /**
     * 设置 [最后更新时间]
     */
    public void setWriteDate(Timestamp writeDate){
        this.writeDate = writeDate ;
        this.modify("write_date",writeDate);
    }

    /**
     * 格式化日期 [最后更新时间]
     */
    public String formatWriteDate(){
        if (this.writeDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(writeDate);
    }
    /**
     * 设置 [Next]
     */
    public void setNumberNext(Integer numberNext){
        this.numberNext = numberNext ;
        this.modify("number_next",numberNext);
    }

    /**
     * 设置 [ID]
     */
    public void setSequenceId(Long sequenceId){
        this.sequenceId = sequenceId ;
        this.modify("sequence_id",sequenceId);
    }

    /**
     * 设置 [最后更新人]
     */
    public void setWriteUid(Long writeUid){
        this.writeUid = writeUid ;
        this.modify("write_uid",writeUid);
    }

    /**
     * 设置 [创建人]
     */
    public void setCreateUid(Long createUid){
        this.createUid = createUid ;
        this.modify("create_uid",createUid);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



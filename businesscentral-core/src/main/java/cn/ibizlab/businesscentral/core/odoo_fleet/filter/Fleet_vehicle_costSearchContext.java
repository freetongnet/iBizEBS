package cn.ibizlab.businesscentral.core.odoo_fleet.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_cost;
/**
 * 关系型数据实体[Fleet_vehicle_cost] 查询条件对象
 */
@Slf4j
@Data
public class Fleet_vehicle_costSearchContext extends QueryWrapperContext<Fleet_vehicle_cost> {

	private String n_cost_type_eq;//[费用所属类别]
	public void setN_cost_type_eq(String n_cost_type_eq) {
        this.n_cost_type_eq = n_cost_type_eq;
        if(!ObjectUtils.isEmpty(this.n_cost_type_eq)){
            this.getSearchCond().eq("cost_type", n_cost_type_eq);
        }
    }
	private String n_contract_id_text_eq;//[合同]
	public void setN_contract_id_text_eq(String n_contract_id_text_eq) {
        this.n_contract_id_text_eq = n_contract_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_contract_id_text_eq)){
            this.getSearchCond().eq("contract_id_text", n_contract_id_text_eq);
        }
    }
	private String n_contract_id_text_like;//[合同]
	public void setN_contract_id_text_like(String n_contract_id_text_like) {
        this.n_contract_id_text_like = n_contract_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_contract_id_text_like)){
            this.getSearchCond().like("contract_id_text", n_contract_id_text_like);
        }
    }
	private String n_name_eq;//[名称]
	public void setN_name_eq(String n_name_eq) {
        this.n_name_eq = n_name_eq;
        if(!ObjectUtils.isEmpty(this.n_name_eq)){
            this.getSearchCond().eq("name", n_name_eq);
        }
    }
	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_parent_id_text_eq;//[上级]
	public void setN_parent_id_text_eq(String n_parent_id_text_eq) {
        this.n_parent_id_text_eq = n_parent_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_eq)){
            this.getSearchCond().eq("parent_id_text", n_parent_id_text_eq);
        }
    }
	private String n_parent_id_text_like;//[上级]
	public void setN_parent_id_text_like(String n_parent_id_text_like) {
        this.n_parent_id_text_like = n_parent_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_like)){
            this.getSearchCond().like("parent_id_text", n_parent_id_text_like);
        }
    }
	private String n_cost_subtype_id_text_eq;//[类型]
	public void setN_cost_subtype_id_text_eq(String n_cost_subtype_id_text_eq) {
        this.n_cost_subtype_id_text_eq = n_cost_subtype_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_cost_subtype_id_text_eq)){
            this.getSearchCond().eq("cost_subtype_id_text", n_cost_subtype_id_text_eq);
        }
    }
	private String n_cost_subtype_id_text_like;//[类型]
	public void setN_cost_subtype_id_text_like(String n_cost_subtype_id_text_like) {
        this.n_cost_subtype_id_text_like = n_cost_subtype_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_cost_subtype_id_text_like)){
            this.getSearchCond().like("cost_subtype_id_text", n_cost_subtype_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_odometer_id_text_eq;//[里程表]
	public void setN_odometer_id_text_eq(String n_odometer_id_text_eq) {
        this.n_odometer_id_text_eq = n_odometer_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_odometer_id_text_eq)){
            this.getSearchCond().eq("odometer_id_text", n_odometer_id_text_eq);
        }
    }
	private String n_odometer_id_text_like;//[里程表]
	public void setN_odometer_id_text_like(String n_odometer_id_text_like) {
        this.n_odometer_id_text_like = n_odometer_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_odometer_id_text_like)){
            this.getSearchCond().like("odometer_id_text", n_odometer_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_cost_subtype_id_eq;//[类型]
	public void setN_cost_subtype_id_eq(Long n_cost_subtype_id_eq) {
        this.n_cost_subtype_id_eq = n_cost_subtype_id_eq;
        if(!ObjectUtils.isEmpty(this.n_cost_subtype_id_eq)){
            this.getSearchCond().eq("cost_subtype_id", n_cost_subtype_id_eq);
        }
    }
	private Long n_contract_id_eq;//[合同]
	public void setN_contract_id_eq(Long n_contract_id_eq) {
        this.n_contract_id_eq = n_contract_id_eq;
        if(!ObjectUtils.isEmpty(this.n_contract_id_eq)){
            this.getSearchCond().eq("contract_id", n_contract_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_parent_id_eq;//[上级]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_vehicle_id_eq;//[车辆]
	public void setN_vehicle_id_eq(Long n_vehicle_id_eq) {
        this.n_vehicle_id_eq = n_vehicle_id_eq;
        if(!ObjectUtils.isEmpty(this.n_vehicle_id_eq)){
            this.getSearchCond().eq("vehicle_id", n_vehicle_id_eq);
        }
    }
	private Long n_odometer_id_eq;//[里程表]
	public void setN_odometer_id_eq(Long n_odometer_id_eq) {
        this.n_odometer_id_eq = n_odometer_id_eq;
        if(!ObjectUtils.isEmpty(this.n_odometer_id_eq)){
            this.getSearchCond().eq("odometer_id", n_odometer_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




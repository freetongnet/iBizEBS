package cn.ibizlab.businesscentral.core.odoo_mro.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_order;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_orderSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mro_order] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mro:odoo-mro}", contextId = "mro-order", fallback = mro_orderFallback.class)
public interface mro_orderFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_orders/{id}")
    Mro_order update(@PathVariable("id") Long id,@RequestBody Mro_order mro_order);

    @RequestMapping(method = RequestMethod.PUT, value = "/mro_orders/batch")
    Boolean updateBatch(@RequestBody List<Mro_order> mro_orders);




    @RequestMapping(method = RequestMethod.POST, value = "/mro_orders")
    Mro_order create(@RequestBody Mro_order mro_order);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_orders/batch")
    Boolean createBatch(@RequestBody List<Mro_order> mro_orders);



    @RequestMapping(method = RequestMethod.GET, value = "/mro_orders/{id}")
    Mro_order get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_orders/search")
    Page<Mro_order> search(@RequestBody Mro_orderSearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_orders/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mro_orders/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/mro_orders/select")
    Page<Mro_order> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mro_orders/getdraft")
    Mro_order getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mro_orders/checkkey")
    Boolean checkKey(@RequestBody Mro_order mro_order);


    @RequestMapping(method = RequestMethod.POST, value = "/mro_orders/save")
    Boolean save(@RequestBody Mro_order mro_order);

    @RequestMapping(method = RequestMethod.POST, value = "/mro_orders/savebatch")
    Boolean saveBatch(@RequestBody List<Mro_order> mro_orders);



    @RequestMapping(method = RequestMethod.POST, value = "/mro_orders/searchdefault")
    Page<Mro_order> searchDefault(@RequestBody Mro_orderSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_lunch.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.businesscentral.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[lunch_cashmove] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-lunch:odoo-lunch}", contextId = "lunch-cashmove", fallback = lunch_cashmoveFallback.class)
public interface lunch_cashmoveFeignClient {




    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_cashmoves/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/lunch_cashmoves/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves")
    Lunch_cashmove create(@RequestBody Lunch_cashmove lunch_cashmove);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/batch")
    Boolean createBatch(@RequestBody List<Lunch_cashmove> lunch_cashmoves);


    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_cashmoves/{id}")
    Lunch_cashmove update(@PathVariable("id") Long id,@RequestBody Lunch_cashmove lunch_cashmove);

    @RequestMapping(method = RequestMethod.PUT, value = "/lunch_cashmoves/batch")
    Boolean updateBatch(@RequestBody List<Lunch_cashmove> lunch_cashmoves);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_cashmoves/{id}")
    Lunch_cashmove get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/search")
    Page<Lunch_cashmove> search(@RequestBody Lunch_cashmoveSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_cashmoves/select")
    Page<Lunch_cashmove> select();


    @RequestMapping(method = RequestMethod.GET, value = "/lunch_cashmoves/getdraft")
    Lunch_cashmove getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/checkkey")
    Boolean checkKey(@RequestBody Lunch_cashmove lunch_cashmove);


    @RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/save")
    Boolean save(@RequestBody Lunch_cashmove lunch_cashmove);

    @RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/savebatch")
    Boolean saveBatch(@RequestBody List<Lunch_cashmove> lunch_cashmoves);



    @RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/searchdefault")
    Page<Lunch_cashmove> searchDefault(@RequestBody Lunch_cashmoveSearchContext context);


}

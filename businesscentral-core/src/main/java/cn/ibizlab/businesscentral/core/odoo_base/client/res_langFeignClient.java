package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_lang;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_langSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_lang] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-lang", fallback = res_langFallback.class)
public interface res_langFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/res_langs/{id}")
    Res_lang get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/res_langs/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_langs/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_langs/{id}")
    Res_lang update(@PathVariable("id") Long id,@RequestBody Res_lang res_lang);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_langs/batch")
    Boolean updateBatch(@RequestBody List<Res_lang> res_langs);




    @RequestMapping(method = RequestMethod.POST, value = "/res_langs")
    Res_lang create(@RequestBody Res_lang res_lang);

    @RequestMapping(method = RequestMethod.POST, value = "/res_langs/batch")
    Boolean createBatch(@RequestBody List<Res_lang> res_langs);




    @RequestMapping(method = RequestMethod.POST, value = "/res_langs/search")
    Page<Res_lang> search(@RequestBody Res_langSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/res_langs/select")
    Page<Res_lang> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_langs/getdraft")
    Res_lang getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_langs/checkkey")
    Boolean checkKey(@RequestBody Res_lang res_lang);


    @RequestMapping(method = RequestMethod.POST, value = "/res_langs/save")
    Boolean save(@RequestBody Res_lang res_lang);

    @RequestMapping(method = RequestMethod.POST, value = "/res_langs/savebatch")
    Boolean saveBatch(@RequestBody List<Res_lang> res_langs);



    @RequestMapping(method = RequestMethod.POST, value = "/res_langs/searchdefault")
    Page<Res_lang> searchDefault(@RequestBody Res_langSearchContext context);


}

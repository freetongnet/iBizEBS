package cn.ibizlab.businesscentral.core.odoo_purchase.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[采购订单行]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "PURCHASE_ORDER_LINE",resultMap = "Purchase_order_lineResultMap")
public class Purchase_order_line extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 单价
     */
    @DEField(name = "price_unit")
    @TableField(value = "price_unit")
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;
    /**
     * 已接收数量
     */
    @DEField(name = "qty_received")
    @TableField(value = "qty_received")
    @JSONField(name = "qty_received")
    @JsonProperty("qty_received")
    private Double qtyReceived;
    /**
     * 总计
     */
    @DEField(name = "price_total")
    @TableField(value = "price_total")
    @JSONField(name = "price_total")
    @JsonProperty("price_total")
    private BigDecimal priceTotal;
    /**
     * 说明
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 数量
     */
    @DEField(name = "product_qty")
    @TableField(value = "product_qty")
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;
    /**
     * 税率
     */
    @DEField(name = "price_tax")
    @TableField(value = "price_tax")
    @JSONField(name = "price_tax")
    @JsonProperty("price_tax")
    private Double priceTax;
    /**
     * 保留
     */
    @TableField(exist = false)
    @JSONField(name = "move_ids")
    @JsonProperty("move_ids")
    private String moveIds;
    /**
     * 小计
     */
    @DEField(name = "price_subtotal")
    @TableField(value = "price_subtotal")
    @JSONField(name = "price_subtotal")
    @JsonProperty("price_subtotal")
    private BigDecimal priceSubtotal;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 序列
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 数量总计
     */
    @DEField(name = "product_uom_qty")
    @TableField(value = "product_uom_qty")
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;
        
    /**
     * 税率
     */
    @TableField(exist = false)
    @JSONField(name = "taxes_id")
    @JsonProperty("taxes_id")
    private String taxesId;    
    /**
     * 账单明细行
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_lines")
    @JsonProperty("invoice_lines")
    private String invoiceLines;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 计划日期
     */
    @DEField(name = "date_planned")
    @TableField(value = "date_planned")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned")
    private Timestamp datePlanned;
    /**
     * 开票数量
     */
    @DEField(name = "qty_invoiced")
    @TableField(value = "qty_invoiced")
    @JSONField(name = "qty_invoiced")
    @JsonProperty("qty_invoiced")
    private Double qtyInvoiced;
    /**
     * 分析标签
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;
    /**
     * 下游移动
     */
    @TableField(exist = false)
    @JSONField(name = "move_dest_ids")
    @JsonProperty("move_dest_ids")
    private String moveDestIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 订货点
     */
    @TableField(exist = false)
    @JSONField(name = "orderpoint_id_text")
    @JsonProperty("orderpoint_id_text")
    private String orderpointIdText;
    /**
     * 销售订单
     */
    @TableField(exist = false)
    @JSONField(name = "sale_order_id_text")
    @JsonProperty("sale_order_id_text")
    private String saleOrderIdText;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;
    /**
     * 产品类型
     */
    @TableField(exist = false)
    @JSONField(name = "product_type")
    @JsonProperty("product_type")
    private String productType;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 状态
     */
    @TableField(exist = false)
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 分析账户
     */
    @TableField(exist = false)
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    private String accountAnalyticIdText;
    /**
     * 产品图片
     */
    @TableField(exist = false)
    @JSONField(name = "product_image")
    @JsonProperty("product_image")
    private byte[] productImage;
    /**
     * 计量单位
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;
    /**
     * 订单关联
     */
    @TableField(exist = false)
    @JSONField(name = "order_id_text")
    @JsonProperty("order_id_text")
    private String orderIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 原销售项
     */
    @TableField(exist = false)
    @JSONField(name = "sale_line_id_text")
    @JsonProperty("sale_line_id_text")
    private String saleLineIdText;
    /**
     * 计量单位类别
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_category_id")
    @JsonProperty("product_uom_category_id")
    private Long productUomCategoryId;
    /**
     * 单据日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_order" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_order")
    private Timestamp dateOrder;
    /**
     * 公司
     */
    @DEField(name = "company_id" , preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 分析账户
     */
    @DEField(name = "account_analytic_id")
    @TableField(value = "account_analytic_id")
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    private Long accountAnalyticId;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 销售订单
     */
    @DEField(name = "sale_order_id")
    @TableField(value = "sale_order_id")
    @JSONField(name = "sale_order_id")
    @JsonProperty("sale_order_id")
    private Long saleOrderId;
    /**
     * 计量单位
     */
    @DEField(name = "product_uom")
    @TableField(value = "product_uom")
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Long productUom;
    /**
     * 原销售项
     */
    @DEField(name = "sale_line_id")
    @TableField(value = "sale_line_id")
    @JSONField(name = "sale_line_id")
    @JsonProperty("sale_line_id")
    private Long saleLineId;
    /**
     * 订单关联
     */
    @DEField(name = "order_id")
    @TableField(value = "order_id")
    @JSONField(name = "order_id")
    @JsonProperty("order_id")
    private Long orderId;
    /**
     * 订货点
     */
    @DEField(name = "orderpoint_id")
    @TableField(value = "orderpoint_id")
    @JSONField(name = "orderpoint_id")
    @JsonProperty("orderpoint_id")
    private Long orderpointId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 产品
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAccountAnalytic;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_order odooOrder;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_line odooSaleLine;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order odooSaleOrder;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_warehouse_orderpoint odooOrderpoint;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom;



    /**
     * 设置 [单价]
     */
    public void setPriceUnit(Double priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [已接收数量]
     */
    public void setQtyReceived(Double qtyReceived){
        this.qtyReceived = qtyReceived ;
        this.modify("qty_received",qtyReceived);
    }

    /**
     * 设置 [总计]
     */
    public void setPriceTotal(BigDecimal priceTotal){
        this.priceTotal = priceTotal ;
        this.modify("price_total",priceTotal);
    }

    /**
     * 设置 [说明]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [数量]
     */
    public void setProductQty(Double productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [税率]
     */
    public void setPriceTax(Double priceTax){
        this.priceTax = priceTax ;
        this.modify("price_tax",priceTax);
    }

    /**
     * 设置 [小计]
     */
    public void setPriceSubtotal(BigDecimal priceSubtotal){
        this.priceSubtotal = priceSubtotal ;
        this.modify("price_subtotal",priceSubtotal);
    }

    /**
     * 设置 [序列]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [数量总计]
     */
    public void setProductUomQty(Double productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [税率]
     */
    public void setTaxesId(String taxesId){
        this.taxesId = taxesId ;
        this.modify("taxes_id",taxesId);
    }
    
    /**
     * 设置 [计划日期]
     */
    public void setDatePlanned(Timestamp datePlanned){
        this.datePlanned = datePlanned ;
        this.modify("date_planned",datePlanned);
    }

    /**
     * 格式化日期 [计划日期]
     */
    public String formatDatePlanned(){
        if (this.datePlanned == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(datePlanned);
    }
    /**
     * 设置 [开票数量]
     */
    public void setQtyInvoiced(Double qtyInvoiced){
        this.qtyInvoiced = qtyInvoiced ;
        this.modify("qty_invoiced",qtyInvoiced);
    }

    /**
     * 设置 [分析账户]
     */
    public void setAccountAnalyticId(Long accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [销售订单]
     */
    public void setSaleOrderId(Long saleOrderId){
        this.saleOrderId = saleOrderId ;
        this.modify("sale_order_id",saleOrderId);
    }

    /**
     * 设置 [计量单位]
     */
    public void setProductUom(Long productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [原销售项]
     */
    public void setSaleLineId(Long saleLineId){
        this.saleLineId = saleLineId ;
        this.modify("sale_line_id",saleLineId);
    }

    /**
     * 设置 [订单关联]
     */
    public void setOrderId(Long orderId){
        this.orderId = orderId ;
        this.modify("order_id",orderId);
    }

    /**
     * 设置 [订货点]
     */
    public void setOrderpointId(Long orderpointId){
        this.orderpointId = orderpointId ;
        this.modify("orderpoint_id",orderpointId);
    }

    /**
     * 设置 [产品]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



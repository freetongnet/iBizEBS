package cn.ibizlab.businesscentral.core.odoo_product.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.businesscentral.core.odoo_product.filter.Product_replenishSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Product_replenishMapper extends BaseMapper<Product_replenish>{

    Page<Product_replenish> searchDefault(IPage page, @Param("srf") Product_replenishSearchContext context, @Param("ew") Wrapper<Product_replenish> wrapper) ;
    @Override
    Product_replenish selectById(Serializable id);
    @Override
    int insert(Product_replenish entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Product_replenish entity);
    @Override
    int update(@Param(Constants.ENTITY) Product_replenish entity, @Param("ew") Wrapper<Product_replenish> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Product_replenish> selectByProductId(@Param("id") Serializable id) ;

    List<Product_replenish> selectByProductTmplId(@Param("id") Serializable id) ;

    List<Product_replenish> selectByCreateUid(@Param("id") Serializable id) ;

    List<Product_replenish> selectByWriteUid(@Param("id") Serializable id) ;

    List<Product_replenish> selectByWarehouseId(@Param("id") Serializable id) ;

    List<Product_replenish> selectByProductUomId(@Param("id") Serializable id) ;


}

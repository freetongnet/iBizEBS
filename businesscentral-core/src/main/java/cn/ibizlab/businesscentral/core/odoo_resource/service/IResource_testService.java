package cn.ibizlab.businesscentral.core.odoo_resource.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.businesscentral.core.odoo_resource.filter.Resource_testSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Resource_test] 服务对象接口
 */
public interface IResource_testService extends IService<Resource_test>{

    boolean create(Resource_test et) ;
    void createBatch(List<Resource_test> list) ;
    boolean update(Resource_test et) ;
    void updateBatch(List<Resource_test> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Resource_test get(Long key) ;
    Resource_test getDraft(Resource_test et) ;
    boolean checkKey(Resource_test et) ;
    boolean save(Resource_test et) ;
    void saveBatch(List<Resource_test> list) ;
    Page<Resource_test> searchDefault(Resource_testSearchContext context) ;
    List<Resource_test> selectByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Collection<Long> ids);
    void removeByResourceCalendarId(Long id);
    List<Resource_test> selectByResourceId(Long id);
    List<Resource_test> selectByResourceId(Collection<Long> ids);
    void removeByResourceId(Long id);
    List<Resource_test> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Resource_test> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Resource_test> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Resource_test> getResourceTestByIds(List<Long> ids) ;
    List<Resource_test> getResourceTestByEntities(List<Resource_test> entities) ;
}



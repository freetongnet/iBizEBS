package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[发送会计发票]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_INVOICE_SEND",resultMap = "Account_invoice_sendResultMap")
public class Account_invoice_send extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 相关评级
     */
    @TableField(exist = false)
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;
    /**
     * 追踪值
     */
    @TableField(exist = false)
    @JSONField(name = "tracking_value_ids")
    @JsonProperty("tracking_value_ids")
    private String trackingValueIds;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;
    /**
     * 邮件列表
     */
    @TableField(exist = false)
    @JSONField(name = "mailing_list_ids")
    @JsonProperty("mailing_list_ids")
    private String mailingListIds;
    /**
     * 已打印
     */
    @TableField(value = "printed")
    @JSONField(name = "printed")
    @JsonProperty("printed")
    private Boolean printed;
    /**
     * 附件
     */
    @TableField(exist = false)
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;
    /**
     * 不发送的发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_without_email")
    @JsonProperty("invoice_without_email")
    private String invoiceWithoutEmail;
    /**
     * 待处理的业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "needaction_partner_ids")
    @JsonProperty("needaction_partner_ids")
    private String needactionPartnerIds;
    /**
     * 透过邮递
     */
    @DEField(name = "snailmail_is_letter")
    @TableField(value = "snailmail_is_letter")
    @JSONField(name = "snailmail_is_letter")
    @JsonProperty("snailmail_is_letter")
    private Boolean snailmailIsLetter;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 打印
     */
    @DEField(name = "is_print")
    @TableField(value = "is_print")
    @JSONField(name = "is_print")
    @JsonProperty("is_print")
    private Boolean isPrint;
    /**
     * 业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;
    /**
     * 邮戳(s)
     */
    @DEField(name = "snailmail_cost")
    @TableField(value = "snailmail_cost")
    @JSONField(name = "snailmail_cost")
    @JsonProperty("snailmail_cost")
    private Double snailmailCost;
    /**
     * 频道
     */
    @TableField(exist = false)
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 收藏夹
     */
    @TableField(exist = false)
    @JSONField(name = "starred_partner_ids")
    @JsonProperty("starred_partner_ids")
    private String starredPartnerIds;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 额外的联系人
     */
    @TableField(exist = false)
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;
    /**
     * 下级信息
     */
    @TableField(exist = false)
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;
    /**
     * 信
     */
    @TableField(exist = false)
    @JSONField(name = "letter_ids")
    @JsonProperty("letter_ids")
    private String letterIds;
    /**
     * 通知
     */
    @TableField(exist = false)
    @JSONField(name = "notification_ids")
    @JsonProperty("notification_ids")
    private String notificationIds;
    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;
    /**
     * EMail
     */
    @DEField(name = "is_email")
    @TableField(value = "is_email")
    @JSONField(name = "is_email")
    @JsonProperty("is_email")
    private Boolean isEmail;
    /**
     * 上级消息
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 使用有效域
     */
    @TableField(exist = false)
    @JSONField(name = "use_active_domain")
    @JsonProperty("use_active_domain")
    private Boolean useActiveDomain;
    /**
     * 审核状态
     */
    @TableField(exist = false)
    @JSONField(name = "moderation_status")
    @JsonProperty("moderation_status")
    private String moderationStatus;
    /**
     * 审核人
     */
    @TableField(exist = false)
    @JSONField(name = "moderator_id")
    @JsonProperty("moderator_id")
    private Long moderatorId;
    /**
     * 有误差
     */
    @TableField(exist = false)
    @JSONField(name = "has_error")
    @JsonProperty("has_error")
    private Boolean hasError;
    /**
     * 有效域
     */
    @TableField(exist = false)
    @JSONField(name = "active_domain")
    @JsonProperty("active_domain")
    private String activeDomain;
    /**
     * 说明
     */
    @TableField(exist = false)
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 线程无应答
     */
    @TableField(exist = false)
    @JSONField(name = "no_auto_thread")
    @JsonProperty("no_auto_thread")
    private Boolean noAutoThread;
    /**
     * 写作模式
     */
    @TableField(exist = false)
    @JSONField(name = "composition_mode")
    @JsonProperty("composition_mode")
    private String compositionMode;
    /**
     * 添加签名
     */
    @TableField(exist = false)
    @JSONField(name = "add_sign")
    @JsonProperty("add_sign")
    private Boolean addSign;
    /**
     * 来自
     */
    @TableField(exist = false)
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;
    /**
     * 日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 邮件活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    private Long mailActivityTypeId;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 待处理
     */
    @TableField(exist = false)
    @JSONField(name = "needaction")
    @JsonProperty("needaction")
    private Boolean needaction;
    /**
     * 主题
     */
    @TableField(exist = false)
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * 消息记录名称
     */
    @TableField(exist = false)
    @JSONField(name = "record_name")
    @JsonProperty("record_name")
    private String recordName;
    /**
     * 作者头像
     */
    @TableField(exist = false)
    @JSONField(name = "author_avatar")
    @JsonProperty("author_avatar")
    private byte[] authorAvatar;
    /**
     * 评级值
     */
    @TableField(exist = false)
    @JSONField(name = "rating_value")
    @JsonProperty("rating_value")
    private Double ratingValue;
    /**
     * 删除邮件
     */
    @TableField(exist = false)
    @JSONField(name = "auto_delete")
    @JsonProperty("auto_delete")
    private Boolean autoDelete;
    /**
     * 删除消息副本
     */
    @TableField(exist = false)
    @JSONField(name = "auto_delete_message")
    @JsonProperty("auto_delete_message")
    private Boolean autoDeleteMessage;
    /**
     * 内容
     */
    @TableField(exist = false)
    @JSONField(name = "body")
    @JsonProperty("body")
    private String body;
    /**
     * 邮件发送服务器
     */
    @TableField(exist = false)
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;
    /**
     * Message-Id
     */
    @TableField(exist = false)
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;
    /**
     * 相关的文档模型
     */
    @TableField(exist = false)
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;
    /**
     * 回复 至
     */
    @TableField(exist = false)
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "message_type")
    @JsonProperty("message_type")
    private String messageType;
    /**
     * 标星号邮件
     */
    @TableField(exist = false)
    @JSONField(name = "starred")
    @JsonProperty("starred")
    private Boolean starred;
    /**
     * 记录内部备注
     */
    @TableField(exist = false)
    @JSONField(name = "is_log")
    @JsonProperty("is_log")
    private Boolean isLog;
    /**
     * 通知关注者
     */
    @TableField(exist = false)
    @JSONField(name = "notify")
    @JsonProperty("notify")
    private Boolean notify;
    /**
     * 群发邮件营销
     */
    @TableField(exist = false)
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    private Long massMailingCampaignId;
    /**
     * 需要审核
     */
    @TableField(exist = false)
    @JSONField(name = "need_moderation")
    @JsonProperty("need_moderation")
    private Boolean needModeration;
    /**
     * 子类型
     */
    @TableField(exist = false)
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    private Long subtypeId;
    /**
     * 作者
     */
    @TableField(exist = false)
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    private Long authorId;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 已发布
     */
    @TableField(exist = false)
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;
    /**
     * 群发邮件
     */
    @TableField(exist = false)
    @JSONField(name = "mass_mailing_id")
    @JsonProperty("mass_mailing_id")
    private Long massMailingId;
    /**
     * 相关文档编号
     */
    @TableField(exist = false)
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;
    /**
     * 使用模版
     */
    @TableField(exist = false)
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;
    /**
     * 群发邮件标题
     */
    @TableField(exist = false)
    @JSONField(name = "mass_mailing_name")
    @JsonProperty("mass_mailing_name")
    private String massMailingName;
    /**
     * 布局
     */
    @TableField(exist = false)
    @JSONField(name = "layout")
    @JsonProperty("layout")
    private String layout;
    /**
     * 邮件撰写者
     */
    @DEField(name = "composer_id")
    @TableField(value = "composer_id")
    @JSONField(name = "composer_id")
    @JsonProperty("composer_id")
    private Long composerId;
    /**
     * 使用模版
     */
    @DEField(name = "template_id")
    @TableField(value = "template_id")
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Long templateId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_compose_message odooComposer;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_template odooTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [已打印]
     */
    public void setPrinted(Boolean printed){
        this.printed = printed ;
        this.modify("printed",printed);
    }

    /**
     * 设置 [透过邮递]
     */
    public void setSnailmailIsLetter(Boolean snailmailIsLetter){
        this.snailmailIsLetter = snailmailIsLetter ;
        this.modify("snailmail_is_letter",snailmailIsLetter);
    }

    /**
     * 设置 [打印]
     */
    public void setIsPrint(Boolean isPrint){
        this.isPrint = isPrint ;
        this.modify("is_print",isPrint);
    }

    /**
     * 设置 [邮戳(s)]
     */
    public void setSnailmailCost(Double snailmailCost){
        this.snailmailCost = snailmailCost ;
        this.modify("snailmail_cost",snailmailCost);
    }

    /**
     * 设置 [EMail]
     */
    public void setIsEmail(Boolean isEmail){
        this.isEmail = isEmail ;
        this.modify("is_email",isEmail);
    }

    /**
     * 设置 [邮件撰写者]
     */
    public void setComposerId(Long composerId){
        this.composerId = composerId ;
        this.modify("composer_id",composerId);
    }

    /**
     * 设置 [使用模版]
     */
    public void setTemplateId(Long templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



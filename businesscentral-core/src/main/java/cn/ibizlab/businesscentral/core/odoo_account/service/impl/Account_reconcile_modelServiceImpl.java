package cn.ibizlab.businesscentral.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_modelSearchContext;
import cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_reconcile_modelService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_account.mapper.Account_reconcile_modelMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[请在发票和付款匹配期间创建日记账分录] 服务对象接口实现
 */
@Slf4j
@Service("Account_reconcile_modelServiceImpl")
public class Account_reconcile_modelServiceImpl extends EBSServiceImpl<Account_reconcile_modelMapper, Account_reconcile_model> implements IAccount_reconcile_modelService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService accountJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_taxService accountTaxService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "account.reconcile.model" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Account_reconcile_model et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_reconcile_modelService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Account_reconcile_model> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Account_reconcile_model et) {
        Account_reconcile_model old = new Account_reconcile_model() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_reconcile_modelService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IAccount_reconcile_modelService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Account_reconcile_model> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Account_reconcile_model get(Long key) {
        Account_reconcile_model et = getById(key);
        if(et==null){
            et=new Account_reconcile_model();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Account_reconcile_model getDraft(Account_reconcile_model et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Account_reconcile_model et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Account_reconcile_model et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Account_reconcile_model et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Account_reconcile_model> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Account_reconcile_model> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Account_reconcile_model> selectByAccountId(Long id) {
        return baseMapper.selectByAccountId(id);
    }
    @Override
    public void removeByAccountId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_reconcile_model>().in("account_id",ids));
    }

    @Override
    public void removeByAccountId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("account_id",id));
    }

	@Override
    public List<Account_reconcile_model> selectBySecondAccountId(Long id) {
        return baseMapper.selectBySecondAccountId(id);
    }
    @Override
    public void removeBySecondAccountId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_reconcile_model>().in("second_account_id",ids));
    }

    @Override
    public void removeBySecondAccountId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("second_account_id",id));
    }

	@Override
    public List<Account_reconcile_model> selectByAnalyticAccountId(Long id) {
        return baseMapper.selectByAnalyticAccountId(id);
    }
    @Override
    public void resetByAnalyticAccountId(Long id) {
        this.update(new UpdateWrapper<Account_reconcile_model>().set("analytic_account_id",null).eq("analytic_account_id",id));
    }

    @Override
    public void resetByAnalyticAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_reconcile_model>().set("analytic_account_id",null).in("analytic_account_id",ids));
    }

    @Override
    public void removeByAnalyticAccountId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("analytic_account_id",id));
    }

	@Override
    public List<Account_reconcile_model> selectBySecondAnalyticAccountId(Long id) {
        return baseMapper.selectBySecondAnalyticAccountId(id);
    }
    @Override
    public void resetBySecondAnalyticAccountId(Long id) {
        this.update(new UpdateWrapper<Account_reconcile_model>().set("second_analytic_account_id",null).eq("second_analytic_account_id",id));
    }

    @Override
    public void resetBySecondAnalyticAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_reconcile_model>().set("second_analytic_account_id",null).in("second_analytic_account_id",ids));
    }

    @Override
    public void removeBySecondAnalyticAccountId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("second_analytic_account_id",id));
    }

	@Override
    public List<Account_reconcile_model> selectByJournalId(Long id) {
        return baseMapper.selectByJournalId(id);
    }
    @Override
    public void removeByJournalId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_reconcile_model>().in("journal_id",ids));
    }

    @Override
    public void removeByJournalId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("journal_id",id));
    }

	@Override
    public List<Account_reconcile_model> selectBySecondJournalId(Long id) {
        return baseMapper.selectBySecondJournalId(id);
    }
    @Override
    public void removeBySecondJournalId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Account_reconcile_model>().in("second_journal_id",ids));
    }

    @Override
    public void removeBySecondJournalId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("second_journal_id",id));
    }

	@Override
    public List<Account_reconcile_model> selectBySecondTaxId(Long id) {
        return baseMapper.selectBySecondTaxId(id);
    }
    @Override
    public List<Account_reconcile_model> selectBySecondTaxId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_reconcile_model>().in("id",ids));
    }

    @Override
    public void removeBySecondTaxId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("second_tax_id",id));
    }

	@Override
    public List<Account_reconcile_model> selectByTaxId(Long id) {
        return baseMapper.selectByTaxId(id);
    }
    @Override
    public List<Account_reconcile_model> selectByTaxId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Account_reconcile_model>().in("id",ids));
    }

    @Override
    public void removeByTaxId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("tax_id",id));
    }

	@Override
    public List<Account_reconcile_model> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Account_reconcile_model>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Account_reconcile_model>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("company_id",id));
    }

	@Override
    public List<Account_reconcile_model> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("create_uid",id));
    }

	@Override
    public List<Account_reconcile_model> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Account_reconcile_model>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Account_reconcile_model> searchDefault(Account_reconcile_modelSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Account_reconcile_model> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Account_reconcile_model>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Account_reconcile_model et){
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__ACCOUNT_ACCOUNT__ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount=et.getOdooAccount();
            if(ObjectUtils.isEmpty(odooAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getAccountId());
                et.setOdooAccount(majorEntity);
                odooAccount=majorEntity;
            }
            et.setAccountIdText(odooAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__ACCOUNT_ACCOUNT__SECOND_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getSecondAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooSecondAccount=et.getOdooSecondAccount();
            if(ObjectUtils.isEmpty(odooSecondAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getSecondAccountId());
                et.setOdooSecondAccount(majorEntity);
                odooSecondAccount=majorEntity;
            }
            et.setSecondAccountIdText(odooSecondAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__ACCOUNT_ANALYTIC_ACCOUNT__ANALYTIC_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getAnalyticAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount=et.getOdooAnalyticAccount();
            if(ObjectUtils.isEmpty(odooAnalyticAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getAnalyticAccountId());
                et.setOdooAnalyticAccount(majorEntity);
                odooAnalyticAccount=majorEntity;
            }
            et.setAnalyticAccountIdText(odooAnalyticAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__ACCOUNT_ANALYTIC_ACCOUNT__SECOND_ANALYTIC_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getSecondAnalyticAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooSecondAnalyticAccount=et.getOdooSecondAnalyticAccount();
            if(ObjectUtils.isEmpty(odooSecondAnalyticAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account majorEntity=accountAnalyticAccountService.get(et.getSecondAnalyticAccountId());
                et.setOdooSecondAnalyticAccount(majorEntity);
                odooSecondAnalyticAccount=majorEntity;
            }
            et.setSecondAnalyticAccountIdText(odooSecondAnalyticAccount.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__ACCOUNT_JOURNAL__JOURNAL_ID]
        if(!ObjectUtils.isEmpty(et.getJournalId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal=et.getOdooJournal();
            if(ObjectUtils.isEmpty(odooJournal)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal majorEntity=accountJournalService.get(et.getJournalId());
                et.setOdooJournal(majorEntity);
                odooJournal=majorEntity;
            }
            et.setJournalIdText(odooJournal.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__ACCOUNT_JOURNAL__SECOND_JOURNAL_ID]
        if(!ObjectUtils.isEmpty(et.getSecondJournalId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooSecondJournal=et.getOdooSecondJournal();
            if(ObjectUtils.isEmpty(odooSecondJournal)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal majorEntity=accountJournalService.get(et.getSecondJournalId());
                et.setOdooSecondJournal(majorEntity);
                odooSecondJournal=majorEntity;
            }
            et.setSecondJournalIdText(odooSecondJournal.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__ACCOUNT_TAX__SECOND_TAX_ID]
        if(!ObjectUtils.isEmpty(et.getSecondTaxId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooSecondTax=et.getOdooSecondTax();
            if(ObjectUtils.isEmpty(odooSecondTax)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax majorEntity=accountTaxService.get(et.getSecondTaxId());
                et.setOdooSecondTax(majorEntity);
                odooSecondTax=majorEntity;
            }
            et.setSecondTaxIdText(odooSecondTax.getName());
            et.setSecondTaxAmountType(odooSecondTax.getAmountType());
            et.setIsSecondTaxPriceIncluded(odooSecondTax.getPriceInclude());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__ACCOUNT_TAX__TAX_ID]
        if(!ObjectUtils.isEmpty(et.getTaxId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooTax=et.getOdooTax();
            if(ObjectUtils.isEmpty(odooTax)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax majorEntity=accountTaxService.get(et.getTaxId());
                et.setOdooTax(majorEntity);
                odooTax=majorEntity;
            }
            et.setTaxAmountType(odooTax.getAmountType());
            et.setIsTaxPriceIncluded(odooTax.getPriceInclude());
            et.setTaxIdText(odooTax.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_ACCOUNT_RECONCILE_MODEL__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Account_reconcile_model> getAccountReconcileModelByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Account_reconcile_model> getAccountReconcileModelByEntities(List<Account_reconcile_model> entities) {
        List ids =new ArrayList();
        for(Account_reconcile_model entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partner_bankSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_partner_bank] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-partner-bank", fallback = res_partner_bankFallback.class)
public interface res_partner_bankFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/search")
    Page<Res_partner_bank> search(@RequestBody Res_partner_bankSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_banks/{id}")
    Res_partner_bank update(@PathVariable("id") Long id,@RequestBody Res_partner_bank res_partner_bank);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_partner_banks/batch")
    Boolean updateBatch(@RequestBody List<Res_partner_bank> res_partner_banks);



    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_banks/{id}")
    Res_partner_bank get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_banks/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_banks/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks")
    Res_partner_bank create(@RequestBody Res_partner_bank res_partner_bank);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/batch")
    Boolean createBatch(@RequestBody List<Res_partner_bank> res_partner_banks);


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_banks/select")
    Page<Res_partner_bank> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_partner_banks/getdraft")
    Res_partner_bank getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/checkkey")
    Boolean checkKey(@RequestBody Res_partner_bank res_partner_bank);


    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/save")
    Boolean save(@RequestBody Res_partner_bank res_partner_bank);

    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/savebatch")
    Boolean saveBatch(@RequestBody List<Res_partner_bank> res_partner_banks);



    @RequestMapping(method = RequestMethod.POST, value = "/res_partner_banks/searchdefault")
    Page<Res_partner_bank> searchDefault(@RequestBody Res_partner_bankSearchContext context);


}

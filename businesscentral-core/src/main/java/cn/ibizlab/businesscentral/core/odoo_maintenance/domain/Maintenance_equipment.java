package cn.ibizlab.businesscentral.core.odoo_maintenance.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[保养设备]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAINTENANCE_EQUIPMENT",resultMap = "Maintenance_equipmentResultMap")
public class Maintenance_equipment extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 下一活动摘要
     */
    @TableField(exist = false)
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 活动
     */
    @TableField(exist = false)
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;
    /**
     * 行动数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 用于
     */
    @DEField(name = "equipment_assign_to")
    @TableField(value = "equipment_assign_to")
    @JSONField(name = "equipment_assign_to")
    @JsonProperty("equipment_assign_to")
    private String equipmentAssignTo;
    /**
     * 地点
     */
    @TableField(value = "location")
    @JSONField(name = "location")
    @JsonProperty("location")
    private String location;
    /**
     * 保修截止日期
     */
    @DEField(name = "warranty_date")
    @TableField(value = "warranty_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "warranty_date" , format="yyyy-MM-dd")
    @JsonProperty("warranty_date")
    private Timestamp warrantyDate;
    /**
     * 网站消息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 设备名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 保养时长
     */
    @DEField(name = "maintenance_duration")
    @TableField(value = "maintenance_duration")
    @JSONField(name = "maintenance_duration")
    @JsonProperty("maintenance_duration")
    private Double maintenanceDuration;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 型号
     */
    @TableField(value = "model")
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;
    /**
     * 当前维护
     */
    @DEField(name = "maintenance_open_count")
    @TableField(value = "maintenance_open_count")
    @JSONField(name = "maintenance_open_count")
    @JsonProperty("maintenance_open_count")
    private Integer maintenanceOpenCount;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 活动状态
     */
    @TableField(exist = false)
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;
    /**
     * 下一活动截止日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;
    /**
     * 维修统计
     */
    @DEField(name = "maintenance_count")
    @TableField(value = "maintenance_count")
    @JSONField(name = "maintenance_count")
    @JsonProperty("maintenance_count")
    private Integer maintenanceCount;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 笔记
     */
    @TableField(value = "note")
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;
    /**
     * 供应商参考
     */
    @DEField(name = "partner_ref")
    @TableField(value = "partner_ref")
    @JSONField(name = "partner_ref")
    @JsonProperty("partner_ref")
    private String partnerRef;
    /**
     * 保养
     */
    @TableField(exist = false)
    @JSONField(name = "maintenance_ids")
    @JsonProperty("maintenance_ids")
    private String maintenanceIds;
    /**
     * 错误数
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 责任用户
     */
    @TableField(exist = false)
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 序列号
     */
    @DEField(name = "serial_no")
    @TableField(value = "serial_no")
    @JSONField(name = "serial_no")
    @JsonProperty("serial_no")
    private String serialNo;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 成本
     */
    @TableField(value = "cost")
    @JSONField(name = "cost")
    @JsonProperty("cost")
    private Double cost;
    /**
     * 下次预防维护日期
     */
    @DEField(name = "next_action_date")
    @TableField(value = "next_action_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_action_date" , format="yyyy-MM-dd")
    @JsonProperty("next_action_date")
    private Timestamp nextActionDate;
    /**
     * 分配日期
     */
    @DEField(name = "assign_date")
    @TableField(value = "assign_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "assign_date" , format="yyyy-MM-dd")
    @JsonProperty("assign_date")
    private Timestamp assignDate;
    /**
     * 实际日期
     */
    @DEField(name = "effective_date")
    @TableField(value = "effective_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "effective_date" , format="yyyy-MM-dd")
    @JsonProperty("effective_date")
    private Timestamp effectiveDate;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 报废日期
     */
    @DEField(name = "scrap_date")
    @TableField(value = "scrap_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scrap_date" , format="yyyy-MM-dd")
    @JsonProperty("scrap_date")
    private Timestamp scrapDate;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 预防维护间隔天数
     */
    @TableField(value = "period")
    @JSONField(name = "period")
    @JsonProperty("period")
    private Integer period;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 下一活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;
    /**
     * 颜色索引
     */
    @TableField(value = "color")
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 分配到部门
     */
    @TableField(exist = false)
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;
    /**
     * 分配到员工
     */
    @TableField(exist = false)
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;
    /**
     * 技术员
     */
    @TableField(exist = false)
    @JSONField(name = "technician_user_id_text")
    @JsonProperty("technician_user_id_text")
    private String technicianUserIdText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 设备类别
     */
    @TableField(exist = false)
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 供应商
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 所有者
     */
    @TableField(exist = false)
    @JSONField(name = "owner_user_id_text")
    @JsonProperty("owner_user_id_text")
    private String ownerUserIdText;
    /**
     * 保养团队
     */
    @TableField(exist = false)
    @JSONField(name = "maintenance_team_id_text")
    @JsonProperty("maintenance_team_id_text")
    private String maintenanceTeamIdText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 技术员
     */
    @DEField(name = "technician_user_id")
    @TableField(value = "technician_user_id")
    @JSONField(name = "technician_user_id")
    @JsonProperty("technician_user_id")
    private Long technicianUserId;
    /**
     * 供应商
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 设备类别
     */
    @DEField(name = "category_id")
    @TableField(value = "category_id")
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Long categoryId;
    /**
     * 分配到员工
     */
    @DEField(name = "employee_id")
    @TableField(value = "employee_id")
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Long employeeId;
    /**
     * 保养团队
     */
    @DEField(name = "maintenance_team_id")
    @TableField(value = "maintenance_team_id")
    @JSONField(name = "maintenance_team_id")
    @JsonProperty("maintenance_team_id")
    private Long maintenanceTeamId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 所有者
     */
    @DEField(name = "owner_user_id")
    @TableField(value = "owner_user_id")
    @JSONField(name = "owner_user_id")
    @JsonProperty("owner_user_id")
    private Long ownerUserId;
    /**
     * 分配到部门
     */
    @DEField(name = "department_id")
    @TableField(value = "department_id")
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Long departmentId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department odooDepartment;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee odooEmployee;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_equipment_category odooCategory;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_maintenance.domain.Maintenance_team odooMaintenanceTeam;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooOwnerUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooTechnicianUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [用于]
     */
    public void setEquipmentAssignTo(String equipmentAssignTo){
        this.equipmentAssignTo = equipmentAssignTo ;
        this.modify("equipment_assign_to",equipmentAssignTo);
    }

    /**
     * 设置 [地点]
     */
    public void setLocation(String location){
        this.location = location ;
        this.modify("location",location);
    }

    /**
     * 设置 [保修截止日期]
     */
    public void setWarrantyDate(Timestamp warrantyDate){
        this.warrantyDate = warrantyDate ;
        this.modify("warranty_date",warrantyDate);
    }

    /**
     * 格式化日期 [保修截止日期]
     */
    public String formatWarrantyDate(){
        if (this.warrantyDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(warrantyDate);
    }
    /**
     * 设置 [设备名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [保养时长]
     */
    public void setMaintenanceDuration(Double maintenanceDuration){
        this.maintenanceDuration = maintenanceDuration ;
        this.modify("maintenance_duration",maintenanceDuration);
    }

    /**
     * 设置 [型号]
     */
    public void setModel(String model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [当前维护]
     */
    public void setMaintenanceOpenCount(Integer maintenanceOpenCount){
        this.maintenanceOpenCount = maintenanceOpenCount ;
        this.modify("maintenance_open_count",maintenanceOpenCount);
    }

    /**
     * 设置 [维修统计]
     */
    public void setMaintenanceCount(Integer maintenanceCount){
        this.maintenanceCount = maintenanceCount ;
        this.modify("maintenance_count",maintenanceCount);
    }

    /**
     * 设置 [笔记]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [供应商参考]
     */
    public void setPartnerRef(String partnerRef){
        this.partnerRef = partnerRef ;
        this.modify("partner_ref",partnerRef);
    }

    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [序列号]
     */
    public void setSerialNo(String serialNo){
        this.serialNo = serialNo ;
        this.modify("serial_no",serialNo);
    }

    /**
     * 设置 [成本]
     */
    public void setCost(Double cost){
        this.cost = cost ;
        this.modify("cost",cost);
    }

    /**
     * 设置 [下次预防维护日期]
     */
    public void setNextActionDate(Timestamp nextActionDate){
        this.nextActionDate = nextActionDate ;
        this.modify("next_action_date",nextActionDate);
    }

    /**
     * 格式化日期 [下次预防维护日期]
     */
    public String formatNextActionDate(){
        if (this.nextActionDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(nextActionDate);
    }
    /**
     * 设置 [分配日期]
     */
    public void setAssignDate(Timestamp assignDate){
        this.assignDate = assignDate ;
        this.modify("assign_date",assignDate);
    }

    /**
     * 格式化日期 [分配日期]
     */
    public String formatAssignDate(){
        if (this.assignDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(assignDate);
    }
    /**
     * 设置 [实际日期]
     */
    public void setEffectiveDate(Timestamp effectiveDate){
        this.effectiveDate = effectiveDate ;
        this.modify("effective_date",effectiveDate);
    }

    /**
     * 格式化日期 [实际日期]
     */
    public String formatEffectiveDate(){
        if (this.effectiveDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(effectiveDate);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [报废日期]
     */
    public void setScrapDate(Timestamp scrapDate){
        this.scrapDate = scrapDate ;
        this.modify("scrap_date",scrapDate);
    }

    /**
     * 格式化日期 [报废日期]
     */
    public String formatScrapDate(){
        if (this.scrapDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(scrapDate);
    }
    /**
     * 设置 [预防维护间隔天数]
     */
    public void setPeriod(Integer period){
        this.period = period ;
        this.modify("period",period);
    }

    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [技术员]
     */
    public void setTechnicianUserId(Long technicianUserId){
        this.technicianUserId = technicianUserId ;
        this.modify("technician_user_id",technicianUserId);
    }

    /**
     * 设置 [供应商]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [设备类别]
     */
    public void setCategoryId(Long categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

    /**
     * 设置 [分配到员工]
     */
    public void setEmployeeId(Long employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }

    /**
     * 设置 [保养团队]
     */
    public void setMaintenanceTeamId(Long maintenanceTeamId){
        this.maintenanceTeamId = maintenanceTeamId ;
        this.modify("maintenance_team_id",maintenanceTeamId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [所有者]
     */
    public void setOwnerUserId(Long ownerUserId){
        this.ownerUserId = ownerUserId ;
        this.modify("owner_user_id",ownerUserId);
    }

    /**
     * 设置 [分配到部门]
     */
    public void setDepartmentId(Long departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



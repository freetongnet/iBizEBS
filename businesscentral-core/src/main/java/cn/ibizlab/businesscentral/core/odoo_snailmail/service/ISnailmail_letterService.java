package cn.ibizlab.businesscentral.core.odoo_snailmail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.businesscentral.core.odoo_snailmail.filter.Snailmail_letterSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Snailmail_letter] 服务对象接口
 */
public interface ISnailmail_letterService extends IService<Snailmail_letter>{

    boolean create(Snailmail_letter et) ;
    void createBatch(List<Snailmail_letter> list) ;
    boolean update(Snailmail_letter et) ;
    void updateBatch(List<Snailmail_letter> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Snailmail_letter get(Long key) ;
    Snailmail_letter getDraft(Snailmail_letter et) ;
    boolean checkKey(Snailmail_letter et) ;
    boolean save(Snailmail_letter et) ;
    void saveBatch(List<Snailmail_letter> list) ;
    Page<Snailmail_letter> searchDefault(Snailmail_letterSearchContext context) ;
    List<Snailmail_letter> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Snailmail_letter> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Snailmail_letter> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Snailmail_letter> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Snailmail_letter> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Snailmail_letter> getSnailmailLetterByIds(List<Long> ids) ;
    List<Snailmail_letter> getSnailmailLetterByEntities(List<Snailmail_letter> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_odometer;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_odometerSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_odometer] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-fleet:odoo-fleet}", contextId = "fleet-vehicle-odometer", fallback = fleet_vehicle_odometerFallback.class)
public interface fleet_vehicle_odometerFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/search")
    Page<Fleet_vehicle_odometer> search(@RequestBody Fleet_vehicle_odometerSearchContext context);




    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_odometers/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_odometers/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers")
    Fleet_vehicle_odometer create(@RequestBody Fleet_vehicle_odometer fleet_vehicle_odometer);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_odometer> fleet_vehicle_odometers);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_odometers/{id}")
    Fleet_vehicle_odometer get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_odometers/{id}")
    Fleet_vehicle_odometer update(@PathVariable("id") Long id,@RequestBody Fleet_vehicle_odometer fleet_vehicle_odometer);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_odometers/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_odometer> fleet_vehicle_odometers);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_odometers/select")
    Page<Fleet_vehicle_odometer> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_odometers/getdraft")
    Fleet_vehicle_odometer getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/checkkey")
    Boolean checkKey(@RequestBody Fleet_vehicle_odometer fleet_vehicle_odometer);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/save")
    Boolean save(@RequestBody Fleet_vehicle_odometer fleet_vehicle_odometer);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/savebatch")
    Boolean saveBatch(@RequestBody List<Fleet_vehicle_odometer> fleet_vehicle_odometers);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_odometers/searchdefault")
    Page<Fleet_vehicle_odometer> searchDefault(@RequestBody Fleet_vehicle_odometerSearchContext context);


}

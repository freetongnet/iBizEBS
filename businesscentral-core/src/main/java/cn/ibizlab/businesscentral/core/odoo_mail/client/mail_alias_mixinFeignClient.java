package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias_mixin;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_alias_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[mail_alias_mixin] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-mail:odoo-mail}", contextId = "mail-alias-mixin", fallback = mail_alias_mixinFallback.class)
public interface mail_alias_mixinFeignClient {


    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_alias_mixins/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/mail_alias_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/mail_alias_mixins/{id}")
    Mail_alias_mixin update(@PathVariable("id") Long id,@RequestBody Mail_alias_mixin mail_alias_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/mail_alias_mixins/batch")
    Boolean updateBatch(@RequestBody List<Mail_alias_mixin> mail_alias_mixins);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins")
    Mail_alias_mixin create(@RequestBody Mail_alias_mixin mail_alias_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/batch")
    Boolean createBatch(@RequestBody List<Mail_alias_mixin> mail_alias_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/mail_alias_mixins/{id}")
    Mail_alias_mixin get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/search")
    Page<Mail_alias_mixin> search(@RequestBody Mail_alias_mixinSearchContext context);



    @RequestMapping(method = RequestMethod.GET, value = "/mail_alias_mixins/select")
    Page<Mail_alias_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/mail_alias_mixins/getdraft")
    Mail_alias_mixin getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/checkkey")
    Boolean checkKey(@RequestBody Mail_alias_mixin mail_alias_mixin);


    @RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/save")
    Boolean save(@RequestBody Mail_alias_mixin mail_alias_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/savebatch")
    Boolean saveBatch(@RequestBody List<Mail_alias_mixin> mail_alias_mixins);



    @RequestMapping(method = RequestMethod.POST, value = "/mail_alias_mixins/searchdefault")
    Page<Mail_alias_mixin> searchDefault(@RequestBody Mail_alias_mixinSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_survey.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_survey.domain.Survey_mail_compose_message;
/**
 * 关系型数据实体[Survey_mail_compose_message] 查询条件对象
 */
@Slf4j
@Data
public class Survey_mail_compose_messageSearchContext extends QueryWrapperContext<Survey_mail_compose_message> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_moderation_status_eq;//[审核状态]
	public void setN_moderation_status_eq(String n_moderation_status_eq) {
        this.n_moderation_status_eq = n_moderation_status_eq;
        if(!ObjectUtils.isEmpty(this.n_moderation_status_eq)){
            this.getSearchCond().eq("moderation_status", n_moderation_status_eq);
        }
    }
	private String n_message_type_eq;//[类型]
	public void setN_message_type_eq(String n_message_type_eq) {
        this.n_message_type_eq = n_message_type_eq;
        if(!ObjectUtils.isEmpty(this.n_message_type_eq)){
            this.getSearchCond().eq("message_type", n_message_type_eq);
        }
    }
	private String n_ibizpublic_eq;//[分享选项]
	public void setN_ibizpublic_eq(String n_ibizpublic_eq) {
        this.n_ibizpublic_eq = n_ibizpublic_eq;
        if(!ObjectUtils.isEmpty(this.n_ibizpublic_eq)){
            this.getSearchCond().eq("ibizpublic", n_ibizpublic_eq);
        }
    }
	private String n_composition_mode_eq;//[写作模式]
	public void setN_composition_mode_eq(String n_composition_mode_eq) {
        this.n_composition_mode_eq = n_composition_mode_eq;
        if(!ObjectUtils.isEmpty(this.n_composition_mode_eq)){
            this.getSearchCond().eq("composition_mode", n_composition_mode_eq);
        }
    }
	private String n_author_id_text_eq;//[作者]
	public void setN_author_id_text_eq(String n_author_id_text_eq) {
        this.n_author_id_text_eq = n_author_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_author_id_text_eq)){
            this.getSearchCond().eq("author_id_text", n_author_id_text_eq);
        }
    }
	private String n_author_id_text_like;//[作者]
	public void setN_author_id_text_like(String n_author_id_text_like) {
        this.n_author_id_text_like = n_author_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_author_id_text_like)){
            this.getSearchCond().like("author_id_text", n_author_id_text_like);
        }
    }
	private String n_subtype_id_text_eq;//[子类型]
	public void setN_subtype_id_text_eq(String n_subtype_id_text_eq) {
        this.n_subtype_id_text_eq = n_subtype_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_subtype_id_text_eq)){
            this.getSearchCond().eq("subtype_id_text", n_subtype_id_text_eq);
        }
    }
	private String n_subtype_id_text_like;//[子类型]
	public void setN_subtype_id_text_like(String n_subtype_id_text_like) {
        this.n_subtype_id_text_like = n_subtype_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_subtype_id_text_like)){
            this.getSearchCond().like("subtype_id_text", n_subtype_id_text_like);
        }
    }
	private String n_template_id_text_eq;//[使用模版]
	public void setN_template_id_text_eq(String n_template_id_text_eq) {
        this.n_template_id_text_eq = n_template_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_template_id_text_eq)){
            this.getSearchCond().eq("template_id_text", n_template_id_text_eq);
        }
    }
	private String n_template_id_text_like;//[使用模版]
	public void setN_template_id_text_like(String n_template_id_text_like) {
        this.n_template_id_text_like = n_template_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_template_id_text_like)){
            this.getSearchCond().like("template_id_text", n_template_id_text_like);
        }
    }
	private String n_mail_activity_type_id_text_eq;//[邮件活动类型]
	public void setN_mail_activity_type_id_text_eq(String n_mail_activity_type_id_text_eq) {
        this.n_mail_activity_type_id_text_eq = n_mail_activity_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_mail_activity_type_id_text_eq)){
            this.getSearchCond().eq("mail_activity_type_id_text", n_mail_activity_type_id_text_eq);
        }
    }
	private String n_mail_activity_type_id_text_like;//[邮件活动类型]
	public void setN_mail_activity_type_id_text_like(String n_mail_activity_type_id_text_like) {
        this.n_mail_activity_type_id_text_like = n_mail_activity_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_mail_activity_type_id_text_like)){
            this.getSearchCond().like("mail_activity_type_id_text", n_mail_activity_type_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_moderator_id_text_eq;//[审核人]
	public void setN_moderator_id_text_eq(String n_moderator_id_text_eq) {
        this.n_moderator_id_text_eq = n_moderator_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_moderator_id_text_eq)){
            this.getSearchCond().eq("moderator_id_text", n_moderator_id_text_eq);
        }
    }
	private String n_moderator_id_text_like;//[审核人]
	public void setN_moderator_id_text_like(String n_moderator_id_text_like) {
        this.n_moderator_id_text_like = n_moderator_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_moderator_id_text_like)){
            this.getSearchCond().like("moderator_id_text", n_moderator_id_text_like);
        }
    }
	private String n_mass_mailing_id_text_eq;//[群发邮件]
	public void setN_mass_mailing_id_text_eq(String n_mass_mailing_id_text_eq) {
        this.n_mass_mailing_id_text_eq = n_mass_mailing_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_mass_mailing_id_text_eq)){
            this.getSearchCond().eq("mass_mailing_id_text", n_mass_mailing_id_text_eq);
        }
    }
	private String n_mass_mailing_id_text_like;//[群发邮件]
	public void setN_mass_mailing_id_text_like(String n_mass_mailing_id_text_like) {
        this.n_mass_mailing_id_text_like = n_mass_mailing_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_mass_mailing_id_text_like)){
            this.getSearchCond().like("mass_mailing_id_text", n_mass_mailing_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_mass_mailing_campaign_id_text_eq;//[群发邮件营销]
	public void setN_mass_mailing_campaign_id_text_eq(String n_mass_mailing_campaign_id_text_eq) {
        this.n_mass_mailing_campaign_id_text_eq = n_mass_mailing_campaign_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_mass_mailing_campaign_id_text_eq)){
            this.getSearchCond().eq("mass_mailing_campaign_id_text", n_mass_mailing_campaign_id_text_eq);
        }
    }
	private String n_mass_mailing_campaign_id_text_like;//[群发邮件营销]
	public void setN_mass_mailing_campaign_id_text_like(String n_mass_mailing_campaign_id_text_like) {
        this.n_mass_mailing_campaign_id_text_like = n_mass_mailing_campaign_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_mass_mailing_campaign_id_text_like)){
            this.getSearchCond().like("mass_mailing_campaign_id_text", n_mass_mailing_campaign_id_text_like);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_survey_id_eq;//[问卷]
	public void setN_survey_id_eq(Long n_survey_id_eq) {
        this.n_survey_id_eq = n_survey_id_eq;
        if(!ObjectUtils.isEmpty(this.n_survey_id_eq)){
            this.getSearchCond().eq("survey_id", n_survey_id_eq);
        }
    }
	private Long n_author_id_eq;//[作者]
	public void setN_author_id_eq(Long n_author_id_eq) {
        this.n_author_id_eq = n_author_id_eq;
        if(!ObjectUtils.isEmpty(this.n_author_id_eq)){
            this.getSearchCond().eq("author_id", n_author_id_eq);
        }
    }
	private Long n_mass_mailing_campaign_id_eq;//[群发邮件营销]
	public void setN_mass_mailing_campaign_id_eq(Long n_mass_mailing_campaign_id_eq) {
        this.n_mass_mailing_campaign_id_eq = n_mass_mailing_campaign_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mass_mailing_campaign_id_eq)){
            this.getSearchCond().eq("mass_mailing_campaign_id", n_mass_mailing_campaign_id_eq);
        }
    }
	private Long n_parent_id_eq;//[上级消息]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_subtype_id_eq;//[子类型]
	public void setN_subtype_id_eq(Long n_subtype_id_eq) {
        this.n_subtype_id_eq = n_subtype_id_eq;
        if(!ObjectUtils.isEmpty(this.n_subtype_id_eq)){
            this.getSearchCond().eq("subtype_id", n_subtype_id_eq);
        }
    }
	private Long n_template_id_eq;//[使用模版]
	public void setN_template_id_eq(Long n_template_id_eq) {
        this.n_template_id_eq = n_template_id_eq;
        if(!ObjectUtils.isEmpty(this.n_template_id_eq)){
            this.getSearchCond().eq("template_id", n_template_id_eq);
        }
    }
	private Long n_mass_mailing_id_eq;//[群发邮件]
	public void setN_mass_mailing_id_eq(Long n_mass_mailing_id_eq) {
        this.n_mass_mailing_id_eq = n_mass_mailing_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mass_mailing_id_eq)){
            this.getSearchCond().eq("mass_mailing_id", n_mass_mailing_id_eq);
        }
    }
	private Long n_moderator_id_eq;//[审核人]
	public void setN_moderator_id_eq(Long n_moderator_id_eq) {
        this.n_moderator_id_eq = n_moderator_id_eq;
        if(!ObjectUtils.isEmpty(this.n_moderator_id_eq)){
            this.getSearchCond().eq("moderator_id", n_moderator_id_eq);
        }
    }
	private Long n_mail_activity_type_id_eq;//[邮件活动类型]
	public void setN_mail_activity_type_id_eq(Long n_mail_activity_type_id_eq) {
        this.n_mail_activity_type_id_eq = n_mail_activity_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_mail_activity_type_id_eq)){
            this.getSearchCond().eq("mail_activity_type_id", n_mail_activity_type_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




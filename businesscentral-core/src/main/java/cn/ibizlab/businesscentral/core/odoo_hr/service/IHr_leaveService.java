package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_leaveSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_leave] 服务对象接口
 */
public interface IHr_leaveService extends IService<Hr_leave>{

    boolean create(Hr_leave et) ;
    void createBatch(List<Hr_leave> list) ;
    boolean update(Hr_leave et) ;
    void updateBatch(List<Hr_leave> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_leave get(Long key) ;
    Hr_leave getDraft(Hr_leave et) ;
    boolean checkKey(Hr_leave et) ;
    boolean save(Hr_leave et) ;
    void saveBatch(List<Hr_leave> list) ;
    Page<Hr_leave> searchDefault(Hr_leaveSearchContext context) ;
    List<Hr_leave> selectByMeetingId(Long id);
    void resetByMeetingId(Long id);
    void resetByMeetingId(Collection<Long> ids);
    void removeByMeetingId(Long id);
    List<Hr_leave> selectByDepartmentId(Long id);
    void resetByDepartmentId(Long id);
    void resetByDepartmentId(Collection<Long> ids);
    void removeByDepartmentId(Long id);
    List<Hr_leave> selectByCategoryId(Long id);
    void resetByCategoryId(Long id);
    void resetByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Hr_leave> selectByEmployeeId(Long id);
    void resetByEmployeeId(Long id);
    void resetByEmployeeId(Collection<Long> ids);
    void removeByEmployeeId(Long id);
    List<Hr_leave> selectByFirstApproverId(Long id);
    void resetByFirstApproverId(Long id);
    void resetByFirstApproverId(Collection<Long> ids);
    void removeByFirstApproverId(Long id);
    List<Hr_leave> selectByManagerId(Long id);
    void resetByManagerId(Long id);
    void resetByManagerId(Collection<Long> ids);
    void removeByManagerId(Long id);
    List<Hr_leave> selectBySecondApproverId(Long id);
    void resetBySecondApproverId(Long id);
    void resetBySecondApproverId(Collection<Long> ids);
    void removeBySecondApproverId(Long id);
    List<Hr_leave> selectByHolidayStatusId(Long id);
    void resetByHolidayStatusId(Long id);
    void resetByHolidayStatusId(Collection<Long> ids);
    void removeByHolidayStatusId(Long id);
    List<Hr_leave> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Hr_leave> selectByModeCompanyId(Long id);
    void resetByModeCompanyId(Long id);
    void resetByModeCompanyId(Collection<Long> ids);
    void removeByModeCompanyId(Long id);
    List<Hr_leave> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_leave> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Hr_leave> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_leave> getHrLeaveByIds(List<Long> ids) ;
    List<Hr_leave> getHrLeaveByEntities(List<Hr_leave> entities) ;
}



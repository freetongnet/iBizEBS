package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_tests_models_complex;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_tests_models_complexSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_tests_models_complex] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base-import:odoo-base-import}", contextId = "base-import-tests-models-complex", fallback = base_import_tests_models_complexFallback.class)
public interface base_import_tests_models_complexFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_complices/{id}")
    Base_import_tests_models_complex update(@PathVariable("id") Long id,@RequestBody Base_import_tests_models_complex base_import_tests_models_complex);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_complices/batch")
    Boolean updateBatch(@RequestBody List<Base_import_tests_models_complex> base_import_tests_models_complices);



    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_complices/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_complices/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_complices/{id}")
    Base_import_tests_models_complex get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices")
    Base_import_tests_models_complex create(@RequestBody Base_import_tests_models_complex base_import_tests_models_complex);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/batch")
    Boolean createBatch(@RequestBody List<Base_import_tests_models_complex> base_import_tests_models_complices);





    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/search")
    Page<Base_import_tests_models_complex> search(@RequestBody Base_import_tests_models_complexSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_complices/select")
    Page<Base_import_tests_models_complex> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_complices/getdraft")
    Base_import_tests_models_complex getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/checkkey")
    Boolean checkKey(@RequestBody Base_import_tests_models_complex base_import_tests_models_complex);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/save")
    Boolean save(@RequestBody Base_import_tests_models_complex base_import_tests_models_complex);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/savebatch")
    Boolean saveBatch(@RequestBody List<Base_import_tests_models_complex> base_import_tests_models_complices);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_complices/searchdefault")
    Page<Base_import_tests_models_complex> searchDefault(@RequestBody Base_import_tests_models_complexSearchContext context);


}

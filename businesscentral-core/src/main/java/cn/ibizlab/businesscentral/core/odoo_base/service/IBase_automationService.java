package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_automation;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_automationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Base_automation] 服务对象接口
 */
public interface IBase_automationService extends IService<Base_automation>{

    boolean create(Base_automation et) ;
    void createBatch(List<Base_automation> list) ;
    boolean update(Base_automation et) ;
    void updateBatch(List<Base_automation> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Base_automation get(Long key) ;
    Base_automation getDraft(Base_automation et) ;
    boolean checkKey(Base_automation et) ;
    boolean save(Base_automation et) ;
    void saveBatch(List<Base_automation> list) ;
    Page<Base_automation> searchDefault(Base_automationSearchContext context) ;
    List<Base_automation> selectByTrgDateCalendarId(Long id);
    void resetByTrgDateCalendarId(Long id);
    void resetByTrgDateCalendarId(Collection<Long> ids);
    void removeByTrgDateCalendarId(Long id);
    List<Base_automation> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Base_automation> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Base_automation> getBaseAutomationByIds(List<Long> ids) ;
    List<Base_automation> getBaseAutomationByEntities(List<Base_automation> entities) ;
}



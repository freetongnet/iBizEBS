package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_reconcile_modelSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_reconcile_model] 服务对象接口
 */
public interface IAccount_reconcile_modelService extends IService<Account_reconcile_model>{

    boolean create(Account_reconcile_model et) ;
    void createBatch(List<Account_reconcile_model> list) ;
    boolean update(Account_reconcile_model et) ;
    void updateBatch(List<Account_reconcile_model> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_reconcile_model get(Long key) ;
    Account_reconcile_model getDraft(Account_reconcile_model et) ;
    boolean checkKey(Account_reconcile_model et) ;
    boolean save(Account_reconcile_model et) ;
    void saveBatch(List<Account_reconcile_model> list) ;
    Page<Account_reconcile_model> searchDefault(Account_reconcile_modelSearchContext context) ;
    List<Account_reconcile_model> selectByAccountId(Long id);
    void removeByAccountId(Collection<Long> ids);
    void removeByAccountId(Long id);
    List<Account_reconcile_model> selectBySecondAccountId(Long id);
    void removeBySecondAccountId(Collection<Long> ids);
    void removeBySecondAccountId(Long id);
    List<Account_reconcile_model> selectByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Long id);
    void resetByAnalyticAccountId(Collection<Long> ids);
    void removeByAnalyticAccountId(Long id);
    List<Account_reconcile_model> selectBySecondAnalyticAccountId(Long id);
    void resetBySecondAnalyticAccountId(Long id);
    void resetBySecondAnalyticAccountId(Collection<Long> ids);
    void removeBySecondAnalyticAccountId(Long id);
    List<Account_reconcile_model> selectByJournalId(Long id);
    void removeByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Account_reconcile_model> selectBySecondJournalId(Long id);
    void removeBySecondJournalId(Collection<Long> ids);
    void removeBySecondJournalId(Long id);
    List<Account_reconcile_model> selectBySecondTaxId(Long id);
    List<Account_reconcile_model> selectBySecondTaxId(Collection<Long> ids);
    void removeBySecondTaxId(Long id);
    List<Account_reconcile_model> selectByTaxId(Long id);
    List<Account_reconcile_model> selectByTaxId(Collection<Long> ids);
    void removeByTaxId(Long id);
    List<Account_reconcile_model> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_reconcile_model> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_reconcile_model> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_reconcile_model> getAccountReconcileModelByIds(List<Long> ids) ;
    List<Account_reconcile_model> getAccountReconcileModelByEntities(List<Account_reconcile_model> entities) ;
}



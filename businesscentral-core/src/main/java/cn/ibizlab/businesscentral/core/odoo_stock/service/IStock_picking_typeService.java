package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_picking_typeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_picking_type] 服务对象接口
 */
public interface IStock_picking_typeService extends IService<Stock_picking_type>{

    boolean create(Stock_picking_type et) ;
    void createBatch(List<Stock_picking_type> list) ;
    boolean update(Stock_picking_type et) ;
    void updateBatch(List<Stock_picking_type> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_picking_type get(Long key) ;
    Stock_picking_type getDraft(Stock_picking_type et) ;
    boolean checkKey(Stock_picking_type et) ;
    boolean save(Stock_picking_type et) ;
    void saveBatch(List<Stock_picking_type> list) ;
    Page<Stock_picking_type> searchDefault(Stock_picking_typeSearchContext context) ;
    List<Stock_picking_type> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_picking_type> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_picking_type> selectByDefaultLocationDestId(Long id);
    void resetByDefaultLocationDestId(Long id);
    void resetByDefaultLocationDestId(Collection<Long> ids);
    void removeByDefaultLocationDestId(Long id);
    List<Stock_picking_type> selectByDefaultLocationSrcId(Long id);
    void resetByDefaultLocationSrcId(Long id);
    void resetByDefaultLocationSrcId(Collection<Long> ids);
    void removeByDefaultLocationSrcId(Long id);
    List<Stock_picking_type> selectByReturnPickingTypeId(Long id);
    void resetByReturnPickingTypeId(Long id);
    void resetByReturnPickingTypeId(Collection<Long> ids);
    void removeByReturnPickingTypeId(Long id);
    List<Stock_picking_type> selectByWarehouseId(Long id);
    void removeByWarehouseId(Collection<Long> ids);
    void removeByWarehouseId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_picking_type> getStockPickingTypeByIds(List<Long> ids) ;
    List<Stock_picking_type> getStockPickingTypeByEntities(List<Stock_picking_type> entities) ;
}



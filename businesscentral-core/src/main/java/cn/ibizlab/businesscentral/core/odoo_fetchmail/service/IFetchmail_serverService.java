package cn.ibizlab.businesscentral.core.odoo_fetchmail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.businesscentral.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Fetchmail_server] 服务对象接口
 */
public interface IFetchmail_serverService extends IService<Fetchmail_server>{

    boolean create(Fetchmail_server et) ;
    void createBatch(List<Fetchmail_server> list) ;
    boolean update(Fetchmail_server et) ;
    void updateBatch(List<Fetchmail_server> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Fetchmail_server get(Long key) ;
    Fetchmail_server getDraft(Fetchmail_server et) ;
    boolean checkKey(Fetchmail_server et) ;
    boolean save(Fetchmail_server et) ;
    void saveBatch(List<Fetchmail_server> list) ;
    Page<Fetchmail_server> searchDefault(Fetchmail_serverSearchContext context) ;
    List<Fetchmail_server> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Fetchmail_server> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Fetchmail_server> getFetchmailServerByIds(List<Long> ids) ;
    List<Fetchmail_server> getFetchmailServerByEntities(List<Fetchmail_server> entities) ;
}



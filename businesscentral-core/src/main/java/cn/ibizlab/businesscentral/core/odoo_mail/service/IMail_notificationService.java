package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_notificationSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_notification] 服务对象接口
 */
public interface IMail_notificationService extends IService<Mail_notification>{

    boolean create(Mail_notification et) ;
    void createBatch(List<Mail_notification> list) ;
    boolean update(Mail_notification et) ;
    void updateBatch(List<Mail_notification> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_notification get(Long key) ;
    Mail_notification getDraft(Mail_notification et) ;
    boolean checkKey(Mail_notification et) ;
    boolean save(Mail_notification et) ;
    void saveBatch(List<Mail_notification> list) ;
    Page<Mail_notification> searchDefault(Mail_notificationSearchContext context) ;
    List<Mail_notification> selectByMailId(Long id);
    void resetByMailId(Long id);
    void resetByMailId(Collection<Long> ids);
    void removeByMailId(Long id);
    List<Mail_notification> selectByMailMessageId(Long id);
    void removeByMailMessageId(Collection<Long> ids);
    void removeByMailMessageId(Long id);
    List<Mail_notification> selectByResPartnerId(Long id);
    void removeByResPartnerId(Collection<Long> ids);
    void removeByResPartnerId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

}



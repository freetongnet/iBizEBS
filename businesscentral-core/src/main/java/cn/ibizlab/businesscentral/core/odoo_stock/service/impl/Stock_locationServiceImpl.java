package cn.ibizlab.businesscentral.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_locationSearchContext;
import cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_stock.mapper.Stock_locationMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[库存位置] 服务对象接口实现
 */
@Slf4j
@Service("Stock_locationServiceImpl")
public class Stock_locationServiceImpl extends EBSServiceImpl<Stock_locationMapper, Stock_location> implements IStock_locationService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_productionService mrpProductionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routingService mrpRoutingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_unbuildService mrpUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_lineService repairLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplierService resSupplierService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_change_product_qtyService stockChangeProductQtyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_fixed_putaway_stratService stockFixedPutawayStratService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventory_lineService stockInventoryLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventoryService stockInventoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService stockMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_destinationService stockPackageDestinationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_package_levelService stockPackageLevelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_picking_typeService stockPickingTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quant_packageService stockQuantPackageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantService stockQuantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_return_pickingService stockReturnPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService stockRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService stockScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouse_orderpointService stockWarehouseOrderpointService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_repairService stockWarnInsufficientQtyRepairService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_scrapService stockWarnInsufficientQtyScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qty_unbuildService stockWarnInsufficientQtyUnbuildService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warn_insufficient_qtyService stockWarnInsufficientQtyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_accountService accountAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_removalService productRemovalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "stock.location" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Stock_location et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_locationService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Stock_location> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Stock_location et) {
        Stock_location old = new Stock_location() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_locationService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IStock_locationService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Stock_location> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        mrpProductionService.resetByLocationDestId(key);
        mrpProductionService.resetByLocationSrcId(key);
        mrpRoutingService.resetByLocationId(key);
        mrpUnbuildService.resetByLocationDestId(key);
        mrpUnbuildService.resetByLocationId(key);
        repairLineService.resetByLocationDestId(key);
        repairLineService.resetByLocationId(key);
        repairOrderService.resetByLocationId(key);
        resCompanyService.resetByInternalTransitLocationId(key);
        stockChangeProductQtyService.resetByLocationId(key);
        stockFixedPutawayStratService.resetByFixedLocationId(key);
        stockInventoryLineService.resetByLocationId(key);
        stockInventoryService.resetByLocationId(key);
        stockMoveLineService.resetByLocationDestId(key);
        stockMoveLineService.resetByLocationId(key);
        stockMoveService.resetByLocationDestId(key);
        stockMoveService.resetByLocationId(key);
        stockPackageDestinationService.resetByLocationDestId(key);
        stockPackageLevelService.resetByLocationDestId(key);
        stockPickingTypeService.resetByDefaultLocationDestId(key);
        stockPickingTypeService.resetByDefaultLocationSrcId(key);
        stockPickingService.resetByLocationDestId(key);
        stockPickingService.resetByLocationId(key);
        stockQuantPackageService.resetByLocationId(key);
        if(!ObjectUtils.isEmpty(stockQuantService.selectByLocationId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_quant]数据，无法删除!","","");
        stockReturnPickingService.resetByLocationId(key);
        stockReturnPickingService.resetByOriginalLocationId(key);
        stockReturnPickingService.resetByParentLocationId(key);
        stockRuleService.resetByLocationId(key);
        stockRuleService.resetByLocationSrcId(key);
        stockScrapService.resetByLocationId(key);
        stockScrapService.resetByScrapLocationId(key);
        stockWarehouseOrderpointService.removeByLocationId(key);
        stockWarehouseService.resetByLotStockId(key);
        stockWarehouseService.resetByPbmLocId(key);
        stockWarehouseService.resetBySamLocId(key);
        stockWarehouseService.resetByViewLocationId(key);
        stockWarehouseService.resetByWhInputStockLocId(key);
        stockWarehouseService.resetByWhOutputStockLocId(key);
        stockWarehouseService.resetByWhPackStockLocId(key);
        stockWarehouseService.resetByWhQcStockLocId(key);
        stockWarnInsufficientQtyRepairService.resetByLocationId(key);
        stockWarnInsufficientQtyScrapService.resetByLocationId(key);
        stockWarnInsufficientQtyUnbuildService.resetByLocationId(key);
        stockWarnInsufficientQtyService.resetByLocationId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        mrpProductionService.resetByLocationDestId(idList);
        mrpProductionService.resetByLocationSrcId(idList);
        mrpRoutingService.resetByLocationId(idList);
        mrpUnbuildService.resetByLocationDestId(idList);
        mrpUnbuildService.resetByLocationId(idList);
        repairLineService.resetByLocationDestId(idList);
        repairLineService.resetByLocationId(idList);
        repairOrderService.resetByLocationId(idList);
        resCompanyService.resetByInternalTransitLocationId(idList);
        stockChangeProductQtyService.resetByLocationId(idList);
        stockFixedPutawayStratService.resetByFixedLocationId(idList);
        stockInventoryLineService.resetByLocationId(idList);
        stockInventoryService.resetByLocationId(idList);
        stockMoveLineService.resetByLocationDestId(idList);
        stockMoveLineService.resetByLocationId(idList);
        stockMoveService.resetByLocationDestId(idList);
        stockMoveService.resetByLocationId(idList);
        stockPackageDestinationService.resetByLocationDestId(idList);
        stockPackageLevelService.resetByLocationDestId(idList);
        stockPickingTypeService.resetByDefaultLocationDestId(idList);
        stockPickingTypeService.resetByDefaultLocationSrcId(idList);
        stockPickingService.resetByLocationDestId(idList);
        stockPickingService.resetByLocationId(idList);
        stockQuantPackageService.resetByLocationId(idList);
        if(!ObjectUtils.isEmpty(stockQuantService.selectByLocationId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Stock_quant]数据，无法删除!","","");
        stockReturnPickingService.resetByLocationId(idList);
        stockReturnPickingService.resetByOriginalLocationId(idList);
        stockReturnPickingService.resetByParentLocationId(idList);
        stockRuleService.resetByLocationId(idList);
        stockRuleService.resetByLocationSrcId(idList);
        stockScrapService.resetByLocationId(idList);
        stockScrapService.resetByScrapLocationId(idList);
        stockWarehouseOrderpointService.removeByLocationId(idList);
        stockWarehouseService.resetByLotStockId(idList);
        stockWarehouseService.resetByPbmLocId(idList);
        stockWarehouseService.resetBySamLocId(idList);
        stockWarehouseService.resetByViewLocationId(idList);
        stockWarehouseService.resetByWhInputStockLocId(idList);
        stockWarehouseService.resetByWhOutputStockLocId(idList);
        stockWarehouseService.resetByWhPackStockLocId(idList);
        stockWarehouseService.resetByWhQcStockLocId(idList);
        stockWarnInsufficientQtyRepairService.resetByLocationId(idList);
        stockWarnInsufficientQtyScrapService.resetByLocationId(idList);
        stockWarnInsufficientQtyUnbuildService.resetByLocationId(idList);
        stockWarnInsufficientQtyService.resetByLocationId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Stock_location get(Long key) {
        Stock_location et = getById(key);
        if(et==null){
            et=new Stock_location();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Stock_location getDraft(Stock_location et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Stock_location et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Stock_location et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Stock_location et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Stock_location> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Stock_location> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Stock_location> selectByValuationInAccountId(Long id) {
        return baseMapper.selectByValuationInAccountId(id);
    }
    @Override
    public void resetByValuationInAccountId(Long id) {
        this.update(new UpdateWrapper<Stock_location>().set("valuation_in_account_id",null).eq("valuation_in_account_id",id));
    }

    @Override
    public void resetByValuationInAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_location>().set("valuation_in_account_id",null).in("valuation_in_account_id",ids));
    }

    @Override
    public void removeByValuationInAccountId(Long id) {
        this.remove(new QueryWrapper<Stock_location>().eq("valuation_in_account_id",id));
    }

	@Override
    public List<Stock_location> selectByValuationOutAccountId(Long id) {
        return baseMapper.selectByValuationOutAccountId(id);
    }
    @Override
    public void resetByValuationOutAccountId(Long id) {
        this.update(new UpdateWrapper<Stock_location>().set("valuation_out_account_id",null).eq("valuation_out_account_id",id));
    }

    @Override
    public void resetByValuationOutAccountId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_location>().set("valuation_out_account_id",null).in("valuation_out_account_id",ids));
    }

    @Override
    public void removeByValuationOutAccountId(Long id) {
        this.remove(new QueryWrapper<Stock_location>().eq("valuation_out_account_id",id));
    }

	@Override
    public List<Stock_location> selectByRemovalStrategyId(Long id) {
        return baseMapper.selectByRemovalStrategyId(id);
    }
    @Override
    public void resetByRemovalStrategyId(Long id) {
        this.update(new UpdateWrapper<Stock_location>().set("removal_strategy_id",null).eq("removal_strategy_id",id));
    }

    @Override
    public void resetByRemovalStrategyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_location>().set("removal_strategy_id",null).in("removal_strategy_id",ids));
    }

    @Override
    public void removeByRemovalStrategyId(Long id) {
        this.remove(new QueryWrapper<Stock_location>().eq("removal_strategy_id",id));
    }

	@Override
    public List<Stock_location> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Stock_location>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Stock_location>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Stock_location>().eq("company_id",id));
    }

	@Override
    public List<Stock_location> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Stock_location>().eq("create_uid",id));
    }

	@Override
    public List<Stock_location> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Stock_location>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Stock_location> searchDefault(Stock_locationSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Stock_location> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Stock_location>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Stock_location et){
        //实体关系[DER1N_STOCK_LOCATION__ACCOUNT_ACCOUNT__VALUATION_IN_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getValuationInAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooValuationInAccount=et.getOdooValuationInAccount();
            if(ObjectUtils.isEmpty(odooValuationInAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getValuationInAccountId());
                et.setOdooValuationInAccount(majorEntity);
                odooValuationInAccount=majorEntity;
            }
            et.setValuationInAccountIdText(odooValuationInAccount.getName());
        }
        //实体关系[DER1N_STOCK_LOCATION__ACCOUNT_ACCOUNT__VALUATION_OUT_ACCOUNT_ID]
        if(!ObjectUtils.isEmpty(et.getValuationOutAccountId())){
            cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooValuationOutAccount=et.getOdooValuationOutAccount();
            if(ObjectUtils.isEmpty(odooValuationOutAccount)){
                cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account majorEntity=accountAccountService.get(et.getValuationOutAccountId());
                et.setOdooValuationOutAccount(majorEntity);
                odooValuationOutAccount=majorEntity;
            }
            et.setValuationOutAccountIdText(odooValuationOutAccount.getName());
        }
        //实体关系[DER1N_STOCK_LOCATION__PRODUCT_REMOVAL__REMOVAL_STRATEGY_ID]
        if(!ObjectUtils.isEmpty(et.getRemovalStrategyId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_removal odooRemovalStrategy=et.getOdooRemovalStrategy();
            if(ObjectUtils.isEmpty(odooRemovalStrategy)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_removal majorEntity=productRemovalService.get(et.getRemovalStrategyId());
                et.setOdooRemovalStrategy(majorEntity);
                odooRemovalStrategy=majorEntity;
            }
            et.setRemovalStrategyIdText(odooRemovalStrategy.getName());
        }
        //实体关系[DER1N_STOCK_LOCATION__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_STOCK_LOCATION__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_STOCK_LOCATION__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Stock_location> getStockLocationByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Stock_location> getStockLocationByEntities(List<Stock_location> entities) {
        List ids =new ArrayList();
        for(Stock_location entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




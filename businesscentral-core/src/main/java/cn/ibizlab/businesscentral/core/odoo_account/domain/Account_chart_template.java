package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[科目表模版]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_CHART_TEMPLATE",resultMap = "Account_chart_templateResultMap")
public class Account_chart_template extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * # 数字
     */
    @DEField(name = "code_digits")
    @TableField(value = "code_digits")
    @JSONField(name = "code_digits")
    @JsonProperty("code_digits")
    private Integer codeDigits;
    /**
     * 主转账帐户的前缀
     */
    @DEField(name = "transfer_account_code_prefix")
    @TableField(value = "transfer_account_code_prefix")
    @JSONField(name = "transfer_account_code_prefix")
    @JsonProperty("transfer_account_code_prefix")
    private String transferAccountCodePrefix;
    /**
     * 银行科目的前缀
     */
    @DEField(name = "bank_account_code_prefix")
    @TableField(value = "bank_account_code_prefix")
    @JSONField(name = "bank_account_code_prefix")
    @JsonProperty("bank_account_code_prefix")
    private String bankAccountCodePrefix;
    /**
     * 口语
     */
    @DEField(name = "spoken_languages")
    @TableField(value = "spoken_languages")
    @JSONField(name = "spoken_languages")
    @JsonProperty("spoken_languages")
    private String spokenLanguages;
    /**
     * 税模板列表
     */
    @TableField(exist = false)
    @JSONField(name = "tax_template_ids")
    @JsonProperty("tax_template_ids")
    private String taxTemplateIds;
    /**
     * 可显示？
     */
    @TableField(value = "visible")
    @JSONField(name = "visible")
    @JsonProperty("visible")
    private Boolean visible;
    /**
     * 关联的科目模板
     */
    @TableField(exist = false)
    @JSONField(name = "account_ids")
    @JsonProperty("account_ids")
    private String accountIds;
    /**
     * 税率完整集合
     */
    @DEField(name = "complete_tax_set")
    @TableField(value = "complete_tax_set")
    @JSONField(name = "complete_tax_set")
    @JsonProperty("complete_tax_set")
    private Boolean completeTaxSet;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 主现金科目的前缀
     */
    @DEField(name = "cash_account_code_prefix")
    @TableField(value = "cash_account_code_prefix")
    @JSONField(name = "cash_account_code_prefix")
    @JsonProperty("cash_account_code_prefix")
    private String cashAccountCodePrefix;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 使用anglo-saxon会计
     */
    @DEField(name = "use_anglo_saxon")
    @TableField(value = "use_anglo_saxon")
    @JSONField(name = "use_anglo_saxon")
    @JsonProperty("use_anglo_saxon")
    private Boolean useAngloSaxon;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 汇率损失科目
     */
    @TableField(exist = false)
    @JSONField(name = "expense_currency_exchange_account_id_text")
    @JsonProperty("expense_currency_exchange_account_id_text")
    private String expenseCurrencyExchangeAccountIdText;
    /**
     * 应收科目
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_receivable_id_text")
    @JsonProperty("property_account_receivable_id_text")
    private String propertyAccountReceivableIdText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 产品模板的费用科目
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_expense_id_text")
    @JsonProperty("property_account_expense_id_text")
    private String propertyAccountExpenseIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 上级表模板
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;
    /**
     * 库存计价的入库科目
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_account_input_categ_id_text")
    @JsonProperty("property_stock_account_input_categ_id_text")
    private String propertyStockAccountInputCategIdText;
    /**
     * 产品模板的收入科目
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_income_id_text")
    @JsonProperty("property_account_income_id_text")
    private String propertyAccountIncomeIdText;
    /**
     * 费用科目的类别
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_expense_categ_id_text")
    @JsonProperty("property_account_expense_categ_id_text")
    private String propertyAccountExpenseCategIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 汇率增益科目
     */
    @TableField(exist = false)
    @JSONField(name = "income_currency_exchange_account_id_text")
    @JsonProperty("income_currency_exchange_account_id_text")
    private String incomeCurrencyExchangeAccountIdText;
    /**
     * 应付科目
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_payable_id_text")
    @JsonProperty("property_account_payable_id_text")
    private String propertyAccountPayableIdText;
    /**
     * 收入科目的类别
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_income_categ_id_text")
    @JsonProperty("property_account_income_categ_id_text")
    private String propertyAccountIncomeCategIdText;
    /**
     * 库存计价的科目模板
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_valuation_account_id_text")
    @JsonProperty("property_stock_valuation_account_id_text")
    private String propertyStockValuationAccountIdText;
    /**
     * 库存计价的出货科目
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_account_output_categ_id_text")
    @JsonProperty("property_stock_account_output_categ_id_text")
    private String propertyStockAccountOutputCategIdText;
    /**
     * 库存计价的科目模板
     */
    @DEField(name = "property_stock_valuation_account_id")
    @TableField(value = "property_stock_valuation_account_id")
    @JSONField(name = "property_stock_valuation_account_id")
    @JsonProperty("property_stock_valuation_account_id")
    private Long propertyStockValuationAccountId;
    /**
     * 汇率损失科目
     */
    @DEField(name = "expense_currency_exchange_account_id")
    @TableField(value = "expense_currency_exchange_account_id")
    @JSONField(name = "expense_currency_exchange_account_id")
    @JsonProperty("expense_currency_exchange_account_id")
    private Long expenseCurrencyExchangeAccountId;
    /**
     * 应付科目
     */
    @DEField(name = "property_account_payable_id")
    @TableField(value = "property_account_payable_id")
    @JSONField(name = "property_account_payable_id")
    @JsonProperty("property_account_payable_id")
    private Long propertyAccountPayableId;
    /**
     * 收入科目的类别
     */
    @DEField(name = "property_account_income_categ_id")
    @TableField(value = "property_account_income_categ_id")
    @JSONField(name = "property_account_income_categ_id")
    @JsonProperty("property_account_income_categ_id")
    private Long propertyAccountIncomeCategId;
    /**
     * 库存计价的出货科目
     */
    @DEField(name = "property_stock_account_output_categ_id")
    @TableField(value = "property_stock_account_output_categ_id")
    @JSONField(name = "property_stock_account_output_categ_id")
    @JsonProperty("property_stock_account_output_categ_id")
    private Long propertyStockAccountOutputCategId;
    /**
     * 产品模板的费用科目
     */
    @DEField(name = "property_account_expense_id")
    @TableField(value = "property_account_expense_id")
    @JSONField(name = "property_account_expense_id")
    @JsonProperty("property_account_expense_id")
    private Long propertyAccountExpenseId;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 上级表模板
     */
    @DEField(name = "parent_id")
    @TableField(value = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 应收科目
     */
    @DEField(name = "property_account_receivable_id")
    @TableField(value = "property_account_receivable_id")
    @JSONField(name = "property_account_receivable_id")
    @JsonProperty("property_account_receivable_id")
    private Long propertyAccountReceivableId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 费用科目的类别
     */
    @DEField(name = "property_account_expense_categ_id")
    @TableField(value = "property_account_expense_categ_id")
    @JSONField(name = "property_account_expense_categ_id")
    @JsonProperty("property_account_expense_categ_id")
    private Long propertyAccountExpenseCategId;
    /**
     * 汇率增益科目
     */
    @DEField(name = "income_currency_exchange_account_id")
    @TableField(value = "income_currency_exchange_account_id")
    @JSONField(name = "income_currency_exchange_account_id")
    @JsonProperty("income_currency_exchange_account_id")
    private Long incomeCurrencyExchangeAccountId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 库存计价的入库科目
     */
    @DEField(name = "property_stock_account_input_categ_id")
    @TableField(value = "property_stock_account_input_categ_id")
    @JSONField(name = "property_stock_account_input_categ_id")
    @JsonProperty("property_stock_account_input_categ_id")
    private Long propertyStockAccountInputCategId;
    /**
     * 产品模板的收入科目
     */
    @DEField(name = "property_account_income_id")
    @TableField(value = "property_account_income_id")
    @JSONField(name = "property_account_income_id")
    @JsonProperty("property_account_income_id")
    private Long propertyAccountIncomeId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooExpenseCurrencyExchangeAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooIncomeCurrencyExchangeAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooPropertyAccountExpenseCateg;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooPropertyAccountExpense;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooPropertyAccountIncomeCateg;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooPropertyAccountIncome;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooPropertyAccountPayable;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooPropertyAccountReceivable;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooPropertyStockAccountInputCateg;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooPropertyStockAccountOutputCateg;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooPropertyStockValuationAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooParent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [# 数字]
     */
    public void setCodeDigits(Integer codeDigits){
        this.codeDigits = codeDigits ;
        this.modify("code_digits",codeDigits);
    }

    /**
     * 设置 [主转账帐户的前缀]
     */
    public void setTransferAccountCodePrefix(String transferAccountCodePrefix){
        this.transferAccountCodePrefix = transferAccountCodePrefix ;
        this.modify("transfer_account_code_prefix",transferAccountCodePrefix);
    }

    /**
     * 设置 [银行科目的前缀]
     */
    public void setBankAccountCodePrefix(String bankAccountCodePrefix){
        this.bankAccountCodePrefix = bankAccountCodePrefix ;
        this.modify("bank_account_code_prefix",bankAccountCodePrefix);
    }

    /**
     * 设置 [口语]
     */
    public void setSpokenLanguages(String spokenLanguages){
        this.spokenLanguages = spokenLanguages ;
        this.modify("spoken_languages",spokenLanguages);
    }

    /**
     * 设置 [可显示？]
     */
    public void setVisible(Boolean visible){
        this.visible = visible ;
        this.modify("visible",visible);
    }

    /**
     * 设置 [税率完整集合]
     */
    public void setCompleteTaxSet(Boolean completeTaxSet){
        this.completeTaxSet = completeTaxSet ;
        this.modify("complete_tax_set",completeTaxSet);
    }

    /**
     * 设置 [主现金科目的前缀]
     */
    public void setCashAccountCodePrefix(String cashAccountCodePrefix){
        this.cashAccountCodePrefix = cashAccountCodePrefix ;
        this.modify("cash_account_code_prefix",cashAccountCodePrefix);
    }

    /**
     * 设置 [使用anglo-saxon会计]
     */
    public void setUseAngloSaxon(Boolean useAngloSaxon){
        this.useAngloSaxon = useAngloSaxon ;
        this.modify("use_anglo_saxon",useAngloSaxon);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [库存计价的科目模板]
     */
    public void setPropertyStockValuationAccountId(Long propertyStockValuationAccountId){
        this.propertyStockValuationAccountId = propertyStockValuationAccountId ;
        this.modify("property_stock_valuation_account_id",propertyStockValuationAccountId);
    }

    /**
     * 设置 [汇率损失科目]
     */
    public void setExpenseCurrencyExchangeAccountId(Long expenseCurrencyExchangeAccountId){
        this.expenseCurrencyExchangeAccountId = expenseCurrencyExchangeAccountId ;
        this.modify("expense_currency_exchange_account_id",expenseCurrencyExchangeAccountId);
    }

    /**
     * 设置 [应付科目]
     */
    public void setPropertyAccountPayableId(Long propertyAccountPayableId){
        this.propertyAccountPayableId = propertyAccountPayableId ;
        this.modify("property_account_payable_id",propertyAccountPayableId);
    }

    /**
     * 设置 [收入科目的类别]
     */
    public void setPropertyAccountIncomeCategId(Long propertyAccountIncomeCategId){
        this.propertyAccountIncomeCategId = propertyAccountIncomeCategId ;
        this.modify("property_account_income_categ_id",propertyAccountIncomeCategId);
    }

    /**
     * 设置 [库存计价的出货科目]
     */
    public void setPropertyStockAccountOutputCategId(Long propertyStockAccountOutputCategId){
        this.propertyStockAccountOutputCategId = propertyStockAccountOutputCategId ;
        this.modify("property_stock_account_output_categ_id",propertyStockAccountOutputCategId);
    }

    /**
     * 设置 [产品模板的费用科目]
     */
    public void setPropertyAccountExpenseId(Long propertyAccountExpenseId){
        this.propertyAccountExpenseId = propertyAccountExpenseId ;
        this.modify("property_account_expense_id",propertyAccountExpenseId);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [上级表模板]
     */
    public void setParentId(Long parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [应收科目]
     */
    public void setPropertyAccountReceivableId(Long propertyAccountReceivableId){
        this.propertyAccountReceivableId = propertyAccountReceivableId ;
        this.modify("property_account_receivable_id",propertyAccountReceivableId);
    }

    /**
     * 设置 [费用科目的类别]
     */
    public void setPropertyAccountExpenseCategId(Long propertyAccountExpenseCategId){
        this.propertyAccountExpenseCategId = propertyAccountExpenseCategId ;
        this.modify("property_account_expense_categ_id",propertyAccountExpenseCategId);
    }

    /**
     * 设置 [汇率增益科目]
     */
    public void setIncomeCurrencyExchangeAccountId(Long incomeCurrencyExchangeAccountId){
        this.incomeCurrencyExchangeAccountId = incomeCurrencyExchangeAccountId ;
        this.modify("income_currency_exchange_account_id",incomeCurrencyExchangeAccountId);
    }

    /**
     * 设置 [库存计价的入库科目]
     */
    public void setPropertyStockAccountInputCategId(Long propertyStockAccountInputCategId){
        this.propertyStockAccountInputCategId = propertyStockAccountInputCategId ;
        this.modify("property_stock_account_input_categ_id",propertyStockAccountInputCategId);
    }

    /**
     * 设置 [产品模板的收入科目]
     */
    public void setPropertyAccountIncomeId(Long propertyAccountIncomeId){
        this.propertyAccountIncomeId = propertyAccountIncomeId ;
        this.modify("property_account_income_id",propertyAccountIncomeId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



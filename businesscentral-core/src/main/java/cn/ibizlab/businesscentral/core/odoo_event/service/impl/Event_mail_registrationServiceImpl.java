package cn.ibizlab.businesscentral.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_mail_registration;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_mail_registrationSearchContext;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_mail_registrationService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_event.mapper.Event_mail_registrationMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[登记邮件调度] 服务对象接口实现
 */
@Slf4j
@Service("Event_mail_registrationServiceImpl")
public class Event_mail_registrationServiceImpl extends EBSServiceImpl<Event_mail_registrationMapper, Event_mail_registration> implements IEvent_mail_registrationService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_mailService eventMailService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService eventRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "event.mail.registration" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Event_mail_registration et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_mail_registrationService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Event_mail_registration> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Event_mail_registration et) {
        Event_mail_registration old = new Event_mail_registration() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_mail_registrationService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_mail_registrationService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Event_mail_registration> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Event_mail_registration get(Long key) {
        Event_mail_registration et = getById(key);
        if(et==null){
            et=new Event_mail_registration();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Event_mail_registration getDraft(Event_mail_registration et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Event_mail_registration et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Event_mail_registration et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Event_mail_registration et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Event_mail_registration> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Event_mail_registration> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Event_mail_registration> selectBySchedulerId(Long id) {
        return baseMapper.selectBySchedulerId(id);
    }
    @Override
    public void removeBySchedulerId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Event_mail_registration>().in("scheduler_id",ids));
    }

    @Override
    public void removeBySchedulerId(Long id) {
        this.remove(new QueryWrapper<Event_mail_registration>().eq("scheduler_id",id));
    }

	@Override
    public List<Event_mail_registration> selectByRegistrationId(Long id) {
        return baseMapper.selectByRegistrationId(id);
    }
    @Override
    public void removeByRegistrationId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Event_mail_registration>().in("registration_id",ids));
    }

    @Override
    public void removeByRegistrationId(Long id) {
        this.remove(new QueryWrapper<Event_mail_registration>().eq("registration_id",id));
    }

	@Override
    public List<Event_mail_registration> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Event_mail_registration>().eq("create_uid",id));
    }

	@Override
    public List<Event_mail_registration> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Event_mail_registration>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Event_mail_registration> searchDefault(Event_mail_registrationSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Event_mail_registration> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Event_mail_registration>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Event_mail_registration et){
        //实体关系[DER1N_EVENT_MAIL_REGISTRATION__EVENT_REGISTRATION__REGISTRATION_ID]
        if(!ObjectUtils.isEmpty(et.getRegistrationId())){
            cn.ibizlab.businesscentral.core.odoo_event.domain.Event_registration odooRegistration=et.getOdooRegistration();
            if(ObjectUtils.isEmpty(odooRegistration)){
                cn.ibizlab.businesscentral.core.odoo_event.domain.Event_registration majorEntity=eventRegistrationService.get(et.getRegistrationId());
                et.setOdooRegistration(majorEntity);
                odooRegistration=majorEntity;
            }
            et.setRegistrationIdText(odooRegistration.getName());
        }
        //实体关系[DER1N_EVENT_MAIL_REGISTRATION__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_EVENT_MAIL_REGISTRATION__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Event_mail_registration> getEventMailRegistrationByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Event_mail_registration> getEventMailRegistrationByEntities(List<Event_mail_registration> entities) {
        List ids =new ArrayList();
        for(Event_mail_registration entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




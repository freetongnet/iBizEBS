package cn.ibizlab.businesscentral.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_partnerSearchContext;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_base.mapper.Res_partnerMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[联系人] 服务对象接口实现
 */
@Slf4j
@Service("Res_partnerServiceImpl")
public class Res_partnerServiceImpl extends EBSServiceImpl<Res_partnerMapper, Res_partner> implements IRes_partnerService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_abstract_paymentService accountAbstractPaymentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_accountService accountAnalyticAccountService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_analytic_lineService accountAnalyticLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_bank_statement_lineService accountBankStatementLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_lineService accountInvoiceLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoice_reportService accountInvoiceReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_invoiceService accountInvoiceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_move_lineService accountMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_moveService accountMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_paymentService accountPaymentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_register_paymentsService accountRegisterPaymentsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_asset.service.IAsset_assetService assetAssetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_automation_lead_testService baseAutomationLeadTestService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_partner_merge_automatic_wizardService basePartnerMergeAutomaticWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_attendeeService calendarAttendeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_calendar.service.ICalendar_contactsService calendarContactsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_activity_reportService crmActivityReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead2opportunity_partner_massService crmLead2opportunityPartnerMassService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_lead2opportunity_partnerService crmLead2opportunityPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_leadService crmLeadService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_partner_bindingService crmPartnerBindingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_eventService eventEventService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService eventRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_assignation_logService fleetVehicleAssignationLogService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_contractService fleetVehicleLogContractService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_fuelService fleetVehicleLogFuelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicle_log_servicesService fleetVehicleLogServicesService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_fleet.service.IFleet_vehicleService fleetVehicleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_applicantService hrApplicantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_employeeService hrEmployeeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheet_register_payment_wizardService hrExpenseSheetRegisterPaymentWizardService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_expense_sheetService hrExpenseSheetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_report_channelService imLivechatReportChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_im_livechat.service.IIm_livechat_report_operatorService imLivechatReportOperatorService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_alertService lunchAlertService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_order_lineService lunchOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_lunch.service.ILunch_productService lunchProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channel_partnerService mailChannelPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_compose_messageService mailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_followersService mailFollowersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_messageService mailMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_notificationService mailNotificationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_resend_partnerService mailResendPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipmentService maintenanceEquipmentService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_tokenService paymentTokenService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_payment.service.IPayment_transactionService paymentTransactionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_portal.service.IPortal_wizard_userService portalWizardUserService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_projectService projectProjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_taskService projectTaskService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_bill_unionService purchaseBillUnionService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_order_lineService purchaseOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_orderService purchaseOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_purchase.service.IPurchase_reportService purchaseReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_rating.service.IRating_ratingService ratingRatingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_repair.service.IRepair_orderService repairOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_companyService resCompanyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_autocomplete_syncService resPartnerAutocompleteSyncService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_bankService resPartnerBankService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_res_partner_category_relService resPartnerResPartnerCategoryRelService;

    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService = this;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_supplierService resSupplierService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_orderService saleOrderService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_reportService saleReportService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_snailmail.service.ISnailmail_letterService snailmailLetterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventory_lineService stockInventoryLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_inventoryService stockInventoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_move_lineService stockMoveLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_pickingService stockPickingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_quantService stockQuantService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_ruleService stockRuleService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_scrapService stockScrapService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_warehouseService stockWarehouseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_mail_compose_messageService surveyMailComposeMessageService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_survey.service.ISurvey_user_inputService surveyUserInputService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService crmTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_country_stateService resCountryStateService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_countryService resCountryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_industryService resPartnerIndustryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partner_titleService resPartnerTitleService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "res.partner" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Res_partner et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_partnerService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Res_partner> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Res_partner et) {
        Res_partner old = new Res_partner() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_partnerService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IRes_partnerService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Res_partner> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountAbstractPaymentService.resetByPartnerId(key);
        accountAnalyticAccountService.resetByPartnerId(key);
        accountAnalyticLineService.resetByPartnerId(key);
        accountBankStatementLineService.resetByPartnerId(key);
        accountInvoiceLineService.resetByPartnerId(key);
        accountInvoiceService.resetByCommercialPartnerId(key);
        if(!ObjectUtils.isEmpty(accountInvoiceService.selectByPartnerId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_invoice]数据，无法删除!","","");
        accountInvoiceService.resetByPartnerShippingId(key);
        if(!ObjectUtils.isEmpty(accountMoveLineService.selectByPartnerId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_move_line]数据，无法删除!","","");
        accountMoveService.resetByPartnerId(key);
        accountPaymentService.resetByPartnerId(key);
        accountRegisterPaymentsService.resetByPartnerId(key);
        assetAssetService.resetByManufacturerId(key);
        assetAssetService.resetByVendorId(key);
        baseAutomationLeadTestService.resetByPartnerId(key);
        basePartnerMergeAutomaticWizardService.resetByDstPartnerId(key);
        calendarAttendeeService.resetByPartnerId(key);
        calendarContactsService.resetByPartnerId(key);
        crmLead2opportunityPartnerMassService.resetByPartnerId(key);
        crmLead2opportunityPartnerService.resetByPartnerId(key);
        crmLeadService.resetByPartnerId(key);
        crmPartnerBindingService.resetByPartnerId(key);
        eventEventService.resetByAddressId(key);
        eventEventService.resetByOrganizerId(key);
        eventRegistrationService.resetByPartnerId(key);
        fleetVehicleAssignationLogService.resetByDriverId(key);
        fleetVehicleLogContractService.resetByInsurerId(key);
        fleetVehicleLogContractService.resetByPurchaserId(key);
        fleetVehicleLogFuelService.resetByPurchaserId(key);
        fleetVehicleLogFuelService.resetByVendorId(key);
        fleetVehicleLogServicesService.resetByPurchaserId(key);
        fleetVehicleLogServicesService.resetByVendorId(key);
        fleetVehicleService.resetByDriverId(key);
        hrApplicantService.resetByPartnerId(key);
        hrEmployeeService.resetByAddressHomeId(key);
        hrEmployeeService.resetByAddressId(key);
        hrExpenseSheetRegisterPaymentWizardService.resetByPartnerId(key);
        hrExpenseSheetService.resetByAddressId(key);
        hrJobService.resetByAddressId(key);
        lunchAlertService.resetByPartnerId(key);
        lunchOrderLineService.resetBySupplier(key);
        lunchProductService.resetBySupplier(key);
        mailChannelPartnerService.removeByPartnerId(key);
        mailComposeMessageService.resetByAuthorId(key);
        mailFollowersService.removeByPartnerId(key);
        mailMessageService.resetByAuthorId(key);
        mailNotificationService.removeByResPartnerId(key);
        mailResendPartnerService.removeByPartnerId(key);
        maintenanceEquipmentService.resetByPartnerId(key);
        paymentTokenService.resetByPartnerId(key);
        paymentTransactionService.resetByPartnerId(key);
        portalWizardUserService.removeByPartnerId(key);
        projectProjectService.resetByPartnerId(key);
        projectTaskService.resetByPartnerId(key);
        purchaseBillUnionService.resetByPartnerId(key);
        purchaseOrderLineService.resetByPartnerId(key);
        purchaseOrderService.resetByDestAddressId(key);
        purchaseReportService.resetByCommercialPartnerId(key);
        purchaseReportService.resetByPartnerId(key);
        ratingRatingService.resetByPartnerId(key);
        ratingRatingService.resetByRatedPartnerId(key);
        repairOrderService.resetByAddressId(key);
        repairOrderService.resetByPartnerId(key);
        repairOrderService.resetByPartnerInvoiceId(key);
        resCompanyService.resetByPartnerId(key);
        resPartnerAutocompleteSyncService.removeByPartnerId(key);
        resPartnerBankService.removeByPartnerId(key);
        resPartnerService.resetByCommercialPartnerId(key);
        resPartnerService.resetByParentId(key);
        if(!ObjectUtils.isEmpty(resUsersService.selectByPartnerId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Res_users]数据，无法删除!","","");
        saleOrderLineService.resetByOrderPartnerId(key);
        saleOrderService.resetByPartnerId(key);
        saleOrderService.resetByPartnerInvoiceId(key);
        saleOrderService.resetByPartnerShippingId(key);
        saleReportService.resetByCommercialPartnerId(key);
        saleReportService.resetByPartnerId(key);
        snailmailLetterService.resetByPartnerId(key);
        stockInventoryLineService.resetByPartnerId(key);
        stockInventoryService.resetByPartnerId(key);
        stockMoveLineService.resetByOwnerId(key);
        stockMoveService.resetByPartnerId(key);
        stockMoveService.resetByRestrictPartnerId(key);
        stockPickingService.resetByOwnerId(key);
        stockPickingService.resetByPartnerId(key);
        stockQuantService.resetByOwnerId(key);
        stockRuleService.resetByPartnerAddressId(key);
        stockScrapService.resetByOwnerId(key);
        stockWarehouseService.resetByPartnerId(key);
        surveyMailComposeMessageService.resetByAuthorId(key);
        surveyUserInputService.resetByPartnerId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountAbstractPaymentService.resetByPartnerId(idList);
        accountAnalyticAccountService.resetByPartnerId(idList);
        accountAnalyticLineService.resetByPartnerId(idList);
        accountBankStatementLineService.resetByPartnerId(idList);
        accountInvoiceLineService.resetByPartnerId(idList);
        accountInvoiceService.resetByCommercialPartnerId(idList);
        if(!ObjectUtils.isEmpty(accountInvoiceService.selectByPartnerId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_invoice]数据，无法删除!","","");
        accountInvoiceService.resetByPartnerShippingId(idList);
        if(!ObjectUtils.isEmpty(accountMoveLineService.selectByPartnerId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Account_move_line]数据，无法删除!","","");
        accountMoveService.resetByPartnerId(idList);
        accountPaymentService.resetByPartnerId(idList);
        accountRegisterPaymentsService.resetByPartnerId(idList);
        assetAssetService.resetByManufacturerId(idList);
        assetAssetService.resetByVendorId(idList);
        baseAutomationLeadTestService.resetByPartnerId(idList);
        basePartnerMergeAutomaticWizardService.resetByDstPartnerId(idList);
        calendarAttendeeService.resetByPartnerId(idList);
        calendarContactsService.resetByPartnerId(idList);
        crmLead2opportunityPartnerMassService.resetByPartnerId(idList);
        crmLead2opportunityPartnerService.resetByPartnerId(idList);
        crmLeadService.resetByPartnerId(idList);
        crmPartnerBindingService.resetByPartnerId(idList);
        eventEventService.resetByAddressId(idList);
        eventEventService.resetByOrganizerId(idList);
        eventRegistrationService.resetByPartnerId(idList);
        fleetVehicleAssignationLogService.resetByDriverId(idList);
        fleetVehicleLogContractService.resetByInsurerId(idList);
        fleetVehicleLogContractService.resetByPurchaserId(idList);
        fleetVehicleLogFuelService.resetByPurchaserId(idList);
        fleetVehicleLogFuelService.resetByVendorId(idList);
        fleetVehicleLogServicesService.resetByPurchaserId(idList);
        fleetVehicleLogServicesService.resetByVendorId(idList);
        fleetVehicleService.resetByDriverId(idList);
        hrApplicantService.resetByPartnerId(idList);
        hrEmployeeService.resetByAddressHomeId(idList);
        hrEmployeeService.resetByAddressId(idList);
        hrExpenseSheetRegisterPaymentWizardService.resetByPartnerId(idList);
        hrExpenseSheetService.resetByAddressId(idList);
        hrJobService.resetByAddressId(idList);
        lunchAlertService.resetByPartnerId(idList);
        lunchOrderLineService.resetBySupplier(idList);
        lunchProductService.resetBySupplier(idList);
        mailChannelPartnerService.removeByPartnerId(idList);
        mailComposeMessageService.resetByAuthorId(idList);
        mailFollowersService.removeByPartnerId(idList);
        mailMessageService.resetByAuthorId(idList);
        mailNotificationService.removeByResPartnerId(idList);
        mailResendPartnerService.removeByPartnerId(idList);
        maintenanceEquipmentService.resetByPartnerId(idList);
        paymentTokenService.resetByPartnerId(idList);
        paymentTransactionService.resetByPartnerId(idList);
        portalWizardUserService.removeByPartnerId(idList);
        projectProjectService.resetByPartnerId(idList);
        projectTaskService.resetByPartnerId(idList);
        purchaseBillUnionService.resetByPartnerId(idList);
        purchaseOrderLineService.resetByPartnerId(idList);
        purchaseOrderService.resetByDestAddressId(idList);
        purchaseReportService.resetByCommercialPartnerId(idList);
        purchaseReportService.resetByPartnerId(idList);
        ratingRatingService.resetByPartnerId(idList);
        ratingRatingService.resetByRatedPartnerId(idList);
        repairOrderService.resetByAddressId(idList);
        repairOrderService.resetByPartnerId(idList);
        repairOrderService.resetByPartnerInvoiceId(idList);
        resCompanyService.resetByPartnerId(idList);
        resPartnerAutocompleteSyncService.removeByPartnerId(idList);
        resPartnerBankService.removeByPartnerId(idList);
        resPartnerService.resetByCommercialPartnerId(idList);
        resPartnerService.resetByParentId(idList);
        if(!ObjectUtils.isEmpty(resUsersService.selectByPartnerId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Res_users]数据，无法删除!","","");
        saleOrderLineService.resetByOrderPartnerId(idList);
        saleOrderService.resetByPartnerId(idList);
        saleOrderService.resetByPartnerInvoiceId(idList);
        saleOrderService.resetByPartnerShippingId(idList);
        saleReportService.resetByCommercialPartnerId(idList);
        saleReportService.resetByPartnerId(idList);
        snailmailLetterService.resetByPartnerId(idList);
        stockInventoryLineService.resetByPartnerId(idList);
        stockInventoryService.resetByPartnerId(idList);
        stockMoveLineService.resetByOwnerId(idList);
        stockMoveService.resetByPartnerId(idList);
        stockMoveService.resetByRestrictPartnerId(idList);
        stockPickingService.resetByOwnerId(idList);
        stockPickingService.resetByPartnerId(idList);
        stockQuantService.resetByOwnerId(idList);
        stockRuleService.resetByPartnerAddressId(idList);
        stockScrapService.resetByOwnerId(idList);
        stockWarehouseService.resetByPartnerId(idList);
        surveyMailComposeMessageService.resetByAuthorId(idList);
        surveyUserInputService.resetByPartnerId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Res_partner get(Long key) {
        Res_partner et = getById(key);
        if(et==null){
            et=new Res_partner();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Res_partner getDraft(Res_partner et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Res_partner et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Res_partner et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Res_partner et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Res_partner> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Res_partner> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Res_partner> selectByTeamId(Long id) {
        return baseMapper.selectByTeamId(id);
    }
    @Override
    public void resetByTeamId(Long id) {
        this.update(new UpdateWrapper<Res_partner>().set("team_id",null).eq("team_id",id));
    }

    @Override
    public void resetByTeamId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_partner>().set("team_id",null).in("team_id",ids));
    }

    @Override
    public void removeByTeamId(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("team_id",id));
    }

	@Override
    public List<Res_partner> selectByCompanyId(Long id) {
        return baseMapper.selectByCompanyId(id);
    }
    @Override
    public void resetByCompanyId(Long id) {
        this.update(new UpdateWrapper<Res_partner>().set("company_id",null).eq("company_id",id));
    }

    @Override
    public void resetByCompanyId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_partner>().set("company_id",null).in("company_id",ids));
    }

    @Override
    public void removeByCompanyId(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("company_id",id));
    }

	@Override
    public List<Res_partner> selectByStateId(Long id) {
        return baseMapper.selectByStateId(id);
    }
    @Override
    public List<Res_partner> selectByStateId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Res_partner>().in("id",ids));
    }

    @Override
    public void removeByStateId(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("state_id",id));
    }

	@Override
    public List<Res_partner> selectByCountryId(Long id) {
        return baseMapper.selectByCountryId(id);
    }
    @Override
    public List<Res_partner> selectByCountryId(Collection<Long> ids) {
        return this.list(new QueryWrapper<Res_partner>().in("id",ids));
    }

    @Override
    public void removeByCountryId(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("country_id",id));
    }

	@Override
    public List<Res_partner> selectByIndustryId(Long id) {
        return baseMapper.selectByIndustryId(id);
    }
    @Override
    public void resetByIndustryId(Long id) {
        this.update(new UpdateWrapper<Res_partner>().set("industry_id",null).eq("industry_id",id));
    }

    @Override
    public void resetByIndustryId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_partner>().set("industry_id",null).in("industry_id",ids));
    }

    @Override
    public void removeByIndustryId(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("industry_id",id));
    }

	@Override
    public List<Res_partner> selectByTitle(Long id) {
        return baseMapper.selectByTitle(id);
    }
    @Override
    public void resetByTitle(Long id) {
        this.update(new UpdateWrapper<Res_partner>().set("title",null).eq("title",id));
    }

    @Override
    public void resetByTitle(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_partner>().set("title",null).in("title",ids));
    }

    @Override
    public void removeByTitle(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("title",id));
    }

	@Override
    public List<Res_partner> selectByCommercialPartnerId(Long id) {
        return baseMapper.selectByCommercialPartnerId(id);
    }
    @Override
    public void resetByCommercialPartnerId(Long id) {
        this.update(new UpdateWrapper<Res_partner>().set("commercial_partner_id",null).eq("commercial_partner_id",id));
    }

    @Override
    public void resetByCommercialPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_partner>().set("commercial_partner_id",null).in("commercial_partner_id",ids));
    }

    @Override
    public void removeByCommercialPartnerId(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("commercial_partner_id",id));
    }

	@Override
    public List<Res_partner> selectByParentId(Long id) {
        return baseMapper.selectByParentId(id);
    }
    @Override
    public void resetByParentId(Long id) {
        this.update(new UpdateWrapper<Res_partner>().set("parent_id",null).eq("parent_id",id));
    }

    @Override
    public void resetByParentId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_partner>().set("parent_id",null).in("parent_id",ids));
    }

    @Override
    public void removeByParentId(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("parent_id",id));
    }

	@Override
    public List<Res_partner> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("create_uid",id));
    }

	@Override
    public List<Res_partner> selectByUserId(Long id) {
        return baseMapper.selectByUserId(id);
    }
    @Override
    public void resetByUserId(Long id) {
        this.update(new UpdateWrapper<Res_partner>().set("user_id",null).eq("user_id",id));
    }

    @Override
    public void resetByUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Res_partner>().set("user_id",null).in("user_id",ids));
    }

    @Override
    public void removeByUserId(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("user_id",id));
    }

	@Override
    public List<Res_partner> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Res_partner>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Res_partner> searchDefault(Res_partnerSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Res_partner> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Res_partner>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Res_partner et){
        //实体关系[DER1N_RES_PARTNER__CRM_TEAM__TEAM_ID]
        if(!ObjectUtils.isEmpty(et.getTeamId())){
            cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team odooTeam=et.getOdooTeam();
            if(ObjectUtils.isEmpty(odooTeam)){
                cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_team majorEntity=crmTeamService.get(et.getTeamId());
                et.setOdooTeam(majorEntity);
                odooTeam=majorEntity;
            }
            et.setTeamIdText(odooTeam.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_COMPANY__COMPANY_ID]
        if(!ObjectUtils.isEmpty(et.getCompanyId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany=et.getOdooCompany();
            if(ObjectUtils.isEmpty(odooCompany)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company majorEntity=resCompanyService.get(et.getCompanyId());
                et.setOdooCompany(majorEntity);
                odooCompany=majorEntity;
            }
            et.setCompanyIdText(odooCompany.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_COUNTRY_STATE__STATE_ID]
        if(!ObjectUtils.isEmpty(et.getStateId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state odooState=et.getOdooState();
            if(ObjectUtils.isEmpty(odooState)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state majorEntity=resCountryStateService.get(et.getStateId());
                et.setOdooState(majorEntity);
                odooState=majorEntity;
            }
            et.setStateIdText(odooState.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_COUNTRY__COUNTRY_ID]
        if(!ObjectUtils.isEmpty(et.getCountryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry=et.getOdooCountry();
            if(ObjectUtils.isEmpty(odooCountry)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country majorEntity=resCountryService.get(et.getCountryId());
                et.setOdooCountry(majorEntity);
                odooCountry=majorEntity;
            }
            et.setCountryIdText(odooCountry.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_PARTNER_INDUSTRY__INDUSTRY_ID]
        if(!ObjectUtils.isEmpty(et.getIndustryId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_industry odooIndustry=et.getOdooIndustry();
            if(ObjectUtils.isEmpty(odooIndustry)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_industry majorEntity=resPartnerIndustryService.get(et.getIndustryId());
                et.setOdooIndustry(majorEntity);
                odooIndustry=majorEntity;
            }
            et.setIndustryIdText(odooIndustry.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_PARTNER_TITLE__TITLE]
        if(!ObjectUtils.isEmpty(et.getTitle())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_title odooTitle=et.getOdooTitle();
            if(ObjectUtils.isEmpty(odooTitle)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_title majorEntity=resPartnerTitleService.get(et.getTitle());
                et.setOdooTitle(majorEntity);
                odooTitle=majorEntity;
            }
            et.setTitleText(odooTitle.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_PARTNER__COMMERCIAL_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getCommercialPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooCommercialPartner=et.getOdooCommercialPartner();
            if(ObjectUtils.isEmpty(odooCommercialPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getCommercialPartnerId());
                et.setOdooCommercialPartner(majorEntity);
                odooCommercialPartner=majorEntity;
            }
            et.setCommercialPartnerIdText(odooCommercialPartner.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_PARTNER__PARENT_ID]
        if(!ObjectUtils.isEmpty(et.getParentId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooParent=et.getOdooParent();
            if(ObjectUtils.isEmpty(odooParent)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getParentId());
                et.setOdooParent(majorEntity);
                odooParent=majorEntity;
            }
            et.setParentName(odooParent.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_USERS__USER_ID]
        if(!ObjectUtils.isEmpty(et.getUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser=et.getOdooUser();
            if(ObjectUtils.isEmpty(odooUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getUserId());
                et.setOdooUser(majorEntity);
                odooUser=majorEntity;
            }
            et.setUserIdText(odooUser.getName());
        }
        //实体关系[DER1N_RES_PARTNER__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Res_partner> getResPartnerByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Res_partner> getResPartnerByEntities(List<Res_partner> entities) {
        List ids =new ArrayList();
        for(Res_partner entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




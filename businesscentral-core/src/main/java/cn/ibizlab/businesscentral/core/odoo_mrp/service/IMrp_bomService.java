package cn.ibizlab.businesscentral.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_bomSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mrp_bom] 服务对象接口
 */
public interface IMrp_bomService extends IService<Mrp_bom>{

    boolean create(Mrp_bom et) ;
    void createBatch(List<Mrp_bom> list) ;
    boolean update(Mrp_bom et) ;
    void updateBatch(List<Mrp_bom> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mrp_bom get(Long key) ;
    Mrp_bom getDraft(Mrp_bom et) ;
    boolean checkKey(Mrp_bom et) ;
    boolean save(Mrp_bom et) ;
    void saveBatch(List<Mrp_bom> list) ;
    Page<Mrp_bom> searchDefault(Mrp_bomSearchContext context) ;
    List<Mrp_bom> selectByRoutingId(Long id);
    void resetByRoutingId(Long id);
    void resetByRoutingId(Collection<Long> ids);
    void removeByRoutingId(Long id);
    List<Mrp_bom> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Mrp_bom> selectByProductTmplId(Long id);
    void resetByProductTmplId(Long id);
    void resetByProductTmplId(Collection<Long> ids);
    void removeByProductTmplId(Long id);
    List<Mrp_bom> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Mrp_bom> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mrp_bom> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Mrp_bom> selectByPickingTypeId(Long id);
    void resetByPickingTypeId(Long id);
    void resetByPickingTypeId(Collection<Long> ids);
    void removeByPickingTypeId(Long id);
    List<Mrp_bom> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mrp_bom> getMrpBomByIds(List<Long> ids) ;
    List<Mrp_bom> getMrpBomByEntities(List<Mrp_bom> entities) ;
}



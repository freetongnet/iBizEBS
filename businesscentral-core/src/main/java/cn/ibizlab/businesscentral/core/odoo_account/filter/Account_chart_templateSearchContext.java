package cn.ibizlab.businesscentral.core.odoo_account.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template;
/**
 * 关系型数据实体[Account_chart_template] 查询条件对象
 */
@Slf4j
@Data
public class Account_chart_templateSearchContext extends QueryWrapperContext<Account_chart_template> {

	private String n_name_like;//[名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_expense_currency_exchange_account_id_text_eq;//[汇率损失科目]
	public void setN_expense_currency_exchange_account_id_text_eq(String n_expense_currency_exchange_account_id_text_eq) {
        this.n_expense_currency_exchange_account_id_text_eq = n_expense_currency_exchange_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_expense_currency_exchange_account_id_text_eq)){
            this.getSearchCond().eq("expense_currency_exchange_account_id_text", n_expense_currency_exchange_account_id_text_eq);
        }
    }
	private String n_expense_currency_exchange_account_id_text_like;//[汇率损失科目]
	public void setN_expense_currency_exchange_account_id_text_like(String n_expense_currency_exchange_account_id_text_like) {
        this.n_expense_currency_exchange_account_id_text_like = n_expense_currency_exchange_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_expense_currency_exchange_account_id_text_like)){
            this.getSearchCond().like("expense_currency_exchange_account_id_text", n_expense_currency_exchange_account_id_text_like);
        }
    }
	private String n_property_account_receivable_id_text_eq;//[应收科目]
	public void setN_property_account_receivable_id_text_eq(String n_property_account_receivable_id_text_eq) {
        this.n_property_account_receivable_id_text_eq = n_property_account_receivable_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_receivable_id_text_eq)){
            this.getSearchCond().eq("property_account_receivable_id_text", n_property_account_receivable_id_text_eq);
        }
    }
	private String n_property_account_receivable_id_text_like;//[应收科目]
	public void setN_property_account_receivable_id_text_like(String n_property_account_receivable_id_text_like) {
        this.n_property_account_receivable_id_text_like = n_property_account_receivable_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_account_receivable_id_text_like)){
            this.getSearchCond().like("property_account_receivable_id_text", n_property_account_receivable_id_text_like);
        }
    }
	private String n_currency_id_text_eq;//[币种]
	public void setN_currency_id_text_eq(String n_currency_id_text_eq) {
        this.n_currency_id_text_eq = n_currency_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_eq)){
            this.getSearchCond().eq("currency_id_text", n_currency_id_text_eq);
        }
    }
	private String n_currency_id_text_like;//[币种]
	public void setN_currency_id_text_like(String n_currency_id_text_like) {
        this.n_currency_id_text_like = n_currency_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_currency_id_text_like)){
            this.getSearchCond().like("currency_id_text", n_currency_id_text_like);
        }
    }
	private String n_property_account_expense_id_text_eq;//[产品模板的费用科目]
	public void setN_property_account_expense_id_text_eq(String n_property_account_expense_id_text_eq) {
        this.n_property_account_expense_id_text_eq = n_property_account_expense_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_expense_id_text_eq)){
            this.getSearchCond().eq("property_account_expense_id_text", n_property_account_expense_id_text_eq);
        }
    }
	private String n_property_account_expense_id_text_like;//[产品模板的费用科目]
	public void setN_property_account_expense_id_text_like(String n_property_account_expense_id_text_like) {
        this.n_property_account_expense_id_text_like = n_property_account_expense_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_account_expense_id_text_like)){
            this.getSearchCond().like("property_account_expense_id_text", n_property_account_expense_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_parent_id_text_eq;//[上级表模板]
	public void setN_parent_id_text_eq(String n_parent_id_text_eq) {
        this.n_parent_id_text_eq = n_parent_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_eq)){
            this.getSearchCond().eq("parent_id_text", n_parent_id_text_eq);
        }
    }
	private String n_parent_id_text_like;//[上级表模板]
	public void setN_parent_id_text_like(String n_parent_id_text_like) {
        this.n_parent_id_text_like = n_parent_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_parent_id_text_like)){
            this.getSearchCond().like("parent_id_text", n_parent_id_text_like);
        }
    }
	private String n_property_stock_account_input_categ_id_text_eq;//[库存计价的入库科目]
	public void setN_property_stock_account_input_categ_id_text_eq(String n_property_stock_account_input_categ_id_text_eq) {
        this.n_property_stock_account_input_categ_id_text_eq = n_property_stock_account_input_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_input_categ_id_text_eq)){
            this.getSearchCond().eq("property_stock_account_input_categ_id_text", n_property_stock_account_input_categ_id_text_eq);
        }
    }
	private String n_property_stock_account_input_categ_id_text_like;//[库存计价的入库科目]
	public void setN_property_stock_account_input_categ_id_text_like(String n_property_stock_account_input_categ_id_text_like) {
        this.n_property_stock_account_input_categ_id_text_like = n_property_stock_account_input_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_input_categ_id_text_like)){
            this.getSearchCond().like("property_stock_account_input_categ_id_text", n_property_stock_account_input_categ_id_text_like);
        }
    }
	private String n_property_account_income_id_text_eq;//[产品模板的收入科目]
	public void setN_property_account_income_id_text_eq(String n_property_account_income_id_text_eq) {
        this.n_property_account_income_id_text_eq = n_property_account_income_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_income_id_text_eq)){
            this.getSearchCond().eq("property_account_income_id_text", n_property_account_income_id_text_eq);
        }
    }
	private String n_property_account_income_id_text_like;//[产品模板的收入科目]
	public void setN_property_account_income_id_text_like(String n_property_account_income_id_text_like) {
        this.n_property_account_income_id_text_like = n_property_account_income_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_account_income_id_text_like)){
            this.getSearchCond().like("property_account_income_id_text", n_property_account_income_id_text_like);
        }
    }
	private String n_property_account_expense_categ_id_text_eq;//[费用科目的类别]
	public void setN_property_account_expense_categ_id_text_eq(String n_property_account_expense_categ_id_text_eq) {
        this.n_property_account_expense_categ_id_text_eq = n_property_account_expense_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_expense_categ_id_text_eq)){
            this.getSearchCond().eq("property_account_expense_categ_id_text", n_property_account_expense_categ_id_text_eq);
        }
    }
	private String n_property_account_expense_categ_id_text_like;//[费用科目的类别]
	public void setN_property_account_expense_categ_id_text_like(String n_property_account_expense_categ_id_text_like) {
        this.n_property_account_expense_categ_id_text_like = n_property_account_expense_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_account_expense_categ_id_text_like)){
            this.getSearchCond().like("property_account_expense_categ_id_text", n_property_account_expense_categ_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_income_currency_exchange_account_id_text_eq;//[汇率增益科目]
	public void setN_income_currency_exchange_account_id_text_eq(String n_income_currency_exchange_account_id_text_eq) {
        this.n_income_currency_exchange_account_id_text_eq = n_income_currency_exchange_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_income_currency_exchange_account_id_text_eq)){
            this.getSearchCond().eq("income_currency_exchange_account_id_text", n_income_currency_exchange_account_id_text_eq);
        }
    }
	private String n_income_currency_exchange_account_id_text_like;//[汇率增益科目]
	public void setN_income_currency_exchange_account_id_text_like(String n_income_currency_exchange_account_id_text_like) {
        this.n_income_currency_exchange_account_id_text_like = n_income_currency_exchange_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_income_currency_exchange_account_id_text_like)){
            this.getSearchCond().like("income_currency_exchange_account_id_text", n_income_currency_exchange_account_id_text_like);
        }
    }
	private String n_property_account_payable_id_text_eq;//[应付科目]
	public void setN_property_account_payable_id_text_eq(String n_property_account_payable_id_text_eq) {
        this.n_property_account_payable_id_text_eq = n_property_account_payable_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_payable_id_text_eq)){
            this.getSearchCond().eq("property_account_payable_id_text", n_property_account_payable_id_text_eq);
        }
    }
	private String n_property_account_payable_id_text_like;//[应付科目]
	public void setN_property_account_payable_id_text_like(String n_property_account_payable_id_text_like) {
        this.n_property_account_payable_id_text_like = n_property_account_payable_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_account_payable_id_text_like)){
            this.getSearchCond().like("property_account_payable_id_text", n_property_account_payable_id_text_like);
        }
    }
	private String n_property_account_income_categ_id_text_eq;//[收入科目的类别]
	public void setN_property_account_income_categ_id_text_eq(String n_property_account_income_categ_id_text_eq) {
        this.n_property_account_income_categ_id_text_eq = n_property_account_income_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_income_categ_id_text_eq)){
            this.getSearchCond().eq("property_account_income_categ_id_text", n_property_account_income_categ_id_text_eq);
        }
    }
	private String n_property_account_income_categ_id_text_like;//[收入科目的类别]
	public void setN_property_account_income_categ_id_text_like(String n_property_account_income_categ_id_text_like) {
        this.n_property_account_income_categ_id_text_like = n_property_account_income_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_account_income_categ_id_text_like)){
            this.getSearchCond().like("property_account_income_categ_id_text", n_property_account_income_categ_id_text_like);
        }
    }
	private String n_property_stock_valuation_account_id_text_eq;//[库存计价的科目模板]
	public void setN_property_stock_valuation_account_id_text_eq(String n_property_stock_valuation_account_id_text_eq) {
        this.n_property_stock_valuation_account_id_text_eq = n_property_stock_valuation_account_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_valuation_account_id_text_eq)){
            this.getSearchCond().eq("property_stock_valuation_account_id_text", n_property_stock_valuation_account_id_text_eq);
        }
    }
	private String n_property_stock_valuation_account_id_text_like;//[库存计价的科目模板]
	public void setN_property_stock_valuation_account_id_text_like(String n_property_stock_valuation_account_id_text_like) {
        this.n_property_stock_valuation_account_id_text_like = n_property_stock_valuation_account_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_stock_valuation_account_id_text_like)){
            this.getSearchCond().like("property_stock_valuation_account_id_text", n_property_stock_valuation_account_id_text_like);
        }
    }
	private String n_property_stock_account_output_categ_id_text_eq;//[库存计价的出货科目]
	public void setN_property_stock_account_output_categ_id_text_eq(String n_property_stock_account_output_categ_id_text_eq) {
        this.n_property_stock_account_output_categ_id_text_eq = n_property_stock_account_output_categ_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_output_categ_id_text_eq)){
            this.getSearchCond().eq("property_stock_account_output_categ_id_text", n_property_stock_account_output_categ_id_text_eq);
        }
    }
	private String n_property_stock_account_output_categ_id_text_like;//[库存计价的出货科目]
	public void setN_property_stock_account_output_categ_id_text_like(String n_property_stock_account_output_categ_id_text_like) {
        this.n_property_stock_account_output_categ_id_text_like = n_property_stock_account_output_categ_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_output_categ_id_text_like)){
            this.getSearchCond().like("property_stock_account_output_categ_id_text", n_property_stock_account_output_categ_id_text_like);
        }
    }
	private Long n_property_stock_valuation_account_id_eq;//[库存计价的科目模板]
	public void setN_property_stock_valuation_account_id_eq(Long n_property_stock_valuation_account_id_eq) {
        this.n_property_stock_valuation_account_id_eq = n_property_stock_valuation_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_valuation_account_id_eq)){
            this.getSearchCond().eq("property_stock_valuation_account_id", n_property_stock_valuation_account_id_eq);
        }
    }
	private Long n_expense_currency_exchange_account_id_eq;//[汇率损失科目]
	public void setN_expense_currency_exchange_account_id_eq(Long n_expense_currency_exchange_account_id_eq) {
        this.n_expense_currency_exchange_account_id_eq = n_expense_currency_exchange_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_expense_currency_exchange_account_id_eq)){
            this.getSearchCond().eq("expense_currency_exchange_account_id", n_expense_currency_exchange_account_id_eq);
        }
    }
	private Long n_property_account_payable_id_eq;//[应付科目]
	public void setN_property_account_payable_id_eq(Long n_property_account_payable_id_eq) {
        this.n_property_account_payable_id_eq = n_property_account_payable_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_payable_id_eq)){
            this.getSearchCond().eq("property_account_payable_id", n_property_account_payable_id_eq);
        }
    }
	private Long n_property_account_income_categ_id_eq;//[收入科目的类别]
	public void setN_property_account_income_categ_id_eq(Long n_property_account_income_categ_id_eq) {
        this.n_property_account_income_categ_id_eq = n_property_account_income_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_income_categ_id_eq)){
            this.getSearchCond().eq("property_account_income_categ_id", n_property_account_income_categ_id_eq);
        }
    }
	private Long n_property_stock_account_output_categ_id_eq;//[库存计价的出货科目]
	public void setN_property_stock_account_output_categ_id_eq(Long n_property_stock_account_output_categ_id_eq) {
        this.n_property_stock_account_output_categ_id_eq = n_property_stock_account_output_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_output_categ_id_eq)){
            this.getSearchCond().eq("property_stock_account_output_categ_id", n_property_stock_account_output_categ_id_eq);
        }
    }
	private Long n_property_account_expense_id_eq;//[产品模板的费用科目]
	public void setN_property_account_expense_id_eq(Long n_property_account_expense_id_eq) {
        this.n_property_account_expense_id_eq = n_property_account_expense_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_expense_id_eq)){
            this.getSearchCond().eq("property_account_expense_id", n_property_account_expense_id_eq);
        }
    }
	private Long n_currency_id_eq;//[币种]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_parent_id_eq;//[上级表模板]
	public void setN_parent_id_eq(Long n_parent_id_eq) {
        this.n_parent_id_eq = n_parent_id_eq;
        if(!ObjectUtils.isEmpty(this.n_parent_id_eq)){
            this.getSearchCond().eq("parent_id", n_parent_id_eq);
        }
    }
	private Long n_property_account_receivable_id_eq;//[应收科目]
	public void setN_property_account_receivable_id_eq(Long n_property_account_receivable_id_eq) {
        this.n_property_account_receivable_id_eq = n_property_account_receivable_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_receivable_id_eq)){
            this.getSearchCond().eq("property_account_receivable_id", n_property_account_receivable_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_property_account_expense_categ_id_eq;//[费用科目的类别]
	public void setN_property_account_expense_categ_id_eq(Long n_property_account_expense_categ_id_eq) {
        this.n_property_account_expense_categ_id_eq = n_property_account_expense_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_expense_categ_id_eq)){
            this.getSearchCond().eq("property_account_expense_categ_id", n_property_account_expense_categ_id_eq);
        }
    }
	private Long n_income_currency_exchange_account_id_eq;//[汇率增益科目]
	public void setN_income_currency_exchange_account_id_eq(Long n_income_currency_exchange_account_id_eq) {
        this.n_income_currency_exchange_account_id_eq = n_income_currency_exchange_account_id_eq;
        if(!ObjectUtils.isEmpty(this.n_income_currency_exchange_account_id_eq)){
            this.getSearchCond().eq("income_currency_exchange_account_id", n_income_currency_exchange_account_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_property_stock_account_input_categ_id_eq;//[库存计价的入库科目]
	public void setN_property_stock_account_input_categ_id_eq(Long n_property_stock_account_input_categ_id_eq) {
        this.n_property_stock_account_input_categ_id_eq = n_property_stock_account_input_categ_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_stock_account_input_categ_id_eq)){
            this.getSearchCond().eq("property_stock_account_input_categ_id", n_property_stock_account_input_categ_id_eq);
        }
    }
	private Long n_property_account_income_id_eq;//[产品模板的收入科目]
	public void setN_property_account_income_id_eq(Long n_property_account_income_id_eq) {
        this.n_property_account_income_id_eq = n_property_account_income_id_eq;
        if(!ObjectUtils.isEmpty(this.n_property_account_income_id_eq)){
            this.getSearchCond().eq("property_account_income_id", n_property_account_income_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




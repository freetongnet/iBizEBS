package cn.ibizlab.businesscentral.core.odoo_purchase.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_purchase.domain.Purchase_requisition;
/**
 * 关系型数据实体[Purchase_requisition] 查询条件对象
 */
@Slf4j
@Data
public class Purchase_requisitionSearchContext extends QueryWrapperContext<Purchase_requisition> {

	private String n_name_like;//[申请编号]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_currency_name_eq;//[币种]
	public void setN_currency_name_eq(String n_currency_name_eq) {
        this.n_currency_name_eq = n_currency_name_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_name_eq)){
            this.getSearchCond().eq("currency_name", n_currency_name_eq);
        }
    }
	private String n_currency_name_like;//[币种]
	public void setN_currency_name_like(String n_currency_name_like) {
        this.n_currency_name_like = n_currency_name_like;
        if(!ObjectUtils.isEmpty(this.n_currency_name_like)){
            this.getSearchCond().like("currency_name", n_currency_name_like);
        }
    }
	private String n_create_uname_eq;//[创建人]
	public void setN_create_uname_eq(String n_create_uname_eq) {
        this.n_create_uname_eq = n_create_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uname_eq)){
            this.getSearchCond().eq("create_uname", n_create_uname_eq);
        }
    }
	private String n_create_uname_like;//[创建人]
	public void setN_create_uname_like(String n_create_uname_like) {
        this.n_create_uname_like = n_create_uname_like;
        if(!ObjectUtils.isEmpty(this.n_create_uname_like)){
            this.getSearchCond().like("create_uname", n_create_uname_like);
        }
    }
	private String n_company_name_eq;//[公司]
	public void setN_company_name_eq(String n_company_name_eq) {
        this.n_company_name_eq = n_company_name_eq;
        if(!ObjectUtils.isEmpty(this.n_company_name_eq)){
            this.getSearchCond().eq("company_name", n_company_name_eq);
        }
    }
	private String n_company_name_like;//[公司]
	public void setN_company_name_like(String n_company_name_like) {
        this.n_company_name_like = n_company_name_like;
        if(!ObjectUtils.isEmpty(this.n_company_name_like)){
            this.getSearchCond().like("company_name", n_company_name_like);
        }
    }
	private String n_user_name_eq;//[采购员]
	public void setN_user_name_eq(String n_user_name_eq) {
        this.n_user_name_eq = n_user_name_eq;
        if(!ObjectUtils.isEmpty(this.n_user_name_eq)){
            this.getSearchCond().eq("user_name", n_user_name_eq);
        }
    }
	private String n_user_name_like;//[采购员]
	public void setN_user_name_like(String n_user_name_like) {
        this.n_user_name_like = n_user_name_like;
        if(!ObjectUtils.isEmpty(this.n_user_name_like)){
            this.getSearchCond().like("user_name", n_user_name_like);
        }
    }
	private String n_type_id_text_eq;//[申请类型]
	public void setN_type_id_text_eq(String n_type_id_text_eq) {
        this.n_type_id_text_eq = n_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_type_id_text_eq)){
            this.getSearchCond().eq("type_id_text", n_type_id_text_eq);
        }
    }
	private String n_type_id_text_like;//[申请类型]
	public void setN_type_id_text_like(String n_type_id_text_like) {
        this.n_type_id_text_like = n_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_type_id_text_like)){
            this.getSearchCond().like("type_id_text", n_type_id_text_like);
        }
    }
	private String n_vendor_id_text_eq;//[供应商]
	public void setN_vendor_id_text_eq(String n_vendor_id_text_eq) {
        this.n_vendor_id_text_eq = n_vendor_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_id_text_eq)){
            this.getSearchCond().eq("vendor_id_text", n_vendor_id_text_eq);
        }
    }
	private String n_vendor_id_text_like;//[供应商]
	public void setN_vendor_id_text_like(String n_vendor_id_text_like) {
        this.n_vendor_id_text_like = n_vendor_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_vendor_id_text_like)){
            this.getSearchCond().like("vendor_id_text", n_vendor_id_text_like);
        }
    }
	private String n_write_uname_eq;//[最后更新人]
	public void setN_write_uname_eq(String n_write_uname_eq) {
        this.n_write_uname_eq = n_write_uname_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uname_eq)){
            this.getSearchCond().eq("write_uname", n_write_uname_eq);
        }
    }
	private String n_write_uname_like;//[最后更新人]
	public void setN_write_uname_like(String n_write_uname_like) {
        this.n_write_uname_like = n_write_uname_like;
        if(!ObjectUtils.isEmpty(this.n_write_uname_like)){
            this.getSearchCond().like("write_uname", n_write_uname_like);
        }
    }
	private Long n_picking_type_id_eq;//[ID]
	public void setN_picking_type_id_eq(Long n_picking_type_id_eq) {
        this.n_picking_type_id_eq = n_picking_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_eq)){
            this.getSearchCond().eq("picking_type_id", n_picking_type_id_eq);
        }
    }
	private Long n_vendor_id_eq;//[供应商]
	public void setN_vendor_id_eq(Long n_vendor_id_eq) {
        this.n_vendor_id_eq = n_vendor_id_eq;
        if(!ObjectUtils.isEmpty(this.n_vendor_id_eq)){
            this.getSearchCond().eq("vendor_id", n_vendor_id_eq);
        }
    }
	private Long n_type_id_eq;//[申请类型]
	public void setN_type_id_eq(Long n_type_id_eq) {
        this.n_type_id_eq = n_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_type_id_eq)){
            this.getSearchCond().eq("type_id", n_type_id_eq);
        }
    }
	private Long n_currency_id_eq;//[ID]
	public void setN_currency_id_eq(Long n_currency_id_eq) {
        this.n_currency_id_eq = n_currency_id_eq;
        if(!ObjectUtils.isEmpty(this.n_currency_id_eq)){
            this.getSearchCond().eq("currency_id", n_currency_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_warehouse_id_eq;//[ID]
	public void setN_warehouse_id_eq(Long n_warehouse_id_eq) {
        this.n_warehouse_id_eq = n_warehouse_id_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_eq)){
            this.getSearchCond().eq("warehouse_id", n_warehouse_id_eq);
        }
    }
	private Long n_user_id_eq;//[采购员]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




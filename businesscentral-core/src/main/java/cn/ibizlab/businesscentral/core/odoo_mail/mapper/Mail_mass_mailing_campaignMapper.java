package cn.ibizlab.businesscentral.core.odoo_mail.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mail_mass_mailing_campaignMapper extends BaseMapper<Mail_mass_mailing_campaign>{

    Page<Mail_mass_mailing_campaign> searchDefault(IPage page, @Param("srf") Mail_mass_mailing_campaignSearchContext context, @Param("ew") Wrapper<Mail_mass_mailing_campaign> wrapper) ;
    @Override
    Mail_mass_mailing_campaign selectById(Serializable id);
    @Override
    int insert(Mail_mass_mailing_campaign entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mail_mass_mailing_campaign entity);
    @Override
    int update(@Param(Constants.ENTITY) Mail_mass_mailing_campaign entity, @Param("ew") Wrapper<Mail_mass_mailing_campaign> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mail_mass_mailing_campaign> selectByStageId(@Param("id") Serializable id) ;

    List<Mail_mass_mailing_campaign> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mail_mass_mailing_campaign> selectByUserId(@Param("id") Serializable id) ;

    List<Mail_mass_mailing_campaign> selectByWriteUid(@Param("id") Serializable id) ;

    List<Mail_mass_mailing_campaign> selectByCampaignId(@Param("id") Serializable id) ;

    List<Mail_mass_mailing_campaign> selectByMediumId(@Param("id") Serializable id) ;

    List<Mail_mass_mailing_campaign> selectBySourceId(@Param("id") Serializable id) ;


}

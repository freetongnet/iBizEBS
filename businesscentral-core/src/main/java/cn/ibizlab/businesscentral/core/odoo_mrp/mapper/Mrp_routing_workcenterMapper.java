package cn.ibizlab.businesscentral.core.odoo_mrp.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing_workcenter;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_routing_workcenterSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mrp_routing_workcenterMapper extends BaseMapper<Mrp_routing_workcenter>{

    Page<Mrp_routing_workcenter> searchDefault(IPage page, @Param("srf") Mrp_routing_workcenterSearchContext context, @Param("ew") Wrapper<Mrp_routing_workcenter> wrapper) ;
    @Override
    Mrp_routing_workcenter selectById(Serializable id);
    @Override
    int insert(Mrp_routing_workcenter entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mrp_routing_workcenter entity);
    @Override
    int update(@Param(Constants.ENTITY) Mrp_routing_workcenter entity, @Param("ew") Wrapper<Mrp_routing_workcenter> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mrp_routing_workcenter> selectByRoutingId(@Param("id") Serializable id) ;

    List<Mrp_routing_workcenter> selectByWorkcenterId(@Param("id") Serializable id) ;

    List<Mrp_routing_workcenter> selectByCompanyId(@Param("id") Serializable id) ;

    List<Mrp_routing_workcenter> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mrp_routing_workcenter> selectByWriteUid(@Param("id") Serializable id) ;


}

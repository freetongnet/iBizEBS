package cn.ibizlab.businesscentral.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_badge;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_badgeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Gamification_badge] 服务对象接口
 */
public interface IGamification_badgeService extends IService<Gamification_badge>{

    boolean create(Gamification_badge et) ;
    void createBatch(List<Gamification_badge> list) ;
    boolean update(Gamification_badge et) ;
    void updateBatch(List<Gamification_badge> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Gamification_badge get(Long key) ;
    Gamification_badge getDraft(Gamification_badge et) ;
    boolean checkKey(Gamification_badge et) ;
    boolean save(Gamification_badge et) ;
    void saveBatch(List<Gamification_badge> list) ;
    Page<Gamification_badge> searchDefault(Gamification_badgeSearchContext context) ;
    List<Gamification_badge> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Gamification_badge> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Gamification_badge> getGamificationBadgeByIds(List<Long> ids) ;
    List<Gamification_badge> getGamificationBadgeByEntities(List<Gamification_badge> entities) ;
}



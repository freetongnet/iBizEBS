package cn.ibizlab.businesscentral.core.odoo_base.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_companySearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[res_company] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base:odoo-base}", contextId = "res-company", fallback = res_companyFallback.class)
public interface res_companyFeignClient {



    @RequestMapping(method = RequestMethod.POST, value = "/res_companies/search")
    Page<Res_company> search(@RequestBody Res_companySearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/res_companies")
    Res_company create(@RequestBody Res_company res_company);

    @RequestMapping(method = RequestMethod.POST, value = "/res_companies/batch")
    Boolean createBatch(@RequestBody List<Res_company> res_companies);


    @RequestMapping(method = RequestMethod.DELETE, value = "/res_companies/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/res_companies/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.PUT, value = "/res_companies/{id}")
    Res_company update(@PathVariable("id") Long id,@RequestBody Res_company res_company);

    @RequestMapping(method = RequestMethod.PUT, value = "/res_companies/batch")
    Boolean updateBatch(@RequestBody List<Res_company> res_companies);



    @RequestMapping(method = RequestMethod.GET, value = "/res_companies/{id}")
    Res_company get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/res_companies/select")
    Page<Res_company> select();


    @RequestMapping(method = RequestMethod.GET, value = "/res_companies/getdraft")
    Res_company getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/res_companies/checkkey")
    Boolean checkKey(@RequestBody Res_company res_company);


    @RequestMapping(method = RequestMethod.POST, value = "/res_companies/save")
    Boolean save(@RequestBody Res_company res_company);

    @RequestMapping(method = RequestMethod.POST, value = "/res_companies/savebatch")
    Boolean saveBatch(@RequestBody List<Res_company> res_companies);



    @RequestMapping(method = RequestMethod.POST, value = "/res_companies/searchdefault")
    Page<Res_company> searchDefault(@RequestBody Res_companySearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/res_companies/searchroot")
    Page<Res_company> searchROOT(@RequestBody Res_companySearchContext context);


}

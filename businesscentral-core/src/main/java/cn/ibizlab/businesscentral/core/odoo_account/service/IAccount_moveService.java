package cn.ibizlab.businesscentral.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_moveSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Account_move] 服务对象接口
 */
public interface IAccount_moveService extends IService<Account_move>{

    boolean create(Account_move et) ;
    void createBatch(List<Account_move> list) ;
    boolean update(Account_move et) ;
    void updateBatch(List<Account_move> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Account_move get(Long key) ;
    Account_move getDraft(Account_move et) ;
    boolean checkKey(Account_move et) ;
    boolean save(Account_move et) ;
    void saveBatch(List<Account_move> list) ;
    Page<Account_move> searchDefault(Account_moveSearchContext context) ;
    List<Account_move> selectByJournalId(Long id);
    void resetByJournalId(Long id);
    void resetByJournalId(Collection<Long> ids);
    void removeByJournalId(Long id);
    List<Account_move> selectByReverseEntryId(Long id);
    void resetByReverseEntryId(Long id);
    void resetByReverseEntryId(Collection<Long> ids);
    void removeByReverseEntryId(Long id);
    List<Account_move> selectByTaxCashBasisRecId(Long id);
    void resetByTaxCashBasisRecId(Long id);
    void resetByTaxCashBasisRecId(Collection<Long> ids);
    void removeByTaxCashBasisRecId(Long id);
    List<Account_move> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Account_move> selectByCurrencyId(Long id);
    void resetByCurrencyId(Long id);
    void resetByCurrencyId(Collection<Long> ids);
    void removeByCurrencyId(Long id);
    List<Account_move> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Account_move> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Account_move> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Account_move> selectByStockMoveId(Long id);
    void resetByStockMoveId(Long id);
    void resetByStockMoveId(Collection<Long> ids);
    void removeByStockMoveId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Account_move> getAccountMoveByIds(List<Long> ids) ;
    List<Account_move> getAccountMoveByEntities(List<Account_move> entities) ;
}



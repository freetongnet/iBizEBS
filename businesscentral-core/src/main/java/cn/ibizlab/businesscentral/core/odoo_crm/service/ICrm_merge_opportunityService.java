package cn.ibizlab.businesscentral.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_crm.domain.Crm_merge_opportunity;
import cn.ibizlab.businesscentral.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Crm_merge_opportunity] 服务对象接口
 */
public interface ICrm_merge_opportunityService extends IService<Crm_merge_opportunity>{

    boolean create(Crm_merge_opportunity et) ;
    void createBatch(List<Crm_merge_opportunity> list) ;
    boolean update(Crm_merge_opportunity et) ;
    void updateBatch(List<Crm_merge_opportunity> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Crm_merge_opportunity get(Long key) ;
    Crm_merge_opportunity getDraft(Crm_merge_opportunity et) ;
    boolean checkKey(Crm_merge_opportunity et) ;
    boolean save(Crm_merge_opportunity et) ;
    void saveBatch(List<Crm_merge_opportunity> list) ;
    Page<Crm_merge_opportunity> searchDefault(Crm_merge_opportunitySearchContext context) ;
    List<Crm_merge_opportunity> selectByTeamId(Long id);
    void resetByTeamId(Long id);
    void resetByTeamId(Collection<Long> ids);
    void removeByTeamId(Long id);
    List<Crm_merge_opportunity> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Crm_merge_opportunity> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Crm_merge_opportunity> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Crm_merge_opportunity> getCrmMergeOpportunityByIds(List<Long> ids) ;
    List<Crm_merge_opportunity> getCrmMergeOpportunityByEntities(List<Crm_merge_opportunity> entities) ;
}



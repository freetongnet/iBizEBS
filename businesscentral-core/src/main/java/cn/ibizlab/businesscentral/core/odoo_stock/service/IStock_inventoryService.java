package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_inventory;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_inventorySearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_inventory] 服务对象接口
 */
public interface IStock_inventoryService extends IService<Stock_inventory>{

    boolean create(Stock_inventory et) ;
    void createBatch(List<Stock_inventory> list) ;
    boolean update(Stock_inventory et) ;
    void updateBatch(List<Stock_inventory> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_inventory get(Long key) ;
    Stock_inventory getDraft(Stock_inventory et) ;
    boolean checkKey(Stock_inventory et) ;
    boolean save(Stock_inventory et) ;
    void saveBatch(List<Stock_inventory> list) ;
    Page<Stock_inventory> searchDefault(Stock_inventorySearchContext context) ;
    List<Stock_inventory> selectByCategoryId(Long id);
    void resetByCategoryId(Long id);
    void resetByCategoryId(Collection<Long> ids);
    void removeByCategoryId(Long id);
    List<Stock_inventory> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_inventory> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Stock_inventory> selectByPartnerId(Long id);
    void resetByPartnerId(Long id);
    void resetByPartnerId(Collection<Long> ids);
    void removeByPartnerId(Long id);
    List<Stock_inventory> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_inventory> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_inventory> selectByLocationId(Long id);
    void resetByLocationId(Long id);
    void resetByLocationId(Collection<Long> ids);
    void removeByLocationId(Long id);
    List<Stock_inventory> selectByLotId(Long id);
    void resetByLotId(Long id);
    void resetByLotId(Collection<Long> ids);
    void removeByLotId(Long id);
    List<Stock_inventory> selectByPackageId(Long id);
    void resetByPackageId(Long id);
    void resetByPackageId(Collection<Long> ids);
    void removeByPackageId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_inventory> getStockInventoryByIds(List<Long> ids) ;
    List<Stock_inventory> getStockInventoryByEntities(List<Stock_inventory> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_confirm;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_confirmSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_invoice_confirm] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-invoice-confirm", fallback = account_invoice_confirmFallback.class)
public interface account_invoice_confirmFeignClient {


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/search")
    Page<Account_invoice_confirm> search(@RequestBody Account_invoice_confirmSearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms")
    Account_invoice_confirm create(@RequestBody Account_invoice_confirm account_invoice_confirm);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/batch")
    Boolean createBatch(@RequestBody List<Account_invoice_confirm> account_invoice_confirms);




    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_confirms/{id}")
    Account_invoice_confirm update(@PathVariable("id") Long id,@RequestBody Account_invoice_confirm account_invoice_confirm);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_confirms/batch")
    Boolean updateBatch(@RequestBody List<Account_invoice_confirm> account_invoice_confirms);



    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_confirms/{id}")
    Account_invoice_confirm get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_confirms/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_confirms/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_confirms/select")
    Page<Account_invoice_confirm> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_confirms/getdraft")
    Account_invoice_confirm getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/checkkey")
    Boolean checkKey(@RequestBody Account_invoice_confirm account_invoice_confirm);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/save")
    Boolean save(@RequestBody Account_invoice_confirm account_invoice_confirm);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/savebatch")
    Boolean saveBatch(@RequestBody List<Account_invoice_confirm> account_invoice_confirms);



    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_confirms/searchdefault")
    Page<Account_invoice_confirm> searchDefault(@RequestBody Account_invoice_confirmSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[寄出邮件]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "MAIL_MAIL",resultMap = "Mail_mailResultMap")
public class Mail_mail extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 通知
     */
    @TableField(value = "notification")
    @JSONField(name = "notification")
    @JsonProperty("notification")
    private Boolean notification;
    /**
     * 安排的发送日期
     */
    @DEField(name = "scheduled_date")
    @TableField(value = "scheduled_date")
    @JSONField(name = "scheduled_date")
    @JsonProperty("scheduled_date")
    private String scheduledDate;
    /**
     * 收藏夹
     */
    @TableField(exist = false)
    @JSONField(name = "starred_partner_ids")
    @JsonProperty("starred_partner_ids")
    private String starredPartnerIds;
    /**
     * 自动删除
     */
    @DEField(name = "auto_delete")
    @TableField(value = "auto_delete")
    @JSONField(name = "auto_delete")
    @JsonProperty("auto_delete")
    private Boolean autoDelete;
    /**
     * 富文本内容
     */
    @DEField(name = "body_html")
    @TableField(value = "body_html")
    @JSONField(name = "body_html")
    @JsonProperty("body_html")
    private String bodyHtml;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 相关评级
     */
    @TableField(exist = false)
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 收件人
     */
    @TableField(exist = false)
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;
    /**
     * 下级消息
     */
    @TableField(exist = false)
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;
    /**
     * 至（合作伙伴）
     */
    @TableField(exist = false)
    @JSONField(name = "recipient_ids")
    @JsonProperty("recipient_ids")
    private String recipientIds;
    /**
     * 追踪值
     */
    @TableField(exist = false)
    @JSONField(name = "tracking_value_ids")
    @JsonProperty("tracking_value_ids")
    private String trackingValueIds;
    /**
     * 待处理的业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "needaction_partner_ids")
    @JsonProperty("needaction_partner_ids")
    private String needactionPartnerIds;
    /**
     * 抄送
     */
    @DEField(name = "email_cc")
    @TableField(value = "email_cc")
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    private String emailCc;
    /**
     * 附件
     */
    @TableField(exist = false)
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;
    /**
     * 失败原因
     */
    @DEField(name = "failure_reason")
    @TableField(value = "failure_reason")
    @JSONField(name = "failure_reason")
    @JsonProperty("failure_reason")
    private String failureReason;
    /**
     * 统计信息
     */
    @TableField(exist = false)
    @JSONField(name = "statistics_ids")
    @JsonProperty("statistics_ids")
    private String statisticsIds;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 标题
     */
    @TableField(value = "headers")
    @JSONField(name = "headers")
    @JsonProperty("headers")
    private String headers;
    /**
     * 至
     */
    @DEField(name = "email_to")
    @TableField(value = "email_to")
    @JSONField(name = "email_to")
    @JsonProperty("email_to")
    private String emailTo;
    /**
     * 渠道
     */
    @TableField(exist = false)
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;
    /**
     * 通知
     */
    @TableField(exist = false)
    @JSONField(name = "notification_ids")
    @JsonProperty("notification_ids")
    private String notificationIds;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 参考
     */
    @TableField(value = "references")
    @JSONField(name = "references")
    @JsonProperty("references")
    private String references;
    /**
     * 群发邮件
     */
    @TableField(exist = false)
    @JSONField(name = "mailing_id_text")
    @JsonProperty("mailing_id_text")
    private String mailingIdText;
    /**
     * 回复 至
     */
    @TableField(exist = false)
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;
    /**
     * 需审核
     */
    @TableField(exist = false)
    @JSONField(name = "need_moderation")
    @JsonProperty("need_moderation")
    private Boolean needModeration;
    /**
     * 消息ID
     */
    @TableField(exist = false)
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;
    /**
     * 无响应
     */
    @TableField(exist = false)
    @JSONField(name = "no_auto_thread")
    @JsonProperty("no_auto_thread")
    private Boolean noAutoThread;
    /**
     * 作者
     */
    @TableField(exist = false)
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    private Long authorId;
    /**
     * 从
     */
    @TableField(exist = false)
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;
    /**
     * 相关的文档模型
     */
    @TableField(exist = false)
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;
    /**
     * 消息记录名称
     */
    @TableField(exist = false)
    @JSONField(name = "record_name")
    @JsonProperty("record_name")
    private String recordName;
    /**
     * 加星的邮件
     */
    @TableField(exist = false)
    @JSONField(name = "starred")
    @JsonProperty("starred")
    private Boolean starred;
    /**
     * 添加签名
     */
    @TableField(exist = false)
    @JSONField(name = "add_sign")
    @JsonProperty("add_sign")
    private Boolean addSign;
    /**
     * 上级消息
     */
    @TableField(exist = false)
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 评级值
     */
    @TableField(exist = false)
    @JSONField(name = "rating_value")
    @JsonProperty("rating_value")
    private Double ratingValue;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 已发布
     */
    @TableField(exist = false)
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;
    /**
     * 内容
     */
    @TableField(exist = false)
    @JSONField(name = "body")
    @JsonProperty("body")
    private String body;
    /**
     * 邮件发送服务器
     */
    @TableField(exist = false)
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;
    /**
     * 说明
     */
    @TableField(exist = false)
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 相关文档编号
     */
    @TableField(exist = false)
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;
    /**
     * 子类型
     */
    @TableField(exist = false)
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    private Long subtypeId;
    /**
     * 收件服务器
     */
    @TableField(exist = false)
    @JSONField(name = "fetchmail_server_id_text")
    @JsonProperty("fetchmail_server_id_text")
    private String fetchmailServerIdText;
    /**
     * 有误差
     */
    @TableField(exist = false)
    @JSONField(name = "has_error")
    @JsonProperty("has_error")
    private Boolean hasError;
    /**
     * 日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 作者头像
     */
    @TableField(exist = false)
    @JSONField(name = "author_avatar")
    @JsonProperty("author_avatar")
    private byte[] authorAvatar;
    /**
     * 管理状态
     */
    @TableField(exist = false)
    @JSONField(name = "moderation_status")
    @JsonProperty("moderation_status")
    private String moderationStatus;
    /**
     * 邮件活动类型
     */
    @TableField(exist = false)
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    private Long mailActivityTypeId;
    /**
     * 管理员
     */
    @TableField(exist = false)
    @JSONField(name = "moderator_id")
    @JsonProperty("moderator_id")
    private Long moderatorId;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "message_type")
    @JsonProperty("message_type")
    private String messageType;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 主题
     */
    @TableField(exist = false)
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;
    /**
     * 待处理
     */
    @TableField(exist = false)
    @JSONField(name = "needaction")
    @JsonProperty("needaction")
    private Boolean needaction;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 群发邮件
     */
    @DEField(name = "mailing_id")
    @TableField(value = "mailing_id")
    @JSONField(name = "mailing_id")
    @JsonProperty("mailing_id")
    private Long mailingId;
    /**
     * 收件服务器
     */
    @DEField(name = "fetchmail_server_id")
    @TableField(value = "fetchmail_server_id")
    @JSONField(name = "fetchmail_server_id")
    @JsonProperty("fetchmail_server_id")
    private Long fetchmailServerId;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 消息
     */
    @DEField(name = "mail_message_id")
    @TableField(value = "mail_message_id")
    @JSONField(name = "mail_message_id")
    @JsonProperty("mail_message_id")
    private Long mailMessageId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_fetchmail.domain.Fetchmail_server odooFetchmailServer;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing odooMailing;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message odooMailMessage;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [通知]
     */
    public void setNotification(Boolean notification){
        this.notification = notification ;
        this.modify("notification",notification);
    }

    /**
     * 设置 [安排的发送日期]
     */
    public void setScheduledDate(String scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }

    /**
     * 设置 [自动删除]
     */
    public void setAutoDelete(Boolean autoDelete){
        this.autoDelete = autoDelete ;
        this.modify("auto_delete",autoDelete);
    }

    /**
     * 设置 [富文本内容]
     */
    public void setBodyHtml(String bodyHtml){
        this.bodyHtml = bodyHtml ;
        this.modify("body_html",bodyHtml);
    }

    /**
     * 设置 [抄送]
     */
    public void setEmailCc(String emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }

    /**
     * 设置 [失败原因]
     */
    public void setFailureReason(String failureReason){
        this.failureReason = failureReason ;
        this.modify("failure_reason",failureReason);
    }

    /**
     * 设置 [标题]
     */
    public void setHeaders(String headers){
        this.headers = headers ;
        this.modify("headers",headers);
    }

    /**
     * 设置 [至]
     */
    public void setEmailTo(String emailTo){
        this.emailTo = emailTo ;
        this.modify("email_to",emailTo);
    }

    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [参考]
     */
    public void setReferences(String references){
        this.references = references ;
        this.modify("references",references);
    }

    /**
     * 设置 [群发邮件]
     */
    public void setMailingId(Long mailingId){
        this.mailingId = mailingId ;
        this.modify("mailing_id",mailingId);
    }

    /**
     * 设置 [收件服务器]
     */
    public void setFetchmailServerId(Long fetchmailServerId){
        this.fetchmailServerId = fetchmailServerId ;
        this.modify("fetchmail_server_id",fetchmailServerId);
    }

    /**
     * 设置 [消息]
     */
    public void setMailMessageId(Long mailMessageId){
        this.mailMessageId = mailMessageId ;
        this.modify("mail_message_id",mailMessageId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



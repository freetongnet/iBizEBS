package cn.ibizlab.businesscentral.core.odoo_website.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.businesscentral.core.odoo_website.filter.Website_redirectSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[website_redirect] 服务对象接口
 */
@Component
public class website_redirectFallback implements website_redirectFeignClient{

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }

    public Website_redirect update(Long id, Website_redirect website_redirect){
            return null;
     }
    public Boolean updateBatch(List<Website_redirect> website_redirects){
            return false;
     }


    public Page<Website_redirect> search(Website_redirectSearchContext context){
            return null;
     }


    public Website_redirect get(Long id){
            return null;
     }




    public Website_redirect create(Website_redirect website_redirect){
            return null;
     }
    public Boolean createBatch(List<Website_redirect> website_redirects){
            return false;
     }


    public Page<Website_redirect> select(){
            return null;
     }

    public Website_redirect getDraft(){
            return null;
    }



    public Boolean checkKey(Website_redirect website_redirect){
            return false;
     }


    public Boolean save(Website_redirect website_redirect){
            return false;
     }
    public Boolean saveBatch(List<Website_redirect> website_redirects){
            return false;
     }

    public Page<Website_redirect> searchDefault(Website_redirectSearchContext context){
            return null;
     }


}

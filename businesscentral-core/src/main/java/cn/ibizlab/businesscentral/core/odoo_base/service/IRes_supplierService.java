package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_supplier;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_supplierSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Res_supplier] 服务对象接口
 */
public interface IRes_supplierService extends IService<Res_supplier>{

    boolean create(Res_supplier et) ;
    void createBatch(List<Res_supplier> list) ;
    boolean update(Res_supplier et) ;
    void updateBatch(List<Res_supplier> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Res_supplier get(Long key) ;
    Res_supplier getDraft(Res_supplier et) ;
    boolean checkKey(Res_supplier et) ;
    Res_supplier masterTabCount(Res_supplier et) ;
    boolean save(Res_supplier et) ;
    void saveBatch(List<Res_supplier> list) ;
    Page<Res_supplier> searchDefault(Res_supplierSearchContext context) ;
    Page<Res_supplier> searchMaster(Res_supplierSearchContext context) ;
    List<Res_supplier> selectByPropertyAccountPositionId(Long id);
    void removeByPropertyAccountPositionId(Long id);
    List<Res_supplier> selectByPropertyPaymentTermId(Long id);
    void removeByPropertyPaymentTermId(Long id);
    List<Res_supplier> selectByPropertySupplierPaymentTermId(Long id);
    void removeByPropertySupplierPaymentTermId(Long id);
    List<Res_supplier> selectByPropertyDeliveryCarrierId(Long id);
    void removeByPropertyDeliveryCarrierId(Long id);
    List<Res_supplier> selectByPropertyProductPricelist(Long id);
    void removeByPropertyProductPricelist(Long id);
    List<Res_supplier> selectByCompanyId(Long id);
    void removeByCompanyId(Long id);
    List<Res_supplier> selectByCountryId(Long id);
    void removeByCountryId(Long id);
    List<Res_supplier> selectByStateId(Long id);
    void removeByStateId(Long id);
    List<Res_supplier> selectByPropertyPurchaseCurrencyId(Long id);
    void removeByPropertyPurchaseCurrencyId(Long id);
    List<Res_supplier> selectByParentId(Long id);
    void removeByParentId(Long id);
    List<Res_supplier> selectByTitle(Long id);
    void removeByTitle(Long id);
    List<Res_supplier> selectByUserId(Long id);
    void removeByUserId(Long id);
    List<Res_supplier> selectByPropertyStockCustomer(Long id);
    void removeByPropertyStockCustomer(Long id);
    List<Res_supplier> selectByPropertyStockSubcontractor(Long id);
    void removeByPropertyStockSubcontractor(Long id);
    List<Res_supplier> selectByPropertyStockSupplier(Long id);
    void removeByPropertyStockSupplier(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Res_supplier> getResSupplierByIds(List<Long> ids) ;
    List<Res_supplier> getResSupplierByEntities(List<Res_supplier> entities) ;
}



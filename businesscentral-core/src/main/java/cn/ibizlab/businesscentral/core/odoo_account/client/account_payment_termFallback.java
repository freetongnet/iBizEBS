package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_payment_termSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[account_payment_term] 服务对象接口
 */
@Component
public class account_payment_termFallback implements account_payment_termFeignClient{

    public Page<Account_payment_term> search(Account_payment_termSearchContext context){
            return null;
     }


    public Account_payment_term create(Account_payment_term account_payment_term){
            return null;
     }
    public Boolean createBatch(List<Account_payment_term> account_payment_terms){
            return false;
     }



    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Account_payment_term update(Long id, Account_payment_term account_payment_term){
            return null;
     }
    public Boolean updateBatch(List<Account_payment_term> account_payment_terms){
            return false;
     }


    public Account_payment_term get(Long id){
            return null;
     }


    public Page<Account_payment_term> select(){
            return null;
     }

    public Account_payment_term getDraft(){
            return null;
    }



    public Boolean checkKey(Account_payment_term account_payment_term){
            return false;
     }


    public Boolean save(Account_payment_term account_payment_term){
            return false;
     }
    public Boolean saveBatch(List<Account_payment_term> account_payment_terms){
            return false;
     }

    public Page<Account_payment_term> searchDefault(Account_payment_termSearchContext context){
            return null;
     }


}

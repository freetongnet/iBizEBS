package cn.ibizlab.businesscentral.core.odoo_mro.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_request_rejectSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mro_request_rejectMapper extends BaseMapper<Mro_request_reject>{

    Page<Mro_request_reject> searchDefault(IPage page, @Param("srf") Mro_request_rejectSearchContext context, @Param("ew") Wrapper<Mro_request_reject> wrapper) ;
    @Override
    Mro_request_reject selectById(Serializable id);
    @Override
    int insert(Mro_request_reject entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mro_request_reject entity);
    @Override
    int update(@Param(Constants.ENTITY) Mro_request_reject entity, @Param("ew") Wrapper<Mro_request_reject> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mro_request_reject> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mro_request_reject> selectByWriteUid(@Param("id") Serializable id) ;


}

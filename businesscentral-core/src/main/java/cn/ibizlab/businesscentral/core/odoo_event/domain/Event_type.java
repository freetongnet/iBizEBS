package cn.ibizlab.businesscentral.core.odoo_event.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[活动类别]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "EVENT_TYPE",resultMap = "Event_typeResultMap")
public class Event_type extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 最大注册数
     */
    @DEField(name = "default_registration_max")
    @TableField(value = "default_registration_max")
    @JSONField(name = "default_registration_max")
    @JsonProperty("default_registration_max")
    private Integer defaultRegistrationMax;
    /**
     * 邮件排程
     */
    @TableField(exist = false)
    @JSONField(name = "event_type_mail_ids")
    @JsonProperty("event_type_mail_ids")
    private String eventTypeMailIds;
    /**
     * Twitter主题标签
     */
    @DEField(name = "default_hashtag")
    @TableField(value = "default_hashtag")
    @JSONField(name = "default_hashtag")
    @JsonProperty("default_hashtag")
    private String defaultHashtag;
    /**
     * 时区
     */
    @DEField(name = "default_timezone")
    @TableField(value = "default_timezone")
    @JSONField(name = "default_timezone")
    @JsonProperty("default_timezone")
    private String defaultTimezone;
    /**
     * 最小注册数
     */
    @DEField(name = "default_registration_min")
    @TableField(value = "default_registration_min")
    @JSONField(name = "default_registration_min")
    @JsonProperty("default_registration_min")
    private Integer defaultRegistrationMin;
    /**
     * 入场券
     */
    @TableField(exist = false)
    @JSONField(name = "event_ticket_ids")
    @JsonProperty("event_ticket_ids")
    private String eventTicketIds;
    /**
     * 出票
     */
    @DEField(name = "use_ticketing")
    @TableField(value = "use_ticketing")
    @JSONField(name = "use_ticketing")
    @JsonProperty("use_ticketing")
    private Boolean useTicketing;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 在线活动
     */
    @DEField(name = "is_online")
    @TableField(value = "is_online")
    @JSONField(name = "is_online")
    @JsonProperty("is_online")
    private Boolean isOnline;
    /**
     * 活动类别
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 自动确认注册
     */
    @DEField(name = "auto_confirm")
    @TableField(value = "auto_confirm")
    @JSONField(name = "auto_confirm")
    @JsonProperty("auto_confirm")
    private Boolean autoConfirm;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 在网站上显示专用菜单
     */
    @DEField(name = "website_menu")
    @TableField(value = "website_menu")
    @JSONField(name = "website_menu")
    @JsonProperty("website_menu")
    private Boolean websiteMenu;
    /**
     * 使用默认主题标签
     */
    @DEField(name = "use_hashtag")
    @TableField(value = "use_hashtag")
    @JSONField(name = "use_hashtag")
    @JsonProperty("use_hashtag")
    private Boolean useHashtag;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 有限名额
     */
    @DEField(name = "has_seats_limitation")
    @TableField(value = "has_seats_limitation")
    @JSONField(name = "has_seats_limitation")
    @JsonProperty("has_seats_limitation")
    private Boolean hasSeatsLimitation;
    /**
     * 自动发送EMail
     */
    @DEField(name = "use_mail_schedule")
    @TableField(value = "use_mail_schedule")
    @JSONField(name = "use_mail_schedule")
    @JsonProperty("use_mail_schedule")
    private Boolean useMailSchedule;
    /**
     * 使用默认时区
     */
    @DEField(name = "use_timezone")
    @TableField(value = "use_timezone")
    @JSONField(name = "use_timezone")
    @JsonProperty("use_timezone")
    private Boolean useTimezone;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [最大注册数]
     */
    public void setDefaultRegistrationMax(Integer defaultRegistrationMax){
        this.defaultRegistrationMax = defaultRegistrationMax ;
        this.modify("default_registration_max",defaultRegistrationMax);
    }

    /**
     * 设置 [Twitter主题标签]
     */
    public void setDefaultHashtag(String defaultHashtag){
        this.defaultHashtag = defaultHashtag ;
        this.modify("default_hashtag",defaultHashtag);
    }

    /**
     * 设置 [时区]
     */
    public void setDefaultTimezone(String defaultTimezone){
        this.defaultTimezone = defaultTimezone ;
        this.modify("default_timezone",defaultTimezone);
    }

    /**
     * 设置 [最小注册数]
     */
    public void setDefaultRegistrationMin(Integer defaultRegistrationMin){
        this.defaultRegistrationMin = defaultRegistrationMin ;
        this.modify("default_registration_min",defaultRegistrationMin);
    }

    /**
     * 设置 [出票]
     */
    public void setUseTicketing(Boolean useTicketing){
        this.useTicketing = useTicketing ;
        this.modify("use_ticketing",useTicketing);
    }

    /**
     * 设置 [在线活动]
     */
    public void setIsOnline(Boolean isOnline){
        this.isOnline = isOnline ;
        this.modify("is_online",isOnline);
    }

    /**
     * 设置 [活动类别]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [自动确认注册]
     */
    public void setAutoConfirm(Boolean autoConfirm){
        this.autoConfirm = autoConfirm ;
        this.modify("auto_confirm",autoConfirm);
    }

    /**
     * 设置 [在网站上显示专用菜单]
     */
    public void setWebsiteMenu(Boolean websiteMenu){
        this.websiteMenu = websiteMenu ;
        this.modify("website_menu",websiteMenu);
    }

    /**
     * 设置 [使用默认主题标签]
     */
    public void setUseHashtag(Boolean useHashtag){
        this.useHashtag = useHashtag ;
        this.modify("use_hashtag",useHashtag);
    }

    /**
     * 设置 [有限名额]
     */
    public void setHasSeatsLimitation(Boolean hasSeatsLimitation){
        this.hasSeatsLimitation = hasSeatsLimitation ;
        this.modify("has_seats_limitation",hasSeatsLimitation);
    }

    /**
     * 设置 [自动发送EMail]
     */
    public void setUseMailSchedule(Boolean useMailSchedule){
        this.useMailSchedule = useMailSchedule ;
        this.modify("use_mail_schedule",useMailSchedule);
    }

    /**
     * 设置 [使用默认时区]
     */
    public void setUseTimezone(Boolean useTimezone){
        this.useTimezone = useTimezone ;
        this.modify("use_timezone",useTimezone);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



package cn.ibizlab.businesscentral.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;
import cn.ibizlab.businesscentral.core.odoo_base.service.IBase_partner_merge_automatic_wizardService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_base.mapper.Base_partner_merge_automatic_wizardMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[合并业务伙伴向导] 服务对象接口实现
 */
@Slf4j
@Service("Base_partner_merge_automatic_wizardServiceImpl")
public class Base_partner_merge_automatic_wizardServiceImpl extends EBSServiceImpl<Base_partner_merge_automatic_wizardMapper, Base_partner_merge_automatic_wizard> implements IBase_partner_merge_automatic_wizardService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IBase_partner_merge_lineService basePartnerMergeLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_partnerService resPartnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "base.partner.merge.automatic.wizard" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Base_partner_merge_automatic_wizard et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IBase_partner_merge_automatic_wizardService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Base_partner_merge_automatic_wizard> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Base_partner_merge_automatic_wizard et) {
        Base_partner_merge_automatic_wizard old = new Base_partner_merge_automatic_wizard() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IBase_partner_merge_automatic_wizardService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IBase_partner_merge_automatic_wizardService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Base_partner_merge_automatic_wizard> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        basePartnerMergeLineService.resetByWizardId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        basePartnerMergeLineService.resetByWizardId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Base_partner_merge_automatic_wizard get(Long key) {
        Base_partner_merge_automatic_wizard et = getById(key);
        if(et==null){
            et=new Base_partner_merge_automatic_wizard();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Base_partner_merge_automatic_wizard getDraft(Base_partner_merge_automatic_wizard et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Base_partner_merge_automatic_wizard et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Base_partner_merge_automatic_wizard et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Base_partner_merge_automatic_wizard et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Base_partner_merge_automatic_wizard> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Base_partner_merge_automatic_wizard> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Base_partner_merge_automatic_wizard> selectByCurrentLineId(Long id) {
        return baseMapper.selectByCurrentLineId(id);
    }
    @Override
    public void resetByCurrentLineId(Long id) {
        this.update(new UpdateWrapper<Base_partner_merge_automatic_wizard>().set("current_line_id",null).eq("current_line_id",id));
    }

    @Override
    public void resetByCurrentLineId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Base_partner_merge_automatic_wizard>().set("current_line_id",null).in("current_line_id",ids));
    }

    @Override
    public void removeByCurrentLineId(Long id) {
        this.remove(new QueryWrapper<Base_partner_merge_automatic_wizard>().eq("current_line_id",id));
    }

	@Override
    public List<Base_partner_merge_automatic_wizard> selectByDstPartnerId(Long id) {
        return baseMapper.selectByDstPartnerId(id);
    }
    @Override
    public void resetByDstPartnerId(Long id) {
        this.update(new UpdateWrapper<Base_partner_merge_automatic_wizard>().set("dst_partner_id",null).eq("dst_partner_id",id));
    }

    @Override
    public void resetByDstPartnerId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Base_partner_merge_automatic_wizard>().set("dst_partner_id",null).in("dst_partner_id",ids));
    }

    @Override
    public void removeByDstPartnerId(Long id) {
        this.remove(new QueryWrapper<Base_partner_merge_automatic_wizard>().eq("dst_partner_id",id));
    }

	@Override
    public List<Base_partner_merge_automatic_wizard> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Base_partner_merge_automatic_wizard>().eq("create_uid",id));
    }

	@Override
    public List<Base_partner_merge_automatic_wizard> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Base_partner_merge_automatic_wizard>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Base_partner_merge_automatic_wizard> searchDefault(Base_partner_merge_automatic_wizardSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Base_partner_merge_automatic_wizard> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Base_partner_merge_automatic_wizard>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Base_partner_merge_automatic_wizard et){
        //实体关系[DER1N_BASE_PARTNER_MERGE_AUTOMATIC_WIZARD__RES_PARTNER__DST_PARTNER_ID]
        if(!ObjectUtils.isEmpty(et.getDstPartnerId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooDstPartner=et.getOdooDstPartner();
            if(ObjectUtils.isEmpty(odooDstPartner)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner majorEntity=resPartnerService.get(et.getDstPartnerId());
                et.setOdooDstPartner(majorEntity);
                odooDstPartner=majorEntity;
            }
            et.setDstPartnerIdText(odooDstPartner.getName());
        }
        //实体关系[DER1N_BASE_PARTNER_MERGE_AUTOMATIC_WIZARD__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_BASE_PARTNER_MERGE_AUTOMATIC_WIZARD__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Base_partner_merge_automatic_wizard> getBasePartnerMergeAutomaticWizardByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Base_partner_merge_automatic_wizard> getBasePartnerMergeAutomaticWizardByEntities(List<Base_partner_merge_automatic_wizard> entities) {
        List ids =new ArrayList();
        for(Base_partner_merge_automatic_wizard entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




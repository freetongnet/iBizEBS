package cn.ibizlab.businesscentral.core.odoo_mail.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
/**
 * 关系型数据实体[Mail_mass_mailing_list_contact_rel] 查询条件对象
 */
@Slf4j
@Data
public class Mail_mass_mailing_list_contact_relSearchContext extends QueryWrapperContext<Mail_mass_mailing_list_contact_rel> {

	private Long n_id_like;//[ID]
	public void setN_id_like(Long n_id_like) {
        this.n_id_like = n_id_like;
        if(!ObjectUtils.isEmpty(this.n_id_like)){
            this.getSearchCond().like("id", n_id_like);
        }
    }
	private String n_list_id_text_eq;//[邮件列表]
	public void setN_list_id_text_eq(String n_list_id_text_eq) {
        this.n_list_id_text_eq = n_list_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_list_id_text_eq)){
            this.getSearchCond().eq("list_id_text", n_list_id_text_eq);
        }
    }
	private String n_list_id_text_like;//[邮件列表]
	public void setN_list_id_text_like(String n_list_id_text_like) {
        this.n_list_id_text_like = n_list_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_list_id_text_like)){
            this.getSearchCond().like("list_id_text", n_list_id_text_like);
        }
    }
	private String n_contact_id_text_eq;//[联系]
	public void setN_contact_id_text_eq(String n_contact_id_text_eq) {
        this.n_contact_id_text_eq = n_contact_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_contact_id_text_eq)){
            this.getSearchCond().eq("contact_id_text", n_contact_id_text_eq);
        }
    }
	private String n_contact_id_text_like;//[联系]
	public void setN_contact_id_text_like(String n_contact_id_text_like) {
        this.n_contact_id_text_like = n_contact_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_contact_id_text_like)){
            this.getSearchCond().like("contact_id_text", n_contact_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_contact_id_eq;//[联系]
	public void setN_contact_id_eq(Long n_contact_id_eq) {
        this.n_contact_id_eq = n_contact_id_eq;
        if(!ObjectUtils.isEmpty(this.n_contact_id_eq)){
            this.getSearchCond().eq("contact_id", n_contact_id_eq);
        }
    }
	private Long n_list_id_eq;//[邮件列表]
	public void setN_list_id_eq(Long n_list_id_eq) {
        this.n_list_id_eq = n_list_id_eq;
        if(!ObjectUtils.isEmpty(this.n_list_id_eq)){
            this.getSearchCond().eq("list_id", n_list_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("id", query)   
            );
		 }
	}
}




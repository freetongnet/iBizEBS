package cn.ibizlab.businesscentral.core.odoo_account.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice_refund;
import cn.ibizlab.businesscentral.core.odoo_account.filter.Account_invoice_refundSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[account_invoice_refund] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-account:odoo-account}", contextId = "account-invoice-refund", fallback = account_invoice_refundFallback.class)
public interface account_invoice_refundFeignClient {


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_refunds/{id}")
    Account_invoice_refund get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_refunds/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_refunds/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/search")
    Page<Account_invoice_refund> search(@RequestBody Account_invoice_refundSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_refunds/{id}")
    Account_invoice_refund update(@PathVariable("id") Long id,@RequestBody Account_invoice_refund account_invoice_refund);

    @RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_refunds/batch")
    Boolean updateBatch(@RequestBody List<Account_invoice_refund> account_invoice_refunds);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds")
    Account_invoice_refund create(@RequestBody Account_invoice_refund account_invoice_refund);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/batch")
    Boolean createBatch(@RequestBody List<Account_invoice_refund> account_invoice_refunds);



    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_refunds/select")
    Page<Account_invoice_refund> select();


    @RequestMapping(method = RequestMethod.GET, value = "/account_invoice_refunds/getdraft")
    Account_invoice_refund getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/checkkey")
    Boolean checkKey(@RequestBody Account_invoice_refund account_invoice_refund);


    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/save")
    Boolean save(@RequestBody Account_invoice_refund account_invoice_refund);

    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/savebatch")
    Boolean saveBatch(@RequestBody List<Account_invoice_refund> account_invoice_refunds);



    @RequestMapping(method = RequestMethod.POST, value = "/account_invoice_refunds/searchdefault")
    Page<Account_invoice_refund> searchDefault(@RequestBody Account_invoice_refundSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[日记账项目]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_MOVE_LINE",resultMap = "Account_move_lineResultMap")
public class Account_move_line extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分析标签
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 标签
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 余额
     */
    @TableField(value = "balance")
    @JSONField(name = "balance")
    @JsonProperty("balance")
    private BigDecimal balance;
    /**
     * 采用的税
     */
    @TableField(exist = false)
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    private String taxIds;
    /**
     * 货币金额
     */
    @DEField(name = "amount_currency")
    @TableField(value = "amount_currency")
    @JSONField(name = "amount_currency")
    @JsonProperty("amount_currency")
    private BigDecimal amountCurrency;
    /**
     * 旧税额
     */
    @TableField(exist = false)
    @JSONField(name = "tax_line_grouping_key")
    @JsonProperty("tax_line_grouping_key")
    private String taxLineGroupingKey;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 已核销
     */
    @TableField(value = "reconciled")
    @JSONField(name = "reconciled")
    @JsonProperty("reconciled")
    private Boolean reconciled;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 现金余额
     */
    @DEField(name = "balance_cash_basis")
    @TableField(value = "balance_cash_basis")
    @JSONField(name = "balance_cash_basis")
    @JsonProperty("balance_cash_basis")
    private BigDecimal balanceCashBasis;
    /**
     * 贷方现金基础
     */
    @DEField(name = "credit_cash_basis")
    @TableField(value = "credit_cash_basis")
    @JSONField(name = "credit_cash_basis")
    @JsonProperty("credit_cash_basis")
    private BigDecimal creditCashBasis;
    /**
     * 数量
     */
    @TableField(value = "quantity")
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private Double quantity;
    /**
     * 残值金额
     */
    @DEField(name = "amount_residual")
    @TableField(value = "amount_residual")
    @JSONField(name = "amount_residual")
    @JsonProperty("amount_residual")
    private BigDecimal amountResidual;
    /**
     * 重新计算税额
     */
    @TableField(exist = false)
    @JSONField(name = "recompute_tax_line")
    @JsonProperty("recompute_tax_line")
    private Boolean recomputeTaxLine;
    /**
     * 出现在VAT报告
     */
    @DEField(name = "tax_exigible")
    @TableField(value = "tax_exigible")
    @JSONField(name = "tax_exigible")
    @JsonProperty("tax_exigible")
    private Boolean taxExigible;
    /**
     * 外币残余金额
     */
    @DEField(name = "amount_residual_currency")
    @TableField(value = "amount_residual_currency")
    @JSONField(name = "amount_residual_currency")
    @JsonProperty("amount_residual_currency")
    private BigDecimal amountResidualCurrency;
    /**
     * 基本金额
     */
    @DEField(name = "tax_base_amount")
    @TableField(value = "tax_base_amount")
    @JSONField(name = "tax_base_amount")
    @JsonProperty("tax_base_amount")
    private BigDecimal taxBaseAmount;
    /**
     * 上级状态
     */
    @TableField(exist = false)
    @JSONField(name = "parent_state")
    @JsonProperty("parent_state")
    private String parentState;
    /**
     * 无催款
     */
    @TableField(value = "blocked")
    @JSONField(name = "blocked")
    @JsonProperty("blocked")
    private Boolean blocked;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 匹配的贷方
     */
    @TableField(exist = false)
    @JSONField(name = "matched_credit_ids")
    @JsonProperty("matched_credit_ids")
    private String matchedCreditIds;
    /**
     * 分析明细行
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_line_ids")
    @JsonProperty("analytic_line_ids")
    private String analyticLineIds;
    /**
     * 到期日期
     */
    @DEField(name = "date_maturity")
    @TableField(value = "date_maturity")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_maturity" , format="yyyy-MM-dd")
    @JsonProperty("date_maturity")
    private Timestamp dateMaturity;
    /**
     * 借方
     */
    @TableField(value = "debit")
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private BigDecimal debit;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 对方
     */
    @TableField(exist = false)
    @JSONField(name = "counterpart")
    @JsonProperty("counterpart")
    private String counterpart;
    /**
     * 匹配的借记卡
     */
    @TableField(exist = false)
    @JSONField(name = "matched_debit_ids")
    @JsonProperty("matched_debit_ids")
    private String matchedDebitIds;
    /**
     * 借记现金基础
     */
    @DEField(name = "debit_cash_basis")
    @TableField(value = "debit_cash_basis")
    @JSONField(name = "debit_cash_basis")
    @JsonProperty("debit_cash_basis")
    private BigDecimal debitCashBasis;
    /**
     * 贷方
     */
    @TableField(value = "credit")
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private BigDecimal credit;
    /**
     * 记叙
     */
    @TableField(exist = false)
    @JSONField(name = "narration")
    @JsonProperty("narration")
    private String narration;
    /**
     * 产品
     */
    @TableField(exist = false)
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "user_type_id_text")
    @JsonProperty("user_type_id_text")
    private String userTypeIdText;
    /**
     * 发起人付款
     */
    @TableField(exist = false)
    @JSONField(name = "payment_id_text")
    @JsonProperty("payment_id_text")
    private String paymentIdText;
    /**
     * 发起人税
     */
    @TableField(exist = false)
    @JSONField(name = "tax_line_id_text")
    @JsonProperty("tax_line_id_text")
    private String taxLineIdText;
    /**
     * 匹配号码
     */
    @TableField(exist = false)
    @JSONField(name = "full_reconcile_id_text")
    @JsonProperty("full_reconcile_id_text")
    private String fullReconcileIdText;
    /**
     * 费用
     */
    @TableField(exist = false)
    @JSONField(name = "expense_id_text")
    @JsonProperty("expense_id_text")
    private String expenseIdText;
    /**
     * 日记账分录
     */
    @TableField(exist = false)
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;
    /**
     * 日记账
     */
    @TableField(exist = false)
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;
    /**
     * 公司货币
     */
    @TableField(exist = false)
    @JSONField(name = "company_currency_id_text")
    @JsonProperty("company_currency_id_text")
    private String companyCurrencyIdText;
    /**
     * 发票
     */
    @TableField(exist = false)
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    private String invoiceIdText;
    /**
     * 分析账户
     */
    @TableField(exist = false)
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    private String analyticAccountIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;
    /**
     * 参考
     */
    @TableField(exist = false)
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 报告
     */
    @TableField(exist = false)
    @JSONField(name = "statement_id_text")
    @JsonProperty("statement_id_text")
    private String statementIdText;
    /**
     * 用该分录核销的银行核销单明细
     */
    @TableField(exist = false)
    @JSONField(name = "statement_line_id_text")
    @JsonProperty("statement_line_id_text")
    private String statementLineIdText;
    /**
     * 业务伙伴
     */
    @TableField(exist = false)
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;
    /**
     * 科目
     */
    @TableField(exist = false)
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;
    /**
     * 日期
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 计量单位
     */
    @TableField(exist = false)
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 日记账分录
     */
    @DEField(name = "move_id")
    @TableField(value = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Long moveId;
    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @TableField(value = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Long currencyId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 发起人付款
     */
    @DEField(name = "payment_id")
    @TableField(value = "payment_id")
    @JSONField(name = "payment_id")
    @JsonProperty("payment_id")
    private Long paymentId;
    /**
     * 公司货币
     */
    @DEField(name = "company_currency_id")
    @TableField(value = "company_currency_id")
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Long companyCurrencyId;
    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @TableField(value = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Long journalId;
    /**
     * 分析账户
     */
    @DEField(name = "analytic_account_id")
    @TableField(value = "analytic_account_id")
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    private Long analyticAccountId;
    /**
     * 发票
     */
    @DEField(name = "invoice_id")
    @TableField(value = "invoice_id")
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    private Long invoiceId;
    /**
     * 发起人税
     */
    @DEField(name = "tax_line_id")
    @TableField(value = "tax_line_id")
    @JSONField(name = "tax_line_id")
    @JsonProperty("tax_line_id")
    private Long taxLineId;
    /**
     * 匹配号码
     */
    @DEField(name = "full_reconcile_id")
    @TableField(value = "full_reconcile_id")
    @JSONField(name = "full_reconcile_id")
    @JsonProperty("full_reconcile_id")
    private Long fullReconcileId;
    /**
     * 用该分录核销的银行核销单明细
     */
    @DEField(name = "statement_line_id")
    @TableField(value = "statement_line_id")
    @JSONField(name = "statement_line_id")
    @JsonProperty("statement_line_id")
    private Long statementLineId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 产品
     */
    @DEField(name = "product_id")
    @TableField(value = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Long productId;
    /**
     * 计量单位
     */
    @DEField(name = "product_uom_id")
    @TableField(value = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Long productUomId;
    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @TableField(value = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Long partnerId;
    /**
     * 科目
     */
    @DEField(name = "account_id")
    @TableField(value = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Long accountId;
    /**
     * 报告
     */
    @DEField(name = "statement_id")
    @TableField(value = "statement_id")
    @JSONField(name = "statement_id")
    @JsonProperty("statement_id")
    private Long statementId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 类型
     */
    @DEField(name = "user_type_id")
    @TableField(value = "user_type_id")
    @JSONField(name = "user_type_id")
    @JsonProperty("user_type_id")
    private Long userTypeId;
    /**
     * 费用
     */
    @DEField(name = "expense_id")
    @TableField(value = "expense_id")
    @JSONField(name = "expense_id")
    @JsonProperty("expense_id")
    private Long expenseId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_type odooUserType;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_line odooStatementLine;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement odooStatement;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_full_reconcile odooFullReconcile;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_invoice odooInvoice;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_move odooMove;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment odooPayment;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax odooTaxLine;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_expense odooExpense;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCompanyCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom;



    /**
     * 设置 [标签]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [余额]
     */
    public void setBalance(BigDecimal balance){
        this.balance = balance ;
        this.modify("balance",balance);
    }

    /**
     * 设置 [货币金额]
     */
    public void setAmountCurrency(BigDecimal amountCurrency){
        this.amountCurrency = amountCurrency ;
        this.modify("amount_currency",amountCurrency);
    }

    /**
     * 设置 [已核销]
     */
    public void setReconciled(Boolean reconciled){
        this.reconciled = reconciled ;
        this.modify("reconciled",reconciled);
    }

    /**
     * 设置 [现金余额]
     */
    public void setBalanceCashBasis(BigDecimal balanceCashBasis){
        this.balanceCashBasis = balanceCashBasis ;
        this.modify("balance_cash_basis",balanceCashBasis);
    }

    /**
     * 设置 [贷方现金基础]
     */
    public void setCreditCashBasis(BigDecimal creditCashBasis){
        this.creditCashBasis = creditCashBasis ;
        this.modify("credit_cash_basis",creditCashBasis);
    }

    /**
     * 设置 [数量]
     */
    public void setQuantity(Double quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }

    /**
     * 设置 [残值金额]
     */
    public void setAmountResidual(BigDecimal amountResidual){
        this.amountResidual = amountResidual ;
        this.modify("amount_residual",amountResidual);
    }

    /**
     * 设置 [出现在VAT报告]
     */
    public void setTaxExigible(Boolean taxExigible){
        this.taxExigible = taxExigible ;
        this.modify("tax_exigible",taxExigible);
    }

    /**
     * 设置 [外币残余金额]
     */
    public void setAmountResidualCurrency(BigDecimal amountResidualCurrency){
        this.amountResidualCurrency = amountResidualCurrency ;
        this.modify("amount_residual_currency",amountResidualCurrency);
    }

    /**
     * 设置 [基本金额]
     */
    public void setTaxBaseAmount(BigDecimal taxBaseAmount){
        this.taxBaseAmount = taxBaseAmount ;
        this.modify("tax_base_amount",taxBaseAmount);
    }

    /**
     * 设置 [无催款]
     */
    public void setBlocked(Boolean blocked){
        this.blocked = blocked ;
        this.modify("blocked",blocked);
    }

    /**
     * 设置 [到期日期]
     */
    public void setDateMaturity(Timestamp dateMaturity){
        this.dateMaturity = dateMaturity ;
        this.modify("date_maturity",dateMaturity);
    }

    /**
     * 格式化日期 [到期日期]
     */
    public String formatDateMaturity(){
        if (this.dateMaturity == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dateMaturity);
    }
    /**
     * 设置 [借方]
     */
    public void setDebit(BigDecimal debit){
        this.debit = debit ;
        this.modify("debit",debit);
    }

    /**
     * 设置 [借记现金基础]
     */
    public void setDebitCashBasis(BigDecimal debitCashBasis){
        this.debitCashBasis = debitCashBasis ;
        this.modify("debit_cash_basis",debitCashBasis);
    }

    /**
     * 设置 [贷方]
     */
    public void setCredit(BigDecimal credit){
        this.credit = credit ;
        this.modify("credit",credit);
    }

    /**
     * 设置 [日记账分录]
     */
    public void setMoveId(Long moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Long currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [发起人付款]
     */
    public void setPaymentId(Long paymentId){
        this.paymentId = paymentId ;
        this.modify("payment_id",paymentId);
    }

    /**
     * 设置 [公司货币]
     */
    public void setCompanyCurrencyId(Long companyCurrencyId){
        this.companyCurrencyId = companyCurrencyId ;
        this.modify("company_currency_id",companyCurrencyId);
    }

    /**
     * 设置 [日记账]
     */
    public void setJournalId(Long journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [分析账户]
     */
    public void setAnalyticAccountId(Long analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }

    /**
     * 设置 [发票]
     */
    public void setInvoiceId(Long invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }

    /**
     * 设置 [发起人税]
     */
    public void setTaxLineId(Long taxLineId){
        this.taxLineId = taxLineId ;
        this.modify("tax_line_id",taxLineId);
    }

    /**
     * 设置 [匹配号码]
     */
    public void setFullReconcileId(Long fullReconcileId){
        this.fullReconcileId = fullReconcileId ;
        this.modify("full_reconcile_id",fullReconcileId);
    }

    /**
     * 设置 [用该分录核销的银行核销单明细]
     */
    public void setStatementLineId(Long statementLineId){
        this.statementLineId = statementLineId ;
        this.modify("statement_line_id",statementLineId);
    }

    /**
     * 设置 [产品]
     */
    public void setProductId(Long productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [计量单位]
     */
    public void setProductUomId(Long productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Long partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [科目]
     */
    public void setAccountId(Long accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [报告]
     */
    public void setStatementId(Long statementId){
        this.statementId = statementId ;
        this.modify("statement_id",statementId);
    }

    /**
     * 设置 [类型]
     */
    public void setUserTypeId(Long userTypeId){
        this.userTypeId = userTypeId ;
        this.modify("user_type_id",userTypeId);
    }

    /**
     * 设置 [费用]
     */
    public void setExpenseId(Long expenseId){
        this.expenseId = expenseId ;
        this.modify("expense_id",expenseId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



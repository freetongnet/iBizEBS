package cn.ibizlab.businesscentral.core.odoo_fleet.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_fleet.domain.Fleet_vehicle_model;
import cn.ibizlab.businesscentral.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[fleet_vehicle_model] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-fleet:odoo-fleet}", contextId = "fleet-vehicle-model", fallback = fleet_vehicle_modelFallback.class)
public interface fleet_vehicle_modelFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_models/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_models/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models")
    Fleet_vehicle_model create(@RequestBody Fleet_vehicle_model fleet_vehicle_model);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/batch")
    Boolean createBatch(@RequestBody List<Fleet_vehicle_model> fleet_vehicle_models);


    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_models/{id}")
    Fleet_vehicle_model update(@PathVariable("id") Long id,@RequestBody Fleet_vehicle_model fleet_vehicle_model);

    @RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_models/batch")
    Boolean updateBatch(@RequestBody List<Fleet_vehicle_model> fleet_vehicle_models);


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_models/{id}")
    Fleet_vehicle_model get(@PathVariable("id") Long id);




    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/search")
    Page<Fleet_vehicle_model> search(@RequestBody Fleet_vehicle_modelSearchContext context);




    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_models/select")
    Page<Fleet_vehicle_model> select();


    @RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_models/getdraft")
    Fleet_vehicle_model getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/checkkey")
    Boolean checkKey(@RequestBody Fleet_vehicle_model fleet_vehicle_model);


    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/save")
    Boolean save(@RequestBody Fleet_vehicle_model fleet_vehicle_model);

    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/savebatch")
    Boolean saveBatch(@RequestBody List<Fleet_vehicle_model> fleet_vehicle_models);



    @RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/searchdefault")
    Page<Fleet_vehicle_model> searchDefault(@RequestBody Fleet_vehicle_modelSearchContext context);


}

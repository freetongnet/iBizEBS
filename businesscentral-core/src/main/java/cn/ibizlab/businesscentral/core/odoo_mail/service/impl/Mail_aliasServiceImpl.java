package cn.ibizlab.businesscentral.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_aliasSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_aliasService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mail.mapper.Mail_aliasMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[EMail别名] 服务对象接口实现
 */
@Slf4j
@Service("Mail_aliasServiceImpl")
public class Mail_aliasServiceImpl extends EBSServiceImpl<Mail_aliasMapper, Mail_alias> implements IMail_aliasService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_journalService accountJournalService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_crm.service.ICrm_teamService crmTeamService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_jobService hrJobService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_hr.service.IHr_recruitment_sourceService hrRecruitmentSourceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_alias_mixinService mailAliasMixinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_channelService mailChannelService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_maintenance.service.IMaintenance_equipment_categoryService maintenanceEquipmentCategoryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_project.service.IProject_projectService projectProjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mail.alias" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mail_alias et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_aliasService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mail_alias> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mail_alias et) {
        Mail_alias old = new Mail_alias() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_aliasService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMail_aliasService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mail_alias> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        accountJournalService.resetByAliasId(key);
        if(!ObjectUtils.isEmpty(crmTeamService.selectByAliasId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Crm_team]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(hrJobService.selectByAliasId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Hr_job]数据，无法删除!","","");
        hrRecruitmentSourceService.resetByAliasId(key);
        if(!ObjectUtils.isEmpty(mailAliasMixinService.selectByAliasId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mail_alias_mixin]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(mailChannelService.selectByAliasId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mail_channel]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(maintenanceEquipmentCategoryService.selectByAliasId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Maintenance_equipment_category]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(projectProjectService.selectByAliasId(key)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Project_project]数据，无法删除!","","");
        resUsersService.resetByAliasId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        accountJournalService.resetByAliasId(idList);
        if(!ObjectUtils.isEmpty(crmTeamService.selectByAliasId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Crm_team]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(hrJobService.selectByAliasId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Hr_job]数据，无法删除!","","");
        hrRecruitmentSourceService.resetByAliasId(idList);
        if(!ObjectUtils.isEmpty(mailAliasMixinService.selectByAliasId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mail_alias_mixin]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(mailChannelService.selectByAliasId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Mail_channel]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(maintenanceEquipmentCategoryService.selectByAliasId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Maintenance_equipment_category]数据，无法删除!","","");
        if(!ObjectUtils.isEmpty(projectProjectService.selectByAliasId(idList)))
            throw new BadRequestAlertException("删除数据失败，当前数据存在关系实体[Project_project]数据，无法删除!","","");
        resUsersService.resetByAliasId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mail_alias get(Long key) {
        Mail_alias et = getById(key);
        if(et==null){
            et=new Mail_alias();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mail_alias getDraft(Mail_alias et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mail_alias et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mail_alias et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mail_alias et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mail_alias> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mail_alias> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mail_alias> selectByAliasUserId(Long id) {
        return baseMapper.selectByAliasUserId(id);
    }
    @Override
    public void resetByAliasUserId(Long id) {
        this.update(new UpdateWrapper<Mail_alias>().set("alias_user_id",null).eq("alias_user_id",id));
    }

    @Override
    public void resetByAliasUserId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mail_alias>().set("alias_user_id",null).in("alias_user_id",ids));
    }

    @Override
    public void removeByAliasUserId(Long id) {
        this.remove(new QueryWrapper<Mail_alias>().eq("alias_user_id",id));
    }

	@Override
    public List<Mail_alias> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mail_alias>().eq("create_uid",id));
    }

	@Override
    public List<Mail_alias> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mail_alias>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mail_alias> searchDefault(Mail_aliasSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mail_alias> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mail_alias>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mail_alias et){
        //实体关系[DER1N_MAIL_ALIAS__RES_USERS__ALIAS_USER_ID]
        if(!ObjectUtils.isEmpty(et.getAliasUserId())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooAliasUser=et.getOdooAliasUser();
            if(ObjectUtils.isEmpty(odooAliasUser)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getAliasUserId());
                et.setOdooAliasUser(majorEntity);
                odooAliasUser=majorEntity;
            }
            et.setAliasUserIdText(odooAliasUser.getName());
        }
        //实体关系[DER1N_MAIL_ALIAS__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MAIL_ALIAS__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mail_alias> getMailAliasByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mail_alias> getMailAliasByEntities(List<Mail_alias> entities) {
        List ids =new ArrayList();
        for(Mail_alias entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




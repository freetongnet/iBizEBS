package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_module_uninstallSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Base_module_uninstall] 服务对象接口
 */
public interface IBase_module_uninstallService extends IService<Base_module_uninstall>{

    boolean create(Base_module_uninstall et) ;
    void createBatch(List<Base_module_uninstall> list) ;
    boolean update(Base_module_uninstall et) ;
    void updateBatch(List<Base_module_uninstall> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Base_module_uninstall get(Long key) ;
    Base_module_uninstall getDraft(Base_module_uninstall et) ;
    boolean checkKey(Base_module_uninstall et) ;
    boolean save(Base_module_uninstall et) ;
    void saveBatch(List<Base_module_uninstall> list) ;
    Page<Base_module_uninstall> searchDefault(Base_module_uninstallSearchContext context) ;
    List<Base_module_uninstall> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Base_module_uninstall> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Base_module_uninstall> getBaseModuleUninstallByIds(List<Long> ids) ;
    List<Base_module_uninstall> getBaseModuleUninstallByEntities(List<Base_module_uninstall> entities) ;
}



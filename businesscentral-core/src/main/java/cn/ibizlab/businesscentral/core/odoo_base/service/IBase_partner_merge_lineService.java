package cn.ibizlab.businesscentral.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Base_partner_merge_line;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Base_partner_merge_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Base_partner_merge_line] 服务对象接口
 */
public interface IBase_partner_merge_lineService extends IService<Base_partner_merge_line>{

    boolean create(Base_partner_merge_line et) ;
    void createBatch(List<Base_partner_merge_line> list) ;
    boolean update(Base_partner_merge_line et) ;
    void updateBatch(List<Base_partner_merge_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Base_partner_merge_line get(Long key) ;
    Base_partner_merge_line getDraft(Base_partner_merge_line et) ;
    boolean checkKey(Base_partner_merge_line et) ;
    boolean save(Base_partner_merge_line et) ;
    void saveBatch(List<Base_partner_merge_line> list) ;
    Page<Base_partner_merge_line> searchDefault(Base_partner_merge_lineSearchContext context) ;
    List<Base_partner_merge_line> selectByWizardId(Long id);
    void resetByWizardId(Long id);
    void resetByWizardId(Collection<Long> ids);
    void removeByWizardId(Long id);
    List<Base_partner_merge_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Base_partner_merge_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Base_partner_merge_line> getBasePartnerMergeLineByIds(List<Long> ids) ;
    List<Base_partner_merge_line> getBasePartnerMergeLineByEntities(List<Base_partner_merge_line> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_portal.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_mixinSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[portal_mixin] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-portal:odoo-portal}", contextId = "portal-mixin", fallback = portal_mixinFallback.class)
public interface portal_mixinFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins")
    Portal_mixin create(@RequestBody Portal_mixin portal_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/batch")
    Boolean createBatch(@RequestBody List<Portal_mixin> portal_mixins);



    @RequestMapping(method = RequestMethod.PUT, value = "/portal_mixins/{id}")
    Portal_mixin update(@PathVariable("id") Long id,@RequestBody Portal_mixin portal_mixin);

    @RequestMapping(method = RequestMethod.PUT, value = "/portal_mixins/batch")
    Boolean updateBatch(@RequestBody List<Portal_mixin> portal_mixins);


    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_mixins/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_mixins/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);




    @RequestMapping(method = RequestMethod.GET, value = "/portal_mixins/{id}")
    Portal_mixin get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/search")
    Page<Portal_mixin> search(@RequestBody Portal_mixinSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/portal_mixins/select")
    Page<Portal_mixin> select();


    @RequestMapping(method = RequestMethod.GET, value = "/portal_mixins/getdraft")
    Portal_mixin getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/checkkey")
    Boolean checkKey(@RequestBody Portal_mixin portal_mixin);


    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/save")
    Boolean save(@RequestBody Portal_mixin portal_mixin);

    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/savebatch")
    Boolean saveBatch(@RequestBody List<Portal_mixin> portal_mixins);



    @RequestMapping(method = RequestMethod.POST, value = "/portal_mixins/searchdefault")
    Page<Portal_mixin> searchDefault(@RequestBody Portal_mixinSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_stock.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_rules_report;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_rules_reportSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[stock_rules_report] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-stock:odoo-stock}", contextId = "stock-rules-report", fallback = stock_rules_reportFallback.class)
public interface stock_rules_reportFeignClient {


    @RequestMapping(method = RequestMethod.PUT, value = "/stock_rules_reports/{id}")
    Stock_rules_report update(@PathVariable("id") Long id,@RequestBody Stock_rules_report stock_rules_report);

    @RequestMapping(method = RequestMethod.PUT, value = "/stock_rules_reports/batch")
    Boolean updateBatch(@RequestBody List<Stock_rules_report> stock_rules_reports);



    @RequestMapping(method = RequestMethod.GET, value = "/stock_rules_reports/{id}")
    Stock_rules_report get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/search")
    Page<Stock_rules_report> search(@RequestBody Stock_rules_reportSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports")
    Stock_rules_report create(@RequestBody Stock_rules_report stock_rules_report);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/batch")
    Boolean createBatch(@RequestBody List<Stock_rules_report> stock_rules_reports);


    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules_reports/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules_reports/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/stock_rules_reports/select")
    Page<Stock_rules_report> select();


    @RequestMapping(method = RequestMethod.GET, value = "/stock_rules_reports/getdraft")
    Stock_rules_report getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/checkkey")
    Boolean checkKey(@RequestBody Stock_rules_report stock_rules_report);


    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/save")
    Boolean save(@RequestBody Stock_rules_report stock_rules_report);

    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/savebatch")
    Boolean saveBatch(@RequestBody List<Stock_rules_report> stock_rules_reports);



    @RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/searchdefault")
    Page<Stock_rules_report> searchDefault(@RequestBody Stock_rules_reportSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_hr.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_applicant;
/**
 * 关系型数据实体[Hr_applicant] 查询条件对象
 */
@Slf4j
@Data
public class Hr_applicantSearchContext extends QueryWrapperContext<Hr_applicant> {

	private String n_priority_eq;//[评价]
	public void setN_priority_eq(String n_priority_eq) {
        this.n_priority_eq = n_priority_eq;
        if(!ObjectUtils.isEmpty(this.n_priority_eq)){
            this.getSearchCond().eq("priority", n_priority_eq);
        }
    }
	private String n_kanban_state_eq;//[看板状态]
	public void setN_kanban_state_eq(String n_kanban_state_eq) {
        this.n_kanban_state_eq = n_kanban_state_eq;
        if(!ObjectUtils.isEmpty(this.n_kanban_state_eq)){
            this.getSearchCond().eq("kanban_state", n_kanban_state_eq);
        }
    }
	private String n_activity_state_eq;//[活动状态]
	public void setN_activity_state_eq(String n_activity_state_eq) {
        this.n_activity_state_eq = n_activity_state_eq;
        if(!ObjectUtils.isEmpty(this.n_activity_state_eq)){
            this.getSearchCond().eq("activity_state", n_activity_state_eq);
        }
    }
	private String n_name_like;//[主题/应用 名称]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_source_id_text_eq;//[来源]
	public void setN_source_id_text_eq(String n_source_id_text_eq) {
        this.n_source_id_text_eq = n_source_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_eq)){
            this.getSearchCond().eq("source_id_text", n_source_id_text_eq);
        }
    }
	private String n_source_id_text_like;//[来源]
	public void setN_source_id_text_like(String n_source_id_text_like) {
        this.n_source_id_text_like = n_source_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_source_id_text_like)){
            this.getSearchCond().like("source_id_text", n_source_id_text_like);
        }
    }
	private String n_campaign_id_text_eq;//[营销]
	public void setN_campaign_id_text_eq(String n_campaign_id_text_eq) {
        this.n_campaign_id_text_eq = n_campaign_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_eq)){
            this.getSearchCond().eq("campaign_id_text", n_campaign_id_text_eq);
        }
    }
	private String n_campaign_id_text_like;//[营销]
	public void setN_campaign_id_text_like(String n_campaign_id_text_like) {
        this.n_campaign_id_text_like = n_campaign_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_text_like)){
            this.getSearchCond().like("campaign_id_text", n_campaign_id_text_like);
        }
    }
	private String n_department_id_text_eq;//[部门]
	public void setN_department_id_text_eq(String n_department_id_text_eq) {
        this.n_department_id_text_eq = n_department_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_department_id_text_eq)){
            this.getSearchCond().eq("department_id_text", n_department_id_text_eq);
        }
    }
	private String n_department_id_text_like;//[部门]
	public void setN_department_id_text_like(String n_department_id_text_like) {
        this.n_department_id_text_like = n_department_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_department_id_text_like)){
            this.getSearchCond().like("department_id_text", n_department_id_text_like);
        }
    }
	private String n_last_stage_id_text_eq;//[最终阶段]
	public void setN_last_stage_id_text_eq(String n_last_stage_id_text_eq) {
        this.n_last_stage_id_text_eq = n_last_stage_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_last_stage_id_text_eq)){
            this.getSearchCond().eq("last_stage_id_text", n_last_stage_id_text_eq);
        }
    }
	private String n_last_stage_id_text_like;//[最终阶段]
	public void setN_last_stage_id_text_like(String n_last_stage_id_text_like) {
        this.n_last_stage_id_text_like = n_last_stage_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_last_stage_id_text_like)){
            this.getSearchCond().like("last_stage_id_text", n_last_stage_id_text_like);
        }
    }
	private String n_stage_id_text_eq;//[阶段]
	public void setN_stage_id_text_eq(String n_stage_id_text_eq) {
        this.n_stage_id_text_eq = n_stage_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_stage_id_text_eq)){
            this.getSearchCond().eq("stage_id_text", n_stage_id_text_eq);
        }
    }
	private String n_stage_id_text_like;//[阶段]
	public void setN_stage_id_text_like(String n_stage_id_text_like) {
        this.n_stage_id_text_like = n_stage_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_stage_id_text_like)){
            this.getSearchCond().like("stage_id_text", n_stage_id_text_like);
        }
    }
	private String n_medium_id_text_eq;//[媒体]
	public void setN_medium_id_text_eq(String n_medium_id_text_eq) {
        this.n_medium_id_text_eq = n_medium_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_eq)){
            this.getSearchCond().eq("medium_id_text", n_medium_id_text_eq);
        }
    }
	private String n_medium_id_text_like;//[媒体]
	public void setN_medium_id_text_like(String n_medium_id_text_like) {
        this.n_medium_id_text_like = n_medium_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_medium_id_text_like)){
            this.getSearchCond().like("medium_id_text", n_medium_id_text_like);
        }
    }
	private String n_user_id_text_eq;//[负责人]
	public void setN_user_id_text_eq(String n_user_id_text_eq) {
        this.n_user_id_text_eq = n_user_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_eq)){
            this.getSearchCond().eq("user_id_text", n_user_id_text_eq);
        }
    }
	private String n_user_id_text_like;//[负责人]
	public void setN_user_id_text_like(String n_user_id_text_like) {
        this.n_user_id_text_like = n_user_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_user_id_text_like)){
            this.getSearchCond().like("user_id_text", n_user_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_type_id_text_eq;//[学历]
	public void setN_type_id_text_eq(String n_type_id_text_eq) {
        this.n_type_id_text_eq = n_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_type_id_text_eq)){
            this.getSearchCond().eq("type_id_text", n_type_id_text_eq);
        }
    }
	private String n_type_id_text_like;//[学历]
	public void setN_type_id_text_like(String n_type_id_text_like) {
        this.n_type_id_text_like = n_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_type_id_text_like)){
            this.getSearchCond().like("type_id_text", n_type_id_text_like);
        }
    }
	private String n_job_id_text_eq;//[申请的职位]
	public void setN_job_id_text_eq(String n_job_id_text_eq) {
        this.n_job_id_text_eq = n_job_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_job_id_text_eq)){
            this.getSearchCond().eq("job_id_text", n_job_id_text_eq);
        }
    }
	private String n_job_id_text_like;//[申请的职位]
	public void setN_job_id_text_like(String n_job_id_text_like) {
        this.n_job_id_text_like = n_job_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_job_id_text_like)){
            this.getSearchCond().like("job_id_text", n_job_id_text_like);
        }
    }
	private String n_employee_name_eq;//[员工姓名]
	public void setN_employee_name_eq(String n_employee_name_eq) {
        this.n_employee_name_eq = n_employee_name_eq;
        if(!ObjectUtils.isEmpty(this.n_employee_name_eq)){
            this.getSearchCond().eq("employee_name", n_employee_name_eq);
        }
    }
	private String n_employee_name_like;//[员工姓名]
	public void setN_employee_name_like(String n_employee_name_like) {
        this.n_employee_name_like = n_employee_name_like;
        if(!ObjectUtils.isEmpty(this.n_employee_name_like)){
            this.getSearchCond().like("employee_name", n_employee_name_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_partner_id_text_eq;//[联系]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[联系]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private Long n_job_id_eq;//[申请的职位]
	public void setN_job_id_eq(Long n_job_id_eq) {
        this.n_job_id_eq = n_job_id_eq;
        if(!ObjectUtils.isEmpty(this.n_job_id_eq)){
            this.getSearchCond().eq("job_id", n_job_id_eq);
        }
    }
	private Long n_campaign_id_eq;//[营销]
	public void setN_campaign_id_eq(Long n_campaign_id_eq) {
        this.n_campaign_id_eq = n_campaign_id_eq;
        if(!ObjectUtils.isEmpty(this.n_campaign_id_eq)){
            this.getSearchCond().eq("campaign_id", n_campaign_id_eq);
        }
    }
	private Long n_last_stage_id_eq;//[最终阶段]
	public void setN_last_stage_id_eq(Long n_last_stage_id_eq) {
        this.n_last_stage_id_eq = n_last_stage_id_eq;
        if(!ObjectUtils.isEmpty(this.n_last_stage_id_eq)){
            this.getSearchCond().eq("last_stage_id", n_last_stage_id_eq);
        }
    }
	private Long n_department_id_eq;//[部门]
	public void setN_department_id_eq(Long n_department_id_eq) {
        this.n_department_id_eq = n_department_id_eq;
        if(!ObjectUtils.isEmpty(this.n_department_id_eq)){
            this.getSearchCond().eq("department_id", n_department_id_eq);
        }
    }
	private Long n_partner_id_eq;//[联系]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_medium_id_eq;//[媒体]
	public void setN_medium_id_eq(Long n_medium_id_eq) {
        this.n_medium_id_eq = n_medium_id_eq;
        if(!ObjectUtils.isEmpty(this.n_medium_id_eq)){
            this.getSearchCond().eq("medium_id", n_medium_id_eq);
        }
    }
	private Long n_emp_id_eq;//[员工]
	public void setN_emp_id_eq(Long n_emp_id_eq) {
        this.n_emp_id_eq = n_emp_id_eq;
        if(!ObjectUtils.isEmpty(this.n_emp_id_eq)){
            this.getSearchCond().eq("emp_id", n_emp_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_type_id_eq;//[学历]
	public void setN_type_id_eq(Long n_type_id_eq) {
        this.n_type_id_eq = n_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_type_id_eq)){
            this.getSearchCond().eq("type_id", n_type_id_eq);
        }
    }
	private Long n_source_id_eq;//[来源]
	public void setN_source_id_eq(Long n_source_id_eq) {
        this.n_source_id_eq = n_source_id_eq;
        if(!ObjectUtils.isEmpty(this.n_source_id_eq)){
            this.getSearchCond().eq("source_id", n_source_id_eq);
        }
    }
	private Long n_stage_id_eq;//[阶段]
	public void setN_stage_id_eq(Long n_stage_id_eq) {
        this.n_stage_id_eq = n_stage_id_eq;
        if(!ObjectUtils.isEmpty(this.n_stage_id_eq)){
            this.getSearchCond().eq("stage_id", n_stage_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_user_id_eq;//[负责人]
	public void setN_user_id_eq(Long n_user_id_eq) {
        this.n_user_id_eq = n_user_id_eq;
        if(!ObjectUtils.isEmpty(this.n_user_id_eq)){
            this.getSearchCond().eq("user_id", n_user_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




package cn.ibizlab.businesscentral.core.odoo_note.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_note.domain.Note_stage;
import cn.ibizlab.businesscentral.core.odoo_note.filter.Note_stageSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[note_stage] 服务对象接口
 */
@Component
public class note_stageFallback implements note_stageFeignClient{


    public Note_stage get(Long id){
            return null;
     }


    public Note_stage update(Long id, Note_stage note_stage){
            return null;
     }
    public Boolean updateBatch(List<Note_stage> note_stages){
            return false;
     }


    public Note_stage create(Note_stage note_stage){
            return null;
     }
    public Boolean createBatch(List<Note_stage> note_stages){
            return false;
     }

    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }



    public Page<Note_stage> search(Note_stageSearchContext context){
            return null;
     }


    public Page<Note_stage> select(){
            return null;
     }

    public Note_stage getDraft(){
            return null;
    }



    public Boolean checkKey(Note_stage note_stage){
            return false;
     }


    public Boolean save(Note_stage note_stage){
            return false;
     }
    public Boolean saveBatch(List<Note_stage> note_stages){
            return false;
     }

    public Page<Note_stage> searchDefault(Note_stageSearchContext context){
            return null;
     }


}

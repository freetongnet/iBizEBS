package cn.ibizlab.businesscentral.core.odoo_website.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[页]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "WEBSITE_PAGE",resultMap = "Website_pageResultMap")
public class Website_page extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 视图类型
     */
    @TableField(exist = false)
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 视图结构
     */
    @TableField(exist = false)
    @JSONField(name = "arch")
    @JsonProperty("arch")
    private String arch;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 网站元说明
     */
    @TableField(exist = false)
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;
    /**
     * 视图名称
     */
    @TableField(exist = false)
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 网站网址
     */
    @TableField(exist = false)
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;
    /**
     * 序号
     */
    @TableField(exist = false)
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private Integer priority;
    /**
     * 外部 ID
     */
    @TableField(exist = false)
    @JSONField(name = "xml_id")
    @JsonProperty("xml_id")
    private String xmlId;
    /**
     * 页面已索引
     */
    @DEField(name = "website_indexed")
    @TableField(value = "website_indexed")
    @JSONField(name = "website_indexed")
    @JsonProperty("website_indexed")
    private Boolean websiteIndexed;
    /**
     * 视图
     */
    @DEField(name = "view_id")
    @TableField(value = "view_id")
    @JSONField(name = "view_id")
    @JsonProperty("view_id")
    private Integer viewId;
    /**
     * 主题模板
     */
    @DEField(name = "theme_template_id")
    @TableField(value = "theme_template_id")
    @JSONField(name = "theme_template_id")
    @JsonProperty("theme_template_id")
    private Integer themeTemplateId;
    /**
     * 网站opengraph图像
     */
    @TableField(exist = false)
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;
    /**
     * 作为可选继承显示
     */
    @TableField(exist = false)
    @JSONField(name = "customize_show")
    @JsonProperty("customize_show")
    private Boolean customizeShow;
    /**
     * 模型
     */
    @TableField(exist = false)
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;
    /**
     * 下级字段
     */
    @TableField(exist = false)
    @JSONField(name = "field_parent")
    @JsonProperty("field_parent")
    private String fieldParent;
    /**
     * Arch Blob
     */
    @TableField(exist = false)
    @JSONField(name = "arch_db")
    @JsonProperty("arch_db")
    private String archDb;
    /**
     * 主页
     */
    @TableField(exist = false)
    @JSONField(name = "is_homepage")
    @JsonProperty("is_homepage")
    private Boolean isHomepage;
    /**
     * 在当前网站显示
     */
    @TableField(exist = false)
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private Boolean websitePublished;
    /**
     * 网站meta标题
     */
    @TableField(exist = false)
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;
    /**
     * 模型数据
     */
    @TableField(exist = false)
    @JSONField(name = "model_data_id")
    @JsonProperty("model_data_id")
    private Integer modelDataId;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 视图继承模式
     */
    @TableField(exist = false)
    @JSONField(name = "mode")
    @JsonProperty("mode")
    private String mode;
    /**
     * 相关菜单
     */
    @TableField(exist = false)
    @JSONField(name = "menu_ids")
    @JsonProperty("menu_ids")
    private String menuIds;
    /**
     * 继承于此的视图
     */
    @TableField(exist = false)
    @JSONField(name = "inherit_children_ids")
    @JsonProperty("inherit_children_ids")
    private String inheritChildrenIds;
    /**
     * 基础视图结构
     */
    @TableField(exist = false)
    @JSONField(name = "arch_base")
    @JsonProperty("arch_base")
    private String archBase;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 可见
     */
    @TableField(exist = false)
    @JSONField(name = "is_visible")
    @JsonProperty("is_visible")
    private Boolean isVisible;
    /**
     * 网站页面
     */
    @TableField(exist = false)
    @JSONField(name = "first_page_id")
    @JsonProperty("first_page_id")
    private Integer firstPageId;
    /**
     * SEO优化
     */
    @TableField(exist = false)
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private Boolean isSeoOptimized;
    /**
     * 标题颜色
     */
    @DEField(name = "header_color")
    @TableField(value = "header_color")
    @JSONField(name = "header_color")
    @JsonProperty("header_color")
    private String headerColor;
    /**
     * 已发布
     */
    @DEField(name = "is_published")
    @TableField(value = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private Boolean isPublished;
    /**
     * 页
     */
    @TableField(exist = false)
    @JSONField(name = "page_ids")
    @JsonProperty("page_ids")
    private String pageIds;
    /**
     * 网站
     */
    @DEField(name = "website_id")
    @TableField(value = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;
    /**
     * 页面 URL
     */
    @TableField(value = "url")
    @JSONField(name = "url")
    @JsonProperty("url")
    private String url;
    /**
     * 标题覆盖层
     */
    @DEField(name = "header_overlay")
    @TableField(value = "header_overlay")
    @JSONField(name = "header_overlay")
    @JsonProperty("header_overlay")
    private Boolean headerOverlay;
    /**
     * 网站meta关键词
     */
    @TableField(exist = false)
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;
    /**
     * 发布日期
     */
    @DEField(name = "date_publish")
    @TableField(value = "date_publish")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_publish" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_publish")
    private Timestamp datePublish;
    /**
     * 群组
     */
    @TableField(exist = false)
    @JSONField(name = "groups_id")
    @JsonProperty("groups_id")
    private String groupsId;
    /**
     * 键
     */
    @TableField(exist = false)
    @JSONField(name = "key")
    @JsonProperty("key")
    private String key;
    /**
     * 继承的视图
     */
    @TableField(exist = false)
    @JSONField(name = "inherit_id")
    @JsonProperty("inherit_id")
    private Integer inheritId;
    /**
     * 有效
     */
    @TableField(exist = false)
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 模型
     */
    @TableField(exist = false)
    @JSONField(name = "model_ids")
    @JsonProperty("model_ids")
    private String modelIds;
    /**
     * Arch 文件名
     */
    @TableField(exist = false)
    @JSONField(name = "arch_fs")
    @JsonProperty("arch_fs")
    private String archFs;
    /**
     * 最后更新者
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [页面已索引]
     */
    public void setWebsiteIndexed(Boolean websiteIndexed){
        this.websiteIndexed = websiteIndexed ;
        this.modify("website_indexed",websiteIndexed);
    }

    /**
     * 设置 [视图]
     */
    public void setViewId(Integer viewId){
        this.viewId = viewId ;
        this.modify("view_id",viewId);
    }

    /**
     * 设置 [主题模板]
     */
    public void setThemeTemplateId(Integer themeTemplateId){
        this.themeTemplateId = themeTemplateId ;
        this.modify("theme_template_id",themeTemplateId);
    }

    /**
     * 设置 [标题颜色]
     */
    public void setHeaderColor(String headerColor){
        this.headerColor = headerColor ;
        this.modify("header_color",headerColor);
    }

    /**
     * 设置 [已发布]
     */
    public void setIsPublished(Boolean isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [页面 URL]
     */
    public void setUrl(String url){
        this.url = url ;
        this.modify("url",url);
    }

    /**
     * 设置 [标题覆盖层]
     */
    public void setHeaderOverlay(Boolean headerOverlay){
        this.headerOverlay = headerOverlay ;
        this.modify("header_overlay",headerOverlay);
    }

    /**
     * 设置 [发布日期]
     */
    public void setDatePublish(Timestamp datePublish){
        this.datePublish = datePublish ;
        this.modify("date_publish",datePublish);
    }

    /**
     * 格式化日期 [发布日期]
     */
    public String formatDatePublish(){
        if (this.datePublish == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(datePublish);
    }

    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



package cn.ibizlab.businesscentral.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_bom_lineSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bom_lineService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_mrp.mapper.Mrp_bom_lineMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[物料清单明细行] 服务对象接口实现
 */
@Slf4j
@Service("Mrp_bom_lineServiceImpl")
public class Mrp_bom_lineServiceImpl extends EBSServiceImpl<Mrp_bom_lineMapper, Mrp_bom_line> implements IMrp_bom_lineService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_moveService stockMoveService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_bomService mrpBomService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routing_workcenterService mrpRoutingWorkcenterService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_mrp.service.IMrp_routingService mrpRoutingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_uom.service.IUom_uomService uomUomService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "mrp.bom.line" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Mrp_bom_line et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_bom_lineService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Mrp_bom_line> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Mrp_bom_line et) {
        Mrp_bom_line old = new Mrp_bom_line() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_bom_lineService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IMrp_bom_lineService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Mrp_bom_line> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        stockMoveService.resetByBomLineId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        stockMoveService.resetByBomLineId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Mrp_bom_line get(Long key) {
        Mrp_bom_line et = getById(key);
        if(et==null){
            et=new Mrp_bom_line();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Mrp_bom_line getDraft(Mrp_bom_line et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Mrp_bom_line et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Mrp_bom_line et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Mrp_bom_line et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Mrp_bom_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Mrp_bom_line> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Mrp_bom_line> selectByBomId(Long id) {
        return baseMapper.selectByBomId(id);
    }
    @Override
    public void removeByBomId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Mrp_bom_line>().in("bom_id",ids));
    }

    @Override
    public void removeByBomId(Long id) {
        this.remove(new QueryWrapper<Mrp_bom_line>().eq("bom_id",id));
    }

	@Override
    public List<Mrp_bom_line> selectByOperationId(Long id) {
        return baseMapper.selectByOperationId(id);
    }
    @Override
    public void resetByOperationId(Long id) {
        this.update(new UpdateWrapper<Mrp_bom_line>().set("operation_id",null).eq("operation_id",id));
    }

    @Override
    public void resetByOperationId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_bom_line>().set("operation_id",null).in("operation_id",ids));
    }

    @Override
    public void removeByOperationId(Long id) {
        this.remove(new QueryWrapper<Mrp_bom_line>().eq("operation_id",id));
    }

	@Override
    public List<Mrp_bom_line> selectByRoutingId(Long id) {
        return baseMapper.selectByRoutingId(id);
    }
    @Override
    public void resetByRoutingId(Long id) {
        this.update(new UpdateWrapper<Mrp_bom_line>().set("routing_id",null).eq("routing_id",id));
    }

    @Override
    public void resetByRoutingId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_bom_line>().set("routing_id",null).in("routing_id",ids));
    }

    @Override
    public void removeByRoutingId(Long id) {
        this.remove(new QueryWrapper<Mrp_bom_line>().eq("routing_id",id));
    }

	@Override
    public List<Mrp_bom_line> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Mrp_bom_line>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_bom_line>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Mrp_bom_line>().eq("product_id",id));
    }

	@Override
    public List<Mrp_bom_line> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Mrp_bom_line>().eq("create_uid",id));
    }

	@Override
    public List<Mrp_bom_line> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Mrp_bom_line>().eq("write_uid",id));
    }

	@Override
    public List<Mrp_bom_line> selectByProductUomId(Long id) {
        return baseMapper.selectByProductUomId(id);
    }
    @Override
    public void resetByProductUomId(Long id) {
        this.update(new UpdateWrapper<Mrp_bom_line>().set("product_uom_id",null).eq("product_uom_id",id));
    }

    @Override
    public void resetByProductUomId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Mrp_bom_line>().set("product_uom_id",null).in("product_uom_id",ids));
    }

    @Override
    public void removeByProductUomId(Long id) {
        this.remove(new QueryWrapper<Mrp_bom_line>().eq("product_uom_id",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Mrp_bom_line> searchDefault(Mrp_bom_lineSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Mrp_bom_line> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Mrp_bom_line>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Mrp_bom_line et){
        //实体关系[DER1N_MRP_BOM_LINE__MRP_BOM__BOM_ID]
        if(!ObjectUtils.isEmpty(et.getBomId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom odooBom=et.getOdooBom();
            if(ObjectUtils.isEmpty(odooBom)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_bom majorEntity=mrpBomService.get(et.getBomId());
                et.setOdooBom(majorEntity);
                odooBom=majorEntity;
            }
            et.setParentProductTmplId(odooBom.getProductTmplId());
        }
        //实体关系[DER1N_MRP_BOM_LINE__MRP_ROUTING_WORKCENTER__OPERATION_ID]
        if(!ObjectUtils.isEmpty(et.getOperationId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing_workcenter odooOperation=et.getOdooOperation();
            if(ObjectUtils.isEmpty(odooOperation)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing_workcenter majorEntity=mrpRoutingWorkcenterService.get(et.getOperationId());
                et.setOdooOperation(majorEntity);
                odooOperation=majorEntity;
            }
            et.setOperationIdText(odooOperation.getName());
        }
        //实体关系[DER1N_MRP_BOM_LINE__MRP_ROUTING__ROUTING_ID]
        if(!ObjectUtils.isEmpty(et.getRoutingId())){
            cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing odooRouting=et.getOdooRouting();
            if(ObjectUtils.isEmpty(odooRouting)){
                cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_routing majorEntity=mrpRoutingService.get(et.getRoutingId());
                et.setOdooRouting(majorEntity);
                odooRouting=majorEntity;
            }
            et.setRoutingIdText(odooRouting.getName());
        }
        //实体关系[DER1N_MRP_BOM_LINE__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductTmplId(odooProduct.getProductTmplId());
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_MRP_BOM_LINE__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_MRP_BOM_LINE__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
        //实体关系[DER1N_MRP_BOM_LINE__UOM_UOM__PRODUCT_UOM_ID]
        if(!ObjectUtils.isEmpty(et.getProductUomId())){
            cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom odooProductUom=et.getOdooProductUom();
            if(ObjectUtils.isEmpty(odooProductUom)){
                cn.ibizlab.businesscentral.core.odoo_uom.domain.Uom_uom majorEntity=uomUomService.get(et.getProductUomId());
                et.setOdooProductUom(majorEntity);
                odooProductUom=majorEntity;
            }
            et.setProductUomIdText(odooProductUom.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Mrp_bom_line> getMrpBomLineByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Mrp_bom_line> getMrpBomLineByEntities(List<Mrp_bom_line> entities) {
        List ids =new ArrayList();
        for(Mrp_bom_line entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




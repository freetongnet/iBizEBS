package cn.ibizlab.businesscentral.core.odoo_web_editor.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.businesscentral.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Web_editor_converter_test] 服务对象接口
 */
public interface IWeb_editor_converter_testService extends IService<Web_editor_converter_test>{

    boolean create(Web_editor_converter_test et) ;
    void createBatch(List<Web_editor_converter_test> list) ;
    boolean update(Web_editor_converter_test et) ;
    void updateBatch(List<Web_editor_converter_test> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Web_editor_converter_test get(Long key) ;
    Web_editor_converter_test getDraft(Web_editor_converter_test et) ;
    boolean checkKey(Web_editor_converter_test et) ;
    boolean save(Web_editor_converter_test et) ;
    void saveBatch(List<Web_editor_converter_test> list) ;
    Page<Web_editor_converter_test> searchDefault(Web_editor_converter_testSearchContext context) ;
    List<Web_editor_converter_test> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Web_editor_converter_test> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Web_editor_converter_test> selectByMany2one(Long id);
    void resetByMany2one(Long id);
    void resetByMany2one(Collection<Long> ids);
    void removeByMany2one(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Web_editor_converter_test> getWebEditorConverterTestByIds(List<Long> ids) ;
    List<Web_editor_converter_test> getWebEditorConverterTestByEntities(List<Web_editor_converter_test> entities) ;
}



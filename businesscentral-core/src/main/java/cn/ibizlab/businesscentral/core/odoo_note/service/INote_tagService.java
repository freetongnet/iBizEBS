package cn.ibizlab.businesscentral.core.odoo_note.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_note.domain.Note_tag;
import cn.ibizlab.businesscentral.core.odoo_note.filter.Note_tagSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Note_tag] 服务对象接口
 */
public interface INote_tagService extends IService<Note_tag>{

    boolean create(Note_tag et) ;
    void createBatch(List<Note_tag> list) ;
    boolean update(Note_tag et) ;
    void updateBatch(List<Note_tag> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Note_tag get(Long key) ;
    Note_tag getDraft(Note_tag et) ;
    boolean checkKey(Note_tag et) ;
    boolean save(Note_tag et) ;
    void saveBatch(List<Note_tag> list) ;
    Page<Note_tag> searchDefault(Note_tagSearchContext context) ;
    List<Note_tag> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Note_tag> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Note_tag> getNoteTagByIds(List<Long> ids) ;
    List<Note_tag> getNoteTagByEntities(List<Note_tag> entities) ;
}



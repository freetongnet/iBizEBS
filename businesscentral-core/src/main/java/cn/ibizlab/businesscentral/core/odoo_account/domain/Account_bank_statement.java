package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[银行对账单]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_BANK_STATEMENT",resultMap = "Account_bank_statementResultMap")
public class Account_bank_statement extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;
    /**
     * 分录明细行
     */
    @TableField(exist = false)
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;
    /**
     * 参考
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 结束余额
     */
    @DEField(name = "balance_end_real")
    @TableField(value = "balance_end_real")
    @JSONField(name = "balance_end_real")
    @JsonProperty("balance_end_real")
    private BigDecimal balanceEndReal;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * 会话
     */
    @DEField(name = "pos_session_id")
    @TableField(value = "pos_session_id")
    @JSONField(name = "pos_session_id")
    @JsonProperty("pos_session_id")
    private Integer posSessionId;
    /**
     * 计算余额
     */
    @DEField(name = "balance_end")
    @TableField(value = "balance_end")
    @JSONField(name = "balance_end")
    @JsonProperty("balance_end")
    private BigDecimal balanceEnd;
    /**
     * 交易小计
     */
    @DEField(name = "total_entry_encoding")
    @TableField(value = "total_entry_encoding")
    @JSONField(name = "total_entry_encoding")
    @JsonProperty("total_entry_encoding")
    private BigDecimal totalEntryEncoding;
    /**
     * 关闭在
     */
    @DEField(name = "date_done")
    @TableField(value = "date_done")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_done" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_done")
    private Timestamp dateDone;
    /**
     * 关注者(渠道)
     */
    @TableField(exist = false)
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 所有的明细行都已核销
     */
    @TableField(exist = false)
    @JSONField(name = "all_lines_reconciled")
    @JsonProperty("all_lines_reconciled")
    private Boolean allLinesReconciled;
    /**
     * 是关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private Boolean messageIsFollower;
    /**
     * 网站信息
     */
    @TableField(exist = false)
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;
    /**
     * 操作编号
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;
    /**
     * 对账单明细行
     */
    @TableField(exist = false)
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    private String lineIds;
    /**
     * 附件数量
     */
    @TableField(exist = false)
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;
    /**
     * 消息递送错误
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private Boolean messageHasError;
    /**
     * 未读消息
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private Boolean messageUnread;
    /**
     * 凭证明细数
     */
    @TableField(exist = false)
    @JSONField(name = "move_line_count")
    @JsonProperty("move_line_count")
    private Integer moveLineCount;
    /**
     * 状态
     */
    @TableField(value = "state")
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;
    /**
     * 币种
     */
    @TableField(exist = false)
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;
    /**
     * 日期
     */
    @TableField(value = "date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;
    /**
     * 是0
     */
    @TableField(exist = false)
    @JSONField(name = "is_difference_zero")
    @JsonProperty("is_difference_zero")
    private Boolean isDifferenceZero;
    /**
     * 未读消息计数器
     */
    @TableField(exist = false)
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @TableField(value = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;
    /**
     * 需要采取行动
     */
    @TableField(exist = false)
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private Boolean messageNeedaction;
    /**
     * 需要一个动作消息的编码
     */
    @TableField(exist = false)
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;
    /**
     * 关注者(业务伙伴)
     */
    @TableField(exist = false)
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;
    /**
     * 关注者
     */
    @TableField(exist = false)
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;
    /**
     * 差异
     */
    @TableField(value = "difference")
    @JSONField(name = "difference")
    @JsonProperty("difference")
    private BigDecimal difference;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 外部引用
     */
    @TableField(value = "reference")
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;
    /**
     * 起始余额
     */
    @DEField(name = "balance_start")
    @TableField(value = "balance_start")
    @JSONField(name = "balance_start")
    @JsonProperty("balance_start")
    private BigDecimal balanceStart;
    /**
     * 会计日期
     */
    @DEField(name = "accounting_date")
    @TableField(value = "accounting_date")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "accounting_date" , format="yyyy-MM-dd")
    @JsonProperty("accounting_date")
    private Timestamp accountingDate;
    /**
     * 负责人
     */
    @TableField(exist = false)
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 日记账
     */
    @TableField(exist = false)
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;
    /**
     * 类型
     */
    @TableField(exist = false)
    @JSONField(name = "journal_type")
    @JsonProperty("journal_type")
    private String journalType;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 默认借方科目
     */
    @TableField(exist = false)
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Long accountId;
    /**
     * 开始钱箱
     */
    @DEField(name = "cashbox_start_id")
    @TableField(value = "cashbox_start_id")
    @JSONField(name = "cashbox_start_id")
    @JsonProperty("cashbox_start_id")
    private Long cashboxStartId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @TableField(value = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Long journalId;
    /**
     * 公司
     */
    @DEField(name = "company_id")
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 关闭钱箱
     */
    @DEField(name = "cashbox_end_id")
    @TableField(value = "cashbox_end_id")
    @JSONField(name = "cashbox_end_id")
    @JsonProperty("cashbox_end_id")
    private Long cashboxEndId;
    /**
     * 负责人
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_cashbox odooCashboxEnd;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_bank_statement_cashbox odooCashboxStart;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [参考]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [结束余额]
     */
    public void setBalanceEndReal(BigDecimal balanceEndReal){
        this.balanceEndReal = balanceEndReal ;
        this.modify("balance_end_real",balanceEndReal);
    }

    /**
     * 设置 [会话]
     */
    public void setPosSessionId(Integer posSessionId){
        this.posSessionId = posSessionId ;
        this.modify("pos_session_id",posSessionId);
    }

    /**
     * 设置 [计算余额]
     */
    public void setBalanceEnd(BigDecimal balanceEnd){
        this.balanceEnd = balanceEnd ;
        this.modify("balance_end",balanceEnd);
    }

    /**
     * 设置 [交易小计]
     */
    public void setTotalEntryEncoding(BigDecimal totalEntryEncoding){
        this.totalEntryEncoding = totalEntryEncoding ;
        this.modify("total_entry_encoding",totalEntryEncoding);
    }

    /**
     * 设置 [关闭在]
     */
    public void setDateDone(Timestamp dateDone){
        this.dateDone = dateDone ;
        this.modify("date_done",dateDone);
    }

    /**
     * 格式化日期 [关闭在]
     */
    public String formatDateDone(){
        if (this.dateDone == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dateDone);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 格式化日期 [日期]
     */
    public String formatDate(){
        if (this.date == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [差异]
     */
    public void setDifference(BigDecimal difference){
        this.difference = difference ;
        this.modify("difference",difference);
    }

    /**
     * 设置 [外部引用]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [起始余额]
     */
    public void setBalanceStart(BigDecimal balanceStart){
        this.balanceStart = balanceStart ;
        this.modify("balance_start",balanceStart);
    }

    /**
     * 设置 [会计日期]
     */
    public void setAccountingDate(Timestamp accountingDate){
        this.accountingDate = accountingDate ;
        this.modify("accounting_date",accountingDate);
    }

    /**
     * 格式化日期 [会计日期]
     */
    public String formatAccountingDate(){
        if (this.accountingDate == null) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(accountingDate);
    }
    /**
     * 设置 [开始钱箱]
     */
    public void setCashboxStartId(Long cashboxStartId){
        this.cashboxStartId = cashboxStartId ;
        this.modify("cashbox_start_id",cashboxStartId);
    }

    /**
     * 设置 [日记账]
     */
    public void setJournalId(Long journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [公司]
     */
    public void setCompanyId(Long companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [关闭钱箱]
     */
    public void setCashboxEndId(Long cashboxEndId){
        this.cashboxEndId = cashboxEndId ;
        this.modify("cashbox_end_id",cashboxEndId);
    }

    /**
     * 设置 [负责人]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



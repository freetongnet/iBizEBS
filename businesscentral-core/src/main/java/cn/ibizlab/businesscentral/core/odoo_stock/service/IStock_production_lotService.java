package cn.ibizlab.businesscentral.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.businesscentral.core.odoo_stock.filter.Stock_production_lotSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Stock_production_lot] 服务对象接口
 */
public interface IStock_production_lotService extends IService<Stock_production_lot>{

    boolean create(Stock_production_lot et) ;
    void createBatch(List<Stock_production_lot> list) ;
    boolean update(Stock_production_lot et) ;
    void updateBatch(List<Stock_production_lot> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Stock_production_lot get(Long key) ;
    Stock_production_lot getDraft(Stock_production_lot et) ;
    boolean checkKey(Stock_production_lot et) ;
    boolean save(Stock_production_lot et) ;
    void saveBatch(List<Stock_production_lot> list) ;
    Page<Stock_production_lot> searchDefault(Stock_production_lotSearchContext context) ;
    List<Stock_production_lot> selectByUseNextOnWorkOrderId(Long id);
    void resetByUseNextOnWorkOrderId(Long id);
    void resetByUseNextOnWorkOrderId(Collection<Long> ids);
    void removeByUseNextOnWorkOrderId(Long id);
    List<Stock_production_lot> selectByProductId(Long id);
    void resetByProductId(Long id);
    void resetByProductId(Collection<Long> ids);
    void removeByProductId(Long id);
    List<Stock_production_lot> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Stock_production_lot> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    List<Stock_production_lot> selectByProductUomId(Long id);
    void resetByProductUomId(Long id);
    void resetByProductUomId(Collection<Long> ids);
    void removeByProductUomId(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Stock_production_lot> getStockProductionLotByIds(List<Long> ids) ;
    List<Stock_production_lot> getStockProductionLotByEntities(List<Stock_production_lot> entities) ;
}



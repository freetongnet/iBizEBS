package cn.ibizlab.businesscentral.core.odoo_mail.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;
import org.springframework.stereotype.Component;

/**
 * 实体[mail_wizard_invite] 服务对象接口
 */
@Component
public class mail_wizard_inviteFallback implements mail_wizard_inviteFeignClient{

    public Mail_wizard_invite update(Long id, Mail_wizard_invite mail_wizard_invite){
            return null;
     }
    public Boolean updateBatch(List<Mail_wizard_invite> mail_wizard_invites){
            return false;
     }


    public Page<Mail_wizard_invite> search(Mail_wizard_inviteSearchContext context){
            return null;
     }



    public Mail_wizard_invite create(Mail_wizard_invite mail_wizard_invite){
            return null;
     }
    public Boolean createBatch(List<Mail_wizard_invite> mail_wizard_invites){
            return false;
     }


    public Mail_wizard_invite get(Long id){
            return null;
     }


    public Boolean remove(Long id){
            return false;
     }
    public Boolean removeBatch(Collection<Long> idList){
            return false;
     }


    public Page<Mail_wizard_invite> select(){
            return null;
     }

    public Mail_wizard_invite getDraft(){
            return null;
    }



    public Boolean checkKey(Mail_wizard_invite mail_wizard_invite){
            return false;
     }


    public Boolean save(Mail_wizard_invite mail_wizard_invite){
            return false;
     }
    public Boolean saveBatch(List<Mail_wizard_invite> mail_wizard_invites){
            return false;
     }

    public Page<Mail_wizard_invite> searchDefault(Mail_wizard_inviteSearchContext context){
            return null;
     }


}

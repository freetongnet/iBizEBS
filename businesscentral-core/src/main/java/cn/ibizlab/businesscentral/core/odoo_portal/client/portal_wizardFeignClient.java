package cn.ibizlab.businesscentral.core.odoo_portal.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.businesscentral.core.odoo_portal.filter.Portal_wizardSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[portal_wizard] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-portal:odoo-portal}", contextId = "portal-wizard", fallback = portal_wizardFallback.class)
public interface portal_wizardFeignClient {




    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizards")
    Portal_wizard create(@RequestBody Portal_wizard portal_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/batch")
    Boolean createBatch(@RequestBody List<Portal_wizard> portal_wizards);


    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizards/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizards/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);


    @RequestMapping(method = RequestMethod.GET, value = "/portal_wizards/{id}")
    Portal_wizard get(@PathVariable("id") Long id);



    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/search")
    Page<Portal_wizard> search(@RequestBody Portal_wizardSearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/portal_wizards/{id}")
    Portal_wizard update(@PathVariable("id") Long id,@RequestBody Portal_wizard portal_wizard);

    @RequestMapping(method = RequestMethod.PUT, value = "/portal_wizards/batch")
    Boolean updateBatch(@RequestBody List<Portal_wizard> portal_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/portal_wizards/select")
    Page<Portal_wizard> select();


    @RequestMapping(method = RequestMethod.GET, value = "/portal_wizards/getdraft")
    Portal_wizard getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/checkkey")
    Boolean checkKey(@RequestBody Portal_wizard portal_wizard);


    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/save")
    Boolean save(@RequestBody Portal_wizard portal_wizard);

    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/savebatch")
    Boolean saveBatch(@RequestBody List<Portal_wizard> portal_wizards);



    @RequestMapping(method = RequestMethod.POST, value = "/portal_wizards/searchdefault")
    Page<Portal_wizard> searchDefault(@RequestBody Portal_wizardSearchContext context);


}

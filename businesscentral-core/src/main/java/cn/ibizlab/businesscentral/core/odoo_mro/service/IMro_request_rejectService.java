package cn.ibizlab.businesscentral.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.businesscentral.core.odoo_mro.filter.Mro_request_rejectSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mro_request_reject] 服务对象接口
 */
public interface IMro_request_rejectService extends IService<Mro_request_reject>{

    boolean create(Mro_request_reject et) ;
    void createBatch(List<Mro_request_reject> list) ;
    boolean update(Mro_request_reject et) ;
    void updateBatch(List<Mro_request_reject> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mro_request_reject get(Long key) ;
    Mro_request_reject getDraft(Mro_request_reject et) ;
    boolean checkKey(Mro_request_reject et) ;
    boolean save(Mro_request_reject et) ;
    void saveBatch(List<Mro_request_reject> list) ;
    Page<Mro_request_reject> searchDefault(Mro_request_rejectSearchContext context) ;
    List<Mro_request_reject> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mro_request_reject> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mro_request_reject> getMroRequestRejectByIds(List<Long> ids) ;
    List<Mro_request_reject> getMroRequestRejectByEntities(List<Mro_request_reject> entities) ;
}



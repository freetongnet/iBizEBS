package cn.ibizlab.businesscentral.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[税率模板]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "ACCOUNT_TAX_TEMPLATE",resultMap = "Account_tax_templateResultMap")
public class Account_tax_template extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @TableField(value = "sequence")
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;
    /**
     * 显示在发票上
     */
    @TableField(value = "description")
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;
    /**
     * 显示名称
     */
    @TableField(exist = false)
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;
    /**
     * 影响后续税收
     */
    @DEField(name = "include_base_amount")
    @TableField(value = "include_base_amount")
    @JSONField(name = "include_base_amount")
    @JsonProperty("include_base_amount")
    private Boolean includeBaseAmount;
    /**
     * 最后修改日
     */
    @TableField(exist = false)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;
    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 应有税金
     */
    @DEField(name = "tax_exigibility")
    @TableField(value = "tax_exigibility")
    @JSONField(name = "tax_exigibility")
    @JsonProperty("tax_exigibility")
    private String taxExigibility;
    /**
     * 税率名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 包含在价格中
     */
    @DEField(name = "price_include")
    @TableField(value = "price_include")
    @JSONField(name = "price_include")
    @JsonProperty("price_include")
    private Boolean priceInclude;
    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @TableField(value = "write_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;
    /**
     * 科目标签
     */
    @TableField(exist = false)
    @JSONField(name = "tag_ids")
    @JsonProperty("tag_ids")
    private String tagIds;
    /**
     * 有效
     */
    @TableField(value = "active")
    @JSONField(name = "active")
    @JsonProperty("active")
    private Boolean active;
    /**
     * 税率计算
     */
    @DEField(name = "amount_type")
    @TableField(value = "amount_type")
    @JSONField(name = "amount_type")
    @JsonProperty("amount_type")
    private String amountType;
    /**
     * 税范围
     */
    @DEField(name = "type_tax_use")
    @TableField(value = "type_tax_use")
    @JSONField(name = "type_tax_use")
    @JsonProperty("type_tax_use")
    private String typeTaxUse;
    /**
     * 金额
     */
    @TableField(value = "amount")
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;
    /**
     * 分析成本
     */
    @TableField(value = "analytic")
    @JSONField(name = "analytic")
    @JsonProperty("analytic")
    private Boolean analytic;
    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @TableField(value = "create_date" , fill = FieldFill.INSERT)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;
    /**
     * 子级税
     */
    @TableField(exist = false)
    @JSONField(name = "children_tax_ids")
    @JsonProperty("children_tax_ids")
    private String childrenTaxIds;
    /**
     * 税应收科目
     */
    @TableField(exist = false)
    @JSONField(name = "cash_basis_account_id_text")
    @JsonProperty("cash_basis_account_id_text")
    private String cashBasisAccountIdText;
    /**
     * 表模板
     */
    @TableField(exist = false)
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;
    /**
     * 最后更新人
     */
    @TableField(exist = false)
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;
    /**
     * 基本税应收科目
     */
    @TableField(exist = false)
    @JSONField(name = "cash_basis_base_account_id_text")
    @JsonProperty("cash_basis_base_account_id_text")
    private String cashBasisBaseAccountIdText;
    /**
     * 税组
     */
    @TableField(exist = false)
    @JSONField(name = "tax_group_id_text")
    @JsonProperty("tax_group_id_text")
    private String taxGroupIdText;
    /**
     * 税率科目
     */
    @TableField(exist = false)
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;
    /**
     * 退款的税金科目
     */
    @TableField(exist = false)
    @JSONField(name = "refund_account_id_text")
    @JsonProperty("refund_account_id_text")
    private String refundAccountIdText;
    /**
     * 创建人
     */
    @TableField(exist = false)
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;
    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @TableField(value = "write_uid")
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Long writeUid;
    /**
     * 表模板
     */
    @DEField(name = "chart_template_id")
    @TableField(value = "chart_template_id")
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Long chartTemplateId;
    /**
     * 退款的税金科目
     */
    @DEField(name = "refund_account_id")
    @TableField(value = "refund_account_id")
    @JSONField(name = "refund_account_id")
    @JsonProperty("refund_account_id")
    private Long refundAccountId;
    /**
     * 税应收科目
     */
    @DEField(name = "cash_basis_account_id")
    @TableField(value = "cash_basis_account_id")
    @JSONField(name = "cash_basis_account_id")
    @JsonProperty("cash_basis_account_id")
    private Long cashBasisAccountId;
    /**
     * 税率科目
     */
    @DEField(name = "account_id")
    @TableField(value = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Long accountId;
    /**
     * 基本税应收科目
     */
    @DEField(name = "cash_basis_base_account_id")
    @TableField(value = "cash_basis_base_account_id")
    @JSONField(name = "cash_basis_base_account_id")
    @JsonProperty("cash_basis_base_account_id")
    private Long cashBasisBaseAccountId;
    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @TableField(value = "create_uid" , fill = FieldFill.INSERT)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Long createUid;
    /**
     * 税组
     */
    @DEField(name = "tax_group_id")
    @TableField(value = "tax_group_id")
    @JSONField(name = "tax_group_id")
    @JsonProperty("tax_group_id")
    private Long taxGroupId;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooCashBasisAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooCashBasisBaseAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_account_template odooRefundAccount;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_chart_template odooChartTemplate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_tax_group odooTaxGroup;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite;



    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [显示在发票上]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [影响后续税收]
     */
    public void setIncludeBaseAmount(Boolean includeBaseAmount){
        this.includeBaseAmount = includeBaseAmount ;
        this.modify("include_base_amount",includeBaseAmount);
    }

    /**
     * 设置 [应有税金]
     */
    public void setTaxExigibility(String taxExigibility){
        this.taxExigibility = taxExigibility ;
        this.modify("tax_exigibility",taxExigibility);
    }

    /**
     * 设置 [税率名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [包含在价格中]
     */
    public void setPriceInclude(Boolean priceInclude){
        this.priceInclude = priceInclude ;
        this.modify("price_include",priceInclude);
    }

    /**
     * 设置 [有效]
     */
    public void setActive(Boolean active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [税率计算]
     */
    public void setAmountType(String amountType){
        this.amountType = amountType ;
        this.modify("amount_type",amountType);
    }

    /**
     * 设置 [税范围]
     */
    public void setTypeTaxUse(String typeTaxUse){
        this.typeTaxUse = typeTaxUse ;
        this.modify("type_tax_use",typeTaxUse);
    }

    /**
     * 设置 [金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [分析成本]
     */
    public void setAnalytic(Boolean analytic){
        this.analytic = analytic ;
        this.modify("analytic",analytic);
    }

    /**
     * 设置 [表模板]
     */
    public void setChartTemplateId(Long chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }

    /**
     * 设置 [退款的税金科目]
     */
    public void setRefundAccountId(Long refundAccountId){
        this.refundAccountId = refundAccountId ;
        this.modify("refund_account_id",refundAccountId);
    }

    /**
     * 设置 [税应收科目]
     */
    public void setCashBasisAccountId(Long cashBasisAccountId){
        this.cashBasisAccountId = cashBasisAccountId ;
        this.modify("cash_basis_account_id",cashBasisAccountId);
    }

    /**
     * 设置 [税率科目]
     */
    public void setAccountId(Long accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [基本税应收科目]
     */
    public void setCashBasisBaseAccountId(Long cashBasisBaseAccountId){
        this.cashBasisBaseAccountId = cashBasisBaseAccountId ;
        this.modify("cash_basis_base_account_id",cashBasisBaseAccountId);
    }

    /**
     * 设置 [税组]
     */
    public void setTaxGroupId(Long taxGroupId){
        this.taxGroupId = taxGroupId ;
        this.modify("tax_group_id",taxGroupId);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



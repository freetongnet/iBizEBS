package cn.ibizlab.businesscentral.core.odoo_base_import.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_base_import.domain.Base_import_mapping;
import cn.ibizlab.businesscentral.core.odoo_base_import.filter.Base_import_mappingSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[base_import_mapping] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-base-import:odoo-base-import}", contextId = "base-import-mapping", fallback = base_import_mappingFallback.class)
public interface base_import_mappingFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_mappings/{id}")
    Base_import_mapping update(@PathVariable("id") Long id,@RequestBody Base_import_mapping base_import_mapping);

    @RequestMapping(method = RequestMethod.PUT, value = "/base_import_mappings/batch")
    Boolean updateBatch(@RequestBody List<Base_import_mapping> base_import_mappings);




    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_mappings/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/base_import_mappings/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings")
    Base_import_mapping create(@RequestBody Base_import_mapping base_import_mapping);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/batch")
    Boolean createBatch(@RequestBody List<Base_import_mapping> base_import_mappings);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/search")
    Page<Base_import_mapping> search(@RequestBody Base_import_mappingSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_mappings/{id}")
    Base_import_mapping get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_mappings/select")
    Page<Base_import_mapping> select();


    @RequestMapping(method = RequestMethod.GET, value = "/base_import_mappings/getdraft")
    Base_import_mapping getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/checkkey")
    Boolean checkKey(@RequestBody Base_import_mapping base_import_mapping);


    @RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/save")
    Boolean save(@RequestBody Base_import_mapping base_import_mapping);

    @RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/savebatch")
    Boolean saveBatch(@RequestBody List<Base_import_mapping> base_import_mappings);



    @RequestMapping(method = RequestMethod.POST, value = "/base_import_mappings/searchdefault")
    Page<Base_import_mapping> searchDefault(@RequestBody Base_import_mappingSearchContext context);


}

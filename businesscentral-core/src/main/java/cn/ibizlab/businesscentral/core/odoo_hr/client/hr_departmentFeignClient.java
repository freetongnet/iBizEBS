package cn.ibizlab.businesscentral.core.odoo_hr.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_departmentSearchContext;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 实体[hr_department] 服务对象接口
 */
@FeignClient(value = "${ibiz.ref.service.odoo-hr:odoo-hr}", contextId = "hr-department", fallback = hr_departmentFallback.class)
public interface hr_departmentFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_departments/{id}")
    Boolean remove(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/hr_departments/batch}")
    Boolean removeBatch(@RequestBody Collection<Long> idList);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/search")
    Page<Hr_department> search(@RequestBody Hr_departmentSearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/hr_departments/{id}")
    Hr_department get(@PathVariable("id") Long id);


    @RequestMapping(method = RequestMethod.PUT, value = "/hr_departments/{id}")
    Hr_department update(@PathVariable("id") Long id,@RequestBody Hr_department hr_department);

    @RequestMapping(method = RequestMethod.PUT, value = "/hr_departments/batch")
    Boolean updateBatch(@RequestBody List<Hr_department> hr_departments);




    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments")
    Hr_department create(@RequestBody Hr_department hr_department);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/batch")
    Boolean createBatch(@RequestBody List<Hr_department> hr_departments);



    @RequestMapping(method = RequestMethod.GET, value = "/hr_departments/select")
    Page<Hr_department> select();


    @RequestMapping(method = RequestMethod.GET, value = "/hr_departments/getdraft")
    Hr_department getDraft();


    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/checkkey")
    Boolean checkKey(@RequestBody Hr_department hr_department);


    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/save")
    Boolean save(@RequestBody Hr_department hr_department);

    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/savebatch")
    Boolean saveBatch(@RequestBody List<Hr_department> hr_departments);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/searchdefault")
    Page<Hr_department> searchDefault(@RequestBody Hr_departmentSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/searchmaster")
    Page<Hr_department> searchMaster(@RequestBody Hr_departmentSearchContext context);



    @RequestMapping(method = RequestMethod.POST, value = "/hr_departments/searchroot")
    Page<Hr_department> searchROOT(@RequestBody Hr_departmentSearchContext context);


}

package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_contract_type;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_contract_typeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_contract_type] 服务对象接口
 */
public interface IHr_contract_typeService extends IService<Hr_contract_type>{

    boolean create(Hr_contract_type et) ;
    void createBatch(List<Hr_contract_type> list) ;
    boolean update(Hr_contract_type et) ;
    void updateBatch(List<Hr_contract_type> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_contract_type get(Long key) ;
    Hr_contract_type getDraft(Hr_contract_type et) ;
    boolean checkKey(Hr_contract_type et) ;
    boolean save(Hr_contract_type et) ;
    void saveBatch(List<Hr_contract_type> list) ;
    Page<Hr_contract_type> searchDefault(Hr_contract_typeSearchContext context) ;
    List<Hr_contract_type> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_contract_type> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_contract_type> getHrContractTypeByIds(List<Long> ids) ;
    List<Hr_contract_type> getHrContractTypeByEntities(List<Hr_contract_type> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.businesscentral.core.odoo_hr.filter.Hr_employeeSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Hr_employee] 服务对象接口
 */
public interface IHr_employeeService extends IService<Hr_employee>{

    boolean create(Hr_employee et) ;
    void createBatch(List<Hr_employee> list) ;
    boolean update(Hr_employee et) ;
    void updateBatch(List<Hr_employee> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Hr_employee get(Long key) ;
    Hr_employee getDraft(Hr_employee et) ;
    boolean checkKey(Hr_employee et) ;
    boolean save(Hr_employee et) ;
    void saveBatch(List<Hr_employee> list) ;
    Page<Hr_employee> searchDefault(Hr_employeeSearchContext context) ;
    Page<Hr_employee> searchMaster(Hr_employeeSearchContext context) ;
    List<Hr_employee> selectByLeaveManagerId(Long id);
    void removeByLeaveManagerId(Long id);
    List<Hr_employee> selectByLastAttendanceId(Long id);
    void resetByLastAttendanceId(Long id);
    void resetByLastAttendanceId(Collection<Long> ids);
    void removeByLastAttendanceId(Long id);
    List<Hr_employee> selectByDepartmentId(Long id);
    void resetByDepartmentId(Long id);
    void resetByDepartmentId(Collection<Long> ids);
    void removeByDepartmentId(Long id);
    List<Hr_employee> selectByCoachId(Long id);
    void resetByCoachId(Long id);
    void resetByCoachId(Collection<Long> ids);
    void removeByCoachId(Long id);
    List<Hr_employee> selectByParentId(Long id);
    void resetByParentId(Long id);
    void resetByParentId(Collection<Long> ids);
    void removeByParentId(Long id);
    List<Hr_employee> selectByJobId(Long id);
    void resetByJobId(Long id);
    void resetByJobId(Collection<Long> ids);
    void removeByJobId(Long id);
    List<Hr_employee> selectByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Long id);
    void resetByResourceCalendarId(Collection<Long> ids);
    void removeByResourceCalendarId(Long id);
    List<Hr_employee> selectByResourceId(Long id);
    List<Hr_employee> selectByResourceId(Collection<Long> ids);
    void removeByResourceId(Long id);
    List<Hr_employee> selectByCompanyId(Long id);
    void resetByCompanyId(Long id);
    void resetByCompanyId(Collection<Long> ids);
    void removeByCompanyId(Long id);
    List<Hr_employee> selectByCountryId(Long id);
    void resetByCountryId(Long id);
    void resetByCountryId(Collection<Long> ids);
    void removeByCountryId(Long id);
    List<Hr_employee> selectByCountryOfBirth(Long id);
    void resetByCountryOfBirth(Long id);
    void resetByCountryOfBirth(Collection<Long> ids);
    void removeByCountryOfBirth(Long id);
    List<Hr_employee> selectByBankAccountId(Long id);
    void resetByBankAccountId(Long id);
    void resetByBankAccountId(Collection<Long> ids);
    void removeByBankAccountId(Long id);
    List<Hr_employee> selectByAddressHomeId(Long id);
    void resetByAddressHomeId(Long id);
    void resetByAddressHomeId(Collection<Long> ids);
    void removeByAddressHomeId(Long id);
    List<Hr_employee> selectByAddressId(Long id);
    void resetByAddressId(Long id);
    void resetByAddressId(Collection<Long> ids);
    void removeByAddressId(Long id);
    List<Hr_employee> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Hr_employee> selectByExpenseManagerId(Long id);
    void resetByExpenseManagerId(Long id);
    void resetByExpenseManagerId(Collection<Long> ids);
    void removeByExpenseManagerId(Long id);
    List<Hr_employee> selectByUserId(Long id);
    void resetByUserId(Long id);
    void resetByUserId(Collection<Long> ids);
    void removeByUserId(Long id);
    List<Hr_employee> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Hr_employee> getHrEmployeeByIds(List<Long> ids) ;
    List<Hr_employee> getHrEmployeeByEntities(List<Hr_employee> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_gamification.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_gamification.domain.Gamification_challenge_line;
import cn.ibizlab.businesscentral.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Gamification_challenge_line] 服务对象接口
 */
public interface IGamification_challenge_lineService extends IService<Gamification_challenge_line>{

    boolean create(Gamification_challenge_line et) ;
    void createBatch(List<Gamification_challenge_line> list) ;
    boolean update(Gamification_challenge_line et) ;
    void updateBatch(List<Gamification_challenge_line> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Gamification_challenge_line get(Long key) ;
    Gamification_challenge_line getDraft(Gamification_challenge_line et) ;
    boolean checkKey(Gamification_challenge_line et) ;
    boolean save(Gamification_challenge_line et) ;
    void saveBatch(List<Gamification_challenge_line> list) ;
    Page<Gamification_challenge_line> searchDefault(Gamification_challenge_lineSearchContext context) ;
    List<Gamification_challenge_line> selectByChallengeId(Long id);
    void removeByChallengeId(Collection<Long> ids);
    void removeByChallengeId(Long id);
    List<Gamification_challenge_line> selectByDefinitionId(Long id);
    void removeByDefinitionId(Collection<Long> ids);
    void removeByDefinitionId(Long id);
    List<Gamification_challenge_line> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Gamification_challenge_line> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Gamification_challenge_line> getGamificationChallengeLineByIds(List<Long> ids) ;
    List<Gamification_challenge_line> getGamificationChallengeLineByEntities(List<Gamification_challenge_line> entities) ;
}



package cn.ibizlab.businesscentral.core.odoo_mail.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_mass_mailing_contact;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_mass_mailing_contactSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[Mail_mass_mailing_contact] 服务对象接口
 */
public interface IMail_mass_mailing_contactService extends IService<Mail_mass_mailing_contact>{

    boolean create(Mail_mass_mailing_contact et) ;
    void createBatch(List<Mail_mass_mailing_contact> list) ;
    boolean update(Mail_mass_mailing_contact et) ;
    void updateBatch(List<Mail_mass_mailing_contact> list) ;
    boolean remove(Long key) ;
    void removeBatch(Collection<Long> idList) ;
    Mail_mass_mailing_contact get(Long key) ;
    Mail_mass_mailing_contact getDraft(Mail_mass_mailing_contact et) ;
    boolean checkKey(Mail_mass_mailing_contact et) ;
    boolean save(Mail_mass_mailing_contact et) ;
    void saveBatch(List<Mail_mass_mailing_contact> list) ;
    Page<Mail_mass_mailing_contact> searchDefault(Mail_mass_mailing_contactSearchContext context) ;
    List<Mail_mass_mailing_contact> selectByCountryId(Long id);
    void resetByCountryId(Long id);
    void resetByCountryId(Collection<Long> ids);
    void removeByCountryId(Long id);
    List<Mail_mass_mailing_contact> selectByTitleId(Long id);
    void resetByTitleId(Long id);
    void resetByTitleId(Collection<Long> ids);
    void removeByTitleId(Long id);
    List<Mail_mass_mailing_contact> selectByCreateUid(Long id);
    void removeByCreateUid(Long id);
    List<Mail_mass_mailing_contact> selectByWriteUid(Long id);
    void removeByWriteUid(Long id);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<Mail_mass_mailing_contact> getMailMassMailingContactByIds(List<Long> ids) ;
    List<Mail_mass_mailing_contact> getMailMassMailingContactByEntities(List<Mail_mass_mailing_contact> entities) ;
}



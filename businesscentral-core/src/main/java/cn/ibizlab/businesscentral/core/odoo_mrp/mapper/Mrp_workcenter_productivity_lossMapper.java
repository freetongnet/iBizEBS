package cn.ibizlab.businesscentral.core.odoo_mrp.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.businesscentral.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Mrp_workcenter_productivity_lossMapper extends BaseMapper<Mrp_workcenter_productivity_loss>{

    Page<Mrp_workcenter_productivity_loss> searchDefault(IPage page, @Param("srf") Mrp_workcenter_productivity_lossSearchContext context, @Param("ew") Wrapper<Mrp_workcenter_productivity_loss> wrapper) ;
    @Override
    Mrp_workcenter_productivity_loss selectById(Serializable id);
    @Override
    int insert(Mrp_workcenter_productivity_loss entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Mrp_workcenter_productivity_loss entity);
    @Override
    int update(@Param(Constants.ENTITY) Mrp_workcenter_productivity_loss entity, @Param("ew") Wrapper<Mrp_workcenter_productivity_loss> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Mrp_workcenter_productivity_loss> selectByLossId(@Param("id") Serializable id) ;

    List<Mrp_workcenter_productivity_loss> selectByCreateUid(@Param("id") Serializable id) ;

    List<Mrp_workcenter_productivity_loss> selectByWriteUid(@Param("id") Serializable id) ;


}

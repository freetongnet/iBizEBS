package cn.ibizlab.businesscentral.core.odoo_ir.service.logic;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.businesscentral.core.odoo_ir.domain.Ir_sequence;

/**
 * 关系型数据实体[create_sequence] 对象
 */
public interface IIr_sequencecreate_sequenceLogic {

    void execute(Ir_sequence et) ;

}

package cn.ibizlab.businesscentral.core.odoo_base.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_config_settings;
import cn.ibizlab.businesscentral.core.odoo_base.filter.Res_config_settingsSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Res_config_settingsMapper extends BaseMapper<Res_config_settings>{

    Page<Res_config_settings> searchDefault(IPage page, @Param("srf") Res_config_settingsSearchContext context, @Param("ew") Wrapper<Res_config_settings> wrapper) ;
    @Override
    Res_config_settings selectById(Serializable id);
    @Override
    int insert(Res_config_settings entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Res_config_settings entity);
    @Override
    int update(@Param(Constants.ENTITY) Res_config_settings entity, @Param("ew") Wrapper<Res_config_settings> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<Res_config_settings> selectByChartTemplateId(@Param("id") Serializable id) ;

    List<Res_config_settings> selectByDigestId(@Param("id") Serializable id) ;

    List<Res_config_settings> selectByTemplateId(@Param("id") Serializable id) ;

    List<Res_config_settings> selectByDepositDefaultProductId(@Param("id") Serializable id) ;

    List<Res_config_settings> selectByCompanyId(@Param("id") Serializable id) ;

    List<Res_config_settings> selectByAuthSignupTemplateUserId(@Param("id") Serializable id) ;

    List<Res_config_settings> selectByCreateUid(@Param("id") Serializable id) ;

    List<Res_config_settings> selectByWriteUid(@Param("id") Serializable id) ;

    List<Res_config_settings> selectByDefaultSaleOrderTemplateId(@Param("id") Serializable id) ;


}

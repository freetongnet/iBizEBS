package cn.ibizlab.businesscentral.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.businesscentral.util.domain.EntityBase;
import cn.ibizlab.businesscentral.util.annotation.DEField;
import cn.ibizlab.businesscentral.util.annotation.DynaProperty;
import cn.ibizlab.businesscentral.util.annotation.BackFillProperty;
import cn.ibizlab.businesscentral.util.enums.DEPredefinedFieldType;
import cn.ibizlab.businesscentral.util.enums.DEFieldDefaultValueType;
import cn.ibizlab.businesscentral.util.helper.DataObject;
import java.io.Serializable;
import lombok.*;
import org.springframework.data.annotation.Transient;
import cn.ibizlab.businesscentral.util.annotation.Audit;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.baomidou.mybatisplus.annotation.*;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

/**
 * 实体[供应商]
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(value = "handler")
@TableName(value = "RES_PARTNER",resultMap = "Res_supplierResultMap")
public class Res_supplier extends EntityMP implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @TableId(value= "id",type=IdType.AUTO)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Long id;
    /**
     * 条码
     */
    @TableField(value = "barcode")
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;
    /**
     * 电话
     */
    @TableField(value = "phone")
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;
    /**
     * 公司类别
     */
    @TableField(exist = false)
    @JSONField(name = "company_type")
    @JsonProperty("company_type")
    private String companyType;
    /**
     * 内部参考
     */
    @TableField(value = "ref")
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;
    /**
     * EMail
     */
    @TableField(value = "email")
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;
    /**
     * 地址类型
     */
    @TableField(value = "type")
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;
    /**
     * 工作岗位
     */
    @TableField(value = "function")
    @JSONField(name = "function")
    @JsonProperty("function")
    private String function;
        
    /**
     * 标签
     */
    @TableField(exist = false)
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private String categoryId;    
    /**
     * 手机
     */
    @TableField(value = "mobile")
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;
    /**
     * 名称
     */
    @TableField(value = "name")
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;
    /**
     * 税号
     */
    @TableField(value = "vat")
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;
    /**
     * 供应商货币
     */
    @TableField(exist = false)
    @JSONField(name = "property_purchase_currency_name")
    @JsonProperty("property_purchase_currency_name")
    private String propertyPurchaseCurrencyName;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;
    /**
     * 分包商位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_subcontractor_name")
    @JsonProperty("property_stock_subcontractor_name")
    private String propertyStockSubcontractorName;
    /**
     * 客户位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_customer_name")
    @JsonProperty("property_stock_customer_name")
    private String propertyStockCustomerName;
    /**
     * 供应商位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_supplier_name")
    @JsonProperty("property_stock_supplier_name")
    private String propertyStockSupplierName;
    /**
     * 公司
     */
    @TableField(exist = false)
    @JSONField(name = "parent_name")
    @JsonProperty("parent_name")
    private String parentName;
    /**
     * 付款条款
     */
    @TableField(exist = false)
    @JSONField(name = "property_payment_term_name")
    @JsonProperty("property_payment_term_name")
    private String propertyPaymentTermName;
    /**
     * 交货方法
     */
    @TableField(exist = false)
    @JSONField(name = "property_delivery_carrier_name")
    @JsonProperty("property_delivery_carrier_name")
    private String propertyDeliveryCarrierName;
    /**
     * 价格表
     */
    @TableField(exist = false)
    @JSONField(name = "property_product_pricelist_name")
    @JsonProperty("property_product_pricelist_name")
    private String propertyProductPricelistName;
    /**
     * 国家/地区
     */
    @TableField(exist = false)
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;
    /**
     * 税科目调整
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_position_name")
    @JsonProperty("property_account_position_name")
    private String propertyAccountPositionName;
    /**
     * 州/省
     */
    @TableField(exist = false)
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    private String stateIdText;
    /**
     * 称谓
     */
    @TableField(exist = false)
    @JSONField(name = "title_text")
    @JsonProperty("title_text")
    private String titleText;
    /**
     * 采购付款条例
     */
    @TableField(exist = false)
    @JSONField(name = "property_supplier_payment_term_name")
    @JsonProperty("property_supplier_payment_term_name")
    private String propertySupplierPaymentTermName;
    /**
     * 销售员
     */
    @TableField(exist = false)
    @JSONField(name = "user_name")
    @JsonProperty("user_name")
    private String userName;
    /**
     * ID
     */
    @DEField(name = "state_id")
    @TableField(value = "state_id")
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Long stateId;
    /**
     * 供应商货币
     */
    @TableField(exist = false)
    @JSONField(name = "property_purchase_currency_id")
    @JsonProperty("property_purchase_currency_id")
    @DynaProperty(res_model="res.partner",reference=cn.ibizlab.businesscentral.core.odoo_base.service.IRes_currencyService.class,pickup_text = "property_purchase_currency_name",reference_field = "name")
    private Long propertyPurchaseCurrencyId;
    /**
     * 采购付款条例
     */
    @TableField(exist = false)
    @JSONField(name = "property_supplier_payment_term_id")
    @JsonProperty("property_supplier_payment_term_id")
    @DynaProperty(res_model="res.partner",reference=cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService.class,pickup_text = "property_supplier_payment_term_name",reference_field = "name")
    private Long propertySupplierPaymentTermId;
    /**
     * 销售付款条款
     */
    @TableField(exist = false)
    @JSONField(name = "property_payment_term_id")
    @JsonProperty("property_payment_term_id")
    @DynaProperty(res_model="res.partner",reference=cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_payment_termService.class,pickup_text = "property_payment_term_name",reference_field = "name")
    private Long propertyPaymentTermId;
    /**
     * ID
     */
    @DEField(name = "country_id")
    @TableField(value = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Long countryId;
    /**
     * 供应商位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_supplier")
    @JsonProperty("property_stock_supplier")
    @DynaProperty(res_model="res.partner",reference=cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService.class,pickup_text = "property_stock_supplier_name",reference_field = "name")
    private Long propertyStockSupplier;
    /**
     * 交货方法
     */
    @TableField(exist = false)
    @JSONField(name = "property_delivery_carrier_id")
    @JsonProperty("property_delivery_carrier_id")
    @DynaProperty(res_model="res.partner",reference=cn.ibizlab.businesscentral.core.odoo_stock.service.IDelivery_carrierService.class,pickup_text = "property_delivery_carrier_name",reference_field = "name")
    private Long propertyDeliveryCarrierId;
    /**
     * ID
     */
    @TableField(value = "title")
    @JSONField(name = "title")
    @JsonProperty("title")
    private Long title;
    /**
     * 税科目调整
     */
    @TableField(exist = false)
    @JSONField(name = "property_account_position_id")
    @JsonProperty("property_account_position_id")
    @DynaProperty(res_model="res.partner",reference=cn.ibizlab.businesscentral.core.odoo_account.service.IAccount_fiscal_positionService.class,pickup_text = "property_account_position_name",reference_field = "name")
    private Long propertyAccountPositionId;
    /**
     * ID
     */
    @DEField(name = "parent_id")
    @TableField(value = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Long parentId;
    /**
     * 客户位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_customer")
    @JsonProperty("property_stock_customer")
    @DynaProperty(res_model="res.partner",reference=cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService.class,pickup_text = "property_stock_customer_name",reference_field = "name")
    private Long propertyStockCustomer;
    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @TableField(value = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Long userId;
    /**
     * 分包商位置
     */
    @TableField(exist = false)
    @JSONField(name = "property_stock_subcontractor")
    @JsonProperty("property_stock_subcontractor")
    @DynaProperty(res_model="res.partner",reference=cn.ibizlab.businesscentral.core.odoo_stock.service.IStock_locationService.class,pickup_text = "property_stock_subcontractor_name",reference_field = "name")
    private Long propertyStockSubcontractor;
    /**
     * ID
     */
    @DEField(name = "company_id" , preType = DEPredefinedFieldType.ORGID)
    @TableField(value = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Long companyId;
    /**
     * 价格表
     */
    @TableField(exist = false)
    @JSONField(name = "property_product_pricelist")
    @JsonProperty("property_product_pricelist")
    @DynaProperty(res_model="res.partner",reference=cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_pricelistService.class,pickup_text = "property_product_pricelist_name",reference_field = "name")
    private Long propertyProductPricelist;
    /**
     * 街道
     */
    @TableField(value = "street")
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;
    /**
     * 网站网址
     */
    @TableField(exist = false)
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;
    /**
     * 城市
     */
    @TableField(value = "city")
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;
    /**
     * 街道 2
     */
    @TableField(value = "street2")
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;
    /**
     * 邮政编码
     */
    @TableField(value = "zip")
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;
    /**
     * 公司
     */
    @DEField(name = "is_company")
    @TableField(value = "is_company")
    @JSONField(name = "is_company")
    @JsonProperty("is_company")
    private Boolean isCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_fiscal_position odooAccountPosition;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooAccountPaymentTerm;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_account.domain.Account_payment_term odooPurchasePaymentTerm;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Delivery_carrier odooDeliveryCarrier;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_product.domain.Product_pricelist odooPricelist;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_country_state odooState;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner odooParent;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_partner_title odooTitle;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooCustomerLocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooSubcontractorLocation;

    /**
     * 
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @TableField(exist = false)
    private cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_location odooVendorLocation;



    /**
     * 设置 [条码]
     */
    public void setBarcode(String barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [电话]
     */
    public void setPhone(String phone){
        this.phone = phone ;
        this.modify("phone",phone);
    }

    /**
     * 设置 [内部参考]
     */
    public void setRef(String ref){
        this.ref = ref ;
        this.modify("ref",ref);
    }

    /**
     * 设置 [EMail]
     */
    public void setEmail(String email){
        this.email = email ;
        this.modify("email",email);
    }

    /**
     * 设置 [地址类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [工作岗位]
     */
    public void setFunction(String function){
        this.function = function ;
        this.modify("function",function);
    }

    /**
     * 设置 [标签]
     */
    public void setCategoryId(String categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }
    
    /**
     * 设置 [手机]
     */
    public void setMobile(String mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }

    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [税号]
     */
    public void setVat(String vat){
        this.vat = vat ;
        this.modify("vat",vat);
    }

    /**
     * 设置 [ID]
     */
    public void setStateId(Long stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }

    /**
     * 设置 [ID]
     */
    public void setCountryId(Long countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [ID]
     */
    public void setTitle(Long title){
        this.title = title ;
        this.modify("title",title);
    }

    /**
     * 设置 [ID]
     */
    public void setParentId(Long parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [销售员]
     */
    public void setUserId(Long userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [街道]
     */
    public void setStreet(String street){
        this.street = street ;
        this.modify("street",street);
    }

    /**
     * 设置 [城市]
     */
    public void setCity(String city){
        this.city = city ;
        this.modify("city",city);
    }

    /**
     * 设置 [街道 2]
     */
    public void setStreet2(String street2){
        this.street2 = street2 ;
        this.modify("street2",street2);
    }

    /**
     * 设置 [邮政编码]
     */
    public void setZip(String zip){
        this.zip = zip ;
        this.modify("zip",zip);
    }

    /**
     * 设置 [公司]
     */
    public void setIsCompany(Boolean isCompany){
        this.isCompany = isCompany ;
        this.modify("is_company",isCompany);
    }


    @Override
    public Serializable getDefaultKey(boolean gen) {
       return IdWorker.getId();
    }
    /**
     * 复制当前对象数据到目标对象(粘贴重置)
     * @param targetEntity 目标数据对象
     * @param bIncEmpty  是否包括空值
     * @param <T>
     * @return
     */
    @Override
    public <T> T copyTo(T targetEntity, boolean bIncEmpty) {
        this.reset("id");
        return super.copyTo(targetEntity,bIncEmpty);
    }
}



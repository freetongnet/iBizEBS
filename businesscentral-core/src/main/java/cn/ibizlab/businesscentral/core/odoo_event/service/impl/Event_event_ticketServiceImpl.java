package cn.ibizlab.businesscentral.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.aop.framework.AopContext;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.businesscentral.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.businesscentral.core.odoo_event.filter.Event_event_ticketSearchContext;
import cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_event_ticketService;

import cn.ibizlab.businesscentral.util.helper.CachedBeanCopier;
import cn.ibizlab.businesscentral.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.core.odoo_event.mapper.Event_event_ticketMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;

import org.springframework.util.StringUtils;

/**
 * 实体[活动入场券] 服务对象接口实现
 */
@Slf4j
@Service("Event_event_ticketServiceImpl")
public class Event_event_ticketServiceImpl extends EBSServiceImpl<Event_event_ticketMapper, Event_event_ticket> implements IEvent_event_ticketService {

    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_registrationService eventRegistrationService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_sale.service.ISale_order_lineService saleOrderLineService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_eventService eventEventService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_event.service.IEvent_typeService eventTypeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_product.service.IProduct_productService productProductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService resUsersService;

    protected int batchSize = 500;

    public String getIrModel(){
        return "event.event.ticket" ;
    }

    private boolean messageinfo = false ;

    public void setMessageInfo(boolean messageinfo){
        this.messageinfo = messageinfo ;
    }

    @Override
    @Transactional
    public boolean create(Event_event_ticket et) {
        boolean mail_create_nosubscribe = et.get("mail_create_nosubscribe") != null;
        boolean mail_create_nolog = et.get("mail_create_nolog") != null;
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et)))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_event_ticketService)AopContext.currentProxy() : this).get(et.getId()),et);

        if (messageinfo && !mail_create_nosubscribe) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_followersExService.class).add_default_followers(this,et);
        }

        if (messageinfo && !mail_create_nolog) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_messageExService.class).add_default_create_message(this,et);
        }

        if (messageinfo && !mail_notrack) {

        }
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<Event_event_ticket> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list,batchSize);
    }

    @Override
    @Transactional
    public boolean update(Event_event_ticket et) {
        Event_event_ticket old = new Event_event_ticket() ;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_event_ticketService)AopContext.currentProxy() : this).get(et.getId()), old);
        boolean mail_notrack = et.get("mail_notrack") != null;
        fillParentData(et);
         if(!update(et,(Wrapper) et.getUpdateWrapper(true).eq("id",et.getId())))
            return false;
        CachedBeanCopier.copy((AopContext.currentProxy() != null ? (IEvent_event_ticketService)AopContext.currentProxy() : this).get(et.getId()),et);
        if (messageinfo && !mail_notrack) {
            cn.ibizlab.businesscentral.util.security.SpringContextHolder.getBean(cn.ibizlab.businesscentral.core.extensions.service.Mail_tracking_valueExService.class).message_track(this,old,et);
        }
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<Event_event_ticket> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list,batchSize);
    }

    @Override
    @Transactional
    public boolean remove(Long key) {
        eventRegistrationService.resetByEventTicketId(key);
        saleOrderLineService.resetByEventTicketId(key);
        boolean result=removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<Long> idList) {
        eventRegistrationService.resetByEventTicketId(idList);
        saleOrderLineService.resetByEventTicketId(idList);
        removeByIds(idList);
    }

    @Override
    @Transactional
    public Event_event_ticket get(Long key) {
        Event_event_ticket et = getById(key);
        if(et==null){
            et=new Event_event_ticket();
            et.setId(key);
        }
        else{
        }
        return et;
    }

    @Override
    public Event_event_ticket getDraft(Event_event_ticket et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(Event_event_ticket et) {
        return (!ObjectUtils.isEmpty(et.getId()))&&(!Objects.isNull(this.getById(et.getId())));
    }
    @Override
    @Transactional
    public boolean save(Event_event_ticket et) {
        if(!saveOrUpdate(et))
            return false;
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(Event_event_ticket et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? this.update(et) : this.create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<Event_event_ticket> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<Event_event_ticket> list) {
        list.forEach(item->fillParentData(item));
        saveOrUpdateBatch(list,batchSize);
    }


	@Override
    public List<Event_event_ticket> selectByEventId(Long id) {
        return baseMapper.selectByEventId(id);
    }
    @Override
    public void removeByEventId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Event_event_ticket>().in("event_id",ids));
    }

    @Override
    public void removeByEventId(Long id) {
        this.remove(new QueryWrapper<Event_event_ticket>().eq("event_id",id));
    }

	@Override
    public List<Event_event_ticket> selectByEventTypeId(Long id) {
        return baseMapper.selectByEventTypeId(id);
    }
    @Override
    public void removeByEventTypeId(Collection<Long> ids) {
        this.remove(new QueryWrapper<Event_event_ticket>().in("event_type_id",ids));
    }

    @Override
    public void removeByEventTypeId(Long id) {
        this.remove(new QueryWrapper<Event_event_ticket>().eq("event_type_id",id));
    }

	@Override
    public List<Event_event_ticket> selectByProductId(Long id) {
        return baseMapper.selectByProductId(id);
    }
    @Override
    public void resetByProductId(Long id) {
        this.update(new UpdateWrapper<Event_event_ticket>().set("product_id",null).eq("product_id",id));
    }

    @Override
    public void resetByProductId(Collection<Long> ids) {
        this.update(new UpdateWrapper<Event_event_ticket>().set("product_id",null).in("product_id",ids));
    }

    @Override
    public void removeByProductId(Long id) {
        this.remove(new QueryWrapper<Event_event_ticket>().eq("product_id",id));
    }

	@Override
    public List<Event_event_ticket> selectByCreateUid(Long id) {
        return baseMapper.selectByCreateUid(id);
    }
    @Override
    public void removeByCreateUid(Long id) {
        this.remove(new QueryWrapper<Event_event_ticket>().eq("create_uid",id));
    }

	@Override
    public List<Event_event_ticket> selectByWriteUid(Long id) {
        return baseMapper.selectByWriteUid(id);
    }
    @Override
    public void removeByWriteUid(Long id) {
        this.remove(new QueryWrapper<Event_event_ticket>().eq("write_uid",id));
    }


    /**
     * 查询集合 数据集
     */
    @Override
    public Page<Event_event_ticket> searchDefault(Event_event_ticketSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Event_event_ticket> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Event_event_ticket>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(Event_event_ticket et){
        //实体关系[DER1N_EVENT_EVENT_TICKET__EVENT_EVENT__EVENT_ID]
        if(!ObjectUtils.isEmpty(et.getEventId())){
            cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event odooEvent=et.getOdooEvent();
            if(ObjectUtils.isEmpty(odooEvent)){
                cn.ibizlab.businesscentral.core.odoo_event.domain.Event_event majorEntity=eventEventService.get(et.getEventId());
                et.setOdooEvent(majorEntity);
                odooEvent=majorEntity;
            }
            et.setEventIdText(odooEvent.getName());
        }
        //实体关系[DER1N_EVENT_EVENT_TICKET__EVENT_TYPE__EVENT_TYPE_ID]
        if(!ObjectUtils.isEmpty(et.getEventTypeId())){
            cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type odooEventType=et.getOdooEventType();
            if(ObjectUtils.isEmpty(odooEventType)){
                cn.ibizlab.businesscentral.core.odoo_event.domain.Event_type majorEntity=eventTypeService.get(et.getEventTypeId());
                et.setOdooEventType(majorEntity);
                odooEventType=majorEntity;
            }
            et.setEventTypeIdText(odooEventType.getName());
        }
        //实体关系[DER1N_EVENT_EVENT_TICKET__PRODUCT_PRODUCT__PRODUCT_ID]
        if(!ObjectUtils.isEmpty(et.getProductId())){
            cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product odooProduct=et.getOdooProduct();
            if(ObjectUtils.isEmpty(odooProduct)){
                cn.ibizlab.businesscentral.core.odoo_product.domain.Product_product majorEntity=productProductService.get(et.getProductId());
                et.setOdooProduct(majorEntity);
                odooProduct=majorEntity;
            }
            et.setProductIdText(odooProduct.getName());
        }
        //实体关系[DER1N_EVENT_EVENT_TICKET__RES_USERS__CREATE_UID]
        if(!ObjectUtils.isEmpty(et.getCreateUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooCreate=et.getOdooCreate();
            if(ObjectUtils.isEmpty(odooCreate)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getCreateUid());
                et.setOdooCreate(majorEntity);
                odooCreate=majorEntity;
            }
            et.setCreateUidText(odooCreate.getName());
        }
        //实体关系[DER1N_EVENT_EVENT_TICKET__RES_USERS__WRITE_UID]
        if(!ObjectUtils.isEmpty(et.getWriteUid())){
            cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users odooWrite=et.getOdooWrite();
            if(ObjectUtils.isEmpty(odooWrite)){
                cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users majorEntity=resUsersService.get(et.getWriteUid());
                et.setOdooWrite(majorEntity);
                odooWrite=majorEntity;
            }
            et.setWriteUidText(odooWrite.getName());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<Event_event_ticket> getEventEventTicketByIds(List<Long> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<Event_event_ticket> getEventEventTicketByEntities(List<Event_event_ticket> entities) {
        List ids =new ArrayList();
        for(Event_event_ticket entity : entities){
            Serializable id=entity.getId();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0)
           return this.listByIds(ids);
        else
           return entities;
    }



}




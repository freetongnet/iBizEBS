package cn.ibizlab.businesscentral.core.extensions.service;

import cn.ibizlab.businesscentral.core.odoo_base.domain.Res_users;
import cn.ibizlab.businesscentral.core.odoo_base.service.IRes_usersService;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_followers_mail_message_subtype_rel;
import cn.ibizlab.businesscentral.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.businesscentral.core.odoo_mail.filter.Mail_message_subtypeSearchContext;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_followers_mail_message_subtype_relService;
import cn.ibizlab.businesscentral.core.odoo_mail.service.IMail_message_subtypeService;
import cn.ibizlab.businesscentral.core.odoo_mail.service.impl.Mail_followersServiceImpl;
import cn.ibizlab.businesscentral.core.util.helper.EBSServiceImpl;
import cn.ibizlab.businesscentral.util.domain.EntityMP;
import cn.ibizlab.businesscentral.util.security.AuthenticationUser;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Primary
@Service("Mail_followersExService")
public class Mail_followersExService extends Mail_followersServiceImpl {

    @Autowired
    IMail_followers_mail_message_subtype_relService mail_followers_mail_message_subtype_relService;

    @Autowired
    IRes_usersService res_usersService;

    @Autowired
    IMail_message_subtypeService mail_message_subtypeService;

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    @Override
    public boolean create(Mail_followers et) {
        Mail_followers exists = new Mail_followers();
        exists.setResModel(et.getResModel());
        exists.setResId(et.getResId());
        if (et.getPartnerId() != null)
            exists.setPartnerId(et.getPartnerId());
        if (et.getChannelId() != null)
            exists.setChannelId(et.getChannelId());
        if (this.getBaseMapper().selectCount(new QueryWrapper<Mail_followers>().setEntity(exists)) > 0)
            return true;
        return super.create(et);
    }

    public <T extends EntityMP> void add_default_followers(EBSServiceImpl service , T et) {
        if (et.get("id") == null) {
            return;
        }
        if (et.get("mail_create_nosubscribe") != null) {
            return;
        }


        AuthenticationUser user = AuthenticationUser.getAuthenticationUser();
        Res_users res_users = res_usersService.get(Long.parseLong(user.getUserid()));
        Mail_followers follower = new Mail_followers();
        follower.setResModel(service.getIrModel());
        follower.setResId(Integer.parseInt(et.get("id").toString()));
        follower.setPartnerId(res_users.getPartnerId());
        this.create(follower);

        //关注消息类型
        Mail_message_subtypeSearchContext ctx = new Mail_message_subtypeSearchContext();
        ctx.set("res_model", service.getIrModel());
        List<Mail_message_subtype> message_subtypes = mail_message_subtypeService.searchDefaultEx(ctx).getContent();
        for (Mail_message_subtype message_subtype : message_subtypes) {
            Mail_followers_mail_message_subtype_rel rel = new Mail_followers_mail_message_subtype_rel();
            rel.setMailFollowersId(follower.getId());
            rel.setMailMessageSubtypeId(message_subtype.getId());
            mail_followers_mail_message_subtype_relService.create(rel);
        }
    }


    public <T> void add_followers(T et) {

    }

    public void add_followers(String model, String res_id, String partner_id, String partner_subtype, Long channel_id, String channel_subtype, boolean check_exists, String existing_policy) {

    }


}

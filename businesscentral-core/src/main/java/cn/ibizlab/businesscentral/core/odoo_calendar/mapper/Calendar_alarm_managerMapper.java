package cn.ibizlab.businesscentral.core.odoo_calendar.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.HashMap;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.businesscentral.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.businesscentral.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface Calendar_alarm_managerMapper extends BaseMapper<Calendar_alarm_manager>{

    Page<Calendar_alarm_manager> searchDefault(IPage page, @Param("srf") Calendar_alarm_managerSearchContext context, @Param("ew") Wrapper<Calendar_alarm_manager> wrapper) ;
    @Override
    Calendar_alarm_manager selectById(Serializable id);
    @Override
    int insert(Calendar_alarm_manager entity);
    @Override
    int updateById(@Param(Constants.ENTITY) Calendar_alarm_manager entity);
    @Override
    int update(@Param(Constants.ENTITY) Calendar_alarm_manager entity, @Param("ew") Wrapper<Calendar_alarm_manager> updateWrapper);
    @Override
    int deleteById(Serializable id);
     /**
      * 自定义查询SQL
      * @param sql
      * @return
      */
     @Select("${sql}")
     List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);


}

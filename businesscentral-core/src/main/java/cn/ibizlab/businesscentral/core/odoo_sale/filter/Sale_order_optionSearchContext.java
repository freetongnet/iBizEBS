package cn.ibizlab.businesscentral.core.odoo_sale.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_sale.domain.Sale_order_option;
/**
 * 关系型数据实体[Sale_order_option] 查询条件对象
 */
@Slf4j
@Data
public class Sale_order_optionSearchContext extends QueryWrapperContext<Sale_order_option> {

	private String n_name_like;//[说明]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新人]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新人]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private String n_order_id_text_eq;//[销售订单参考]
	public void setN_order_id_text_eq(String n_order_id_text_eq) {
        this.n_order_id_text_eq = n_order_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_order_id_text_eq)){
            this.getSearchCond().eq("order_id_text", n_order_id_text_eq);
        }
    }
	private String n_order_id_text_like;//[销售订单参考]
	public void setN_order_id_text_like(String n_order_id_text_like) {
        this.n_order_id_text_like = n_order_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_order_id_text_like)){
            this.getSearchCond().like("order_id_text", n_order_id_text_like);
        }
    }
	private String n_line_id_text_eq;//[行]
	public void setN_line_id_text_eq(String n_line_id_text_eq) {
        this.n_line_id_text_eq = n_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_line_id_text_eq)){
            this.getSearchCond().eq("line_id_text", n_line_id_text_eq);
        }
    }
	private String n_line_id_text_like;//[行]
	public void setN_line_id_text_like(String n_line_id_text_like) {
        this.n_line_id_text_like = n_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_line_id_text_like)){
            this.getSearchCond().like("line_id_text", n_line_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_uom_id_text_eq;//[计量单位]
	public void setN_uom_id_text_eq(String n_uom_id_text_eq) {
        this.n_uom_id_text_eq = n_uom_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_uom_id_text_eq)){
            this.getSearchCond().eq("uom_id_text", n_uom_id_text_eq);
        }
    }
	private String n_uom_id_text_like;//[计量单位]
	public void setN_uom_id_text_like(String n_uom_id_text_like) {
        this.n_uom_id_text_like = n_uom_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_uom_id_text_like)){
            this.getSearchCond().like("uom_id_text", n_uom_id_text_like);
        }
    }
	private Long n_uom_id_eq;//[计量单位]
	public void setN_uom_id_eq(Long n_uom_id_eq) {
        this.n_uom_id_eq = n_uom_id_eq;
        if(!ObjectUtils.isEmpty(this.n_uom_id_eq)){
            this.getSearchCond().eq("uom_id", n_uom_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新人]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }
	private Long n_order_id_eq;//[销售订单参考]
	public void setN_order_id_eq(Long n_order_id_eq) {
        this.n_order_id_eq = n_order_id_eq;
        if(!ObjectUtils.isEmpty(this.n_order_id_eq)){
            this.getSearchCond().eq("order_id", n_order_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_line_id_eq;//[行]
	public void setN_line_id_eq(Long n_line_id_eq) {
        this.n_line_id_eq = n_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_line_id_eq)){
            this.getSearchCond().eq("line_id", n_line_id_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




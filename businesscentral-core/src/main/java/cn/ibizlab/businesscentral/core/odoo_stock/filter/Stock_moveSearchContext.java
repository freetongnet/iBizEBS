package cn.ibizlab.businesscentral.core.odoo_stock.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.businesscentral.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.businesscentral.core.odoo_stock.domain.Stock_move;
/**
 * 关系型数据实体[Stock_move] 查询条件对象
 */
@Slf4j
@Data
public class Stock_moveSearchContext extends QueryWrapperContext<Stock_move> {

	private String n_priority_eq;//[优先级]
	public void setN_priority_eq(String n_priority_eq) {
        this.n_priority_eq = n_priority_eq;
        if(!ObjectUtils.isEmpty(this.n_priority_eq)){
            this.getSearchCond().eq("priority", n_priority_eq);
        }
    }
	private String n_state_eq;//[状态]
	public void setN_state_eq(String n_state_eq) {
        this.n_state_eq = n_state_eq;
        if(!ObjectUtils.isEmpty(this.n_state_eq)){
            this.getSearchCond().eq("state", n_state_eq);
        }
    }
	private String n_name_like;//[说明]
	public void setN_name_like(String n_name_like) {
        this.n_name_like = n_name_like;
        if(!ObjectUtils.isEmpty(this.n_name_like)){
            this.getSearchCond().like("name", n_name_like);
        }
    }
	private String n_procure_method_eq;//[供应方法]
	public void setN_procure_method_eq(String n_procure_method_eq) {
        this.n_procure_method_eq = n_procure_method_eq;
        if(!ObjectUtils.isEmpty(this.n_procure_method_eq)){
            this.getSearchCond().eq("procure_method", n_procure_method_eq);
        }
    }
	private String n_unbuild_id_text_eq;//[拆卸顺序]
	public void setN_unbuild_id_text_eq(String n_unbuild_id_text_eq) {
        this.n_unbuild_id_text_eq = n_unbuild_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_unbuild_id_text_eq)){
            this.getSearchCond().eq("unbuild_id_text", n_unbuild_id_text_eq);
        }
    }
	private String n_unbuild_id_text_like;//[拆卸顺序]
	public void setN_unbuild_id_text_like(String n_unbuild_id_text_like) {
        this.n_unbuild_id_text_like = n_unbuild_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_unbuild_id_text_like)){
            this.getSearchCond().like("unbuild_id_text", n_unbuild_id_text_like);
        }
    }
	private String n_company_id_text_eq;//[公司]
	public void setN_company_id_text_eq(String n_company_id_text_eq) {
        this.n_company_id_text_eq = n_company_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_eq)){
            this.getSearchCond().eq("company_id_text", n_company_id_text_eq);
        }
    }
	private String n_company_id_text_like;//[公司]
	public void setN_company_id_text_like(String n_company_id_text_like) {
        this.n_company_id_text_like = n_company_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_company_id_text_like)){
            this.getSearchCond().like("company_id_text", n_company_id_text_like);
        }
    }
	private String n_production_id_text_eq;//[成品的生产订单]
	public void setN_production_id_text_eq(String n_production_id_text_eq) {
        this.n_production_id_text_eq = n_production_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_production_id_text_eq)){
            this.getSearchCond().eq("production_id_text", n_production_id_text_eq);
        }
    }
	private String n_production_id_text_like;//[成品的生产订单]
	public void setN_production_id_text_like(String n_production_id_text_like) {
        this.n_production_id_text_like = n_production_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_production_id_text_like)){
            this.getSearchCond().like("production_id_text", n_production_id_text_like);
        }
    }
	private String n_warehouse_id_text_eq;//[仓库]
	public void setN_warehouse_id_text_eq(String n_warehouse_id_text_eq) {
        this.n_warehouse_id_text_eq = n_warehouse_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_text_eq)){
            this.getSearchCond().eq("warehouse_id_text", n_warehouse_id_text_eq);
        }
    }
	private String n_warehouse_id_text_like;//[仓库]
	public void setN_warehouse_id_text_like(String n_warehouse_id_text_like) {
        this.n_warehouse_id_text_like = n_warehouse_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_text_like)){
            this.getSearchCond().like("warehouse_id_text", n_warehouse_id_text_like);
        }
    }
	private String n_created_production_id_text_eq;//[创建生产订单]
	public void setN_created_production_id_text_eq(String n_created_production_id_text_eq) {
        this.n_created_production_id_text_eq = n_created_production_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_created_production_id_text_eq)){
            this.getSearchCond().eq("created_production_id_text", n_created_production_id_text_eq);
        }
    }
	private String n_created_production_id_text_like;//[创建生产订单]
	public void setN_created_production_id_text_like(String n_created_production_id_text_like) {
        this.n_created_production_id_text_like = n_created_production_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_created_production_id_text_like)){
            this.getSearchCond().like("created_production_id_text", n_created_production_id_text_like);
        }
    }
	private String n_created_purchase_line_id_text_eq;//[创建采购订单行]
	public void setN_created_purchase_line_id_text_eq(String n_created_purchase_line_id_text_eq) {
        this.n_created_purchase_line_id_text_eq = n_created_purchase_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_created_purchase_line_id_text_eq)){
            this.getSearchCond().eq("created_purchase_line_id_text", n_created_purchase_line_id_text_eq);
        }
    }
	private String n_created_purchase_line_id_text_like;//[创建采购订单行]
	public void setN_created_purchase_line_id_text_like(String n_created_purchase_line_id_text_like) {
        this.n_created_purchase_line_id_text_like = n_created_purchase_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_created_purchase_line_id_text_like)){
            this.getSearchCond().like("created_purchase_line_id_text", n_created_purchase_line_id_text_like);
        }
    }
	private String n_location_dest_id_text_eq;//[目的位置]
	public void setN_location_dest_id_text_eq(String n_location_dest_id_text_eq) {
        this.n_location_dest_id_text_eq = n_location_dest_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_text_eq)){
            this.getSearchCond().eq("location_dest_id_text", n_location_dest_id_text_eq);
        }
    }
	private String n_location_dest_id_text_like;//[目的位置]
	public void setN_location_dest_id_text_like(String n_location_dest_id_text_like) {
        this.n_location_dest_id_text_like = n_location_dest_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_text_like)){
            this.getSearchCond().like("location_dest_id_text", n_location_dest_id_text_like);
        }
    }
	private String n_picking_type_id_text_eq;//[作业类型]
	public void setN_picking_type_id_text_eq(String n_picking_type_id_text_eq) {
        this.n_picking_type_id_text_eq = n_picking_type_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_eq)){
            this.getSearchCond().eq("picking_type_id_text", n_picking_type_id_text_eq);
        }
    }
	private String n_picking_type_id_text_like;//[作业类型]
	public void setN_picking_type_id_text_like(String n_picking_type_id_text_like) {
        this.n_picking_type_id_text_like = n_picking_type_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_text_like)){
            this.getSearchCond().like("picking_type_id_text", n_picking_type_id_text_like);
        }
    }
	private String n_sale_line_id_text_eq;//[销售明细行]
	public void setN_sale_line_id_text_eq(String n_sale_line_id_text_eq) {
        this.n_sale_line_id_text_eq = n_sale_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_line_id_text_eq)){
            this.getSearchCond().eq("sale_line_id_text", n_sale_line_id_text_eq);
        }
    }
	private String n_sale_line_id_text_like;//[销售明细行]
	public void setN_sale_line_id_text_like(String n_sale_line_id_text_like) {
        this.n_sale_line_id_text_like = n_sale_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_sale_line_id_text_like)){
            this.getSearchCond().like("sale_line_id_text", n_sale_line_id_text_like);
        }
    }
	private String n_partner_id_text_eq;//[目的地地址]
	public void setN_partner_id_text_eq(String n_partner_id_text_eq) {
        this.n_partner_id_text_eq = n_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_eq)){
            this.getSearchCond().eq("partner_id_text", n_partner_id_text_eq);
        }
    }
	private String n_partner_id_text_like;//[目的地地址]
	public void setN_partner_id_text_like(String n_partner_id_text_like) {
        this.n_partner_id_text_like = n_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_partner_id_text_like)){
            this.getSearchCond().like("partner_id_text", n_partner_id_text_like);
        }
    }
	private String n_raw_material_production_id_text_eq;//[原材料的生产订单]
	public void setN_raw_material_production_id_text_eq(String n_raw_material_production_id_text_eq) {
        this.n_raw_material_production_id_text_eq = n_raw_material_production_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_raw_material_production_id_text_eq)){
            this.getSearchCond().eq("raw_material_production_id_text", n_raw_material_production_id_text_eq);
        }
    }
	private String n_raw_material_production_id_text_like;//[原材料的生产订单]
	public void setN_raw_material_production_id_text_like(String n_raw_material_production_id_text_like) {
        this.n_raw_material_production_id_text_like = n_raw_material_production_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_raw_material_production_id_text_like)){
            this.getSearchCond().like("raw_material_production_id_text", n_raw_material_production_id_text_like);
        }
    }
	private String n_origin_returned_move_id_text_eq;//[原始退回移动]
	public void setN_origin_returned_move_id_text_eq(String n_origin_returned_move_id_text_eq) {
        this.n_origin_returned_move_id_text_eq = n_origin_returned_move_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_origin_returned_move_id_text_eq)){
            this.getSearchCond().eq("origin_returned_move_id_text", n_origin_returned_move_id_text_eq);
        }
    }
	private String n_origin_returned_move_id_text_like;//[原始退回移动]
	public void setN_origin_returned_move_id_text_like(String n_origin_returned_move_id_text_like) {
        this.n_origin_returned_move_id_text_like = n_origin_returned_move_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_origin_returned_move_id_text_like)){
            this.getSearchCond().like("origin_returned_move_id_text", n_origin_returned_move_id_text_like);
        }
    }
	private String n_repair_id_text_eq;//[维修]
	public void setN_repair_id_text_eq(String n_repair_id_text_eq) {
        this.n_repair_id_text_eq = n_repair_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_repair_id_text_eq)){
            this.getSearchCond().eq("repair_id_text", n_repair_id_text_eq);
        }
    }
	private String n_repair_id_text_like;//[维修]
	public void setN_repair_id_text_like(String n_repair_id_text_like) {
        this.n_repair_id_text_like = n_repair_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_repair_id_text_like)){
            this.getSearchCond().like("repair_id_text", n_repair_id_text_like);
        }
    }
	private String n_purchase_line_id_text_eq;//[采购订单行]
	public void setN_purchase_line_id_text_eq(String n_purchase_line_id_text_eq) {
        this.n_purchase_line_id_text_eq = n_purchase_line_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_line_id_text_eq)){
            this.getSearchCond().eq("purchase_line_id_text", n_purchase_line_id_text_eq);
        }
    }
	private String n_purchase_line_id_text_like;//[采购订单行]
	public void setN_purchase_line_id_text_like(String n_purchase_line_id_text_like) {
        this.n_purchase_line_id_text_like = n_purchase_line_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_purchase_line_id_text_like)){
            this.getSearchCond().like("purchase_line_id_text", n_purchase_line_id_text_like);
        }
    }
	private String n_picking_id_text_eq;//[调拨参照]
	public void setN_picking_id_text_eq(String n_picking_id_text_eq) {
        this.n_picking_id_text_eq = n_picking_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_id_text_eq)){
            this.getSearchCond().eq("picking_id_text", n_picking_id_text_eq);
        }
    }
	private String n_picking_id_text_like;//[调拨参照]
	public void setN_picking_id_text_like(String n_picking_id_text_like) {
        this.n_picking_id_text_like = n_picking_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_picking_id_text_like)){
            this.getSearchCond().like("picking_id_text", n_picking_id_text_like);
        }
    }
	private String n_product_id_text_eq;//[产品]
	public void setN_product_id_text_eq(String n_product_id_text_eq) {
        this.n_product_id_text_eq = n_product_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_eq)){
            this.getSearchCond().eq("product_id_text", n_product_id_text_eq);
        }
    }
	private String n_product_id_text_like;//[产品]
	public void setN_product_id_text_like(String n_product_id_text_like) {
        this.n_product_id_text_like = n_product_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_id_text_like)){
            this.getSearchCond().like("product_id_text", n_product_id_text_like);
        }
    }
	private String n_location_id_text_eq;//[源位置]
	public void setN_location_id_text_eq(String n_location_id_text_eq) {
        this.n_location_id_text_eq = n_location_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_eq)){
            this.getSearchCond().eq("location_id_text", n_location_id_text_eq);
        }
    }
	private String n_location_id_text_like;//[源位置]
	public void setN_location_id_text_like(String n_location_id_text_like) {
        this.n_location_id_text_like = n_location_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_location_id_text_like)){
            this.getSearchCond().like("location_id_text", n_location_id_text_like);
        }
    }
	private String n_create_uid_text_eq;//[创建人]
	public void setN_create_uid_text_eq(String n_create_uid_text_eq) {
        this.n_create_uid_text_eq = n_create_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_eq)){
            this.getSearchCond().eq("create_uid_text", n_create_uid_text_eq);
        }
    }
	private String n_create_uid_text_like;//[创建人]
	public void setN_create_uid_text_like(String n_create_uid_text_like) {
        this.n_create_uid_text_like = n_create_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_create_uid_text_like)){
            this.getSearchCond().like("create_uid_text", n_create_uid_text_like);
        }
    }
	private String n_product_uom_text_eq;//[单位]
	public void setN_product_uom_text_eq(String n_product_uom_text_eq) {
        this.n_product_uom_text_eq = n_product_uom_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_eq)){
            this.getSearchCond().eq("product_uom_text", n_product_uom_text_eq);
        }
    }
	private String n_product_uom_text_like;//[单位]
	public void setN_product_uom_text_like(String n_product_uom_text_like) {
        this.n_product_uom_text_like = n_product_uom_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_uom_text_like)){
            this.getSearchCond().like("product_uom_text", n_product_uom_text_like);
        }
    }
	private String n_operation_id_text_eq;//[待加工的作业]
	public void setN_operation_id_text_eq(String n_operation_id_text_eq) {
        this.n_operation_id_text_eq = n_operation_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_operation_id_text_eq)){
            this.getSearchCond().eq("operation_id_text", n_operation_id_text_eq);
        }
    }
	private String n_operation_id_text_like;//[待加工的作业]
	public void setN_operation_id_text_like(String n_operation_id_text_like) {
        this.n_operation_id_text_like = n_operation_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_operation_id_text_like)){
            this.getSearchCond().like("operation_id_text", n_operation_id_text_like);
        }
    }
	private String n_workorder_id_text_eq;//[待消耗的工单]
	public void setN_workorder_id_text_eq(String n_workorder_id_text_eq) {
        this.n_workorder_id_text_eq = n_workorder_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_workorder_id_text_eq)){
            this.getSearchCond().eq("workorder_id_text", n_workorder_id_text_eq);
        }
    }
	private String n_workorder_id_text_like;//[待消耗的工单]
	public void setN_workorder_id_text_like(String n_workorder_id_text_like) {
        this.n_workorder_id_text_like = n_workorder_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_workorder_id_text_like)){
            this.getSearchCond().like("workorder_id_text", n_workorder_id_text_like);
        }
    }
	private String n_consume_unbuild_id_text_eq;//[拆卸单]
	public void setN_consume_unbuild_id_text_eq(String n_consume_unbuild_id_text_eq) {
        this.n_consume_unbuild_id_text_eq = n_consume_unbuild_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_consume_unbuild_id_text_eq)){
            this.getSearchCond().eq("consume_unbuild_id_text", n_consume_unbuild_id_text_eq);
        }
    }
	private String n_consume_unbuild_id_text_like;//[拆卸单]
	public void setN_consume_unbuild_id_text_like(String n_consume_unbuild_id_text_like) {
        this.n_consume_unbuild_id_text_like = n_consume_unbuild_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_consume_unbuild_id_text_like)){
            this.getSearchCond().like("consume_unbuild_id_text", n_consume_unbuild_id_text_like);
        }
    }
	private String n_product_packaging_text_eq;//[首选包装]
	public void setN_product_packaging_text_eq(String n_product_packaging_text_eq) {
        this.n_product_packaging_text_eq = n_product_packaging_text_eq;
        if(!ObjectUtils.isEmpty(this.n_product_packaging_text_eq)){
            this.getSearchCond().eq("product_packaging_text", n_product_packaging_text_eq);
        }
    }
	private String n_product_packaging_text_like;//[首选包装]
	public void setN_product_packaging_text_like(String n_product_packaging_text_like) {
        this.n_product_packaging_text_like = n_product_packaging_text_like;
        if(!ObjectUtils.isEmpty(this.n_product_packaging_text_like)){
            this.getSearchCond().like("product_packaging_text", n_product_packaging_text_like);
        }
    }
	private String n_restrict_partner_id_text_eq;//[所有者]
	public void setN_restrict_partner_id_text_eq(String n_restrict_partner_id_text_eq) {
        this.n_restrict_partner_id_text_eq = n_restrict_partner_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_restrict_partner_id_text_eq)){
            this.getSearchCond().eq("restrict_partner_id_text", n_restrict_partner_id_text_eq);
        }
    }
	private String n_restrict_partner_id_text_like;//[所有者]
	public void setN_restrict_partner_id_text_like(String n_restrict_partner_id_text_like) {
        this.n_restrict_partner_id_text_like = n_restrict_partner_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_restrict_partner_id_text_like)){
            this.getSearchCond().like("restrict_partner_id_text", n_restrict_partner_id_text_like);
        }
    }
	private String n_inventory_id_text_eq;//[库存]
	public void setN_inventory_id_text_eq(String n_inventory_id_text_eq) {
        this.n_inventory_id_text_eq = n_inventory_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_inventory_id_text_eq)){
            this.getSearchCond().eq("inventory_id_text", n_inventory_id_text_eq);
        }
    }
	private String n_inventory_id_text_like;//[库存]
	public void setN_inventory_id_text_like(String n_inventory_id_text_like) {
        this.n_inventory_id_text_like = n_inventory_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_inventory_id_text_like)){
            this.getSearchCond().like("inventory_id_text", n_inventory_id_text_like);
        }
    }
	private String n_rule_id_text_eq;//[库存规则]
	public void setN_rule_id_text_eq(String n_rule_id_text_eq) {
        this.n_rule_id_text_eq = n_rule_id_text_eq;
        if(!ObjectUtils.isEmpty(this.n_rule_id_text_eq)){
            this.getSearchCond().eq("rule_id_text", n_rule_id_text_eq);
        }
    }
	private String n_rule_id_text_like;//[库存规则]
	public void setN_rule_id_text_like(String n_rule_id_text_like) {
        this.n_rule_id_text_like = n_rule_id_text_like;
        if(!ObjectUtils.isEmpty(this.n_rule_id_text_like)){
            this.getSearchCond().like("rule_id_text", n_rule_id_text_like);
        }
    }
	private String n_write_uid_text_eq;//[最后更新者]
	public void setN_write_uid_text_eq(String n_write_uid_text_eq) {
        this.n_write_uid_text_eq = n_write_uid_text_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_eq)){
            this.getSearchCond().eq("write_uid_text", n_write_uid_text_eq);
        }
    }
	private String n_write_uid_text_like;//[最后更新者]
	public void setN_write_uid_text_like(String n_write_uid_text_like) {
        this.n_write_uid_text_like = n_write_uid_text_like;
        if(!ObjectUtils.isEmpty(this.n_write_uid_text_like)){
            this.getSearchCond().like("write_uid_text", n_write_uid_text_like);
        }
    }
	private Long n_created_production_id_eq;//[创建生产订单]
	public void setN_created_production_id_eq(Long n_created_production_id_eq) {
        this.n_created_production_id_eq = n_created_production_id_eq;
        if(!ObjectUtils.isEmpty(this.n_created_production_id_eq)){
            this.getSearchCond().eq("created_production_id", n_created_production_id_eq);
        }
    }
	private Long n_consume_unbuild_id_eq;//[拆卸单]
	public void setN_consume_unbuild_id_eq(Long n_consume_unbuild_id_eq) {
        this.n_consume_unbuild_id_eq = n_consume_unbuild_id_eq;
        if(!ObjectUtils.isEmpty(this.n_consume_unbuild_id_eq)){
            this.getSearchCond().eq("consume_unbuild_id", n_consume_unbuild_id_eq);
        }
    }
	private Long n_sale_line_id_eq;//[销售明细行]
	public void setN_sale_line_id_eq(Long n_sale_line_id_eq) {
        this.n_sale_line_id_eq = n_sale_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_sale_line_id_eq)){
            this.getSearchCond().eq("sale_line_id", n_sale_line_id_eq);
        }
    }
	private Long n_origin_returned_move_id_eq;//[原始退回移动]
	public void setN_origin_returned_move_id_eq(Long n_origin_returned_move_id_eq) {
        this.n_origin_returned_move_id_eq = n_origin_returned_move_id_eq;
        if(!ObjectUtils.isEmpty(this.n_origin_returned_move_id_eq)){
            this.getSearchCond().eq("origin_returned_move_id", n_origin_returned_move_id_eq);
        }
    }
	private Long n_picking_type_id_eq;//[作业类型]
	public void setN_picking_type_id_eq(Long n_picking_type_id_eq) {
        this.n_picking_type_id_eq = n_picking_type_id_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_type_id_eq)){
            this.getSearchCond().eq("picking_type_id", n_picking_type_id_eq);
        }
    }
	private Long n_operation_id_eq;//[待加工的作业]
	public void setN_operation_id_eq(Long n_operation_id_eq) {
        this.n_operation_id_eq = n_operation_id_eq;
        if(!ObjectUtils.isEmpty(this.n_operation_id_eq)){
            this.getSearchCond().eq("operation_id", n_operation_id_eq);
        }
    }
	private Long n_bom_line_id_eq;//[BOM行]
	public void setN_bom_line_id_eq(Long n_bom_line_id_eq) {
        this.n_bom_line_id_eq = n_bom_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_bom_line_id_eq)){
            this.getSearchCond().eq("bom_line_id", n_bom_line_id_eq);
        }
    }
	private Long n_repair_id_eq;//[维修]
	public void setN_repair_id_eq(Long n_repair_id_eq) {
        this.n_repair_id_eq = n_repair_id_eq;
        if(!ObjectUtils.isEmpty(this.n_repair_id_eq)){
            this.getSearchCond().eq("repair_id", n_repair_id_eq);
        }
    }
	private Long n_product_id_eq;//[产品]
	public void setN_product_id_eq(Long n_product_id_eq) {
        this.n_product_id_eq = n_product_id_eq;
        if(!ObjectUtils.isEmpty(this.n_product_id_eq)){
            this.getSearchCond().eq("product_id", n_product_id_eq);
        }
    }
	private Long n_unbuild_id_eq;//[拆卸顺序]
	public void setN_unbuild_id_eq(Long n_unbuild_id_eq) {
        this.n_unbuild_id_eq = n_unbuild_id_eq;
        if(!ObjectUtils.isEmpty(this.n_unbuild_id_eq)){
            this.getSearchCond().eq("unbuild_id", n_unbuild_id_eq);
        }
    }
	private Long n_production_id_eq;//[成品的生产订单]
	public void setN_production_id_eq(Long n_production_id_eq) {
        this.n_production_id_eq = n_production_id_eq;
        if(!ObjectUtils.isEmpty(this.n_production_id_eq)){
            this.getSearchCond().eq("production_id", n_production_id_eq);
        }
    }
	private Long n_created_purchase_line_id_eq;//[创建采购订单行]
	public void setN_created_purchase_line_id_eq(Long n_created_purchase_line_id_eq) {
        this.n_created_purchase_line_id_eq = n_created_purchase_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_created_purchase_line_id_eq)){
            this.getSearchCond().eq("created_purchase_line_id", n_created_purchase_line_id_eq);
        }
    }
	private Long n_workorder_id_eq;//[待消耗的工单]
	public void setN_workorder_id_eq(Long n_workorder_id_eq) {
        this.n_workorder_id_eq = n_workorder_id_eq;
        if(!ObjectUtils.isEmpty(this.n_workorder_id_eq)){
            this.getSearchCond().eq("workorder_id", n_workorder_id_eq);
        }
    }
	private Long n_product_uom_eq;//[单位]
	public void setN_product_uom_eq(Long n_product_uom_eq) {
        this.n_product_uom_eq = n_product_uom_eq;
        if(!ObjectUtils.isEmpty(this.n_product_uom_eq)){
            this.getSearchCond().eq("product_uom", n_product_uom_eq);
        }
    }
	private Long n_picking_id_eq;//[调拨参照]
	public void setN_picking_id_eq(Long n_picking_id_eq) {
        this.n_picking_id_eq = n_picking_id_eq;
        if(!ObjectUtils.isEmpty(this.n_picking_id_eq)){
            this.getSearchCond().eq("picking_id", n_picking_id_eq);
        }
    }
	private Long n_partner_id_eq;//[目的地地址]
	public void setN_partner_id_eq(Long n_partner_id_eq) {
        this.n_partner_id_eq = n_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_partner_id_eq)){
            this.getSearchCond().eq("partner_id", n_partner_id_eq);
        }
    }
	private Long n_create_uid_eq;//[创建人]
	public void setN_create_uid_eq(Long n_create_uid_eq) {
        this.n_create_uid_eq = n_create_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_create_uid_eq)){
            this.getSearchCond().eq("create_uid", n_create_uid_eq);
        }
    }
	private Long n_package_level_id_eq;//[包裹层级]
	public void setN_package_level_id_eq(Long n_package_level_id_eq) {
        this.n_package_level_id_eq = n_package_level_id_eq;
        if(!ObjectUtils.isEmpty(this.n_package_level_id_eq)){
            this.getSearchCond().eq("package_level_id", n_package_level_id_eq);
        }
    }
	private Long n_product_packaging_eq;//[首选包装]
	public void setN_product_packaging_eq(Long n_product_packaging_eq) {
        this.n_product_packaging_eq = n_product_packaging_eq;
        if(!ObjectUtils.isEmpty(this.n_product_packaging_eq)){
            this.getSearchCond().eq("product_packaging", n_product_packaging_eq);
        }
    }
	private Long n_restrict_partner_id_eq;//[所有者]
	public void setN_restrict_partner_id_eq(Long n_restrict_partner_id_eq) {
        this.n_restrict_partner_id_eq = n_restrict_partner_id_eq;
        if(!ObjectUtils.isEmpty(this.n_restrict_partner_id_eq)){
            this.getSearchCond().eq("restrict_partner_id", n_restrict_partner_id_eq);
        }
    }
	private Long n_rule_id_eq;//[库存规则]
	public void setN_rule_id_eq(Long n_rule_id_eq) {
        this.n_rule_id_eq = n_rule_id_eq;
        if(!ObjectUtils.isEmpty(this.n_rule_id_eq)){
            this.getSearchCond().eq("rule_id", n_rule_id_eq);
        }
    }
	private Long n_warehouse_id_eq;//[仓库]
	public void setN_warehouse_id_eq(Long n_warehouse_id_eq) {
        this.n_warehouse_id_eq = n_warehouse_id_eq;
        if(!ObjectUtils.isEmpty(this.n_warehouse_id_eq)){
            this.getSearchCond().eq("warehouse_id", n_warehouse_id_eq);
        }
    }
	private Long n_location_dest_id_eq;//[目的位置]
	public void setN_location_dest_id_eq(Long n_location_dest_id_eq) {
        this.n_location_dest_id_eq = n_location_dest_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_dest_id_eq)){
            this.getSearchCond().eq("location_dest_id", n_location_dest_id_eq);
        }
    }
	private Long n_raw_material_production_id_eq;//[原材料的生产订单]
	public void setN_raw_material_production_id_eq(Long n_raw_material_production_id_eq) {
        this.n_raw_material_production_id_eq = n_raw_material_production_id_eq;
        if(!ObjectUtils.isEmpty(this.n_raw_material_production_id_eq)){
            this.getSearchCond().eq("raw_material_production_id", n_raw_material_production_id_eq);
        }
    }
	private Long n_inventory_id_eq;//[库存]
	public void setN_inventory_id_eq(Long n_inventory_id_eq) {
        this.n_inventory_id_eq = n_inventory_id_eq;
        if(!ObjectUtils.isEmpty(this.n_inventory_id_eq)){
            this.getSearchCond().eq("inventory_id", n_inventory_id_eq);
        }
    }
	private Long n_purchase_line_id_eq;//[采购订单行]
	public void setN_purchase_line_id_eq(Long n_purchase_line_id_eq) {
        this.n_purchase_line_id_eq = n_purchase_line_id_eq;
        if(!ObjectUtils.isEmpty(this.n_purchase_line_id_eq)){
            this.getSearchCond().eq("purchase_line_id", n_purchase_line_id_eq);
        }
    }
	private Long n_location_id_eq;//[源位置]
	public void setN_location_id_eq(Long n_location_id_eq) {
        this.n_location_id_eq = n_location_id_eq;
        if(!ObjectUtils.isEmpty(this.n_location_id_eq)){
            this.getSearchCond().eq("location_id", n_location_id_eq);
        }
    }
	private Long n_company_id_eq;//[公司]
	public void setN_company_id_eq(Long n_company_id_eq) {
        this.n_company_id_eq = n_company_id_eq;
        if(!ObjectUtils.isEmpty(this.n_company_id_eq)){
            this.getSearchCond().eq("company_id", n_company_id_eq);
        }
    }
	private Long n_write_uid_eq;//[最后更新者]
	public void setN_write_uid_eq(Long n_write_uid_eq) {
        this.n_write_uid_eq = n_write_uid_eq;
        if(!ObjectUtils.isEmpty(this.n_write_uid_eq)){
            this.getSearchCond().eq("write_uid", n_write_uid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("name", query)   
            );
		 }
	}
}




import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Uom_categoryService from '@/service/uom-category/uom-category-service';
import BasicListExpViewlistexpbarModel from './basic-list-exp-viewlistexpbar-listexpbar-model';


/**
 * BasicListExpViewlistexpbar 部件服务对象
 *
 * @export
 * @class BasicListExpViewlistexpbarService
 */
export default class BasicListExpViewlistexpbarService extends ControlService {

    /**
     * 产品计量单位 类别服务对象
     *
     * @type {Uom_categoryService}
     * @memberof BasicListExpViewlistexpbarService
     */
    public appEntityService: Uom_categoryService = new Uom_categoryService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof BasicListExpViewlistexpbarService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of BasicListExpViewlistexpbarService.
     * 
     * @param {*} [opts={}]
     * @memberof BasicListExpViewlistexpbarService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new BasicListExpViewlistexpbarModel();
    }

}
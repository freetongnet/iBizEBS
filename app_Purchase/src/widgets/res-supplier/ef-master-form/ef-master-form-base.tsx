import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Res_supplierService from '@/service/res-supplier/res-supplier-service';
import EF_MasterService from './ef-master-form-service';
import Res_supplierUIService from '@/uiservice/res-supplier/res-supplier-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_MasterEditFormBase}
 */
export class EF_MasterEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_MasterService}
     * @memberof EF_MasterEditFormBase
     */
    public service: EF_MasterService = new EF_MasterService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Res_supplierService}
     * @memberof EF_MasterEditFormBase
     */
    public appEntityService: Res_supplierService = new Res_supplierService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected appDeName: string = 'res_supplier';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected appDeLogicName: string = '供应商';

    /**
     * 界面UI服务对象
     *
     * @type {Res_supplierUIService}
     * @memberof EF_MasterBase
     */  
    public appUIService:Res_supplierUIService = new Res_supplierUIService(this.$store);


    /**
     * 关系界面数量
     *
     * @protected
     * @type {number}
     * @memberof EF_MasterEditFormBase
     */
    protected drCount: number = 1;
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public data: any = {
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        type: null,
        vat: null,
        is_company: null,
        parent_name: null,
        ibizfunction: null,
        title_text: null,
        phone: null,
        mobile: null,
        email: null,
        website_url: null,
        category_id: null,
        country_id_text: null,
        state_id_text: null,
        city: null,
        street: null,
        street2: null,
        zip: null,
        user_name: null,
        property_delivery_carrier_name: null,
        property_payment_term_name: null,
        property_product_pricelist_name: null,
        barcode: null,
        property_supplier_payment_term_name: null,
        property_purchase_currency_name: null,
        property_stock_customer_name: null,
        property_stock_supplier_name: null,
        property_stock_subcontractor_name: null,
        ref: null,
        company_id_text: null,
        property_account_position_name: null,
        company_id: null,
        property_account_position_id: null,
        country_id: null,
        property_purchase_currency_id: null,
        property_stock_supplier: null,
        id: null,
        property_stock_subcontractor: null,
        state_id: null,
        title: null,
        property_stock_customer: null,
        property_delivery_carrier_id: null,
        property_supplier_payment_term_id: null,
        user_id: null,
        property_payment_term_id: null,
        property_product_pricelist: null,
        parent_id: null,
        res_supplier:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public rules():any{
        return {
        type: [
            { required: this.detailsModel.type.required, type: 'string', message: '地址类型 值不能为空', trigger: 'change' },
            { required: this.detailsModel.type.required, type: 'string', message: '地址类型 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '分组面板', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: false, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_supplier.ef_master_form', extractMode: 'ITEM', details: [] } }),

        group1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_supplier.ef_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel1: new FormGroupPanelModel({ caption: '地址', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_supplier.ef_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel3: new FormGroupPanelModel({ caption: '销售', detailType: 'GROUPPANEL', name: 'grouppanel3', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_supplier.ef_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel4: new FormGroupPanelModel({ caption: '采购', detailType: 'GROUPPANEL', name: 'grouppanel4', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_supplier.ef_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel5: new FormGroupPanelModel({ caption: '库存', detailType: 'GROUPPANEL', name: 'grouppanel5', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_supplier.ef_master_form', extractMode: 'ITEM', details: [] } }),

        grouppanel6: new FormGroupPanelModel({ caption: '其他', detailType: 'GROUPPANEL', name: 'grouppanel6', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_supplier.ef_master_form', extractMode: 'ITEM', details: [] } }),

        druipart1: new FormDRUIPartModel({ caption: '银行账户', detailType: 'DRUIPART', name: 'druipart1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel7: new FormGroupPanelModel({ caption: '开票', detailType: 'GROUPPANEL', name: 'grouppanel7', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.res_supplier.ef_master_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '名称', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        type: new FormItemModel({ caption: '地址类型', detailType: 'FORMITEM', name: 'type', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        vat: new FormItemModel({ caption: '税号', detailType: 'FORMITEM', name: 'vat', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        is_company: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'is_company', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        parent_name: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'parent_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        ibizfunction: new FormItemModel({ caption: '工作岗位', detailType: 'FORMITEM', name: 'ibizfunction', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        title_text: new FormItemModel({ caption: '称谓', detailType: 'FORMITEM', name: 'title_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        phone: new FormItemModel({ caption: '电话', detailType: 'FORMITEM', name: 'phone', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        mobile: new FormItemModel({ caption: '手机', detailType: 'FORMITEM', name: 'mobile', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        email: new FormItemModel({ caption: 'EMail', detailType: 'FORMITEM', name: 'email', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        website_url: new FormItemModel({ caption: '网站网址', detailType: 'FORMITEM', name: 'website_url', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        category_id: new FormItemModel({ caption: '标签', detailType: 'FORMITEM', name: 'category_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        country_id_text: new FormItemModel({ caption: '国家/地区', detailType: 'FORMITEM', name: 'country_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        state_id_text: new FormItemModel({ caption: '州/省', detailType: 'FORMITEM', name: 'state_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        city: new FormItemModel({ caption: '城市', detailType: 'FORMITEM', name: 'city', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        street: new FormItemModel({ caption: '街道', detailType: 'FORMITEM', name: 'street', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        street2: new FormItemModel({ caption: '街道 2', detailType: 'FORMITEM', name: 'street2', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        zip: new FormItemModel({ caption: '邮政编码', detailType: 'FORMITEM', name: 'zip', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        user_name: new FormItemModel({ caption: '销售员', detailType: 'FORMITEM', name: 'user_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_delivery_carrier_name: new FormItemModel({ caption: '交货方法', detailType: 'FORMITEM', name: 'property_delivery_carrier_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_payment_term_name: new FormItemModel({ caption: '付款条款', detailType: 'FORMITEM', name: 'property_payment_term_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_product_pricelist_name: new FormItemModel({ caption: '价格表', detailType: 'FORMITEM', name: 'property_product_pricelist_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        barcode: new FormItemModel({ caption: '条码', detailType: 'FORMITEM', name: 'barcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_supplier_payment_term_name: new FormItemModel({ caption: '付款条款', detailType: 'FORMITEM', name: 'property_supplier_payment_term_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_purchase_currency_name: new FormItemModel({ caption: '供应商货币', detailType: 'FORMITEM', name: 'property_purchase_currency_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_stock_customer_name: new FormItemModel({ caption: '客户位置', detailType: 'FORMITEM', name: 'property_stock_customer_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_stock_supplier_name: new FormItemModel({ caption: '供应商位置', detailType: 'FORMITEM', name: 'property_stock_supplier_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_stock_subcontractor_name: new FormItemModel({ caption: '分包商位置', detailType: 'FORMITEM', name: 'property_stock_subcontractor_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        ref: new FormItemModel({ caption: '编号', detailType: 'FORMITEM', name: 'ref', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_id_text: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_account_position_name: new FormItemModel({ caption: '税科目调整', detailType: 'FORMITEM', name: 'property_account_position_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'company_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_account_position_id: new FormItemModel({ caption: '税科目调整', detailType: 'FORMITEM', name: 'property_account_position_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        country_id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'country_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_purchase_currency_id: new FormItemModel({ caption: '供应商货币', detailType: 'FORMITEM', name: 'property_purchase_currency_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_stock_supplier: new FormItemModel({ caption: '供应商位置', detailType: 'FORMITEM', name: 'property_stock_supplier', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        property_stock_subcontractor: new FormItemModel({ caption: '分包商位置', detailType: 'FORMITEM', name: 'property_stock_subcontractor', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        state_id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'state_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        title: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'title', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_stock_customer: new FormItemModel({ caption: '客户位置', detailType: 'FORMITEM', name: 'property_stock_customer', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_delivery_carrier_id: new FormItemModel({ caption: '交货方法', detailType: 'FORMITEM', name: 'property_delivery_carrier_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_supplier_payment_term_id: new FormItemModel({ caption: '采购付款条例', detailType: 'FORMITEM', name: 'property_supplier_payment_term_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        user_id: new FormItemModel({ caption: '销售员', detailType: 'FORMITEM', name: 'user_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_payment_term_id: new FormItemModel({ caption: '销售付款条款', detailType: 'FORMITEM', name: 'property_payment_term_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        property_product_pricelist: new FormItemModel({ caption: '价格表', detailType: 'FORMITEM', name: 'property_product_pricelist', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        parent_id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'parent_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };

    /**
     * 表单项逻辑
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @returns {Promise<void>}
     * @memberof EF_MasterEditFormBase
     */
    public async formLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): Promise<void> {
                
        if (Object.is(name, '') || Object.is(name, 'is_company')) {
            let ret = false;
            const _is_company = this.data.is_company;
            if (this.$verify.testCond(_is_company, 'EQ', 'false')) {
                ret = true;
            }
            this.detailsModel.grouppanel2.setVisible(ret);
        }
































































    }
}
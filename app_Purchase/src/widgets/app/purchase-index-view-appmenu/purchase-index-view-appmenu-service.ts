import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import PurchaseIndexViewModel from './purchase-index-view-appmenu-model';


/**
 * PurchaseIndexView 部件服务对象
 *
 * @export
 * @class PurchaseIndexViewService
 */
export default class PurchaseIndexViewService extends ControlService {

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof PurchaseIndexViewService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of PurchaseIndexViewService.
     * 
     * @param {*} [opts={}]
     * @memberof PurchaseIndexViewService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new PurchaseIndexViewModel();
    }

    /**
     * 获取数据
     *
     * @returns {Promise<any>}
     * @memberof PurchaseIndexView
     */
    @Errorlog
    public get(params: any = {}): Promise<any> {
        return Http.getInstance().get('v7/purchase-index-viewappmenu', params);
    }

}
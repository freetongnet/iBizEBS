/**
 * EF_BasicQuick 部件模型
 *
 * @export
 * @class EF_BasicQuickModel
 */
export default class EF_BasicQuickModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_BasicQuickModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'product_tmpl_id_text',
        prop: 'product_tmpl_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'name_text',
        prop: 'name_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'min_qty',
        prop: 'min_qty',
        dataType: 'FLOAT',
      },
      {
        name: 'delay',
        prop: 'delay',
        dataType: 'INT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'product_tmpl_id',
        prop: 'product_tmpl_id',
        dataType: 'PICKUP',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'PICKUP',
      },
      {
        name: 'product_supplierinfo',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
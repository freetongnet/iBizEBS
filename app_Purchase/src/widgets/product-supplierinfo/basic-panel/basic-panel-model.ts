/**
 * Basic 部件模型
 *
 * @export
 * @class BasicModel
 */
export default class BasicModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof BasicModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'product_tmpl_id_text',
        prop: 'product_tmpl_id_text'
      },
      {
        name: 'name_text',
        prop: 'name_text'
      },
      {
        name: 'price',
        prop: 'price'
      }
    ]
  }
}
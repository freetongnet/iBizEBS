/**
 * State 部件模型
 *
 * @export
 * @class StateModel
 */
export default class StateModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof StateModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'name',
        prop: 'name',
      },
      {
        name: 'origin',
        prop: 'origin',
      },
      {
        name: 'date_end',
        prop: 'date_end',
      },
      {
        name: 'schedule_date',
        prop: 'schedule_date',
      },
      {
        name: 'create_date',
        prop: 'create_date',
      },
      {
        name: 'purchase_requisition',
        prop: 'id',
      },
      {
        name: 'ordering_date',
        prop: 'ordering_date',
      },
      {
        name: '__last_update',
        prop: '__last_update',
      },
      {
        name: 'state',
        prop: 'state',
      },
      {
        name: 'write_date',
        prop: 'write_date',
      },
      {
        name: 'order_count',
        prop: 'order_count',
      },
      {
        name: 'currency_name',
        prop: 'currency_name',
      },
      {
        name: 'create_uname',
        prop: 'create_uname',
      },
      {
        name: 'quantity_copy',
        prop: 'quantity_copy',
      },
      {
        name: 'company_name',
        prop: 'company_name',
      },
      {
        name: 'user_name',
        prop: 'user_name',
      },
      {
        name: 'type_id_text',
        prop: 'type_id_text',
      },
      {
        name: 'vendor_id_text',
        prop: 'vendor_id_text',
      },
      {
        name: 'write_uname',
        prop: 'write_uname',
      },
      {
        name: 'picking_type_id',
        prop: 'picking_type_id',
      },
      {
        name: 'vendor_id',
        prop: 'vendor_id',
      },
      {
        name: 'type_id',
        prop: 'type_id',
      },
      {
        name: 'currency_id',
        prop: 'currency_id',
      },
      {
        name: 'create_uid',
        prop: 'create_uid',
      },
      {
        name: 'write_uid',
        prop: 'write_uid',
      },
      {
        name: 'warehouse_id',
        prop: 'warehouse_id',
      },
      {
        name: 'user_id',
        prop: 'user_id',
      },
      {
        name: 'company_id',
        prop: 'company_id',
      },
    ]
  }


}
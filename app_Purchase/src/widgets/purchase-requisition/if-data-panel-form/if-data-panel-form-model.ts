/**
 * IF_DataPanel 部件模型
 *
 * @export
 * @class IF_DataPanelModel
 */
export default class IF_DataPanelModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof IF_DataPanelModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'type_id_text',
        prop: 'type_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'order_count',
        prop: 'order_count',
        dataType: 'INT',
      },
      {
        name: 'schedule_date',
        prop: 'schedule_date',
        dataType: 'DATE',
      },
      {
        name: 'state',
        prop: 'state',
        dataType: 'SSCODELIST',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'purchase_requisition',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
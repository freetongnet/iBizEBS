import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Purchase_requisitionService from '@/service/purchase-requisition/purchase-requisition-service';
import EF_MasterService from './ef-master-form-service';
import Purchase_requisitionUIService from '@/uiservice/purchase-requisition/purchase-requisition-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_MasterEditFormBase}
 */
export class EF_MasterEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_MasterService}
     * @memberof EF_MasterEditFormBase
     */
    public service: EF_MasterService = new EF_MasterService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisitionService}
     * @memberof EF_MasterEditFormBase
     */
    public appEntityService: Purchase_requisitionService = new Purchase_requisitionService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected appDeName: string = 'purchase_requisition';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterEditFormBase
     */
    protected appDeLogicName: string = '采购申请';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_requisitionUIService}
     * @memberof EF_MasterBase
     */  
    public appUIService:Purchase_requisitionUIService = new Purchase_requisitionUIService(this.$store);


    /**
     * 关系界面数量
     *
     * @protected
     * @type {number}
     * @memberof EF_MasterEditFormBase
     */
    protected drCount: number = 1;
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public data: any = {
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        user_name: null,
        type_id_text: null,
        vendor_id_text: null,
        currency_name: null,
        date_end: null,
        ordering_date: null,
        schedule_date: null,
        origin: null,
        company_name: null,
        id: null,
        company_id: null,
        currency_id: null,
        user_id: null,
        vendor_id: null,
        type_id: null,
        purchase_requisition:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public rules():any{
        return {
        name: [
            { required: this.detailsModel.name.required, type: 'string', message: '申请编号 值不能为空', trigger: 'change' },
            { required: this.detailsModel.name.required, type: 'string', message: '申请编号 值不能为空', trigger: 'blur' },
        ],
        type_id_text: [
            { required: this.detailsModel.type_id_text.required, type: 'string', message: '申请类型 值不能为空', trigger: 'change' },
            { required: this.detailsModel.type_id_text.required, type: 'string', message: '申请类型 值不能为空', trigger: 'blur' },
        ],
        vendor_id_text: [
            { required: this.detailsModel.vendor_id_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'change' },
            { required: this.detailsModel.vendor_id_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'blur' },
        ],
        currency_name: [
            { required: this.detailsModel.currency_name.required, type: 'string', message: '币种 值不能为空', trigger: 'change' },
            { required: this.detailsModel.currency_name.required, type: 'string', message: '币种 值不能为空', trigger: 'blur' },
        ],
        company_name: [
            { required: this.detailsModel.company_name.required, type: 'string', message: '公司 值不能为空', trigger: 'change' },
            { required: this.detailsModel.company_name.required, type: 'string', message: '公司 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_MasterEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.purchase_requisition.ef_master_form', extractMode: 'ITEM', details: [] } }),

        druipart1: new FormDRUIPartModel({ caption: '', detailType: 'DRUIPART', name: 'druipart1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel1: new FormGroupPanelModel({ caption: '产品', detailType: 'GROUPPANEL', name: 'grouppanel1', visible: true, isShowCaption: true, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.purchase_requisition.ef_master_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfmajortext: new FormItemModel({ caption: '申请编号', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '申请编号', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        user_name: new FormItemModel({ caption: '采购员', detailType: 'FORMITEM', name: 'user_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        type_id_text: new FormItemModel({ caption: '申请类型', detailType: 'FORMITEM', name: 'type_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        vendor_id_text: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'vendor_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        currency_name: new FormItemModel({ caption: '币种', detailType: 'FORMITEM', name: 'currency_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        date_end: new FormItemModel({ caption: '申请截止日期', detailType: 'FORMITEM', name: 'date_end', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        ordering_date: new FormItemModel({ caption: '订购日期', detailType: 'FORMITEM', name: 'ordering_date', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        schedule_date: new FormItemModel({ caption: '交货日期', detailType: 'FORMITEM', name: 'schedule_date', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        origin: new FormItemModel({ caption: '源单据', detailType: 'FORMITEM', name: 'origin', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_name: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        company_id: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        currency_id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'currency_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        user_id: new FormItemModel({ caption: '采购员', detailType: 'FORMITEM', name: 'user_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        vendor_id: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'vendor_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        type_id: new FormItemModel({ caption: '申请类型', detailType: 'FORMITEM', name: 'type_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };
}
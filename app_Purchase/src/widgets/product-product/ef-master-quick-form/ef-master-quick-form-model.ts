/**
 * EF_MasterQuick 部件模型
 *
 * @export
 * @class EF_MasterQuickModel
 */
export default class EF_MasterQuickModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_MasterQuickModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'sale_ok',
        prop: 'sale_ok',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'purchase_ok',
        prop: 'purchase_ok',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'can_be_expensed',
        prop: 'can_be_expensed',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'type',
        prop: 'type',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'uom_name',
        prop: 'uom_name',
        dataType: 'PICKUPDATA',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'product_tmpl_id',
        prop: 'product_tmpl_id',
        dataType: 'PICKUP',
      },
      {
        name: 'product_product',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
/**
 * LineEdit 部件模型
 *
 * @export
 * @class LineEditModel
 */
export default class LineEditModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof LineEditGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof LineEditGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'bank_name',
          prop: 'bank_name',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'acc_number',
          prop: 'acc_number',
          dataType: 'TEXT',
          isEditable:true
        },
        {
          name: 'bank_id',
          prop: 'bank_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'res_partner_bank',
          prop: 'id',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
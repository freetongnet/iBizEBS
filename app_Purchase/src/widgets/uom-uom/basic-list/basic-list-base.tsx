import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, ListControlBase } from '@/studio-core';
import Uom_uomService from '@/service/uom-uom/uom-uom-service';
import BasicService from './basic-list-service';
import Uom_uomUIService from '@/uiservice/uom-uom/uom-uom-ui-service';


/**
 * listexpbar_list部件基类
 *
 * @export
 * @class ListControlBase
 * @extends {BasicListBase}
 */
export class BasicListBase extends ListControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected controlType: string = 'LIST';

    /**
     * 建构部件服务对象
     *
     * @type {BasicService}
     * @memberof BasicListBase
     */
    public service: BasicService = new BasicService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Uom_uomService}
     * @memberof BasicListBase
     */
    public appEntityService: Uom_uomService = new Uom_uomService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeName: string = 'uom_uom';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof BasicListBase
     */
    protected appDeLogicName: string = '产品计量单位';

    /**
     * 界面UI服务对象
     *
     * @type {Uom_uomUIService}
     * @memberof BasicBase
     */  
    public appUIService:Uom_uomUIService = new Uom_uomUIService(this.$store);


    /**
     * 分页条数
     *
     * @type {number}
     * @memberof BasicListBase
     */
    public limit: number = 1000;

    /**
     * 排序方向
     *
     * @type {string}
     * @memberof BasicListBase
     */
    public minorSortDir: string = '';


}
/**
 * EF_Master 部件模型
 *
 * @export
 * @class EF_MasterModel
 */
export default class EF_MasterModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof EF_MasterModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'TEXT',
      },
      {
        name: 'sale_ok',
        prop: 'sale_ok',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'purchase_ok',
        prop: 'purchase_ok',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'can_be_expensed',
        prop: 'can_be_expensed',
        dataType: 'TRUEFALSE',
      },
      {
        name: 'type',
        prop: 'type',
        dataType: 'SSCODELIST',
      },
      {
        name: 'categ_id_text',
        prop: 'categ_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'default_code',
        prop: 'default_code',
        dataType: 'TEXT',
      },
      {
        name: 'barcode',
        prop: 'barcode',
        dataType: 'TEXT',
      },
      {
        name: 'list_price',
        prop: 'list_price',
        dataType: 'FLOAT',
      },
      {
        name: 'taxes_id',
        prop: 'taxes_id',
        dataType: 'ONE2MANYDATA',
      },
      {
        name: 'standard_price',
        prop: 'standard_price',
        dataType: 'FLOAT',
      },
      {
        name: 'company_id_text',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'uom_name',
        prop: 'uom_name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'uom_po_id_text',
        prop: 'uom_po_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'uom_id',
        prop: 'uom_id',
        dataType: 'PICKUP',
      },
      {
        name: 'categ_id',
        prop: 'categ_id',
        dataType: 'PICKUP',
      },
      {
        name: 'uom_po_id',
        prop: 'uom_po_id',
        dataType: 'PICKUP',
      },
      {
        name: 'product_template',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
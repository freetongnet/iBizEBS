import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Product_templateService from '@/service/product-template/product-template-service';
import MasterTabInfoViewtabexppanelModel from './master-tab-info-viewtabexppanel-tabexppanel-model';


/**
 * MasterTabInfoViewtabexppanel 部件服务对象
 *
 * @export
 * @class MasterTabInfoViewtabexppanelService
 */
export default class MasterTabInfoViewtabexppanelService extends ControlService {

    /**
     * 产品模板服务对象
     *
     * @type {Product_templateService}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public appEntityService: Product_templateService = new Product_templateService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of MasterTabInfoViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof MasterTabInfoViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new MasterTabInfoViewtabexppanelModel();
    }

    
}
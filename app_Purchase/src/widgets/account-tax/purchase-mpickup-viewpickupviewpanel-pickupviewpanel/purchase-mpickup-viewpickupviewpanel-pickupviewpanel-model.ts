/**
 * PurchaseMPickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PurchaseMPickupViewpickupviewpanelModel
 */
export default class PurchaseMPickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PurchaseMPickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'account_tax',
        prop: 'id',
      },
      {
        name: 'create_date',
      },
      {
        name: 'display_name',
      },
      {
        name: 'type_tax_use',
      },
      {
        name: 'active',
      },
      {
        name: 'children_tax_ids',
      },
      {
        name: 'analytic',
      },
      {
        name: 'price_include',
      },
      {
        name: 'description',
      },
      {
        name: 'write_date',
      },
      {
        name: 'tax_exigibility',
      },
      {
        name: 'name',
      },
      {
        name: 'tag_ids',
      },
      {
        name: 'amount_type',
      },
      {
        name: '__last_update',
      },
      {
        name: 'amount',
      },
      {
        name: 'sequence',
      },
      {
        name: 'include_base_amount',
      },
      {
        name: 'cash_basis_base_account_id_text',
      },
      {
        name: 'hide_tax_exigibility',
      },
      {
        name: 'tax_group_id_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'cash_basis_base_account_id',
      },
      {
        name: 'tax_group_id',
      },
      {
        name: 'company_id',
      },
    ]
  }


}
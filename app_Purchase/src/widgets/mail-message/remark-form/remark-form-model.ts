/**
 * Remark 部件模型
 *
 * @export
 * @class RemarkModel
 */
export default class RemarkModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof RemarkModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'body',
        prop: 'body',
        dataType: 'HTMLTEXT',
      },
      {
        name: 'attachment_ids',
        prop: 'attachment_ids',
        dataType: 'LONGTEXT',
      },
      {
        name: 'subtype_id',
        prop: 'subtype_id',
        dataType: 'PICKUP',
      },
      {
        name: 'model',
        prop: 'model',
        dataType: 'TEXT',
      },
      {
        name: 'res_id',
        prop: 'res_id',
        dataType: 'INT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'mail_message',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
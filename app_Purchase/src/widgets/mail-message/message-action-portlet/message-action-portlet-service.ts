import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MessageAction 部件服务对象
 *
 * @export
 * @class MessageActionService
 */
export default class MessageActionService extends ControlService {
}

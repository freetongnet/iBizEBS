import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import MasterTabViewtabexppanelModel from './master-tab-viewtabexppanel-tabexppanel-model';


/**
 * MasterTabViewtabexppanel 部件服务对象
 *
 * @export
 * @class MasterTabViewtabexppanelService
 */
export default class MasterTabViewtabexppanelService extends ControlService {

    /**
     * 消息服务对象
     *
     * @type {Mail_messageService}
     * @memberof MasterTabViewtabexppanelService
     */
    public appEntityService: Mail_messageService = new Mail_messageService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof MasterTabViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of MasterTabViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof MasterTabViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new MasterTabViewtabexppanelModel();
    }

    
}
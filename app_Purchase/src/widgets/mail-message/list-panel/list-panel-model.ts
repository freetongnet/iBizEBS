/**
 * List 部件模型
 *
 * @export
 * @class ListModel
 */
export default class ListModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ListModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'date',
        prop: 'date'
      },
      {
        name: 'subject',
        prop: 'subject'
      },
      {
        name: 'body',
        prop: 'body'
      }
    ]
  }
}
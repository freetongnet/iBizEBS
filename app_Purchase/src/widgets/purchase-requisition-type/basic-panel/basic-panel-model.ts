/**
 * Basic 部件模型
 *
 * @export
 * @class BasicModel
 */
export default class BasicModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof BasicModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'name',
        prop: 'name'
      },
      {
        name: 'exclusive',
        prop: 'exclusive'
      }
    ]
  }
}
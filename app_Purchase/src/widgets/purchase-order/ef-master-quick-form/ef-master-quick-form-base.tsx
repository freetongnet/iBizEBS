import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import Purchase_orderService from '@/service/purchase-order/purchase-order-service';
import EF_MasterQuickService from './ef-master-quick-form-service';
import Purchase_orderUIService from '@/uiservice/purchase-order/purchase-order-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';


/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {EF_MasterQuickEditFormBase}
 */
export class EF_MasterQuickEditFormBase extends EditFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterQuickEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {EF_MasterQuickService}
     * @memberof EF_MasterQuickEditFormBase
     */
    public service: EF_MasterQuickService = new EF_MasterQuickService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Purchase_orderService}
     * @memberof EF_MasterQuickEditFormBase
     */
    public appEntityService: Purchase_orderService = new Purchase_orderService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterQuickEditFormBase
     */
    protected appDeName: string = 'purchase_order';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof EF_MasterQuickEditFormBase
     */
    protected appDeLogicName: string = '采购订单';

    /**
     * 界面UI服务对象
     *
     * @type {Purchase_orderUIService}
     * @memberof EF_MasterQuickBase
     */  
    public appUIService:Purchase_orderUIService = new Purchase_orderUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        name: null,
        requisition_id_text: null,
        partner_id_text: null,
        currency_id_text: null,
        date_order: null,
        company_id_text: null,
        picking_type_id_text: null,
        id: null,
        company_id: null,
        partner_id: null,
        currency_id: null,
        picking_type_id: null,
        requisition_id: null,
        purchase_order:null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public majorMessageField: string = "name";

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public rules():any{
        return {
        partner_id_text: [
            { required: this.detailsModel.partner_id_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'change' },
            { required: this.detailsModel.partner_id_text.required, type: 'string', message: '供应商 值不能为空', trigger: 'blur' },
        ],
        currency_id_text: [
            { required: this.detailsModel.currency_id_text.required, type: 'string', message: '币种 值不能为空', trigger: 'change' },
            { required: this.detailsModel.currency_id_text.required, type: 'string', message: '币种 值不能为空', trigger: 'blur' },
        ],
        date_order: [
            { required: this.detailsModel.date_order.required, type: 'string', message: '单据日期 值不能为空', trigger: 'change' },
            { required: this.detailsModel.date_order.required, type: 'string', message: '单据日期 值不能为空', trigger: 'blur' },
        ],
        company_id_text: [
            { required: this.detailsModel.company_id_text.required, type: 'string', message: '公司 值不能为空', trigger: 'change' },
            { required: this.detailsModel.company_id_text.required, type: 'string', message: '公司 值不能为空', trigger: 'blur' },
        ],
        picking_type_id_text: [
            { required: this.detailsModel.picking_type_id_text.required, type: 'string', message: '交货到 值不能为空', trigger: 'change' },
            { required: this.detailsModel.picking_type_id_text.required, type: 'string', message: '交货到 值不能为空', trigger: 'blur' },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof EF_MasterQuickBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof EF_MasterQuickEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.purchase_order.ef_masterquick_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({ caption: '最后更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srforikey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfkey: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        srfmajortext: new FormItemModel({ caption: '订单关联', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srftempmode: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfuf: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfdeid: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        srfsourcekey: new FormItemModel({ caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        name: new FormItemModel({ caption: '订单关联', detailType: 'FORMITEM', name: 'name', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        requisition_id_text: new FormItemModel({ caption: '采购申请单', detailType: 'FORMITEM', name: 'requisition_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        partner_id_text: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'partner_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        currency_id_text: new FormItemModel({ caption: '币种', detailType: 'FORMITEM', name: 'currency_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        date_order: new FormItemModel({ caption: '单据日期', detailType: 'FORMITEM', name: 'date_order', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        company_id_text: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        picking_type_id_text: new FormItemModel({ caption: '交货到', detailType: 'FORMITEM', name: 'picking_type_id_text', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:true, disabled: false, enableCond: 3 }),

        id: new FormItemModel({ caption: 'ID', detailType: 'FORMITEM', name: 'id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 0 }),

        company_id: new FormItemModel({ caption: '公司', detailType: 'FORMITEM', name: 'company_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        partner_id: new FormItemModel({ caption: '供应商', detailType: 'FORMITEM', name: 'partner_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        currency_id: new FormItemModel({ caption: '币种', detailType: 'FORMITEM', name: 'currency_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        picking_type_id: new FormItemModel({ caption: '交货到', detailType: 'FORMITEM', name: 'picking_type_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

        requisition_id: new FormItemModel({ caption: '采购申请单', detailType: 'FORMITEM', name: 'requisition_id', visible: true, isShowCaption: true, form: this, showMoreMode: 0, required:false, disabled: false, enableCond: 3 }),

    };

    /**
     * 新建默认值
     * @memberof EF_MasterQuickEditFormBase
     */
    public createDefault(){                    
        if (this.data.hasOwnProperty('name')) {
            this.data['name'] = 'NEW';
        }
    }
}
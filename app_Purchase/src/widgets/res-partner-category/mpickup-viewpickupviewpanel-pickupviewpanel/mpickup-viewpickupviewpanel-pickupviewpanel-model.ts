/**
 * MPickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class MPickupViewpickupviewpanelModel
 */
export default class MPickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MPickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: '__last_update',
      },
      {
        name: 'color',
      },
      {
        name: 'res_partner_category',
        prop: 'id',
      },
      {
        name: 'partner_ids',
      },
      {
        name: 'name',
      },
      {
        name: 'display_name',
      },
      {
        name: 'write_date',
      },
      {
        name: 'child_ids',
      },
      {
        name: 'create_date',
      },
      {
        name: 'active',
      },
      {
        name: 'parent_path',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'parent_id_text',
      },
      {
        name: 'write_uid',
      },
      {
        name: 'parent_id',
      },
      {
        name: 'create_uid',
      },
    ]
  }


}
import enUSUser from '../user/en-US.user';
import { Util } from '@/utils/util/util';
import res_partner_en_US from '@locale/lanres/entities/res-partner/res-partner_en_US';
import stock_location_en_US from '@locale/lanres/entities/stock-location/stock-location_en_US';
import ir_attachment_en_US from '@locale/lanres/entities/ir-attachment/ir-attachment_en_US';
import account_tax_en_US from '@locale/lanres/entities/account-tax/account-tax_en_US';
import res_bank_en_US from '@locale/lanres/entities/res-bank/res-bank_en_US';
import account_fiscal_position_en_US from '@locale/lanres/entities/account-fiscal-position/account-fiscal-position_en_US';
import mail_activity_en_US from '@locale/lanres/entities/mail-activity/mail-activity_en_US';
import mail_message_en_US from '@locale/lanres/entities/mail-message/mail-message_en_US';
import uom_uom_en_US from '@locale/lanres/entities/uom-uom/uom-uom_en_US';
import account_incoterms_en_US from '@locale/lanres/entities/account-incoterms/account-incoterms_en_US';
import stock_picking_type_en_US from '@locale/lanres/entities/stock-picking-type/stock-picking-type_en_US';
import res_config_settings_en_US from '@locale/lanres/entities/res-config-settings/res-config-settings_en_US';
import product_template_en_US from '@locale/lanres/entities/product-template/product-template_en_US';
import product_template_attribute_line_en_US from '@locale/lanres/entities/product-template-attribute-line/product-template-attribute-line_en_US';
import res_supplier_en_US from '@locale/lanres/entities/res-supplier/res-supplier_en_US';
import delivery_carrier_en_US from '@locale/lanres/entities/delivery-carrier/delivery-carrier_en_US';
import res_users_en_US from '@locale/lanres/entities/res-users/res-users_en_US';
import purchase_requisition_type_en_US from '@locale/lanres/entities/purchase-requisition-type/purchase-requisition-type_en_US';
import res_partner_category_en_US from '@locale/lanres/entities/res-partner-category/res-partner-category_en_US';
import mail_activity_type_en_US from '@locale/lanres/entities/mail-activity-type/mail-activity-type_en_US';
import res_currency_en_US from '@locale/lanres/entities/res-currency/res-currency_en_US';
import res_company_en_US from '@locale/lanres/entities/res-company/res-company_en_US';
import mail_followers_en_US from '@locale/lanres/entities/mail-followers/mail-followers_en_US';
import res_partner_bank_en_US from '@locale/lanres/entities/res-partner-bank/res-partner-bank_en_US';
import product_pricelist_en_US from '@locale/lanres/entities/product-pricelist/product-pricelist_en_US';
import res_partner_title_en_US from '@locale/lanres/entities/res-partner-title/res-partner-title_en_US';
import purchase_order_en_US from '@locale/lanres/entities/purchase-order/purchase-order_en_US';
import uom_category_en_US from '@locale/lanres/entities/uom-category/uom-category_en_US';
import res_country_en_US from '@locale/lanres/entities/res-country/res-country_en_US';
import product_category_en_US from '@locale/lanres/entities/product-category/product-category_en_US';
import purchase_requisition_line_en_US from '@locale/lanres/entities/purchase-requisition-line/purchase-requisition-line_en_US';
import res_country_state_en_US from '@locale/lanres/entities/res-country-state/res-country-state_en_US';
import res_country_group_en_US from '@locale/lanres/entities/res-country-group/res-country-group_en_US';
import product_supplierinfo_en_US from '@locale/lanres/entities/product-supplierinfo/product-supplierinfo_en_US';
import purchase_order_line_en_US from '@locale/lanres/entities/purchase-order-line/purchase-order-line_en_US';
import mail_tracking_value_en_US from '@locale/lanres/entities/mail-tracking-value/mail-tracking-value_en_US';
import mail_compose_message_en_US from '@locale/lanres/entities/mail-compose-message/mail-compose-message_en_US';
import account_payment_term_en_US from '@locale/lanres/entities/account-payment-term/account-payment-term_en_US';
import product_product_en_US from '@locale/lanres/entities/product-product/product-product_en_US';
import purchase_requisition_en_US from '@locale/lanres/entities/purchase-requisition/purchase-requisition_en_US';
import components_en_US from '@locale/lanres/components/components_en_US';
import codelist_en_US from '@locale/lanres/codelist/codelist_en_US';
import userCustom_en_US from '@locale/lanres/userCustom/userCustom_en_US';

const data: any = {
    app: {
        commonWords:{
            error: "Error",
            success: "Success",
            ok: "OK",
            cancel: "Cancel",
            save: "Save",
            codeNotExist: 'Code list does not exist',
            reqException: "Request exception",
            sysException: "System abnormality",
            warning: "Warning",
            wrong: "Error",
            rulesException: "Abnormal value check rule",
            saveSuccess: "Saved successfully",
            saveFailed: "Save failed",
            deleteSuccess: "Successfully deleted!",
            deleteError: "Failed to delete",
            delDataFail: "Failed to delete data",
            noData: "No data",
            startsuccess:"Start successful",
            createFailed: 'Unable to create',
            isExist: 'existed'
        },
        local:{
            new: "New",
            add: "Add",
        },
        gridpage: {
            choicecolumns: "Choice columns",
            refresh: "refresh",
            show: "Show",
            records: "records",
            totle: "totle",
            noData: "No data",
            valueVail: "Value cannot be empty",
            notConfig: {
                fetchAction: "The view table fetchaction parameter is not configured",
                removeAction: "The view table removeaction parameter is not configured",
                createAction: "The view table createaction parameter is not configured",
                updateAction: "The view table updateaction parameter is not configured",
                loaddraftAction: "The view table loadtrafaction parameter is not configured",
            },
            data: "Data",
            delDataFail: "Failed to delete data",
            delSuccess: "Delete successfully!",
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
            notBatch: "Batch addition not implemented",
            grid: "Grid",
            exportFail: "Data export failed",
            sum: "Total",
            formitemFailed: "Form item update failed",
        },
        list: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
                createAction: "View list createAction parameter is not configured",
                updateAction: "View list updateAction parameter is not configured",
            },
            confirmDel: "Are you sure you want to delete",
            notRecoverable: "delete will not be recoverable?",
        },
        listExpBar: {
            title: "List navigation bar",
        },
        wfExpBar: {
            title: "Process navigation bar",
        },
        calendarExpBar:{
            title: "Calendar navigation bar",
        },
        treeExpBar: {
            title: "Tree view navigation bar",
        },
        portlet: {
            noExtensions: "No extensions",
        },
        tabpage: {
            sureclosetip: {
                title: "Close warning",
                content: "Form data Changed, are sure close?",
            },
            closeall: "Close all",
            closeother: "Close other",
        },
        fileUpload: {
            caption: "Upload",
        },
        searchButton: {
            search: "Search",
            reset: "Reset",
        },
        calendar:{
          today: "today",
          month: "month",
          week: "week",
          day: "day",
          list: "list",
          dateSelectModalTitle: "select the time you wanted",
          gotoDate: "goto",
          from: "From",
          to: "To",
        },
        // 非实体视图
        views: {
            purchaseindexview: {
                caption: "采购",
                title: "采购首页视图",
            },
        },
        utilview:{
            importview:"Import Data",
            warning:"warning",
            info:"Please configure the data import item"    
        },
        menus: {
            purchaseindexview: {
                user_menus: "用户菜单",
                top_menus: "顶部菜单",
                left_exp: "左侧菜单",
                menuitem1: "订单",
                menuitem2: "采购申请",
                menuitem3: "询价单",
                menuitem4: "采购订单",
                menuitem5: "供应商",
                menuitem7: "产品",
                menuitem6: "产品",
                menuitem8: "产品变种",
                menuitem9: "配置",
                menuitem15: "采购设置",
                menuitem10: "供应商价格表",
                menuitem11: "采购申请类型",
                menuitem12: "产品类别",
                menuitem13: "计量单位",
                menuitem14: "计量单位类别",
                bottom_exp: "底部内容",
                footer_left: "底部左侧",
                footer_center: "底部中间",
                footer_right: "底部右侧",
            },
        },
        formpage:{
            error: "Error",
            desc1: "Operation failed, failed to find current form item",
            desc2: "Can't continue",
            notconfig: {
                loadaction: "View form loadAction parameter is not configured",
                loaddraftaction: "View form loaddraftAction parameter is not configured",
                actionname: "View form actionName parameter is not configured",
                removeaction: "View form removeAction parameter is not configured",
            },
            saveerror: "Error saving data",
            savecontent: "The data is inconsistent. The background data may have been modified. Do you want to reload the data?",
            valuecheckex: "Value rule check exception",
            savesuccess: "Saved successfully!",
            deletesuccess: "Successfully deleted!",  
            workflow: {
                starterror: "Workflow started successfully",
                startsuccess: "Workflow failed to start",
                submiterror: "Workflow submission failed",
                submitsuccess: "Workflow submitted successfully",
            },
            updateerror: "Form item update failed",       
        },
        gridBar: {
            title: "Table navigation bar",
        },
        multiEditView: {
            notConfig: {
                fetchAction: "View multi-edit view panel fetchAction parameter is not configured",
                loaddraftAction: "View multi-edit view panel loaddraftAction parameter is not configured",
            },
        },
        dataViewExpBar: {
            title: "Card view navigation bar",
        },
        kanban: {
            notConfig: {
                fetchAction: "View list fetchAction parameter is not configured",
                removeAction: "View table removeAction parameter is not configured",
            },
            delete1: "Confirm to delete ",
            delete2: "the delete operation will be unrecoverable!",
        },
        dashBoard: {
            handleClick: {
                title: "Panel design",
            },
        },
        dataView: {
            sum: "total",
            data: "data",
        },
        chart: {
            undefined: "Undefined",
            quarter: "Quarter",   
            year: "Year",
        },
        searchForm: {
            notConfig: {
                loadAction: "View search form loadAction parameter is not configured",
                loaddraftAction: "View search form loaddraftAction parameter is not configured",
            },
            custom: "Store custom queries",
            title: "Name",
        },
        wizardPanel: {
            back: "Back",
            next: "Next",
            complete: "Complete",
            preactionmessage:"The calculation of the previous behavior is not configured"
        },
        viewLayoutPanel: {
            appLogoutView: {
                prompt1: "Dear customer, you have successfully exited the system, after",
                prompt2: "seconds, we will jump to the",
                logingPage: "login page",
            },
            appWfstepTraceView: {
                title: "Application process processing record view",
            },
            appWfstepDataView: {
                title: "Application process tracking view",
            },
            appLoginView: {
                username: "Username",
                password: "Password",
                login: "Login",
            },
        },
    },
    form: {
        group: {
            show_more: "Show More",
            hidden_more: "Hide More"
        }
    },
    entities: {
        res_partner: res_partner_en_US,
        stock_location: stock_location_en_US,
        ir_attachment: ir_attachment_en_US,
        account_tax: account_tax_en_US,
        res_bank: res_bank_en_US,
        account_fiscal_position: account_fiscal_position_en_US,
        mail_activity: mail_activity_en_US,
        mail_message: mail_message_en_US,
        uom_uom: uom_uom_en_US,
        account_incoterms: account_incoterms_en_US,
        stock_picking_type: stock_picking_type_en_US,
        res_config_settings: res_config_settings_en_US,
        product_template: product_template_en_US,
        product_template_attribute_line: product_template_attribute_line_en_US,
        res_supplier: res_supplier_en_US,
        delivery_carrier: delivery_carrier_en_US,
        res_users: res_users_en_US,
        purchase_requisition_type: purchase_requisition_type_en_US,
        res_partner_category: res_partner_category_en_US,
        mail_activity_type: mail_activity_type_en_US,
        res_currency: res_currency_en_US,
        res_company: res_company_en_US,
        mail_followers: mail_followers_en_US,
        res_partner_bank: res_partner_bank_en_US,
        product_pricelist: product_pricelist_en_US,
        res_partner_title: res_partner_title_en_US,
        purchase_order: purchase_order_en_US,
        uom_category: uom_category_en_US,
        res_country: res_country_en_US,
        product_category: product_category_en_US,
        purchase_requisition_line: purchase_requisition_line_en_US,
        res_country_state: res_country_state_en_US,
        res_country_group: res_country_group_en_US,
        product_supplierinfo: product_supplierinfo_en_US,
        purchase_order_line: purchase_order_line_en_US,
        mail_tracking_value: mail_tracking_value_en_US,
        mail_compose_message: mail_compose_message_en_US,
        account_payment_term: account_payment_term_en_US,
        product_product: product_product_en_US,
        purchase_requisition: purchase_requisition_en_US,
    },
    components: components_en_US,
    codelist: codelist_en_US,
    userCustom: userCustom_en_US,
};
// 合并用户自定义多语言
Util.mergeDeepObject(data, enUSUser);
// 默认导出
export default data;
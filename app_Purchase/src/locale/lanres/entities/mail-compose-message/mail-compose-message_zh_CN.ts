export default {
  fields: {
    active_domain: "有效域名",
    body: "内容",
    starred_partner_ids: "收藏夹",
    needaction_partner_ids: "待处理的业务伙伴",
    moderation_status: "管理状态",
    auto_delete: "删除邮件",
    use_active_domain: "使用有效域名",
    mass_mailing_name: "群发邮件标题",
    rating_ids: "相关评级",
    notification_ids: "通知",
    description: "说明",
    partner_ids: "添加联系人",
    display_name: "显示名称",
    no_auto_thread: "无响应",
    tracking_value_ids: "追踪值",
    reply_to: "回复 至",
    has_error: "有误差",
    message_id: "消息ID",
    composition_mode: "写作模式",
    __last_update: "最后修改日",
    needaction: "待处理",
    attachment_ids: "附件",
    subject: "主题",
    add_sign: "添加签名",
    mail_server_id: "邮件发送服务器",
    channel_ids: "渠道",
    date: "日期",
    website_published: "已发布",
    child_ids: "下级消息",
    message_type: "类型",
    res_id: "相关文档编号",
    auto_delete_message: "删除消息副本",
    need_moderation: "需审核",
    create_date: "创建时间",
    layout: "布局",
    write_date: "最后更新时间",
    rating_value: "评级值",
    model: "相关的文档模型",
    notify: "通知关注者",
    email_from: "从",
    mailing_list_ids: "邮件列表",
    is_log: "记录内部备注",
    id: "ID",
    starred: "加星的邮件",
    record_name: "消息记录名称",
    template_id_text: "使用模版",
    create_uid_text: "创建人",
    write_uid_text: "最后更新者",
    author_id_text: "作者",
    subtype_id_text: "子类型",
    moderator_id_text: "管理员",
    mass_mailing_campaign_id_text: "群发邮件营销",
    mail_activity_type_id_text: "邮件活动类型",
    author_avatar: "作者头像",
    mass_mailing_id_text: "群发邮件",
    parent_id: "上级消息",
    subtype_id: "子类型",
    template_id: "使用模版",
    mail_activity_type_id: "邮件活动类型",
    author_id: "作者",
    mass_mailing_campaign_id: "群发邮件营销",
    create_uid: "创建人",
    write_uid: "最后更新者",
    moderator_id: "管理员",
    mass_mailing_id: "群发邮件",
  },
};
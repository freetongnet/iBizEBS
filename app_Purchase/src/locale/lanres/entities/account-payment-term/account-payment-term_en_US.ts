export default {
  fields: {
    __last_update: "最后修改日",
    display_name: "显示名称",
    create_date: "创建时间",
    sequence: "序号",
    active: "有效",
    name: "付款条款",
    note: "发票描述",
    line_ids: "条款",
    id: "ID",
    write_date: "最后更新时间",
    create_uid_text: "创建人",
    write_uid_text: "最后更新人",
    company_id_text: "公司",
    company_id: "公司",
    write_uid: "最后更新人",
    create_uid: "创建人",
  },
	views: {
		pickupgridview: {
			caption: "付款条款",
      		title: "付款条款选择表格视图",
		},
		pickupview: {
			caption: "付款条款",
      		title: "付款条款数据选择视图",
		},
	},
	main_grid: {
		nodata: "",
		columns: {
			name: "付款条款",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};
export default {
  fields: {
    __last_update: "最后修改日",
    id: "ID",
    name: "州/省名称",
    code: "州/省代码",
    create_date: "创建时间",
    write_date: "最后更新时间",
    display_name: "显示名称",
    country_id_text: "国家/地区",
    create_uid_text: "创建人",
    write_uid_text: "最后更新者",
    write_uid: "最后更新者",
    create_uid: "创建人",
    country_id: "国家/地区",
  },
};
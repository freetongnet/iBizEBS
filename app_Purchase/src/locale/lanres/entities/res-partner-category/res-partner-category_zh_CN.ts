export default {
  fields: {
    __last_update: "最后修改日",
    color: "颜色索引",
    id: "ID",
    partner_ids: "合作伙伴",
    name: "标签名",
    display_name: "显示名称",
    write_date: "最后更新时间",
    child_ids: "下级标签",
    create_date: "创建时间",
    active: "有效",
    parent_path: "父级路径",
    write_uid_text: "最后更新者",
    create_uid_text: "创建人",
    parent_id_text: "上级类别",
    write_uid: "最后更新者",
    parent_id: "上级类别",
    create_uid: "创建人",
  },
	views: {
		pickupgridview: {
			caption: "业务伙伴标签",
      		title: "业务伙伴标签选择表格视图",
		},
		mpickupview: {
			caption: "业务伙伴标签",
      		title: "业务伙伴标签数据多项选择视图",
		},
	},
	main_grid: {
		nodata: "",
		columns: {
			name: "标签名",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};
export default {
  fields: {
    product_template_value_ids: "产品属性值",
    create_date: "创建时间",
    __last_update: "最后修改日",
    display_name: "显示名称",
    value_ids: "属性值",
    write_date: "最后更新时间",
    id: "ID",
    product_tmpl_id_text: "产品模板",
    write_uid_text: "最后更新人",
    attribute_id_text: "属性",
    create_uid_text: "创建人",
    product_tmpl_id: "产品模板",
    write_uid: "最后更新人",
    attribute_id: "属性",
    create_uid: "创建人",
  },
};
export default {
  fields: {
    field_desc: "字段说明",
    old_value_char: "旧字符值",
    write_date: "最后更新时间",
    new_value_datetime: "新日期时间值",
    old_value_monetary: "旧货币值",
    tracking_sequence: "tracking_sequence",
    new_value_char: "新字符值",
    new_value_text: "新文本值",
    new_value_monetary: "新货币值",
    old_value_datetime: "旧日期时间值",
    __last_update: "最后修改日",
    create_date: "创建时间",
    old_value_integer: "旧整数值",
    old_value_text: "旧文本值",
    field_type: "字段类型",
    new_value_integer: "新整数值",
    display_name: "显示名称",
    new_value_float: "新浮点值",
    id: "ID",
    field: "更改的字段",
    old_value_float: "旧浮点值",
    write_uid_text: "最后更新者",
    create_uid_text: "创建人",
    write_uid: "最后更新者",
    create_uid: "创建人",
    mail_message_id: "邮件消息ID",
  },
	views: {
		editview: {
			caption: "邮件跟踪值",
      		title: "邮件跟踪值编辑视图",
		},
		gridview: {
			caption: "邮件跟踪值",
      		title: "邮件跟踪值表格视图",
		},
	},
	main_form: {
		details: {
			group1: "邮件跟踪值基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "ID", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	main_grid: {
		nodata: "",
		columns: {
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "保存",
			tip: "保存",
		},
		tbitem4: {
			caption: "保存并新建",
			tip: "保存并新建",
		},
		tbitem5: {
			caption: "保存并关闭",
			tip: "保存并关闭",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "删除并关闭",
			tip: "删除并关闭",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "新建",
			tip: "新建",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "帮助",
			tip: "帮助",
		},
	},
	gridviewtoolbar_toolbar: {
		tbitem3: {
			caption: "新建",
			tip: "新建",
		},
		tbitem4: {
			caption: "编辑",
			tip: "编辑",
		},
		tbitem6: {
			caption: "拷贝",
			tip: "拷贝",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "删除",
			tip: "删除",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "导出",
			tip: "导出",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem16: {
			caption: "其它",
			tip: "其它",
		},
		tbitem21: {
			caption: "导出数据模型",
			tip: "导出数据模型",
		},
		tbitem23: {
			caption: "数据导入",
			tip: "数据导入",
		},
		tbitem17: {
			caption: "-",
			tip: "",
		},
		tbitem19: {
			caption: "过滤",
			tip: "过滤",
		},
		tbitem18: {
			caption: "帮助",
			tip: "帮助",
		},
	},
};
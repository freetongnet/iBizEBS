import Res_countryUIServiceBase from './res-country-ui-service-base';

/**
 * 国家/地区UI服务对象
 *
 * @export
 * @class Res_countryUIService
 */
export default class Res_countryUIService extends Res_countryUIServiceBase {

    /**
     * Creates an instance of  Res_countryUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_countryUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
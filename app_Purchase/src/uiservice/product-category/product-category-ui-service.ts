import Product_categoryUIServiceBase from './product-category-ui-service-base';

/**
 * 产品种类UI服务对象
 *
 * @export
 * @class Product_categoryUIService
 */
export default class Product_categoryUIService extends Product_categoryUIServiceBase {

    /**
     * Creates an instance of  Product_categoryUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_categoryUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import Res_partner_bankUIServiceBase from './res-partner-bank-ui-service-base';

/**
 * 银行账户UI服务对象
 *
 * @export
 * @class Res_partner_bankUIService
 */
export default class Res_partner_bankUIService extends Res_partner_bankUIServiceBase {

    /**
     * Creates an instance of  Res_partner_bankUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_bankUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
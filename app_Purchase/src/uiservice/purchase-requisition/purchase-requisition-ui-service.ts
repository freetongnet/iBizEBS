import Purchase_requisitionUIServiceBase from './purchase-requisition-ui-service-base';

/**
 * 采购申请UI服务对象
 *
 * @export
 * @class Purchase_requisitionUIService
 */
export default class Purchase_requisitionUIService extends Purchase_requisitionUIServiceBase {

    /**
     * Creates an instance of  Purchase_requisitionUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisitionUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
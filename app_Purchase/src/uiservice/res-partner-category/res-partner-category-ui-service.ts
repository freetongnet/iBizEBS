import Res_partner_categoryUIServiceBase from './res-partner-category-ui-service-base';

/**
 * 业务伙伴标签UI服务对象
 *
 * @export
 * @class Res_partner_categoryUIService
 */
export default class Res_partner_categoryUIService extends Res_partner_categoryUIServiceBase {

    /**
     * Creates an instance of  Res_partner_categoryUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_categoryUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
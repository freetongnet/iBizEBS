import Product_supplierinfoUIServiceBase from './product-supplierinfo-ui-service-base';

/**
 * 供应商价格表UI服务对象
 *
 * @export
 * @class Product_supplierinfoUIService
 */
export default class Product_supplierinfoUIService extends Product_supplierinfoUIServiceBase {

    /**
     * Creates an instance of  Product_supplierinfoUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_supplierinfoUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import Res_currencyUIServiceBase from './res-currency-ui-service-base';

/**
 * 币种UI服务对象
 *
 * @export
 * @class Res_currencyUIService
 */
export default class Res_currencyUIService extends Res_currencyUIServiceBase {

    /**
     * Creates an instance of  Res_currencyUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_currencyUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
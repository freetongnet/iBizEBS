import Res_partner_titleUIServiceBase from './res-partner-title-ui-service-base';

/**
 * 业务伙伴称谓UI服务对象
 *
 * @export
 * @class Res_partner_titleUIService
 */
export default class Res_partner_titleUIService extends Res_partner_titleUIServiceBase {

    /**
     * Creates an instance of  Res_partner_titleUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_titleUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
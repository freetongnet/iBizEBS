import Account_fiscal_positionUIServiceBase from './account-fiscal-position-ui-service-base';

/**
 * 税科目调整UI服务对象
 *
 * @export
 * @class Account_fiscal_positionUIService
 */
export default class Account_fiscal_positionUIService extends Account_fiscal_positionUIServiceBase {

    /**
     * Creates an instance of  Account_fiscal_positionUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_fiscal_positionUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
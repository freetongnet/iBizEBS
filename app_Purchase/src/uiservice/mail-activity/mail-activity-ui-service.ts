import Mail_activityUIServiceBase from './mail-activity-ui-service-base';

/**
 * 活动UI服务对象
 *
 * @export
 * @class Mail_activityUIService
 */
export default class Mail_activityUIService extends Mail_activityUIServiceBase {

    /**
     * Creates an instance of  Mail_activityUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_activityUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
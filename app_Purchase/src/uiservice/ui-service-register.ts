/**
 * UI服务注册中心
 *
 * @export
 * @class UIServiceRegister
 */
export class UIServiceRegister {

    /**
     * 所有UI实体服务Map
     *
     * @protected
     * @type {*}
     * @memberof UIServiceRegister
     */
    protected allUIService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载UI实体服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof UIServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of UIServiceRegister.
     * @memberof UIServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof UIServiceRegister
     */
    protected init(): void {
                this.allUIService.set('res_partner', () => import('@/uiservice/res-partner/res-partner-ui-service'));
        this.allUIService.set('stock_location', () => import('@/uiservice/stock-location/stock-location-ui-service'));
        this.allUIService.set('ir_attachment', () => import('@/uiservice/ir-attachment/ir-attachment-ui-service'));
        this.allUIService.set('account_tax', () => import('@/uiservice/account-tax/account-tax-ui-service'));
        this.allUIService.set('res_bank', () => import('@/uiservice/res-bank/res-bank-ui-service'));
        this.allUIService.set('account_fiscal_position', () => import('@/uiservice/account-fiscal-position/account-fiscal-position-ui-service'));
        this.allUIService.set('mail_activity', () => import('@/uiservice/mail-activity/mail-activity-ui-service'));
        this.allUIService.set('mail_message', () => import('@/uiservice/mail-message/mail-message-ui-service'));
        this.allUIService.set('uom_uom', () => import('@/uiservice/uom-uom/uom-uom-ui-service'));
        this.allUIService.set('account_incoterms', () => import('@/uiservice/account-incoterms/account-incoterms-ui-service'));
        this.allUIService.set('stock_picking_type', () => import('@/uiservice/stock-picking-type/stock-picking-type-ui-service'));
        this.allUIService.set('res_config_settings', () => import('@/uiservice/res-config-settings/res-config-settings-ui-service'));
        this.allUIService.set('product_template', () => import('@/uiservice/product-template/product-template-ui-service'));
        this.allUIService.set('product_template_attribute_line', () => import('@/uiservice/product-template-attribute-line/product-template-attribute-line-ui-service'));
        this.allUIService.set('res_supplier', () => import('@/uiservice/res-supplier/res-supplier-ui-service'));
        this.allUIService.set('delivery_carrier', () => import('@/uiservice/delivery-carrier/delivery-carrier-ui-service'));
        this.allUIService.set('res_users', () => import('@/uiservice/res-users/res-users-ui-service'));
        this.allUIService.set('purchase_requisition_type', () => import('@/uiservice/purchase-requisition-type/purchase-requisition-type-ui-service'));
        this.allUIService.set('res_partner_category', () => import('@/uiservice/res-partner-category/res-partner-category-ui-service'));
        this.allUIService.set('mail_activity_type', () => import('@/uiservice/mail-activity-type/mail-activity-type-ui-service'));
        this.allUIService.set('res_currency', () => import('@/uiservice/res-currency/res-currency-ui-service'));
        this.allUIService.set('res_company', () => import('@/uiservice/res-company/res-company-ui-service'));
        this.allUIService.set('mail_followers', () => import('@/uiservice/mail-followers/mail-followers-ui-service'));
        this.allUIService.set('res_partner_bank', () => import('@/uiservice/res-partner-bank/res-partner-bank-ui-service'));
        this.allUIService.set('product_pricelist', () => import('@/uiservice/product-pricelist/product-pricelist-ui-service'));
        this.allUIService.set('res_partner_title', () => import('@/uiservice/res-partner-title/res-partner-title-ui-service'));
        this.allUIService.set('purchase_order', () => import('@/uiservice/purchase-order/purchase-order-ui-service'));
        this.allUIService.set('uom_category', () => import('@/uiservice/uom-category/uom-category-ui-service'));
        this.allUIService.set('res_country', () => import('@/uiservice/res-country/res-country-ui-service'));
        this.allUIService.set('product_category', () => import('@/uiservice/product-category/product-category-ui-service'));
        this.allUIService.set('purchase_requisition_line', () => import('@/uiservice/purchase-requisition-line/purchase-requisition-line-ui-service'));
        this.allUIService.set('res_country_state', () => import('@/uiservice/res-country-state/res-country-state-ui-service'));
        this.allUIService.set('res_country_group', () => import('@/uiservice/res-country-group/res-country-group-ui-service'));
        this.allUIService.set('product_supplierinfo', () => import('@/uiservice/product-supplierinfo/product-supplierinfo-ui-service'));
        this.allUIService.set('purchase_order_line', () => import('@/uiservice/purchase-order-line/purchase-order-line-ui-service'));
        this.allUIService.set('mail_tracking_value', () => import('@/uiservice/mail-tracking-value/mail-tracking-value-ui-service'));
        this.allUIService.set('mail_compose_message', () => import('@/uiservice/mail-compose-message/mail-compose-message-ui-service'));
        this.allUIService.set('account_payment_term', () => import('@/uiservice/account-payment-term/account-payment-term-ui-service'));
        this.allUIService.set('product_product', () => import('@/uiservice/product-product/product-product-ui-service'));
        this.allUIService.set('purchase_requisition', () => import('@/uiservice/purchase-requisition/purchase-requisition-ui-service'));
    }

    /**
     * 加载服务实体
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allUIService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const uiServiceRegister: UIServiceRegister = new UIServiceRegister();
import Mail_tracking_valueUIServiceBase from './mail-tracking-value-ui-service-base';

/**
 * 邮件跟踪值UI服务对象
 *
 * @export
 * @class Mail_tracking_valueUIService
 */
export default class Mail_tracking_valueUIService extends Mail_tracking_valueUIServiceBase {

    /**
     * Creates an instance of  Mail_tracking_valueUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_tracking_valueUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 产品模板属性明细行服务对象基类
 *
 * @export
 * @class Product_template_attribute_lineServiceBase
 * @extends {EntityServie}
 */
export default class Product_template_attribute_lineServiceBase extends EntityService {

    /**
     * Creates an instance of  Product_template_attribute_lineServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_template_attribute_lineServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Product_template_attribute_lineServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='product_template_attribute_line';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'product_template_attribute_lines';
        this.APPDETEXT = 'id';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_template_attribute_lines`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/product_template_attribute_lines`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_template_attribute_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_template_attribute_lines/${context.product_template_attribute_line}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/product_template_attribute_lines/${context.product_template_attribute_line}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_template_attribute_line){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_template_attribute_lines/${context.product_template_attribute_line}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/product_template_attribute_lines/${context.product_template_attribute_line}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_template_attribute_line){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_template_attribute_lines/${context.product_template_attribute_line}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/product_template_attribute_lines/${context.product_template_attribute_line}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && true){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_template_attribute_lines/getdraft`,isloading);
            res.data.product_template_attribute_line = data.product_template_attribute_line;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/product_template_attribute_lines/getdraft`,isloading);
        res.data.product_template_attribute_line = data.product_template_attribute_line;
        
        return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_template_attribute_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_template_attribute_lines/${context.product_template_attribute_line}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/product_template_attribute_lines/${context.product_template_attribute_line}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_template_attribute_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_template_attribute_lines/${context.product_template_attribute_line}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/product_template_attribute_lines/${context.product_template_attribute_line}/createbatch`,data,isloading);
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_template_attribute_line){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_template_attribute_lines/${context.product_template_attribute_line}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/product_template_attribute_lines/${context.product_template_attribute_line}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_template_attribute_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_template_attribute_lines/${context.product_template_attribute_line}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/product_template_attribute_lines/${context.product_template_attribute_line}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && context.product_template_attribute_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_template_attribute_lines/${context.product_template_attribute_line}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/product_template_attribute_lines/${context.product_template_attribute_line}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Product_template_attribute_lineServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.product_template && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_templates/${context.product_template}/product_template_attribute_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/product_template_attribute_lines/fetchdefault`,tempData,isloading);
        return res;
    }
}
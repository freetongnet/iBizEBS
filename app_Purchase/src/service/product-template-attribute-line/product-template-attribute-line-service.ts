import { Http } from '@/utils';
import { Util } from '@/utils';
import Product_template_attribute_lineServiceBase from './product-template-attribute-line-service-base';


/**
 * 产品模板属性明细行服务对象
 *
 * @export
 * @class Product_template_attribute_lineService
 * @extends {Product_template_attribute_lineServiceBase}
 */
export default class Product_template_attribute_lineService extends Product_template_attribute_lineServiceBase {

    /**
     * Creates an instance of  Product_template_attribute_lineService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_template_attribute_lineService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
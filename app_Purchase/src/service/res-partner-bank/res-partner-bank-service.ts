import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_partner_bankServiceBase from './res-partner-bank-service-base';


/**
 * 银行账户服务对象
 *
 * @export
 * @class Res_partner_bankService
 * @extends {Res_partner_bankServiceBase}
 */
export default class Res_partner_bankService extends Res_partner_bankServiceBase {

    /**
     * Creates an instance of  Res_partner_bankService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_bankService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
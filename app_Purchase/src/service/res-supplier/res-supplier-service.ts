import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_supplierServiceBase from './res-supplier-service-base';


/**
 * 供应商服务对象
 *
 * @export
 * @class Res_supplierService
 * @extends {Res_supplierServiceBase}
 */
export default class Res_supplierService extends Res_supplierServiceBase {

    /**
     * Creates an instance of  Res_supplierService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_supplierService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http } from '@/utils';
import { Util } from '@/utils';
import Purchase_order_lineServiceBase from './purchase-order-line-service-base';


/**
 * 采购订单行服务对象
 *
 * @export
 * @class Purchase_order_lineService
 * @extends {Purchase_order_lineServiceBase}
 */
export default class Purchase_order_lineService extends Purchase_order_lineServiceBase {

    /**
     * Creates an instance of  Purchase_order_lineService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_order_lineService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
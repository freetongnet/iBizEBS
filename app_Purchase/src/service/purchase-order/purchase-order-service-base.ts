import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 采购订单服务对象基类
 *
 * @export
 * @class Purchase_orderServiceBase
 * @extends {EntityServie}
 */
export default class Purchase_orderServiceBase extends EntityService {

    /**
     * Creates an instance of  Purchase_orderServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_orderServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Purchase_orderServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='purchase_order';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'purchase_orders';
        this.APPDETEXT = 'name';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_order_lines',JSON.stringify(res.data.purchase_order_lines?res.data.purchase_order_lines:[]));
            
            return res;
        }
        if(context.res_supplier && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_order_lines',JSON.stringify(res.data.purchase_order_lines?res.data.purchase_order_lines:[]));
            
            return res;
        }
        if(context.purchase_requisition && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders`,data,isloading);
            this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_order_lines',JSON.stringify(res.data.purchase_order_lines?res.data.purchase_order_lines:[]));
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/purchase_orders`,data,isloading);
        this.tempStorage.setItem(tempContext.srfsessionkey+'_purchase_order_lines',JSON.stringify(res.data.purchase_order_lines?res.data.purchase_order_lines:[]));
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/purchase_orders/${context.purchase_order}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}`,isloading);
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}`,isloading);
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let res:any = Http.getInstance().delete(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/purchase_orders/${context.purchase_order}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}`,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}`,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let res:any = await Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/purchase_orders/${context.purchase_order}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && true){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/getdraft`,isloading);
            res.data.purchase_order = data.purchase_order;
            
            return res;
        }
        if(context.res_supplier && true){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/getdraft`,isloading);
            res.data.purchase_order = data.purchase_order;
            
            return res;
        }
        if(context.purchase_requisition && true){
            let res:any = await Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/getdraft`,isloading);
            res.data.purchase_order = data.purchase_order;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/purchase_orders/getdraft`,isloading);
        res.data.purchase_order = data.purchase_order;
        
        return res;
    }

    /**
     * Button_approve接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Button_approve(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_approve`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/button_approve`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_approve`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_orders/${context.purchase_order}/button_approve`,data,isloading);
            return res;
    }

    /**
     * Button_cancel接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Button_cancel(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_cancel`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/button_cancel`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_cancel`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_orders/${context.purchase_order}/button_cancel`,data,isloading);
            return res;
    }

    /**
     * Button_confirm接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Button_confirm(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_confirm`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/button_confirm`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_confirm`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_orders/${context.purchase_order}/button_confirm`,data,isloading);
            return res;
    }

    /**
     * Button_done接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Button_done(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_done`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/button_done`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_done`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_orders/${context.purchase_order}/button_done`,data,isloading);
            return res;
    }

    /**
     * Button_unlock接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Button_unlock(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_unlock`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/button_unlock`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/button_unlock`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_orders/${context.purchase_order}/button_unlock`,data,isloading);
            return res;
    }

    /**
     * Calc_amount接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Calc_amount(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/calc_amount`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/calc_amount`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/calc_amount`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_orders/${context.purchase_order}/calc_amount`,data,isloading);
            return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_orders/${context.purchase_order}/checkkey`,data,isloading);
            return res;
    }

    /**
     * CreateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async CreateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/createbatch`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/createbatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_orders/${context.purchase_order}/createbatch`,data,isloading);
            return res;
    }

    /**
     * Get_name接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Get_name(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/get_name`,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/get_name`,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let res:any = await Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/get_name`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/purchase_orders/${context.purchase_order}/get_name`,isloading);
            
            return res;
    }

    /**
     * RemoveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async RemoveBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/removebatch`,isloading);
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/removebatch`,isloading);
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let res:any = Http.getInstance().delete(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/removebatch`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/purchase_orders/${context.purchase_order}/removebatch`,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/save`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/save`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/purchase_orders/${context.purchase_order}/save`,data,isloading);
            
            return res;
    }

    /**
     * UpdateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async UpdateBatch(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.res_supplier && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_orders/${context.purchase_order}/updatebatch`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_order){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/${context.purchase_order}/updatebatch`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().put(`/purchase_orders/${context.purchase_order}/updatebatch`,data,isloading);
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.res_supplier && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.purchase_requisition && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/purchase_orders/fetchdefault`,tempData,isloading);
        return res;
    }

    /**
     * FetchMaster接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async FetchMaster(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/fetchmaster`,tempData,isloading);
            return res;
        }
        if(context.res_supplier && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/fetchmaster`,tempData,isloading);
            return res;
        }
        if(context.purchase_requisition && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/fetchmaster`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/purchase_orders/fetchmaster`,tempData,isloading);
        return res;
    }

    /**
     * FetchOrder接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_orderServiceBase
     */
    public async FetchOrder(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_orders/fetchorder`,tempData,isloading);
            return res;
        }
        if(context.res_supplier && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_orders/fetchorder`,tempData,isloading);
            return res;
        }
        if(context.purchase_requisition && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_orders/fetchorder`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/purchase_orders/fetchorder`,tempData,isloading);
        return res;
    }
}
import { Http } from '@/utils';
import { Util } from '@/utils';
import Purchase_orderServiceBase from './purchase-order-service-base';


/**
 * 采购订单服务对象
 *
 * @export
 * @class Purchase_orderService
 * @extends {Purchase_orderServiceBase}
 */
export default class Purchase_orderService extends Purchase_orderServiceBase {

    /**
     * Creates an instance of  Purchase_orderService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_orderService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
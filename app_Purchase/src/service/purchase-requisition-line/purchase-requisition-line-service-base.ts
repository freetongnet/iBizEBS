import { Http } from '@/utils';
import { Util } from '@/utils';
import EntityService from '../entity-service';



/**
 * 采购申请行服务对象基类
 *
 * @export
 * @class Purchase_requisition_lineServiceBase
 * @extends {EntityServie}
 */
export default class Purchase_requisition_lineServiceBase extends EntityService {

    /**
     * Creates an instance of  Purchase_requisition_lineServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisition_lineServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化基础数据
     *
     * @memberof Purchase_requisition_lineServiceBase
     */
    public initBasicData(){
        this.APPLYDEKEY ='purchase_requisition_line';
        this.APPDEKEY = 'id';
        this.APPDENAME = 'purchase_requisition_lines';
        this.APPDETEXT = 'id';
        this.APPNAME = 'purchase';
        this.SYSTEMNAME = 'ibizbusinesscentral';
    }

// 实体接口

    /**
     * Select接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async Select(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_requisition_line){
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/select`,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_requisition_line){
            let res:any = Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/select`,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_requisition_line){
            let res:any = Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/select`,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_requisition_line){
            let res:any = Http.getInstance().get(`/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/select`,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().get(`/purchase_requisition_lines/${context.purchase_requisition_line}/select`,isloading);
            
            return res;
    }

    /**
     * Create接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async Create(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines`,data,isloading);
            
            return res;
        }
        if(context.product_product && true){
            let masterData:any = {};
            Object.assign(data,masterData);
            if(!data.srffrontuf || data.srffrontuf !== "1"){
                data[this.APPDEKEY] = null;
            }
            if(data.srffrontuf){
                delete data.srffrontuf;
            }
            let tempContext:any = JSON.parse(JSON.stringify(context));
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_requisition_lines`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
        if(!data.srffrontuf || data.srffrontuf !== "1"){
            data[this.APPDEKEY] = null;
        }
        if(data.srffrontuf){
            delete data.srffrontuf;
        }
        let tempContext:any = JSON.parse(JSON.stringify(context));
        let res:any = await Http.getInstance().post(`/purchase_requisition_lines`,data,isloading);
        
        return res;
    }

    /**
     * Update接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async Update(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().put(`/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().put(`/purchase_requisition_lines/${context.purchase_requisition_line}`,data,isloading);
            
            return res;
    }

    /**
     * Remove接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async Remove(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_requisition_line){
            let res:any = Http.getInstance().delete(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_requisition_line){
            let res:any = Http.getInstance().delete(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            return res;
        }
        if(context.purchase_requisition && context.purchase_requisition_line){
            let res:any = Http.getInstance().delete(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            return res;
        }
        if(context.product_product && context.purchase_requisition_line){
            let res:any = Http.getInstance().delete(`/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            return res;
        }
            let res:any = Http.getInstance().delete(`/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            return res;
    }

    /**
     * Get接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async Get(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_requisition_line){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_requisition_line){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_requisition_line){
            let res:any = await Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_requisition_line){
            let res:any = await Http.getInstance().get(`/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            
            return res;
        }
            let res:any = await Http.getInstance().get(`/purchase_requisition_lines/${context.purchase_requisition_line}`,isloading);
            
            return res;
    }

    /**
     * GetDraft接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async GetDraft(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && true){
            let res:any = await Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/getdraft`,isloading);
            res.data.purchase_requisition_line = data.purchase_requisition_line;
            
            return res;
        }
        if(context.product_template && context.product_product && true){
            let res:any = await Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/getdraft`,isloading);
            res.data.purchase_requisition_line = data.purchase_requisition_line;
            
            return res;
        }
        if(context.purchase_requisition && true){
            let res:any = await Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/getdraft`,isloading);
            res.data.purchase_requisition_line = data.purchase_requisition_line;
            
            return res;
        }
        if(context.product_product && true){
            let res:any = await Http.getInstance().get(`/product_products/${context.product_product}/purchase_requisition_lines/getdraft`,isloading);
            res.data.purchase_requisition_line = data.purchase_requisition_line;
            
            return res;
        }
        let res:any = await  Http.getInstance().get(`/purchase_requisition_lines/getdraft`,isloading);
        res.data.purchase_requisition_line = data.purchase_requisition_line;
        
        return res;
    }

    /**
     * Calc_price接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async Calc_price(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/calc_price`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/calc_price`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/calc_price`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/calc_price`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisition_lines/${context.purchase_requisition_line}/calc_price`,data,isloading);
            return res;
    }

    /**
     * CheckKey接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async CheckKey(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/checkkey`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/checkkey`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisition_lines/${context.purchase_requisition_line}/checkkey`,data,isloading);
            return res;
    }

    /**
     * Product_change接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async Product_change(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/product_change`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/product_change`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/product_change`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/product_change`,data,isloading);
            
            return res;
        }
            let res:any = Http.getInstance().post(`/purchase_requisition_lines/${context.purchase_requisition_line}/product_change`,data,isloading);
            return res;
    }

    /**
     * Save接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async Save(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/save`,data,isloading);
            
            return res;
        }
        if(context.product_template && context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/save`,data,isloading);
            
            return res;
        }
        if(context.purchase_requisition && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/${context.purchase_requisition_line}/save`,data,isloading);
            
            return res;
        }
        if(context.product_product && context.purchase_requisition_line){
            let masterData:any = {};
            Object.assign(data,masterData);
            let res:any = await Http.getInstance().post(`/product_products/${context.product_product}/purchase_requisition_lines/${context.purchase_requisition_line}/save`,data,isloading);
            
            return res;
        }
        let masterData:any = {};
        Object.assign(data,masterData);
            let res:any = await  Http.getInstance().post(`/purchase_requisition_lines/${context.purchase_requisition_line}/save`,data,isloading);
            
            return res;
    }

    /**
     * FetchDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Purchase_requisition_lineServiceBase
     */
    public async FetchDefault(context: any = {},data: any = {}, isloading?: boolean): Promise<any> {
        if(context.res_supplier && context.purchase_requisition && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/res_suppliers/${context.res_supplier}/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.product_template && context.product_product && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_templates/${context.product_template}/product_products/${context.product_product}/purchase_requisition_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.purchase_requisition && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/purchase_requisitions/${context.purchase_requisition}/purchase_requisition_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        if(context.product_product && true){
            let tempData:any = JSON.parse(JSON.stringify(data));
            let res:any = Http.getInstance().get(`/product_products/${context.product_product}/purchase_requisition_lines/fetchdefault`,tempData,isloading);
            return res;
        }
        let tempData:any = JSON.parse(JSON.stringify(data));
        let res:any = Http.getInstance().get(`/purchase_requisition_lines/fetchdefault`,tempData,isloading);
        return res;
    }
}
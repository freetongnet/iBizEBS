import { Http } from '@/utils';
import { Util } from '@/utils';
import Mail_tracking_valueServiceBase from './mail-tracking-value-service-base';


/**
 * 邮件跟踪值服务对象
 *
 * @export
 * @class Mail_tracking_valueService
 * @extends {Mail_tracking_valueServiceBase}
 */
export default class Mail_tracking_valueService extends Mail_tracking_valueServiceBase {

    /**
     * Creates an instance of  Mail_tracking_valueService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_tracking_valueService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
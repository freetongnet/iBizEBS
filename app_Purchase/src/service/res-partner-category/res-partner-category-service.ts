import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_partner_categoryServiceBase from './res-partner-category-service-base';


/**
 * 业务伙伴标签服务对象
 *
 * @export
 * @class Res_partner_categoryService
 * @extends {Res_partner_categoryServiceBase}
 */
export default class Res_partner_categoryService extends Res_partner_categoryServiceBase {

    /**
     * Creates an instance of  Res_partner_categoryService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_partner_categoryService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
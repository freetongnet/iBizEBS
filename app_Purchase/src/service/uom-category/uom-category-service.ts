import { Http } from '@/utils';
import { Util } from '@/utils';
import Uom_categoryServiceBase from './uom-category-service-base';


/**
 * 产品计量单位 类别服务对象
 *
 * @export
 * @class Uom_categoryService
 * @extends {Uom_categoryServiceBase}
 */
export default class Uom_categoryService extends Uom_categoryServiceBase {

    /**
     * Creates an instance of  Uom_categoryService.
     * 
     * @param {*} [opts={}]
     * @memberof  Uom_categoryService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
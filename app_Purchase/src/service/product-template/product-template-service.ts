import { Http } from '@/utils';
import { Util } from '@/utils';
import Product_templateServiceBase from './product-template-service-base';


/**
 * 产品模板服务对象
 *
 * @export
 * @class Product_templateService
 * @extends {Product_templateServiceBase}
 */
export default class Product_templateService extends Product_templateServiceBase {

    /**
     * Creates an instance of  Product_templateService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_templateService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
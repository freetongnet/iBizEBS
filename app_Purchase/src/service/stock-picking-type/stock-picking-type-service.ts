import { Http } from '@/utils';
import { Util } from '@/utils';
import Stock_picking_typeServiceBase from './stock-picking-type-service-base';


/**
 * 拣货类型服务对象
 *
 * @export
 * @class Stock_picking_typeService
 * @extends {Stock_picking_typeServiceBase}
 */
export default class Stock_picking_typeService extends Stock_picking_typeServiceBase {

    /**
     * Creates an instance of  Stock_picking_typeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Stock_picking_typeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
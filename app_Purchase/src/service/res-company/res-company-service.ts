import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_companyServiceBase from './res-company-service-base';


/**
 * 公司服务对象
 *
 * @export
 * @class Res_companyService
 * @extends {Res_companyServiceBase}
 */
export default class Res_companyService extends Res_companyServiceBase {

    /**
     * Creates an instance of  Res_companyService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_companyService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
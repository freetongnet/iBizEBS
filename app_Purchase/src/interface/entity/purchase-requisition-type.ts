/**
 * 采购申请类型
 *
 * @export
 * @interface Purchase_requisition_type
 */
export interface Purchase_requisition_type {

    /**
     * 申请类型
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    name?: any;

    /**
     * 供应商选择
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    exclusive?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    id?: any;

    /**
     * 设置数量方式
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    quantity_copy?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    write_date?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    create_date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    __last_update?: any;

    /**
     * 明细行
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    line_copy?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    create_uname?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    write_uname?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_requisition_type
     */
    create_uid?: any;
}
/**
 * 业务伙伴标签
 *
 * @export
 * @interface Res_partner_category
 */
export interface Res_partner_category {

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    __last_update?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    color?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    id?: any;

    /**
     * 合作伙伴
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    partner_ids?: any;

    /**
     * 标签名
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    name?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    display_name?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    write_date?: any;

    /**
     * 下级标签
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    child_ids?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    create_date?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    active?: any;

    /**
     * 父级路径
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    parent_path?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    create_uid_text?: any;

    /**
     * 上级类别
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    parent_id_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    write_uid?: any;

    /**
     * 上级类别
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    parent_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_partner_category
     */
    create_uid?: any;
}
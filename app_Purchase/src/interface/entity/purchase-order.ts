/**
 * 采购订单
 *
 * @export
 * @interface Purchase_order
 */
export interface Purchase_order {

    /**
     * 是否要运送
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    is_shipped?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    write_date?: any;

    /**
     * 安全令牌
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    access_token?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    activity_user_id?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_unread?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_channel_ids?: any;

    /**
     * 订单行
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    order_line?: any;

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    activity_date_deadline?: any;

    /**
     * 条款和条件
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    notes?: any;

    /**
     * 总计
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    amount_total?: any;

    /**
     * 下一活动摘要
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    activity_summary?: any;

    /**
     * 税率
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    amount_tax?: any;

    /**
     * 门户访问网址
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    access_url?: any;

    /**
     * 账单数量
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    invoice_count?: any;

    /**
     * 未税金额
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    amount_untaxed?: any;

    /**
     * 接收
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    picking_ids?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    create_date?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_partner_ids?: any;

    /**
     * 访问警告
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    access_warning?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    __last_update?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    state?: any;

    /**
     * 动作编号
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_needaction_counter?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    activity_type_id?: any;

    /**
     * 拣货数
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    picking_count?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_ids?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_unread_counter?: any;

    /**
     * 消息传输错误
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_has_error?: any;

    /**
     * 账单
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    invoice_ids?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    id?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    product_id?: any;

    /**
     * 网站信息
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    website_message_ids?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_has_error_counter?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_is_follower?: any;

    /**
     * 单据日期
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    date_order?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_attachment_count?: any;

    /**
     * 前置操作
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_needaction?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_main_attachment_id?: any;

    /**
     * 目标位置类型
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    default_location_dest_id_usage?: any;

    /**
     * 订单关联
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    name?: any;

    /**
     * 补货组
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    group_id?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    display_name?: any;

    /**
     * 账单状态
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    invoice_status?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    activity_state?: any;

    /**
     * 审批日期
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    date_approve?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    activity_ids?: any;

    /**
     * 供应商参考
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    partner_ref?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    message_follower_ids?: any;

    /**
     * 计划日期
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    date_planned?: any;

    /**
     * 源文档
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    origin?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    user_id_text?: any;

    /**
     * 税科目调整
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    fiscal_position_id_text?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    currency_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    company_id_text?: any;

    /**
     * 代发货地址
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    dest_address_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    write_uid_text?: any;

    /**
     * 采购申请单
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    requisition_id_text?: any;

    /**
     * 付款条款
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    payment_term_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    create_uid_text?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    partner_id_text?: any;

    /**
     * 交货到
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    picking_type_id_text?: any;

    /**
     * 国际贸易术语
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    incoterm_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    create_uid?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    partner_id?: any;

    /**
     * 代发货地址
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    dest_address_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    company_id?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    currency_id?: any;

    /**
     * 采购申请单
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    requisition_id?: any;

    /**
     * 付款条款
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    payment_term_id?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    user_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    write_uid?: any;

    /**
     * 国际贸易术语
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    incoterm_id?: any;

    /**
     * 税科目调整
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    fiscal_position_id?: any;

    /**
     * 交货到
     *
     * @returns {*}
     * @memberof Purchase_order
     */
    picking_type_id?: any;
}
/**
 * 产品计量单位
 *
 * @export
 * @interface Uom_uom
 */
export interface Uom_uom {

    /**
     * 单位
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    name?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    write_date?: any;

    /**
     * 更大比率
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    factor_inv?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    active?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    __last_update?: any;

    /**
     * 舍入精度
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    rounding?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    display_name?: any;

    /**
     * 比例
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    factor?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    id?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    create_date?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    uom_type?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    write_uid_text?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    category_id_text?: any;

    /**
     * 分组销售点中的产品
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    is_pos_groupable?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    create_uid_text?: any;

    /**
     * 计量单位的类别
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    measure_type?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    create_uid?: any;

    /**
     * 类别
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    category_id?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Uom_uom
     */
    write_uid?: any;
}
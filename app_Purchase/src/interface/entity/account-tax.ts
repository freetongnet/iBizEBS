/**
 * 税率
 *
 * @export
 * @interface Account_tax
 */
export interface Account_tax {

    /**
     * ID
     *
     * @returns {*}
     * @memberof Account_tax
     */
    id?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Account_tax
     */
    create_date?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Account_tax
     */
    display_name?: any;

    /**
     * 税范围
     *
     * @returns {*}
     * @memberof Account_tax
     */
    type_tax_use?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Account_tax
     */
    active?: any;

    /**
     * 子级税
     *
     * @returns {*}
     * @memberof Account_tax
     */
    children_tax_ids?: any;

    /**
     * 包含在分析成本
     *
     * @returns {*}
     * @memberof Account_tax
     */
    analytic?: any;

    /**
     * 包含在价格中
     *
     * @returns {*}
     * @memberof Account_tax
     */
    price_include?: any;

    /**
     * 发票上的标签
     *
     * @returns {*}
     * @memberof Account_tax
     */
    description?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Account_tax
     */
    write_date?: any;

    /**
     * 应有税金
     *
     * @returns {*}
     * @memberof Account_tax
     */
    tax_exigibility?: any;

    /**
     * 税率名称
     *
     * @returns {*}
     * @memberof Account_tax
     */
    name?: any;

    /**
     * 标签
     *
     * @returns {*}
     * @memberof Account_tax
     */
    tag_ids?: any;

    /**
     * 税率计算
     *
     * @returns {*}
     * @memberof Account_tax
     */
    amount_type?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Account_tax
     */
    __last_update?: any;

    /**
     * 金额
     *
     * @returns {*}
     * @memberof Account_tax
     */
    amount?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Account_tax
     */
    sequence?: any;

    /**
     * 影响后续税收的基础
     *
     * @returns {*}
     * @memberof Account_tax
     */
    include_base_amount?: any;

    /**
     * 基本税应收科目
     *
     * @returns {*}
     * @memberof Account_tax
     */
    cash_basis_base_account_id_text?: any;

    /**
     * 隐藏现金收付制选项
     *
     * @returns {*}
     * @memberof Account_tax
     */
    hide_tax_exigibility?: any;

    /**
     * 税组
     *
     * @returns {*}
     * @memberof Account_tax
     */
    tax_group_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_tax
     */
    company_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_tax
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_tax
     */
    create_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_tax
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_tax
     */
    write_uid?: any;

    /**
     * 基本税应收科目
     *
     * @returns {*}
     * @memberof Account_tax
     */
    cash_basis_base_account_id?: any;

    /**
     * 税组
     *
     * @returns {*}
     * @memberof Account_tax
     */
    tax_group_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_tax
     */
    company_id?: any;
}
/**
 * 供应商价格表
 *
 * @export
 * @interface Product_supplierinfo
 */
export interface Product_supplierinfo {

    /**
     * 供应商产品代码
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    product_code?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    date_end?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    date_start?: any;

    /**
     * 最少数量
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    min_qty?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    id?: any;

    /**
     * 供应商产品名称
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    product_name?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    sequence?: any;

    /**
     * 交货提前时间
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    delay?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    display_name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    __last_update?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    create_date?: any;

    /**
     * 价格
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    price?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    currency_id_text?: any;

    /**
     * 产品模板
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    product_tmpl_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    company_id_text?: any;

    /**
     * 计量单位
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    product_uom?: any;

    /**
     * 变体计数
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    product_variant_count?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    create_uid_text?: any;

    /**
     * 产品变体
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    product_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    write_uid_text?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    name_text?: any;

    /**
     * 产品模板
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    product_tmpl_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    company_id?: any;

    /**
     * 产品变体
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    product_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    purchase_requisition_line_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    create_uid?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    currency_id?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    name?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_supplierinfo
     */
    write_uid?: any;
}
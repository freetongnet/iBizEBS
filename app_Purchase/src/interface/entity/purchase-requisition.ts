/**
 * 采购申请
 *
 * @export
 * @interface Purchase_requisition
 */
export interface Purchase_requisition {

    /**
     * 申请编号
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    name?: any;

    /**
     * 源单据
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    origin?: any;

    /**
     * 申请截止日期
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    date_end?: any;

    /**
     * 交货日期
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    schedule_date?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    create_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    id?: any;

    /**
     * 订购日期
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    ordering_date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    __last_update?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    state?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    write_date?: any;

    /**
     * 采购订单数量
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    order_count?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    currency_name?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    create_uname?: any;

    /**
     * 设置数量方式
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    quantity_copy?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    company_name?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    user_name?: any;

    /**
     * 申请类型
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    type_id_text?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    vendor_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    write_uname?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    picking_type_id?: any;

    /**
     * 供应商
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    vendor_id?: any;

    /**
     * 申请类型
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    type_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    currency_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    write_uid?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    warehouse_id?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    user_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Purchase_requisition
     */
    company_id?: any;
}
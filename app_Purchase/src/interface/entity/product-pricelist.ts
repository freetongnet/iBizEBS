/**
 * 价格表
 *
 * @export
 * @interface Product_pricelist
 */
export interface Product_pricelist {

    /**
     * 价格表名称
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    name?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    display_name?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    sequence?: any;

    /**
     * 可选
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    selectable?: any;

    /**
     * 折扣政策
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    discount_policy?: any;

    /**
     * 国家组
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    country_group_ids?: any;

    /**
     * 电子商务促销代码
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    code?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    website_id?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    active?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    write_date?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    create_date?: any;

    /**
     * 价格表项目
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    item_ids?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    id?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    __last_update?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    currency_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    create_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    company_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    create_uid?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    write_uid?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    currency_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Product_pricelist
     */
    company_id?: any;
}
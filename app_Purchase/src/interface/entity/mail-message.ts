/**
 * 消息
 *
 * @export
 * @interface Mail_message
 */
export interface Mail_message {

    /**
     * 待处理
     *
     * @returns {*}
     * @memberof Mail_message
     */
    needaction?: any;

    /**
     * 相关的文档模型
     *
     * @returns {*}
     * @memberof Mail_message
     */
    model?: any;

    /**
     * 无响应
     *
     * @returns {*}
     * @memberof Mail_message
     */
    no_auto_thread?: any;

    /**
     * 收件人
     *
     * @returns {*}
     * @memberof Mail_message
     */
    partner_ids?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Mail_message
     */
    create_date?: any;

    /**
     * 消息记录名称
     *
     * @returns {*}
     * @memberof Mail_message
     */
    record_name?: any;

    /**
     * 消息ID
     *
     * @returns {*}
     * @memberof Mail_message
     */
    message_id?: any;

    /**
     * 追踪值
     *
     * @returns {*}
     * @memberof Mail_message
     */
    tracking_value_ids?: any;

    /**
     * 管理状态
     *
     * @returns {*}
     * @memberof Mail_message
     */
    moderation_status?: any;

    /**
     * 通知
     *
     * @returns {*}
     * @memberof Mail_message
     */
    notification_ids?: any;

    /**
     * 从
     *
     * @returns {*}
     * @memberof Mail_message
     */
    email_from?: any;

    /**
     * 渠道
     *
     * @returns {*}
     * @memberof Mail_message
     */
    channel_ids?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Mail_message
     */
    id?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Mail_message
     */
    description?: any;

    /**
     * 加星的邮件
     *
     * @returns {*}
     * @memberof Mail_message
     */
    starred?: any;

    /**
     * 相关评级
     *
     * @returns {*}
     * @memberof Mail_message
     */
    rating_ids?: any;

    /**
     * 待处理的业务伙伴
     *
     * @returns {*}
     * @memberof Mail_message
     */
    needaction_partner_ids?: any;

    /**
     * 回复 至
     *
     * @returns {*}
     * @memberof Mail_message
     */
    reply_to?: any;

    /**
     * 邮件发送服务器
     *
     * @returns {*}
     * @memberof Mail_message
     */
    mail_server_id?: any;

    /**
     * 相关文档编号
     *
     * @returns {*}
     * @memberof Mail_message
     */
    res_id?: any;

    /**
     * 需审核
     *
     * @returns {*}
     * @memberof Mail_message
     */
    need_moderation?: any;

    /**
     * 主题
     *
     * @returns {*}
     * @memberof Mail_message
     */
    subject?: any;

    /**
     * 内容
     *
     * @returns {*}
     * @memberof Mail_message
     */
    body?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Mail_message
     */
    display_name?: any;

    /**
     * 已发布
     *
     * @returns {*}
     * @memberof Mail_message
     */
    website_published?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Mail_message
     */
    attachment_ids?: any;

    /**
     * 下级消息
     *
     * @returns {*}
     * @memberof Mail_message
     */
    child_ids?: any;

    /**
     * 评级值
     *
     * @returns {*}
     * @memberof Mail_message
     */
    rating_value?: any;

    /**
     * 添加签名
     *
     * @returns {*}
     * @memberof Mail_message
     */
    add_sign?: any;

    /**
     * 收藏夹
     *
     * @returns {*}
     * @memberof Mail_message
     */
    starred_partner_ids?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Mail_message
     */
    __last_update?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Mail_message
     */
    write_date?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof Mail_message
     */
    date?: any;

    /**
     * 有误差
     *
     * @returns {*}
     * @memberof Mail_message
     */
    has_error?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Mail_message
     */
    message_type?: any;

    /**
     * 作者
     *
     * @returns {*}
     * @memberof Mail_message
     */
    author_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_message
     */
    create_uid_text?: any;

    /**
     * 邮件活动类型
     *
     * @returns {*}
     * @memberof Mail_message
     */
    mail_activity_type_id_text?: any;

    /**
     * 管理员
     *
     * @returns {*}
     * @memberof Mail_message
     */
    moderator_id_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_message
     */
    write_uid_text?: any;

    /**
     * 子类型
     *
     * @returns {*}
     * @memberof Mail_message
     */
    subtype_id_text?: any;

    /**
     * 作者头像
     *
     * @returns {*}
     * @memberof Mail_message
     */
    author_avatar?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Mail_message
     */
    write_uid?: any;

    /**
     * 邮件活动类型
     *
     * @returns {*}
     * @memberof Mail_message
     */
    mail_activity_type_id?: any;

    /**
     * 管理员
     *
     * @returns {*}
     * @memberof Mail_message
     */
    moderator_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Mail_message
     */
    create_uid?: any;

    /**
     * 作者
     *
     * @returns {*}
     * @memberof Mail_message
     */
    author_id?: any;

    /**
     * 上级消息
     *
     * @returns {*}
     * @memberof Mail_message
     */
    parent_id?: any;

    /**
     * 子类型
     *
     * @returns {*}
     * @memberof Mail_message
     */
    subtype_id?: any;
}
/**
 * 国家/地区群组
 *
 * @export
 * @interface Res_country_group
 */
export interface Res_country_group {

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    write_date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    __last_update?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    id?: any;

    /**
     * 价格表
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    pricelist_ids?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    name?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    display_name?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    create_date?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    country_ids?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    create_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    write_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_country_group
     */
    create_uid?: any;
}
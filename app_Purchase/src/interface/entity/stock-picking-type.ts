/**
 * 拣货类型
 *
 * @export
 * @interface Stock_picking_type
 */
export interface Stock_picking_type {

    /**
     * 显示详细作业
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    show_operations?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    id?: any;

    /**
     * 参考序列
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    sequence_id?: any;

    /**
     * 创建新批次/序列号码
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    use_create_lots?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    create_date?: any;

    /**
     * 条码
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    barcode?: any;

    /**
     * 显示预留
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    show_reserved?: any;

    /**
     * 欠单比率
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    rate_picking_backorders?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    sequence?: any;

    /**
     * 拣货欠单个数
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    count_picking_backorders?: any;

    /**
     * 生产单的数量
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    count_mo_todo?: any;

    /**
     * 颜色
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    color?: any;

    /**
     * 移动整个包裹
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    show_entire_packs?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    display_name?: any;

    /**
     * 生产单等待的数量
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    count_mo_waiting?: any;

    /**
     * 草稿拣货个数
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    count_picking_draft?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    active?: any;

    /**
     * 迟到拣货个数
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    count_picking_late?: any;

    /**
     * 拣货个数
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    count_picking?: any;

    /**
     * 拣货个数准备好
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    count_picking_ready?: any;

    /**
     * 最近10笔完成的拣货
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    last_done_picking?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    __last_update?: any;

    /**
     * 等待拣货个数
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    count_picking_waiting?: any;

    /**
     * 作业的类型
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    code?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    write_date?: any;

    /**
     * 延迟比率
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    rate_picking_late?: any;

    /**
     * 作业类型
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    name?: any;

    /**
     * 生产单数量延迟
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    count_mo_late?: any;

    /**
     * 使用已有批次/序列号码
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    use_existing_lots?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    create_uid_text?: any;

    /**
     * 默认源位置
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    default_location_src_id_text?: any;

    /**
     * 退回的作业类型
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    return_picking_type_id_text?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    warehouse_id_text?: any;

    /**
     * 默认目的位置
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    default_location_dest_id_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    write_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    write_uid?: any;

    /**
     * 默认目的位置
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    default_location_dest_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    create_uid?: any;

    /**
     * 仓库
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    warehouse_id?: any;

    /**
     * 默认源位置
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    default_location_src_id?: any;

    /**
     * 退回的作业类型
     *
     * @returns {*}
     * @memberof Stock_picking_type
     */
    return_picking_type_id?: any;
}
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { MPickupViewBase } from '@/studio-core';
import Account_taxService from '@/service/account-tax/account-tax-service';
import Account_taxAuthService from '@/authservice/account-tax/account-tax-auth-service';
import MPickupViewEngine from '@engine/view/mpickup-view-engine';
import Account_taxUIService from '@/uiservice/account-tax/account-tax-ui-service';

/**
 * 税率视图基类
 *
 * @export
 * @class Account_taxPurchaseMPickupViewBase
 * @extends {MPickupViewBase}
 */
export class Account_taxPurchaseMPickupViewBase extends MPickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    protected appDeName: string = 'account_tax';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Account_taxService}
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    protected appEntityService: Account_taxService = new Account_taxService;

    /**
     * 实体权限服务对象
     *
     * @type Account_taxUIService
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    public appUIService: Account_taxUIService = new Account_taxUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.account_tax.views.purchasempickupview.caption',
        srfTitle: 'entities.account_tax.views.purchasempickupview.title',
        srfSubTitle: 'entities.account_tax.views.purchasempickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchaseMPickupViewBase
     */
	protected viewtag: string = '9cfc393e5fbc8f48d6de3e36d5bdb397';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchaseMPickupViewBase
     */ 
    protected viewName:string = "account_taxPurchaseMPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    public engine: MPickupViewEngine = new MPickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Account_taxPurchaseMPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'account_tax',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Account_taxPurchaseMPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }



    /**
     * 添加左侧面板所有数据到右侧
     *
     * @memberof Account_taxPurchaseMPickupView
     */
    public onCLickAllRight(): void {
        Object.values(this.containerModel).forEach((model: any) => {
            if (!Object.is(model.type, 'PICKUPVIEWPANEL')) {
                return;
            }
            if (model.datas.length > 0) {
                model.datas.forEach((data: any, index: any) => {
                    Object.assign(data, { srfmajortext: data['name'] });
                })
            }
            model.datas.forEach((item: any) => {
                const index: number = this.viewSelections.findIndex((selection: any) => Object.is(item.srfkey, selection.srfkey));
                if (index === -1) {
                    item._select = false;
                    this.viewSelections.push(item);
                }
            });
        });
        this.selectedData = JSON.stringify(this.viewSelections);
    }


}
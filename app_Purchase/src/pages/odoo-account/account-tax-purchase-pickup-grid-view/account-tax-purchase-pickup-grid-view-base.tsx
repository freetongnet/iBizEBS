import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Account_taxService from '@/service/account-tax/account-tax-service';
import Account_taxAuthService from '@/authservice/account-tax/account-tax-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Account_taxUIService from '@/uiservice/account-tax/account-tax-ui-service';

/**
 * 税率选择表格视图视图基类
 *
 * @export
 * @class Account_taxPurchasePickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Account_taxPurchasePickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    protected appDeName: string = 'account_tax';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchasePickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Account_taxService}
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    protected appEntityService: Account_taxService = new Account_taxService;

    /**
     * 实体权限服务对象
     *
     * @type Account_taxUIService
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    public appUIService: Account_taxUIService = new Account_taxUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.account_tax.views.purchasepickupgridview.caption',
        srfTitle: 'entities.account_tax.views.purchasepickupgridview.title',
        srfSubTitle: 'entities.account_tax.views.purchasepickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchasePickupGridViewBase
     */
	protected viewtag: string = '0678faedaa5916c618d1d73a44b13024';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Account_taxPurchasePickupGridViewBase
     */ 
    protected viewName:string = "account_taxPurchasePickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Account_taxPurchasePickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            keyPSDEField: 'account_tax',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Account_taxPurchasePickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
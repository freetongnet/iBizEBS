import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Uom_categoryService from '@/service/uom-category/uom-category-service';
import Uom_categoryAuthService from '@/authservice/uom-category/uom-category-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Uom_categoryUIService from '@/uiservice/uom-category/uom-category-ui-service';

/**
 * 快速新建视图视图基类
 *
 * @export
 * @class Uom_categoryBasicQuickViewBase
 * @extends {OptionViewBase}
 */
export class Uom_categoryBasicQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicQuickViewBase
     */
    protected appDeName: string = 'uom_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicQuickViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Uom_categoryService}
     * @memberof Uom_categoryBasicQuickViewBase
     */
    protected appEntityService: Uom_categoryService = new Uom_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Uom_categoryUIService
     * @memberof Uom_categoryBasicQuickViewBase
     */
    public appUIService: Uom_categoryUIService = new Uom_categoryUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Uom_categoryBasicQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryBasicQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.uom_category.views.basicquickview.caption',
        srfTitle: 'entities.uom_category.views.basicquickview.title',
        srfSubTitle: 'entities.uom_category.views.basicquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryBasicQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicQuickViewBase
     */
	protected viewtag: string = '244f52820f45a1fd04532bf700eb3af6';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicQuickViewBase
     */ 
    protected viewName:string = "uom_categoryBasicQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Uom_categoryBasicQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Uom_categoryBasicQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Uom_categoryBasicQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'uom_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryBasicQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryBasicQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryBasicQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
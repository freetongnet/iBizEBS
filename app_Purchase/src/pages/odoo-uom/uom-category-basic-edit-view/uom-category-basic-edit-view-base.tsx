import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Uom_categoryService from '@/service/uom-category/uom-category-service';
import Uom_categoryAuthService from '@/authservice/uom-category/uom-category-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Uom_categoryUIService from '@/uiservice/uom-category/uom-category-ui-service';

/**
 * 配置信息编辑视图视图基类
 *
 * @export
 * @class Uom_categoryBasicEditViewBase
 * @extends {EditViewBase}
 */
export class Uom_categoryBasicEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicEditViewBase
     */
    protected appDeName: string = 'uom_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicEditViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicEditViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicEditViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Uom_categoryService}
     * @memberof Uom_categoryBasicEditViewBase
     */
    protected appEntityService: Uom_categoryService = new Uom_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Uom_categoryUIService
     * @memberof Uom_categoryBasicEditViewBase
     */
    public appUIService: Uom_categoryUIService = new Uom_categoryUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Uom_categoryBasicEditViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryBasicEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.uom_category.views.basiceditview.caption',
        srfTitle: 'entities.uom_category.views.basiceditview.title',
        srfSubTitle: 'entities.uom_category.views.basiceditview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryBasicEditViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicEditViewBase
     */
	protected viewtag: string = '73ec5ee653ea252842738499c351e806';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicEditViewBase
     */ 
    protected viewName:string = "uom_categoryBasicEditView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Uom_categoryBasicEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Uom_categoryBasicEditViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Uom_categoryBasicEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'uom_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryBasicEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryBasicEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryBasicEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
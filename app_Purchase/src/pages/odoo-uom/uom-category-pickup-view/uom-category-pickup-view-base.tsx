import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Uom_categoryService from '@/service/uom-category/uom-category-service';
import Uom_categoryAuthService from '@/authservice/uom-category/uom-category-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Uom_categoryUIService from '@/uiservice/uom-category/uom-category-ui-service';

/**
 * 产品计量单位 类别数据选择视图视图基类
 *
 * @export
 * @class Uom_categoryPickupViewBase
 * @extends {PickupViewBase}
 */
export class Uom_categoryPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupViewBase
     */
    protected appDeName: string = 'uom_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Uom_categoryService}
     * @memberof Uom_categoryPickupViewBase
     */
    protected appEntityService: Uom_categoryService = new Uom_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Uom_categoryUIService
     * @memberof Uom_categoryPickupViewBase
     */
    public appUIService: Uom_categoryUIService = new Uom_categoryUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.uom_category.views.pickupview.caption',
        srfTitle: 'entities.uom_category.views.pickupview.title',
        srfSubTitle: 'entities.uom_category.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupViewBase
     */
	protected viewtag: string = 'c98d519438489bdaaf82b58443149cc1';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryPickupViewBase
     */ 
    protected viewName:string = "uom_categoryPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Uom_categoryPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Uom_categoryPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Uom_categoryPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'uom_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
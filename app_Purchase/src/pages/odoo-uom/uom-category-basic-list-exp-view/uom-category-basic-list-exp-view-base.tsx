import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { ListExpViewBase } from '@/studio-core';
import Uom_categoryService from '@/service/uom-category/uom-category-service';
import Uom_categoryAuthService from '@/authservice/uom-category/uom-category-auth-service';
import ListExpViewEngine from '@engine/view/list-exp-view-engine';
import Uom_categoryUIService from '@/uiservice/uom-category/uom-category-ui-service';

/**
 * 配置列表导航视图视图基类
 *
 * @export
 * @class Uom_categoryBasicListExpViewBase
 * @extends {ListExpViewBase}
 */
export class Uom_categoryBasicListExpViewBase extends ListExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicListExpViewBase
     */
    protected appDeName: string = 'uom_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicListExpViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicListExpViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Uom_categoryService}
     * @memberof Uom_categoryBasicListExpViewBase
     */
    protected appEntityService: Uom_categoryService = new Uom_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Uom_categoryUIService
     * @memberof Uom_categoryBasicListExpViewBase
     */
    public appUIService: Uom_categoryUIService = new Uom_categoryUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Uom_categoryBasicListExpViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryBasicListExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.uom_category.views.basiclistexpview.caption',
        srfTitle: 'entities.uom_category.views.basiclistexpview.title',
        srfSubTitle: 'entities.uom_category.views.basiclistexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Uom_categoryBasicListExpViewBase
     */
    protected containerModel: any = {
        view_listexpbar: { name: 'listexpbar', type: 'LISTEXPBAR' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Uom_categoryBasicListExpView
     */
    public basiclistexpviewlistexpbar_toolbarModels: any = {
        tbitem3: { name: 'tbitem3', caption: '新建', 'isShowCaption': true, 'isShowIcon': true, tooltip: '新建', iconcls: 'fa fa-file-text-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'New', target: '', class: '' } },

        tbitem7: {  name: 'tbitem7', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem8: { name: 'tbitem8', caption: '删除', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'MULTIKEY', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicListExpViewBase
     */
	protected viewtag: string = 'f76810e24d59572d6248acabba384f15';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicListExpViewBase
     */ 
    protected viewName:string = "uom_categoryBasicListExpView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Uom_categoryBasicListExpViewBase
     */
    public engine: ListExpViewEngine = new ListExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Uom_categoryBasicListExpViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Uom_categoryBasicListExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            listexpbar: this.$refs.listexpbar,
            keyPSDEField: 'uom_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * listexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryBasicListExpViewBase
     */
    public listexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'selectionchange', $event);
    }

    /**
     * listexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryBasicListExpViewBase
     */
    public listexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'activated', $event);
    }

    /**
     * listexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_categoryBasicListExpViewBase
     */
    public listexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Uom_categoryBasicListExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.uom_category;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'uom_categories', parameterName: 'uom_category' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'uom-category-basic-quick-view', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.uom_category.views.basicquickview.title'),
            placement: 'DRAWER_RIGHT',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Uom_categoryBasicListExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }




    /**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Uom_categoryBasicListExpView
     */
    protected viewUID: string = 'odoo-uom-uom-category-basic-list-exp-view';


}
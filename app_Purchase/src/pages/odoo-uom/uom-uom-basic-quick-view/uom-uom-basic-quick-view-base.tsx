import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Uom_uomService from '@/service/uom-uom/uom-uom-service';
import Uom_uomAuthService from '@/authservice/uom-uom/uom-uom-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Uom_uomUIService from '@/uiservice/uom-uom/uom-uom-ui-service';

/**
 * 快速新建视图视图基类
 *
 * @export
 * @class Uom_uomBasicQuickViewBase
 * @extends {OptionViewBase}
 */
export class Uom_uomBasicQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicQuickViewBase
     */
    protected appDeName: string = 'uom_uom';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicQuickViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Uom_uomService}
     * @memberof Uom_uomBasicQuickViewBase
     */
    protected appEntityService: Uom_uomService = new Uom_uomService;

    /**
     * 实体权限服务对象
     *
     * @type Uom_uomUIService
     * @memberof Uom_uomBasicQuickViewBase
     */
    public appUIService: Uom_uomUIService = new Uom_uomUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Uom_uomBasicQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Uom_uomBasicQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.uom_uom.views.basicquickview.caption',
        srfTitle: 'entities.uom_uom.views.basicquickview.title',
        srfSubTitle: 'entities.uom_uom.views.basicquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Uom_uomBasicQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicQuickViewBase
     */
	protected viewtag: string = '7d4c11699bc484f1477d6c640fbd63cf';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Uom_uomBasicQuickViewBase
     */ 
    protected viewName:string = "uom_uomBasicQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Uom_uomBasicQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Uom_uomBasicQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Uom_uomBasicQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'uom_uom',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_uomBasicQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_uomBasicQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Uom_uomBasicQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
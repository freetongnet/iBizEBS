import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import Res_supplierService from '@/service/res-supplier/res-supplier-service';
import Res_supplierAuthService from '@/authservice/res-supplier/res-supplier-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import DataPanelEngine from '@engine/ctrl/data-panel-engine';
import Res_supplierUIService from '@/uiservice/res-supplier/res-supplier-ui-service';

/**
 * 主信息总览视图视图基类
 *
 * @export
 * @class Res_supplierMasterTabInfoViewBase
 * @extends {TabExpViewBase}
 */
export class Res_supplierMasterTabInfoViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    protected appDeName: string = 'res_supplier';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Res_supplierService}
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    protected appEntityService: Res_supplierService = new Res_supplierService;

    /**
     * 实体权限服务对象
     *
     * @type Res_supplierUIService
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    public appUIService: Res_supplierUIService = new Res_supplierUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_supplier.views.mastertabinfoview.caption',
        srfTitle: 'entities.res_supplier.views.mastertabinfoview.title',
        srfSubTitle: 'entities.res_supplier.views.mastertabinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_datapanel: { name: 'datapanel', type: 'FORM' },
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Res_supplierMasterTabInfoView
     */
    public toolBarModels: any = {
        tbitem1_masteredit: { name: 'tbitem1_masteredit', caption: '编辑', 'isShowCaption': true, 'isShowIcon': true, tooltip: '编辑', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'MasterEdit', target: 'SINGLEKEY', class: '' } },

        tbitem1_masterremove_sep: {  name: 'tbitem1_masterremove_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_masterremove: { name: 'tbitem1_masterremove', caption: '删除并关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除并关闭', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'MasterRemove', target: 'SINGLEKEY', class: '' } },

        tbitem2: {  name: 'tbitem2', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterTabInfoViewBase
     */
	protected viewtag: string = '0a68f3cf7fc39261aec28734fcaec309';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterTabInfoViewBase
     */ 
    protected viewName:string = "res_supplierMasterTabInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    public datapanel: DataPanelEngine = new DataPanelEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_supplierMasterTabInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'res_supplier',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
        this.datapanel.init({
            view: this,
            datapanel: this.$refs.datapanel,
            keyPSDEField: 'res_supplier',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1_masteredit')) {
            this.toolbar_tbitem1_masteredit_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_masterremove')) {
            this.toolbar_tbitem1_masterremove_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem9')) {
            this.toolbar_tbitem9_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_masteredit_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Res_supplierUIService  = new Res_supplierUIService();
        curUIService.Res_supplier_MasterEdit(datas,contextJO, paramJO,  $event, xData,this,"Res_supplier");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_masterremove_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Res_supplierUIService  = new Res_supplierUIService();
        curUIService.Res_supplier_MasterRemove(datas,contextJO, paramJO,  $event, xData,this,"Res_supplier");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem9_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.SaveAndStart(datas, contextJO,paramJO,  $event, xData,this,"Res_supplier");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"Res_supplier");
    }

    /**
     * 开始流程
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    public SaveAndStart(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        const _this: any = this;
        if (!xData || !(xData.wfstart instanceof Function)) {
            return;
        }
        xData.wfstart(args).then((response: any) => {
            if (!response || response.status !== 200) {
                return;
            }
            const { data: _data } = response;
            if(window.parent){
                window.parent.postMessage({ ..._data },'*');
            }
            if (_this.viewdata) {
                _this.$emit('viewdataschange', [{ ..._data }]);
                _this.$emit('close');
            }else if (this.$tabPageExp) {
                this.$tabPageExp.onClose(this.$route.fullPath);
            }
        });
    }
    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Res_supplierMasterTabInfoViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import Res_supplierService from '@/service/res-supplier/res-supplier-service';
import Res_supplierAuthService from '@/authservice/res-supplier/res-supplier-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import Res_supplierUIService from '@/uiservice/res-supplier/res-supplier-ui-service';

/**
 * 供应商数据选择视图视图基类
 *
 * @export
 * @class Res_supplierPickupViewBase
 * @extends {PickupViewBase}
 */
export class Res_supplierPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierPickupViewBase
     */
    protected appDeName: string = 'res_supplier';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierPickupViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierPickupViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Res_supplierService}
     * @memberof Res_supplierPickupViewBase
     */
    protected appEntityService: Res_supplierService = new Res_supplierService;

    /**
     * 实体权限服务对象
     *
     * @type Res_supplierUIService
     * @memberof Res_supplierPickupViewBase
     */
    public appUIService: Res_supplierUIService = new Res_supplierUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_supplierPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_supplier.views.pickupview.caption',
        srfTitle: 'entities.res_supplier.views.pickupview.title',
        srfSubTitle: 'entities.res_supplier.views.pickupview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_supplierPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: { name: 'pickupviewpanel', type: 'PICKUPVIEWPANEL' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierPickupViewBase
     */
	protected viewtag: string = 'f65c8430fe2c75de5d2abdb244453ef7';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierPickupViewBase
     */ 
    protected viewName:string = "res_supplierPickupView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_supplierPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_supplierPickupViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_supplierPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'res_supplier',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}
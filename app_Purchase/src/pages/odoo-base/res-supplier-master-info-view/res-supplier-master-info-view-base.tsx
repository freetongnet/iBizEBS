import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Res_supplierService from '@/service/res-supplier/res-supplier-service';
import Res_supplierAuthService from '@/authservice/res-supplier/res-supplier-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Res_supplierUIService from '@/uiservice/res-supplier/res-supplier-ui-service';

/**
 * 主信息概览视图基类
 *
 * @export
 * @class Res_supplierMasterInfoViewBase
 * @extends {EditViewBase}
 */
export class Res_supplierMasterInfoViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterInfoViewBase
     */
    protected appDeName: string = 'res_supplier';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterInfoViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Res_supplierService}
     * @memberof Res_supplierMasterInfoViewBase
     */
    protected appEntityService: Res_supplierService = new Res_supplierService;

    /**
     * 实体权限服务对象
     *
     * @type Res_supplierUIService
     * @memberof Res_supplierMasterInfoViewBase
     */
    public appUIService: Res_supplierUIService = new Res_supplierUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Res_supplierMasterInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_supplierMasterInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_supplier.views.masterinfoview.caption',
        srfTitle: 'entities.res_supplier.views.masterinfoview.title',
        srfSubTitle: 'entities.res_supplier.views.masterinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_supplierMasterInfoViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterInfoViewBase
     */
	protected viewtag: string = '273bd8803959ff9357b9c2479037352c';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_supplierMasterInfoViewBase
     */ 
    protected viewName:string = "res_supplierMasterInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_supplierMasterInfoViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_supplierMasterInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_supplierMasterInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'res_supplier',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierMasterInfoViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierMasterInfoViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_supplierMasterInfoViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
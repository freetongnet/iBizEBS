import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Res_partner_categoryService from '@/service/res-partner-category/res-partner-category-service';
import Res_partner_categoryAuthService from '@/authservice/res-partner-category/res-partner-category-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Res_partner_categoryUIService from '@/uiservice/res-partner-category/res-partner-category-ui-service';

/**
 * 业务伙伴标签选择表格视图视图基类
 *
 * @export
 * @class Res_partner_categoryPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Res_partner_categoryPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    protected appDeName: string = 'res_partner_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryPickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Res_partner_categoryService}
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    protected appEntityService: Res_partner_categoryService = new Res_partner_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Res_partner_categoryUIService
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public appUIService: Res_partner_categoryUIService = new Res_partner_categoryUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.res_partner_category.views.pickupgridview.caption',
        srfTitle: 'entities.res_partner_category.views.pickupgridview.title',
        srfSubTitle: 'entities.res_partner_category.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryPickupGridViewBase
     */
	protected viewtag: string = '0bb629da79eaf7ac10841ccb2737e094';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Res_partner_categoryPickupGridViewBase
     */ 
    protected viewName:string = "res_partner_categoryPickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Res_partner_categoryPickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'res_partner_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Res_partner_categoryPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import Purchase_requisitionService from '@/service/purchase-requisition/purchase-requisition-service';
import Purchase_requisitionAuthService from '@/authservice/purchase-requisition/purchase-requisition-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import DataPanelEngine from '@engine/ctrl/data-panel-engine';
import Purchase_requisitionUIService from '@/uiservice/purchase-requisition/purchase-requisition-ui-service';

/**
 * 主信息总览视图视图基类
 *
 * @export
 * @class Purchase_requisitionMasterTabInfoViewBase
 * @extends {TabExpViewBase}
 */
export class Purchase_requisitionMasterTabInfoViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    protected appDeName: string = 'purchase_requisition';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisitionService}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    protected appEntityService: Purchase_requisitionService = new Purchase_requisitionService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_requisitionUIService
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    public appUIService: Purchase_requisitionUIService = new Purchase_requisitionUIService(this.$store);

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof Purchase_requisitionMasterTabInfoViewBase
	 */
    protected customViewNavContexts: any = {
        'N_RES_MODEL_EQ': { isRawValue: true, value: 'purchase.requisition' },
        'N_RES_ID_EQ': { isRawValue: false, value: 'purchase_requisition' }
    };

    /**
     * 是否显示信息栏
     *
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_requisition.views.mastertabinfoview.caption',
        srfTitle: 'entities.purchase_requisition.views.mastertabinfoview.title',
        srfSubTitle: 'entities.purchase_requisition.views.mastertabinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_datapanel: { name: 'datapanel', type: 'FORM' },
        view_statewizardpanel: { name: 'statewizardpanel', type: 'STATEWIZARDPANEL' },
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Purchase_requisitionMasterTabInfoView
     */
    public toolBarModels: any = {
        tbitem1_masteredit: { name: 'tbitem1_masteredit', caption: '编辑', 'isShowCaption': true, 'isShowIcon': true, tooltip: '编辑', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'MasterEdit', target: 'SINGLEKEY', class: '' } },

        tbitem1_masterremove_sep: {  name: 'tbitem1_masterremove_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_masterremove: { name: 'tbitem1_masterremove', caption: '删除并关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除并关闭', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'MasterRemove', target: 'SINGLEKEY', class: '' } },

        tbitem1_action_new_order_sep: {  name: 'tbitem1_action_new_order_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_action_new_order: { name: 'tbitem1_action_new_order', caption: '新采购报价', 'isShowCaption': true, 'isShowIcon': true, tooltip: '新采购报价', iconcls: 'fa fa-cog', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'action_new_order', target: 'SINGLEKEY', class: '' } },

        tbitem1_action_open_sep: {  name: 'tbitem1_action_open_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_action_open: { name: 'tbitem1_action_open', caption: '验证', 'isShowCaption': true, 'isShowIcon': true, tooltip: '验证', iconcls: 'fa fa-cog', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'action_open', target: 'SINGLEKEY', class: '' } },

        tbitem1_action_in_progress: { name: 'tbitem1_action_in_progress', caption: '确认', 'isShowCaption': true, 'isShowIcon': true, tooltip: '确认', iconcls: 'fa fa-cog', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'action_in_progress', target: 'SINGLEKEY', class: '' } },

        tbitem1_action_done_sep: {  name: 'tbitem1_action_done_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_action_done: { name: 'tbitem1_action_done', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-cog', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'action_done', target: 'SINGLEKEY', class: '' } },

        tbitem1_action_cancel_sep: {  name: 'tbitem1_action_cancel_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_action_cancel: { name: 'tbitem1_action_cancel', caption: '取消', 'isShowCaption': true, 'isShowIcon': true, tooltip: '取消', iconcls: 'fa fa-cog', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'action_cancel', target: 'SINGLEKEY', class: '' } },

        tbitem1_action_draft_sep: {  name: 'tbitem1_action_draft_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_action_draft: { name: 'tbitem1_action_draft', caption: '重置为草稿', 'isShowCaption': true, 'isShowIcon': true, tooltip: '重置为草稿', iconcls: 'fa fa-cog', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'action_draft', target: 'SINGLEKEY', class: '' } },

        tbitem2: {  name: 'tbitem2', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
	protected viewtag: string = '999794b38995fb8355f30590b92500b1';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */ 
    protected viewName:string = "purchase_requisitionMasterTabInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    public datapanel: DataPanelEngine = new DataPanelEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'purchase_requisition',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
        this.datapanel.init({
            view: this,
            datapanel: this.$refs.datapanel,
            keyPSDEField: 'purchase_requisition',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1_masteredit')) {
            this.toolbar_tbitem1_masteredit_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_masterremove')) {
            this.toolbar_tbitem1_masterremove_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_action_new_order')) {
            this.toolbar_tbitem1_action_new_order_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_action_open')) {
            this.toolbar_tbitem1_action_open_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_action_in_progress')) {
            this.toolbar_tbitem1_action_in_progress_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_action_done')) {
            this.toolbar_tbitem1_action_done_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_action_cancel')) {
            this.toolbar_tbitem1_action_cancel_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_action_draft')) {
            this.toolbar_tbitem1_action_draft_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem9')) {
            this.toolbar_tbitem9_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_masteredit_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_requisitionUIService  = new Purchase_requisitionUIService();
        curUIService.Purchase_requisition_MasterEdit(datas,contextJO, paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_masterremove_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_requisitionUIService  = new Purchase_requisitionUIService();
        curUIService.Purchase_requisition_MasterRemove(datas,contextJO, paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_action_new_order_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_requisitionUIService  = new Purchase_requisitionUIService();
        curUIService.Purchase_requisition_action_new_order(datas,contextJO, paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_action_open_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_requisitionUIService  = new Purchase_requisitionUIService();
        curUIService.Purchase_requisition_action_open(datas,contextJO, paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_action_in_progress_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_requisitionUIService  = new Purchase_requisitionUIService();
        curUIService.Purchase_requisition_action_in_progress(datas,contextJO, paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_action_done_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_requisitionUIService  = new Purchase_requisitionUIService();
        curUIService.Purchase_requisition_action_done(datas,contextJO, paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_action_cancel_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_requisitionUIService  = new Purchase_requisitionUIService();
        curUIService.Purchase_requisition_action_cancel(datas,contextJO, paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_action_draft_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_requisitionUIService  = new Purchase_requisitionUIService();
        curUIService.Purchase_requisition_action_draft(datas,contextJO, paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem9_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.SaveAndStart(datas, contextJO,paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"Purchase_requisition");
    }

    /**
     * 开始流程
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    public SaveAndStart(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        const _this: any = this;
        if (!xData || !(xData.wfstart instanceof Function)) {
            return;
        }
        xData.wfstart(args).then((response: any) => {
            if (!response || response.status !== 200) {
                return;
            }
            const { data: _data } = response;
            if(window.parent){
                window.parent.postMessage({ ..._data },'*');
            }
            if (_this.viewdata) {
                _this.$emit('viewdataschange', [{ ..._data }]);
                _this.$emit('close');
            }else if (this.$tabPageExp) {
                this.$tabPageExp.onClose(this.$route.fullPath);
            }
        });
    }
    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Purchase_requisitionMasterTabInfoViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}
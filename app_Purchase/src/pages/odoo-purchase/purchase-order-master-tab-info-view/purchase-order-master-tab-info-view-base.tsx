import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import Purchase_orderService from '@/service/purchase-order/purchase-order-service';
import Purchase_orderAuthService from '@/authservice/purchase-order/purchase-order-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import DataPanelEngine from '@engine/ctrl/data-panel-engine';
import Purchase_orderUIService from '@/uiservice/purchase-order/purchase-order-ui-service';

/**
 * 主信息总览视图视图基类
 *
 * @export
 * @class Purchase_orderMasterTabInfoViewBase
 * @extends {TabExpViewBase}
 */
export class Purchase_orderMasterTabInfoViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    protected appDeName: string = 'purchase_order';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Purchase_orderService}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    protected appEntityService: Purchase_orderService = new Purchase_orderService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_orderUIService
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    public appUIService: Purchase_orderUIService = new Purchase_orderUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_order.views.mastertabinfoview.caption',
        srfTitle: 'entities.purchase_order.views.mastertabinfoview.title',
        srfSubTitle: 'entities.purchase_order.views.mastertabinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    protected containerModel: any = {
        view_datapanel: { name: 'datapanel', type: 'FORM' },
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Purchase_orderMasterTabInfoView
     */
    public toolBarModels: any = {
        tbitem1_masteredit: { name: 'tbitem1_masteredit', caption: '编辑', 'isShowCaption': true, 'isShowIcon': true, tooltip: '编辑', iconcls: 'fa fa-edit', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'MasterEdit', target: 'SINGLEKEY', class: '' } },

        tbitem1_masterremove_sep: {  name: 'tbitem1_masterremove_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_masterremove: { name: 'tbitem1_masterremove', caption: '删除并关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除并关闭', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'MasterRemove', target: 'SINGLEKEY', class: '' } },

        tbitem1_button_confirm_sep: {  name: 'tbitem1_button_confirm_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_button_confirm: { name: 'tbitem1_button_confirm', caption: '确认订单', 'isShowCaption': true, 'isShowIcon': true, tooltip: '确认订单', iconcls: 'fa fa-cog', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'button_confirm', target: 'SINGLEKEY', class: '' } },

        tbitem1_button_unlock_sep: {  name: 'tbitem1_button_unlock_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_button_unlock: { name: 'tbitem1_button_unlock', caption: '解锁', 'isShowCaption': true, 'isShowIcon': true, tooltip: '解锁', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'button_unlock', target: 'SINGLEKEY', class: '' } },

        tbitem1_button_done_sep: {  name: 'tbitem1_button_done_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_button_done: { name: 'tbitem1_button_done', caption: '锁定', 'isShowCaption': true, 'isShowIcon': true, tooltip: '锁定', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'button_done', target: 'SINGLEKEY', class: '' } },

        tbitem1_button_cancel_sep: {  name: 'tbitem1_button_cancel_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_button_cancel: { name: 'tbitem1_button_cancel', caption: '取消', 'isShowCaption': true, 'isShowIcon': true, tooltip: '取消', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'button_cancel', target: 'SINGLEKEY', class: '' } },

        tbitem2: {  name: 'tbitem2', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
	protected viewtag: string = '474a86d5a3c0c3907c16dc3fdfcabfb8';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */ 
    protected viewName:string = "purchase_orderMasterTabInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();

    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    public datapanel: DataPanelEngine = new DataPanelEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_orderMasterTabInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'purchase_order',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
        this.datapanel.init({
            view: this,
            datapanel: this.$refs.datapanel,
            keyPSDEField: 'purchase_order',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1_masteredit')) {
            this.toolbar_tbitem1_masteredit_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_masterremove')) {
            this.toolbar_tbitem1_masterremove_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_button_confirm')) {
            this.toolbar_tbitem1_button_confirm_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_button_unlock')) {
            this.toolbar_tbitem1_button_unlock_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_button_done')) {
            this.toolbar_tbitem1_button_done_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_button_cancel')) {
            this.toolbar_tbitem1_button_cancel_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem9')) {
            this.toolbar_tbitem9_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_masteredit_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_orderUIService  = new Purchase_orderUIService();
        curUIService.Purchase_order_MasterEdit(datas,contextJO, paramJO,  $event, xData,this,"Purchase_order");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_masterremove_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_orderUIService  = new Purchase_orderUIService();
        curUIService.Purchase_order_MasterRemove(datas,contextJO, paramJO,  $event, xData,this,"Purchase_order");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_button_confirm_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_orderUIService  = new Purchase_orderUIService();
        curUIService.Purchase_order_button_confirm(datas,contextJO, paramJO,  $event, xData,this,"Purchase_order");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_button_unlock_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_orderUIService  = new Purchase_orderUIService();
        curUIService.Purchase_order_button_unlock(datas,contextJO, paramJO,  $event, xData,this,"Purchase_order");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_button_done_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_orderUIService  = new Purchase_orderUIService();
        curUIService.Purchase_order_button_done(datas,contextJO, paramJO,  $event, xData,this,"Purchase_order");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_button_cancel_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Purchase_orderUIService  = new Purchase_orderUIService();
        curUIService.Purchase_order_button_cancel(datas,contextJO, paramJO,  $event, xData,this,"Purchase_order");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem9_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.SaveAndStart(datas, contextJO,paramJO,  $event, xData,this,"Purchase_order");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"Purchase_order");
    }

    /**
     * 开始流程
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    public SaveAndStart(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        const _this: any = this;
        if (!xData || !(xData.wfstart instanceof Function)) {
            return;
        }
        xData.wfstart(args).then((response: any) => {
            if (!response || response.status !== 200) {
                return;
            }
            const { data: _data } = response;
            if(window.parent){
                window.parent.postMessage({ ..._data },'*');
            }
            if (_this.viewdata) {
                _this.$emit('viewdataschange', [{ ..._data }]);
                _this.$emit('close');
            }else if (this.$tabPageExp) {
                this.$tabPageExp.onClose(this.$route.fullPath);
            }
        });
    }
    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Purchase_orderMasterTabInfoViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Purchase_requisitionService from '@/service/purchase-requisition/purchase-requisition-service';
import Purchase_requisitionAuthService from '@/authservice/purchase-requisition/purchase-requisition-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Purchase_requisitionUIService from '@/uiservice/purchase-requisition/purchase-requisition-ui-service';

/**
 * 主信息概览视图基类
 *
 * @export
 * @class Purchase_requisitionMasterInfoViewBase
 * @extends {EditViewBase}
 */
export class Purchase_requisitionMasterInfoViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    protected appDeName: string = 'purchase_requisition';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Purchase_requisitionService}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    protected appEntityService: Purchase_requisitionService = new Purchase_requisitionService;

    /**
     * 实体权限服务对象
     *
     * @type Purchase_requisitionUIService
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    public appUIService: Purchase_requisitionUIService = new Purchase_requisitionUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.purchase_requisition.views.masterinfoview.caption',
        srfTitle: 'entities.purchase_requisition.views.masterinfoview.title',
        srfSubTitle: 'entities.purchase_requisition.views.masterinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
	protected viewtag: string = 'ed8d6abde7ba9daaccdfa5049373df37';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */ 
    protected viewName:string = "purchase_requisitionMasterInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Purchase_requisitionMasterInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'purchase_requisition',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Purchase_requisitionMasterInfoViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
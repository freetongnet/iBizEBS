import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { DashboardViewBase } from '@/studio-core';
import Product_templateService from '@/service/product-template/product-template-service';
import Product_templateAuthService from '@/authservice/product-template/product-template-auth-service';
import PortalViewEngine from '@engine/view/portal-view-engine';
import Product_templateUIService from '@/uiservice/product-template/product-template-ui-service';

/**
 * 概览视图基类
 *
 * @export
 * @class Product_templateMasterSummaryViewBase
 * @extends {DashboardViewBase}
 */
export class Product_templateMasterSummaryViewBase extends DashboardViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterSummaryViewBase
     */
    protected appDeName: string = 'product_template';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterSummaryViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterSummaryViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Product_templateService}
     * @memberof Product_templateMasterSummaryViewBase
     */
    protected appEntityService: Product_templateService = new Product_templateService;

    /**
     * 实体权限服务对象
     *
     * @type Product_templateUIService
     * @memberof Product_templateMasterSummaryViewBase
     */
    public appUIService: Product_templateUIService = new Product_templateUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_templateMasterSummaryViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_templateMasterSummaryViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_template.views.mastersummaryview.caption',
        srfTitle: 'entities.product_template.views.mastersummaryview.title',
        srfSubTitle: 'entities.product_template.views.mastersummaryview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_templateMasterSummaryViewBase
     */
    protected containerModel: any = {
        view_dashboard: { name: 'dashboard', type: 'DASHBOARD' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterSummaryViewBase
     */
	protected viewtag: string = '44a7d256aebf043cc67d2e1b956d61eb';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_templateMasterSummaryViewBase
     */ 
    protected viewName:string = "product_templateMasterSummaryView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_templateMasterSummaryViewBase
     */
    public engine: PortalViewEngine = new PortalViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_templateMasterSummaryViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_templateMasterSummaryViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            dashboard: this.$refs.dashboard,
            keyPSDEField: 'product_template',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * dashboard 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_templateMasterSummaryViewBase
     */
    public dashboard_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('dashboard', 'load', $event);
    }


}
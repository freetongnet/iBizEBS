import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { ListExpViewBase } from '@/studio-core';
import Product_categoryService from '@/service/product-category/product-category-service';
import Product_categoryAuthService from '@/authservice/product-category/product-category-auth-service';
import ListExpViewEngine from '@engine/view/list-exp-view-engine';
import Product_categoryUIService from '@/uiservice/product-category/product-category-ui-service';

/**
 * 配置列表导航视图视图基类
 *
 * @export
 * @class Product_categoryBasicListExpViewBase
 * @extends {ListExpViewBase}
 */
export class Product_categoryBasicListExpViewBase extends ListExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicListExpViewBase
     */
    protected appDeName: string = 'product_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicListExpViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicListExpViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Product_categoryService}
     * @memberof Product_categoryBasicListExpViewBase
     */
    protected appEntityService: Product_categoryService = new Product_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Product_categoryUIService
     * @memberof Product_categoryBasicListExpViewBase
     */
    public appUIService: Product_categoryUIService = new Product_categoryUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_categoryBasicListExpViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_categoryBasicListExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_category.views.basiclistexpview.caption',
        srfTitle: 'entities.product_category.views.basiclistexpview.title',
        srfSubTitle: 'entities.product_category.views.basiclistexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_categoryBasicListExpViewBase
     */
    protected containerModel: any = {
        view_listexpbar: { name: 'listexpbar', type: 'LISTEXPBAR' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Product_categoryBasicListExpView
     */
    public basiclistexpviewlistexpbar_toolbarModels: any = {
        tbitem3: { name: 'tbitem3', caption: '新建', 'isShowCaption': true, 'isShowIcon': true, tooltip: '新建', iconcls: 'fa fa-file-text-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'New', target: '', class: '' } },

        tbitem7: {  name: 'tbitem7', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem8: { name: 'tbitem8', caption: '删除', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'MULTIKEY', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicListExpViewBase
     */
	protected viewtag: string = '8edd23ee9ade06059e61231de1194c86';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicListExpViewBase
     */ 
    protected viewName:string = "product_categoryBasicListExpView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_categoryBasicListExpViewBase
     */
    public engine: ListExpViewEngine = new ListExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_categoryBasicListExpViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_categoryBasicListExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            listexpbar: this.$refs.listexpbar,
            keyPSDEField: 'product_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * listexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_categoryBasicListExpViewBase
     */
    public listexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'selectionchange', $event);
    }

    /**
     * listexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_categoryBasicListExpViewBase
     */
    public listexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'activated', $event);
    }

    /**
     * listexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_categoryBasicListExpViewBase
     */
    public listexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Product_categoryBasicListExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.product_category;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'product_categories', parameterName: 'product_category' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'product-category-basic-quick-view', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.product_category.views.basicquickview.title'),
            placement: 'DRAWER_RIGHT',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Product_categoryBasicListExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }




    /**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicListExpView
     */
    protected viewUID: string = 'odoo-product-product-category-basic-list-exp-view';


}
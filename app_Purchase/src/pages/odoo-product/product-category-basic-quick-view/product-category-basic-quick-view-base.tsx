import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Product_categoryService from '@/service/product-category/product-category-service';
import Product_categoryAuthService from '@/authservice/product-category/product-category-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Product_categoryUIService from '@/uiservice/product-category/product-category-ui-service';

/**
 * 快速新建视图视图基类
 *
 * @export
 * @class Product_categoryBasicQuickViewBase
 * @extends {OptionViewBase}
 */
export class Product_categoryBasicQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicQuickViewBase
     */
    protected appDeName: string = 'product_category';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicQuickViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Product_categoryService}
     * @memberof Product_categoryBasicQuickViewBase
     */
    protected appEntityService: Product_categoryService = new Product_categoryService;

    /**
     * 实体权限服务对象
     *
     * @type Product_categoryUIService
     * @memberof Product_categoryBasicQuickViewBase
     */
    public appUIService: Product_categoryUIService = new Product_categoryUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Product_categoryBasicQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Product_categoryBasicQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.product_category.views.basicquickview.caption',
        srfTitle: 'entities.product_category.views.basicquickview.title',
        srfSubTitle: 'entities.product_category.views.basicquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Product_categoryBasicQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicQuickViewBase
     */
	protected viewtag: string = 'd59fb68967527633b916a81f3fa6ae0b';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Product_categoryBasicQuickViewBase
     */ 
    protected viewName:string = "product_categoryBasicQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Product_categoryBasicQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Product_categoryBasicQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Product_categoryBasicQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'product_category',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_categoryBasicQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_categoryBasicQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Product_categoryBasicQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
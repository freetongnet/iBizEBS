import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取全部数组
mock.onGet('./assets/json/data-dictionary.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status, [
        {
        srfkey: "UOM_CATEGORY__MEASURE_TYPE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "unit",
                label: "默认计数",
                text: "默认计数",
                "data":"",
                "codename":"Unit",
                value: "unit",
                
                disabled: false,
            },
            {
                id: "weight",
                label: "默认重量",
                text: "默认重量",
                "data":"",
                "codename":"Weight",
                value: "weight",
                
                disabled: false,
            },
            {
                id: "working_time",
                label: "默认工作时间",
                text: "默认工作时间",
                "data":"",
                "codename":"Working_time",
                value: "working_time",
                
                disabled: false,
            },
            {
                id: "length",
                label: "默认长度",
                text: "默认长度",
                "data":"",
                "codename":"Length",
                value: "length",
                
                disabled: false,
            },
            {
                id: "volume",
                label: "默认体积",
                text: "默认体积",
                "data":"",
                "codename":"Volume",
                value: "volume",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PURCHASE_ORDER__STATE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "draft",
                label: "询价单",
                text: "询价单",
                "data":"",
                "codename":"Draft",
                value: "draft",
                
                disabled: false,
            },
            {
                id: "sent",
                label: "发送询价单",
                text: "发送询价单",
                "data":"",
                "codename":"Sent",
                value: "sent",
                
                disabled: false,
            },
            {
                id: "to approve",
                label: "待批准",
                text: "待批准",
                "data":"",
                "codename":"To_approve",
                value: "to approve",
                
                disabled: false,
            },
            {
                id: "purchase",
                label: "采购订单",
                text: "采购订单",
                "data":"",
                "codename":"Purchase",
                value: "purchase",
                
                disabled: false,
            },
            {
                id: "done",
                label: "已锁定",
                text: "已锁定",
                "data":"",
                "codename":"Done",
                value: "done",
                
                disabled: false,
            },
            {
                id: "cancel",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Cancel",
                value: "cancel",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PURCHASE_ORDER__INVOICE_STATUS",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "no",
                label: "无需开单",
                text: "无需开单",
                "data":"",
                "codename":"No",
                value: "no",
                
                disabled: false,
            },
            {
                id: "to invoice",
                label: "未收到账单",
                text: "未收到账单",
                "data":"",
                "codename":"To_invoice",
                value: "to invoice",
                
                disabled: false,
            },
            {
                id: "invoiced",
                label: "已完全开单",
                text: "已完全开单",
                "data":"",
                "codename":"Invoiced",
                value: "invoiced",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PRODUCT_CATEGORY__PROPERTY_COST_METHOD",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "standard",
                label: "标准价格",
                text: "标准价格",
                "data":"",
                "codename":"Standard",
                value: "standard",
                
                disabled: false,
            },
            {
                id: "fifo",
                label: "先进先出(FIFO)",
                text: "先进先出(FIFO)",
                "data":"",
                "codename":"Fifo",
                value: "fifo",
                
                disabled: false,
            },
            {
                id: "average",
                label: "平均成本(AVCO)",
                text: "平均成本(AVCO)",
                "data":"",
                "codename":"Average",
                value: "average",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PURCHASE_REQUISITION__STATE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "draft",
                label: "草稿",
                text: "草稿",
                "data":"",
                "codename":"Draft",
                value: "draft",
                
                disabled: false,
            },
            {
                id: "ongoing",
                label: "正在进行",
                text: "正在进行",
                "data":"",
                "codename":"Ongoing",
                value: "ongoing",
                
                disabled: false,
            },
            {
                id: "in_progress",
                label: "已确认",
                text: "已确认",
                "data":"",
                "codename":"In_progress",
                value: "in_progress",
                
                disabled: false,
            },
            {
                id: "open",
                label: "出价选择",
                text: "出价选择",
                "data":"",
                "codename":"Open",
                value: "open",
                
                disabled: false,
            },
            {
                id: "done",
                label: "已关闭",
                text: "已关闭",
                "data":"",
                "codename":"Done",
                value: "done",
                
                disabled: false,
            },
            {
                id: "cancel",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Cancel",
                value: "cancel",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "UOM_UOM__UOM_TYPE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "bigger",
                label: "大于参考单位",
                text: "大于参考单位",
                "data":"",
                "codename":"Bigger",
                value: "bigger",
                
                disabled: false,
            },
            {
                id: "reference",
                label: "这个类别的参考计量单位",
                text: "这个类别的参考计量单位",
                "data":"",
                "codename":"Reference",
                value: "reference",
                
                disabled: false,
            },
            {
                id: "smaller",
                label: "小于参考计量单位",
                text: "小于参考计量单位",
                "data":"",
                "codename":"Smaller",
                value: "smaller",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "RES_CONFIG_SETTINGS__DEFAULT_PURCHASE_METHOD",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "purchase",
                label: "订购数量",
                text: "订购数量",
                "data":"",
                "codename":"Purchase",
                value: "purchase",
                
                disabled: false,
            },
            {
                id: "receive",
                label: "收到数量",
                text: "收到数量",
                "data":"",
                "codename":"Receive",
                value: "receive",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "RES_COMPANY__PO_LOCK",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "edit",
                label: "允许修改采购订单",
                text: "允许修改采购订单",
                "data":"",
                "codename":"Edit",
                value: "edit",
                
                disabled: false,
            },
            {
                id: "lock",
                label: "禁止修改已确认订单",
                text: "禁止修改已确认订单",
                "data":"",
                "codename":"Lock",
                value: "lock",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PURCHASE_REQUISITION_TYPE__LINE_COPY",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "copy",
                label: "使用申请单的明细行",
                text: "使用申请单的明细行",
                "data":"",
                "codename":"Copy",
                value: "copy",
                
                disabled: false,
            },
            {
                id: "none",
                label: "不自动创建采购订单明细行",
                text: "不自动创建采购订单明细行",
                "data":"",
                "codename":"None",
                value: "none",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "RES_PARTNER__TYPE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "contact",
                label: "联系人",
                text: "联系人",
                "data":"",
                "codename":"Contact",
                value: "contact",
                
                disabled: false,
            },
            {
                id: "invoice",
                label: "发票地址",
                text: "发票地址",
                "data":"",
                "codename":"Invoice",
                value: "invoice",
                
                disabled: false,
            },
            {
                id: "delivery",
                label: "送货地址",
                text: "送货地址",
                "data":"",
                "codename":"Delivery",
                value: "delivery",
                
                disabled: false,
            },
            {
                id: "other",
                label: "其他地址",
                text: "其他地址",
                "data":"",
                "codename":"Other",
                value: "other",
                
                disabled: false,
            },
            {
                id: "private",
                label: "家庭住址",
                text: "家庭住址",
                "data":"",
                "codename":"Private",
                value: "private",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PURCHASE_REQUISITION_TYPE__EXCLUSIVE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "exclusive",
                label: "仅选择一个询价单(单一供应商)",
                text: "仅选择一个询价单(单一供应商)",
                "data":"",
                "codename":"Exclusive",
                value: "exclusive",
                
                disabled: false,
            },
            {
                id: "multiple",
                label: "选择多个询价单",
                text: "选择多个询价单",
                "data":"",
                "codename":"Multiple",
                value: "multiple",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Odoo_truefalse",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "false",
                label: "否",
                text: "否",
                "data":"",
                "codename":"False",
                value: "false",
                
                disabled: false,
            },
            {
                id: "true",
                label: "是",
                text: "是",
                "data":"",
                "codename":"True",
                value: "true",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PRODUCT_TEMPLATE__TYPE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "consu",
                label: "可消耗",
                text: "可消耗",
                "data":"",
                "codename":"Consu",
                value: "consu",
                
                disabled: false,
            },
            {
                id: "service",
                label: "服务",
                text: "服务",
                "data":"",
                "codename":"Service",
                value: "service",
                
                disabled: false,
            },
            {
                id: "product",
                label: "可库存产品",
                text: "可库存产品",
                "data":"",
                "codename":"Product",
                value: "product",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "RES_COMPANY__PO_DOUBLE_VALIDATION",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "one_step",
                label: "直接确认订单",
                text: "直接确认订单",
                "data":"",
                "codename":"One_step",
                value: "one_step",
                
                disabled: false,
            },
            {
                id: "two_step",
                label: "2级审批确认采购订单",
                text: "2级审批确认采购订单",
                "data":"",
                "codename":"Two_step",
                value: "two_step",
                
                disabled: false,
            },
        ]
    }
    ]];
});


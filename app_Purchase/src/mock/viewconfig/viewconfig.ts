import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取studio链接数据
mock.onGet('./assets/json/view-config.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
                "purchase_requisition_typebasiclistexpview": {
            "title": "配置列表导航视图",
            "caption": "采购申请类型",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisition_typeBasicListExpView",
            "viewtag": "011a0f05ed392aa4fc7b7edfbd30a182"
        },
        "product_productpickupview": {
            "title": "产品数据选择视图",
            "caption": "产品",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productPickupView",
            "viewtag": "03656c91dd184ab62a81166134954d3b"
        },
        "account_taxpurchasepickupgridview": {
            "title": "税率选择表格视图",
            "caption": "税率",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_taxPurchasePickupGridView",
            "viewtag": "0678faedaa5916c618d1d73a44b13024"
        },
        "product_templatepickupview": {
            "title": "产品模板数据选择视图",
            "caption": "产品模板",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_templatePickupView",
            "viewtag": "083974d7385593d391f4c25235061b19"
        },
        "res_suppliermastertabinfoview": {
            "title": "主信息总览视图",
            "caption": "供应商",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_supplierMasterTabInfoView",
            "viewtag": "0a68f3cf7fc39261aec28734fcaec309"
        },
        "res_partner_categorypickupgridview": {
            "title": "业务伙伴标签选择表格视图",
            "caption": "业务伙伴标签",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partner_categoryPickupGridView",
            "viewtag": "0bb629da79eaf7ac10841ccb2737e094"
        },
        "res_partner_categorympickupview": {
            "title": "业务伙伴标签数据多项选择视图",
            "caption": "业务伙伴标签",
            "viewtype": "DEMPICKUPVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partner_categoryMPickupView",
            "viewtag": "0df51febb18b55dd835055f5fcb60e46"
        },
        "mail_activityeditview": {
            "title": "活动编辑视图",
            "caption": "活动",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_activityEditView",
            "viewtag": "0ed9b8a29b1aaaf542dbcc7b9ea9ec1d"
        },
        "product_templatemasterinfoview": {
            "title": "主信息概览",
            "caption": "产品模板",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_templateMasterInfoView",
            "viewtag": "12583aa40f1e9c0eaa5a5ba2ce0dd7a4"
        },
        "uom_uombasiclistexpview": {
            "title": "配置列表导航视图",
            "caption": "产品计量单位",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "odoo_uom",
            "viewname": "uom_uomBasicListExpView",
            "viewtag": "1551b807bd3deca8061c8cda38fadbc2"
        },
        "product_templatemastertabinfoview": {
            "title": "主信息总览视图",
            "caption": "产品模板",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_templateMasterTabInfoView",
            "viewtag": "175b7500bc95e4c2564abecb6e5bfb57"
        },
        "product_supplierinfobasiclistexpview": {
            "title": "配置列表导航视图",
            "caption": "供应商价格表",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_supplierinfoBasicListExpView",
            "viewtag": "23d299cd56de1e4e8cabd5d5dc155438"
        },
        "uom_categorybasicquickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_uom",
            "viewname": "uom_categoryBasicQuickView",
            "viewtag": "244f52820f45a1fd04532bf700eb3af6"
        },
        "res_suppliermasterinfoview": {
            "title": "主信息概览",
            "caption": "供应商",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_supplierMasterInfoView",
            "viewtag": "273bd8803959ff9357b9c2479037352c"
        },
        "product_productmasterquickview": {
            "title": "快速新建",
            "caption": "产品",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productMasterQuickView",
            "viewtag": "2c2058b72216560ba352abf8f1fb59de"
        },
        "purchase_ordermasterinfoview": {
            "title": "主信息概览",
            "caption": "采购订单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_orderMasterInfoView",
            "viewtag": "2f2403c07f98ac47a93c2ccbf1485a9c"
        },
        "product_productmastereditview": {
            "title": "主编辑视图",
            "caption": "产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productMasterEditView",
            "viewtag": "360a6bd6871b47937d21942ed041e923"
        },
        "purchase_requisition_lineeditview": {
            "title": "订单申请行编辑视图",
            "caption": "采购申请行",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisition_lineEditView",
            "viewtag": "3650b5d36d60a645cbc3dbdb6aebd03b"
        },
        "product_productpickupgridview": {
            "title": "产品选择表格视图",
            "caption": "产品",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productPickupGridView",
            "viewtag": "36b18dd7ec23975b726c9ce6149e1f4e"
        },
        "res_suppliermastergridview": {
            "title": "首选表格视图",
            "caption": "供应商",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_supplierMasterGridView",
            "viewtag": "37f650c54b5668f3be6a4511c4b81f66"
        },
        "res_partnercardview": {
            "title": "联系人卡片视图",
            "caption": "联系人",
            "viewtype": "DEDATAVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partnerCardView",
            "viewtag": "38623a8febf9da055523bdc8b0bfe754"
        },
        "account_taxmpickupview": {
            "title": "税率数据多项选择视图",
            "caption": "税率",
            "viewtype": "DEMPICKUPVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_taxMPickupView",
            "viewtag": "3e0ae40c6eb52d8be2029b14eab2890f"
        },
        "purchase_ordermasterquickview": {
            "title": "快速新建",
            "caption": "采购订单",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_orderMasterQuickView",
            "viewtag": "3fa97a49854e23e3ad5cdb87315bc919"
        },
        "purchase_ordermastergridview": {
            "title": "首选表格视图",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_orderMasterGridView",
            "viewtag": "43b29101d22eac924161b03afc6bfda8"
        },
        "product_templatemastersummaryview": {
            "title": "概览",
            "caption": "产品模板",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_templateMasterSummaryView",
            "viewtag": "44a7d256aebf043cc67d2e1b956d61eb"
        },
        "purchase_ordermastertabinfoview": {
            "title": "主信息总览视图",
            "caption": "采购订单",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_orderMasterTabInfoView",
            "viewtag": "474a86d5a3c0c3907c16dc3fdfcabfb8"
        },
        "mail_messagemainview": {
            "title": "消息数据看板视图",
            "caption": "消息",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageMainView",
            "viewtag": "47b5eabc445ed79c0fd04d8da748c776"
        },
        "res_suppliermasterquickview": {
            "title": "快速新建",
            "caption": "供应商",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_supplierMasterQuickView",
            "viewtag": "4a9f1473ff1847a6c719009ea4362e88"
        },
        "account_taxpickupgridview": {
            "title": "税率选择表格视图",
            "caption": "税率",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_taxPickupGridView",
            "viewtag": "4f602151957c04cb4039f34bbbb71fe6"
        },
        "purchase_requisition_typebasiceditview": {
            "title": "配置信息编辑视图",
            "caption": "供应商价格表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisition_typeBasicEditView",
            "viewtag": "6917a6b4ac10d888f7e41d0daef2df90"
        },
        "res_userspickupview": {
            "title": "用户数据选择视图",
            "caption": "用户",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_usersPickupView",
            "viewtag": "6928c8dc50acd54e98c657410871afa9"
        },
        "product_productmastersummaryview": {
            "title": "概览",
            "caption": "产品",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productMasterSummaryView",
            "viewtag": "69d64b992c942b72ac526dbbb4fa3e6c"
        },
        "purchase_requisition_typepickupview": {
            "title": "采购申请类型数据选择视图",
            "caption": "采购申请类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisition_typePickupView",
            "viewtag": "6a0c617ff14554d4fea25e8572ef67c9"
        },
        "res_partnerpickupview": {
            "title": "联系人数据选择视图",
            "caption": "联系人",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partnerPickupView",
            "viewtag": "6abc5f67274f993c9ecb116f94900e85"
        },
        "purchase_requisition_linelineedit": {
            "title": "行编辑表格视图",
            "caption": "采购申请行",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisition_lineLineEdit",
            "viewtag": "6c2ed7dbdc3e94cfd8dadb9915ac60d1"
        },
        "purchase_requisition_typebasicquickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisition_typeBasicQuickView",
            "viewtag": "6cf91abfbf2a39c529df314651e7b8e9"
        },
        "purchase_requisition_lineline": {
            "title": "行编辑表格视图",
            "caption": "采购申请行",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisition_lineLine",
            "viewtag": "6ff1a605b7c38a825e0b678bb9b0b340"
        },
        "res_config_settingspurchaseeditview": {
            "title": "配置设定编辑视图",
            "caption": "配置设定",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_config_settingsPurchaseEditView",
            "viewtag": "7045de775c0fdff5d3e3bd0cef75f348"
        },
        "res_suppliermastereditview": {
            "title": "主编辑视图",
            "caption": "供应商",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_supplierMasterEditView",
            "viewtag": "72e741b20d81402c6a306e779d4d24b7"
        },
        "res_partner_bankline": {
            "title": "行编辑表格视图",
            "caption": "银行账户",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partner_bankLine",
            "viewtag": "73a5fcd7f7450ec1c38adecb5fd8fc38"
        },
        "purchase_order_lineeditview": {
            "title": "采购订单行编辑视图",
            "caption": "采购订单行",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_order_lineEditView",
            "viewtag": "73bbfb31298bf72c9dbd20a9ff078c3a"
        },
        "uom_categorybasiceditview": {
            "title": "配置信息编辑视图",
            "caption": "供应商价格表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_uom",
            "viewname": "uom_categoryBasicEditView",
            "viewtag": "73ec5ee653ea252842738499c351e806"
        },
        "purchase_requisition_typepickupgridview": {
            "title": "采购申请类型选择表格视图",
            "caption": "采购申请类型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisition_typePickupGridView",
            "viewtag": "77a2a70b11298000acab49c9de786dea"
        },
        "purchase_ordermastergridview_order": {
            "title": "首选表格视图",
            "caption": "采购订单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_orderMasterGridView_Order",
            "viewtag": "78c65f1c30d343c4e926b75e04a0270c"
        },
        "product_supplierinfobasicquickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_supplierinfoBasicQuickView",
            "viewtag": "794b114292ebc7840d037dc2b553456c"
        },
        "mail_messageremarkview": {
            "title": "消息编辑视图",
            "caption": "消息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageRemarkView",
            "viewtag": "7d1c76645cc1b9d4a16e555ed36f03c1"
        },
        "uom_uombasicquickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_uom",
            "viewname": "uom_uomBasicQuickView",
            "viewtag": "7d4c11699bc484f1477d6c640fbd63cf"
        },
        "purchase_requisitionmastersummaryview": {
            "title": "概览",
            "caption": "采购申请",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisitionMasterSummaryView",
            "viewtag": "80291fb77442ee85e0bb0ad9249c39e5"
        },
        "uom_categorypickupgridview": {
            "title": "产品计量单位 类别选择表格视图",
            "caption": "产品计量单位 类别",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_uom",
            "viewname": "uom_categoryPickupGridView",
            "viewtag": "825088b4d6d996d3789b9fec1eed8b7a"
        },
        "purchase_order_linelineedit": {
            "title": "行编辑表格视图",
            "caption": "采购订单行",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_order_lineLineEdit",
            "viewtag": "85a1249f4157e1334350ca14e8960e4c"
        },
        "purchase_ordermastergridview_enquiry": {
            "title": "首选表格视图",
            "caption": "询价单",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_orderMasterGridView_Enquiry",
            "viewtag": "881f113aaefe3dc53ffd9c967417bf3e"
        },
        "product_categorybasiclistexpview": {
            "title": "配置列表导航视图",
            "caption": "产品种类",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_categoryBasicListExpView",
            "viewtag": "8edd23ee9ade06059e61231de1194c86"
        },
        "product_categorybasiceditview": {
            "title": "配置信息编辑视图",
            "caption": "供应商价格表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_categoryBasicEditView",
            "viewtag": "913d12caf6e3d513f7e2939e616ff1f3"
        },
        "product_templatemastereditview": {
            "title": "主编辑视图",
            "caption": "产品模板",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_templateMasterEditView",
            "viewtag": "927f2214efea768cf3cd3c4cffea0f08"
        },
        "res_supplierpickupgridview": {
            "title": "供应商选择表格视图",
            "caption": "供应商",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_supplierPickupGridView",
            "viewtag": "96541c93d48032e914cc7645b075a17c"
        },
        "mail_tracking_valueeditview": {
            "title": "邮件跟踪值编辑视图",
            "caption": "邮件跟踪值",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_tracking_valueEditView",
            "viewtag": "968828b545ae61cbd2ccfbc098d27439"
        },
        "mail_tracking_valuegridview": {
            "title": "邮件跟踪值表格视图",
            "caption": "邮件跟踪值",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_tracking_valueGridView",
            "viewtag": "97876cd531c323f7e33100a91c5ef130"
        },
        "product_templatepickupgridview": {
            "title": "产品模板选择表格视图",
            "caption": "产品模板",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_templatePickupGridView",
            "viewtag": "985f726271ed67e18824f88e3b68bb17"
        },
        "purchase_requisitionmastertabinfoview": {
            "title": "主信息总览视图",
            "caption": "采购申请",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisitionMasterTabInfoView",
            "viewtag": "999794b38995fb8355f30590b92500b1"
        },
        "account_taxpurchasempickupview": {
            "title": "税率",
            "caption": "税率",
            "viewtype": "DEMPICKUPVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_taxPurchaseMPickupView",
            "viewtag": "9cfc393e5fbc8f48d6de3e36d5bdb397"
        },
        "res_userspickupgridview": {
            "title": "用户选择表格视图",
            "caption": "用户",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_usersPickupGridView",
            "viewtag": "9d9be8fc580e553c1afb70e2906de3b1"
        },
        "delivery_carrierpickupview": {
            "title": "送货方式数据选择视图",
            "caption": "送货方式",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_stock",
            "viewname": "delivery_carrierPickupView",
            "viewtag": "9fbed2a2c15cb087c6c41ec9b18d64b2"
        },
        "res_partner_banklineedit": {
            "title": "行编辑表格视图",
            "caption": "银行账户",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partner_bankLineEdit",
            "viewtag": "a7f3041df1b70e8537a0e99589e43185"
        },
        "mail_messagebyreslistview": {
            "title": "消息列表视图",
            "caption": "消息",
            "viewtype": "DELISTVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageByResListView",
            "viewtag": "b08e5b415c5f01a339a70045e7d3bbd2"
        },
        "res_partner_bankeditview": {
            "title": "银行账户编辑视图",
            "caption": "银行账户",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partner_bankEditView",
            "viewtag": "b37c46823c4ae40f305089f3a62ff069"
        },
        "res_suppliermastersummaryview": {
            "title": "概览",
            "caption": "供应商",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_supplierMasterSummaryView",
            "viewtag": "b42a148563e92720dcbdc83b5d46117a"
        },
        "res_partnerpickupgridview": {
            "title": "联系人选择表格视图",
            "caption": "联系人",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partnerPickupGridView",
            "viewtag": "b9c01aac6e18ec1eed1b74d47f275384"
        },
        "purchase_ordermastersummaryview": {
            "title": "概览",
            "caption": "采购订单",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_orderMasterSummaryView",
            "viewtag": "ba1e054173433d27d55d5ce19d746621"
        },
        "product_templatemastergridview": {
            "title": "首选表格视图",
            "caption": "产品",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_templateMasterGridView",
            "viewtag": "be4ed5c86df8369fa55d46dd5c404cd0"
        },
        "mail_messagemastertabview": {
            "title": "消息分页导航视图",
            "caption": "消息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageMasterTabView",
            "viewtag": "c1160abb36ff4fc18ddcaea34cc2ab51"
        },
        "purchase_requisitionmasterquickview": {
            "title": "快速新建",
            "caption": "采购申请",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisitionMasterQuickView",
            "viewtag": "c3c9c9431ba6282dfa40dabd8dc01145"
        },
        "product_productmastergridview": {
            "title": "首选表格视图",
            "caption": "产品变种",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productMasterGridView",
            "viewtag": "c49562ec1ffc30686416dd5faee2326d"
        },
        "account_payment_termpickupgridview": {
            "title": "付款条款选择表格视图",
            "caption": "付款条款",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_payment_termPickupGridView",
            "viewtag": "c88ced76e543002649f1daab2157f8a9"
        },
        "uom_categorypickupview": {
            "title": "产品计量单位 类别数据选择视图",
            "caption": "产品计量单位 类别",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_uom",
            "viewname": "uom_categoryPickupView",
            "viewtag": "c98d519438489bdaaf82b58443149cc1"
        },
        "purchase_requisitionmastereditview": {
            "title": "主编辑视图",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisitionMasterEditView",
            "viewtag": "cbed9661c4340482cd2c392a349d8edc"
        },
        "purchase_ordermastereditview": {
            "title": "主编辑视图",
            "caption": "采购订单",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_orderMasterEditView",
            "viewtag": "d0e6c6f2ae964187efd612a848818619"
        },
        "ir_attachmentbyresdataview": {
            "title": "附件数据视图",
            "caption": "附件",
            "viewtype": "DEDATAVIEW",
            "viewmodule": "odoo_ir",
            "viewname": "ir_attachmentByResDataView",
            "viewtag": "d5688a3507c45bd42213ffdd5a8376fc"
        },
        "product_categorybasicquickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_categoryBasicQuickView",
            "viewtag": "d59fb68967527633b916a81f3fa6ae0b"
        },
        "mail_messagesendview": {
            "title": "消息编辑视图",
            "caption": "消息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageSendView",
            "viewtag": "dbd94e64a1afa7763551ee81ad319556"
        },
        "delivery_carrierpickupgridview": {
            "title": "送货方式选择表格视图",
            "caption": "送货方式",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_stock",
            "viewname": "delivery_carrierPickupGridView",
            "viewtag": "e0c880f6e920b932c7d0b7ddcb84e746"
        },
        "product_supplierinfobasiceditview": {
            "title": "配置信息编辑视图",
            "caption": "供应商价格表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_supplierinfoBasicEditView",
            "viewtag": "e10d70201b07a1a9a3f9126cf60265ba"
        },
        "uom_uombasiceditview": {
            "title": "配置信息编辑视图",
            "caption": "供应商价格表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_uom",
            "viewname": "uom_uomBasicEditView",
            "viewtag": "e3a30586132f7bb68b424b8057900c39"
        },
        "mail_activitybyreslistview": {
            "title": "活动列表视图",
            "caption": "活动",
            "viewtype": "DELISTVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_activityByResListView",
            "viewtag": "e79e0aa4e64b7c70a08332f6cc3aaa93"
        },
        "product_templatemasterquickview": {
            "title": "快速新建",
            "caption": "产品模板",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_templateMasterQuickView",
            "viewtag": "e85d9e51a4c1b134bb3745f0817f34bf"
        },
        "purchase_requisitionmasterinfoview": {
            "title": "主信息概览",
            "caption": "采购申请",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisitionMasterInfoView",
            "viewtag": "ed8d6abde7ba9daaccdfa5049373df37"
        },
        "purchase_order_lineline": {
            "title": "行编辑表格视图",
            "caption": "采购订单行",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_order_lineLine",
            "viewtag": "f3659046722b66fc4a405e6967b1cc86"
        },
        "res_supplierpickupview": {
            "title": "供应商数据选择视图",
            "caption": "供应商",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_supplierPickupView",
            "viewtag": "f65c8430fe2c75de5d2abdb244453ef7"
        },
        "product_productmasterinfoview": {
            "title": "主信息概览",
            "caption": "产品",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productMasterInfoView",
            "viewtag": "f70a6b0e958493244ca97f4177ba1c24"
        },
        "uom_categorybasiclistexpview": {
            "title": "配置列表导航视图",
            "caption": "产品计量单位 类别",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "odoo_uom",
            "viewname": "uom_categoryBasicListExpView",
            "viewtag": "f76810e24d59572d6248acabba384f15"
        },
        "purchase_requisitionmastergridview": {
            "title": "首选表格视图",
            "caption": "采购申请",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "purchase_requisitionMasterGridView",
            "viewtag": "fa79c811f8008cd1b1d9e2f4502e85d8"
        },
        "purchaseindexview": {
            "title": "采购首页视图",
            "caption": "采购",
            "viewtype": "APPINDEXVIEW",
            "viewmodule": "odoo_purchase",
            "viewname": "PurchaseIndexView",
            "viewtag": "fb00976b7de01eefb698c5529e7bc9a0"
        },
        "mail_followersbyreslistview": {
            "title": "文档关注者列表视图",
            "caption": "文档关注者",
            "viewtype": "DELISTVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_followersByResListView",
            "viewtag": "fc5eec6ef08d3fe889ac304fa8d6b45d"
        },
        "product_productmastertabinfoview": {
            "title": "主信息总览视图",
            "caption": "产品",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productMasterTabInfoView",
            "viewtag": "fdabc40fcb32358d1035072e381e847c"
        },
        "account_payment_termpickupview": {
            "title": "付款条款数据选择视图",
            "caption": "付款条款",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_payment_termPickupView",
            "viewtag": "fe7216759cd1bacac1b5dafd5d02e948"
        }
    }];
});

// 获取视图消息分组信息
mock.onGet('./assets/json/view-message-group.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
    }];
});
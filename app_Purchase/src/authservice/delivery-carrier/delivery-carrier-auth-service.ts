import Delivery_carrierAuthServiceBase from './delivery-carrier-auth-service-base';


/**
 * 送货方式权限服务对象
 *
 * @export
 * @class Delivery_carrierAuthService
 * @extends {Delivery_carrierAuthServiceBase}
 */
export default class Delivery_carrierAuthService extends Delivery_carrierAuthServiceBase {

    /**
     * Creates an instance of  Delivery_carrierAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Delivery_carrierAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Uom_categoryAuthServiceBase from './uom-category-auth-service-base';


/**
 * 产品计量单位 类别权限服务对象
 *
 * @export
 * @class Uom_categoryAuthService
 * @extends {Uom_categoryAuthServiceBase}
 */
export default class Uom_categoryAuthService extends Uom_categoryAuthServiceBase {

    /**
     * Creates an instance of  Uom_categoryAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Uom_categoryAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
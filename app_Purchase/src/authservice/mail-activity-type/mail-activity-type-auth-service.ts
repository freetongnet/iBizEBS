import Mail_activity_typeAuthServiceBase from './mail-activity-type-auth-service-base';


/**
 * 活动类型权限服务对象
 *
 * @export
 * @class Mail_activity_typeAuthService
 * @extends {Mail_activity_typeAuthServiceBase}
 */
export default class Mail_activity_typeAuthService extends Mail_activity_typeAuthServiceBase {

    /**
     * Creates an instance of  Mail_activity_typeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_activity_typeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
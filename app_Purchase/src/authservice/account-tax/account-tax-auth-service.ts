import Account_taxAuthServiceBase from './account-tax-auth-service-base';


/**
 * 税率权限服务对象
 *
 * @export
 * @class Account_taxAuthService
 * @extends {Account_taxAuthServiceBase}
 */
export default class Account_taxAuthService extends Account_taxAuthServiceBase {

    /**
     * Creates an instance of  Account_taxAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_taxAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
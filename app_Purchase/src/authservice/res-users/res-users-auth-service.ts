import Res_usersAuthServiceBase from './res-users-auth-service-base';


/**
 * 用户权限服务对象
 *
 * @export
 * @class Res_usersAuthService
 * @extends {Res_usersAuthServiceBase}
 */
export default class Res_usersAuthService extends Res_usersAuthServiceBase {

    /**
     * Creates an instance of  Res_usersAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_usersAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
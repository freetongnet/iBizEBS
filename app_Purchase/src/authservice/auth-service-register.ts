/**
 * 实体权限服务注册中心
 *
 * @export
 * @class AuthServiceRegister
 */
export class AuthServiceRegister {

    /**
     * 所有实体权限服务Map
     *
     * @protected
     * @type {*}
     * @memberof AuthServiceRegister
     */
    protected allAuthService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载实体权限服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof AuthServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of AuthServiceRegister.
     * @memberof AuthServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof AuthServiceRegister
     */
    protected init(): void {
                this.allAuthService.set('res_partner', () => import('@/authservice/res-partner/res-partner-auth-service'));
        this.allAuthService.set('stock_location', () => import('@/authservice/stock-location/stock-location-auth-service'));
        this.allAuthService.set('ir_attachment', () => import('@/authservice/ir-attachment/ir-attachment-auth-service'));
        this.allAuthService.set('account_tax', () => import('@/authservice/account-tax/account-tax-auth-service'));
        this.allAuthService.set('res_bank', () => import('@/authservice/res-bank/res-bank-auth-service'));
        this.allAuthService.set('account_fiscal_position', () => import('@/authservice/account-fiscal-position/account-fiscal-position-auth-service'));
        this.allAuthService.set('mail_activity', () => import('@/authservice/mail-activity/mail-activity-auth-service'));
        this.allAuthService.set('mail_message', () => import('@/authservice/mail-message/mail-message-auth-service'));
        this.allAuthService.set('uom_uom', () => import('@/authservice/uom-uom/uom-uom-auth-service'));
        this.allAuthService.set('account_incoterms', () => import('@/authservice/account-incoterms/account-incoterms-auth-service'));
        this.allAuthService.set('stock_picking_type', () => import('@/authservice/stock-picking-type/stock-picking-type-auth-service'));
        this.allAuthService.set('res_config_settings', () => import('@/authservice/res-config-settings/res-config-settings-auth-service'));
        this.allAuthService.set('product_template', () => import('@/authservice/product-template/product-template-auth-service'));
        this.allAuthService.set('product_template_attribute_line', () => import('@/authservice/product-template-attribute-line/product-template-attribute-line-auth-service'));
        this.allAuthService.set('res_supplier', () => import('@/authservice/res-supplier/res-supplier-auth-service'));
        this.allAuthService.set('delivery_carrier', () => import('@/authservice/delivery-carrier/delivery-carrier-auth-service'));
        this.allAuthService.set('res_users', () => import('@/authservice/res-users/res-users-auth-service'));
        this.allAuthService.set('purchase_requisition_type', () => import('@/authservice/purchase-requisition-type/purchase-requisition-type-auth-service'));
        this.allAuthService.set('res_partner_category', () => import('@/authservice/res-partner-category/res-partner-category-auth-service'));
        this.allAuthService.set('mail_activity_type', () => import('@/authservice/mail-activity-type/mail-activity-type-auth-service'));
        this.allAuthService.set('res_currency', () => import('@/authservice/res-currency/res-currency-auth-service'));
        this.allAuthService.set('res_company', () => import('@/authservice/res-company/res-company-auth-service'));
        this.allAuthService.set('mail_followers', () => import('@/authservice/mail-followers/mail-followers-auth-service'));
        this.allAuthService.set('res_partner_bank', () => import('@/authservice/res-partner-bank/res-partner-bank-auth-service'));
        this.allAuthService.set('product_pricelist', () => import('@/authservice/product-pricelist/product-pricelist-auth-service'));
        this.allAuthService.set('res_partner_title', () => import('@/authservice/res-partner-title/res-partner-title-auth-service'));
        this.allAuthService.set('purchase_order', () => import('@/authservice/purchase-order/purchase-order-auth-service'));
        this.allAuthService.set('uom_category', () => import('@/authservice/uom-category/uom-category-auth-service'));
        this.allAuthService.set('res_country', () => import('@/authservice/res-country/res-country-auth-service'));
        this.allAuthService.set('product_category', () => import('@/authservice/product-category/product-category-auth-service'));
        this.allAuthService.set('purchase_requisition_line', () => import('@/authservice/purchase-requisition-line/purchase-requisition-line-auth-service'));
        this.allAuthService.set('res_country_state', () => import('@/authservice/res-country-state/res-country-state-auth-service'));
        this.allAuthService.set('res_country_group', () => import('@/authservice/res-country-group/res-country-group-auth-service'));
        this.allAuthService.set('product_supplierinfo', () => import('@/authservice/product-supplierinfo/product-supplierinfo-auth-service'));
        this.allAuthService.set('purchase_order_line', () => import('@/authservice/purchase-order-line/purchase-order-line-auth-service'));
        this.allAuthService.set('mail_tracking_value', () => import('@/authservice/mail-tracking-value/mail-tracking-value-auth-service'));
        this.allAuthService.set('mail_compose_message', () => import('@/authservice/mail-compose-message/mail-compose-message-auth-service'));
        this.allAuthService.set('account_payment_term', () => import('@/authservice/account-payment-term/account-payment-term-auth-service'));
        this.allAuthService.set('product_product', () => import('@/authservice/product-product/product-product-auth-service'));
        this.allAuthService.set('purchase_requisition', () => import('@/authservice/purchase-requisition/purchase-requisition-auth-service'));
    }

    /**
     * 加载实体权限服务
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof AuthServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allAuthService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体权限服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof AuthServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const authService: any = await this.loadService(name);
        if (authService && authService.default) {
            const instance: any = new authService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const authServiceRegister: AuthServiceRegister = new AuthServiceRegister();
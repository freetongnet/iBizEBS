import Stock_locationAuthServiceBase from './stock-location-auth-service-base';


/**
 * 库存位置权限服务对象
 *
 * @export
 * @class Stock_locationAuthService
 * @extends {Stock_locationAuthServiceBase}
 */
export default class Stock_locationAuthService extends Stock_locationAuthServiceBase {

    /**
     * Creates an instance of  Stock_locationAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Stock_locationAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import AuthService from '../auth-service';

/**
 * 配置设定权限服务对象基类
 *
 * @export
 * @class Res_config_settingsAuthServiceBase
 * @extends {AuthService}
 */
export default class Res_config_settingsAuthServiceBase extends AuthService {

    /**
     * Creates an instance of  Res_config_settingsAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_config_settingsAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 根据当前数据获取实体操作标识
     *
     * @param {*} mainSateOPPrivs 传入数据操作标识
     * @returns {any}
     * @memberof Res_config_settingsAuthServiceBase
     */
    public getOPPrivs(mainSateOPPrivs:any):any{
        let curDefaultOPPrivs:any = this.getSysOPPrivs();
        let copyDefaultOPPrivs:any = JSON.parse(JSON.stringify(curDefaultOPPrivs));
        if(mainSateOPPrivs){
            Object.assign(curDefaultOPPrivs,mainSateOPPrivs);
        }
        // 统一资源优先
        Object.keys(curDefaultOPPrivs).forEach((name:string) => {
            if(this.sysOPPrivsMap.get(name) && copyDefaultOPPrivs[name] === 0){
                curDefaultOPPrivs[name] = copyDefaultOPPrivs[name];
            }
        });
        return curDefaultOPPrivs;
    }

}
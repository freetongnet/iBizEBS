import Res_country_groupAuthServiceBase from './res-country-group-auth-service-base';


/**
 * 国家/地区群组权限服务对象
 *
 * @export
 * @class Res_country_groupAuthService
 * @extends {Res_country_groupAuthServiceBase}
 */
export default class Res_country_groupAuthService extends Res_country_groupAuthServiceBase {

    /**
     * Creates an instance of  Res_country_groupAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_country_groupAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Mail_activityAuthServiceBase from './mail-activity-auth-service-base';


/**
 * 活动权限服务对象
 *
 * @export
 * @class Mail_activityAuthService
 * @extends {Mail_activityAuthServiceBase}
 */
export default class Mail_activityAuthService extends Mail_activityAuthServiceBase {

    /**
     * Creates an instance of  Mail_activityAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_activityAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
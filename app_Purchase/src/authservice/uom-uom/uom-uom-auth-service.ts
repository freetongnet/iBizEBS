import Uom_uomAuthServiceBase from './uom-uom-auth-service-base';


/**
 * 产品计量单位权限服务对象
 *
 * @export
 * @class Uom_uomAuthService
 * @extends {Uom_uomAuthServiceBase}
 */
export default class Uom_uomAuthService extends Uom_uomAuthServiceBase {

    /**
     * Creates an instance of  Uom_uomAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Uom_uomAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Product_templateAuthServiceBase from './product-template-auth-service-base';


/**
 * 产品模板权限服务对象
 *
 * @export
 * @class Product_templateAuthService
 * @extends {Product_templateAuthServiceBase}
 */
export default class Product_templateAuthService extends Product_templateAuthServiceBase {

    /**
     * Creates an instance of  Product_templateAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_templateAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Purchase_order_lineAuthServiceBase from './purchase-order-line-auth-service-base';


/**
 * 采购订单行权限服务对象
 *
 * @export
 * @class Purchase_order_lineAuthService
 * @extends {Purchase_order_lineAuthServiceBase}
 */
export default class Purchase_order_lineAuthService extends Purchase_order_lineAuthServiceBase {

    /**
     * Creates an instance of  Purchase_order_lineAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_order_lineAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
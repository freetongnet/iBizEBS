import Product_pricelistAuthServiceBase from './product-pricelist-auth-service-base';


/**
 * 价格表权限服务对象
 *
 * @export
 * @class Product_pricelistAuthService
 * @extends {Product_pricelistAuthServiceBase}
 */
export default class Product_pricelistAuthService extends Product_pricelistAuthServiceBase {

    /**
     * Creates an instance of  Product_pricelistAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_pricelistAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
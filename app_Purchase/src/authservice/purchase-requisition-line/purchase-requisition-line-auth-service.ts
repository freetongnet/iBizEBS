import Purchase_requisition_lineAuthServiceBase from './purchase-requisition-line-auth-service-base';


/**
 * 采购申请行权限服务对象
 *
 * @export
 * @class Purchase_requisition_lineAuthService
 * @extends {Purchase_requisition_lineAuthServiceBase}
 */
export default class Purchase_requisition_lineAuthService extends Purchase_requisition_lineAuthServiceBase {

    /**
     * Creates an instance of  Purchase_requisition_lineAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Purchase_requisition_lineAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
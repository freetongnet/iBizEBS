import Product_categoryAuthServiceBase from './product-category-auth-service-base';


/**
 * 产品种类权限服务对象
 *
 * @export
 * @class Product_categoryAuthService
 * @extends {Product_categoryAuthServiceBase}
 */
export default class Product_categoryAuthService extends Product_categoryAuthServiceBase {

    /**
     * Creates an instance of  Product_categoryAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Product_categoryAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
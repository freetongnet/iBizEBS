package cn.ibizlab.businesscentral.util.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface BackFillProperty {

    Class reference() ;

    Class referenceService() ;

    String referenceKey();

    String referenceName() default "" ;

    String backFillName() default "" ;

}

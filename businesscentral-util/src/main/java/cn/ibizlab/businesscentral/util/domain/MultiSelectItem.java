package cn.ibizlab.businesscentral.util.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class MultiSelectItem implements Serializable {

    @JSONField(name = "srfmajortext")
    @JsonProperty("srfmajortext")
    private String srfmajortext;

    @JSONField(name = "srfkey")
    @JsonProperty("srfkey")
    private Long srfkey ;
}

/**
 * List 部件模型
 *
 * @export
 * @class ListModel
 */
export default class ListModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof ListModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'icon',
        prop: 'icon'
      },
      {
        name: 'activity_type_id_text',
        prop: 'activity_type_id_text'
      },
      {
        name: 'create_date',
        prop: 'create_date'
      },
      {
        name: 'user_id_text',
        prop: 'user_id_text'
      },
      {
        name: 'note',
        prop: 'note'
      }
    ]
  }
}
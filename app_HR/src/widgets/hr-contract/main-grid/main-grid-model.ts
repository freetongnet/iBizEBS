/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'company_id_text',
          prop: 'company_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'department_id_text',
          prop: 'department_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'employee_id_text',
          prop: 'employee_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'job_id_text',
          prop: 'job_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'date_start',
          prop: 'date_start',
          dataType: 'DATE',
          isEditable:true
        },
        {
          name: 'date_end',
          prop: 'date_end',
          dataType: 'DATE',
          isEditable:true
        },
        {
          name: 'trial_date_end',
          prop: 'trial_date_end',
          dataType: 'DATE',
          isEditable:true
        },
        {
          name: 'resource_calendar_id_text',
          prop: 'resource_calendar_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'state',
          prop: 'state',
          dataType: 'SSCODELIST',
          isEditable:true
        },
        {
          name: 'wage',
          prop: 'wage',
          dataType: 'FLOAT',
        },
        {
          name: 'notes',
          prop: 'notes',
          dataType: 'LONGTEXT',
          isEditable:true
        },
        {
          name: 'company_id',
          prop: 'company_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'department_id',
          prop: 'department_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'job_id',
          prop: 'job_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'resource_calendar_id',
          prop: 'resource_calendar_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'employee_id',
          prop: 'employee_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'hr_responsible_id',
          prop: 'hr_responsible_id',
          dataType: 'PICKUP',
        },
        {
          name: 'hr_contract',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
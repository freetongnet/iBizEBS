/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'name',
          prop: 'name',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'partner_id',
          prop: 'partner_id',
          dataType: 'PICKUP',
        },
        {
          name: 'currency_id',
          prop: 'currency_id',
          dataType: 'PICKUP',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'account_purchase_tax_id',
          prop: 'account_purchase_tax_id',
          dataType: 'PICKUP',
        },
        {
          name: 'resource_calendar_id',
          prop: 'resource_calendar_id',
          dataType: 'PICKUP',
        },
        {
          name: 'tax_cash_basis_journal_id',
          prop: 'tax_cash_basis_journal_id',
          dataType: 'PICKUP',
        },
        {
          name: 'currency_exchange_journal_id',
          prop: 'currency_exchange_journal_id',
          dataType: 'PICKUP',
        },
        {
          name: 'property_stock_valuation_account_id',
          prop: 'property_stock_valuation_account_id',
          dataType: 'PICKUP',
        },
        {
          name: 'incoterm_id',
          prop: 'incoterm_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'chart_template_id',
          prop: 'chart_template_id',
          dataType: 'PICKUP',
        },
        {
          name: 'internal_transit_location_id',
          prop: 'internal_transit_location_id',
          dataType: 'PICKUP',
        },
        {
          name: 'account_opening_move_id',
          prop: 'account_opening_move_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'property_stock_account_input_categ_id',
          prop: 'property_stock_account_input_categ_id',
          dataType: 'PICKUP',
        },
        {
          name: 'property_stock_account_output_categ_id',
          prop: 'property_stock_account_output_categ_id',
          dataType: 'PICKUP',
        },
        {
          name: 'transfer_account_id',
          prop: 'transfer_account_id',
          dataType: 'PICKUP',
        },
        {
          name: 'account_sale_tax_id',
          prop: 'account_sale_tax_id',
          dataType: 'PICKUP',
        },
        {
          name: 'parent_id',
          prop: 'parent_id',
          dataType: 'PICKUP',
        },
        {
          name: 'res_company',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
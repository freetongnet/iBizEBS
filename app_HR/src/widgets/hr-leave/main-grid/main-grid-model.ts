/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof MainGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'mode_company_id',
          prop: 'mode_company_id',
          dataType: 'PICKUP',
        },
        {
          name: 'manager_id',
          prop: 'manager_id',
          dataType: 'PICKUP',
        },
        {
          name: 'first_approver_id',
          prop: 'first_approver_id',
          dataType: 'PICKUP',
        },
        {
          name: 'department_id',
          prop: 'department_id',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'category_id',
          prop: 'category_id',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'holiday_status_id',
          prop: 'holiday_status_id',
          dataType: 'PICKUP',
        },
        {
          name: 'user_id',
          prop: 'user_id',
          dataType: 'PICKUP',
        },
        {
          name: 'meeting_id',
          prop: 'meeting_id',
          dataType: 'PICKUP',
        },
        {
          name: 'second_approver_id',
          prop: 'second_approver_id',
          dataType: 'PICKUP',
        },
        {
          name: 'employee_id',
          prop: 'employee_id',
          dataType: 'PICKUP',
        },
        {
          name: 'parent_id',
          prop: 'parent_id',
          dataType: 'PICKUP',
        },
        {
          name: 'hr_leave',
          prop: 'id',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
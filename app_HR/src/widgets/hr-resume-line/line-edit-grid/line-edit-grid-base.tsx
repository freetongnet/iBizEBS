import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, GridControlBase } from '@/studio-core';
import Hr_resume_lineService from '@/service/hr-resume-line/hr-resume-line-service';
import LineEditService from './line-edit-grid-service';
import Hr_resume_lineUIService from '@/uiservice/hr-resume-line/hr-resume-line-ui-service';
import { FormItemModel } from '@/model/form-detail';


/**
 * grid部件基类
 *
 * @export
 * @class GridControlBase
 * @extends {LineEditGridBase}
 */
export class LineEditGridBase extends GridControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof LineEditGridBase
     */
    protected controlType: string = 'GRID';

    /**
     * 建构部件服务对象
     *
     * @type {LineEditService}
     * @memberof LineEditGridBase
     */
    public service: LineEditService = new LineEditService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_resume_lineService}
     * @memberof LineEditGridBase
     */
    public appEntityService: Hr_resume_lineService = new Hr_resume_lineService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof LineEditGridBase
     */
    protected appDeName: string = 'hr_resume_line';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof LineEditGridBase
     */
    protected appDeLogicName: string = '员工简历行';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_resume_lineUIService}
     * @memberof LineEditBase
     */  
    public appUIService:Hr_resume_lineUIService = new Hr_resume_lineUIService(this.$store);


    /**
     * 界面行为模型
     *
     * @type {*}
     * @memberof LineEditBase
     */  
    public ActionModel: any = {
    };

    /**
     * 主信息表格列
     *
     * @type {string}
     * @memberof LineEditBase
     */  
    public majorInfoColName:string = "name";


    /**
     * 本地缓存标识
     *
     * @protected
     * @type {string}
     * @memberof LineEditBase
     */
    protected localStorageTag: string = 'hr_resume_line_lineedit_grid';

    /**
     * 是否支持分页
     *
     * @type {boolean}
     * @memberof LineEditGridBase
     */
    public isEnablePagingBar: boolean = false;

    /**
     * 所有列成员
     *
     * @type {any[]}
     * @memberof LineEditGridBase
     */
    public allColumns: any[] = [
        {
            name: 'name',
            label: '名称',
            langtag: 'entities.hr_resume_line.lineedit_grid.columns.name',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'display_type',
            label: '显示类型',
            langtag: 'entities.hr_resume_line.lineedit_grid.columns.display_type',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'line_type_id_text',
            label: '类型',
            langtag: 'entities.hr_resume_line.lineedit_grid.columns.line_type_id_text',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'date_start',
            label: '开始日期',
            langtag: 'entities.hr_resume_line.lineedit_grid.columns.date_start',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'date_end',
            label: '结束日期',
            langtag: 'entities.hr_resume_line.lineedit_grid.columns.date_end',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'employee_id',
            label: '员工',
            langtag: 'entities.hr_resume_line.lineedit_grid.columns.employee_id',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'line_type_id',
            label: '类型',
            langtag: 'entities.hr_resume_line.lineedit_grid.columns.line_type_id',
            show: false,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
        {
            name: 'write_uid',
            label: 'ID',
            langtag: 'entities.hr_resume_line.lineedit_grid.columns.write_uid',
            show: true,
            unit: 'PX',
            isEnableRowEdit: false,
            enableCond: 3 ,
        },
    ]

    /**
     * 获取表格行模型
     *
     * @type {*}
     * @memberof LineEditGridBase
     */
    public getGridRowModel(){
        return {
          date_end: new FormItemModel(),
          line_type_id_text: new FormItemModel(),
          date_start: new FormItemModel(),
          name: new FormItemModel(),
          line_type_id: new FormItemModel(),
          srfkey: new FormItemModel(),
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof LineEditGridBase
     */
    public rules: any = {
        date_end: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '结束日期 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '结束日期 值不能为空', trigger: 'blur' },
        ],
        line_type_id_text: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '类型 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '类型 值不能为空', trigger: 'blur' },
        ],
        date_start: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '开始日期 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '开始日期 值不能为空', trigger: 'blur' },
        ],
        name: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '名称 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '名称 值不能为空', trigger: 'blur' },
        ],
        line_type_id: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: '值不能为空', trigger: 'blur' },
        ],
        srfkey: [
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'change' },
            { required: false, validator: (rule:any, value:any, callback:any) => { return (rule.required && (value === null || value === undefined || value === "")) ? false : true;}, message: 'ID 值不能为空', trigger: 'blur' },
        ],
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof LineEditBase
     */
    public deRules:any = {
    };

    /**
     * 获取对应列class
     *
     * @type {*}
     * @memberof LineEditBase
     */
    public hasRowEdit: any = {
        'name':false,
        'display_type':false,
        'line_type_id_text':false,
        'date_start':false,
        'date_end':false,
        'employee_id':false,
        'line_type_id':false,
        'write_uid':false,
    };

    /**
     * 获取对应列class
     *
     * @param {*} $args row 行数据，column 列数据，rowIndex 行索引，列索引
     * @returns {void}
     * @memberof LineEditBase
     */
    public getCellClassName(args: {row: any, column: any, rowIndex: number, columnIndex: number}): any {
        return ( this.hasRowEdit[args.column.property] && this.actualIsOpenEdit ) ? "edit-cell" : "info-cell";
    }


    /**
     * 导出数据格式化
     *
     * @param {*} filterVal
     * @param {*} jsonData
     * @param {any[]} [codelistColumns=[]]
     * @returns {Promise<any>}
     * @memberof LineEditGridBase
     */
    public async formatExcelData(filterVal: any, jsonData: any, codelistColumns?: any[]): Promise<any> {
        return super.formatExcelData(filterVal, jsonData, [
            {
                name: 'display_type',
                srfkey: 'HR_RESUME_LINE__DISPLAY_TYPE',
                codelistType : 'STATIC',
                renderMode: 'other',
                textSeparator: '、',
                valueSeparator: ',',
            },
        ]);
    }


    /**
     * 更新默认值
     * @param {*}  row 行数据
     * @memberof LineEditBase
     */
    public updateDefault(row: any){                    
    }

    /**
     * 计算数据对象类型的默认值
     * @param {string}  action 行为
     * @param {string}  param 默认值参数
     * @param {*}  data 当前行数据
     * @memberof LineEditBase
     */
    public computeDefaultValueWithParam(action:string,param:string,data:any){
        if(Object.is(action,"UPDATE")){
            const nativeData:any = this.service.getCopynativeData();
            if(nativeData && (nativeData instanceof Array) && nativeData.length >0){
                let targetData:any = nativeData.find((item:any) =>{
                    return item.id === data.srfkey;
                })
                if(targetData){
                    return targetData[param]?targetData[param]:null;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
           return this.service.getRemoteCopyData()[param]?this.service.getRemoteCopyData()[param]:null;
        }
    }


}
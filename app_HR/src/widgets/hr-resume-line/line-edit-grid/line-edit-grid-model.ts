/**
 * LineEdit 部件模型
 *
 * @export
 * @class LineEditModel
 */
export default class LineEditModel {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof LineEditGridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof LineEditGridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'name',
          prop: 'name',
          dataType: 'TEXT',
          isEditable:true
        },
        {
          name: 'display_type',
          prop: 'display_type',
          dataType: 'SSCODELIST',
        },
        {
          name: 'line_type_id_text',
          prop: 'line_type_id_text',
          dataType: 'PICKUPTEXT',
          isEditable:true
        },
        {
          name: 'date_start',
          prop: 'date_start',
          dataType: 'DATE',
          isEditable:true
        },
        {
          name: 'date_end',
          prop: 'date_end',
          dataType: 'DATE',
          isEditable:true
        },
        {
          name: 'employee_id',
          prop: 'employee_id',
          dataType: 'PICKUP',
        },
        {
          name: 'line_type_id',
          prop: 'line_type_id',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'write_uid',
          prop: 'write_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'create_uid',
          prop: 'create_uid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'name',
          dataType: 'TEXT',
        },
        {
          name: 'srfkey',
          prop: 'id',
          dataType: 'ACID',
          isEditable:true
        },
        {
          name: 'srfdataaccaction',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'id',
          prop: 'id',
          dataType: 'ACID',
        },
        {
          name: 'hr_resume_line',
          prop: 'id',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}
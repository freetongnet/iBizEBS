/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: '__last_update',
      },
      {
        name: 'resource_resource',
        prop: 'id',
      },
      {
        name: 'name',
      },
      {
        name: 'time_efficiency',
      },
      {
        name: 'create_date',
      },
      {
        name: 'active',
      },
      {
        name: 'resource_type',
      },
      {
        name: 'tz',
      },
      {
        name: 'write_date',
      },
      {
        name: 'display_name',
      },
      {
        name: 'calendar_id_text',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'company_id_text',
      },
      {
        name: 'user_id_text',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'calendar_id',
      },
      {
        name: 'company_id',
      },
      {
        name: 'user_id',
      },
      {
        name: 'write_uid',
      },
    ]
  }


}
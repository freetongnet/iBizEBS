import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Activity 部件服务对象
 *
 * @export
 * @class ActivityService
 */
export default class ActivityService extends ControlService {
}

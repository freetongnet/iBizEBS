import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, MainControlBase } from '@/studio-core';
import Mail_messageService from '@/service/mail-message/mail-message-service';
import MasterTabViewtabviewpanel23Service from './master-tab-viewtabviewpanel23-tabviewpanel-service';
import Mail_messageUIService from '@/uiservice/mail-message/mail-message-ui-service';


/**
 * tabviewpanel23部件基类
 *
 * @export
 * @class MainControlBase
 * @extends {MasterTabViewtabviewpanel23TabviewpanelBase}
 */
export class MasterTabViewtabviewpanel23TabviewpanelBase extends MainControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterTabViewtabviewpanel23TabviewpanelBase
     */
    protected controlType: string = 'TABVIEWPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {MasterTabViewtabviewpanel23Service}
     * @memberof MasterTabViewtabviewpanel23TabviewpanelBase
     */
    public service: MasterTabViewtabviewpanel23Service = new MasterTabViewtabviewpanel23Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Mail_messageService}
     * @memberof MasterTabViewtabviewpanel23TabviewpanelBase
     */
    public appEntityService: Mail_messageService = new Mail_messageService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabViewtabviewpanel23TabviewpanelBase
     */
    protected appDeName: string = 'mail_message';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabViewtabviewpanel23TabviewpanelBase
     */
    protected appDeLogicName: string = '消息';

    /**
     * 界面UI服务对象
     *
     * @type {Mail_messageUIService}
     * @memberof MasterTabViewtabviewpanel23Base
     */  
    public appUIService:Mail_messageUIService = new Mail_messageUIService(this.$store);


    /**
     * 导航模式下项是否激活
     *
     * @type {*}
     * @memberof MasterTabViewtabviewpanel23
     */
    @Prop()
    public expActive!: any;

    /**
     * 获取多项数据
     *
     * @returns {any[]}
     * @memberof MasterTabViewtabviewpanel23
     */
    public getDatas(): any[] {
        return [];
    }

    /**
     * 获取单项树
     *
     * @returns {*}
     * @memberof MasterTabViewtabviewpanel23
     */
    public getData(): any {
        return null;
    }

    /**
     * 是否被激活
     *
     * @type {boolean}
     * @memberof MasterTabViewtabviewpanel23
     */
    public isActivied: boolean = true;

    /**
     * 局部上下文
     *
     * @type {*}
     * @memberof MasterTabViewtabviewpanel23
     */
    public localContext: any = null;

    /**
     * 局部视图参数
     *
     * @type {*}
     * @memberof MasterTabViewtabviewpanel23
     */
    public localViewParam: any = null;

    /**
     * 传入上下文
     *
     * @type {string}
     * @memberof TabExpViewtabviewpanel
     */
    public viewdata: string = JSON.stringify(this.context);

    /**
     * 传入视图参数
     *
     * @type {string}
     * @memberof PickupViewpickupviewpanel
     */
    public viewparam: string = JSON.stringify(this.viewparams);

    /**
     * 视图面板过滤项
     *
     * @type {string}
     * @memberof MasterTabViewtabviewpanel23
     */
    public navfilter: string = "";
             
    /**
     * vue 生命周期
     *
     * @returns
     * @memberof MasterTabViewtabviewpanel23
     */
    public created() {
        this.afterCreated();
    }

    /**
     * 执行created后的逻辑
     *
     *  @memberof MasterTabViewtabviewpanel23
     */    
    public afterCreated(){
        this.initNavParam();
        if (this.viewState) {
            this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }) => {
                if (!Object.is(tag, this.name)) {
                    return;
                }
                this.context.clearAll();
                Object.assign(this.context, data);
                this.$forceUpdate();
                this.initNavParam();
            });
        }
    }

    /**
     * 初始化导航参数
     *
     * @memberof MasterTabViewtabviewpanel23
     */
    public initNavParam(){
        if(!Object.is(this.navfilter,"")){
            Object.assign(this.viewparams,{[this.navfilter]:this.context['majorentity']})
        }
        if(this.localContext && Object.keys(this.localContext).length >0){
            let _context:any = this.$util.computedNavData({},this.context,this.viewparams,this.localContext);
            Object.assign(this.context,_context);
        }
        if(this.localViewParam && Object.keys(this.localViewParam).length >0){
            let _param:any = this.$util.computedNavData({},this.context,this.viewparams,this.localViewParam);
            Object.assign(this.viewparams,_param);
        }
        this.viewdata =JSON.stringify(this.context);
        this.viewparam = JSON.stringify(this.viewparams);
    }

    /**
     * 视图数据变化
     *
     * @memberof  MasterTabViewtabviewpanel23
     */
    public viewDatasChange($event:any){
        this.$emit('viewpanelDatasChange',$event);
    }

    /**
     * vue 生命周期
     *
     * @memberof MasterTabViewtabviewpanel23
     */
    public destroyed() {
        this.afterDestroy();
    }

    /**
     * 执行destroyed后的逻辑
     *
     * @memberof MasterTabViewtabviewpanel23
     */
    public afterDestroy() {
        if (this.viewStateEvent) {
            this.viewStateEvent.unsubscribe();
        }
    }
}
/**
 * IF_Master 部件模型
 *
 * @export
 * @class IF_MasterModel
 */
export default class IF_MasterModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof IF_MasterModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'write_date',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'srfmajortext',
        prop: 'name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'name',
        prop: 'name',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'job_title',
        prop: 'job_title',
        dataType: 'TEXT',
      },
      {
        name: 'mobile_phone',
        prop: 'mobile_phone',
        dataType: 'TEXT',
      },
      {
        name: 'work_phone',
        prop: 'work_phone',
        dataType: 'TEXT',
      },
      {
        name: 'work_email',
        prop: 'work_email',
        dataType: 'TEXT',
      },
      {
        name: 'work_location',
        prop: 'work_location',
        dataType: 'TEXT',
      },
      {
        name: 'company_id_text',
        prop: 'company_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'department_id_text',
        prop: 'department_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'job_id_text',
        prop: 'job_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'parent_id_text',
        prop: 'parent_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'job_id',
        prop: 'job_id',
        dataType: 'PICKUP',
      },
      {
        name: 'company_id',
        prop: 'company_id',
        dataType: 'PICKUP',
      },
      {
        name: 'department_id',
        prop: 'department_id',
        dataType: 'PICKUP',
      },
      {
        name: 'parent_id',
        prop: 'parent_id',
        dataType: 'PICKUP',
      },
      {
        name: 'address_id',
        prop: 'address_id',
        dataType: 'PICKUP',
      },
      {
        name: 'address_id_text',
        prop: 'address_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'coach_id',
        prop: 'coach_id',
        dataType: 'PICKUP',
      },
      {
        name: 'expense_manager_id',
        prop: 'expense_manager_id',
        dataType: 'PICKUP',
      },
      {
        name: 'expense_manager_id_text',
        prop: 'expense_manager_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'coach_id_text',
        prop: 'coach_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'address_home_id',
        prop: 'address_home_id',
        dataType: 'PICKUP',
      },
      {
        name: 'address_home_id_text',
        prop: 'address_home_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'emergency_contact',
        prop: 'emergency_contact',
        dataType: 'TEXT',
      },
      {
        name: 'emergency_phone',
        prop: 'emergency_phone',
        dataType: 'TEXT',
      },
      {
        name: 'marital',
        prop: 'marital',
        dataType: 'SSCODELIST',
      },
      {
        name: 'spouse_complete_name',
        prop: 'spouse_complete_name',
        dataType: 'TEXT',
      },
      {
        name: 'spouse_birthdate',
        prop: 'spouse_birthdate',
        dataType: 'DATE',
      },
      {
        name: 'children',
        prop: 'children',
        dataType: 'INT',
      },
      {
        name: 'country_id',
        prop: 'country_id',
        dataType: 'PICKUP',
      },
      {
        name: 'country_id_text',
        prop: 'country_id_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'identification_id',
        prop: 'identification_id',
        dataType: 'TEXT',
      },
      {
        name: 'passport_id',
        prop: 'passport_id',
        dataType: 'TEXT',
      },
      {
        name: 'gender',
        prop: 'gender',
        dataType: 'SSCODELIST',
      },
      {
        name: 'birthday',
        prop: 'birthday',
        dataType: 'DATE',
      },
      {
        name: 'place_of_birth',
        prop: 'place_of_birth',
        dataType: 'TEXT',
      },
      {
        name: 'country_of_birth',
        prop: 'country_of_birth',
        dataType: 'PICKUP',
      },
      {
        name: 'country_of_birth_text',
        prop: 'country_of_birth_text',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'certificate',
        prop: 'certificate',
        dataType: 'SSCODELIST',
      },
      {
        name: 'study_field',
        prop: 'study_field',
        dataType: 'TEXT',
      },
      {
        name: 'study_school',
        prop: 'study_school',
        dataType: 'TEXT',
      },
      {
        name: 'permit_no',
        prop: 'permit_no',
        dataType: 'TEXT',
      },
      {
        name: 'visa_no',
        prop: 'visa_no',
        dataType: 'TEXT',
      },
      {
        name: 'visa_expire',
        prop: 'visa_expire',
        dataType: 'DATE',
      },
      {
        name: 'pin',
        prop: 'pin',
        dataType: 'TEXT',
      },
      {
        name: 'barcode',
        prop: 'barcode',
        dataType: 'TEXT',
      },
      {
        name: 'id',
        prop: 'id',
        dataType: 'ACID',
      },
      {
        name: 'hr_employee',
        prop: 'id',
        dataType: 'FONTKEY',
      },
    ]
  }

}
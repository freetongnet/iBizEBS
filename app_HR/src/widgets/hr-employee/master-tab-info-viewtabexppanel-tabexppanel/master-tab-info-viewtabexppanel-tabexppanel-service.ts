import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Hr_employeeService from '@/service/hr-employee/hr-employee-service';
import MasterTabInfoViewtabexppanelModel from './master-tab-info-viewtabexppanel-tabexppanel-model';


/**
 * MasterTabInfoViewtabexppanel 部件服务对象
 *
 * @export
 * @class MasterTabInfoViewtabexppanelService
 */
export default class MasterTabInfoViewtabexppanelService extends ControlService {

    /**
     * 员工服务对象
     *
     * @type {Hr_employeeService}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public appEntityService: Hr_employeeService = new Hr_employeeService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of MasterTabInfoViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof MasterTabInfoViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new MasterTabInfoViewtabexppanelModel();
    }

    
}
import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import Hr_departmentService from '@/service/hr-department/hr-department-service';
import MasterTabInfoViewtabexppanelModel from './master-tab-info-viewtabexppanel-tabexppanel-model';


/**
 * MasterTabInfoViewtabexppanel 部件服务对象
 *
 * @export
 * @class MasterTabInfoViewtabexppanelService
 */
export default class MasterTabInfoViewtabexppanelService extends ControlService {

    /**
     * HR 部门服务对象
     *
     * @type {Hr_departmentService}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public appEntityService: Hr_departmentService = new Hr_departmentService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof MasterTabInfoViewtabexppanelService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of MasterTabInfoViewtabexppanelService.
     * 
     * @param {*} [opts={}]
     * @memberof MasterTabInfoViewtabexppanelService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new MasterTabInfoViewtabexppanelModel();
    }

    
}
import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, TabExpPanelControlBase } from '@/studio-core';
import Hr_departmentService from '@/service/hr-department/hr-department-service';
import MasterTabInfoViewtabexppanelService from './master-tab-info-viewtabexppanel-tabexppanel-service';
import Hr_departmentUIService from '@/uiservice/hr-department/hr-department-ui-service';
import Hr_departmentAuthService from '@/authservice/hr-department/hr-department-auth-service';
import { Environment } from '@/environments/environment';


/**
 * tabexppanel部件基类
 *
 * @export
 * @class TabExpPanelControlBase
 * @extends {MasterTabInfoViewtabexppanelTabexppanelBase}
 */
export class MasterTabInfoViewtabexppanelTabexppanelBase extends TabExpPanelControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected controlType: string = 'TABEXPPANEL';

    /**
     * 建构部件服务对象
     *
     * @type {MasterTabInfoViewtabexppanelService}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    public service: MasterTabInfoViewtabexppanelService = new MasterTabInfoViewtabexppanelService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_departmentService}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    public appEntityService: Hr_departmentService = new Hr_departmentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected appDeName: string = 'hr_department';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelTabexppanelBase
     */
    protected appDeLogicName: string = 'HR 部门';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_departmentUIService}
     * @memberof MasterTabInfoViewtabexppanelBase
     */  
    public appUIService:Hr_departmentUIService = new Hr_departmentUIService(this.$store);

    /**
     * 是否初始化
     *
     * @protected
     * @returns {any}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected isInit: any = {
    }

    /**
     * 被激活的分页面板
     *
     * @protected
     * @type {string}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected activatedTabViewPanel: string = '';

    /**
     * 实体权限服务对象
     *
     * @protected
     * @type Hr_departmentAuthServiceBase
     * @memberof TabExpViewtabexppanelBase
     */
    protected appAuthService: Hr_departmentAuthService = new Hr_departmentAuthService();

    /**
     * 分页面板权限标识存储对象
     *
     * @protected
     * @type {*}
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected authResourceObject:any = {};

    /**
     * 组件创建完毕
     *
     * @protected
     * @memberof MasterTabInfoViewtabexppanelBase
     */
    protected ctrlCreated(): void {
        //设置分页导航srfparentdename和srfparentkey
        if (this.context.hr_department) {
            Object.assign(this.context, { srfparentdename: 'Hr_department', srfparentkey: this.context.hr_department });
        }
        super.ctrlCreated();
    }
}
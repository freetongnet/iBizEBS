import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool,Util,ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import Hr_departmentService from '@/service/hr-department/hr-department-service';
import SF_MasterService from './sf-master-searchform-service';
import Hr_departmentUIService from '@/uiservice/hr-department/hr-department-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {SF_MasterSearchFormBase}
 */
export class SF_MasterSearchFormBase extends SearchFormControlBase {

    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof SF_MasterSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {SF_MasterService}
     * @memberof SF_MasterSearchFormBase
     */
    public service: SF_MasterService = new SF_MasterService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {Hr_departmentService}
     * @memberof SF_MasterSearchFormBase
     */
    public appEntityService: Hr_departmentService = new Hr_departmentService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SF_MasterSearchFormBase
     */
    protected appDeName: string = 'hr_department';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof SF_MasterSearchFormBase
     */
    protected appDeLogicName: string = 'HR 部门';

    /**
     * 界面UI服务对象
     *
     * @type {Hr_departmentUIService}
     * @memberof SF_MasterBase
     */  
    public appUIService:Hr_departmentUIService = new Hr_departmentUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof SF_MasterSearchFormBase
     */
    public data: any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof SF_MasterSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
    };

    /**
     * 新建默认值
     * @memberof SF_MasterBase
     */
    public createDefault(){                    
    }
}
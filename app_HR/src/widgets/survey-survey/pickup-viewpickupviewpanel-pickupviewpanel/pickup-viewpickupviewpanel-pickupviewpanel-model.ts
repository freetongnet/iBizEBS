/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'message_partner_ids',
      },
      {
        name: 'create_date',
      },
      {
        name: 'display_name',
      },
      {
        name: 'website_message_ids',
      },
      {
        name: 'users_can_go_back',
      },
      {
        name: 'message_channel_ids',
      },
      {
        name: 'message_unread',
      },
      {
        name: 'message_has_error',
      },
      {
        name: 'message_ids',
      },
      {
        name: 'survey_survey',
        prop: 'id',
      },
      {
        name: 'tot_start_survey',
      },
      {
        name: 'result_url',
      },
      {
        name: 'print_url',
      },
      {
        name: 'quizz_mode',
      },
      {
        name: '__last_update',
      },
      {
        name: 'tot_comp_survey',
      },
      {
        name: 'write_date',
      },
      {
        name: 'message_unread_counter',
      },
      {
        name: 'activity_summary',
      },
      {
        name: 'public_url_html',
      },
      {
        name: 'activity_type_id',
      },
      {
        name: 'message_main_attachment_id',
      },
      {
        name: 'message_follower_ids',
      },
      {
        name: 'message_is_follower',
      },
      {
        name: 'user_input_ids',
      },
      {
        name: 'color',
      },
      {
        name: 'page_ids',
      },
      {
        name: 'auth_required',
      },
      {
        name: 'description',
      },
      {
        name: 'message_needaction_counter',
      },
      {
        name: 'tot_sent_survey',
      },
      {
        name: 'activity_user_id',
      },
      {
        name: 'activity_ids',
      },
      {
        name: 'message_attachment_count',
      },
      {
        name: 'designed',
      },
      {
        name: 'activity_state',
      },
      {
        name: 'message_has_error_counter',
      },
      {
        name: 'title',
      },
      {
        name: 'thank_you_message',
      },
      {
        name: 'activity_date_deadline',
      },
      {
        name: 'public_url',
      },
      {
        name: 'active',
      },
      {
        name: 'message_needaction',
      },
      {
        name: 'write_uid_text',
      },
      {
        name: 'is_closed',
      },
      {
        name: 'create_uid_text',
      },
      {
        name: 'stage_id_text',
      },
      {
        name: 'email_template_id_text',
      },
      {
        name: 'stage_id',
      },
      {
        name: 'create_uid',
      },
      {
        name: 'email_template_id',
      },
      {
        name: 'write_uid',
      },
    ]
  }


}
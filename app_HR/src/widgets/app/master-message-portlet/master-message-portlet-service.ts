import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MasterMessage 部件服务对象
 *
 * @export
 * @class MasterMessageService
 */
export default class MasterMessageService extends ControlService {
}

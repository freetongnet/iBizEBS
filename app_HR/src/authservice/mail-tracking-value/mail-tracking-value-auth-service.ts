import Mail_tracking_valueAuthServiceBase from './mail-tracking-value-auth-service-base';


/**
 * 邮件跟踪值权限服务对象
 *
 * @export
 * @class Mail_tracking_valueAuthService
 * @extends {Mail_tracking_valueAuthServiceBase}
 */
export default class Mail_tracking_valueAuthService extends Mail_tracking_valueAuthServiceBase {

    /**
     * Creates an instance of  Mail_tracking_valueAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_tracking_valueAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
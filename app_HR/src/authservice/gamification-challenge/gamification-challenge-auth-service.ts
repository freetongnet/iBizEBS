import Gamification_challengeAuthServiceBase from './gamification-challenge-auth-service-base';


/**
 * 游戏化挑战权限服务对象
 *
 * @export
 * @class Gamification_challengeAuthService
 * @extends {Gamification_challengeAuthServiceBase}
 */
export default class Gamification_challengeAuthService extends Gamification_challengeAuthServiceBase {

    /**
     * Creates an instance of  Gamification_challengeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Gamification_challengeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Mail_messageAuthServiceBase from './mail-message-auth-service-base';


/**
 * 消息权限服务对象
 *
 * @export
 * @class Mail_messageAuthService
 * @extends {Mail_messageAuthServiceBase}
 */
export default class Mail_messageAuthService extends Mail_messageAuthServiceBase {

    /**
     * Creates an instance of  Mail_messageAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Mail_messageAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
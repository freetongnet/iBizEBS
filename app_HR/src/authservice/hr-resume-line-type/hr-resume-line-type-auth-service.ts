import Hr_resume_line_typeAuthServiceBase from './hr-resume-line-type-auth-service-base';


/**
 * 简历类型权限服务对象
 *
 * @export
 * @class Hr_resume_line_typeAuthService
 * @extends {Hr_resume_line_typeAuthServiceBase}
 */
export default class Hr_resume_line_typeAuthService extends Hr_resume_line_typeAuthServiceBase {

    /**
     * Creates an instance of  Hr_resume_line_typeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_resume_line_typeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Hr_resume_lineAuthServiceBase from './hr-resume-line-auth-service-base';


/**
 * 员工简历行权限服务对象
 *
 * @export
 * @class Hr_resume_lineAuthService
 * @extends {Hr_resume_lineAuthServiceBase}
 */
export default class Hr_resume_lineAuthService extends Hr_resume_lineAuthServiceBase {

    /**
     * Creates an instance of  Hr_resume_lineAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_resume_lineAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
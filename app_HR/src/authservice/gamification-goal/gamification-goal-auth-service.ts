import Gamification_goalAuthServiceBase from './gamification-goal-auth-service-base';


/**
 * 游戏化目标权限服务对象
 *
 * @export
 * @class Gamification_goalAuthService
 * @extends {Gamification_goalAuthServiceBase}
 */
export default class Gamification_goalAuthService extends Gamification_goalAuthServiceBase {

    /**
     * Creates an instance of  Gamification_goalAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Gamification_goalAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
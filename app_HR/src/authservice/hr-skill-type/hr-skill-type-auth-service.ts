import Hr_skill_typeAuthServiceBase from './hr-skill-type-auth-service-base';


/**
 * 技能类型权限服务对象
 *
 * @export
 * @class Hr_skill_typeAuthService
 * @extends {Hr_skill_typeAuthServiceBase}
 */
export default class Hr_skill_typeAuthService extends Hr_skill_typeAuthServiceBase {

    /**
     * Creates an instance of  Hr_skill_typeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skill_typeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Account_analytic_groupAuthServiceBase from './account-analytic-group-auth-service-base';


/**
 * 分析类别权限服务对象
 *
 * @export
 * @class Account_analytic_groupAuthService
 * @extends {Account_analytic_groupAuthServiceBase}
 */
export default class Account_analytic_groupAuthService extends Account_analytic_groupAuthServiceBase {

    /**
     * Creates an instance of  Account_analytic_groupAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_analytic_groupAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Hr_skillAuthServiceBase from './hr-skill-auth-service-base';


/**
 * 技能权限服务对象
 *
 * @export
 * @class Hr_skillAuthService
 * @extends {Hr_skillAuthServiceBase}
 */
export default class Hr_skillAuthService extends Hr_skillAuthServiceBase {

    /**
     * Creates an instance of  Hr_skillAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skillAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Hr_departmentAuthServiceBase from './hr-department-auth-service-base';


/**
 * HR 部门权限服务对象
 *
 * @export
 * @class Hr_departmentAuthService
 * @extends {Hr_departmentAuthServiceBase}
 */
export default class Hr_departmentAuthService extends Hr_departmentAuthServiceBase {

    /**
     * Creates an instance of  Hr_departmentAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_departmentAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Hr_jobAuthServiceBase from './hr-job-auth-service-base';


/**
 * 工作岗位权限服务对象
 *
 * @export
 * @class Hr_jobAuthService
 * @extends {Hr_jobAuthServiceBase}
 */
export default class Hr_jobAuthService extends Hr_jobAuthServiceBase {

    /**
     * Creates an instance of  Hr_jobAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_jobAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
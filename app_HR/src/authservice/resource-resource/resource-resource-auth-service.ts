import Resource_resourceAuthServiceBase from './resource-resource-auth-service-base';


/**
 * 资源权限服务对象
 *
 * @export
 * @class Resource_resourceAuthService
 * @extends {Resource_resourceAuthServiceBase}
 */
export default class Resource_resourceAuthService extends Resource_resourceAuthServiceBase {

    /**
     * Creates an instance of  Resource_resourceAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Resource_resourceAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Account_analytic_lineAuthServiceBase from './account-analytic-line-auth-service-base';


/**
 * 分析行权限服务对象
 *
 * @export
 * @class Account_analytic_lineAuthService
 * @extends {Account_analytic_lineAuthServiceBase}
 */
export default class Account_analytic_lineAuthService extends Account_analytic_lineAuthServiceBase {

    /**
     * Creates an instance of  Account_analytic_lineAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_analytic_lineAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import Account_analytic_accountAuthServiceBase from './account-analytic-account-auth-service-base';


/**
 * 分析账户权限服务对象
 *
 * @export
 * @class Account_analytic_accountAuthService
 * @extends {Account_analytic_accountAuthServiceBase}
 */
export default class Account_analytic_accountAuthService extends Account_analytic_accountAuthServiceBase {

    /**
     * Creates an instance of  Account_analytic_accountAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_analytic_accountAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
/**
 * 币种
 *
 * @export
 * @interface Res_currency
 */
export interface Res_currency {

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_currency
     */
    create_date?: any;

    /**
     * 小数点位置
     *
     * @returns {*}
     * @memberof Res_currency
     */
    decimal_places?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_currency
     */
    name?: any;

    /**
     * 货币单位
     *
     * @returns {*}
     * @memberof Res_currency
     */
    currency_unit_label?: any;

    /**
     * 舍入系数
     *
     * @returns {*}
     * @memberof Res_currency
     */
    rounding?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_currency
     */
    __last_update?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_currency
     */
    id?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_currency
     */
    write_date?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof Res_currency
     */
    date?: any;

    /**
     * 符号位置
     *
     * @returns {*}
     * @memberof Res_currency
     */
    position?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_currency
     */
    display_name?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Res_currency
     */
    active?: any;

    /**
     * 当前汇率
     *
     * @returns {*}
     * @memberof Res_currency
     */
    rate?: any;

    /**
     * 比率
     *
     * @returns {*}
     * @memberof Res_currency
     */
    rate_ids?: any;

    /**
     * 货币符号
     *
     * @returns {*}
     * @memberof Res_currency
     */
    symbol?: any;

    /**
     * 货币子单位
     *
     * @returns {*}
     * @memberof Res_currency
     */
    currency_subunit_label?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_currency
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_currency
     */
    create_uid_text?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_currency
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_currency
     */
    create_uid?: any;
}
/**
 * 员工
 *
 * @export
 * @interface Hr_employee
 */
export interface Hr_employee {

    /**
     * 办公手机
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    mobile_phone?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_has_error_counter?: any;

    /**
     * 行动数量
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_needaction_counter?: any;

    /**
     * 起始日期
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    leave_date_from?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_unread?: any;

    /**
     * 子女数目
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    children?: any;

    /**
     * 小尺寸照片
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    image_small?: any;

    /**
     * 需要采取行动
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_needaction?: any;

    /**
     * PIN
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    pin?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_main_attachment_id?: any;

    /**
     * 毕业院校
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    study_school?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    activity_ids?: any;

    /**
     * 婚姻状况
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    marital?: any;

    /**
     * 直接徽章
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    direct_badge_ids?: any;

    /**
     * 中等尺寸照片
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    image_medium?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    __last_update?: any;

    /**
     * 紧急电话
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    emergency_phone?: any;

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    activity_date_deadline?: any;

    /**
     * 上班距离
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    km_home_work?: any;

    /**
     * 签证号
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    visa_no?: any;

    /**
     * 出生地
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    place_of_birth?: any;

    /**
     * 配偶全名
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    spouse_complete_name?: any;

    /**
     * 社会保险号SIN
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    sinid?: any;

    /**
     * 员工徽章
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    badge_ids?: any;

    /**
     * 工作EMail
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    work_email?: any;

    /**
     * 拥有徽章
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    has_badges?: any;

    /**
     * 当前休假类型
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    current_leave_id?: any;

    /**
     * 员工合同
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    contract_ids?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_is_follower?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_channel_ids?: any;

    /**
     * 工牌 ID
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    barcode?: any;

    /**
     * 出生日期
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    birthday?: any;

    /**
     * 证书等级
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    certificate?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    activity_user_id?: any;

    /**
     * 研究领域
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    study_field?: any;

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    website_message_ids?: any;

    /**
     * 下一活动摘要
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    activity_summary?: any;

    /**
     * 出勤状态
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    attendance_state?: any;

    /**
     * 手动设置出席
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    manually_set_present?: any;

    /**
     * 合同统计
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    contracts_count?: any;

    /**
     * 性别
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    gender?: any;

    /**
     * 照片
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    image?: any;

    /**
     * 配偶生日
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    spouse_birthdate?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_unread_counter?: any;

    /**
     * 休假天数
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    leaves_count?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    notes?: any;

    /**
     * 附加说明
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    additional_note?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    write_date?: any;

    /**
     * 体检日期
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    medic_exam?: any;

    /**
     * 签证到期日期
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    visa_expire?: any;

    /**
     * 办公电话
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    work_phone?: any;

    /**
     * 紧急联系人
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    emergency_contact?: any;

    /**
     * 新近雇用的员工
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    newly_hired_employee?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    display_name?: any;

    /**
     * 已与公司地址相关联的员工住址
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    is_address_home_a_company?: any;

    /**
     * 出勤
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    attendance_ids?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    activity_state?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_has_error?: any;

    /**
     * 当前休假状态
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    current_leave_state?: any;

    /**
     * 下属
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    child_ids?: any;

    /**
     * 信息
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_ids?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    color?: any;

    /**
     * 手动出勤
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    manual_attendance?: any;

    /**
     * 剩余的法定休假
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    remaining_leaves?: any;

    /**
     * 社会保障号SSN
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    ssnid?: any;

    /**
     * 工作头衔
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    job_title?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    activity_type_id?: any;

    /**
     * 今日缺勤
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    is_absent_totay?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_partner_ids?: any;

    /**
     * 护照号
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    passport_id?: any;

    /**
     * 员工人力资源目标
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    goal_ids?: any;

    /**
     * 至日期
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    leave_date_to?: any;

    /**
     * 工作地点
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    work_location?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_attachment_count?: any;

    /**
     * 当前合同
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    contract_id?: any;

    /**
     * 工作许可编号
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    permit_no?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    message_follower_ids?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    id?: any;

    /**
     * 身份证号
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    identification_id?: any;

    /**
     * 公司汽车
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    vehicle?: any;

    /**
     * 标签
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    category_ids?: any;

    /**
     * 能查看剩余的休假
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    show_leaves?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    create_date?: any;

    /**
     * 经理
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    parent_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    write_uid_text?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    name?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    create_uid_text?: any;

    /**
     * 家庭住址
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    address_home_id_text?: any;

    /**
     * 时区
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    tz?: any;

    /**
     * 工作时间
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    resource_calendar_id_text?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    user_id_text?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    department_id_text?: any;

    /**
     * 国籍
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    country_of_birth_text?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    expense_manager_id_text?: any;

    /**
     * 工作岗位
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    job_id_text?: any;

    /**
     * 工作地址
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    address_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    company_id_text?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    active?: any;

    /**
     * 教练
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    coach_id_text?: any;

    /**
     * 国籍(国家)
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    country_id_text?: any;

    /**
     * 家庭住址
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    address_home_id?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    expense_manager_id?: any;

    /**
     * 银行账户号码
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    bank_account_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    create_uid?: any;

    /**
     * 国籍(国家)
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    country_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    company_id?: any;

    /**
     * 工作岗位
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    job_id?: any;

    /**
     * 资源
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    resource_id?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    user_id?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    department_id?: any;

    /**
     * 经理
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    parent_id?: any;

    /**
     * 上次出勤
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    last_attendance_id?: any;

    /**
     * 教练
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    coach_id?: any;

    /**
     * 工作地址
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    address_id?: any;

    /**
     * 国籍
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    country_of_birth?: any;

    /**
     * 工作时间
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    resource_calendar_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_employee
     */
    leave_manager_id?: any;
}
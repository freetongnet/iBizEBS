/**
 * 游戏化挑战
 *
 * @export
 * @interface Gamification_challenge
 */
export interface Gamification_challenge {

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    website_message_ids?: any;

    /**
     * 挑战名称
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    name?: any;

    /**
     * 需要激活
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_needaction?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    id?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    create_date?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_unread?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    start_date?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_has_error_counter?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_main_attachment_id?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_has_error?: any;

    /**
     * 最新报告日期
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    last_report_date?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    end_date?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_follower_ids?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_attachment_count?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_is_follower?: any;

    /**
     * 用户领域
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    user_domain?: any;

    /**
     * 出现在
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    category?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    description?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    user_ids?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_unread_counter?: any;

    /**
     * 明细行
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    line_ids?: any;

    /**
     * 报告的频率
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    report_message_frequency?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_partner_ids?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    display_name?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_channel_ids?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_ids?: any;

    /**
     * 每完成一个目标就马上奖励
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_realtime?: any;

    /**
     * 奖励未达成目标的最优者?
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_failure?: any;

    /**
     * 周期
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    period?: any;

    /**
     * 显示模式
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    visibility_mode?: any;

    /**
     * 未更新的手动目标稍后将被提醒
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    remind_update_delay?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    write_date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    __last_update?: any;

    /**
     * 建议用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    invited_user_ids?: any;

    /**
     * 行动数量
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    message_needaction_counter?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    state?: any;

    /**
     * 下次报告日期
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    next_report_date?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    manager_id_text?: any;

    /**
     * 抄送
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    report_message_group_id_text?: any;

    /**
     * 报告模板
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    report_template_id_text?: any;

    /**
     * 每位获得成功的用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_id_text?: any;

    /**
     * 第一位用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_first_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    create_uid_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    write_uid_text?: any;

    /**
     * 第二位用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_second_id_text?: any;

    /**
     * 第三位用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_third_id_text?: any;

    /**
     * 第一位用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_first_id?: any;

    /**
     * 报告模板
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    report_template_id?: any;

    /**
     * 第二位用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_second_id?: any;

    /**
     * 第三位用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_third_id?: any;

    /**
     * 抄送
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    report_message_group_id?: any;

    /**
     * 每位获得成功的用户
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    reward_id?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    manager_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Gamification_challenge
     */
    create_uid?: any;
}
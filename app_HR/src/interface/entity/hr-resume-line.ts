/**
 * 员工简历行
 *
 * @export
 * @interface Hr_resume_line
 */
export interface Hr_resume_line {

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    id?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    write_date?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    name?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    create_date?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    date_start?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    date_end?: any;

    /**
     * 显示类型
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    display_type?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    line_type_id_text?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    employee_id_text?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    employee_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    create_uid?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    line_type_id?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_resume_line
     */
    write_uid?: any;
}
/**
 * 技能
 *
 * @export
 * @interface Hr_skill
 */
export interface Hr_skill {

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_skill
     */
    create_date?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_skill
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_skill
     */
    id?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Hr_skill
     */
    name?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Hr_skill
     */
    skill_type_name?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_skill
     */
    create_uid?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_skill
     */
    write_uid?: any;

    /**
     * 技能类型
     *
     * @returns {*}
     * @memberof Hr_skill
     */
    skill_type_id?: any;
}
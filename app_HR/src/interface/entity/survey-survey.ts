/**
 * 问卷
 *
 * @export
 * @interface Survey_survey
 */
export interface Survey_survey {

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_partner_ids?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    create_date?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    display_name?: any;

    /**
     * 网站信息
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    website_message_ids?: any;

    /**
     * 用户可返回
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    users_can_go_back?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_channel_ids?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_unread?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_has_error?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_ids?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    id?: any;

    /**
     * 已开始的调查数量
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    tot_start_survey?: any;

    /**
     * 结果链接
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    result_url?: any;

    /**
     * 打印链接
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    print_url?: any;

    /**
     * 测验模式
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    quizz_mode?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    __last_update?: any;

    /**
     * 已完成的调查数量
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    tot_comp_survey?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    write_date?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_unread_counter?: any;

    /**
     * 下一活动摘要
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    activity_summary?: any;

    /**
     * 公开链接（HTML版）
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    public_url_html?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    activity_type_id?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_main_attachment_id?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_follower_ids?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_is_follower?: any;

    /**
     * 用户回应
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    user_input_ids?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    color?: any;

    /**
     * 页面
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    page_ids?: any;

    /**
     * 需要登录
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    auth_required?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    description?: any;

    /**
     * 行动数量
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_needaction_counter?: any;

    /**
     * 已发送调查者数量
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    tot_sent_survey?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    activity_user_id?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    activity_ids?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_attachment_count?: any;

    /**
     * 已经设计了
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    designed?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    activity_state?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_has_error_counter?: any;

    /**
     * 称谓
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    title?: any;

    /**
     * 感谢留言
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    thank_you_message?: any;

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    activity_date_deadline?: any;

    /**
     * 公开链接
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    public_url?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    active?: any;

    /**
     * 采取行动
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    message_needaction?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    write_uid_text?: any;

    /**
     * 已关闭
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    is_closed?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    create_uid_text?: any;

    /**
     * 阶段
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    stage_id_text?: any;

    /**
     * 邮件模板
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    email_template_id_text?: any;

    /**
     * 阶段
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    stage_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    create_uid?: any;

    /**
     * 邮件模板
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    email_template_id?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Survey_survey
     */
    write_uid?: any;
}
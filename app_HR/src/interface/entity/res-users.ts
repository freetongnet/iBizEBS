/**
 * 用户
 *
 * @export
 * @interface Res_users
 */
export interface Res_users {

    /**
     * 管理员
     *
     * @returns {*}
     * @memberof Res_users
     */
    is_moderator?: any;

    /**
     * 资源
     *
     * @returns {*}
     * @memberof Res_users
     */
    resource_ids?: any;

    /**
     * 创建日期
     *
     * @returns {*}
     * @memberof Res_users
     */
    create_date?: any;

    /**
     * 标签
     *
     * @returns {*}
     * @memberof Res_users
     */
    category_id?: any;

    /**
     * 默认工作时间
     *
     * @returns {*}
     * @memberof Res_users
     */
    resource_calendar_id?: any;

    /**
     * 用户登录记录
     *
     * @returns {*}
     * @memberof Res_users
     */
    log_ids?: any;

    /**
     * 消息
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_ids?: any;

    /**
     * 徽章
     *
     * @returns {*}
     * @memberof Res_users
     */
    badge_ids?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_users
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_users
     */
    id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_users
     */
    company_ids?: any;

    /**
     * 联系人
     *
     * @returns {*}
     * @memberof Res_users
     */
    child_ids?: any;

    /**
     * 时区偏移
     *
     * @returns {*}
     * @memberof Res_users
     */
    tz_offset?: any;

    /**
     * 活动达成
     *
     * @returns {*}
     * @memberof Res_users
     */
    target_sales_done?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_partner_ids?: any;

    /**
     * 通知管理
     *
     * @returns {*}
     * @memberof Res_users
     */
    notification_type?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Res_users
     */
    active?: any;

    /**
     * IM的状态
     *
     * @returns {*}
     * @memberof Res_users
     */
    im_status?: any;

    /**
     * 贡献值
     *
     * @returns {*}
     * @memberof Res_users
     */
    karma?: any;

    /**
     * 登记网站
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_id?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Res_users
     */
    activity_ids?: any;

    /**
     * 金质徽章个数
     *
     * @returns {*}
     * @memberof Res_users
     */
    gold_badge?: any;

    /**
     * 相关的员工
     *
     * @returns {*}
     * @memberof Res_users
     */
    employee_ids?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Res_users
     */
    state?: any;

    /**
     * 管理频道
     *
     * @returns {*}
     * @memberof Res_users
     */
    moderation_channel_ids?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_channel_ids?: any;

    /**
     * 银质徽章个数
     *
     * @returns {*}
     * @memberof Res_users
     */
    silver_badge?: any;

    /**
     * 付款令牌
     *
     * @returns {*}
     * @memberof Res_users
     */
    payment_token_ids?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_users
     */
    display_name?: any;

    /**
     * 公司数量
     *
     * @returns {*}
     * @memberof Res_users
     */
    companies_count?: any;

    /**
     * 销售订单目标发票
     *
     * @returns {*}
     * @memberof Res_users
     */
    target_sales_invoiced?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_follower_ids?: any;

    /**
     * 最后连接
     *
     * @returns {*}
     * @memberof Res_users
     */
    login_date?: any;

    /**
     * 渠道
     *
     * @returns {*}
     * @memberof Res_users
     */
    channel_ids?: any;

    /**
     * 群组
     *
     * @returns {*}
     * @memberof Res_users
     */
    groups_id?: any;

    /**
     * 共享用户
     *
     * @returns {*}
     * @memberof Res_users
     */
    share?: any;

    /**
     * 银行
     *
     * @returns {*}
     * @memberof Res_users
     */
    bank_ids?: any;

    /**
     * 销售订单
     *
     * @returns {*}
     * @memberof Res_users
     */
    sale_order_ids?: any;

    /**
     * 设置密码
     *
     * @returns {*}
     * @memberof Res_users
     */
    new_password?: any;

    /**
     * OdooBot 状态
     *
     * @returns {*}
     * @memberof Res_users
     */
    odoobot_state?: any;

    /**
     * 公司是指业务伙伴
     *
     * @returns {*}
     * @memberof Res_users
     */
    ref_company_ids?: any;

    /**
     * 密码
     *
     * @returns {*}
     * @memberof Res_users
     */
    password?: any;

    /**
     * 青铜徽章数目
     *
     * @returns {*}
     * @memberof Res_users
     */
    bronze_badge?: any;

    /**
     * 会议
     *
     * @returns {*}
     * @memberof Res_users
     */
    meeting_ids?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_users
     */
    __last_update?: any;

    /**
     * 待发布的帖子
     *
     * @returns {*}
     * @memberof Res_users
     */
    forum_waiting_posts_count?: any;

    /**
     * 目标
     *
     * @returns {*}
     * @memberof Res_users
     */
    goal_ids?: any;

    /**
     * 签单是商机的最终目标
     *
     * @returns {*}
     * @memberof Res_users
     */
    target_sales_won?: any;

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_message_ids?: any;

    /**
     * 主页动作
     *
     * @returns {*}
     * @memberof Res_users
     */
    action_id?: any;

    /**
     * 登录
     *
     * @returns {*}
     * @memberof Res_users
     */
    login?: any;

    /**
     * 客户合同
     *
     * @returns {*}
     * @memberof Res_users
     */
    contract_ids?: any;

    /**
     * 审核数
     *
     * @returns {*}
     * @memberof Res_users
     */
    moderation_counter?: any;

    /**
     * 签名
     *
     * @returns {*}
     * @memberof Res_users
     */
    signature?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Res_users
     */
    user_ids?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Res_users
     */
    opportunity_ids?: any;

    /**
     * 任务
     *
     * @returns {*}
     * @memberof Res_users
     */
    task_ids?: any;

    /**
     * 发票
     *
     * @returns {*}
     * @memberof Res_users
     */
    invoice_ids?: any;

    /**
     * 内部参考
     *
     * @returns {*}
     * @memberof Res_users
     */
    ref?: any;

    /**
     * 错误个数
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_has_error_counter?: any;

    /**
     * 最近的在线销售订单
     *
     * @returns {*}
     * @memberof Res_users
     */
    last_website_so_id?: any;

    /**
     * 日期
     *
     * @returns {*}
     * @memberof Res_users
     */
    date?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_is_follower?: any;

    /**
     * 客户位置
     *
     * @returns {*}
     * @memberof Res_users
     */
    property_stock_customer?: any;

    /**
     * 最近的发票和付款匹配时间
     *
     * @returns {*}
     * @memberof Res_users
     */
    last_time_entries_checked?: any;

    /**
     * 语言
     *
     * @returns {*}
     * @memberof Res_users
     */
    lang?: any;

    /**
     * 销售警告
     *
     * @returns {*}
     * @memberof Res_users
     */
    sale_warn?: any;

    /**
     * #会议
     *
     * @returns {*}
     * @memberof Res_users
     */
    meeting_count?: any;

    /**
     * 街道
     *
     * @returns {*}
     * @memberof Res_users
     */
    street?: any;

    /**
     * 发票
     *
     * @returns {*}
     * @memberof Res_users
     */
    invoice_warn?: any;

    /**
     * 注册令牌 Token
     *
     * @returns {*}
     * @memberof Res_users
     */
    signup_token?: any;

    /**
     * # 任务
     *
     * @returns {*}
     * @memberof Res_users
     */
    task_count?: any;

    /**
     * 注册令牌（ Token  ）是有效的
     *
     * @returns {*}
     * @memberof Res_users
     */
    signup_valid?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_unread_counter?: any;

    /**
     * 注册令牌（Token）类型
     *
     * @returns {*}
     * @memberof Res_users
     */
    signup_type?: any;

    /**
     * 应收账款
     *
     * @returns {*}
     * @memberof Res_users
     */
    property_account_receivable_id?: any;

    /**
     * 网站opengraph图像
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_meta_og_img?: any;

    /**
     * 活动
     *
     * @returns {*}
     * @memberof Res_users
     */
    event_count?: any;

    /**
     * 日记账项目
     *
     * @returns {*}
     * @memberof Res_users
     */
    journal_item_count?: any;

    /**
     * 上级名称
     *
     * @returns {*}
     * @memberof Res_users
     */
    parent_name?: any;

    /**
     * 用户的销售团队
     *
     * @returns {*}
     * @memberof Res_users
     */
    sale_team_id_text?: any;

    /**
     * 应收总计
     *
     * @returns {*}
     * @memberof Res_users
     */
    credit?: any;

    /**
     * 商机
     *
     * @returns {*}
     * @memberof Res_users
     */
    opportunity_count?: any;

    /**
     * 注册网址
     *
     * @returns {*}
     * @memberof Res_users
     */
    signup_url?: any;

    /**
     * 应付账款
     *
     * @returns {*}
     * @memberof Res_users
     */
    property_account_payable_id?: any;

    /**
     * 省/ 州
     *
     * @returns {*}
     * @memberof Res_users
     */
    state_id?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_has_error?: any;

    /**
     * 销售订单消息
     *
     * @returns {*}
     * @memberof Res_users
     */
    sale_warn_msg?: any;

    /**
     * 在当前网站显示
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_published?: any;

    /**
     * 已开票总计
     *
     * @returns {*}
     * @memberof Res_users
     */
    total_invoiced?: any;

    /**
     * 应付总计
     *
     * @returns {*}
     * @memberof Res_users
     */
    debit?: any;

    /**
     * 销售点订单计数
     *
     * @returns {*}
     * @memberof Res_users
     */
    pos_order_count?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_users
     */
    write_uid_text?: any;

    /**
     * 称谓
     *
     * @returns {*}
     * @memberof Res_users
     */
    title?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_unread?: any;

    /**
     * ＃供应商账单
     *
     * @returns {*}
     * @memberof Res_users
     */
    supplier_invoice_count?: any;

    /**
     * 城市
     *
     * @returns {*}
     * @memberof Res_users
     */
    city?: any;

    /**
     * 库存拣货
     *
     * @returns {*}
     * @memberof Res_users
     */
    picking_warn?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_main_attachment_id?: any;

    /**
     * 责任用户
     *
     * @returns {*}
     * @memberof Res_users
     */
    activity_user_id?: any;

    /**
     * 附加信息
     *
     * @returns {*}
     * @memberof Res_users
     */
    additional_info?: any;

    /**
     * 工作岗位
     *
     * @returns {*}
     * @memberof Res_users
     */
    function?: any;

    /**
     * 网站网址
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_url?: any;

    /**
     * 最后的提醒已经标志为已读
     *
     * @returns {*}
     * @memberof Res_users
     */
    calendar_last_notif_ack?: any;

    /**
     * 操作次数
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_needaction_counter?: any;

    /**
     * 下一活动截止日期
     *
     * @returns {*}
     * @memberof Res_users
     */
    activity_date_deadline?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Res_users
     */
    employee?: any;

    /**
     * 活动状态
     *
     * @returns {*}
     * @memberof Res_users
     */
    activity_state?: any;

    /**
     * 条码
     *
     * @returns {*}
     * @memberof Res_users
     */
    barcode?: any;

    /**
     * 公司数据库ID
     *
     * @returns {*}
     * @memberof Res_users
     */
    partner_gid?: any;

    /**
     * 地址类型
     *
     * @returns {*}
     * @memberof Res_users
     */
    type?: any;

    /**
     * 税号
     *
     * @returns {*}
     * @memberof Res_users
     */
    vat?: any;

    /**
     * 采购订单消息
     *
     * @returns {*}
     * @memberof Res_users
     */
    purchase_warn_msg?: any;

    /**
     * 便签
     *
     * @returns {*}
     * @memberof Res_users
     */
    comment?: any;

    /**
     * 网站meta关键词
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_meta_keywords?: any;

    /**
     * 关联公司
     *
     * @returns {*}
     * @memberof Res_users
     */
    parent_id?: any;

    /**
     * 采购订单
     *
     * @returns {*}
     * @memberof Res_users
     */
    purchase_warn?: any;

    /**
     * 激活的合作伙伴
     *
     * @returns {*}
     * @memberof Res_users
     */
    active_partner?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_users
     */
    currency_id?: any;

    /**
     * 工业
     *
     * @returns {*}
     * @memberof Res_users
     */
    industry_id?: any;

    /**
     * 供应商位置
     *
     * @returns {*}
     * @memberof Res_users
     */
    property_stock_supplier?: any;

    /**
     * 付款令牌计数
     *
     * @returns {*}
     * @memberof Res_users
     */
    payment_token_count?: any;

    /**
     * 前置操作
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_needaction?: any;

    /**
     * 销售员
     *
     * @returns {*}
     * @memberof Res_users
     */
    user_id?: any;

    /**
     * 客户付款条款
     *
     * @returns {*}
     * @memberof Res_users
     */
    property_payment_term_id?: any;

    /**
     * 下一活动类型
     *
     * @returns {*}
     * @memberof Res_users
     */
    activity_type_id?: any;

    /**
     * 合同统计
     *
     * @returns {*}
     * @memberof Res_users
     */
    contracts_count?: any;

    /**
     * 自己
     *
     * @returns {*}
     * @memberof Res_users
     */
    self?: any;

    /**
     * 网站元说明
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_meta_description?: any;

    /**
     * 图像
     *
     * @returns {*}
     * @memberof Res_users
     */
    image?: any;

    /**
     * EMail
     *
     * @returns {*}
     * @memberof Res_users
     */
    email?: any;

    /**
     * 中等尺寸图像
     *
     * @returns {*}
     * @memberof Res_users
     */
    image_medium?: any;

    /**
     * 下一个活动摘要
     *
     * @returns {*}
     * @memberof Res_users
     */
    activity_summary?: any;

    /**
     * 应付限额
     *
     * @returns {*}
     * @memberof Res_users
     */
    debit_limit?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_users
     */
    country_id?: any;

    /**
     * 信用额度
     *
     * @returns {*}
     * @memberof Res_users
     */
    credit_limit?: any;

    /**
     * 公司名称实体
     *
     * @returns {*}
     * @memberof Res_users
     */
    commercial_company_name?: any;

    /**
     * 发票消息
     *
     * @returns {*}
     * @memberof Res_users
     */
    invoice_warn_msg?: any;

    /**
     * 已发布
     *
     * @returns {*}
     * @memberof Res_users
     */
    is_published?: any;

    /**
     * 对此债务人的信任度
     *
     * @returns {*}
     * @memberof Res_users
     */
    trust?: any;

    /**
     * 手机
     *
     * @returns {*}
     * @memberof Res_users
     */
    mobile?: any;

    /**
     * 格式化的邮件
     *
     * @returns {*}
     * @memberof Res_users
     */
    email_formatted?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_users
     */
    is_company?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_users
     */
    create_uid_text?: any;

    /**
     * 销售团队
     *
     * @returns {*}
     * @memberof Res_users
     */
    team_id?: any;

    /**
     * 黑名单
     *
     * @returns {*}
     * @memberof Res_users
     */
    is_blacklisted?: any;

    /**
     * 银行
     *
     * @returns {*}
     * @memberof Res_users
     */
    bank_account_count?: any;

    /**
     * 价格表
     *
     * @returns {*}
     * @memberof Res_users
     */
    property_product_pricelist?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Res_users
     */
    name?: any;

    /**
     * 库存拣货单消息
     *
     * @returns {*}
     * @memberof Res_users
     */
    picking_warn_msg?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_attachment_count?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Res_users
     */
    website?: any;

    /**
     * 电话
     *
     * @returns {*}
     * @memberof Res_users
     */
    phone?: any;

    /**
     * 街道 2
     *
     * @returns {*}
     * @memberof Res_users
     */
    street2?: any;

    /**
     * 有未核销的分录
     *
     * @returns {*}
     * @memberof Res_users
     */
    has_unreconciled_entries?: any;

    /**
     * 注册到期
     *
     * @returns {*}
     * @memberof Res_users
     */
    signup_expiration?: any;

    /**
     * 时区
     *
     * @returns {*}
     * @memberof Res_users
     */
    tz?: any;

    /**
     * 完整地址
     *
     * @returns {*}
     * @memberof Res_users
     */
    contact_address?: any;

    /**
     * 网站业务伙伴简介
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_short_description?: any;

    /**
     * 共享合作伙伴
     *
     * @returns {*}
     * @memberof Res_users
     */
    partner_share?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_users
     */
    company_id_text?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Res_users
     */
    color?: any;

    /**
     * 邮政编码
     *
     * @returns {*}
     * @memberof Res_users
     */
    zip?: any;

    /**
     * 销售订单个数
     *
     * @returns {*}
     * @memberof Res_users
     */
    sale_order_count?: any;

    /**
     * 公司类别
     *
     * @returns {*}
     * @memberof Res_users
     */
    company_type?: any;

    /**
     * 税科目调整
     *
     * @returns {*}
     * @memberof Res_users
     */
    property_account_position_id?: any;

    /**
     * SEO优化
     *
     * @returns {*}
     * @memberof Res_users
     */
    is_seo_optimized?: any;

    /**
     * 退回
     *
     * @returns {*}
     * @memberof Res_users
     */
    message_bounce?: any;

    /**
     * 网站meta标题
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_meta_title?: any;

    /**
     * 小尺寸图像
     *
     * @returns {*}
     * @memberof Res_users
     */
    image_small?: any;

    /**
     * 供应商货币
     *
     * @returns {*}
     * @memberof Res_users
     */
    property_purchase_currency_id?: any;

    /**
     * 采购订单数
     *
     * @returns {*}
     * @memberof Res_users
     */
    purchase_order_count?: any;

    /**
     * 网站业务伙伴的详细说明
     *
     * @returns {*}
     * @memberof Res_users
     */
    website_description?: any;

    /**
     * 供应商付款条款
     *
     * @returns {*}
     * @memberof Res_users
     */
    property_supplier_payment_term_id?: any;

    /**
     * 商业实体
     *
     * @returns {*}
     * @memberof Res_users
     */
    commercial_partner_id?: any;

    /**
     * 安全联系人别名
     *
     * @returns {*}
     * @memberof Res_users
     */
    alias_contact?: any;

    /**
     * 公司名称
     *
     * @returns {*}
     * @memberof Res_users
     */
    company_name?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Res_users
     */
    company_id?: any;

    /**
     * 相关的业务伙伴
     *
     * @returns {*}
     * @memberof Res_users
     */
    partner_id?: any;

    /**
     * 别名
     *
     * @returns {*}
     * @memberof Res_users
     */
    alias_id?: any;

    /**
     * 用户的销售团队
     *
     * @returns {*}
     * @memberof Res_users
     */
    sale_team_id?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_users
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_users
     */
    create_uid?: any;
}
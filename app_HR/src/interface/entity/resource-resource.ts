/**
 * 资源
 *
 * @export
 * @interface Resource_resource
 */
export interface Resource_resource {

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    __last_update?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    id?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    name?: any;

    /**
     * 效率因子
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    time_efficiency?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    create_date?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    active?: any;

    /**
     * 资源类型
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    resource_type?: any;

    /**
     * 时区
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    tz?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    write_date?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    display_name?: any;

    /**
     * 工作时间
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    calendar_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    write_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    company_id_text?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    user_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    create_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    create_uid?: any;

    /**
     * 工作时间
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    calendar_id?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    company_id?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    user_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Resource_resource
     */
    write_uid?: any;
}
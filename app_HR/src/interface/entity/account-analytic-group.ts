/**
 * 分析类别
 *
 * @export
 * @interface Account_analytic_group
 */
export interface Account_analytic_group {

    /**
     * ID
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    id?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    __last_update?: any;

    /**
     * 父级路径
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    parent_path?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    create_date?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    display_name?: any;

    /**
     * 完整名称
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    complete_name?: any;

    /**
     * 说明
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    description?: any;

    /**
     * 下级
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    children_ids?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    write_date?: any;

    /**
     * 上级
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    parent_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    write_uid_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    create_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    company_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    create_uid?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    company_id?: any;

    /**
     * 上级
     *
     * @returns {*}
     * @memberof Account_analytic_group
     */
    parent_id?: any;
}
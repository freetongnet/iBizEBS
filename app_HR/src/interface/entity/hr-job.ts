/**
 * 工作岗位
 *
 * @export
 * @interface Hr_job
 */
export interface Hr_job {

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_is_follower?: any;

    /**
     * 当前员工数量
     *
     * @returns {*}
     * @memberof Hr_job
     */
    no_of_employee?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Hr_job
     */
    state?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_partner_ids?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Hr_job
     */
    __last_update?: any;

    /**
     * 已雇用员工
     *
     * @returns {*}
     * @memberof Hr_job
     */
    no_of_hired_employee?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_unread_counter?: any;

    /**
     * 网站元说明
     *
     * @returns {*}
     * @memberof Hr_job
     */
    website_meta_description?: any;

    /**
     * 期望的新员工
     *
     * @returns {*}
     * @memberof Hr_job
     */
    no_of_recruitment?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_job
     */
    write_date?: any;

    /**
     * SEO优化
     *
     * @returns {*}
     * @memberof Hr_job
     */
    is_seo_optimized?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_channel_ids?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_main_attachment_id?: any;

    /**
     * 工作岗位
     *
     * @returns {*}
     * @memberof Hr_job
     */
    name?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Hr_job
     */
    display_name?: any;

    /**
     * 工作说明
     *
     * @returns {*}
     * @memberof Hr_job
     */
    description?: any;

    /**
     * 网站网址
     *
     * @returns {*}
     * @memberof Hr_job
     */
    website_url?: any;

    /**
     * 要求
     *
     * @returns {*}
     * @memberof Hr_job
     */
    requirements?: any;

    /**
     * 网站meta标题
     *
     * @returns {*}
     * @memberof Hr_job
     */
    website_meta_title?: any;

    /**
     * 应用数量
     *
     * @returns {*}
     * @memberof Hr_job
     */
    application_count?: any;

    /**
     * 求职申请
     *
     * @returns {*}
     * @memberof Hr_job
     */
    application_ids?: any;

    /**
     * 已发布
     *
     * @returns {*}
     * @memberof Hr_job
     */
    is_published?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_has_error_counter?: any;

    /**
     * 网站opengraph图像
     *
     * @returns {*}
     * @memberof Hr_job
     */
    website_meta_og_img?: any;

    /**
     * 预计员工数合计
     *
     * @returns {*}
     * @memberof Hr_job
     */
    expected_employees?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_job
     */
    create_date?: any;

    /**
     * 在当前网站显示
     *
     * @returns {*}
     * @memberof Hr_job
     */
    website_published?: any;

    /**
     * 信息
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_ids?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_attachment_count?: any;

    /**
     * 网站说明
     *
     * @returns {*}
     * @memberof Hr_job
     */
    website_description?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_job
     */
    id?: any;

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Hr_job
     */
    website_message_ids?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Hr_job
     */
    color?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_follower_ids?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_unread?: any;

    /**
     * 文档
     *
     * @returns {*}
     * @memberof Hr_job
     */
    document_ids?: any;

    /**
     * 需要采取行动
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_needaction?: any;

    /**
     * 行动数量
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_needaction_counter?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Hr_job
     */
    message_has_error?: any;

    /**
     * 员工
     *
     * @returns {*}
     * @memberof Hr_job
     */
    employee_ids?: any;

    /**
     * 文档数
     *
     * @returns {*}
     * @memberof Hr_job
     */
    documents_count?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Hr_job
     */
    website_id?: any;

    /**
     * 网站meta关键词
     *
     * @returns {*}
     * @memberof Hr_job
     */
    website_meta_keywords?: any;

    /**
     * 默认值
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_defaults?: any;

    /**
     * 别名
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_name?: any;

    /**
     * 记录线索ID
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_force_thread_id?: any;

    /**
     * 网域别名
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_domain?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_job
     */
    write_uid_text?: any;

    /**
     * 上级模型
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_parent_model_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_job
     */
    create_uid_text?: any;

    /**
     * 面试表单
     *
     * @returns {*}
     * @memberof Hr_job
     */
    survey_title?: any;

    /**
     * 上级记录ID
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_parent_thread_id?: any;

    /**
     * 模型别名
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_model_id?: any;

    /**
     * 工作地点
     *
     * @returns {*}
     * @memberof Hr_job
     */
    address_id_text?: any;

    /**
     * 所有者
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_user_id?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof Hr_job
     */
    department_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_job
     */
    company_id_text?: any;

    /**
     * 招聘负责人
     *
     * @returns {*}
     * @memberof Hr_job
     */
    user_id_text?: any;

    /**
     * 部门经理
     *
     * @returns {*}
     * @memberof Hr_job
     */
    manager_id_text?: any;

    /**
     * 别名联系人安全
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_contact?: any;

    /**
     * 人力资源主管
     *
     * @returns {*}
     * @memberof Hr_job
     */
    hr_responsible_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_job
     */
    company_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_job
     */
    write_uid?: any;

    /**
     * 部门经理
     *
     * @returns {*}
     * @memberof Hr_job
     */
    manager_id?: any;

    /**
     * 工作地点
     *
     * @returns {*}
     * @memberof Hr_job
     */
    address_id?: any;

    /**
     * 面试表单
     *
     * @returns {*}
     * @memberof Hr_job
     */
    survey_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_job
     */
    create_uid?: any;

    /**
     * 人力资源主管
     *
     * @returns {*}
     * @memberof Hr_job
     */
    hr_responsible_id?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof Hr_job
     */
    department_id?: any;

    /**
     * 别名
     *
     * @returns {*}
     * @memberof Hr_job
     */
    alias_id?: any;

    /**
     * 招聘负责人
     *
     * @returns {*}
     * @memberof Hr_job
     */
    user_id?: any;
}
/**
 * HR 部门
 *
 * @export
 * @interface Hr_department
 */
export interface Hr_department {

    /**
     * 需要采取行动
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_needaction?: any;

    /**
     * 关注者
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_follower_ids?: any;

    /**
     * 今日缺勤
     *
     * @returns {*}
     * @memberof Hr_department
     */
    absence_of_today?: any;

    /**
     * 未读消息
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_unread?: any;

    /**
     * 待批准休假
     *
     * @returns {*}
     * @memberof Hr_department
     */
    leave_to_approve_count?: any;

    /**
     * 颜色索引
     *
     * @returns {*}
     * @memberof Hr_department
     */
    color?: any;

    /**
     * 关注者(业务伙伴)
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_partner_ids?: any;

    /**
     * 附件
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_main_attachment_id?: any;

    /**
     * 未读消息计数器
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_unread_counter?: any;

    /**
     * 部门名称
     *
     * @returns {*}
     * @memberof Hr_department
     */
    name?: any;

    /**
     * 是关注者
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_is_follower?: any;

    /**
     * 有效
     *
     * @returns {*}
     * @memberof Hr_department
     */
    active?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_department
     */
    write_date?: any;

    /**
     * 待批准的费用报告
     *
     * @returns {*}
     * @memberof Hr_department
     */
    expense_sheets_to_approve_count?: any;

    /**
     * 待批准的分配
     *
     * @returns {*}
     * @memberof Hr_department
     */
    allocation_to_approve_count?: any;

    /**
     * 新雇用的员工
     *
     * @returns {*}
     * @memberof Hr_department
     */
    new_hired_employee?: any;

    /**
     * 笔记
     *
     * @returns {*}
     * @memberof Hr_department
     */
    note?: any;

    /**
     * 子部门
     *
     * @returns {*}
     * @memberof Hr_department
     */
    child_ids?: any;

    /**
     * 关注者(渠道)
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_channel_ids?: any;

    /**
     * 错误数
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_has_error_counter?: any;

    /**
     * 预期的员工
     *
     * @returns {*}
     * @memberof Hr_department
     */
    expected_employee?: any;

    /**
     * 网站消息
     *
     * @returns {*}
     * @memberof Hr_department
     */
    website_message_ids?: any;

    /**
     * 消息递送错误
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_has_error?: any;

    /**
     * 员工总数
     *
     * @returns {*}
     * @memberof Hr_department
     */
    total_employee?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Hr_department
     */
    __last_update?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_department
     */
    id?: any;

    /**
     * 工作
     *
     * @returns {*}
     * @memberof Hr_department
     */
    jobs_ids?: any;

    /**
     * 信息
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_ids?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Hr_department
     */
    display_name?: any;

    /**
     * 附件数量
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_attachment_count?: any;

    /**
     * 新申请
     *
     * @returns {*}
     * @memberof Hr_department
     */
    new_applicant_count?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_department
     */
    create_date?: any;

    /**
     * 完整名称
     *
     * @returns {*}
     * @memberof Hr_department
     */
    complete_name?: any;

    /**
     * 行动数量
     *
     * @returns {*}
     * @memberof Hr_department
     */
    message_needaction_counter?: any;

    /**
     * 会员
     *
     * @returns {*}
     * @memberof Hr_department
     */
    member_ids?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_department
     */
    write_uid_text?: any;

    /**
     * 经理
     *
     * @returns {*}
     * @memberof Hr_department
     */
    manager_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_department
     */
    create_uid_text?: any;

    /**
     * 上级部门
     *
     * @returns {*}
     * @memberof Hr_department
     */
    parent_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_department
     */
    company_id_text?: any;

    /**
     * 经理
     *
     * @returns {*}
     * @memberof Hr_department
     */
    manager_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_department
     */
    create_uid?: any;

    /**
     * 上级部门
     *
     * @returns {*}
     * @memberof Hr_department
     */
    parent_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_department
     */
    write_uid?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Hr_department
     */
    company_id?: any;
}
/**
 * 科目
 *
 * @export
 * @interface Account_account
 */
export interface Account_account {

    /**
     * ID
     *
     * @returns {*}
     * @memberof Account_account
     */
    id?: any;

    /**
     * 期初借方
     *
     * @returns {*}
     * @memberof Account_account
     */
    opening_debit?: any;

    /**
     * 内部备注
     *
     * @returns {*}
     * @memberof Account_account
     */
    note?: any;

    /**
     * 期初贷方
     *
     * @returns {*}
     * @memberof Account_account
     */
    opening_credit?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Account_account
     */
    create_date?: any;

    /**
     * 代码
     *
     * @returns {*}
     * @memberof Account_account
     */
    code?: any;

    /**
     * 默认税
     *
     * @returns {*}
     * @memberof Account_account
     */
    tax_ids?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Account_account
     */
    write_date?: any;

    /**
     * 废弃
     *
     * @returns {*}
     * @memberof Account_account
     */
    deprecated?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Account_account
     */
    display_name?: any;

    /**
     * 允许核销
     *
     * @returns {*}
     * @memberof Account_account
     */
    reconcile?: any;

    /**
     * 标签
     *
     * @returns {*}
     * @memberof Account_account
     */
    tag_ids?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Account_account
     */
    name?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Account_account
     */
    __last_update?: any;

    /**
     * 内部类型
     *
     * @returns {*}
     * @memberof Account_account
     */
    internal_type?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_account
     */
    create_uid_text?: any;

    /**
     * 科目币种
     *
     * @returns {*}
     * @memberof Account_account
     */
    currency_id_text?: any;

    /**
     * 内部群组
     *
     * @returns {*}
     * @memberof Account_account
     */
    internal_group?: any;

    /**
     * 组
     *
     * @returns {*}
     * @memberof Account_account
     */
    group_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_account
     */
    write_uid_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_account
     */
    company_id_text?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Account_account
     */
    user_type_id_text?: any;

    /**
     * 公司
     *
     * @returns {*}
     * @memberof Account_account
     */
    company_id?: any;

    /**
     * 组
     *
     * @returns {*}
     * @memberof Account_account
     */
    group_id?: any;

    /**
     * 类型
     *
     * @returns {*}
     * @memberof Account_account
     */
    user_type_id?: any;

    /**
     * 科目币种
     *
     * @returns {*}
     * @memberof Account_account
     */
    currency_id?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Account_account
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Account_account
     */
    create_uid?: any;
}
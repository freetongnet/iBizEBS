/**
 * 游戏化目标
 *
 * @export
 * @interface Gamification_goal
 */
export interface Gamification_goal {

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    start_date?: any;

    /**
     * 更新
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    to_update?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    write_date?: any;

    /**
     * 提醒延迟
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    remind_update_delay?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    state?: any;

    /**
     * 完整性
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    completeness?: any;

    /**
     * 达到
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    target_goal?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    create_date?: any;

    /**
     * 关闭的目标
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    closed?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    id?: any;

    /**
     * 最近更新
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    last_update?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    display_name?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    end_date?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    __last_update?: any;

    /**
     * 当前值
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    current?: any;

    /**
     * 定义说明
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    definition_description?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    create_uid_text?: any;

    /**
     * 后缀
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    definition_suffix?: any;

    /**
     * 挑战
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    challenge_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    write_uid_text?: any;

    /**
     * 目标定义
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    definition_id_text?: any;

    /**
     * 显示为
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    definition_display?: any;

    /**
     * 挑战行
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    line_id_text?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    user_id_text?: any;

    /**
     * 目标绩效
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    definition_condition?: any;

    /**
     * 计算模式
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    computation_mode?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    write_uid?: any;

    /**
     * 挑战行
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    line_id?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    create_uid?: any;

    /**
     * 目标定义
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    definition_id?: any;

    /**
     * 挑战
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    challenge_id?: any;

    /**
     * 用户
     *
     * @returns {*}
     * @memberof Gamification_goal
     */
    user_id?: any;
}
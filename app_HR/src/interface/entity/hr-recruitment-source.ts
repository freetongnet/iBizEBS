/**
 * 应聘者来源
 *
 * @export
 * @interface Hr_recruitment_source
 */
export interface Hr_recruitment_source {

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    __last_update?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    id?: any;

    /**
     * 网址参数
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    url?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    create_date?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    write_date?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    display_name?: any;

    /**
     * 招聘ID
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    job_id_text?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    write_uid_text?: any;

    /**
     * EMail
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    email?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    create_uid_text?: any;

    /**
     * 来源名称
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    name?: any;

    /**
     * 最后更新人
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    write_uid?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    create_uid?: any;

    /**
     * 来源
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    source_id?: any;

    /**
     * 别名ID
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    alias_id?: any;

    /**
     * 招聘ID
     *
     * @returns {*}
     * @memberof Hr_recruitment_source
     */
    job_id?: any;
}
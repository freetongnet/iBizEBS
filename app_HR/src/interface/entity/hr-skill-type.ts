/**
 * 技能类型
 *
 * @export
 * @interface Hr_skill_type
 */
export interface Hr_skill_type {

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Hr_skill_type
     */
    create_date?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Hr_skill_type
     */
    write_date?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_skill_type
     */
    id?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof Hr_skill_type
     */
    name?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_skill_type
     */
    write_uid?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Hr_skill_type
     */
    create_uid?: any;
}
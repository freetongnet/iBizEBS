/**
 * 公司
 *
 * @export
 * @interface Res_company
 */
export interface Res_company {

    /**
     * 公司口号
     *
     * @returns {*}
     * @memberof Res_company
     */
    report_header?: any;

    /**
     * 国家/地区
     *
     * @returns {*}
     * @memberof Res_company
     */
    country_id?: any;

    /**
     * 正进行销售面板的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    sale_quotation_onboarding_state?: any;

    /**
     * 默认报价有效期（日）
     *
     * @returns {*}
     * @memberof Res_company
     */
    quotation_validity_days?: any;

    /**
     * 有待被确认的发票步骤的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_onboarding_invoice_layout_state?: any;

    /**
     * 科目号码
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_no?: any;

    /**
     * 银行科目的前缀
     *
     * @returns {*}
     * @memberof Res_company
     */
    bank_account_code_prefix?: any;

    /**
     * 银行日记账
     *
     * @returns {*}
     * @memberof Res_company
     */
    bank_journal_ids?: any;

    /**
     * 邮政编码
     *
     * @returns {*}
     * @memberof Res_company
     */
    zip?: any;

    /**
     * 非顾问的锁定日期
     *
     * @returns {*}
     * @memberof Res_company
     */
    period_lock_date?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    state_id?: any;

    /**
     * 使用现金收付制
     *
     * @returns {*}
     * @memberof Res_company
     */
    tax_exigibility?: any;

    /**
     * 工作时间
     *
     * @returns {*}
     * @memberof Res_company
     */
    resource_calendar_ids?: any;

    /**
     * 银行账户
     *
     * @returns {*}
     * @memberof Res_company
     */
    bank_ids?: any;

    /**
     * 处于会计面板的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_dashboard_onboarding_state?: any;

    /**
     * 创建时间
     *
     * @returns {*}
     * @memberof Res_company
     */
    create_date?: any;

    /**
     * 颜色
     *
     * @returns {*}
     * @memberof Res_company
     */
    snailmail_color?: any;

    /**
     * 城市
     *
     * @returns {*}
     * @memberof Res_company
     */
    city?: any;

    /**
     * 科目状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_setup_coa_state?: any;

    /**
     * 预设邮件
     *
     * @returns {*}
     * @memberof Res_company
     */
    catchall?: any;

    /**
     * 显示名称
     *
     * @returns {*}
     * @memberof Res_company
     */
    display_name?: any;

    /**
     * 使用anglo-saxon会计
     *
     * @returns {*}
     * @memberof Res_company
     */
    anglo_saxon_accounting?: any;

    /**
     * 双面
     *
     * @returns {*}
     * @memberof Res_company
     */
    snailmail_duplex?: any;

    /**
     * GitHub账户
     *
     * @returns {*}
     * @memberof Res_company
     */
    social_github?: any;

    /**
     * 有待被确认的银行数据步骤的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_setup_bank_data_state?: any;

    /**
     * ID
     *
     * @returns {*}
     * @memberof Res_company
     */
    id?: any;

    /**
     * 街道 2
     *
     * @returns {*}
     * @memberof Res_company
     */
    street2?: any;

    /**
     * 预计会计科目表
     *
     * @returns {*}
     * @memberof Res_company
     */
    expects_chart_of_accounts?: any;

    /**
     * 转账帐户的前缀
     *
     * @returns {*}
     * @memberof Res_company
     */
    transfer_account_code_prefix?: any;

    /**
     * 会计年度的最后一天
     *
     * @returns {*}
     * @memberof Res_company
     */
    fiscalyear_last_day?: any;

    /**
     * 接受的用户
     *
     * @returns {*}
     * @memberof Res_company
     */
    user_ids?: any;

    /**
     * 银行核销阈值
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_bank_reconciliation_start?: any;

    /**
     * 在线支付
     *
     * @returns {*}
     * @memberof Res_company
     */
    portal_confirmation_pay?: any;

    /**
     * 显示SEPA QR码
     *
     * @returns {*}
     * @memberof Res_company
     */
    qr_code?: any;

    /**
     * 街道
     *
     * @returns {*}
     * @memberof Res_company
     */
    street?: any;

    /**
     * 处于会计发票面板的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_invoice_onboarding_state?: any;

    /**
     * 下级公司
     *
     * @returns {*}
     * @memberof Res_company
     */
    child_ids?: any;

    /**
     * 序号
     *
     * @returns {*}
     * @memberof Res_company
     */
    sequence?: any;

    /**
     * 命名规则
     *
     * @returns {*}
     * @memberof Res_company
     */
    nomenclature_id?: any;

    /**
     * 入职支付收单机构的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    payment_acquirer_onboarding_state?: any;

    /**
     * 报表页脚
     *
     * @returns {*}
     * @memberof Res_company
     */
    report_footer?: any;

    /**
     * 选择付款方式
     *
     * @returns {*}
     * @memberof Res_company
     */
    payment_onboarding_payment_method?: any;

    /**
     * 最后更新时间
     *
     * @returns {*}
     * @memberof Res_company
     */
    write_date?: any;

    /**
     * 批准等级
     *
     * @returns {*}
     * @memberof Res_company
     */
    po_double_validation?: any;

    /**
     * 采购提前时间
     *
     * @returns {*}
     * @memberof Res_company
     */
    po_lead?: any;

    /**
     * 有待被确认的样品报价单步骤的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    sale_onboarding_sample_quotation_state?: any;

    /**
     * 有待被确认的订单步骤的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    sale_onboarding_order_confirmation_state?: any;

    /**
     * 文档模板
     *
     * @returns {*}
     * @memberof Res_company
     */
    external_report_layout_id?: any;

    /**
     * 请选择付款方式
     *
     * @returns {*}
     * @memberof Res_company
     */
    sale_onboarding_payment_method?: any;

    /**
     * 有待被确认的样品报价单步骤的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_onboarding_sample_invoice_state?: any;

    /**
     * 公司状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    base_onboarding_company_state?: any;

    /**
     * 领英账号
     *
     * @returns {*}
     * @memberof Res_company
     */
    social_linkedin?: any;

    /**
     * 制造提前期(日)
     *
     * @returns {*}
     * @memberof Res_company
     */
    manufacturing_lead?: any;

    /**
     * 再次验证金额
     *
     * @returns {*}
     * @memberof Res_company
     */
    po_double_validation_amount?: any;

    /**
     * 销售订单修改
     *
     * @returns {*}
     * @memberof Res_company
     */
    po_lock?: any;

    /**
     * Twitter账号
     *
     * @returns {*}
     * @memberof Res_company
     */
    social_twitter?: any;

    /**
     * Instagram 账号
     *
     * @returns {*}
     * @memberof Res_company
     */
    social_instagram?: any;

    /**
     * 有待被确认的会计年度步骤的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_setup_fy_data_state?: any;

    /**
     * 税率计算的舍入方法
     *
     * @returns {*}
     * @memberof Res_company
     */
    tax_calculation_rounding_method?: any;

    /**
     * 现金科目的前缀
     *
     * @returns {*}
     * @memberof Res_company
     */
    cash_account_code_prefix?: any;

    /**
     * 有待被确认的报价单步骤的状态
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_onboarding_sale_tax_state?: any;

    /**
     * 销售安全天数
     *
     * @returns {*}
     * @memberof Res_company
     */
    security_lead?: any;

    /**
     * 招聘网站主题一步完成
     *
     * @returns {*}
     * @memberof Res_company
     */
    website_theme_onboarding_done?: any;

    /**
     * 通过默认值打印
     *
     * @returns {*}
     * @memberof Res_company
     */
    invoice_is_print?: any;

    /**
     * 公司注册
     *
     * @returns {*}
     * @memberof Res_company
     */
    company_registry?: any;

    /**
     * 最后修改日
     *
     * @returns {*}
     * @memberof Res_company
     */
    __last_update?: any;

    /**
     * 网页徽标
     *
     * @returns {*}
     * @memberof Res_company
     */
    logo_web?: any;

    /**
     * 锁定日期
     *
     * @returns {*}
     * @memberof Res_company
     */
    fiscalyear_lock_date?: any;

    /**
     * 默认以信件发送
     *
     * @returns {*}
     * @memberof Res_company
     */
    invoice_is_snailmail?: any;

    /**
     * 网站销售状态入职付款收单机构步骤
     *
     * @returns {*}
     * @memberof Res_company
     */
    website_sale_onboarding_payment_acquirer_state?: any;

    /**
     * 脸书账号
     *
     * @returns {*}
     * @memberof Res_company
     */
    social_facebook?: any;

    /**
     * 在线签名
     *
     * @returns {*}
     * @memberof Res_company
     */
    portal_confirmation_sign?: any;

    /**
     * 纸张格式
     *
     * @returns {*}
     * @memberof Res_company
     */
    paperformat_id?: any;

    /**
     * 会计年度的最后一个月
     *
     * @returns {*}
     * @memberof Res_company
     */
    fiscalyear_last_month?: any;

    /**
     * 默认邮件
     *
     * @returns {*}
     * @memberof Res_company
     */
    invoice_is_email?: any;

    /**
     * Youtube账号
     *
     * @returns {*}
     * @memberof Res_company
     */
    social_youtube?: any;

    /**
     * 汇率损失科目
     *
     * @returns {*}
     * @memberof Res_company
     */
    expense_currency_exchange_account_id?: any;

    /**
     * 公司数据库ID
     *
     * @returns {*}
     * @memberof Res_company
     */
    partner_gid?: any;

    /**
     * 电话
     *
     * @returns {*}
     * @memberof Res_company
     */
    phone?: any;

    /**
     * 公司 Logo
     *
     * @returns {*}
     * @memberof Res_company
     */
    logo?: any;

    /**
     * 库存计价的入库科目
     *
     * @returns {*}
     * @memberof Res_company
     */
    property_stock_account_input_categ_id_text?: any;

    /**
     * 默认进项税
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_purchase_tax_id_text?: any;

    /**
     * 公司名称
     *
     * @returns {*}
     * @memberof Res_company
     */
    name?: any;

    /**
     * 默认国际贸易术语
     *
     * @returns {*}
     * @memberof Res_company
     */
    incoterm_id_text?: any;

    /**
     * 期初日记账
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_opening_journal_id?: any;

    /**
     * 银行间转账科目
     *
     * @returns {*}
     * @memberof Res_company
     */
    transfer_account_id_text?: any;

    /**
     * 汇率增益科目
     *
     * @returns {*}
     * @memberof Res_company
     */
    income_currency_exchange_account_id?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_company
     */
    currency_id_text?: any;

    /**
     * 期初日期
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_opening_date?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_company
     */
    write_uid_text?: any;

    /**
     * 表模板
     *
     * @returns {*}
     * @memberof Res_company
     */
    chart_template_id_text?: any;

    /**
     * 默认销售税
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_sale_tax_id_text?: any;

    /**
     * 期初日记账分录
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_opening_move_id_text?: any;

    /**
     * EMail
     *
     * @returns {*}
     * @memberof Res_company
     */
    email?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_company
     */
    create_uid_text?: any;

    /**
     * 库存计价的出货科目
     *
     * @returns {*}
     * @memberof Res_company
     */
    property_stock_account_output_categ_id_text?: any;

    /**
     * 库存计价的科目模板
     *
     * @returns {*}
     * @memberof Res_company
     */
    property_stock_valuation_account_id_text?: any;

    /**
     * 上级公司
     *
     * @returns {*}
     * @memberof Res_company
     */
    parent_id_text?: any;

    /**
     * 现金收付制日记账
     *
     * @returns {*}
     * @memberof Res_company
     */
    tax_cash_basis_journal_id_text?: any;

    /**
     * 内部中转位置
     *
     * @returns {*}
     * @memberof Res_company
     */
    internal_transit_location_id_text?: any;

    /**
     * 网站
     *
     * @returns {*}
     * @memberof Res_company
     */
    website?: any;

    /**
     * 税号
     *
     * @returns {*}
     * @memberof Res_company
     */
    vat?: any;

    /**
     * 默认工作时间
     *
     * @returns {*}
     * @memberof Res_company
     */
    resource_calendar_id_text?: any;

    /**
     * 汇兑损益
     *
     * @returns {*}
     * @memberof Res_company
     */
    currency_exchange_journal_id_text?: any;

    /**
     * 创建人
     *
     * @returns {*}
     * @memberof Res_company
     */
    create_uid?: any;

    /**
     * 上级公司
     *
     * @returns {*}
     * @memberof Res_company
     */
    parent_id?: any;

    /**
     * 币种
     *
     * @returns {*}
     * @memberof Res_company
     */
    currency_id?: any;

    /**
     * 最后更新者
     *
     * @returns {*}
     * @memberof Res_company
     */
    write_uid?: any;

    /**
     * 库存计价的出货科目
     *
     * @returns {*}
     * @memberof Res_company
     */
    property_stock_account_output_categ_id?: any;

    /**
     * 库存计价的科目模板
     *
     * @returns {*}
     * @memberof Res_company
     */
    property_stock_valuation_account_id?: any;

    /**
     * 期初日记账分录
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_opening_move_id?: any;

    /**
     * 内部中转位置
     *
     * @returns {*}
     * @memberof Res_company
     */
    internal_transit_location_id?: any;

    /**
     * 默认进项税
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_purchase_tax_id?: any;

    /**
     * 表模板
     *
     * @returns {*}
     * @memberof Res_company
     */
    chart_template_id?: any;

    /**
     * 默认销售税
     *
     * @returns {*}
     * @memberof Res_company
     */
    account_sale_tax_id?: any;

    /**
     * 现金收付制日记账
     *
     * @returns {*}
     * @memberof Res_company
     */
    tax_cash_basis_journal_id?: any;

    /**
     * 库存计价的入库科目
     *
     * @returns {*}
     * @memberof Res_company
     */
    property_stock_account_input_categ_id?: any;

    /**
     * 业务伙伴
     *
     * @returns {*}
     * @memberof Res_company
     */
    partner_id?: any;

    /**
     * 默认国际贸易术语
     *
     * @returns {*}
     * @memberof Res_company
     */
    incoterm_id?: any;

    /**
     * 默认工作时间
     *
     * @returns {*}
     * @memberof Res_company
     */
    resource_calendar_id?: any;

    /**
     * 银行间转账科目
     *
     * @returns {*}
     * @memberof Res_company
     */
    transfer_account_id?: any;

    /**
     * 汇兑损益
     *
     * @returns {*}
     * @memberof Res_company
     */
    currency_exchange_journal_id?: any;
}
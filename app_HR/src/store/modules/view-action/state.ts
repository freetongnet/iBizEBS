/**
 * 所有应用视图
 */
export const viewstate: any = {
    appviews: [
        {
            viewtag: '0561ac50742161ab8956b4a9f7f1e04d',
            viewmodule: 'odoo_hr',
            viewname: 'hr_skill_levelEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '0626393582cbb7fd983d27f9b1341670',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employeePickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '0c28c7d1b5467ef547acaa3fd50ad707',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageSendView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '107307dcd3d80f3c8ff51c8a8894baba',
            viewmodule: 'odoo_hr',
            viewname: 'hr_skill_typeBasicEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'ff6183ee62410e029fffc2d2b36b2a0d',
                '665c79d3b2552f269e26be742c2ff944',
            ],
        },
        {
            viewtag: '1bf7794526c1642df10a2b9788dfd850',
            viewmodule: 'odoo_hr',
            viewname: 'account_analytic_lineLine',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '62db01f049434fe499fc2230f67134ad',
            ],
        },
        {
            viewtag: '207f2394ba3237b0cd0125199df45312',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employeeMasterTabInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '1bf7794526c1642df10a2b9788dfd850',
                'c8c4a945338c7b14ac11d56b9d322006',
                '5fdb423d24542aebde2d32f0104eadf2',
                '490a981f9c39f68d146cbde197dab3dc',
                'dd123e093294210ed5319ccd063709c0',
                'c81bf4a79286794aba838b3e058622de',
            ],
        },
        {
            viewtag: '2d28144e9daaba4cd6615cbdb6ae960e',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '32b931d5314f23e469988db3b5e71ab5',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employeeMasterGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '207f2394ba3237b0cd0125199df45312',
                'fc5d8102daf9c9af51e2414dab7fdb11',
            ],
        },
        {
            viewtag: '33b31adec9be196e954c1b75e4ff6e5c',
            viewmodule: 'odoo_mail',
            viewname: 'mail_activityEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '371605d75479a65a8a70a01c0264ca9f',
            viewmodule: 'odoo_hr',
            viewname: 'hr_resume_line_typeEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '3d90688394c225f742436305b4f54a92',
            viewmodule: 'odoo_base',
            viewname: 'res_partnerPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '2d28144e9daaba4cd6615cbdb6ae960e',
            ],
        },
        {
            viewtag: '40d3ef7b5f091b9a38d760e4f7e6fd88',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employee_categoryEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '44e8aa5916fee576739592ea43587884',
            viewmodule: 'odoo_hr',
            viewname: 'hr_jobMasterQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '820c1678c0c8d641e261ae2dca273657',
                'bab2a924e4f1f6f783c1c15be5f2b64b',
                '69e4c3d30d14aa7afbc44c1f9cc3b887',
                '765a695b889be2aa0520261276422fb9',
                '89f22e53f7ad7ee53bf0abd030269dfe',
                '62f15d7b41688fbe1c3c98652da3c298',
                '3d90688394c225f742436305b4f54a92',
            ],
        },
        {
            viewtag: '45f8206e08dc28bf540d375340cab3af',
            viewmodule: 'odoo_maintenance',
            viewname: 'maintenance_equipmentEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '485b88fa6da5e97d044e52b8947b7e58',
            viewmodule: 'odoo_hr',
            viewname: 'hr_jobPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'ed9e03dd6f3e139a520f139d5bad9496',
            ],
        },
        {
            viewtag: '48b1a2b2892a307999e9d85527673c1a',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employeePickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '0626393582cbb7fd983d27f9b1341670',
            ],
        },
        {
            viewtag: '48c00dfd1c27d1c90438e9ae1d715847',
            viewmodule: 'odoo_hr',
            viewname: 'hr_jobMasterEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '820c1678c0c8d641e261ae2dca273657',
                '69e4c3d30d14aa7afbc44c1f9cc3b887',
                '765a695b889be2aa0520261276422fb9',
                '89f22e53f7ad7ee53bf0abd030269dfe',
                '3d90688394c225f742436305b4f54a92',
            ],
        },
        {
            viewtag: '490a981f9c39f68d146cbde197dab3dc',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employeeMasterSummaryView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'f3651c83b6944418c948f91616a23eb9',
                'b0816566792fd556e775203e4dfa964b',
            ],
        },
        {
            viewtag: '494085a60115c0ef1d0fd25765405dbb',
            viewmodule: 'odoo_hr',
            viewname: 'hr_skillEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '4bf6e06e5af9074336f57d7489625777',
            viewmodule: 'odoo_resource',
            viewname: 'resource_calendarPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '4c7c4d76ebd6de40890b94d8874e6a1a',
            viewmodule: 'odoo_hr',
            viewname: 'hr_departmentMasterGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'f4d97651e0ab8e42ede8190daba9658a',
                'c8e35a748e9dcd200e18227dec30ef05',
            ],
        },
        {
            viewtag: '4dd3028fce390013a76d56212a66d426',
            viewmodule: 'odoo_hr',
            viewname: 'hr_skill_typeBasicQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '50fbd127c0ef3a1fd2284548ec60fdfe',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageMasterTabView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '6655ba8af3994f2fda2a62d6c3360650',
                'e0d74eb1892ab881817fe144ee8400dc',
                'f0960db439f0f13b26e6a43eb5096d9e',
                '0c28c7d1b5467ef547acaa3fd50ad707',
            ],
        },
        {
            viewtag: '552042f767655d7cc70d611fa3b2c16e',
            viewmodule: 'odoo_hr',
            viewname: 'hr_jobMasterInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '48c00dfd1c27d1c90438e9ae1d715847',
            ],
        },
        {
            viewtag: '5fdb423d24542aebde2d32f0104eadf2',
            viewmodule: 'odoo_maintenance',
            viewname: 'maintenance_equipmentGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '45f8206e08dc28bf540d375340cab3af',
            ],
        },
        {
            viewtag: '62db01f049434fe499fc2230f67134ad',
            viewmodule: 'odoo_account',
            viewname: 'account_analytic_lineEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '62f15d7b41688fbe1c3c98652da3c298',
            viewmodule: 'odoo_survey',
            viewname: 'survey_surveyEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '6655ba8af3994f2fda2a62d6c3360650',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageRemarkView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '665c79d3b2552f269e26be742c2ff944',
            viewmodule: 'odoo_hr',
            viewname: 'hr_skill_levelLineEdit',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '0561ac50742161ab8956b4a9f7f1e04d',
            ],
        },
        {
            viewtag: '67225f2bcac7c2fc3770fec346f3e1bf',
            viewmodule: 'odoo_hr',
            viewname: 'hr_jobMasterGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'afa6bbe62a681a3630907d594588770b',
                '44e8aa5916fee576739592ea43587884',
            ],
        },
        {
            viewtag: '69e4c3d30d14aa7afbc44c1f9cc3b887',
            viewmodule: 'odoo_survey',
            viewname: 'survey_surveyPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'e62157d555e4d41070c10edf4b8843d4',
            ],
        },
        {
            viewtag: '6f8e0737003d72cf20baef50ee849c73',
            viewmodule: 'odoo_hr',
            viewname: 'hr_resume_lineLine',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'a09d5ebab0b5cec0ed2117814b6e34dc',
            ],
        },
        {
            viewtag: '765a695b889be2aa0520261276422fb9',
            viewmodule: 'odoo_hr',
            viewname: 'hr_departmentPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '9e9b93bac1ef049bb6e24b2d217aae7a',
            ],
        },
        {
            viewtag: '820c1678c0c8d641e261ae2dca273657',
            viewmodule: 'odoo_base',
            viewname: 'res_usersPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'dab7fa889a32eccea235a6606e44c3c5',
            ],
        },
        {
            viewtag: '86a904137b85ce5f7fadfcc6b711d11f',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employeeTreeExpView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '32b931d5314f23e469988db3b5e71ab5',
            ],
        },
        {
            viewtag: '89b6dbe4fc4224a326f32d0d7adff239',
            viewmodule: 'odoo_hr',
            viewname: 'hr_jobTreeExpView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '67225f2bcac7c2fc3770fec346f3e1bf',
            ],
        },
        {
            viewtag: '89f22e53f7ad7ee53bf0abd030269dfe',
            viewmodule: 'odoo_base',
            viewname: 'res_companyPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'b271ed05e84698d997c001f9e358641a',
            ],
        },
        {
            viewtag: '8cc954bbf41346fe5ddea7ec2f24787a',
            viewmodule: 'odoo_hr',
            viewname: 'hr_contractBasicQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '9634daea422961cc68105c0284fcd66d',
            ],
        },
        {
            viewtag: '8f480c3fa80b2efd38fd263d831a26dd',
            viewmodule: 'odoo_resource',
            viewname: 'resource_resourcePickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '9198dbd0c30e749d1d2e35392f0fcd14',
            ],
        },
        {
            viewtag: '9198dbd0c30e749d1d2e35392f0fcd14',
            viewmodule: 'odoo_resource',
            viewname: 'resource_resourcePickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: '9634daea422961cc68105c0284fcd66d',
            viewmodule: 'odoo_resource',
            viewname: 'resource_calendarPickupView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '4bf6e06e5af9074336f57d7489625777',
            ],
        },
        {
            viewtag: '9e9b93bac1ef049bb6e24b2d217aae7a',
            viewmodule: 'odoo_hr',
            viewname: 'hr_departmentPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'a04463a8a14e612c23a7465c42d3f60e',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employee_categoryLineEdit',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '40d3ef7b5f091b9a38d760e4f7e6fd88',
            ],
        },
        {
            viewtag: 'a09d5ebab0b5cec0ed2117814b6e34dc',
            viewmodule: 'odoo_hr',
            viewname: 'hr_resume_lineEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'a149ddfe684227db8e5a34d8516ac6c2',
            viewmodule: 'odoo_hr',
            viewname: 'hr_leaveEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'a37277c57cf3e92d7e95e3ca7104792c',
            viewmodule: 'odoo_hr',
            viewname: 'HRIndexView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'c8c4a945338c7b14ac11d56b9d322006',
                'df72c1fde592f3fdabb51dc833f325e4',
                '86a904137b85ce5f7fadfcc6b711d11f',
                '4c7c4d76ebd6de40890b94d8874e6a1a',
                'de39fe439bb68c0ee40f50d086e691be',
                '89b6dbe4fc4224a326f32d0d7adff239',
                'a04463a8a14e612c23a7465c42d3f60e',
            ],
        },
        {
            viewtag: 'afa6bbe62a681a3630907d594588770b',
            viewmodule: 'odoo_hr',
            viewname: 'hr_jobMasterTabInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '552042f767655d7cc70d611fa3b2c16e',
            ],
        },
        {
            viewtag: 'b0816566792fd556e775203e4dfa964b',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageMainView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'd031b6ebd0b1b5ea5e7a73729821495d',
                '50fbd127c0ef3a1fd2284548ec60fdfe',
                'bb65aa54bf9c7dcc5093c238adcf6e77',
                'f0960db439f0f13b26e6a43eb5096d9e',
            ],
        },
        {
            viewtag: 'b271ed05e84698d997c001f9e358641a',
            viewmodule: 'odoo_base',
            viewname: 'res_companyPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'bab2a924e4f1f6f783c1c15be5f2b64b',
            viewmodule: 'odoo_survey',
            viewname: 'survey_surveyRedirectView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '62f15d7b41688fbe1c3c98652da3c298',
            ],
        },
        {
            viewtag: 'bb65aa54bf9c7dcc5093c238adcf6e77',
            viewmodule: 'odoo_mail',
            viewname: 'mail_messageByResListView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'c81bf4a79286794aba838b3e058622de',
            viewmodule: 'odoo_hr',
            viewname: 'hr_leaveGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'a149ddfe684227db8e5a34d8516ac6c2',
            ],
        },
        {
            viewtag: 'c8c4a945338c7b14ac11d56b9d322006',
            viewmodule: 'odoo_hr',
            viewname: 'hr_contractGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                'cc0e8ae93915f16260ebb7e16d966699',
                '8cc954bbf41346fe5ddea7ec2f24787a',
            ],
        },
        {
            viewtag: 'c8e35a748e9dcd200e18227dec30ef05',
            viewmodule: 'odoo_hr',
            viewname: 'hr_departmentMasterTabInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'cc0e8ae93915f16260ebb7e16d966699',
            viewmodule: 'odoo_hr',
            viewname: 'hr_contractBasicEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '9634daea422961cc68105c0284fcd66d',
                '820c1678c0c8d641e261ae2dca273657',
            ],
        },
        {
            viewtag: 'd031b6ebd0b1b5ea5e7a73729821495d',
            viewmodule: 'odoo_mail',
            viewname: 'mail_activityByResListView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '33b31adec9be196e954c1b75e4ff6e5c',
            ],
        },
        {
            viewtag: 'dab7fa889a32eccea235a6606e44c3c5',
            viewmodule: 'odoo_base',
            viewname: 'res_usersPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'dd123e093294210ed5319ccd063709c0',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employeeMasterEditView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'de39fe439bb68c0ee40f50d086e691be',
            viewmodule: 'odoo_hr',
            viewname: 'hr_skill_typeBasicListExpView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '107307dcd3d80f3c8ff51c8a8894baba',
                '4dd3028fce390013a76d56212a66d426',
            ],
        },
        {
            viewtag: 'df72c1fde592f3fdabb51dc833f325e4',
            viewmodule: 'odoo_hr',
            viewname: 'hr_resume_line_typeLineEdit',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '371605d75479a65a8a70a01c0264ca9f',
            ],
        },
        {
            viewtag: 'e0d74eb1892ab881817fe144ee8400dc',
            viewmodule: 'odoo_ir',
            viewname: 'ir_attachmentByResDataView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'e62157d555e4d41070c10edf4b8843d4',
            viewmodule: 'odoo_survey',
            viewname: 'survey_surveyPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'ed9e03dd6f3e139a520f139d5bad9496',
            viewmodule: 'odoo_hr',
            viewname: 'hr_jobPickupGridView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'f0960db439f0f13b26e6a43eb5096d9e',
            viewmodule: 'odoo_mail',
            viewname: 'mail_followersByResListView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
            ],
        },
        {
            viewtag: 'f3651c83b6944418c948f91616a23eb9',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employeeMasterInfoView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '6f8e0737003d72cf20baef50ee849c73',
            ],
        },
        {
            viewtag: 'f4d97651e0ab8e42ede8190daba9658a',
            viewmodule: 'odoo_hr',
            viewname: 'hr_departmentMasterQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '765a695b889be2aa0520261276422fb9',
                '89f22e53f7ad7ee53bf0abd030269dfe',
                '48b1a2b2892a307999e9d85527673c1a',
            ],
        },
        {
            viewtag: 'fc5d8102daf9c9af51e2414dab7fdb11',
            viewmodule: 'odoo_hr',
            viewname: 'hr_employeeMasterQuickView',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '765a695b889be2aa0520261276422fb9',
                '89f22e53f7ad7ee53bf0abd030269dfe',
                '485b88fa6da5e97d044e52b8947b7e58',
                '8f480c3fa80b2efd38fd263d831a26dd',
                '48b1a2b2892a307999e9d85527673c1a',
            ],
        },
        {
            viewtag: 'ff6183ee62410e029fffc2d2b36b2a0d',
            viewmodule: 'odoo_hr',
            viewname: 'hr_skillLineEdit',
            viewaction: '',
            viewdatachange: false,
            refviews: [
                '494085a60115c0ef1d0fd25765405dbb',
            ],
        },
    ],
    createdviews: [],
}
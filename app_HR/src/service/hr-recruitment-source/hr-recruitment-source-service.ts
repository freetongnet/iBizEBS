import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_recruitment_sourceServiceBase from './hr-recruitment-source-service-base';


/**
 * 应聘者来源服务对象
 *
 * @export
 * @class Hr_recruitment_sourceService
 * @extends {Hr_recruitment_sourceServiceBase}
 */
export default class Hr_recruitment_sourceService extends Hr_recruitment_sourceServiceBase {

    /**
     * Creates an instance of  Hr_recruitment_sourceService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_recruitment_sourceService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http } from '@/utils';
import { Util } from '@/utils';
import Account_analytic_lineServiceBase from './account-analytic-line-service-base';


/**
 * 分析行服务对象
 *
 * @export
 * @class Account_analytic_lineService
 * @extends {Account_analytic_lineServiceBase}
 */
export default class Account_analytic_lineService extends Account_analytic_lineServiceBase {

    /**
     * Creates an instance of  Account_analytic_lineService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_analytic_lineService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http } from '@/utils';
import { Util } from '@/utils';
import Hr_employee_categoryServiceBase from './hr-employee-category-service-base';


/**
 * 员工类别服务对象
 *
 * @export
 * @class Hr_employee_categoryService
 * @extends {Hr_employee_categoryServiceBase}
 */
export default class Hr_employee_categoryService extends Hr_employee_categoryServiceBase {

    /**
     * Creates an instance of  Hr_employee_categoryService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_employee_categoryService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http } from '@/utils';
import { Util } from '@/utils';
import Account_analytic_accountServiceBase from './account-analytic-account-service-base';


/**
 * 分析账户服务对象
 *
 * @export
 * @class Account_analytic_accountService
 * @extends {Account_analytic_accountServiceBase}
 */
export default class Account_analytic_accountService extends Account_analytic_accountServiceBase {

    /**
     * Creates an instance of  Account_analytic_accountService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_analytic_accountService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
import { Http } from '@/utils';
import { Util } from '@/utils';
import Gamification_badgeServiceBase from './gamification-badge-service-base';


/**
 * 游戏化徽章服务对象
 *
 * @export
 * @class Gamification_badgeService
 * @extends {Gamification_badgeServiceBase}
 */
export default class Gamification_badgeService extends Gamification_badgeServiceBase {

    /**
     * Creates an instance of  Gamification_badgeService.
     * 
     * @param {*} [opts={}]
     * @memberof  Gamification_badgeService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
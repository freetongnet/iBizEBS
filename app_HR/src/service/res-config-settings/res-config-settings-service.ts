import { Http } from '@/utils';
import { Util } from '@/utils';
import Res_config_settingsServiceBase from './res-config-settings-service-base';


/**
 * 配置设定服务对象
 *
 * @export
 * @class Res_config_settingsService
 * @extends {Res_config_settingsServiceBase}
 */
export default class Res_config_settingsService extends Res_config_settingsServiceBase {

    /**
     * Creates an instance of  Res_config_settingsService.
     * 
     * @param {*} [opts={}]
     * @memberof  Res_config_settingsService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
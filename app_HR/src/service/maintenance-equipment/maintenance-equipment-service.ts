import { Http } from '@/utils';
import { Util } from '@/utils';
import Maintenance_equipmentServiceBase from './maintenance-equipment-service-base';


/**
 * 保养设备服务对象
 *
 * @export
 * @class Maintenance_equipmentService
 * @extends {Maintenance_equipmentServiceBase}
 */
export default class Maintenance_equipmentService extends Maintenance_equipmentServiceBase {

    /**
     * Creates an instance of  Maintenance_equipmentService.
     * 
     * @param {*} [opts={}]
     * @memberof  Maintenance_equipmentService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
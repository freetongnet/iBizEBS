import { Http } from '@/utils';
import { Util } from '@/utils';
import Resource_resourceServiceBase from './resource-resource-service-base';


/**
 * 资源服务对象
 *
 * @export
 * @class Resource_resourceService
 * @extends {Resource_resourceServiceBase}
 */
export default class Resource_resourceService extends Resource_resourceServiceBase {

    /**
     * Creates an instance of  Resource_resourceService.
     * 
     * @param {*} [opts={}]
     * @memberof  Resource_resourceService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
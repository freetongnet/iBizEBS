import { Http } from '@/utils';
import { Util } from '@/utils';
import Gamification_goalServiceBase from './gamification-goal-service-base';


/**
 * 游戏化目标服务对象
 *
 * @export
 * @class Gamification_goalService
 * @extends {Gamification_goalServiceBase}
 */
export default class Gamification_goalService extends Gamification_goalServiceBase {

    /**
     * Creates an instance of  Gamification_goalService.
     * 
     * @param {*} [opts={}]
     * @memberof  Gamification_goalService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}
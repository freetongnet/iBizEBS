export default {
    HR_CONTRACT__STATE: {
        "draft": "新建",
        "open": "运行中",
        "close": "过期",
        "cancel": "已取消",
        "empty": ""
    },
    HR_EMPLOYEE__GENDER: {
        "male": "男性",
        "female": "女性",
        "other": "其他",
        "empty": ""
    },
    HR_EMPLOYEE__MARITAL: {
        "single": "单身",
        "married": "已婚",
        "cohabitant": "合法同居者",
        "widower": "丧偶",
        "divorced": "离异",
        "empty": ""
    },
    HR_EMPLOYEE__CERTIFICATE: {
        "bachelor": "学士",
        "master": "主版本",
        "other": "其他",
        "empty": ""
    },
    PRODUCT_CATEGORY__PROPERTY_COST_METHOD: {
        "standard": "标准价格",
        "fifo": "先进先出(FIFO)",
        "average": "平均成本(AVCO)",
        "empty": ""
    },
    HR_RESUME_LINE__DISPLAY_TYPE: {
        "classic": "Classic",
        "course": "课程",
        "certification": "认证",
        "empty": ""
    },
    Odoo_truefalse: {
        "false": "否",
        "true": "是",
        "empty": ""
    },
    HR_JOB__STATE: {
        "recruit": "正在招聘",
        "open": "停止招聘",
        "empty": ""
    },
};
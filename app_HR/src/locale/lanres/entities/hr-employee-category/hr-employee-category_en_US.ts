export default {
  fields: {
    color: "颜色索引",
    employee_ids: "员工",
    id: "ID",
    name: "员工标签",
    display_name: "显示名称",
    __last_update: "最后修改日",
    write_date: "最后更新时间",
    create_date: "创建时间",
    write_uid_text: "最后更新人",
    create_uid_text: "创建人",
    write_uid: "最后更新人",
    create_uid: "创建人",
  },
	views: {
		editview: {
			caption: "员工类别",
      		title: "员工类别编辑视图",
		},
		lineedit: {
			caption: "员工类别",
      		title: "行编辑表格视图",
		},
	},
	main_form: {
		details: {
			group1: "员工类别基本信息", 
			formpage1: "基本信息", 
			group2: "操作信息", 
			formpage2: "其它", 
			srfupdatedate: "最后更新时间", 
			srforikey: "", 
			srfkey: "ID", 
			srfmajortext: "员工标签", 
			srftempmode: "", 
			srfuf: "", 
			srfdeid: "", 
			srfsourcekey: "", 
			name: "员工标签", 
			id: "ID", 
		},
		uiactions: {
		},
	},
	lineedit_grid: {
		nodata: "",
		columns: {
			id: "ID",
			name: "员工标签",
			color: "颜色索引",
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: "Save",
			tip: "Save",
		},
		tbitem4: {
			caption: "Save And New",
			tip: "Save And New",
		},
		tbitem5: {
			caption: "Save And Close",
			tip: "Save And Close Window",
		},
		tbitem6: {
			caption: "-",
			tip: "",
		},
		tbitem7: {
			caption: "Remove And Close",
			tip: "Remove And Close Window",
		},
		tbitem8: {
			caption: "-",
			tip: "",
		},
		tbitem12: {
			caption: "New",
			tip: "New",
		},
		tbitem13: {
			caption: "-",
			tip: "",
		},
		tbitem14: {
			caption: "Copy",
			tip: "Copy {0}",
		},
		tbitem16: {
			caption: "-",
			tip: "",
		},
		tbitem22: {
			caption: "Help",
			tip: "Help",
		},
	},
	lineedittoolbar_toolbar: {
		tbitem24: {
			caption: "行编辑",
			tip: "行编辑",
		},
		tbitem7: {
			caption: "-",
			tip: "",
		},
		tbitem25: {
			caption: "新建行",
			tip: "新建行",
		},
		tbitem26: {
			caption: "-",
			tip: "",
		},
		tbitem8: {
			caption: "保存行",
			tip: "保存行",
		},
		tbitem10: {
			caption: "-",
			tip: "",
		},
		tbitem11: {
			caption: "Remove",
			tip: "Remove {0}",
		},
		tbitem9: {
			caption: "-",
			tip: "",
		},
		tbitem13: {
			caption: "Export",
			tip: "Export {0} Data To Excel",
		},
	},
};
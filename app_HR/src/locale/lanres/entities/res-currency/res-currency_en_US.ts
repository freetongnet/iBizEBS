export default {
  fields: {
    create_date: "创建时间",
    decimal_places: "小数点位置",
    name: "币种",
    currency_unit_label: "货币单位",
    rounding: "舍入系数",
    __last_update: "最后修改日",
    id: "ID",
    write_date: "最后更新时间",
    date: "日期",
    position: "符号位置",
    display_name: "显示名称",
    active: "有效",
    rate: "当前汇率",
    rate_ids: "比率",
    symbol: "货币符号",
    currency_subunit_label: "货币子单位",
    write_uid_text: "最后更新者",
    create_uid_text: "创建人",
    write_uid: "最后更新者",
    create_uid: "创建人",
  },
	views: {
		pickupview: {
			caption: "币种",
      		title: "币种数据选择视图",
		},
		pickupgridview: {
			caption: "币种",
      		title: "币种选择表格视图",
		},
	},
	main_grid: {
		nodata: "",
		columns: {
			name: "币种",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};
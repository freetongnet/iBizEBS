export default {
  fields: {
    id: "ID",
    opening_debit: "期初借方",
    note: "内部备注",
    opening_credit: "期初贷方",
    create_date: "创建时间",
    code: "代码",
    tax_ids: "默认税",
    write_date: "最后更新时间",
    deprecated: "废弃",
    display_name: "显示名称",
    reconcile: "允许核销",
    tag_ids: "标签",
    name: "名称",
    __last_update: "最后修改日",
    internal_type: "内部类型",
    create_uid_text: "创建人",
    currency_id_text: "科目币种",
    internal_group: "内部群组",
    group_id_text: "组",
    write_uid_text: "最后更新人",
    company_id_text: "公司",
    user_type_id_text: "类型",
    company_id: "公司",
    group_id: "组",
    user_type_id: "类型",
    currency_id: "科目币种",
    write_uid: "最后更新人",
    create_uid: "创建人",
  },
	views: {
		pickupgridview: {
			caption: "科目",
      		title: "科目选择表格视图",
		},
		pickupview: {
			caption: "科目",
      		title: "科目数据选择视图",
		},
	},
	main_grid: {
		nodata: "",
		columns: {
			name: "名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};
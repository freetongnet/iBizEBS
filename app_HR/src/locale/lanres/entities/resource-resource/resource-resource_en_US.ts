export default {
  fields: {
    __last_update: "最后修改日",
    id: "ID",
    name: "名称",
    time_efficiency: "效率因子",
    create_date: "创建时间",
    active: "有效",
    resource_type: "资源类型",
    tz: "时区",
    write_date: "最后更新时间",
    display_name: "显示名称",
    calendar_id_text: "工作时间",
    write_uid_text: "最后更新人",
    company_id_text: "公司",
    user_id_text: "用户",
    create_uid_text: "创建人",
    create_uid: "创建人",
    calendar_id: "工作时间",
    company_id: "公司",
    user_id: "用户",
    write_uid: "最后更新人",
  },
	views: {
		pickupview: {
			caption: "资源",
      		title: "资源数据选择视图",
		},
		pickupgridview: {
			caption: "资源",
      		title: "资源选择表格视图",
		},
	},
	main_grid: {
		nodata: "",
		columns: {
			name: "名称",
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};
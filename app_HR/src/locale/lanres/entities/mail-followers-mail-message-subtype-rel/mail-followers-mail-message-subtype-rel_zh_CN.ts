export default {
  fields: {
    id: "ID",
    mail_message_subtype_id: "ID",
    mail_followers_id: "ID",
  },
	views: {
		pickupgridview: {
			caption: "关注消息类型",
      		title: "关注消息类型选择表格视图",
		},
	},
	main_grid: {
		nodata: "",
		columns: {
		},
		uiactions: {
		},
	},
	default_searchform: {
		details: {
			formpage1: "常规条件", 
		},
		uiactions: {
		},
	},
};
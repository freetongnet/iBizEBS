import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Resource_calendarService from '@/service/resource-calendar/resource-calendar-service';
import Resource_calendarAuthService from '@/authservice/resource-calendar/resource-calendar-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Resource_calendarUIService from '@/uiservice/resource-calendar/resource-calendar-ui-service';

/**
 * 资源工作时间选择表格视图视图基类
 *
 * @export
 * @class Resource_calendarPickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Resource_calendarPickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupGridViewBase
     */
    protected appDeName: string = 'resource_calendar';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupGridViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Resource_calendarService}
     * @memberof Resource_calendarPickupGridViewBase
     */
    protected appEntityService: Resource_calendarService = new Resource_calendarService;

    /**
     * 实体权限服务对象
     *
     * @type Resource_calendarUIService
     * @memberof Resource_calendarPickupGridViewBase
     */
    public appUIService: Resource_calendarUIService = new Resource_calendarUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Resource_calendarPickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.resource_calendar.views.pickupgridview.caption',
        srfTitle: 'entities.resource_calendar.views.pickupgridview.title',
        srfSubTitle: 'entities.resource_calendar.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Resource_calendarPickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupGridViewBase
     */
	protected viewtag: string = '4bf6e06e5af9074336f57d7489625777';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_calendarPickupGridViewBase
     */ 
    protected viewName:string = "resource_calendarPickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Resource_calendarPickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Resource_calendarPickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Resource_calendarPickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'resource_calendar',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_calendarPickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Resource_calendarPickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
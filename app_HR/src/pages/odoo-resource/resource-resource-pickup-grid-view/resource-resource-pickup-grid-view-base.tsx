import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupGridViewBase } from '@/studio-core';
import Resource_resourceService from '@/service/resource-resource/resource-resource-service';
import Resource_resourceAuthService from '@/authservice/resource-resource/resource-resource-auth-service';
import PickupGridViewEngine from '@engine/view/pickup-grid-view-engine';
import Resource_resourceUIService from '@/uiservice/resource-resource/resource-resource-ui-service';

/**
 * 资源选择表格视图视图基类
 *
 * @export
 * @class Resource_resourcePickupGridViewBase
 * @extends {PickupGridViewBase}
 */
export class Resource_resourcePickupGridViewBase extends PickupGridViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupGridViewBase
     */
    protected appDeName: string = 'resource_resource';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupGridViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupGridViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupGridViewBase
     */ 
    protected dataControl:string = "grid";

    /**
     * 实体服务对象
     *
     * @type {Resource_resourceService}
     * @memberof Resource_resourcePickupGridViewBase
     */
    protected appEntityService: Resource_resourceService = new Resource_resourceService;

    /**
     * 实体权限服务对象
     *
     * @type Resource_resourceUIService
     * @memberof Resource_resourcePickupGridViewBase
     */
    public appUIService: Resource_resourceUIService = new Resource_resourceUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Resource_resourcePickupGridViewBase
     */
    protected model: any = {
        srfCaption: 'entities.resource_resource.views.pickupgridview.caption',
        srfTitle: 'entities.resource_resource.views.pickupgridview.title',
        srfSubTitle: 'entities.resource_resource.views.pickupgridview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Resource_resourcePickupGridViewBase
     */
    protected containerModel: any = {
        view_grid: { name: 'grid', type: 'GRID' },
        view_searchform: { name: 'searchform', type: 'SEARCHFORM' },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupGridViewBase
     */
	protected viewtag: string = '9198dbd0c30e749d1d2e35392f0fcd14';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Resource_resourcePickupGridViewBase
     */ 
    protected viewName:string = "resource_resourcePickupGridView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Resource_resourcePickupGridViewBase
     */
    public engine: PickupGridViewEngine = new PickupGridViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Resource_resourcePickupGridViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Resource_resourcePickupGridViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            grid: this.$refs.grid,
            searchform: this.$refs.searchform,
            keyPSDEField: 'resource_resource',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * grid 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupGridViewBase
     */
    public grid_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'selectionchange', $event);
    }

    /**
     * grid 部件 beforeload 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupGridViewBase
     */
    public grid_beforeload($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'beforeload', $event);
    }

    /**
     * grid 部件 rowdblclick 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupGridViewBase
     */
    public grid_rowdblclick($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'rowdblclick', $event);
    }

    /**
     * grid 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupGridViewBase
     */
    public grid_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('grid', 'load', $event);
    }

    /**
     * searchform 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupGridViewBase
     */
    public searchform_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'save', $event);
    }

    /**
     * searchform 部件 search 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupGridViewBase
     */
    public searchform_search($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'search', $event);
    }

    /**
     * searchform 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Resource_resourcePickupGridViewBase
     */
    public searchform_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('searchform', 'load', $event);
    }



    /**
     * 是否展开搜索表单
     *
     * @protected
     * @type {boolean}
     * @memberof Resource_resourcePickupGridViewBase
     */
    protected isExpandSearchForm: boolean = true;


}
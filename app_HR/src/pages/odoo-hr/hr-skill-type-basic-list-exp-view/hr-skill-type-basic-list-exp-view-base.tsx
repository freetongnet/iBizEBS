import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { ListExpViewBase } from '@/studio-core';
import Hr_skill_typeService from '@/service/hr-skill-type/hr-skill-type-service';
import Hr_skill_typeAuthService from '@/authservice/hr-skill-type/hr-skill-type-auth-service';
import ListExpViewEngine from '@engine/view/list-exp-view-engine';
import Hr_skill_typeUIService from '@/uiservice/hr-skill-type/hr-skill-type-ui-service';

/**
 * 配置列表导航视图视图基类
 *
 * @export
 * @class Hr_skill_typeBasicListExpViewBase
 * @extends {ListExpViewBase}
 */
export class Hr_skill_typeBasicListExpViewBase extends ListExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    protected appDeName: string = 'hr_skill_type';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Hr_skill_typeService}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    protected appEntityService: Hr_skill_typeService = new Hr_skill_typeService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_skill_typeUIService
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    public appUIService: Hr_skill_typeUIService = new Hr_skill_typeUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_skill_type.views.basiclistexpview.caption',
        srfTitle: 'entities.hr_skill_type.views.basiclistexpview.title',
        srfSubTitle: 'entities.hr_skill_type.views.basiclistexpview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    protected containerModel: any = {
        view_listexpbar: { name: 'listexpbar', type: 'LISTEXPBAR' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Hr_skill_typeBasicListExpView
     */
    public basiclistexpviewlistexpbar_toolbarModels: any = {
        tbitem3: { name: 'tbitem3', caption: '新建', 'isShowCaption': true, 'isShowIcon': true, tooltip: '新建', iconcls: 'fa fa-file-text-o', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'New', target: '', class: '' } },

        tbitem7: {  name: 'tbitem7', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem8: { name: 'tbitem8', caption: '删除', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除', iconcls: 'fa fa-remove', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Remove', target: 'MULTIKEY', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
	protected viewtag: string = 'de39fe439bb68c0ee40f50d086e691be';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */ 
    protected viewName:string = "hr_skill_typeBasicListExpView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    public engine: ListExpViewEngine = new ListExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_skill_typeBasicListExpViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            listexpbar: this.$refs.listexpbar,
            keyPSDEField: 'hr_skill_type',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * listexpbar 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    public listexpbar_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'selectionchange', $event);
    }

    /**
     * listexpbar 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    public listexpbar_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'activated', $event);
    }

    /**
     * listexpbar 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_skill_typeBasicListExpViewBase
     */
    public listexpbar_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('listexpbar', 'load', $event);
    }

    /**
     * 打开新建数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Hr_skill_typeBasicListExpView
     */
    public newdata(args: any[],fullargs?:any[], params?: any, $event?: any, xData?: any) {
        let localContext:any = null;
        let localViewParam:any =null;
        const data: any = {};
        if(args[0].srfsourcekey){
            data.srfsourcekey = args[0].srfsourcekey;
        }
        if(fullargs && (fullargs as any).copymode) {
            Object.assign(data, { copymode: (fullargs as any).copymode });
        }
        let tempContext = JSON.parse(JSON.stringify(this.context));
        delete tempContext.hr_skill_type;
        if(args.length >0){
            Object.assign(tempContext,args[0]);
        }
        const deResParameters: any[] = [];
        const parameters: any[] = [
            { pathName: 'hr_skill_types', parameterName: 'hr_skill_type' },
        ];
        const _this: any = this;
        const openDrawer = (view: any, data: any) => {
            let container: Subject<any> = this.$appdrawer.openDrawer(view, tempContext, data);
            container.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                    return;
                }
                if (!xData || !(xData.refresh instanceof Function)) {
                    return;
                }
                xData.refresh(result.datas);
            });
        }
        const view: any = {
            viewname: 'hr-skill-type-basic-quick-view', 
            height: 0, 
            width: 0,  
            title: this.$t('entities.hr_skill_type.views.basicquickview.title'),
            placement: 'DRAWER_RIGHT',
        };
        openDrawer(view, data);
    }


    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args
     * @param {*} [params]
     * @param {*} [fullargs]
     * @param {*} [$event]
     * @param {*} [xData]
     * @memberof Hr_skill_typeBasicListExpView
     */
    public opendata(args: any[],fullargs?:any[],params?: any, $event?: any, xData?: any) {
    this.$Notice.warning({ title: '错误', desc: '未指定关系视图' });
    }




    /**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_skill_typeBasicListExpView
     */
    protected viewUID: string = 'odoo-hr-hr-skill-type-basic-list-exp-view';


}
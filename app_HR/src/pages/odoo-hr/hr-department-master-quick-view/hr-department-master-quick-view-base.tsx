import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Hr_departmentService from '@/service/hr-department/hr-department-service';
import Hr_departmentAuthService from '@/authservice/hr-department/hr-department-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Hr_departmentUIService from '@/uiservice/hr-department/hr-department-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class Hr_departmentMasterQuickViewBase
 * @extends {OptionViewBase}
 */
export class Hr_departmentMasterQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentMasterQuickViewBase
     */
    protected appDeName: string = 'hr_department';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentMasterQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentMasterQuickViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentMasterQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Hr_departmentService}
     * @memberof Hr_departmentMasterQuickViewBase
     */
    protected appEntityService: Hr_departmentService = new Hr_departmentService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_departmentUIService
     * @memberof Hr_departmentMasterQuickViewBase
     */
    public appUIService: Hr_departmentUIService = new Hr_departmentUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_departmentMasterQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_departmentMasterQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_department.views.masterquickview.caption',
        srfTitle: 'entities.hr_department.views.masterquickview.title',
        srfSubTitle: 'entities.hr_department.views.masterquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_departmentMasterQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentMasterQuickViewBase
     */
	protected viewtag: string = 'f4d97651e0ab8e42ede8190daba9658a';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_departmentMasterQuickViewBase
     */ 
    protected viewName:string = "hr_departmentMasterQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_departmentMasterQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_departmentMasterQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_departmentMasterQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'hr_department',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentMasterQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentMasterQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_departmentMasterQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { TabExpViewBase } from '@/studio-core';
import Hr_employeeService from '@/service/hr-employee/hr-employee-service';
import Hr_employeeAuthService from '@/authservice/hr-employee/hr-employee-auth-service';
import TabExpViewEngine from '@engine/view/tab-exp-view-engine';
import Hr_employeeUIService from '@/uiservice/hr-employee/hr-employee-ui-service';

/**
 * 主信息总览视图视图基类
 *
 * @export
 * @class Hr_employeeMasterTabInfoViewBase
 * @extends {TabExpViewBase}
 */
export class Hr_employeeMasterTabInfoViewBase extends TabExpViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    protected appDeName: string = 'hr_employee';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 实体服务对象
     *
     * @type {Hr_employeeService}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    protected appEntityService: Hr_employeeService = new Hr_employeeService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_employeeUIService
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    public appUIService: Hr_employeeUIService = new Hr_employeeUIService(this.$store);

	/**
	 * 自定义视图导航上下文集合
	 *
     * @protected
	 * @type {*}
	 * @memberof Hr_employeeMasterTabInfoViewBase
	 */
    protected customViewNavContexts: any = {
        'N_RES_MODEL_EQ': { isRawValue: true, value: 'hr.employee' },
        'N_RES_ID_EQ': { isRawValue: false, value: 'hr_employee' }
    };

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_employee.views.mastertabinfoview.caption',
        srfTitle: 'entities.hr_employee.views.mastertabinfoview.title',
        srfSubTitle: 'entities.hr_employee.views.mastertabinfoview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_tabexppanel: { name: 'tabexppanel', type: 'TABEXPPANEL' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Hr_employeeMasterTabInfoView
     */
    public toolBarModels: any = {
        tbitem1_masteredit: { name: 'tbitem1_masteredit', caption: '编辑', 'isShowCaption': true, 'isShowIcon': true, tooltip: '编辑', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'MasterEdit', target: 'SINGLEKEY', class: '' } },

        tbitem1_masterremove_sep: {  name: 'tbitem1_masterremove_sep', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem1_masterremove: { name: 'tbitem1_masterremove', caption: '删除并关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '删除并关闭', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'MasterRemove', target: 'SINGLEKEY', class: '' } },

        tbitem2: {  name: 'tbitem2', type: 'SEPERATOR', visabled: true, dataaccaction: '', uiaction: { } },
        tbitem12: { name: 'tbitem12', caption: '关闭', 'isShowCaption': true, 'isShowIcon': true, tooltip: '关闭', iconcls: 'fa fa-sign-out', icon: '', disabled: false, type: 'DEUIACTION', visabled: true,noprivdisplaymode:2,dataaccaction: '', uiaction: { tag: 'Exit', target: '', class: '' } },

    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
	protected viewtag: string = '207f2394ba3237b0cd0125199df45312';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */ 
    protected viewName:string = "hr_employeeMasterTabInfoView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    public engine: TabExpViewEngine = new TabExpViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_employeeMasterTabInfoViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            keyPSDEField: 'hr_employee',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * toolbar 部件 click 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    public toolbar_click($event: any, $event2?: any): void {
        if (Object.is($event.tag, 'tbitem1_masteredit')) {
            this.toolbar_tbitem1_masteredit_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem1_masterremove')) {
            this.toolbar_tbitem1_masterremove_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem9')) {
            this.toolbar_tbitem9_click(null, '', $event2);
        }
        if (Object.is($event.tag, 'tbitem12')) {
            this.toolbar_tbitem12_click(null, '', $event2);
        }
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_masteredit_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Hr_employeeUIService  = new Hr_employeeUIService();
        curUIService.Hr_employee_MasterEdit(datas,contextJO, paramJO,  $event, xData,this,"Hr_employee");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem1_masterremove_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        const curUIService:Hr_employeeUIService  = new Hr_employeeUIService();
        curUIService.Hr_employee_MasterRemove(datas,contextJO, paramJO,  $event, xData,this,"Hr_employee");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem9_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.SaveAndStart(datas, contextJO,paramJO,  $event, xData,this,"Hr_employee");
    }

    /**
     * 逻辑事件
     *
     * @param {*} [params={}]
     * @param {*} [tag]
     * @param {*} [$event]
     * @memberof 
     */
    public toolbar_tbitem12_click(params: any = {}, tag?: any, $event?: any) {
        // 参数
        // 取数
        let datas: any[] = [];
        let xData: any = null;
        // _this 指向容器对象
        const _this: any = this;
        let paramJO:any = {};
        let contextJO:any = {};
        if (_this.getDatas && _this.getDatas instanceof Function) {
            datas = [..._this.getDatas()];
        }
        if(params){
          datas = [params];
        }
        // 界面行为
        this.Exit(datas, contextJO,paramJO,  $event, xData,this,"Hr_employee");
    }

    /**
     * 开始流程
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    public SaveAndStart(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        const _this: any = this;
        if (!xData || !(xData.wfstart instanceof Function)) {
            return;
        }
        xData.wfstart(args).then((response: any) => {
            if (!response || response.status !== 200) {
                return;
            }
            const { data: _data } = response;
            if(window.parent){
                window.parent.postMessage({ ..._data },'*');
            }
            if (_this.viewdata) {
                _this.$emit('viewdataschange', [{ ..._data }]);
                _this.$emit('close');
            }else if (this.$tabPageExp) {
                this.$tabPageExp.onClose(this.$route.fullPath);
            }
        });
    }
    /**
     * 关闭
     *
     * @param {any[]} args 当前数据
     * @param {any} contextJO 行为附加上下文
     * @param {*} [params] 附加参数
     * @param {*} [$event] 事件源
     * @param {*} [xData]  执行行为所需当前部件
     * @param {*} [actionContext]  执行行为上下文
     * @memberof Hr_employeeMasterTabInfoViewBase
     */
    public Exit(args: any[],contextJO?:any, params?: any, $event?: any, xData?: any,actionContext?:any,srfParentDeName?:string) {
        this.closeView(args);
        if(window.parent){
            window.parent.postMessage([{ ...args }],'*');
        }
    }


}
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { EditViewBase } from '@/studio-core';
import Hr_jobService from '@/service/hr-job/hr-job-service';
import Hr_jobAuthService from '@/authservice/hr-job/hr-job-auth-service';
import EditViewEngine from '@engine/view/edit-view-engine';
import Hr_jobUIService from '@/uiservice/hr-job/hr-job-ui-service';

/**
 * 主编辑视图视图基类
 *
 * @export
 * @class Hr_jobMasterEditViewBase
 * @extends {EditViewBase}
 */
export class Hr_jobMasterEditViewBase extends EditViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterEditViewBase
     */
    protected appDeName: string = 'hr_job';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterEditViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterEditViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterEditViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Hr_jobService}
     * @memberof Hr_jobMasterEditViewBase
     */
    protected appEntityService: Hr_jobService = new Hr_jobService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_jobUIService
     * @memberof Hr_jobMasterEditViewBase
     */
    public appUIService: Hr_jobUIService = new Hr_jobUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_jobMasterEditViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_jobMasterEditViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_job.views.mastereditview.caption',
        srfTitle: 'entities.hr_job.views.mastereditview.title',
        srfSubTitle: 'entities.hr_job.views.mastereditview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_jobMasterEditViewBase
     */
    protected containerModel: any = {
        view_toolbar: { name: 'toolbar', type: 'TOOLBAR' },
        view_form: { name: 'form', type: 'FORM' },
    };

    /**
     * 工具栏模型
     *
     * @type {*}
     * @memberof Hr_jobMasterEditView
     */
    public toolBarModels: any = {
    };



	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterEditViewBase
     */
	protected viewtag: string = '48c00dfd1c27d1c90438e9ae1d715847';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterEditViewBase
     */ 
    protected viewName:string = "hr_jobMasterEditView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_jobMasterEditViewBase
     */
    public engine: EditViewEngine = new EditViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_jobMasterEditViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_jobMasterEditViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'hr_job',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_jobMasterEditViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_jobMasterEditViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_jobMasterEditViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { OptionViewBase } from '@/studio-core';
import Hr_jobService from '@/service/hr-job/hr-job-service';
import Hr_jobAuthService from '@/authservice/hr-job/hr-job-auth-service';
import OptionViewEngine from '@engine/view/option-view-engine';
import Hr_jobUIService from '@/uiservice/hr-job/hr-job-ui-service';

/**
 * 快速新建视图基类
 *
 * @export
 * @class Hr_jobMasterQuickViewBase
 * @extends {OptionViewBase}
 */
export class Hr_jobMasterQuickViewBase extends OptionViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterQuickViewBase
     */
    protected appDeName: string = 'hr_job';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterQuickViewBase
     */
    protected appDeKey: string = 'id';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterQuickViewBase
     */
    protected appDeMajor: string = 'name';

    /**
     * 数据部件名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterQuickViewBase
     */ 
    protected dataControl:string = "form";

    /**
     * 实体服务对象
     *
     * @type {Hr_jobService}
     * @memberof Hr_jobMasterQuickViewBase
     */
    protected appEntityService: Hr_jobService = new Hr_jobService;

    /**
     * 实体权限服务对象
     *
     * @type Hr_jobUIService
     * @memberof Hr_jobMasterQuickViewBase
     */
    public appUIService: Hr_jobUIService = new Hr_jobUIService(this.$store);

    /**
     * 是否显示信息栏
     *
     * @memberof Hr_jobMasterQuickViewBase
     */
    isShowDataInfoBar = true;

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof Hr_jobMasterQuickViewBase
     */
    protected model: any = {
        srfCaption: 'entities.hr_job.views.masterquickview.caption',
        srfTitle: 'entities.hr_job.views.masterquickview.title',
        srfSubTitle: 'entities.hr_job.views.masterquickview.subtitle',
        dataInfo: ''
    }

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof Hr_jobMasterQuickViewBase
     */
    protected containerModel: any = {
        view_form: { name: 'form', type: 'FORM' },
        view_okbtn: { name: 'okbtn', type: 'button', text: '确定', disabled: true },
        view_cancelbtn: { name: 'cancelbtn', type: 'button', text: '取消', disabled: false },
        view_leftbtn: { name: 'leftbtn', type: 'button', text: '左移', disabled: true },
        view_rightbtn: { name: 'rightbtn', type: 'button', text: '右移', disabled: true },
        view_allleftbtn: { name: 'allleftbtn', type: 'button', text: '全部左移', disabled: true },
        view_allrightbtn: { name: 'allrightbtn', type: 'button', text: '全部右移', disabled: true },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterQuickViewBase
     */
	protected viewtag: string = '44e8aa5916fee576739592ea43587884';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof Hr_jobMasterQuickViewBase
     */ 
    protected viewName:string = "hr_jobMasterQuickView";


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof Hr_jobMasterQuickViewBase
     */
    public engine: OptionViewEngine = new OptionViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof Hr_jobMasterQuickViewBase
     */    
    public counterServiceArray:Array<any> = [];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof Hr_jobMasterQuickViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            form: this.$refs.form,
            p2k: '0',
            keyPSDEField: 'hr_job',
            majorPSDEField: 'name',
            isLoadDefault: true,
        });
    }

    /**
     * form 部件 save 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_jobMasterQuickViewBase
     */
    public form_save($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'save', $event);
    }

    /**
     * form 部件 remove 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_jobMasterQuickViewBase
     */
    public form_remove($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'remove', $event);
    }

    /**
     * form 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof Hr_jobMasterQuickViewBase
     */
    public form_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('form', 'load', $event);
    }


}
/**
 * UI服务注册中心
 *
 * @export
 * @class UIServiceRegister
 */
export class UIServiceRegister {

    /**
     * 所有UI实体服务Map
     *
     * @protected
     * @type {*}
     * @memberof UIServiceRegister
     */
    protected allUIService: Map<string, () => Promise<any>> = new Map();

    /**
     * 已加载UI实体服务Map缓存
     *
     * @protected
     * @type {Map<string, any>}
     * @memberof UIServiceRegister
     */
    protected serviceCache: Map<string, any> = new Map();

    /**
     * Creates an instance of UIServiceRegister.
     * @memberof UIServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * 初始化
     *
     * @protected
     * @memberof UIServiceRegister
     */
    protected init(): void {
                this.allUIService.set('account_analytic_line', () => import('@/uiservice/account-analytic-line/account-analytic-line-ui-service'));
        this.allUIService.set('hr_employee', () => import('@/uiservice/hr-employee/hr-employee-ui-service'));
        this.allUIService.set('res_currency', () => import('@/uiservice/res-currency/res-currency-ui-service'));
        this.allUIService.set('hr_job', () => import('@/uiservice/hr-job/hr-job-ui-service'));
        this.allUIService.set('mail_followers', () => import('@/uiservice/mail-followers/mail-followers-ui-service'));
        this.allUIService.set('account_move_line', () => import('@/uiservice/account-move-line/account-move-line-ui-service'));
        this.allUIService.set('mail_activity', () => import('@/uiservice/mail-activity/mail-activity-ui-service'));
        this.allUIService.set('hr_skill_type', () => import('@/uiservice/hr-skill-type/hr-skill-type-ui-service'));
        this.allUIService.set('account_analytic_group', () => import('@/uiservice/account-analytic-group/account-analytic-group-ui-service'));
        this.allUIService.set('hr_resume_line', () => import('@/uiservice/hr-resume-line/hr-resume-line-ui-service'));
        this.allUIService.set('hr_skill', () => import('@/uiservice/hr-skill/hr-skill-ui-service'));
        this.allUIService.set('res_company', () => import('@/uiservice/res-company/res-company-ui-service'));
        this.allUIService.set('hr_recruitment_source', () => import('@/uiservice/hr-recruitment-source/hr-recruitment-source-ui-service'));
        this.allUIService.set('hr_leave', () => import('@/uiservice/hr-leave/hr-leave-ui-service'));
        this.allUIService.set('res_users', () => import('@/uiservice/res-users/res-users-ui-service'));
        this.allUIService.set('survey_survey', () => import('@/uiservice/survey-survey/survey-survey-ui-service'));
        this.allUIService.set('account_analytic_account', () => import('@/uiservice/account-analytic-account/account-analytic-account-ui-service'));
        this.allUIService.set('ir_attachment', () => import('@/uiservice/ir-attachment/ir-attachment-ui-service'));
        this.allUIService.set('hr_skill_level', () => import('@/uiservice/hr-skill-level/hr-skill-level-ui-service'));
        this.allUIService.set('hr_department', () => import('@/uiservice/hr-department/hr-department-ui-service'));
        this.allUIService.set('gamification_goal', () => import('@/uiservice/gamification-goal/gamification-goal-ui-service'));
        this.allUIService.set('hr_contract', () => import('@/uiservice/hr-contract/hr-contract-ui-service'));
        this.allUIService.set('hr_employee_category', () => import('@/uiservice/hr-employee-category/hr-employee-category-ui-service'));
        this.allUIService.set('resource_calendar', () => import('@/uiservice/resource-calendar/resource-calendar-ui-service'));
        this.allUIService.set('mail_message', () => import('@/uiservice/mail-message/mail-message-ui-service'));
        this.allUIService.set('hr_contract_type', () => import('@/uiservice/hr-contract-type/hr-contract-type-ui-service'));
        this.allUIService.set('res_config_settings', () => import('@/uiservice/res-config-settings/res-config-settings-ui-service'));
        this.allUIService.set('product_product', () => import('@/uiservice/product-product/product-product-ui-service'));
        this.allUIService.set('hr_resume_line_type', () => import('@/uiservice/hr-resume-line-type/hr-resume-line-type-ui-service'));
        this.allUIService.set('gamification_challenge', () => import('@/uiservice/gamification-challenge/gamification-challenge-ui-service'));
        this.allUIService.set('mail_tracking_value', () => import('@/uiservice/mail-tracking-value/mail-tracking-value-ui-service'));
        this.allUIService.set('product_category', () => import('@/uiservice/product-category/product-category-ui-service'));
        this.allUIService.set('res_partner', () => import('@/uiservice/res-partner/res-partner-ui-service'));
        this.allUIService.set('sale_order_line', () => import('@/uiservice/sale-order-line/sale-order-line-ui-service'));
        this.allUIService.set('resource_resource', () => import('@/uiservice/resource-resource/resource-resource-ui-service'));
        this.allUIService.set('gamification_badge', () => import('@/uiservice/gamification-badge/gamification-badge-ui-service'));
        this.allUIService.set('hr_leave_type', () => import('@/uiservice/hr-leave-type/hr-leave-type-ui-service'));
        this.allUIService.set('maintenance_equipment', () => import('@/uiservice/maintenance-equipment/maintenance-equipment-ui-service'));
        this.allUIService.set('account_account', () => import('@/uiservice/account-account/account-account-ui-service'));
    }

    /**
     * 加载服务实体
     *
     * @protected
     * @param {string} serviceName
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    protected async loadService(serviceName: string): Promise<any> {
        const service = this.allUIService.get(serviceName);
        if (service) {
            return service();
        }
    }

    /**
     * 获取应用实体服务
     *
     * @param {string} name
     * @returns {Promise<any>}
     * @memberof UIServiceRegister
     */
    public async getService(name: string): Promise<any> {
        if (this.serviceCache.has(name)) {
            return this.serviceCache.get(name);
        }
        const entityService: any = await this.loadService(name);
        if (entityService && entityService.default) {
            const instance: any = new entityService.default();
            this.serviceCache.set(name, instance);
            return instance;
        }
    }

}
export const uiServiceRegister: UIServiceRegister = new UIServiceRegister();
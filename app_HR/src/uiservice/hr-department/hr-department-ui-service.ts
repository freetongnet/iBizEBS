import Hr_departmentUIServiceBase from './hr-department-ui-service-base';

/**
 * HR 部门UI服务对象
 *
 * @export
 * @class Hr_departmentUIService
 */
export default class Hr_departmentUIService extends Hr_departmentUIServiceBase {

    /**
     * Creates an instance of  Hr_departmentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_departmentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
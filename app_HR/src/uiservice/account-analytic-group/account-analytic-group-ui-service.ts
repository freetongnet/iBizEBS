import Account_analytic_groupUIServiceBase from './account-analytic-group-ui-service-base';

/**
 * 分析类别UI服务对象
 *
 * @export
 * @class Account_analytic_groupUIService
 */
export default class Account_analytic_groupUIService extends Account_analytic_groupUIServiceBase {

    /**
     * Creates an instance of  Account_analytic_groupUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Account_analytic_groupUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import Hr_employee_categoryUIServiceBase from './hr-employee-category-ui-service-base';

/**
 * 员工类别UI服务对象
 *
 * @export
 * @class Hr_employee_categoryUIService
 */
export default class Hr_employee_categoryUIService extends Hr_employee_categoryUIServiceBase {

    /**
     * Creates an instance of  Hr_employee_categoryUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_employee_categoryUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
import Maintenance_equipmentUIServiceBase from './maintenance-equipment-ui-service-base';

/**
 * 保养设备UI服务对象
 *
 * @export
 * @class Maintenance_equipmentUIService
 */
export default class Maintenance_equipmentUIService extends Maintenance_equipmentUIServiceBase {

    /**
     * Creates an instance of  Maintenance_equipmentUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Maintenance_equipmentUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
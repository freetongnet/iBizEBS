import Survey_surveyUIServiceBase from './survey-survey-ui-service-base';

/**
 * 问卷UI服务对象
 *
 * @export
 * @class Survey_surveyUIService
 */
export default class Survey_surveyUIService extends Survey_surveyUIServiceBase {

    /**
     * Creates an instance of  Survey_surveyUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Survey_surveyUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
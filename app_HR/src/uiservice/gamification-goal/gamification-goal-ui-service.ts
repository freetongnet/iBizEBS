import Gamification_goalUIServiceBase from './gamification-goal-ui-service-base';

/**
 * 游戏化目标UI服务对象
 *
 * @export
 * @class Gamification_goalUIService
 */
export default class Gamification_goalUIService extends Gamification_goalUIServiceBase {

    /**
     * Creates an instance of  Gamification_goalUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Gamification_goalUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
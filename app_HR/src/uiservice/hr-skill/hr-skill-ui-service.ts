import Hr_skillUIServiceBase from './hr-skill-ui-service-base';

/**
 * 技能UI服务对象
 *
 * @export
 * @class Hr_skillUIService
 */
export default class Hr_skillUIService extends Hr_skillUIServiceBase {

    /**
     * Creates an instance of  Hr_skillUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  Hr_skillUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}
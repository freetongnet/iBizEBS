import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取studio链接数据
mock.onGet('./assets/json/view-config.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
                "hr_skill_leveleditview": {
            "title": "技能等级编辑视图",
            "caption": "技能等级",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_skill_levelEditView",
            "viewtag": "0561ac50742161ab8956b4a9f7f1e04d"
        },
        "hr_employeepickupgridview": {
            "title": "员工选择表格视图",
            "caption": "员工",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeePickupGridView",
            "viewtag": "0626393582cbb7fd983d27f9b1341670"
        },
        "hr_contracteditview": {
            "title": "Contract编辑视图",
            "caption": "合同",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_contractEditView",
            "viewtag": "0a49ba3a5fef26b3cab040ecdc538132"
        },
        "mail_messagesendview": {
            "title": "消息编辑视图",
            "caption": "消息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageSendView",
            "viewtag": "0c28c7d1b5467ef547acaa3fd50ad707"
        },
        "hr_skill_typebasiceditview": {
            "title": "配置信息编辑视图",
            "caption": "供应商价格表",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_skill_typeBasicEditView",
            "viewtag": "107307dcd3d80f3c8ff51c8a8894baba"
        },
        "account_analytic_lineline": {
            "title": "行编辑表格视图",
            "caption": "分析行",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "account_analytic_lineLine",
            "viewtag": "1bf7794526c1642df10a2b9788dfd850"
        },
        "hr_employeemastertabinfoview": {
            "title": "主信息总览视图",
            "caption": "员工",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeeMasterTabInfoView",
            "viewtag": "207f2394ba3237b0cd0125199df45312"
        },
        "account_analytic_grouppickupgridview": {
            "title": "分析类别选择表格视图",
            "caption": "分析类别",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_analytic_groupPickupGridView",
            "viewtag": "216cb5573cfaef8089a872a62593d0f3"
        },
        "hr_resume_line_typepickupgridview": {
            "title": "简历类型选择表格视图",
            "caption": "简历类型",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_resume_line_typePickupGridView",
            "viewtag": "2c6e47895e3038cdfa67c9b595523b02"
        },
        "res_partnerpickupgridview": {
            "title": "联系人选择表格视图",
            "caption": "联系人",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partnerPickupGridView",
            "viewtag": "2d28144e9daaba4cd6615cbdb6ae960e"
        },
        "hr_employeemastergridview": {
            "title": "首选表格视图",
            "caption": "员工",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeeMasterGridView",
            "viewtag": "32b931d5314f23e469988db3b5e71ab5"
        },
        "mail_activityeditview": {
            "title": "活动编辑视图",
            "caption": "活动",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_activityEditView",
            "viewtag": "33b31adec9be196e954c1b75e4ff6e5c"
        },
        "hr_resume_line_typeeditview": {
            "title": "简历类型编辑视图",
            "caption": "简历类型",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_resume_line_typeEditView",
            "viewtag": "371605d75479a65a8a70a01c0264ca9f"
        },
        "res_partnerpickupview": {
            "title": "联系人数据选择视图",
            "caption": "联系人",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_partnerPickupView",
            "viewtag": "3d90688394c225f742436305b4f54a92"
        },
        "hr_employee_categoryeditview": {
            "title": "员工类别编辑视图",
            "caption": "员工类别",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employee_categoryEditView",
            "viewtag": "40d3ef7b5f091b9a38d760e4f7e6fd88"
        },
        "hr_jobmasterquickview": {
            "title": "快速新建",
            "caption": "工作岗位",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_jobMasterQuickView",
            "viewtag": "44e8aa5916fee576739592ea43587884"
        },
        "maintenance_equipmenteditview": {
            "title": "保养设备编辑视图",
            "caption": "保养设备",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_maintenance",
            "viewname": "maintenance_equipmentEditView",
            "viewtag": "45f8206e08dc28bf540d375340cab3af"
        },
        "hr_jobpickupview": {
            "title": "工作岗位数据选择视图",
            "caption": "工作岗位",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_jobPickupView",
            "viewtag": "485b88fa6da5e97d044e52b8947b7e58"
        },
        "hr_employeepickupview": {
            "title": "员工数据选择视图",
            "caption": "员工",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeePickupView",
            "viewtag": "48b1a2b2892a307999e9d85527673c1a"
        },
        "hr_jobmastereditview": {
            "title": "主编辑视图",
            "caption": "工作岗位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_jobMasterEditView",
            "viewtag": "48c00dfd1c27d1c90438e9ae1d715847"
        },
        "hr_employeemastersummaryview": {
            "title": "概览",
            "caption": "员工",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeeMasterSummaryView",
            "viewtag": "490a981f9c39f68d146cbde197dab3dc"
        },
        "hr_skilleditview": {
            "title": "技能编辑视图",
            "caption": "技能",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_skillEditView",
            "viewtag": "494085a60115c0ef1d0fd25765405dbb"
        },
        "resource_calendarpickupgridview": {
            "title": "资源工作时间选择表格视图",
            "caption": "资源工作时间",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_resource",
            "viewname": "resource_calendarPickupGridView",
            "viewtag": "4bf6e06e5af9074336f57d7489625777"
        },
        "hr_departmentmastergridview": {
            "title": "首选表格视图",
            "caption": "HR 部门",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_departmentMasterGridView",
            "viewtag": "4c7c4d76ebd6de40890b94d8874e6a1a"
        },
        "sale_order_linepickupview": {
            "title": "销售订单行数据选择视图",
            "caption": "销售订单行",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_sale",
            "viewname": "sale_order_linePickupView",
            "viewtag": "4cd84e26dfe3d6b4b0315bb4218e8acb"
        },
        "hr_skill_typebasicquickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_skill_typeBasicQuickView",
            "viewtag": "4dd3028fce390013a76d56212a66d426"
        },
        "mail_messagemastertabview": {
            "title": "消息分页导航视图",
            "caption": "消息",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageMasterTabView",
            "viewtag": "50fbd127c0ef3a1fd2284548ec60fdfe"
        },
        "hr_jobmasterinfoview": {
            "title": "主信息概览",
            "caption": "工作岗位",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_jobMasterInfoView",
            "viewtag": "552042f767655d7cc70d611fa3b2c16e"
        },
        "product_productpickupgridview": {
            "title": "产品选择表格视图",
            "caption": "产品",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productPickupGridView",
            "viewtag": "59f7fba6c81ebdfae1f47324eac986b2"
        },
        "account_accountpickupgridview": {
            "title": "科目选择表格视图",
            "caption": "科目",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_accountPickupGridView",
            "viewtag": "5f1719659331cca5b391cdbe06819a1b"
        },
        "account_analytic_linelineedit": {
            "title": "行编辑表格视图",
            "caption": "分析行",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "account_analytic_lineLineEdit",
            "viewtag": "5fc2559c4f58486d0045c9bc41bdb985"
        },
        "maintenance_equipmentgridview": {
            "title": "保养设备表格视图",
            "caption": "保养设备",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_maintenance",
            "viewname": "maintenance_equipmentGridView",
            "viewtag": "5fdb423d24542aebde2d32f0104eadf2"
        },
        "account_analytic_lineeditview": {
            "title": "分析行编辑视图",
            "caption": "分析行",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_analytic_lineEditView",
            "viewtag": "62db01f049434fe499fc2230f67134ad"
        },
        "survey_surveyeditview": {
            "title": "问卷编辑视图",
            "caption": "问卷",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_survey",
            "viewname": "survey_surveyEditView",
            "viewtag": "62f15d7b41688fbe1c3c98652da3c298"
        },
        "mail_messageremarkview": {
            "title": "消息编辑视图",
            "caption": "消息",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageRemarkView",
            "viewtag": "6655ba8af3994f2fda2a62d6c3360650"
        },
        "hr_skill_levellineedit": {
            "title": "行编辑表格视图",
            "caption": "技能等级",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_skill_levelLineEdit",
            "viewtag": "665c79d3b2552f269e26be742c2ff944"
        },
        "hr_jobmastergridview": {
            "title": "首选表格视图",
            "caption": "工作岗位",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_jobMasterGridView",
            "viewtag": "67225f2bcac7c2fc3770fec346f3e1bf"
        },
        "survey_surveypickupview": {
            "title": "问卷数据选择视图",
            "caption": "问卷",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_survey",
            "viewname": "survey_surveyPickupView",
            "viewtag": "69e4c3d30d14aa7afbc44c1f9cc3b887"
        },
        "account_analytic_grouppickupview": {
            "title": "分析类别数据选择视图",
            "caption": "分析类别",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_analytic_groupPickupView",
            "viewtag": "6bfdb56b768bfe7c49e2634d17f603b8"
        },
        "hr_resume_lineline": {
            "title": "行编辑表格视图",
            "caption": "员工简历行",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_resume_lineLine",
            "viewtag": "6f8e0737003d72cf20baef50ee849c73"
        },
        "res_currencypickupview": {
            "title": "币种数据选择视图",
            "caption": "币种",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_currencyPickupView",
            "viewtag": "73efabdda1f1c176b16065c256fc6891"
        },
        "hr_departmentpickupview": {
            "title": "HR 部门数据选择视图",
            "caption": "HR 部门",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_departmentPickupView",
            "viewtag": "765a695b889be2aa0520261276422fb9"
        },
        "product_productpickupview": {
            "title": "产品数据选择视图",
            "caption": "产品",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_productPickupView",
            "viewtag": "77fff7ed484c53f4b0261ea30086dff3"
        },
        "resource_calendareditview": {
            "title": "资源工作时间编辑视图",
            "caption": "资源工作时间",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_resource",
            "viewname": "resource_calendarEditView",
            "viewtag": "7e73a1c4ea1986aac6a13bed7c77ce85"
        },
        "res_userspickupview": {
            "title": "用户数据选择视图",
            "caption": "用户",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_usersPickupView",
            "viewtag": "820c1678c0c8d641e261ae2dca273657"
        },
        "account_move_linepickupgridview": {
            "title": "日记账项目选择表格视图",
            "caption": "日记账项目",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_move_linePickupGridView",
            "viewtag": "82514970faee8746de0ed26f67c274d8"
        },
        "res_currencypickupgridview": {
            "title": "币种选择表格视图",
            "caption": "币种",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_currencyPickupGridView",
            "viewtag": "83d72d536bd558920673d6cbf005ef35"
        },
        "hr_employeetreeexpview": {
            "title": "员工树导航视图",
            "caption": "员工",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeeTreeExpView",
            "viewtag": "86a904137b85ce5f7fadfcc6b711d11f"
        },
        "hr_jobtreeexpview": {
            "title": "工作岗位树导航视图",
            "caption": "工作岗位",
            "viewtype": "DETREEEXPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_jobTreeExpView",
            "viewtag": "89b6dbe4fc4224a326f32d0d7adff239"
        },
        "res_companypickupview": {
            "title": "公司数据选择视图",
            "caption": "公司",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_companyPickupView",
            "viewtag": "89f22e53f7ad7ee53bf0abd030269dfe"
        },
        "sale_order_linepickupgridview": {
            "title": "销售订单行选择表格视图",
            "caption": "销售订单行",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_sale",
            "viewname": "sale_order_linePickupGridView",
            "viewtag": "8a0a72f7bbc73f7c61a121435a54fc98"
        },
        "hr_contractbasicquickview": {
            "title": "合同快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_contractBasicQuickView",
            "viewtag": "8cc954bbf41346fe5ddea7ec2f24787a"
        },
        "resource_resourcepickupview": {
            "title": "资源数据选择视图",
            "caption": "资源",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_resource",
            "viewname": "resource_resourcePickupView",
            "viewtag": "8f480c3fa80b2efd38fd263d831a26dd"
        },
        "hr_resume_linelineedit": {
            "title": "行编辑表格视图",
            "caption": "员工简历行",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_resume_lineLineEdit",
            "viewtag": "905261c1785724202c268fc22857a2ce"
        },
        "resource_resourcepickupgridview": {
            "title": "资源选择表格视图",
            "caption": "资源",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_resource",
            "viewname": "resource_resourcePickupGridView",
            "viewtag": "9198dbd0c30e749d1d2e35392f0fcd14"
        },
        "resource_calendarpickupview": {
            "title": "资源工作时间数据选择视图",
            "caption": "资源工作时间",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_resource",
            "viewname": "resource_calendarPickupView",
            "viewtag": "9634daea422961cc68105c0284fcd66d"
        },
        "resource_calendargridview": {
            "title": "资源工作时间表格视图",
            "caption": "资源工作时间",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_resource",
            "viewname": "resource_calendarGridView",
            "viewtag": "96e2056c6bcf409f96736bc217c17310"
        },
        "hr_employeepersonalinfoview": {
            "title": "主信息概览",
            "caption": "员工",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeePersonalInfoView",
            "viewtag": "9b54d2ea1b6b287dd66e6c1e672aac31"
        },
        "hr_departmentpickupgridview": {
            "title": "HR 部门选择表格视图",
            "caption": "HR 部门",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_departmentPickupGridView",
            "viewtag": "9e9b93bac1ef049bb6e24b2d217aae7a"
        },
        "hr_employee_categorylineedit": {
            "title": "行编辑表格视图",
            "caption": "员工类别",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employee_categoryLineEdit",
            "viewtag": "a04463a8a14e612c23a7465c42d3f60e"
        },
        "hr_resume_lineeditview": {
            "title": "员工简历行编辑视图",
            "caption": "员工简历行",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_resume_lineEditView",
            "viewtag": "a09d5ebab0b5cec0ed2117814b6e34dc"
        },
        "hr_leaveeditview": {
            "title": "休假编辑视图",
            "caption": "休假",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_leaveEditView",
            "viewtag": "a149ddfe684227db8e5a34d8516ac6c2"
        },
        "hrindexview": {
            "title": "员工首页视图",
            "caption": "员工",
            "viewtype": "APPINDEXVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "HRIndexView",
            "viewtag": "a37277c57cf3e92d7e95e3ca7104792c"
        },
        "hr_jobmastersummaryview": {
            "title": "概览",
            "caption": "工作岗位",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_jobMasterSummaryView",
            "viewtag": "a952d1d8e8ddd0bae26a4d1b6e6dc88f"
        },
        "hr_jobmastertabinfoview": {
            "title": "主信息总览视图",
            "caption": "工作岗位",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_jobMasterTabInfoView",
            "viewtag": "afa6bbe62a681a3630907d594588770b"
        },
        "mail_messagemainview": {
            "title": "消息数据看板视图",
            "caption": "消息",
            "viewtype": "DEPORTALVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageMainView",
            "viewtag": "b0816566792fd556e775203e4dfa964b"
        },
        "res_companypickupgridview": {
            "title": "公司选择表格视图",
            "caption": "公司",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_companyPickupGridView",
            "viewtag": "b271ed05e84698d997c001f9e358641a"
        },
        "hr_resume_line_typepickupview": {
            "title": "简历类型数据选择视图",
            "caption": "简历类型",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_resume_line_typePickupView",
            "viewtag": "b8677ad003803e1eaab6b5aca33f3b91"
        },
        "survey_surveyredirectview": {
            "title": "问卷数据重定向视图",
            "caption": "问卷",
            "viewtype": "DEREDIRECTVIEW",
            "viewmodule": "odoo_survey",
            "viewname": "survey_surveyRedirectView",
            "viewtag": "bab2a924e4f1f6f783c1c15be5f2b64b"
        },
        "mail_messagebyreslistview": {
            "title": "消息列表视图",
            "caption": "消息",
            "viewtype": "DELISTVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_messageByResListView",
            "viewtag": "bb65aa54bf9c7dcc5093c238adcf6e77"
        },
        "account_analytic_accountpickupgridview": {
            "title": "分析账户选择表格视图",
            "caption": "分析账户",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_analytic_accountPickupGridView",
            "viewtag": "c59ce23bdbf54515c4963eace0595df0"
        },
        "hr_leavegridview": {
            "title": "休假表格视图",
            "caption": "休假",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_leaveGridView",
            "viewtag": "c81bf4a79286794aba838b3e058622de"
        },
        "hr_contractgridview": {
            "title": "合同表格视图",
            "caption": "合同",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_contractGridView",
            "viewtag": "c8c4a945338c7b14ac11d56b9d322006"
        },
        "hr_departmentmastertabinfoview": {
            "title": "主信息总览视图",
            "caption": "HR 部门",
            "viewtype": "DETABEXPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_departmentMasterTabInfoView",
            "viewtag": "c8e35a748e9dcd200e18227dec30ef05"
        },
        "product_categorybasicquickview": {
            "title": "快速新建视图",
            "caption": "快速新建",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_product",
            "viewname": "product_categoryBasicQuickView",
            "viewtag": "caea8adbddf79caf7193b69f6f159a19"
        },
        "hr_contractbasiceditview": {
            "title": "合同信息编辑视图",
            "caption": "合同",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_contractBasicEditView",
            "viewtag": "cc0e8ae93915f16260ebb7e16d966699"
        },
        "mail_activitybyreslistview": {
            "title": "活动列表视图",
            "caption": "活动",
            "viewtype": "DELISTVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_activityByResListView",
            "viewtag": "d031b6ebd0b1b5ea5e7a73729821495d"
        },
        "account_move_linepickupview": {
            "title": "日记账项目数据选择视图",
            "caption": "日记账项目",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_move_linePickupView",
            "viewtag": "d6c797b7606af118c9c82b721bea6c6c"
        },
        "res_userspickupgridview": {
            "title": "用户选择表格视图",
            "caption": "用户",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_base",
            "viewname": "res_usersPickupGridView",
            "viewtag": "dab7fa889a32eccea235a6606e44c3c5"
        },
        "hr_employeemastereditview": {
            "title": "主编辑视图",
            "caption": "员工",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeeMasterEditView",
            "viewtag": "dd123e093294210ed5319ccd063709c0"
        },
        "hr_skill_typebasiclistexpview": {
            "title": "配置列表导航视图",
            "caption": "技能类型",
            "viewtype": "DELISTEXPVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_skill_typeBasicListExpView",
            "viewtag": "de39fe439bb68c0ee40f50d086e691be"
        },
        "hr_resume_line_typelineedit": {
            "title": "行编辑表格视图",
            "caption": "简历类型",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_resume_line_typeLineEdit",
            "viewtag": "df72c1fde592f3fdabb51dc833f325e4"
        },
        "ir_attachmentbyresdataview": {
            "title": "附件数据视图",
            "caption": "附件",
            "viewtype": "DEDATAVIEW",
            "viewmodule": "odoo_ir",
            "viewname": "ir_attachmentByResDataView",
            "viewtag": "e0d74eb1892ab881817fe144ee8400dc"
        },
        "survey_surveypickupgridview": {
            "title": "问卷选择表格视图",
            "caption": "问卷",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_survey",
            "viewname": "survey_surveyPickupGridView",
            "viewtag": "e62157d555e4d41070c10edf4b8843d4"
        },
        "account_accountpickupview": {
            "title": "科目数据选择视图",
            "caption": "科目",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_accountPickupView",
            "viewtag": "e7964ada527cb2983b3ced82327401c5"
        },
        "hr_jobpickupgridview": {
            "title": "工作岗位选择表格视图",
            "caption": "工作岗位",
            "viewtype": "DEPICKUPGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_jobPickupGridView",
            "viewtag": "ed9e03dd6f3e139a520f139d5bad9496"
        },
        "account_analytic_accountpickupview": {
            "title": "分析账户数据选择视图",
            "caption": "分析账户",
            "viewtype": "DEPICKUPVIEW",
            "viewmodule": "odoo_account",
            "viewname": "account_analytic_accountPickupView",
            "viewtag": "ef478ad71aa376bc8994ab723c57c120"
        },
        "mail_followersbyreslistview": {
            "title": "文档关注者列表视图",
            "caption": "文档关注者",
            "viewtype": "DELISTVIEW",
            "viewmodule": "odoo_mail",
            "viewname": "mail_followersByResListView",
            "viewtag": "f0960db439f0f13b26e6a43eb5096d9e"
        },
        "hr_employeemasterinfoview": {
            "title": "主信息概览",
            "caption": "员工",
            "viewtype": "DEEDITVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeeMasterInfoView",
            "viewtag": "f3651c83b6944418c948f91616a23eb9"
        },
        "hr_departmentmasterquickview": {
            "title": "快速新建",
            "caption": "HR 部门",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_departmentMasterQuickView",
            "viewtag": "f4d97651e0ab8e42ede8190daba9658a"
        },
        "hr_employeemasterquickview": {
            "title": "快速新建",
            "caption": "员工",
            "viewtype": "DEOPTVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_employeeMasterQuickView",
            "viewtag": "fc5d8102daf9c9af51e2414dab7fdb11"
        },
        "hr_skilllineedit": {
            "title": "行编辑表格视图",
            "caption": "技能",
            "viewtype": "DEGRIDVIEW",
            "viewmodule": "odoo_hr",
            "viewname": "hr_skillLineEdit",
            "viewtag": "ff6183ee62410e029fffc2d2b36b2a0d"
        }
    }];
});

// 获取视图消息分组信息
mock.onGet('./assets/json/view-message-group.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status,{
    }];
});
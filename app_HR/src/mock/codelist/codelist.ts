import { MockAdapter } from '../mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'

// 获取全部数组
mock.onGet('./assets/json/data-dictionary.json').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status, [
        {
        srfkey: "HR_CONTRACT__STATE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "draft",
                label: "新建",
                text: "新建",
                "data":"",
                "codename":"Draft",
                value: "draft",
                
                disabled: false,
            },
            {
                id: "open",
                label: "运行中",
                text: "运行中",
                "data":"",
                "codename":"Open",
                value: "open",
                
                disabled: false,
            },
            {
                id: "close",
                label: "过期",
                text: "过期",
                "data":"",
                "codename":"Close",
                value: "close",
                
                disabled: false,
            },
            {
                id: "cancel",
                label: "已取消",
                text: "已取消",
                "data":"",
                "codename":"Cancel",
                value: "cancel",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "HR_EMPLOYEE__GENDER",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "male",
                label: "男性",
                text: "男性",
                "data":"",
                "codename":"Male",
                value: "male",
                
                disabled: false,
            },
            {
                id: "female",
                label: "女性",
                text: "女性",
                "data":"",
                "codename":"Female",
                value: "female",
                
                disabled: false,
            },
            {
                id: "other",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Other",
                value: "other",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "HR_EMPLOYEE__MARITAL",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "single",
                label: "单身",
                text: "单身",
                "data":"",
                "codename":"Single",
                value: "single",
                
                disabled: false,
            },
            {
                id: "married",
                label: "已婚",
                text: "已婚",
                "data":"",
                "codename":"Married",
                value: "married",
                
                disabled: false,
            },
            {
                id: "cohabitant",
                label: "合法同居者",
                text: "合法同居者",
                "data":"",
                "codename":"Cohabitant",
                value: "cohabitant",
                
                disabled: false,
            },
            {
                id: "widower",
                label: "丧偶",
                text: "丧偶",
                "data":"",
                "codename":"Widower",
                value: "widower",
                
                disabled: false,
            },
            {
                id: "divorced",
                label: "离异",
                text: "离异",
                "data":"",
                "codename":"Divorced",
                value: "divorced",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "HR_EMPLOYEE__CERTIFICATE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "bachelor",
                label: "学士",
                text: "学士",
                "data":"",
                "codename":"Bachelor",
                value: "bachelor",
                
                disabled: false,
            },
            {
                id: "master",
                label: "主版本",
                text: "主版本",
                "data":"",
                "codename":"Master",
                value: "master",
                
                disabled: false,
            },
            {
                id: "other",
                label: "其他",
                text: "其他",
                "data":"",
                "codename":"Other",
                value: "other",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "PRODUCT_CATEGORY__PROPERTY_COST_METHOD",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "standard",
                label: "标准价格",
                text: "标准价格",
                "data":"",
                "codename":"Standard",
                value: "standard",
                
                disabled: false,
            },
            {
                id: "fifo",
                label: "先进先出(FIFO)",
                text: "先进先出(FIFO)",
                "data":"",
                "codename":"Fifo",
                value: "fifo",
                
                disabled: false,
            },
            {
                id: "average",
                label: "平均成本(AVCO)",
                text: "平均成本(AVCO)",
                "data":"",
                "codename":"Average",
                value: "average",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "HR_RESUME_LINE__DISPLAY_TYPE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "classic",
                label: "Classic",
                text: "Classic",
                "data":"",
                "codename":"Classic",
                value: "classic",
                
                disabled: false,
            },
            {
                id: "course",
                label: "课程",
                text: "课程",
                "data":"",
                "codename":"Course",
                value: "course",
                
                disabled: false,
            },
            {
                id: "certification",
                label: "认证",
                text: "认证",
                "data":"",
                "codename":"Certification",
                value: "certification",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "Odoo_truefalse",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "false",
                label: "否",
                text: "否",
                "data":"",
                "codename":"False",
                value: "false",
                
                disabled: false,
            },
            {
                id: "true",
                label: "是",
                text: "是",
                "data":"",
                "codename":"True",
                value: "true",
                
                disabled: false,
            },
        ]
    },
    {
        srfkey: "HR_JOB__STATE",
        emptytext: "未定义",
        "codelisttype":"static",
        items: [
            {
                id: "recruit",
                label: "正在招聘",
                text: "正在招聘",
                "data":"",
                "codename":"Recruit",
                value: "recruit",
                
                disabled: false,
            },
            {
                id: "open",
                label: "停止招聘",
                text: "停止招聘",
                "data":"",
                "codename":"Open",
                value: "open",
                
                disabled: false,
            },
        ]
    }
    ]];
});

